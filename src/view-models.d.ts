/// <reference path="./shop-view-model.d.ts" />

declare module ViewModels {

    export interface IFbAccountKit {
        appId: string;
        csrf: string;
        version: string;
    }

    export interface IPhoneVerification {
        CountryCode: string;
        PhoneNumber: string;
    }

    export interface IErrorDataViewModel {
        ErrorCode?: number;
        Message?: string;
        Data?: any;
    }

    export interface IVariationItemViewModel {
        _id: string;
        Value: string;
        Price: number;
        InStock: boolean;
        Qty: number;
    }

    export interface IVariationViewModel {
        Name: string;
        Items: IVariationItemViewModel[];
    }

    export interface IProductArributeViewModel {
        Id: string;
        Name: string;
        Value: Array<any>;
        UnitOfMeasure: string;
    }

    export interface IProductMediaViewModel {
        Id: string;
        Type: string;
        Name: String;
        Image: String;
    }

    export interface ISaleStatsViewModel {
        Month: number;
        Quarter: number;
        Year: number;
        LifeToTime: number;
        LastThirtyDays: number;
    }

    export interface IProductCategoryViewModel {
        Id: string;
        Name: string;
        Attributes: [{
            Id: string;
            Name: string;
            Values: Array<any>;
        }];
        SubCategories: [{
            Id: string;
            Name: string;
            Attributes: [{
                Id: string;
                Name: string;
                Values: Array<any>;
            }]
        }]
    }

    export interface IProductViewModel {
        _id: string;
        Name: string;
        Description?: string;
        Price: number;
        Currency: string;
        ReviewsCount: number;
        FavoritesCount: number;
        LinkToShopPolicy: string;
        LinkToReviews: string;
        ShipsFrom: string;
        Categories?: IProductCategoryViewModel[];
        IsNew?: boolean;
        Show: string;
        Delete: string;
        Attributes?: IProductArributeViewModel[];
        Variation?: IVariationViewModel;
        ShippingProfile?: string;
        ShopSection: {
            Id: string;
            Name: string
        };
        Shop: {
            Id: string;
            Name: string
        };
        ColumnOne: boolean;
        ColumnTwo: boolean;
        ColumnThree: boolean;
        ColumnFour: boolean;
        QuantityAvailable?: number;
        Trending: number;
        Rating: number;
        Media?: IProductMediaViewModel[];
        ProductUrl: string;
        Images?: Array<any>;
        Dimensions?: any;
        Sales?: ISaleStatsViewModel[];
        Search?: string;
    }

    export interface IProductReviewViewModel {
        _id: string;
        Date: Date;
        Rating: Number;
        Text: string;
        User: {
            Profile: {
                DisplayName: string;
                Picture: string;
            }
        }
    }

    export interface ILoginViewModel {
        UserName: any;
        Password: string;
        StaySignedIn: boolean;
    }

    export interface IRegistrationViewModel {
        // UserName?: string;
        Email?: string;
        ConfirmEmailAddress?: string;
        Password?: string;
        ConfirmPassword?: string;
        Gender?: Number;
        PostalCode?: string;
        ReceiveZgrooNewsLetters?: boolean;
        AcceptTermsAndConditions: boolean;
        Mobile?: Number;
        Profile: {
            FirstName?: string;
            LastName?: string;
            DisplayName?: string;
            DisplayPicture?: string;
            Address: IAddressViewModel;
        };
    }

    export interface IThirdPartyLoginViewModel {
        code: string;
        clientId: string;
        redirectUri: string;
        ReceiveZgrooNewsLetters?: boolean;
        AcceptTermsAndConditions?: boolean;
        IsRegistration: boolean;

    }

    export interface IPreferenceViewModel {
        _id: string;
        User: string;
        Currency: string;
        Country: string;
        ReceiveNewsLetter: Boolean;
        ReceivePhoneCall: Boolean;
        ReceivePostalMail: Boolean;

    }

    export interface ILoginResponseViewModel {
        Token?: string;
        TimeToExpireInDays?: number;
        Activated: boolean;
    }

    export interface IActivationRequestViewModel {
        UserName: string;
        ActivationCode: string;
    }

    export interface IActivationResponseViewModel {
        Activated: boolean;
    }

    export interface IAddressViewModel {
        FirstName: string;
        LastName: string;
        Phone: string;
        Line1: string;
        Line2?: string;
        City: string;
        State: string;
        Zip: string;
        Country: string;
        Email: string;
    }

    export interface IShippingAddressViewModel extends IAddressViewModel {
        User: string;
        _id: string;
        IsPrimary: boolean;
    }

    export interface IProfileViewModel {
        FirstName: string;
        LastName: string;
        DisplayName: string;
        Gender: number;
        Picture: string;
        // Address: IAddressViewModel;
        ShippingAddresses: IShippingAddressViewModel[];
        //Phone: string;

    }

    interface IDebitCreditCardViewModel {
        _id: string;
        NameOnCard: string;
        Address: IAddressViewModel;
        Type: string;
        Number: string;
        SecurityCode: string;
        ExpirationMonth: string;
        ExpirationYear: number;
        User: string;
    }

    export interface IUserViewModel {
        _id: string;
        Email: string;
        SellerTier: number;
        IsApprovedSeller: boolean;
        LastOrderIds: Array<any>;
        Profile: IProfileViewModel;
        Roles: string[];
        Gender: number;
        IsLocked: boolean;
        IsApiUser: boolean;
        UserType: number;
        CreditCard: IDebitCreditCardViewModel;
        Shop: Shop.IShopViewModel;
        Activated: boolean;
        ResetPassword: boolean;
        TermsAndConditionsAccepted: boolean;
        IsLocalLogin: boolean;
    }

    export interface IUserAccountViewModel {

        CreditCard: IDebitCreditCardViewModel;

    }

    export interface ICollectionViewModel<T> {
        TotalPages: number;
        TotalItems: number;
        CurrentPage: number;
        Items: T[];
    }

    export interface ISessionViewModel {
        User: ViewModels.IUserViewModel;
    }

    export interface ILogRequestViewModel {
        Url: string;
        Message: string;
        Trace: string;
        Cause?: string;
    }

    export interface IMasterDataItemViewModel {
        Code: string;
        Name: string;
    }

    export interface IMasterDataViewModel {
        _id: string;
        Category: string;
        Items: IMasterDataItemViewModel[];
    }

    export interface IAttributeDisplayViewModel extends IAttributeViewModel {
        IsDerived?: boolean;
    }

    export interface IAttributeViewModel {
        _id: string;
        Name: string;
        IsSystemDefined: boolean;
        Values?: Array<any>;
        UnitOfMeasures?: string[];
    }
    export interface IReasonViewModel {
        _id: string;
        Reason: string;
    }

    export interface ICategoryViewModel {
        _id: string;
        Name: string;
        Domain: string;
        HomePage: number;
        Attributes: any[];
        ParentCategory: string;
    }

    export interface ICategoryDisplayViewModel {
        _id: string;
        Name: string;
        Attributes: IAttributeDisplayViewModel[];
        ParentCategory: string;
        Level: number;
    }

    export interface ICategoryListDisplayViewModel {
        Name: string;
        ChildCategories?: string[];
    }

    export interface IVendorViewModel {
        _id: string;
        Name: string;
        NumberOfProducts: number;
        NumberOfFavorites: number;
        LogoUrl: string;
    }

    export interface IAttributeSaveViewModel extends ViewModels.IAttributeViewModel {
        CurrentUnitOfMeasure: string;
        CurrentValue: string;
    }



    export interface IMediaViewModel {
        _id?: string;
        Type: string;
        Image: string;
    }

    export interface IShippingDetailViewModel {
        Country: string;
        ShippingMethod: IShippingMethodViewModel[];
    }

    export interface IShippingMethodViewModel {
        Name: string;
        ProcessingTime: string;
        Default: boolean;
        ShipsTo: IShipsToViewModel[];
    }

    export interface IShipsToViewModel {
        Zone: string;
        Cost: number;
        CostWithAnotherItem: number;
        FreeShippingTotalValue: number;
        CashOnDelivery: number;
    }
    export interface IPackageDetailViewModel{
        name:String,
        noOfItems:Number,
        weight:Number,
        height:Number,
        length:Number,
        width:Number,
    }

    export interface IShippingProfileViewModel {
        _id: string;
        User: string;
        Name: string;
        Shop: {
            Id: string;
            Name: string;
        };
        CODPrice: string;
        ShipsFrom: string;
        ProcessingTime: string;
        IsCustom: boolean;
        Detail: IShippingDetailViewModel[];
        ShippingCountries: Array<any>;
    }

    export interface IShopSectionViewModel {
        _id: string;
        Shop: string;
        Name: string;

    }

    export interface IChangePasswordRequestViewModel {
        CurrentPassword: string;
        NewPassword: string;
        ConfirmNewPassword: string;
        Token: string;
    }


    export interface IPopulateViewModel {
        path: string;
        options?: {
            limit: number;
            offset: number;
        };
        select?: string;
    }

    export interface ICartViewModelOld {
        UserId?: string;
        _id: string;
        ProductName: string;
        ProductImage: string;
        Quantity: number;
        Price: number;
        ShopId: string;
        Variation?: IVariationItemViewModel;

    }

    export interface ICartItemViewModel {
        Shop: {
            Id: string;
            Name: string;
        };
        PayKey: string;
        PaymentStatus: string;
        PaymentDetails: any;
        Product: {
            ProductStatus: string;
            IsProductDeleted: boolean;
            Id: string;
            Name: string;
            Qty: number;
            Price: number;
            QtyFlag: string;
            PriceFlag: string;
            QuantityAvailable: number;
            CouponCode: string;
            DiscountType: string;
            DiscountValue: number;
            ReductionAmount: number;
            NewPriceAfterDiscount: number;
            ShippingCost: number;
            CostWithAnotherItem: number;
            ShippingMethod: string;
            ShippingMethodName: string;
            ShippingProfile: any;
            Variation: IVariationViewModel[];
            Attributes: Array<any>;
            Media: IMediaViewModel[];
        };
        // OriginalPrice: number;
        //ShippingCost?: number;
        Tax?: number;
        Price?: number;
        CanShip?: boolean;
        CreatedDate: Date;
        ModifiedDate: Date;
    }

    export interface ICartTotalViewModel {
        Shop?: string;
        ProductCost: number;
        TotalCost: number;
        ShippingCost: number;
        Tax: number;
        Discount: number;
        CouponDiscount: number;
    }

    export interface IGrouppedCartViewModel extends ICartViewModel {
        GrouppedItems?: {
            ShopName: string;
            Items: ICartItemViewModel[];
            Total: ICartTotalViewModel;
        }[];
        GrandTotal?: ICartTotalViewModel;
    }

    export interface ICartViewModel {
        _id?: string;
        User?: string;
        Totals?: ICartTotalViewModel[];
        Coupons?: [{
            Shop: string;
            Code: string;
        }];
        ShippingAddress?: IAddressViewModel;
        CreditCard?: {
            NameOnCard: string;
            Address: {
                Line1: string;
                Line2?: string;
                City: string;
                State: string;
                Zip: string;
                Country: string;
            };
            Number: string;
            ExpirationMonth: string;
            ExpirationYear: number;
        };
        Items?: ICartItemViewModel[];
        CreatedDate?: Date;
        ModifiedDate?: Date;
    }

    export interface IOrderViewModel {
        _id: string;
        User: string;
        OrderId: string;
        OrderDetailId: string;
        ZepoReferenceId: string;
        ReplacementReferenceId: string;
        TotalsArray: Array<any>;
        Totals: ICartTotalViewModel[];
        PaymentStatus: string;
        TransactionId: string;
        PaymentDetails: any;
        ShopIds: string;
        Status: number;
        Coupons: [{
            Shop: string;
            Code: string;
        }];
        FromAddress: any;
        ShippingAddress: IAddressViewModel;
        CreditCard?: {
            NameOnCard: string;
            Address: {
                Line1: string;
                Line2?: string;
                City: string;
                State: string;
                Zip: string;
                Country: string;
            };
            Number: string;
            ExpirationMonth: string;
            ExpirationYear: number;
        };
        Items: Array<any>;
        //Items?: ICartItemViewModel[];
        CreatedDate: Date;
        OrderStatus: string;
        ModifiedDate: Date;
    }

    export interface IOrderParamsViewModel {
        payment_mode: string;
        number_of_package: number;
        package_details: IOrderPackageDetailsViewModel[];
        product_type: string;
    }

    export interface IOrderPackageDimensionViewModel {
        weight: number;
        height: number;
        length: number;
        width: number;
    }

    export interface IOrderPackageDetailsViewModel {
        package_content: string;
        no_of_items: number;
        invoice_value: number;
        package_dimension: IOrderPackageDimensionViewModel;
    }

    export interface IOrderDetailViewModel {
        _id: string;
        User: string;
        OrderId: string;
        Params: IOrderParamsViewModel;
        CreatedDate: Date;
        ModifiedDate: Date;
        /*_id: string;
        User: string;
        OrderId: string;
        Totals: ICartTotalViewModel[];
        ShippingAddress: IAddressViewModel;
        Status: number;
        CreditCard?: {
            NameOnCard: string;
            Address: {
                Line1: string;
                Line2?: string;
                City: string;
                State: string;
                Zip: string;
                Country: string;
            };
            Number: string;
            ExpirationMonth: string;
            ExpirationYear: number;
        };
        Items?: ICartItemViewModel[]*/
    }

    export interface IPaypalModel {

    }

    export interface IOrderConfirmationModel {
        ShippingAddress: IShippingAddressViewModel;
        CreditCard: IDebitCreditCardViewModel;
    }

    export interface IRecentlyViewedProductViewModel {
        User?: string;
        Product: {
            Id: string;
            Name: string;
            Price?: number;
            Categories: string[];
        }
        Shop: {
            Id: string;
            Name: string;
        },
        Date?: Date;
    }
    export interface IFavoriteProductViewModel {
        Product: {
            Id: string;
            IsProductDeleted: boolean;
            Name: string;
            //Price: number;
            Media: string;
        }
        /*Shop: {
            Id: string;
            Name: string;
        };*/
        CreatedDate?: Date,
        ModifiedDate?: Date
    }
    export interface IFeaturedProductViewModel {
        _id?: string,
        Product: {
            Id: string,
            Name: string,
            Price?: number
        }
        Shop: {
            Id: string,
            Name: string
        },
        CreatedDate?: Date,
        ModifiedDate?: Date
    }
    export interface IFavoriteShopViewModel {
        Shop: {
            Id: string;
            Name: string;
            Logo: string;
        };
        CreatedDate: Date;
        ModifiedDate: Date;
    }
    export interface IFeaturedShopViewModel {
        _id?: string;
        Shop: {
            Id: string;
            Name: string;
            Banner?: string;
        },
        NumberOfProducts?: number;
        NumberOfFollowers?: number;
        CreatedDate?: Date;
        ModifiedDate?: Date;
    }
    export interface IMessageViewModel {
        To: string,
        From: string,
        FromUserImage: string;
        ToUserImage: string;
        Subject: string,
        Message: string,
        Attachment: Array<any>;
        VisibilityTo: string;
        VisibilityFrom: string;
        Replies: IMessageRepliesViewModel[];
        CreatedDate?: Date;
        ModifiedDate?: Date;
    }

    export interface IMessageRepliesViewModel {
        From: string,
        FromUserImage: string;
        To: string,
        Reply: string;
        Attachment: Array<any>;
        VisibilityTo: string;
        VisibilityFrom: string;
        CreatedDate?: Date;
    }

    export interface IReviewViewModel {
        User: {
            Id: String,
            Name: String,
            Image: String
        },
        Shop: {
            Id: String,
            Name: String
        },
        Product: {
            Id: string;
            Name: string;
            Image: string;
            IsProductDeleted: boolean;
        },
        // Product: String,
        // Shop: String,
        IsVerifiedBuyer: boolean,
        Rating: Number,
        Review: String,
        ReviewHeadLine: String,
        CreatedDate: Date
    }

    export interface IRatingViewModel {
        Product: String,
        OneStar: Number,
        TwoStar: Number,
        ThreeStar: Number,
        FourStar: Number,
        FiveStar: Number,
        Rating: Number,
        CreatedDate: Date,
        ModifiedDate: Date
    }
    export interface IBannerViewModel {
        BannerUrl: string;
        Dimensions: any;
        AdminCriteriaId: any;
        CreatedDate: Date;
        ModifiedDate: Date;
    }

    export interface IAwsImageModel {
        Name: String,
        ImageData: String
    }
 
    export interface ICommentsViewModel {
        CommentedUser: string;
        DisplayPicture: string;
        Comment: string;
        CreatedDate?: Date;
    }

    export interface IFeedbackViewModel {
        User: string;
        DisplayPicture: string;
        Title: string;
        Feedback: string;
        Status: string;
        VoteCount: number;
        IsVoted: boolean;
        UserId: Array<any>;
        Comments: ICommentsViewModel[];
        CreatedDate: Date;
    }

    export interface IAdminCriteriaModel {
        ColumnOne: string,
        ColumnTwo: string,
        ColumnThree: string,
        ColumnFour: string,
        Limit: number
    }

    export interface IAdminShopbyModel {
        Name: string;
        Criteria: any;
        Show: boolean;
    }

    export interface IAdminShopbyCriteriaModel {
        Price: {
            From: number,
            To: number
        };
        Color: string[];
        Shop: string[];
        Discount: {
            Amount: number,
            Percentage: number
        };
        Categories: string[];
        SubCategories: string[];
        Condition: string;
        Gender: string[];
        GemStone: {
            Color: string[];
            Type: string[];
            Shape: string[];
            Treatment: string[];
        };
        MandatoryFields: string[];
        Query: string;
        Size: number;
        Material: string[];
        Metal: string[];
        Karat: number;
        MainStone: string[];
        Brand: string[];
        RecentlyListed: string[];
        SoldInLastThreeSixtyFiveDays: string[];
        FreeShipping: boolean;
        Country: string[];
        ShipsTo: string[];
        ShipsFrom: string[];
        SellerWasActive: string[];
        SellerRating: number;
        ProductInStock: boolean;
    }

    export interface ISaveForLaterModel {
        Shop: {
            Id: string;
            Name: string;
        };
        Product: {
            ProductStatus: string;
            IsProductDeleted: boolean;
            Id: string;
            Name: string;
            Qty: number;
            Price: number;
            QtyFlag: string;
            PriceFlag: string;
            QuantityAvailable: number;
            CouponCode: string;
            DiscountType: string;
            DiscountValue: number;
            ReductionAmount: number;
            NewPriceAfterDiscount: number;
            ShippingCost: number;
            CostWithAnotherItem: number;
            ShippingMethod: string;
            ShippingMethodName: string;
            ShippingProfile: any;
            Variation: IVariationViewModel[];
            Attributes: Array<any>;
            Media: IMediaViewModel[];
        };
        CreatedDate: Date;
        ModifiedDate: Date;
    }

    export interface IThreeColumnViewModel {
        BoldContent: string;
        NormalContent: string;
        Image: string;
        AdminCriteriaId: string;
        CreatedDate: Date;
        ModifiedDate: Date;
    }

    export interface IRepliesViewModel {
        RepliedUserName: string;
        RepliedUserPicture: string;
        Message: string;
        Attachment: Array<any>;
        CreatedDate: Date;
    }

    export interface ISellerCareViewModel {
        UserId: string;
        UserName: string;
        UserPicture: string;
        From: string;
        To: string,
        Subject: string;
        Replies: IRepliesViewModel[];
        TicketStatus: string;
        TicketId: string;
        Domain: string;
        CreatedDate: Date;
        ShopId:string;
        Shop:string;
        Email:string;
    }

    export interface IOrderTicketsMessagesViewModel {
        From: string;
        To:String;
        UserPicture: string;
        Reason: string;
        Description: string;
        Attachment: Array<any>;
        CreatedDate: Date;
        IsAdmin: boolean;
    }

    export interface IOrderTicketsViewModel {
        User: {
            Id: string;
            Name: string;
        },
        IsAdmin: boolean,
        OrderId: string;
        OrderCreatedDate: Date,
        Product: {
            Id: string;
            Name: string;
            Qty: number;
            Price: number;
            TotalCost: number;
            ReturnQty: number;
            ProductIndexInOrder: number;
            Variation: {
                Id: string;
                Name: string;
            };
            Status: string;
        },
        TicketId: string;
        TicketStatus: string;
        Messages: IOrderTicketsMessagesViewModel[];
        Replies: IRepliesViewModel[];
        CreatedDate: Date
    }

    export interface ISpecificCommissionViewModel {
        SpecificCommission: number;
        CourierCommission: number;
        StartDate: Date;
        ExpiryDate: Date;
    }

    export interface ICommissionViewModel {
        User: {
            Id: string;
            Name: string;
        },
        Shop: {
            Id: string;
            Name: string;
        },
        DefaultCommission: number;
        GST: number;
        CourierCommission: number;
        CollectionFee: number;
        FixedFee: number;
        Plans: ISpecificCommissionViewModel[];
    }

    export interface IDefaultCommissionViewModel {
        User: string;
        DefaultCommission: number;
        GST: number;
        CourierCommission: number;
        CollectionFee: number;
        FixedFee: number;
        GSTJewelProducts: number,
        HSNNumber: number
    }

    export interface IForumGroupTitleViewModel {
        GroupTitle: string;
        ButtonTitle: string;
        CreatedDate: Date;
    }

    export interface IResponseViewModel {
        User: {
            Id: string;
            Name: string;
            Image: string;
        };
        Response: string;
        Replies: IReplyViewModel[];
        CreatedDate: Date;
    }

    export interface IReplyViewModel {
        User: {
            Id: string;
            Name: string;
            Image: string;
        };
        Reply: string;
        CreatedDate: Date;
    }

    export interface IPostsViewModel {
        User: {
            Id: string;
            Name: string;
            Image: string;
        };
        Title: string;
        Post: string;
        IsOpened: boolean;
        Tags: string;
        Pin: boolean;
        Status: string;
        MarkThread: boolean;
        MarkedUserIds: Array<any>;
        IsLiked: boolean;
        LikedUserIds: Array<any>;
        ResponseCount: number;
        LikesCount: boolean;
        CreatedDate: Date;
        ModifiedDate: Date;
        Responses: IResponseViewModel[];
    }

    export interface IForumContentViewModel {
        Group: {
            Id: string;
            Name: string;
            ButtonTitle: string;
        };
        Posts: IPostsViewModel[];
        CreatedDate: Date;
    }

    export interface IAnnouncementResponseViewModel {
        User: {
            Id: string;
            Name: string;
            Image: string;
        };
        Response: string;
        MarkThread: boolean;
        MarkedUserIds: Array<any>;
        CreatedDate: Date;
    }

    export interface IAcnnouncementViewModel {
        User: {
            Id: string;
            Name: string;
            Image: string;
        };
        Title: string;
        Content: string;
        ResponseCount: number;
        Responses: IAnnouncementResponseViewModel[];
        CreatedDate: Date,
        ModifiedDate: Date
    }

    export interface IZepoCourierDetailsViewModel {
        User: {
            Id: string;
            Name: string;
        };
        Shop: string;
        OrderId: string;
        TransactionDetails: any;
        CreatedDate: Date;
    }

    export interface IOrderEmailRepliesViewModel {
        FromUserName: string;
        ToUserName: string;
        Subject: string;
        Message: string;
        Attachments: Array<any>;
        ProductStatus: string;
        /*Product: {
            Id: string;
            Name: string;
            Qty: number;
            Price: number;
        };
        ShopId: string;
        ShippingCost: number;
        TotalCost: number;
        TicketId: string;
        TicketStatus: string;*/
        CreatedDate: Date
    }

    export interface IOrderEmailContentViewModel {
        OrderId: string;
        FromUserName: string;
        ToUserName: string;
        Subject: string;
        Message: string;
        Attachments: Array<any>;
        ProductStatus: string;
        ShipsToAddress: any;
        Product: {
            Id: string;
            Name: string;
            Qty: number;
            Price: number;
        };
        ShopId: string;
        ShippingCost: number;
        TotalCost: number;
        TicketId: string;
        TicketStatus: string;
        Replies: IOrderEmailRepliesViewModel[];
        CreatedDate: Date
    }
}
declare module "view-models" {
    export = ViewModels;
}