declare module ViewModels.PayPal {
    export interface IPaymentRequestViewModel {
        intent: string;
        payer: {
            payment_method: string;
            funding_instruments: IFundingInstrumentsViewModel[];
        };
        transactions: ITransactionViewModel[];
    }
    export interface ITransactionViewModel {
        amount: {
            total: string;
            currency: string;
            details: {
                subtotal: string;
                tax: string;
                shipping: string;
            }
        };
        description: string;
    }
    export interface IFundingInstrumentsViewModel {
        credit_card: {
            type: string;
            number: string;
            expire_month: string;
            expire_year: string;
            cvv2: string;
            first_name: string;
            last_name: string;
            billing_address: {
                line1: string;
                city: string;
                state: string;
                postal_code: string;
                country_code: string;
            };
        }
    }

    export interface IVerifyStatusRequestViewModel {
        requestEnvelope: {
            errorLanguage: string;
            detailLevel: string;
        };
        accountIdentifier: {
            emailAddress: string;
        };
        firstName: string;
        lastName: string;
        matchCriteria: string;
    }

    export interface IPaypalAccountViewModel {
        AccountId: string;
        Email: string;
        IsVerified: boolean;
        AccountType: string;
    }
    
    export interface IPaymentAcknowledgementViewModel {
        PaymentId: string;
        SaleId: string;
        PaymentState: string;
        Intent: string;
        PaymentMethod: string;
        SaleState:string;
    }
}