var SimilarProduct = (function() {

  return {
    load: function() {
      $('.Clssimilar-product').slick({
        arrows: true,
        infinite: false,
        speed: 300,
        slidesToShow: 8,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 5,
              slidesToScroll: 1,
              infinite: true,
              arrows: true
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 5,
              slidesToScroll: 1,
              arrows: true
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              arrows: true
            }
          }
        ]
      });
    }
  }

})(SimilarProduct||{})

var FeaturedProduct = (function() {

  return {
    load: function() {
      $('.Clsfeature-product').slick({
        arrows: true,
        infinite: false,
        speed: 300,
        slidesToShow: 8,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 5,
              slidesToScroll: 1,
              infinite: true,
              arrows: true
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 5,
              slidesToScroll: 1,
              arrows: true
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              arrows: true
            }
          }
        ]
      });
    }
  }

})(FeaturedProduct||{})


$(document).ready(function () {
  $(".navbar-nav li a").not(".dropdown-toggle").click(function(event) {
      $(".navbar-collapse").collapse('hide');
  });
});

$(document).on("click", function(){
  $(".navbar-collapse").collapse('hide');
});

// $(document).on('click','.navbar-collapse.in',function(e) {
//   if( $(e.target).is('a') ) {
//       $(this).collapse('hide');
//   }
//   $(this).collapse('hide');
// });

$(".ngx-gallery-thumbnail").on("click", function(){
  alert(1);
  $("html, body").animate({ scrollTop: 0 }, "slow");
  return false;
});

$(".imgAdd").click(function(){
  $(this).closest(".row").find('.imgAdd').before('<div class="col-sm-2 imgUp"><div class="imagePreview"></div><label class="btn btn-primary">Upload<input type="file" class="uploadFile img" value="Upload Photo" style="width:0px;height:0px;overflow:hidden;"></label><i class="fa fa-times del"></i></div>');
});
$(document).on("click", "i.del" , function() {
	$(this).parent().remove();
});
$(function() {
    $(document).on("change",".uploadFile", function()
    {
    		var uploadFile = $(this);
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
 
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
uploadFile.closest(".imgUp").find('.imagePreview').css("background-image", "url("+this.result+")");
            }
        }
      
    });
});

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();  
  // ('.Clstooltip').hover(
  //   function () {
  //     $('.Clstooltip_text').show();
  //   }, 
  //   function () {
  //     $('.Clstooltip_text').hide();
  //   }
  // ); 

});