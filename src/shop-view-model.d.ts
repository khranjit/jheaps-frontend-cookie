/// <reference path="view-models.d.ts"/>
/// <reference path="paypal-view-model.d.ts"/>
declare module ViewModels.Shop {
    export interface IShopViewModel {
        _id: string;
        Name: string;
        Currency: string;
        Logo: string;
        HideShop?: boolean;
        Banner?: string;
        Summary?: string;
        TopProducts: Array<any>;
        ProductCount: number;
        TopProductsCount: number;
        CashOnDeliveryAllowed:boolean;
        Headline: string;
        Photos: Array<any>;
        Facebook: string;
        Twitter: string;
        GooglePlus: string;
        LinkedIn: string;
        Pinterest: string;
        ShopAddress: any;        
        BannerDimensions?: any;
        LogoDimensions?: any;
        CreatedDate?: Date;
        ModifiedDate?: Date;
    }

    export interface IShopAccountViewModel {
        _id: string;
        User: string;
        Shop: {
            Id: string;
            Name: string;
        };
        IsApprovedBusinessDetails: string;
        IsApprovedBankDetails: string;
        Paypal: PayPal.IPaypalAccountViewModel;
        CreditCard: IDebitCreditCardViewModel;
        BusinessDetails: IBusinessDetailsViewModel[];
        BankDetails: IBankDetailsViewModel[];
    }

    export interface IOpenShopViewModel {
        Domain: string;
        Shop: IShopViewModel;
        Profile: IProfileViewModel;
        ShopAccount: IShopAccountViewModel;
        AgreeToZgrooTermsAndConditions: boolean;
        PhoneVerificationMethod: string;
        PhoneVerificationCode: string;
        HearedAboutZgrooCode?: string;
        HearedAboutZgrooText?: string;
    }

    export interface IBusinessDetailsViewModel {
        Name: string;
        Type: string;
        GstinProvisionalId: string;
        GstinProof: string;
        TanNumber: string;
        TanProof: string;
        PersonalPanNumber: string;
        BusinessPanNumber: string;
        PanProof: string;
        Signature: string;
        AttachAddressProof: string;
        IsApprovedBankDetails:string;
        Profile: Object;
        ShipsFromAddress:Object;
        CreatedDate: Date;
        ModifiedDate: Date;
    }

    export interface IBankDetailsViewModel {
        AccountHolderName: string;
        AccountNumber: number;
        ConfirmAccountNumber: number;
        IfscCode: string;
        BankName: string;
        State: string;
        City: string;
        District: string;
        BranchName: string;
        CancelledCheckCopy: string;
        CreatedDate: Date;
        ModifiedDate: Date;
    }

    export interface IPoliciesViewModel {
        ReturnPolicy?: string;
        PaymentPolicy?: string;
        ShippingPolicy?: string;
    }

    export interface ITaxRateViewModel {
        _id: string;
        Shop: string;
        Jurisdiction: string;
        Country: string;
        State: string;
        ZipCode: string;
        ZipCodeRange: {
            From: string;
            To: string;
        };
        Tax: number;

    }

    export interface IDiscountViewModel {
        MinimumOrderValue: number;
        DiscountValue: number;
        DiscountType: string; //Percentage or Flat rate
    }

   	export interface ICouponViewModel {
        _id: string;
        Shop: {
            Id: string,
            Name: string;
        };
        Code: string;
        StartDate: Date,
        ExpiryDate: Date;
        DiscountType: string;
        DiscountValue: number; //Percentage or Flat rate
        //IsExpired: boolean;
        Products: string[];
        CreatedDate?: Date;
        ModifiedDate?: Date;
    }
    export interface IShopSettingsViewModel {
        _id: string;
        Shop: string;
        Messages?: IMessageViewModel;
        Policies?: IPoliciesViewModel;
        TaxRates?: ITaxRateViewModel[];
        Discount?: IDiscountViewModel;
        Coupons?: ICouponViewModel[];
    }

    export interface IMessageViewModel {
        MessagesToBuyer?: string;
        ShopAnouncement?: string;
        MessagesToDigitalProductsBuyers?: string;
        ReceiptInfo?: string;
    }
}