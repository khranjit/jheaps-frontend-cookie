"use strict";
exports.__esModule = true;
var request = require("request");
var nconf = require("nconf");
var q = require("q");
var http = require("http");
var base64 = require('base-64');

var fs = require('fs');
var fstream = require('fstream');
var Proxy = /** @class */ (function () {
    function Proxy() {
    }
    Proxy.ApiProxy = function (hostR, portR) {
        return function (req, res, next) {
            var options = {
                method: req.method
            };
            if (req.originalUrl.match("/elastic")) {
                req.originalUrl = req.originalUrl.split('/elastic')[1];
                var body = req.body;
                var mustArray = [];
                var stockobject =         
                {
                    "bool": {
                            "should": [
                                    {"bool": {"must":[{"term":{ "useVariation":false}},{"range":{"QuantityAvailable":{"gt": 0 }}}]}},
                                    {"bool": {"must":[{"term":{ "useVariation":true}},{"term":{"useDefaultQuantity":false}},{"range":{"VariationQuantityAvailable":{"gt": 0 }}}]}},
                                    {"bool": {"must":[{"term":{ "useVariation":true}},{"term":{"useDefaultQuantity":true}},{"range":{"QuantityAvailable":{"gt": 0 }}}]}}
                                    ]
                            }
                }
                if (req.body.hasOwnProperty("query") && !req.originalUrl.match("/jheaps.shopsearches")) {
                    if (req.body.query.hasOwnProperty("bool")) {
                        if (req.body.query.bool.hasOwnProperty("must")) {
                            mustArray = req.body.query.bool.must;
                        }
                    }
                    else if (req.body.query.hasOwnProperty("constant_score")) {
                        if (req.body.query.constant_score.hasOwnProperty("filter")) {
                            if (req.body.query.constant_score.filter.hasOwnProperty("bool")) {
                                if (req.body.query.constant_score.filter.bool.hasOwnProperty("must")) {
                                    mustArray = req.body.query.constant_score.filter.bool.must;
                                }
                            }
                        }
                    }
                        var onlyNonDeleted = {
                            "match_phrase": {
                                "Delete": "false"
                            }
                        };
                        var onlyActive = {
                            "match_phrase": {
                                "Show": "Active"
                            }
                        };
                        var variationQty = {
                            "match_phrase": {
                                "VariationQuantityAvailable": 0
                            }
                        }
                        var isMatchPhrase = mustArray.some(function (object) {
                            return object.hasOwnProperty("match_phrase");
                        });
                        if (isMatchPhrase) {
                            var isNonDeleted = mustArray.some(function (object) {
                                if (object.hasOwnProperty("match_phrase")) {
                                    return object.match_phrase.hasOwnProperty("Delete");
                                }
                            });
                            var isShowNonActive = mustArray.some(function (object) {
                                if (object.hasOwnProperty("match_phrase")) {
                                    return object.match_phrase.hasOwnProperty("Show") ? object.match_phrase["Show"] != 'Active' : false;
                                }
                            });
                            var Quantity = mustArray.some(function (object) {
                                if (object.hasOwnProperty("bool")) {
                                    return object.bool.hasOwnProperty("should");
                                }
                            });
                            if (!(isNonDeleted)) {
                                mustArray.push(onlyNonDeleted);
                            }
                            if (!(isShowNonActive) && !Quantity) {
                                mustArray.push(onlyActive);
                                mustArray.push(stockobject);
                            }
                            else
                            {
                                if(!(isShowNonActive))
                                {
                                    mustArray.push(onlyActive);
                                }
                            }
                        }
                        else {
                            mustArray.push(onlyNonDeleted);
                            mustArray.push(onlyActive);
                            mustArray.push(stockobject);
                        }
                }
            }
            if (portR) {
                options['url'] = "http://" + hostR + ":" + portR + req.originalUrl;
            }
            else {
                options['url'] = "http://" + hostR + req.originalUrl;
            }
            if (!req.query['rand'] && !options['url'].match("9200")) {
                if (req.originalUrl.indexOf('?') > -1) {
                    options['url'] += '&rand=' + new Date().getTime();
                }
                else {
                    options['url'] += '?rand=' + new Date().getTime();
                }
            }
            var method = req.method.toUpperCase();
            options['headers'] = {
                'content-type': 'application/json',
                'x-api-key': 'f0f650dd-0ddc-4021-bdf8-b82e99991afa'
            };
            if (req.header('Authorization')) {
                options['headers']['Authorization'] = req.header('Authorization');
            }
            if (method == 'POST' || method == 'PUT') {
                options['body'] = JSON.stringify(req.body);
            }
            if (req.header('authorization')) {
                req['session']['authorization'] = req.header('authorization');
            }
            else if (req['session']['authorization']) {
                req.headers['authorization'] = req['session']['authorization'];
            }
            if(options['url'].match("9200"))
            {
                options['headers']['Authorization'] = 'Basic ' + base64.encode('admin:UJsyfh3vEV');
                if(method == 'POST' && options['url'].match("_update"))
                {
                    if(req.body.doc.hasOwnProperty("isTopProduct"))
                    {
                        req.body.doc = { isTopProduct: req.body.doc.isTopProduct};
                        options['headers']['Authorization'] = 'Basic ' + base64.encode('admin:UJsyfh3vEV');
                    }
                }
            }
            if (req.files && req.files['file']) {
                var data = req.body;
                data = encodeURIComponent(JSON.stringify(data));
                var apiSettings = {
                    "host": hostR,
                    "port": portR
                }; 
                var options_1 = {
                    method: 'POST',
                    host: apiSettings.host,
                    port: apiSettings.port,
                    path: req.originalUrl + '?data=' + data + '&fileName=' + req.files['file'].name,
                    headers: {
                        'x-api-key': 'f0f650dd-0ddc-4021-bdf8-b82e99991afa',
                        'authorization': req.headers['authorization']
                    }
                };
                var httpRequest1 = http.request;
                var httpRequest = http.request(options_1, function (proxyResponse) {
                    fs.unlink(req.files['file'].path);
                    if (proxyResponse.statusCode == 200) {
                        res.end(req.body);
                    }
                    else {
                        res.send(proxyResponse.statusCode);
                    }
                });
                fstream.Reader({
                    "path": req.files['file'].path
                }).pipe(httpRequest);
            }
            else {
                var proxyRequest = request(options, function (err, proxyResponse, body) {
                    if (err) {
                        res.status(500).end();
                        return;
                    }
                    if (proxyResponse.statusCode == 404) {
                        res.status(proxyResponse.statusCode).end('Not found');
                        return;
                    }
                    if (proxyResponse.statusCode >= 400) {
                        res.status(proxyResponse.statusCode).end(body);
                        return;
                    }
                    if (req.method.toLowerCase() != 'get') {
                        res.send(body && body.length > 0 ? JSON.parse(body) : '');
                    }
                });
                if (req.method.toLowerCase() == 'get') {
                    req
                        .pipe(proxyRequest)
                        .pipe(res);
                }
            }
        };
    };
    Proxy.CallApi = function (url, method, req) {
        var options = {}, defer = q.defer();
        options['url'] = nconf.get('pp-api') + url;
        if (req.originalUrl.indexOf('?') > -1) {
            options['url'] += '?rand=' + new Date().getTime();
        }
        else {
            options['url'] += '&rand=' + new Date().getTime();
        }
        if (!req.header('authorization') && req['session']['authorization']) {
            req.headers['authorization'] = req['session']['authorization'];
        }
        options['headers'] = {
            'content-type': 'application/json',
            'x-api-key': nconf.get('pp-api-key')
        };
        if (method == 'POST' || method == 'PUT') {
            options['body'] = JSON.stringify(req.body);
        }
        var proxyRequest = request(options, function (err, proxyResponse, body) {
            if (err) {
                defer.reject(err);
                return;
            }
            else {
                if (body) {
                    defer.resolve(JSON.parse(body));
                }
                else {
                    defer.resolve(null);
                }
            }
        });
        req.pipe(proxyRequest);
        return defer.promise;
    };
    return Proxy;
}());
module.exports = Proxy;
