  import { NgModule } from '@angular/core';
  import { Routes, RouterModule } from '@angular/router';
  
  
  const routes: Routes = [
    { path: '', loadChildren:'./root/home/home.module#HomeModule' },

    //Home Page,Product Page,Shop page, Cart Page Modules
    {path: 'user', loadChildren:'./root/root.module#RootModule',runGuardsAndResolvers: 'always'},

    //myaccounts
    {path: 'myaccounts', loadChildren:'./myaccounts/myaccounts.module#MyAccountsModule'},

    //404 page
    { path: "**", loadChildren:'./root/notfoundpage/notfoundpage.module#NotFoundPageModule' }
    

  ];
  
  @NgModule({
    imports: [
      RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})
    ],
    exports: [RouterModule],
    providers: []
  })
  export class AppRoutingModule { }
  
