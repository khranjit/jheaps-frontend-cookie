import { Component,Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material';

@Component({
    selector: 'snack-bar-component-example-snack',
    templateUrl: 'SuccessSnack.html',
    styles: [`
      .example-pizza-party {
        color: hotpink;
      }
    `],
})
export class SuccessSnackComponent {
    constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) { }
}
@Component({
    selector: 'snack-bar-component-example-snack',
    templateUrl: 'ErrorSnack.html',
    styles: [`
      .example-pizza-party {
        color: hotpink;
      }
    `],
})
export class ErrorSnackComponent {
    constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) { }
}
