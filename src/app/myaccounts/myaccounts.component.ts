import { Component,AfterViewInit} from '@angular/core';
import { UserService } from '../services/user.service';


declare var jQuery: any;
declare const gapi: any;
@Component({ 
  
  providers:[UserService],
  selector: 'myaccount',
  templateUrl: './myaccounts.component.html'
})

export class myaccountComponent implements AfterViewInit{
    ShowUserRole : any;
    UserType : any;
    errorMessage : any;
    loginInfo: any;
    Roles : any;
    getUserToken : any;
    myaccount = false;
    public logoutshow;
    ErrorUser = false;
    public productCurrency: any = {value: "USD"};
    
    constructor( private userService: UserService) {
        this.Init();
        this.ShowUserRole = [];   
    }
    ngAfterViewInit() {
        if(jQuery(window).width() > 767)
        {
            // change functionality for smaller screens
            // jQuery("#navbar-menu").attr("aria-expanded","true");
            jQuery("#navbar-menu1").addClass("in");
            jQuery("#left-menu").addClass("col-sm-3");
            // jQuery("#navbar-menu-btn").attr("aria-expanded","true");
        } 
    }
    public Init() {
        this.userService.showProfile().then(
            response => {
                this.ShowUserRole = JSON.parse(response).Items[0].Roles;
                this.UserType = JSON.parse(response).Items[0].UserType;
            },
            error => this.errorMessage = <any>error);
       
    }
}