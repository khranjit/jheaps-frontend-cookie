import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';


const user: Routes = 
[
    {path: '', redirectTo: 'profile', pathMatch: 'full'},
    {path: 'profile', loadChildren:'./_profile/profile.module#ProfileModule'},
    {path: 'purchases', loadChildren:'./_purchases/purchases.module#PurchasesModule'},
    {path: 'favourites', loadChildren:'./_favourites/favourites.module#FavouritesModule'},
    {path: 'messages', loadChildren:'./_messages/messages.module#MessagesModule'},
    {path: 'reviews', loadChildren:'./_reviews/reviews.module#ReviewsModule'},
    {path: 'sell-on-jheaps', loadChildren:'./_sell-on-jheaps/sell-on-jheaps.module#SellOnJheapsModule'}
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(user)
  ],
  declarations: [
  ]
})
export class UserModule { }
