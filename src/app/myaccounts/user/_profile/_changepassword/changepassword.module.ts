import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChangePasswordComponent } from './changepassword.component';
import { SharedModule } from '../../../../shared/shared.module';
import { MatInputModule } from '@angular/material';

  
const changepassword: Routes = [
    {path: '',component:ChangePasswordComponent}
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(changepassword),
    MatInputModule
  ],
  declarations: [
    ChangePasswordComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ChangePasswordModule { }