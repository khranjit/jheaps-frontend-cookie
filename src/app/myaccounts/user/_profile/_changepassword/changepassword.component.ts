import { Component, Inject} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../../services/user.service';
import { LoginService } from '../../../../services/login.service';
import { NotifyService } from '../../../../services/notify.service';

@Component({ 
  selector: 'changepassword',
  providers:[UserService,NotifyService,LoginService],
  templateUrl: './changepassword.component.html'
})

export class ChangePasswordComponent{ 
   public loginModel: ViewModels.ILoginViewModel;  
  public passwordModel: ViewModels.IChangePasswordRequestViewModel;
    loginInfo:any;
    errorMessage:any;
    ShowUser: any;
    ShowUserProfileDetails: any;
    SocialUser:boolean;

  constructor(private route: ActivatedRoute, private router: Router, private userService: UserService,private loginService: LoginService,private snack:NotifyService) {
    this.Init();
    this.ShowProfile();
  }
  public Init() {
      
      this.loginModel = <ViewModels.ILoginViewModel>{};
      this.passwordModel = <ViewModels.IChangePasswordRequestViewModel>{};
      
  }
     login() {
        this.loginService.login(this.loginModel)
            .then(
            loginInfo => {
                console.log(loginInfo)
                this.loginInfo = JSON.parse(loginInfo);
                
                    console.log(this.loginInfo.Token)
        
            }, error => {
                this.errorMessage = <any>error
         this.snack.ErrorSnack( "Check user name and password");});
    }
    public ShowProfile() {
      this.userService.showProfile().then(
          response => {
              this.ShowUserProfileDetails = JSON.parse(response);
          //    console.log("showProfile :", this.ShowUserProfileDetails);
              this.ShowUser = this.ShowUserProfileDetails.Items[0];
              if(!this.ShowUser.Password){
                if(this.ShowUser.FacebookId){
                  this.SocialUser=true;
                }
                else if(this.ShowUser.GoogleId){
                  this.SocialUser=true;
                }
                else{
                  this.SocialUser=false;
                }
              }
              else{
                this.SocialUser=false;
              }
            //  console.log("User Details", this.ShowUser);

              //this.profileModel=this.ShowUserProfile;
              //console.log("Profile Model : ",this.profileModel)
              //               
          },
          error => { this.snack.ErrorSnack( 'You are unauthorized !!..'); });
  }
  editPassword(){
       if(this.passwordModel.NewPassword==this.passwordModel.ConfirmNewPassword)
        {
      this.userService.changePassword(this.passwordModel).then(
            response => {
              this.snack.SuccessSnack('You have successfully changed your password');
              this.ShowProfile();
              },
            error => {
				this.snack.ErrorSnack('Check your existing password');
                });
           }
       else{
          
            this.snack.ErrorSnack( "Password Not Matched");
            }
}
}