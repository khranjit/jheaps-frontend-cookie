import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { SharedModule } from '../../../shared/shared.module';
import { ProfileComponent } from './profile.component';
import { Routes, RouterModule } from '@angular/router';
  
  
const profile: Routes = [
  {path:'change',component:ProfileComponent, children:[
    {path: '', redirectTo: 'profile_edit', pathMatch: 'full'},
    {path: 'profile_edit' ,loadChildren:'./_profileedit/profile-edit.module#ProfileEditModule'},
    {path: 'change_password', loadChildren:'./_changepassword/changepassword.module#ChangePasswordModule'},
    {path: 'shipping_address', loadChildren:'./_shipping-address/shipping-address.module#ShippingAddressModule'},
    {path: 'add_address', loadChildren:'./_shipping-address/_add-shipping-address/add-shipping-address.module#AddshippingAddressModule'}
  ]}
]


@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(profile)
  ],
  declarations: [
    ProfileComponent
  ]
})
export class ProfileModule { }