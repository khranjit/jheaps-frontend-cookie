import { Component } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { UserService } from '../../../../../services/user.service';
import { RegisterService } from '../../../../../services/register.service';
import { ShippingProfileService } from '../../../../../services/shipping-profile.service';
import { NotifyService } from '../../../../../services/notify.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'addshippingaddress',
  providers: [UserService, ShippingProfileService, NotifyService, RegisterService],
  templateUrl: './add-shipping-address.component.html'
})

export class AddShippingAddressComponent {

  public shippingAddressModel: ViewModels.IShippingAddressViewModel;
  public addressModel: ViewModels.IAddressViewModel;
  public Address: any;
  public AddressId: any;
  newbutton = true;
  editbutton = false;
  errorMessage: any;
  ShowUserProfileDetails: any;
  ShowUserProfile: any;
  ShowUserAddress: any;
  ShowUser: any;
  ShowUserProfileAddress: any;
  countriesArray: any;
  statesArray: any;
  citiesArray: any;
  countryName: any;
  countryCode: any;
  AddressJson: any;
  AddAddressflag = false;

  value: any;
  default: any;

  constructor(private userService: UserService, private registerService: RegisterService, private route: ActivatedRoute, private router: Router, private shippingProfileService: ShippingProfileService, private snack: NotifyService, private loadingservice: NgxSpinnerService) {
    this.shippingAddressModel = <ViewModels.IShippingAddressViewModel>{};
    this.addressModel = <ViewModels.IAddressViewModel>{};
    this.countriesArray = [];
    this.statesArray = [];
    this.countryName = "";
    this.AddressId = {};
    this.countryCode = {};
    this.AddressJson = {};
    this.Init();
  }

  public Init() {
    this.Address = <any>{};
    this.route.queryParams.subscribe(
      params => {
        console.log(params)
        this.AddressId = params['Id'];
        console.log("Query", this.AddressId)
        if (this.AddressId == null) {
          this.editbutton = false;
          this.newbutton = true;
        }
        else {
          this.editbutton = true;
          this.newbutton = false;
        }
        this.shippingProfileService.showShippingAddress().then(
          response => {
            console.log("Shipping : ", response);
            this.ShowUserProfile = JSON.parse(response);
            console.log(this.ShowUserProfile);
            this.ShowUserAddress = this.ShowUserProfile.ShippingAddress;
            console.log("Total Address Length", this.ShowUserAddress.length);
            // var userid=this.ShowUserProfile.User;
            // this.userService.getUserDetailsById(userid).then(
            //   result=>
            //   {
            //     var res=JSON.parse(result);
            //     this.addressModel.FirstName=res.FirstName;
            //     this.addressModel.LastName=res.LastName;
            //     this.addressModel.Phone=res.Mobile;
            var flag = false;
            for (var i = 0; i < this.ShowUserAddress.length; i++) {
              if (this.AddressId == this.ShowUserAddress[i]._id) {
                console.log("Got ID Value")
                this.addressModel = this.ShowUserAddress[i];
                this.AddAddressflag = true;
                // this.stateChanged(this.addressModel.State)
                break;
              }
            }

            console.log("Address Model : ", this.addressModel)
            this.countryName = this.addressModel.Country;

            //   }
            // );

          });

      });
    this.registerService.getCountries().then(
      countriesInfo => {
        this.countriesArray = JSON.parse(countriesInfo);
        console.log(this.countriesArray);
        console.log("--------" + this.countriesArray.length);
        if (this.editbutton) {
          // for(var i=0;i<this.countriesArray.length;i++){
          //     if(this.countriesArray[i].name==this.countryName){
          //         // this.countryCode={"CountryId":this.countriesArray[i].id}
          //         break;
          //         }   
          // }
          this.countryCode = { "CountryId": 101 }

          this.registerService.getStates(this.countryCode).then(
            statesInfo => {
              this.statesArray = JSON.parse(statesInfo);
              console.log(this.statesArray);
            }, error => this.errorMessage = <any>error);
        }

      }, error => this.errorMessage = <any>error);

    // this.shippingProfileModel = <ViewModels.IShippingProfileViewModel>{};
    this.userService.showProfile().then(
      response => {
        this.ShowUserProfileDetails = JSON.parse(response);
        this.ShowUser = this.ShowUserProfileDetails.Items[0];
        this.ShowUserProfile = this.ShowUser.Profile;
        if (this.AddAddressflag == false) {
          this.addressModel.FirstName = this.ShowUser.Profile.FirstName;
          this.addressModel.LastName = this.ShowUser.Profile.LastName;
          this.addressModel.Phone = this.ShowUser.Mobile;
        }
        this.ShowUserAddress = this.ShowUserProfile.Address;

      },
      error => this.errorMessage = <any>error);

    var val = { "CountryId": 101 };
    this.registerService.getStates(val).then(
      statesInfo => {
        this.statesArray = JSON.parse(statesInfo);
      }, error => this.errorMessage = <any>error);
  }

  CheckPinCode() {
    var model = { country: this.addressModel.Country, State: this.addressModel.State, pincode: this.addressModel.Zip };
    return this.registerService.checkPincode(model).then(result => {
      var pincode = JSON.parse(result);
      return result;
    });
  }

  addAddress() {
    var numberAlone = /^\d+$/;

    this.addressModel.Country = "India";

    if (this.addressModel.FirstName) {
      if (this.addressModel.LastName) {
        if (this.addressModel.Line1) {
          if (!this.addressModel.Line1.toString().match(numberAlone)) {
            if (this.addressModel.Line2) {
              if (!this.addressModel.Line2.toString().match(numberAlone)) {
                if (this.addressModel.State) {
                  if (this.addressModel.City) {
                    if (this.addressModel.Zip) {
                      if (this.addressModel.Phone) {
                        this.shippingProfileService.addShippingAddress(this.addressModel).then(
                          response => {
                            var result = JSON.parse(response);
                            if (result.hasOwnProperty('flag')) {
                              this.snack.ErrorSnack(result.msg);
                            } else {
                              console.log("add response :", response)
                              this.snack.SuccessSnack('Added Successfully');
                              this.router.navigate(['myaccounts/user/profile/change/shipping_address']);
                            }

                          },
                          error => error => this.errorMessage = <any>error);

                      } else {
                        this.snack.ErrorSnack('Please fill phone number');
                      }
                    } else {
                      this.snack.ErrorSnack('Please fill postal code');
                    }
                  } else {
                    this.snack.ErrorSnack('Please fill city');
                  }
                } else {
                  this.snack.ErrorSnack('Please fill state');
                }
              } else {
                this.snack.ErrorSnack('Please check address format line 2');
              }
            } else {
              this.snack.ErrorSnack('Please fill address line2');
            }
          } else {
            this.snack.ErrorSnack('Please check address format');
          }
        } else {
          this.snack.ErrorSnack('Please fill address line1');
        }
      } else {
        this.snack.ErrorSnack('Please fill last name');
      }
    } else {
      this.snack.ErrorSnack('Please fill first name');
    }

  }

  editAddress() {
    var numberAlone = /^\d+$/;

    if (this.addressModel.FirstName) {
      if (this.addressModel.LastName) {
        if (this.addressModel.Line1) {
          if (!this.addressModel.Line1.toString().match(numberAlone)) {
            if (this.addressModel.Line2) {
              if (!this.addressModel.Line2.toString().match(numberAlone)) {
                if (this.addressModel.State) {
                  if (this.addressModel.City) {
                    if (this.addressModel.Zip) {
                      if (this.addressModel.Phone) {
                        this.shippingProfileService.putShippingAddress(this.addressModel).then(response => {
                          this.loadingservice.hide();
                          var result = JSON.parse(response);
                          if (result.hasOwnProperty('flag')) {
                            this.snack.ErrorSnack(result.msg);
                          } else {
                            this.snack.SuccessSnack('Edited Successfully');
                            this.router.navigate(['/myaccounts/user/profile/change/shipping_address']);
                          }

                        });
                      } else {
                        this.snack.ErrorSnack('Please fill phone number');
                      }
                    } else {
                      this.snack.ErrorSnack('Please fill postal code');
                    }
                  } else {
                    this.snack.ErrorSnack('Please fill city');
                  }
                } else {
                  this.snack.ErrorSnack('Please fill state');
                }
              } else {
                this.snack.ErrorSnack('Please check address format line2');
              }
            } else {
              this.snack.ErrorSnack('Please fill address line2');
            }
          } else {
            this.snack.ErrorSnack('Please check address format');
          }
        } else {
          this.snack.ErrorSnack('Please fill address line1');
        }
      } else {
        this.snack.ErrorSnack('Please fill last name');
      }
    } else {
      this.snack.ErrorSnack('Please fill first name');
    }

  }

  onChangeCountry(value) {
    var countryid = 0;
    for (var i = 0; i < this.countriesArray.length; i++) {
      if (this.countriesArray[i].name == value) {
        countryid = this.countriesArray[i].id;
      }
    }
    var val = { "CountryId": countryid };
    this.registerService.getStates(val).then(
      statesInfo => {
        this.statesArray = JSON.parse(statesInfo);
      }, error => this.errorMessage = <any>error);
  }

  goBack() {
    this.router.navigate(['/myaccounts/user/profile/change/shipping_address']);
  }

}