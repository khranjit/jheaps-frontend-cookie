import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../../shared/shared.module';
import {NgPipesModule} from 'ngx-pipes';
import { Routes, RouterModule } from '@angular/router';
import { AddShippingAddressComponent} from './add-shipping-address.component'
import { MatInputModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';
  
const addaddress: Routes = [
    {path: '', component:AddShippingAddressComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(addaddress),
    MatInputModule,
    MatSelectModule
  ],
  declarations: [
    AddShippingAddressComponent
  ]
})
export class AddshippingAddressModule { }