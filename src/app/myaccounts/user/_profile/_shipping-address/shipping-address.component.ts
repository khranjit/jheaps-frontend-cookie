import { Component} from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ShippingProfileService } from '../../../../services/shipping-profile.service';
import { UserService } from '../../../../services/user.service';
import { NotifyService } from '../../../../services/notify.service';
declare var jQuery: any;

@Component({
    
    selector: 'shippingaddress',
    providers: [UserService, ShippingProfileService, NotifyService],
    templateUrl: './shipping-address.component.html'
})

export class ShippingAddressComponent {

    public shippingProfileModel: ViewModels.IShippingProfileViewModel;
    ShippingProfileList: any;
    ShippingProfiles: any;
    errorMessage: any;
    deletedShippingProfile: any;
    ShowUserProfile: any;
    ShowUserAddress: any;
    ShowUser: any;
    ShowUserProfileAddress: any;
    ViewUserProfileDetails: any;
    ViewUserProfile: any;
    ViewUserAddress: any;
    ViewUser: any;
    ViewUserProfileAddress: any;
    selectedAddress: boolean;
    SetDefault: boolean;
    DefaultAddress: boolean;
    DefaultIndex: number;
    ProfileId : any;
    changeProfilePrimary:any;
    ProfileAddress: any;
    AddressSwapJson: any;

    constructor(private userService: UserService, private route: ActivatedRoute, private router: Router, private shippingProfileService: ShippingProfileService, private snack: NotifyService) {
        this.ProfileAddress = {};
        this.AddressSwapJson = [];
        this.selectedAddress = true;
        this.Init();
        //   this.ShowShippingAddress();
    }

    public Init() {

        this.shippingProfileModel = <ViewModels.IShippingProfileViewModel>{};
        this.ViewUserProfileDetails = <any>{};
        this.ViewUser = <any>{};
        this.ViewUserProfile = <any>{};
        this.ViewUserAddress = <any>{};
        this.ViewUserProfileAddress = [];
        this.AddressSwapJson = [];
        this.shippingProfileService.showShippingAddress().then(
            response => {
                console.log(response);
                this.ShowUserProfile = JSON.parse(response);
                console.log(this.ShowUserProfile);
                this.ShowUserAddress = this.ShowUserProfile.ShippingAddress;
                console.log("Total Address Length", this.ShowUserAddress.length);
                for (var i = 0; i < this.ShowUserAddress.length; i++) {
                    if (this.ShowUserAddress[i].IsPrimary == true) {
                        console.log("True Loop")
                        this.AddressSwapJson.splice(0, 0, this.ShowUserAddress[i]);
                    }
                    else {
                        console.log("False Loop")
                        this.AddressSwapJson.push(this.ShowUserAddress[i]);
                    }
                }
                console.log("True  : ", this.AddressSwapJson)
                this.userProfileData();
            },
            error => this.errorMessage = <any>error);
    }
    
    userProfileData() {
        this.userService.showProfile().then(
            response => {
                this.ViewUserProfileDetails = JSON.parse(response);
                this.ViewUser = this.ViewUserProfileDetails.Items[0];
                this.ViewUserProfile = this.ViewUser.Profile;
                // this.ViewUserAddress = this.ViewUserProfile.Address;
                // this.ProfileAddress["FirstName"] = this.ViewUserProfile.FirstName;
                // this.ProfileAddress["LastName"] = this.ViewUserProfile.LastName;
                // this.ProfileAddress["Line1"] = this.ViewUserAddress.Line1;
                // this.ProfileAddress["Line2"] = this.ViewUserAddress.Line2;
                // this.ProfileAddress["City"] = this.ViewUserAddress.City;
                // this.ProfileAddress["State"] = this.ViewUserAddress.State;
                // this.ProfileAddress["Country"] = this.ViewUserAddress.Country;
                // this.ProfileAddress["Zip"] = this.ViewUserAddress.Zip;
                // this.ProfileAddress["Phone"] = "8234124129";
                // this.ProfileAddress["IsPrimary"] = true;
                // this.ProfileAddress["ProfileAddress"]=false;
                this.ProfileId=this.ViewUser._id
                if(this.AddressSwapJson.length!=0){
                    
                if (!this.AddressSwapJson[0].IsPrimary) {
                    // this.AddressSwapJson.splice(0, 0, this.ProfileAddress);
                }
                else {
                    this.ProfileAddress["IsPrimary"] = false;
                    // this.AddressSwapJson.push(this.ProfileAddress);
                }
                }
                else {
                    this.ProfileAddress["IsPrimary"] = true;
                    // this.AddressSwapJson.push(this.ProfileAddress);
                }
                console.log("Swapped Json", this.AddressSwapJson);
         }, error => this.errorMessage = <any>error);
    }
    
    public deleteShippingProfile(shippingProfileModel) {
        this.deletedShippingProfile = shippingProfileModel;
        jQuery("#confirmDeleteModal").modal('show');
    }
    
    selectAddress() {
        this.selectedAddress = false;
    }

    delete(deleteAddress) {
        console.log("delete address id :");
        this.shippingProfileService.removeShippingAddress(deleteAddress).then(
            response => {
                console.log("Success")
                this.Init();
                this.snack.SuccessSnack('Address has been deleted Successfully!!');

            },
            error => {
                this.snack.ErrorSnack( 'There is an error in deleting your Shipping Address');
            }
        );
    }
    addNew() {
        this.router.navigate(['/myaccounts/user/profile/change/shipping_address/add']);
    }
    
    editAddress(Id) {
        console.log("Id :",Id)
        let navimodel: NavigationExtras = {
            queryParams: { 'Id': Id }
        }
        console.log("navimodel 1:",navimodel)
        this.router.navigate(['/myaccounts/user/profile/change/shipping_address/add'], navimodel);
    }
    
    setDefault(temp,id) {
        if(temp==undefined){
            this.changeProfilePrimary={"ProfileAddress" : "Primary"};
            this.shippingProfileService.changeProfileAsPrimary(this.changeProfilePrimary).then(
                response => {
                    this.Init();
                });
        }
        else{
            console.log("Contain Value")
             this.shippingProfileService.changePrimaryAddress(temp).then(
                response => {
                     this.Init(); 
                });
        }
    }

}