import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import {NgPipesModule} from 'ngx-pipes';
import { Routes, RouterModule } from '@angular/router';
import { ShippingAddressComponent} from './shipping-address.component'

  
const shippingaddress: Routes = [
    {path: '', component: ShippingAddressComponent},
    {path: 'add', loadChildren:'./_add-shipping-address/add-shipping-address.module#AddshippingAddressModule'},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(shippingaddress)
  ],
  declarations: [
    ShippingAddressComponent
  ]
})
export class ShippingAddressModule { }