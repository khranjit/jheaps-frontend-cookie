import { Component, Inject} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../../services/user.service';
import { RegisterService } from '../../../../services/register.service';
import { ToasterPopService } from '../../../../services/toasterpop.service';
import { UtilService } from '../../../../services/util.service';
import * as Q from 'q';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifyService } from '../../../../services/notify.service';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { ViewEncapsulation } from '@angular/core';


declare var jQuery: any;
declare var Cropper: any;
declare var Croppie: any;


interface HTMLInputEvent extends Event {
    target: HTMLInputElement & EventTarget;
}

@Component({
    
    selector: 'profileedit',
    providers: [UserService, ToasterPopService, RegisterService, UtilService],
    templateUrl: './profile-edit.component.html',
    encapsulation: ViewEncapsulation.None
})



export class ProfileEditComponent  {
  
    public profile: ViewModels.IUserViewModel;
    public profileModel: ViewModels.IProfileViewModel;
    // public profileModelAddress: ViewModels.IAddressViewModel;
    ShowUserProfileDetails: any;
    // public VerificationModel: any;
    ShowUserProfile: any;
    ShowUserAddress=<any>{};
    ShowUser: any;
    countryName: any;
    countryId: any;
    errorMessage: any;
    countriesArray: any;
    statesArray: any;
    tempImageBase64: any;
    selectedImageForCropping: any;
    public imageTransitions = <any>{};
    public cropper: any;
    public croppie: any;
    public zoomRatio = 0;
    public editProfileImage: boolean;
    public imageFlag: string;
    public editProfile: boolean=true;
    public firstUser: boolean=false;
    readonlyVar: boolean;
    loader = false;
    zipcodes : any;
    phoneValid :any;
    Mobile : number;
    OldMobile : number;
    UserEmail : string;
    EditMobile = false;
    EditEmail = false;
    getPassword = false;
    OTPVerify = false;
    OTP:number;
    numberExist = true;
    OTPWrong= false;
    UserPassword : string;
    OldEmail : string;
    getEmailPassword = false;
    EmailOTPVerify = false;
    EmailOTP:number;
    EmailExist = true;
    EmailOTPWrong= false;
    EmailPassword : string;
   
    genderValue : string;
    zipcodelength : boolean;
    phoneLength : boolean ;
    
    constructor(private route: ActivatedRoute, private loaderComponent:NgxSpinnerService, private router: Router, private userService: UserService, private toasterPopService: ToasterPopService,
        private utilService: UtilService, private registerService: RegisterService,private snack:NotifyService) {
        this.statesArray = [];
        this.countriesArray = [];
        this.countryId = {};
        this.readonlyVar = true;
        this.Init();
        this.ShowProfile();
        this.zipcodelength =false;
        this.phoneLength = false;
    }
    public Init() {
        //  jQuery("input[readonly]").attr("readonly",true);
        //console.log("Testing",localStorage.getItem('userToken'));
        this.profile = <ViewModels.IUserViewModel>{}
        this.profileModel = <ViewModels.IProfileViewModel>{};
        // this.profileModelAddress = <ViewModels.IAddressViewModel>{};
        this.ShowUserProfileDetails = <any>{};
        this.ShowUser = <any>{};
        this.ShowUserProfile = <any>{};
        this.ShowUserAddress = <any>{};
        this.editProfile = true;
        //console.log("Local Storage:\n", localStorage.getItem('userToken'))
    }
     validZip(){
         if(this.ShowUserAddress.Zip)
         {
         this.zipcodes=this.ShowUserAddress.Zip.toString();
         //console.log(" zipCode:"+this.ShowUserAddress.Zip);
        //console.log(" zipCode:"+this.zipcodes);
        //console.log(" zipCode:"+this.zipcodes.length);
        if(this.zipcodes.length >6 || this.zipcodes.length < 6 ){
            this.zipcodelength = true;
        }
        else 
            this.zipcodelength = false;
        //console.log(" zipCodelength:"+this.zipcodelength);
    }
    else{
        this.zipcodelength = false;
        //console.log('else'+this.zipcodelength);
    }
    }

   
    
    validPhone(){
        if(this.Mobile){
        this.phoneValid=this.Mobile.toString();
       //console.log(" phonenumber:"+this.ShowUserAddress.Phone);
     //  console.log(" phoneValid:"+this.phoneValid);
       //console.log(" numberLength:"+this.phoneValid.length);
       if(this.phoneValid.length >12 || this.phoneValid.length < 10 && this.phoneValid.length != null){
        this.phoneLength = true;
       // console.log("phone  Boolean"+this.phoneLength);

       }
       else 
       {
           this.phoneLength = false;
      // console.log(" number:"+this.phoneValid.length);
       //console.log("phone  Boolean"+this.phoneLength);
    }
    }
    else{
        this.phoneLength = false;
        //console.log("phone else"+this.phoneLength);
    }
    }

    public ShowProfile() {
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfileDetails = JSON.parse(response);
            //    console.log("showProfile :", this.ShowUserProfileDetails);
                this.ShowUser = this.ShowUserProfileDetails.Items[0];
           //     console.log("User Details", this.ShowUser);
                if(!this.ShowUser.Profile.hasOwnProperty('FirstName')){
                    this.editProfile = false;
                    this.readonlyVar = false;
                    this.firstUser = true;
                }
                this.Mobile = this.ShowUser.Mobile;
                this.UserEmail = this.ShowUser.Email;
                if(this.ShowUser.Gender == -1)
                    this.genderValue = "Others";
                else if(this.ShowUser.Gender == 1)
                    this.genderValue = "Female";
                else
                    this.genderValue = "Male";
         //       console.log("Gender" , this.genderValue );
                this.ShowUserProfile = this.ShowUser.Profile;
       //         console.log("ShowUserProfile", this.ShowUserProfile);
                this.ShowUserAddress = this.ShowUserProfile.Address;
     //           console.log("ShowUserAddress", this.ShowUserAddress);
                // this.countryName = this.ShowUserAddress.Country;
                this.registerService.getCountries()
                    .then(
                    countriesInfo => {
                        this.countriesArray = JSON.parse(countriesInfo);
      //                  console.log(this.countriesArray);
        //                console.log("--------" + this.countriesArray.length);
                        for (var i = 0; i < this.countriesArray.length; i++) {
                            if (this.countriesArray[i].name == this.countryName) {
                                this.countryId = { "CountryId": this.countriesArray[i].id }
          //                      console.log(this.countryId);
                                break;
                            }
                        }
                        this.registerService.getStates(this.countryId).then(
                            statesInfo => {
                                this.statesArray = JSON.parse(statesInfo);
            //                    console.log(this.statesArray);
                            }, error => this.errorMessage = <any>error);
                    }, error => this.errorMessage = <any>error);

                //this.profileModel=this.ShowUserProfile;
                //console.log("Profile Model : ",this.profileModel)
                //               
            },
            error => { this.snack.ErrorSnack( 'You are unauthorized !!..'); });
    }
    
    editProfilefunc() {
    ///    console.log(this.readonlyVar);
   //     console.log("clicked");
        this.readonlyVar = false;
     //   console.log(this.readonlyVar);
    }

    ValidateCouriers(zip, paymentMode):Q.Promise<any>{
        var defer = Q.defer();
        this.loaderComponent.show();
        this.utilService.ValidateCouriers(zip,paymentMode).then(res =>{
            this.loaderComponent.hide();
            var response = JSON.parse(res);
            if(response.status == true)
            defer.resolve({status:'success'});
            else
            defer.resolve({status:'failure'})
        }, err=>{
            this.loaderComponent.hide();
            this.snack.ErrorSnack("Something went wrong, please try again after some time");
        })

        return defer.promise; 
    }
    
    saveProfile() {

        if(this.ShowUser.IsApprovedSeller == true && this.ShowUser.Roles[0]=='seller')
        {
                    this.readonlyVar = true;
                    // jQuery("input[readonly]").attr("readonly",true);
                    // this.profileModelAddress["Line1"] = this.ShowUserAddress.Line1;
                    // this.profileModelAddress["Line2"] = this.ShowUserAddress.Line2;
                    // this.profileModelAddress["City"] = this.ShowUserAddress.City;
                    // this.profileModelAddress["State"] = this.ShowUserAddress.State;
                    // this.profileModelAddress["Country"] = this.ShowUserAddress.Country;
                    // this.profileModelAddress["Zip"] = this.ShowUserAddress.Zip;
                    // this.profileModelAddress["Phone"] = this.ShowUserAddress.Phone;
                    this.profileModel["FirstName"] = this.ShowUserProfile.FirstName;
                    this.profileModel["LastName"] = this.ShowUserProfile.LastName;
                    this.profileModel["DisplayName"] = this.ShowUserProfile.DisplayName;
                    this.profileModel["DisplayPicture"] = this.ShowUserProfile.DisplayPicture;
                    this.profile["Gender"] = this.ShowUser.Gender;
                    // this.profileModel["Address"] = this.profileModelAddress;
                    this.profile["Profile"] = this.profileModel;
                //    console.log(this.profileModelAddress)
                 //   console.log(this.profile);
                    this.userService.updateProfile(this.profile).then(
                        response => {
                 //           console.log(response);
                            this.ShowProfile();
                            jQuery("#profile_image").attr("src",this.profileModel["DisplayPicture"]);
                            jQuery("#displayFirstName").html(this.profileModel['FirstName'])
                            //jQuery("input[readonly]").attr("readonly", true);
                            this.snack.SuccessSnack("You have successfully updated your Profile");
                            this.editProfile=true;
                        },
                        error => {
                            this.snack.ErrorSnack('There is an error in your Profile');
                            this.editProfile=false;
                        }
                    );   
        }

        else{
            this.readonlyVar = true;
            // jQuery("input[readonly]").attr("readonly",true);
            // this.profileModelAddress["Line1"] = this.ShowUserAddress.Line1;
            // this.profileModelAddress["Line2"] = this.ShowUserAddress.Line2;
            // this.profileModelAddress["City"] = this.ShowUserAddress.City;
            // this.profileModelAddress["State"] = this.ShowUserAddress.State;
            // this.profileModelAddress["Country"] = this.ShowUserAddress.Country;
            // this.profileModelAddress["Zip"] = this.ShowUserAddress.Zip;
            // this.profileModelAddress["Phone"] = this.ShowUserAddress.Phone;
            this.profileModel["FirstName"] = this.ShowUserProfile.FirstName;
            this.profileModel["LastName"] = this.ShowUserProfile.LastName;
            this.profileModel["DisplayName"] = this.ShowUserProfile.DisplayName;
            this.profileModel["DisplayPicture"] = this.ShowUserProfile.DisplayPicture;
            this.profile["Gender"] = this.ShowUser.Gender;
            // this.profileModel["Address"] = this.profileModelAddress;
            this.profile["Profile"] = this.profileModel;
        //    console.log(this.profileModelAddress)
         //   console.log(this.profile);
            this.userService.updateProfile(this.profile).then(
                response => {
         //           console.log(response);
                    this.ShowProfile();
                    this.editProfile=true;
                    jQuery("#profile_image").attr("src",this.profileModel["DisplayPicture"]);
                    jQuery("#displayFirstName").html(this.profileModel["FirstName"])
                    //jQuery("input[readonly]").attr("readonly", true);
                    this.snack.SuccessSnack("You have successfully updated your Profile");

                },
                error => {
                    this.editProfile=false;
                    this.snack.ErrorSnack( 'There is an error in your Profile');
                }
            );
        }
        
    }

    triggerFileUpload() {
        jQuery('#profileUploadImage').trigger('click');
    }


    onChangeCountry(value) {
        var countryid = 0;
        for (var i = 0; i < this.countriesArray.length; i++) {
            if (this.countriesArray[i].name == value) {
                countryid = this.countriesArray[i].id;
            }
        }
        var val = { "CountryId": countryid };
     //   console.log(val);
        this.registerService.getStates(val).then(
            statesInfo => {
                this.statesArray = JSON.parse(statesInfo);
     //           console.log(this.statesArray);
            }, error => this.errorMessage = <any>error);
    }

    ChangeListener(e,self) {
        this.loader = true;
        this.loaderComponent.show();
     //   console.log("change listenser called")
        // let files = [].slice.call($event.target.files);
        const width = 500;
        const height = 500;
        const fileName = e.target.files[0].name;
        var size = e.target.files[0].size;
        if(size < 10485760){
            const reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);
            reader.onload = e => {
                var event:any = e;
                const img = new Image();
                img.src = event.target.result;
                    img.onload = () => {
                    if(img.width > 400 && img.height >400){
                        const elem = document.createElement('canvas');
                        elem.width = width;
                        elem.height = height;
                        const ctx = elem.getContext('2d');
                        // img.width and img.height will contain the original dimensions
                        ctx.drawImage(img, 0, 0, width, height);
                        ctx.canvas.toBlob((blob) => {
                            const file = new File([blob], fileName, {
                                type: 'image/jpeg',
                                lastModified: Date.now()
                            });
                            self.readthis(file);
                        }, 'image/jpeg', 0.5);
                    }else{
                        this.loaderComponent.hide();
                        this.loader = false;
                        this.snack.ErrorSnack("Image size must be minimum 400 X 400")
                    }
                    
                    },
                    reader.onerror = error => console.log(error);
               
               
            };
        }else{
            this.snack.ErrorSnack("Kindly upload an image less than 10 MB")

        }
        

    }

    public readthis(inputValue: any): any {
        var file: File = inputValue;
        let fileName = file.name.split(".")[0];
    //    console.log('fileName', file.name.split(".")[1]);
        this.utilService.convertToBase64Change(file, 186, 172 , file.size);
        let count = 0;
        this.utilService.invokeEvent.subscribe(value => {
            if (count == 0) {
                this.utilService.uploadBase64AndGetUrl(fileName, value).then(
                    response => {
      //                  console.log("Extract Imagedata from uploadBase64AndGetUrl", JSON.parse(response));
                        let obj = { "Name": fileName, "URL": JSON.parse(response).ImageData };
                        // this.attachments.push(obj);
                        // console.log(" this.attachments ",this.attachments);
                        this.profileModel["DisplayPicture"] = JSON.parse(response).ImageData;
                        this.ShowUserProfile["DisplayPicture"] = JSON.parse(response).ImageData;
                       //    this.tempImageBase64.push(value);
                        this.loader = false;
                        this.loaderComponent.hide();
                        this.cropImage(value);
                        
                    },
                    error => {
                        this.loader = false;
                        this.loaderComponent.hide();
                        this.snack.ErrorSnack("Problem in uploading the image. Please try again");
                    });
            }
            count++;
        });
    }
    
    cropImage(selectedImage) {

        let self = this;
        this.selectedImageForCropping = selectedImage;
    //    console.log("Croppie");
        if (this.croppie) {
            this.croppie.destroy();
        }
        jQuery("#editImageModal").modal("show");
        setTimeout(function() {
            let image = document.getElementById('EditableImage');
            //console.log("image ", image);
            self.croppie = new Croppie(image, {
                viewport: {
                    width: 200,
                    height: 200
                },
                boundary: { width: 800, height: 500 },
                enableOrientation: true

            });
            self.croppie.bind({
                url: selectedImage,
                zoom: 0
            }).then(function(blob) {
                // do something with cropped blob
            });
        }, 1000);
      //  this.saveCroppedImage();
    }
    
    cropperImageReset(){
        this.cropper.reset(); 
    }
    cropperImageZoomIn(){
        let croppieZoom = this.croppie.get();
        this.zoomRatio = croppieZoom.zoom;
   //     console.log(croppieZoom.zoom, this.zoomRatio)
        if(croppieZoom.zoom >= this.zoomRatio){
            this.zoomRatio = this.zoomRatio + 0.1;
      //      console.log(this.zoomRatio);
     //       console.log();
            this.croppie.setZoom(this.zoomRatio);
        }
    }
    cropperImageZoomOut(){
        let croppieZoom = this.croppie.get();
        this.zoomRatio = croppieZoom.zoom;
        if(this.zoomRatio > 0){
            this.zoomRatio = this.zoomRatio - 0.1;
      //      console.log(this.zoomRatio);
            this.croppie.setZoom(this.zoomRatio);  
        }
    }
    cropperImageRotateLeft(){
        this.croppie.rotate(-90);        
    }
    cropperImageRotateRight(){
        this.croppie.rotate(90);        
    }
    numberChange(){
        this.getPassword=false;
        this.EditMobile =true;
        this.OldMobile = this.Mobile;
    }
    sendOTP(){
        var numb= this.Mobile.toString().length;
        if(this.OldMobile == this.Mobile){
            this.snack.ErrorSnack('There is no change in mobile number');
            
            this.EditMobile =false;
        }else if(numb == 10 && numb != null){
            this.EditMobile =false;
            this.OTPVerify = false;
            this.OTPWrong = false;
            this.OTP = null;
            this.UserPassword = "";
            this.registerService.SendOTP(this.Mobile).then(data=>{
                var dat = JSON.parse(data);
                if(dat.mes){
                    this.snack.SuccessSnack('OTP has been sent to your mobile');
                    this.getPassword=true;
                }else{
                    this.numberExist=false;
                    this.snack.ErrorSnack('Mobile number already Exist ');

                }
            });

        }else{
            this.snack.ErrorSnack( 'Mobile number must be 10 digits ');
        }
    }
    OTPCheck(){
        this.OTPWrong = false;
        this.OTPVerify = false;
        if(this.OTP!=null)
        {
            var numb= this.OTP.toString().length;
        }
        else{
            var numb=0;  
        }
       
        var model = {
            Mobile:this.Mobile,
            OTP:this.OTP
        }
        if(numb == 6){
            this.registerService.OTPCheck(model).then(result=>{
                var res=JSON.parse(result);
                if(res.mes){
                    this.OTPWrong = false;
                    this.OTPVerify=true;
                }else{
                    this.OTPWrong = true;
                    // this.OTPVerify=true;
                }
                // this.getPassword=true;
            });
        }
    }

    SaveMobile(){
        var model = {
            Mobile:this.Mobile,
            Password:this.UserPassword
        }
        this.registerService.SaveMobile(model).then(result=>{
            var res=JSON.parse(result);
            if(res.mes){
                this.OTP = null;
                this.getPassword = false;
                this.UserPassword = "";
                this.EditMobile= false;
                this.OldMobile = this.Mobile;
                this.OTPVerify=false;
                this.OTPWrong= false;
                this.snack.SuccessSnack('Mobile number has been updated Successfully');
            }else{
                this.snack.ErrorSnack( 'Kindly check your account password');
            }
            // this.getPassword=true;
        });
    }

    Existreset(){
        this.numberExist = true;
    }

    EmailChange(){
        // var mailstyle=document.getElementById('Email1');
        // mailstyle.style.paddingRight="36%";
        // mailstyle.addClass('addEmailpadding')
        jQuery('#Email1').removeClass('removeEmailpadding').addClass('addEmailpadding');
     
        this.getEmailPassword=false;
    

        this.EditEmail=true;
        if(this.UserEmail != "") this.OldEmail = this.UserEmail;

    }
    sendEmailOTP(){
    
        if(this.OldEmail == "" || (this.OldEmail == this.UserEmail)){
            this.snack.ErrorSnack( 'There is no change in email id');
            // var mailstyle=document.getElementById('Email1');
            // mailstyle.style.paddingRight="31%";
            jQuery('#Email1').removeClass('addEmailpadding').addClass('removeEmailpadding');
            this.EditEmail =false;
        }else if(this.UserEmail != null){
            jQuery('#Email1').removeClass('addEmailpadding').addClass('removeEmailpadding');
            this.EditEmail =false;
            this.OTPVerify = false;
            this.OTPWrong = false;
            this.EmailOTP = null;
            this.EmailPassword = "";
            this.registerService.SendEmailOTP(this.UserEmail).then(data=>{
                var dat = JSON.parse(data);
                if(dat.mes){
                    this.snack.SuccessSnack( 'OTP has been sent to your Email');
                    this.getEmailPassword=true;
                }else{
                    jQuery('#Email1').removeClass('addEmailpadding').addClass('removeEmailpadding');
                    this.EmailExist=false;
                    this.snack.ErrorSnack( dat.message);

                }
            });

        }else{
            jQuery('#Email1').removeClass('addEmailpadding').addClass('removeEmailpadding');
            this.snack.ErrorSnack( 'Something Went wrong');
        }
    }
    EmailOTPCheck(){
        this.EmailOTPWrong = false;
        this.EmailOTPVerify = false;
        if(this.EmailOTP!=null)
        {
            var numb= this.EmailOTP.toString().length;
        }
        else{
            var numb=0; 
        }
       
        var model = {
            Email:this.UserEmail,
            OTP:this.EmailOTP
        }
        if(numb == 6){
            this.registerService.EmailOTPCheck(model).then(result=>{
                var res=JSON.parse(result);
                if(res.mes){
                    this.EmailOTPWrong = false;
                    this.EmailOTPVerify=true;
                }else{
                    this.EmailOTPWrong = true;
                    if(res.message != "Wrong OTP"){
                        this.snack.ErrorSnack( res.message);
                    }
                    // this.OTPVerify=true;
                }
                // this.getPassword=true;
            });
        }
    }

    SaveEmail(){
        var model = {
            Email:this.UserEmail,
            Password:this.EmailPassword,
            OTP: this.EmailOTP
        }
        this.registerService.SaveEmail(model).then(result=>{
            var res=JSON.parse(result);
            if(res.mes){
                this.EmailOTP = null;
                this.getEmailPassword = false;
                this.EmailPassword = "";
                this.EditEmail= false;
                this.OldEmail = this.UserEmail;
                this.EmailOTPVerify=false;
                this.EmailOTPWrong= false;
                this.snack.SuccessSnack( 'Email has been updated Successfully');
            }else{
            
                this.snack.ErrorSnack(res.message);
            }
            // this.getPassword=true;
        });
    }

    EmailExistreset(){
        this.EmailExist = true;
    }
    cancel(){
        this.editProfile=true;
        this.readonlyVar = true;
        this.ShowProfile();
    }
    
    
   saveCroppedImage(){
       jQuery("#editImageModal").modal("hide");
       let self = this;
       this.loader = true;
       var deleteImage = self.profileModel["DisplayPicture"];
       this.loaderComponent.show();
       this.ShowUserProfile.DisplayPicture = this.imageTransitions;
        jQuery("#editImageModal").modal("hide");
        this.croppie.result('base64', 'viewport', 'png', 1, false).then(function(response) {
            self.utilService.uploadBase64AndGetUrl(self.ShowUserProfile.DisplayPicture.Name, response).then(
                response => {
                  self.profileModel["DisplayPicture"]=JSON.parse(response).ImageData;
                  self.loader = false
                  
                  self.registerService.UpdateProfileImage(self.profileModel).then(result=>{
                  self.ShowUserProfile.DisplayPicture=JSON.parse(response).ImageData;
                  jQuery("#profile_image").attr("src",self.profileModel["DisplayPicture"]);
                  jQuery("#profile_image1").attr("src",self.profileModel["DisplayPicture"]); 
                  self.loaderComponent.hide();
                  var deletefile = {
                    AWSFileName : deleteImage
                  }
                  self.utilService.deleteAWSFile(deletefile);
                  self.snack.SuccessSnack("Edited image updated Successfully");                 
                 })
                },
                error => {
                  self.loader = false
                  self.loaderComponent.hide();
                  self.snack.ErrorSnack("Problem in uploading the image. Please try again");
                }
            );
        });
          
      
   }
   closeCroppedImage()
   {
    this.ShowProfile();
    jQuery("#editImageModal").modal("hide");
   }
    }