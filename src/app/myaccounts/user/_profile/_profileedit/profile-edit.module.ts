import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileEditComponent } from './profile-edit.component';
import { SharedModule } from '../../../../shared/shared.module';
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
  
const profileedit: Routes = [
    {path: '', component:ProfileEditComponent}
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(profileedit),
    MatFormFieldModule,
    MatInputModule
  ],
  declarations: [
    ProfileEditComponent
  ]
})
export class ProfileEditModule { }