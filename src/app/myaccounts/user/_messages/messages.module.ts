import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { MessagesComponent } from './messages.component';
import { Routes, RouterModule } from '@angular/router';
  
  
const messages: Routes = [
  {path:'read',component:MessagesComponent, children:[
    {path: '', redirectTo: 'inbox', pathMatch: 'full'},
    {path: 'inbox', loadChildren:'./_inbox/inbox.module#InboxModule'},
    {path: 'sent', loadChildren:'./_sent/sent.module#SentModule'},
    {path: 'trash', loadChildren:'./_trash/trash.module#TrashModule'}
  ]}
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(messages)
  ],
  declarations: [
      MessagesComponent
  ]
})

export class MessagesModule { }