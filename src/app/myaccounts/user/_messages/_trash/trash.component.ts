import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../../../services/messages.service'
import { NotifyService } from '../../../../services/notify.service'
import { UserService } from '../../../../services/user.service';
import { UtilService } from '../../../../services/util.service';

@Component({
    
    selector: 'trash',
    styleUrls:['trash.component.css'],
    providers: [MessageService, NotifyService,UserService, UtilService],
    templateUrl: './trash.component.html'
})
export class TrashComponent {
    public MessageRepliesModel: ViewModels.IMessageRepliesViewModel;
    public trashMessagesList: any;
    public trashMessages: any;
    public pages: any[]; 
    public messageList = [];
    showAllMessages = true;
    showMail=false;
    composeMail=false;
    public errorMessage: any;
    ShowUserId : any;
    ShowUserProfile: any;
   
    pageNumber: number;
    public selectedTrashMessage: any;
    public selectedReplyMessage: any;
    attachments = [];
    sendReply= false;
    totalPages = 0;
    currentPage = 1;
    from = 0;
    size = 10;
    total = 0;
    showImgAttachment:boolean;
    
    constructor(private messageService: MessageService, private toasterService: NotifyService, 
    private userService: UserService, private utilService: UtilService) {
        this.MessageRepliesModel=<ViewModels.IMessageRepliesViewModel>{};
        this.totalPages;
        this.pageNumber = 0;
        this.init();
    }

    init() {
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response); 
                this.ShowUserId = JSON.parse(response).Items[0]._id;
                console.log("roles",this.ShowUserId);
            },
            error => this.errorMessage = <any>error);
        this.trashMessagesList = [];
        this.from = 0 ;
        this.currentPage = 1;
       // this.getAdminMessageData();
   
        this.getdeletedMessages(this.from, this.size);
       
    }
    getdeletedMessages(from,size){
        this.messageService.getTrashMessages(from,this.size)
        .then(
       Messages => {
            this.trashMessagesList = JSON.parse(Messages).Items;
            this.totalPages = JSON.parse(Messages).TotalPages;
            this.trashMessages = this.trashMessagesList;
            this.total = JSON.parse(Messages).TotalItems;
            
            this.ValidateScrollContainer();
            },

        error => this.errorMessage = <any>error);

    }

    getdeletedMessagesOnScroll(){
        this.currentPage += 1;
        if (this.currentPage < this.totalPages) {
        this.messageService.getTrashMessages(this.currentPage, this.size)
        .then(
        Messages => {
            this.trashMessagesList = JSON.parse(Messages).Items;
            if(this.trashMessagesList.length != 0)
            {
                this.total = this.trashMessagesList.TotalItems;            
                this.trashMessages = this.trashMessages.concat(this.trashMessagesList.Items);
            }
        },
        error => this.errorMessage = <any>error);
    }
}
     public showMessage(message) {
  //      console.log(message);
        this.showAllMessages = false;
        this.composeMail=false;
        this.selectedTrashMessage = message;
        this.selectedReplyMessage= this.selectedTrashMessage.Replies;
    }
        
     
    ReplyDiv(){
         this.sendReply=true;
        
       }       
       
     ReplyMessage() {
        let model = { "messageId": this.selectedTrashMessage._id};
        this.MessageRepliesModel.To=this.selectedTrashMessage.From;
        //this.MessageRepliesModel.Reply=this.MessageModel.Message;
        this.MessageRepliesModel.Attachment=this.attachments ;
        model["Reply"]= this.MessageRepliesModel
    //    console.log("this.replyModel", model);
        this.messageService.replyMail(model).then(
            res => {
      //          console.log("Reply Send", res);
                this.selectedTrashMessage=JSON.parse(res);
                this.toasterService.SuccessSnack( "Reply Mail Sent Successfully");
                this.sendReply = false;
                this.showMail = false;
                this.showAllMessages = false;
               
                
                //this.init();
               
            },
            error => this.errorMessage = <any>error);
       
    }   
   
    test(value, i) {
        this.attachments.splice(i, 1);
    //    console.log("Pop", this.attachments);
    }
    loadFile(){

        $('#my-file').click();

}
    ChangeListener($event) {
      //  console.log("change listenser called")
        let files = [].slice.call($event.target.files);
        var file: File = files[0];
        let fileName = file.name.split(".")[0];
        this.utilService.convertToBase64(file);
        let count = 0;
        this.utilService.invokeEvent.subscribe(value => {
            if (count == 0) {
                this.utilService.uploadBase64AndGetUrl(fileName, value).then(
                    response => {
          //              console.log("Extract Imagedata from uploadBase64AndGetUrl", JSON.parse(response));
                        let obj = { "Name": fileName, "URL": JSON.parse(response).ImageData };
                        this.attachments.push(obj);
        //                console.log(" this.attachments ", this.attachments);

                    },
                    error => {
                        this.toasterService.ErrorSnack("Problem in uploading the image. Please try again");
                    });
            }
            count++;
        });

    } 
    
    LoadOnScroll(){
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);
            
        if(scrollLength - scrollContainer.scrollTop < 1) 
            
        {
                setTimeout(()=>{
                 this.getdeletedMessagesOnScroll();
                },500)
            
            
        }
        
     }
 
     ResetPagination()
     {
         this.from = 0;
         this.total = 0;
         jQuery('#scroll-container').css("height",500);
     }


    ValidateScrollContainer()
    {
      setTimeout(()=>{
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
    if(scrollContainer!=null && scrollBox!=null){
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        if(scrollBoxPos.height < scrollContainerPos.height)
        {
            jQuery('#scroll-container').css("height",scrollBoxPos.height);
        }
    }
      },1000)
    }

}