import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { TrashComponent } from './trash.component';
import { Routes, RouterModule } from '@angular/router';
  
  
const trash: Routes = [
    {path: '', component:TrashComponent}
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(trash)
  ],
  declarations: [
    TrashComponent
  ]
})

export class TrashModule { }