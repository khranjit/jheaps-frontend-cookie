import { Component, OnInit, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { UserService } from '../../../services/user.service';


@Component({
  changeDetection: ChangeDetectionStrategy.Default,
  encapsulation: ViewEncapsulation.Emulated,
  selector: 'messages',
  templateUrl: './messages.component.html'
})
export class MessagesComponent {

  userRole: boolean = false;
  ShowUserProfile: any;

  constructor(private userService: UserService) {
    this.userService.showProfile().then(
      response => {
        this.ShowUserProfile = JSON.parse(response);
        var user = this.ShowUserProfile.Items[0].Roles[0];
        if(user != 'Other'){
          this.userRole = true;
        }

      })
  }
}