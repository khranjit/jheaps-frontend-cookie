import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { SentComponent } from './sent.component';
import { Routes, RouterModule } from '@angular/router';
  
  
const sent: Routes = [
    {path: '', component:SentComponent}
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(sent)
  ],
  declarations: [
    SentComponent
  ]
})

export class SentModule { }