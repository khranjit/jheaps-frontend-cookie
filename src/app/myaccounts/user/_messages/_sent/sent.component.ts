import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../../../services/messages.service'
import { NotifyService } from '../../../../services/notify.service'
import { UtilService } from '../../../../services/util.service';
import { UserService } from '../../../../services/user.service';

declare var jQuery : any;

@Component({
    
    selector: 'sent',
    styleUrls:['sent.component.css'],
    providers: [MessageService, NotifyService, UtilService],
    templateUrl: './sent.component.html'
})
export class SentComponent {
    public MessageRepliesModel: ViewModels.IMessageRepliesViewModel;
    public sentMessagesList: any;
    public errorMessage: any;
    public selectedOutboxMessage: any;
    public selectedReplyMessage: any;
    public messageList = [];
    public pages: any[]; 
    delete =true;
    showAllMessages = true;
    showMail=false;
    sentMessages: any;
    deletedMailArray = [];
    ShowUserId : any;
    ShowUserProfile: any;
    pageLoader = false;
    showImgAttachment:boolean;

    pageNumber: number;
    attachments = [];
    sendReply= false;
    public DeletedJson: any;
    totalPages = 0;
    currentPage = 1;
    from = 0;
    size = 10;
    total = 0;
    constructor(private userService: UserService,private messageService: MessageService, private toasterService: NotifyService,
     private utilService: UtilService) {
        this.MessageRepliesModel=<ViewModels.IMessageRepliesViewModel>{};
        this.DeletedJson = {  "Ids": [] };
        this.totalPages = 0;
        this.pageNumber = 0;
        this.init();
    }

    init() {
        this.showImgAttachment=false;
        this.delete=true;
        console.log(this.delete);
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response); 
                this.ShowUserId = JSON.parse(response).Items[0]._id;
                console.log("roles",this.ShowUserId);
            },
            error => this.errorMessage = <any>error);
        window.scrollTo(0,20);
        this.from = 0 ;
        this.currentPage = 1;
       // this.getAdminMessageData();
        this.sentMessagesList = [];
        this.getSentMessages(this.from, this.size);
    }


    getSentMessages(from, size)
    {
        console.log(from);
        this.pageLoader = true;
        this.messageService.getOutboxMessages(from, this.size)
        .then(
        outboxMessages => {
            this.sentMessagesList = JSON.parse(outboxMessages).Items;
            this.sentMessages = this.sentMessagesList;
            this.pageLoader = false;
            this.total = JSON.parse(outboxMessages).TotalItems;
           
this.ValidateScrollContainer();
        },
        error => this.errorMessage = <any>error);

    }

    getSentMessagesOnScroll(){
        this.from += this.size;
        if (this.from < this.total) {
        this.messageService.getOutboxMessages(this.from, this.size)
        .then(
        outboxMessages => {
            this.sentMessagesList = JSON.parse(outboxMessages);
            this.total = this.sentMessagesList.TotalItems;            
            this.sentMessages = this.sentMessages.concat(this.sentMessagesList.Items);
        },
        error => this.errorMessage = <any>error);
    }
    }

    public showMessage(message) {
    //    console.log(message);
        this.showAllMessages = false;
        this.selectedOutboxMessage = message;
        this.selectedReplyMessage= this.selectedOutboxMessage.Replies;
    }

    checkedMail(value, event) {

        if (event.target.checked) {
            
            this.delete=false;
            console.log(this.delete);
            this.deletedMailArray.push(value);
        }
        else if (!event.target.checked) {
            this.delete=true;
            console.log(this.delete);
            let indexx = this.deletedMailArray.indexOf(value);
            this.deletedMailArray.splice(indexx, 1);
        }
      //  console.log(this.deletedMailArray)

    }


    public moveToTrash() {
    
        if (this.deletedMailArray.length > 0) {
           
            this.DeletedJson.Ids = this.deletedMailArray;
        //    console.log(this.DeletedJson)
            this.messageService.deleteMessages(this.DeletedJson)
                .then(
                deleteMessages => {
          //          console.log(deleteMessages);
                    this.toasterService.SuccessSnack('Selected Mails Deleted');
                    this.init();
                },
                error => this.errorMessage = <any>error);
        }
        else {
            this.toasterService.ErrorSnack( 'Please select mails to Delete');
        }

    }
    
    ReplyDiv(){
         this.sendReply=true;
         this.attachments = [];
         jQuery('#writeReply1').focus();
       }       
       
     ReplyMessage() {
        let model = { "messageId": this.selectedOutboxMessage._id};
        this.MessageRepliesModel.From=this.selectedOutboxMessage.From._id;
        this.MessageRepliesModel.To=this.selectedOutboxMessage.To._id;
        //this.MessageRepliesModel.Reply=this.MessageModel.Message;
        this.MessageRepliesModel.Attachment=this.attachments ;
        model["Reply"]= this.MessageRepliesModel
    //    console.log("this.replyModel", model);
        this.messageService.replyMail(model).then(
            res => {
      //          console.log("Reply Send", res);
                this.selectedOutboxMessage=JSON.parse(res);
                jQuery('#replyForm')[0].reset();
                this.toasterService.SuccessSnack( "Reply Mail Sent Successfully");
                this.showImgAttachment=false;
                this.sendReply = false;

                this.showMail = false;
                this.showAllMessages = true;
                this.init();
               
            },
            error => this.errorMessage = <any>error);
       
    }   
   
    test(value, i) {
        this.attachments.splice(i, 1);
    //    console.log("Pop", this.attachments);
    }
    loadFile(){

        $('#my-file').click();
        this.showImgAttachment=true;
    }
    ChangeListener($event) {
        //console.log("change listenser called")
        let files = [].slice.call($event.target.files);
        var file: File = files[0];
        let fileName = file.name.split(".")[0];
        this.utilService.convertToBase64(file);
        let count = 0;
        this.utilService.invokeEvent.subscribe(value => {
            if (count == 0) {
                this.utilService.uploadBase64AndGetUrl(fileName, value).then(
                    response => {
      //                  console.log("Extract Imagedata from uploadBase64AndGetUrl", JSON.parse(response));
                        let obj = { "Name": fileName, "URL": JSON.parse(response).ImageData };
                        this.attachments.push(obj);
        //                console.log(" this.attachments ", this.attachments);
                
                    },
                    error => {
                        this.toasterService.ErrorSnack( "Problem in uploading the image. Please try again");
                    });
            }
            count++;
        });

    } 
    LoadOnScroll(){
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);
            
        if(scrollLength - scrollContainer.scrollTop < 1) 
            
        {
                setTimeout(()=>{
                 this.getSentMessagesOnScroll();
                },500)
            
            
        }
        
     }
 
     ResetPagination()
     {
         this.from = 0;
         this.total = 0;
         jQuery('#scroll-container').css("height",500);
     }


    ValidateScrollContainer()
    {
      setTimeout(()=>{
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
    if(scrollContainer!=null && scrollBox!=null){
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        if(scrollBoxPos.height < scrollContainerPos.height)
        {
            jQuery('#scroll-container').css("height",scrollBoxPos.height);
        }
    }
      },1000)
    }
}