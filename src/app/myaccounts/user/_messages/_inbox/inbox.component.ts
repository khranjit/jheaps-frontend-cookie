import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../../../services/messages.service'
import { NotifyService } from '../../../../services/notify.service'
import { UtilService } from '../../../../services/util.service';
import { UserService } from '../../../../services/user.service';

declare var jQuery : any;

@Component({
    
    selector: 'inbox',
    styleUrls:['inbox.component.css'],
    providers: [UserService,MessageService, NotifyService, UtilService],
    templateUrl: './inbox.component.html'
})
export class InboxComponent {
    public MessageModel: ViewModels.IMessageViewModel;
    public MessageRepliesModel: ViewModels.IMessageRepliesViewModel;
    public inboxMessagesList: any;
    public errorMessage: any;
    public selectedInboxMessage: any;
    public selectedReplyMessage: any;
    showAllMessages = true;
    showMail=false;
    composeMail=false;
    showChooseFile=false;
    delete =true;
    inboxMessages: any;
    deletedMailArray = [];
    public pages: any[]; 
    ShowUserId : any;
    ShowUserProfile: any;
    pageNumber: number;
    public DeletedJson:any;
    attachments = [];
    sendReply= false;
    totalPages = 0;
    currentPage = 1;
    from = 0;
    size = 10;
    total = 0;
    userRole:string;
    userRoles:boolean = false;
    showImgAttachment:boolean;
    
    constructor(private userService: UserService, private messageService: MessageService, private toasterService: NotifyService,
        private utilService: UtilService) {
            this.showChooseFile=false;
 
        this.MessageModel=<ViewModels.IMessageViewModel>{};
        this.MessageRepliesModel=<ViewModels.IMessageRepliesViewModel>{};
        this.DeletedJson={"Ids" :[]};
        this.totalPages = 0;
        this.pageNumber = 0;
        this.init();
    }

    init() {
        this.showImgAttachment=false;
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response); 
                this.ShowUserId = JSON.parse(response).Items[0]._id;
                console.log("roles",this.ShowUserId);
                this.userRole = this.ShowUserProfile.Items[0].Roles[0];
                var user = this.ShowUserProfile.Items[0].Roles[0];
                if(user != 'Other'){
                    this.userRoles = true;
                }
            },
            error => this.errorMessage = <any>error);
    
        window.scrollTo(0,20);
        this.from = 0 ;
        this.currentPage = 1;
        this.inboxMessagesList = [];
        this.getinboxMessages(this.from,this.size);
    }


    getinboxMessages(from,size){
        this.messageService.getInboxMessages(from,size)
        .then(
        inboxMessages => {
            this.inboxMessagesList = JSON.parse(inboxMessages).Items;
            this.inboxMessages = this.inboxMessagesList;
            this.total = JSON.parse(inboxMessages).TotalItems;
           
            this.ValidateScrollContainer();
        },
        error => this.errorMessage = <any>error);

    }

    public getinboxMessagesOnScroll(){
        this.messageService.getInboxMessages(this.from,this.size)
        .then(
        inboxMessages => {
            this.inboxMessagesList = JSON.parse(inboxMessages);
            this.total = this.inboxMessagesList.TotalItems;            
            this.inboxMessages = this.inboxMessages.concat(this.inboxMessagesList.Items);
        },
        error => this.errorMessage = <any>error);
        
    }

    public showMessage(message) {
     //   console.log(message);
        this.showAllMessages = false;
        this.composeMail=false;
        this.selectedInboxMessage = message;
        this.selectedReplyMessage= this.selectedInboxMessage.Replies;
        // if(this.selectedReplyMessage[0].Reply.From._id!=this.ShowUserId)
        if(this.selectedInboxMessage.From._id != this.ShowUserId)
        {
            console.log('id');
        } 
        
            if(this.selectedReplyMessage[0].From._id==this.ShowUserId){
                console.log('id');
            }
               
        console.log(this.selectedReplyMessage);
    }

    
    checkedMail(value, event) {
        if (event.target.checked) {
            this.delete=false;
            this.deletedMailArray.push(value);
        }
        else if (!event.target.checked) {
            this.delete=true;
            let indexx = this.deletedMailArray.indexOf(value);
            this.deletedMailArray.splice(indexx, 1);
        }
     }


    moveToTrash() {
        if(this.deletedMailArray.length>0){
            this.DeletedJson.Ids =this.deletedMailArray;
       //     console.log(this.DeletedJson)
            this.messageService.deleteMessages(this.DeletedJson).then(
               deleteMessages=>{
         //       console.log(deleteMessages);
                this.toasterService.SuccessSnack( 'Deleted Successfully');
                this.init();
               },
               error => this.errorMessage = <any>error);
         }
        else
            {
             this.toasterService.ErrorSnack('Please select mails to Delete');
            }
      }
    
      LoadOnScroll(){
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);
            
        if(scrollLength - scrollContainer.scrollTop < 1) 
            
        {
                setTimeout(()=>{
                 this.getinboxMessagesOnScroll();
                },500)
            
            
        }
        
     }
 
     ResetPagination()
     {
         this.from = 0;
         this.total = 0;
         jQuery('#scroll-container').css("height",500);
     }

     ValidateScrollContainer()
     {
       setTimeout(()=>{
         var scrollContainer = document.getElementById('scroll-container');
         var scrollBox = document.getElementById('scroll-box');
        if(scrollContainer!=null && scrollBox!=null){
         var scrollContainerPos = scrollContainer.getBoundingClientRect();
         var scrollBoxPos = scrollBox.getBoundingClientRect();
         if(scrollBoxPos.height < scrollContainerPos.height)
         {
             jQuery('#scroll-container').css("height",scrollBoxPos.height);
         }
        }
       },1000)
     }
     
      
   mailMessage(){
      //  console.log(this.MessageModel);
      this.MessageModel.Attachment=this.attachments;
      this.messageService.sendMail(this.MessageModel)
            .then(
            res => {  
        //    console.log("Mail Send",res);
            jQuery('#ComposeForm')[0].reset(); 
            this.toasterService.SuccessSnack("Mail Sent Successfully");
            this.showAllMessages=true;
            this.composeMail=false;
            this.init();
            },
            error => {
                this.errorMessage = <any>error;
                this.toasterService.ErrorSnack("User Not Found");
            }
            
        );
    }
   
     ReplyDiv(){
         this.sendReply=true;
         this.attachments=[];
         jQuery('#writeReply').focus();
       }       
       
     ReplyMessage() {
        let model = { "messageId": this.selectedInboxMessage._id};
        this.MessageRepliesModel.From=this.selectedInboxMessage.From._id;
        this.MessageRepliesModel.To=this.selectedInboxMessage.To._id;
        //this.MessageRepliesModel.Reply=this.MessageModel.Message;
        this.MessageRepliesModel.Attachment=this.attachments ;
        model["Reply"]= this.MessageRepliesModel
    //    console.log("this.replyModel", model);
        this.messageService.replyMail(model).then(
            res => {
      //          console.log("Reply Send", res);
                this.selectedInboxMessage=JSON.parse(res);
                jQuery('#replyForm')[0].reset();                
                this.toasterService.SuccessSnack( "Reply Mail Sent Successfully");
                this.showImgAttachment=false;
                this.sendReply = false;
                this.showAllMessages = true;
                this.showMail = false;
                this.composeMail = false;
                this.init();
            },
            error => this.errorMessage = <any>error);
       
    }   
   
    ShowComposeMail() {
        
       this.showMail=false;
       this.showAllMessages=false;
       
   }
   
   test(value, i) {
        this.attachments.splice(i, 1);
        //console.log("Pop", this.attachments);
    }

    FileClick($event)
    {
//        console.log($event);
    }
    loadFile(){

            $('#my-file').click();
            this.showImgAttachment=true;

    }
  
    ChangeListener($event) {
        //console.log("change listenser called")
        let files = [].slice.call($event.target.files);
        var file: File = files[0];
        let fileName = file.name.split(".")[0];
        this.utilService.convertToBase64(file);
        let count = 0;
        this.utilService.invokeEvent.subscribe(value => {
            if (count == 0) {
                this.utilService.uploadBase64AndGetUrl(fileName, value).then(
                    response => {
      //                  console.log("Extract Imagedata from uploadBase64AndGetUrl", JSON.parse(response));
                        let obj = { "Name": fileName, "URL": JSON.parse(response).ImageData };
                        this.attachments.push(obj);
        //                console.log(" this.attachments ", this.attachments);

                    },
                    error => {
                        this.toasterService.ErrorSnack( "Problem in uploading the image. Please try again");
                    });
            }
            count++;
        });

    } 
    
}
