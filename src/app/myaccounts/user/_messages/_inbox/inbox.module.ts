import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { InboxComponent } from './inbox.component';
import { Routes, RouterModule } from '@angular/router';
  
  
const inbox: Routes = [
    {path: '', component:InboxComponent}
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(inbox)
  ],
  declarations: [
    InboxComponent
  ]
})

export class InboxModule { }