import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PurchasesComponent } from './purchases.component';
import { SharedModule } from '../../../shared/shared.module';
import { MatInputModule } from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';



  
const purchase: Routes = [
    {path: '', component:PurchasesComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(purchase),
    MatInputModule,
    MatSelectModule,
    MatButtonModule
  ],
  declarations: [
    PurchasesComponent
  ],
})
export class PurchasesModule { }