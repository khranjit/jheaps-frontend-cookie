import { Component,HostListener, OnInit, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { OrderService } from '../../../services/order.service';
import { UtilService } from '../../../services/util.service';
import { MessageService } from '../../../services/messages.service';
import { NotifyService } from '../../../services/notify.service';
import { NgxSpinnerService } from 'ngx-spinner';
import {ReturnReasonService} from '../../../services/return-reason.service';
import { UserService } from '../../../services/user.service';
import { SellOnZgrooService } from '../../../services/sell-on-zgroo.service';
import { RegisterService } from '../../../services/register.service';



import { Router, NavigationExtras, NavigationEnd, ActivatedRoute } from '@angular/router';

declare var jQuery: any;

@Component({

    changeDetection: ChangeDetectionStrategy.Default,
    encapsulation: ViewEncapsulation.Emulated,
    selector: 'purchases',
    providers: [UserService, OrderService, UtilService, NotifyService, MessageService,ReturnReasonService,SellOnZgrooService,RegisterService],
    styleUrls:['purchases.component.css'],
    templateUrl: './purchases.component.html'
})

export class PurchasesComponent {
    @HostListener("window:scroll", ["$event"])
    onWindowScroll() {
    let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    let max = document.documentElement.scrollHeight;
    var percentage = Math.round((pos/max) * 100) / 100;
     if(percentage  > 0.75 )   {
         this.callscroll();
     }
    }
    cancelReasonModel: any = {
        subject: "",
        message: ""
    }
    public firstItem:boolean = false;
    activeTabModel: any;
    loadtrigger = true;
    activeTabProperty: any;
    bankdetailsModel:any = {};
    returnOrderDate: any;
    returnProductName: any;
    returnShop: any;
    Attachment = [];
    buyerId: any;
    buyerName: any;
    order:any = [];
    purchaseReturnOrder: any;
    purchaseCancelOrder: any;
    errorMessage: any;
    paymentStatusInfo: any;
    paymentStatus: any;
    public shopLists: any;
    public shopListArray: any;
    products: any;
    public productCurrency: any = { value: "₹" };
    orderStatus: any;
    //totalPages: number;
    viewAllOrders: any;
    showPurchase: boolean;
    orderList:any=[];
    placedorder: any;
    intransitorder: any;
    delivered: any;
    DisplayCancelOrder: boolean;
    attachments = [];
    ShopIdPurchase: any;
    objKey = "";
    cancelOrderId: any;
    selectedOrder = [];
    refundReplaceOrder=[];
    showCancel: boolean;
    returnModel = {};
    returnReason: any;
    returnDetails: any;
    returnButtonStatus: boolean;
    returnOrderId: any;
    returnProductId: any;
    returnShopId: any;
    returnProductIndex: any;
    productReviewHead: any;
    productReviewComment: any;
    productRating: any;
    reviewModel: any;
    reviewProductId: any;
    reviewProductName: any;
    reviewShop: any;
    orderArray = [];
    allOrder = <any>{};
    disableSaveBank = false;
    editDetails = true;
    editBank = false;
    totalPages = 0;
    from = 0;
    currentPage = 0;
    pageNumber = this.currentPage + 1;
    size = 4;
    total = 0;
    returnQty = 0;
    enteredQty = 1;
    CancelItem = 0;
    CancelMsg = "";
    CancelAbonded = false;
    ShowTicket = false;
    showReturnedOrderDiv=false;
    showOrderDetails=false;
    Messages: any = [];
    Ticket: any = {};
    replyMail = false;
    bankdetail = false;
    BuyerTickets = <any>{};
    ShowUserId: any;
    reviewShopId: any;
    reviewShopName: any;
    ShowUserProfile: any;
    loader1 = false;
    loader2 :boolean;
    pageLoader = false;
    returnreasonlist=[];
    returnTicketId:any;
    IFSC_code: string;
    responseDataModel: any;
    BankDetails: any;
    IFSCsearch: boolean;
    BankSearch: any;
    StateSearch: any;
    CitySearch: any;
    BranchSearch: any;
    IFSCResult: any;
    bankListArray: any;
    accountIFSCCheck = false;
    stateListArray: any;
    cityListArray: any;
    branchListArray: any;
    statesArray: any;
    countryId: any;
    stateId: any;
    state: string;
    cityLists: any;
    selectedCityName: any;
    selectedBranchName: any;
    selectedBankName: any;
    banksearchresult = "result";
    orderType: any;
    sellerName: any;

    public pages: any[];
    statusList = [{ name: "Placed", code: 1 }, { name: "Ready for Shipping", code: 2 }, { name: "Intransit", code: 3 }, { name: "Out for Delivery", code: 4 }]

    constructor(private userService: UserService,private returnReasonService: ReturnReasonService, private router: Router, private orderService: OrderService, private messageService: MessageService, private loader: NgxSpinnerService, private utilService: UtilService, private loadingservice: NgxSpinnerService, private snack: NotifyService,private sellOnZgrooService:SellOnZgrooService,private registerService:RegisterService) {

        this.returnButtonStatus = true;
        this.orderStatus = {};
        this.viewAllOrders = [];
        this.ShopIdPurchase = [];
        this.showPurchase = true;
        this.IFSC_code = "";
        this.BankDetails = {};
        this.attachments = [];
        this.responseDataModel = {};
        this.bankListArray = [];
        this.stateListArray = [];
        this.cityListArray = [];
        this.branchListArray = [];
        this.statesArray = []; 
        this.BankSearch = "";
        this.StateSearch = "";
        this.CitySearch = "";
        this.BranchSearch = "";
        this.IFSCResult = "";
        this.countryId = {};
        this.stateId = {};
        this.cityLists = [];
        this.IFSCsearch = true;
        this.loader2= false;
        this.init();
    }

    getShopkey(item) {
        return Object.keys(item);
    }
    public MediaImage(Product) {
        var Image = () => {
            return Product.Media.find(x => x.IsPrimary).EditedImage;
        }
        if (Product.hasOwnProperty("Variation")) {
            if (Product.Variation.hasOwnProperty("Image")) {
                if (Product.Variation.Image != "") {
                    return Product.Variation.Image;
                }
                else {
                    return Image();
                }
            }
            else {
                return Image();
            }
        }
        else {
            return Image();
        }
    }
    public init() {
        this.ResetPagination();
        this.getBankList();
        this.countryId = { "CountryId": 101 }
        this.registerService.getStates(this.countryId).then(
            statesInfo => {
                this.statesArray = JSON.parse(statesInfo);
            }, error => this.errorMessage = <any>error);
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response);
                this.ShowUserId = JSON.parse(response).Items[0]._id;
            },
            error => this.errorMessage = <any>error);
        this.returnReasonView();
        this.getDataforView();
    }
    getSellerBankDetails(){
        window.scroll(0, 0);
        this.bankdetailsModel.IFSC = this.IFSCResult
        if(this.bankdetailsModel.AccountNumber == this.bankdetailsModel.ConfirmAccountNumber){
            this.bankdetailsModel.OrderId = this.refundReplaceOrder[0]._id;
            this.orderService.updateBankDetails(this.bankdetailsModel).then(res=>{
            var result = JSON.parse(res);
            if(!result.result){
                this.snack.ErrorSnack(result.msg);
            }else{
                this.bankdetail = false;
                this.showReturnedOrderDiv=false
                this.showPurchase=false;
                this.showOrderDetails=true;
            }
            })
        }else{
            this.snack.ErrorSnack("Account Number doesn't match");
        }
       
    }

    LoadOnScroll() {
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

        if (scrollLength - scrollContainer.scrollTop < 1) {
            setTimeout(() => {
                this.dataforView();
            }, 500)
        }
    }

    ResetPagination() {
        this.pageNumber = 0;
        this.total = 0;
        this.from = 0;
        this.size = 4;
        jQuery('#scroll-container').css("height", 500);
        jQuery('#scroll-container1').css("height", 500);
    }
    goToTop() {
        window.scrollTo(0, 0);
    }
    ReplyMessage() {
        window.scroll(0, 0);
        var replyModel = {
            To: this.Messages[0].To[0]._id,
            Reason: this.BuyerTickets.Reason,
            Description: this.BuyerTickets.Description,
            Attachment: this.attachments,
            id: this.Ticket._id._id
        }
        this.messageService.replyMessage(replyModel).then(
            res => {
                var response = JSON.parse(res);
                if(response.hasOwnProperty('flag'))
                {
                    this.snack.ErrorSnack(response.msg);
                }
                else{
                    this.snack.SuccessSnack("Your queries successfully Submitted");
                }
                this.viewTicket(this.Ticket._id._id,this.orderType,this.sellerName);
            },
            error => this.errorMessage = <any>error);
    }
    ValidateScrollContainer() {
        setTimeout(() => {
            var scrollContainer = document.getElementById('scroll-container');
            var scrollBox = document.getElementById('scroll-box');
            
        if(scrollContainer!=null && scrollBox!=null)
        {
                var scrollContainerPos = scrollContainer.getBoundingClientRect();
                 var scrollBoxPos = scrollBox.getBoundingClientRect();
                if (scrollBoxPos.height < scrollContainerPos.height) {
                jQuery('#scroll-container').css("height", scrollBoxPos.height);
                }
        }
        }, 1000)
    }
    stepindicator(step,order)
    {
        if(step == 1 && ( order== 'Placed' || order == 'Ready For Shipment' || order == 'Pickup Scheduled' || order == 'In Transit' || order == 'Delivered' || order == 'Completed'))
        {
            return "completed";
        }
        else if(step == 2 && (order == 'Pickup Scheduled' || order == 'In Transit' || order == 'Delivered' || order == 'Completed'))
        {
            return "completed";
        }
        else if(step == 3 && (order == 'In Transit' || order == 'Delivered' || order == 'Completed'))
        {
            return "completed";
        }
        else if(step == 4 && (order == 'Delivered' || order == 'Completed'))
        {
            return "completed";
        }
    }
    getDataforView() {
        this.pageLoader = true;
        this.orderService.Buyerpuchase(this.currentPage, this.size).then(
            response => {
                var data = JSON.parse(response);
                this.pageLoader = false;
                this.firstItem=true;
                this.totalPages = data.TotalPage;
                this.orderList = data.Items;
                this.total = data.TotalItems;
            }, error => this.errorMessage = <any>error);
    }
    async callscroll() {
        if (this.firstItem) {
            await  this.dataforView();
        }
    }
    dataforView() {
        if (this.currentPage + 1 < this.totalPages && this.loadtrigger) {
        this.loadtrigger = false;
        this.currentPage += 1;
        this.loader1= true;
        this.orderService.Buyerpuchase(this.currentPage, this.size).then(
            response => {
                var data = JSON.parse(response);
                this.loader1= false;
                this.orderList = this.orderList.concat(data.Items);
                this.loadtrigger=true;
                this.total = this.orderList.Items;
                this.totalPages = data.TotalPage;
            }, error => this.errorMessage = <any>error);
        }
    }
  
    checkCancelHours(CreatedDate, status,order) {
        if (status == "Placed" && order.OrderType == 1 && !order.InvoiceLink) 
            this.DisplayCancelOrder = true;
        else
            this.DisplayCancelOrder = false;
        return this.DisplayCancelOrder;
    }

    cancel(order, id, cancel) {
        this.cancelOrderId = id;
        if (cancel == "Item") {
            this.purchaseCancelOrder = order;
            jQuery("#cancelItemModal").modal("show");
            this.CancelItem = 0;
        }
        else {
            this.purchaseCancelOrder = "";
            jQuery("#cancelOrderModal").modal("show");
        }
    }
    viewBankDetail(){
        this.bankdetail = true;
        this.showReturnedOrderDiv=false
        this.showPurchase=false;
        this.showOrderDetails=false;
    }
    viewTicket(TicketId,orderType,sellerName) {
        this.sellerName=sellerName;
        this.orderType=orderType;
        this.BuyerTickets = {};
        this.attachments = [];
        this.replyMail = false;
        this.ShowTicket = true;
        this.showPurchase=false;
        this.showOrderDetails=false;
        this.showReturnedOrderDiv=false;
        this.returnTicketId=TicketId;
        this.messageService.viewBuyerTickets(TicketId)
            .then(
                tickets => {
                    var data = JSON.parse(tickets);
                    this.Ticket = data[0];
                    this.Messages = this.Ticket.Messages;
                },
                error => this.errorMessage = <any>error);
    }
    confirmCancelOrder() {
        jQuery("#cancelOrderModal").modal("hide");
        jQuery("#cancelApprovalModal").modal("hide");
        jQuery("#confirmCancelOrderModal").modal("show");
    }
    confirmItemCancel() {
        this.CancelItem = 1;
        this.confirmCancelOrder();
    }
    download(res) {
        var link = document.createElement("a");
        link.download = res;
        link.target = '_blank';
        link.href = res;
        document.body.appendChild(link);
        link.click();

    }
    downloadlabel(orderId) {
        var model: any = {
            orderId: orderId
        };
        this.orderService.donloadShippingLabel(model).then(
            response => {
                var res = JSON.parse(response);
                this.download(res.TransactionDetails.shipments[0].shipmentPackages.forward_label);
            });
    }
    action(orderId) {
        let model = {
            "OrderIds": [orderId]
        }
        this.loader.show();
        this.orderService.shippingLabel(model).then(
            response => {
                var res = JSON.parse(response);
                this.loader.hide();
                this.snack.SuccessSnack( "Shipping Label generated successfully");
                this.download(res[0][0].shipments[0].shipmentPackages.forward_label);
                this.dataforView();
            },
            error => {
                this.errorMessage = <any>error;
                this.loader.hide();
            });
    }
    ProceedToCancel() {
        this.CancelAbonded = false;
        
        jQuery("#confirmCancelOrderModal").modal("hide");
        jQuery("#cancelItemModal").modal("hide");
        // var Attachment = {
        //     Name:this.purchaseCancelOrder.Name,
        //     URL:this.MediaImage(this.purchaseCancelOrder)
        // };
        var Replies = {
            // Attachment : Attachment,
            Message: this.cancelReasonModel.message
        };

        var EmailContent = {
            Replies: Replies,
            Subject: this.cancelReasonModel.subject
        };

        var cancelModel: any = {
            OrderId: this.cancelOrderId,
            EmailContent: EmailContent
        }
        if (this.purchaseCancelOrder != "") {
            cancelModel.ItemId = this.purchaseCancelOrder.Id;
            if(this.purchaseCancelOrder.hasOwnProperty('Variation'))
            {
                cancelModel.VariationId = this.purchaseCancelOrder.Variation._id;
            }
            if (this.CancelItem == 1) {
            cancelModel.ItemConfirmed = "Yes";
            }
        }
        this.orderService.cancelOrderBuyer(cancelModel).then(
            response => {
                var Result = JSON.parse(response);
                if (Result.msg || Result.flag == false) {
                    if(!Result.flag){
                        this.snack.ErrorSnack( "Item already cancelled");
                        this.showPurchase = true;
                        this.showOrderDetails=false;
                        this.showReturnedOrderDiv=false;
                        this.cancelReasonModel.message = "";
                        this.cancelReasonModel.subject = "";
                        this.init();
                    }else{
                        this.snack.ErrorSnack( "Can't Cancel this Order");
                    }
                }
                else if (Result.hasOwnProperty("Diff")) {
                    if (Result.Diff != 0) {
                        if (Result.RefundAmount < Result.RemovedProductPrice) {
                            if (Result.Diff > Result.RemovedProductPrice) {
                                this.CancelMsg = "You can not cancel this Item..!!";
                                this.CancelAbonded = true;
                            }
                            else {
                                if(this.selectedOrder[0].PaymentMethod == "COD"){
                                    this.CancelMsg = "Due to your cancellation you to have pay the additional charges for shipping which is Rs." + Result.Diff + ".Are you sure you want to continue..?";            
                                }else{
                                    this.CancelMsg = "Due to your cancellation you to have pay the additional charges for shipping which is Rs." + Result.Diff + " that has been deducted from your Cancel product amount so, you may get refund the amount of Rs." + Result.RefundAmount + " Are you sure you want to continue..?";
                                }
                            }
                        }
                        else {
                            this.CancelMsg = "Due to your cancellation you can get Refund Amount Rs." + Result.RefundAmount + " Because of we refund your shipping charges also that is Rs." + Result.Diff + " Are you sure you want to continue..?";
                        }
                        jQuery("#cancelApprovalModal").modal("show");
                    }
                    else {
                        this.confirmItemCancel();
                    }
                }
                else {
                    jQuery("#cancelOrderModal").modal("hide");
                    this.snack.SuccessSnack("Order has been cancelled");
                    this.showPurchase = true;
                    this.showOrderDetails=false;
                    this.showReturnedOrderDiv=false;
                    this.cancelReasonModel.message = "";
                    this.cancelReasonModel.subject = "";
                    this.init();
                    // this.router.navigate(["myaccounts/user/purchases"]);
                }
            },

            error => this.errorMessage = <any>error);
    }



    returnOrder(orderId, productId, productName, shopId, order, i) {
        this.returnOrderId = orderId;
        this.returnProductId = productId;
        this.returnProductName = productName;
        this.returnShop = shopId;
        this.enteredQty = 1;
        this.returnProductIndex = i;
        this.purchaseReturnOrder = order;
        this.returnQty = order.Items[i].Product.Qty;

        jQuery("#returnOrderModal").modal("show");
    }

    requestCourier(orderId, order, productId, shopId, i) {
        let model = {
            "OrderId": orderId,
            "ShopId": shopId,
            "ProductId": productId,
            "ProductIndexInShop": i,
            "productStatus": "Replace To Seller"
        };
        this.orderService.replaceToSeller(model).then(
            response => {
            },
            error => this.errorMessage = <any>error);
    }

    status(value) {
        this.returnReason = value;

    }

    Rating(value) {
        this.productRating = Number(value);
    }

    ReviewModal(orders) {
 
        this.reviewProductId = orders.Items[0].Product.Id;
        this.reviewProductName = orders.Items[0].Product.Name;
        this.reviewShopId= orders.Items[0].Product.ShopId;
        this.reviewShopName=orders.ShopAccountId.approvedBusinessDetails.Name;
        jQuery("#reviewModal").modal("show");
    }

    Review() {
        this.reviewModel = {
            "Product": this.reviewProductId, "ShopId":this.reviewShopId, "Rating": this.productRating,
            "Review": this.productReviewComment
        };
        this.orderService.productReview(this.reviewModel).then(
            response => {
                var res = JSON.parse(response);
                if(res.Flag == 2){
                    jQuery("#reviewModal").modal("hide");
                    this.snack.ErrorSnack("You are already review for this product");
                }else{
                    jQuery("#reviewModal").modal("hide");
                    this.snack.SuccessSnack( "Your Review posted successfully");
                }

            },

            error => this.errorMessage = <any>error);
    }

    confirmReturnOrder() {
        this.returnModel = {
            "OrderId": this.returnOrderId,
            "ItemId": this.returnProductId,
            "ReturnQty": this.enteredQty,
            "ShopId": this.purchaseReturnOrder.Items[0].Product.ShopId,
            "EmailContent": {
                "Reason": this.returnReason,
                "Replies": {
                    "Description": this.returnDetails,
                    "Attachment": this.attachments
                },
            }
        };
        this.orderService.returnOrder(this.returnModel).then(
            response => {
                this.returnButtonStatus = false;
                var res=JSON.parse(response);
               
                if(res.ok==1)
                {  jQuery("#returnOrderModal").modal("hide");
                   this.snack.SuccessSnack( "Your return requested sucessfully");
                    this.showPurchase=true; 
                    this.showOrderDetails=false;
                    this.showReturnedOrderDiv=false;
                    this.currentPage=0;
                    this.init();
                }
                else {
                   this.snack.ErrorSnack(res.message);
                   this.showPurchase=true; 
                   this.showOrderDetails=false;
                   this.showReturnedOrderDiv=false;
                   this.currentPage=0;
                   this.init();
                }
            },
            error => this.errorMessage = <any>error);
        }

    purchasedItem(paymentStatusInfo) {
        this.selectedOrder.push(paymentStatusInfo);
    }

    test(value, i) {
        this.attachments.splice(i, 1);
    }

    ChangeListener($event) {
        let files = [].slice.call($event.target.files);
        var file: File = files[0];
        var type = file.name.split('.').pop();
        if (type == "jpeg" || type == "jpg" || type == "png" || type == "bmp") {
            if (file.size < 5 * 1024 * 1024) {
                if (this.attachments.length < 5) {
                    this.loadingservice.show();
                    let fileName = file.name.split(".")[0];
                    this.utilService.convertToBase64(file);
                    let count = 0;
                    this.utilService.invokeEvent.subscribe(value => {
                        if (count == 0) {
                            this.utilService.uploadBase64AndGetUrl(fileName, value).then(
                                response => {
                                    let obj = { "Name": fileName, "URL": JSON.parse(response).ImageData };
                                    this.attachments.push(obj);
                                    this.loadingservice.hide();
                                },
                                error => {
                                    this.snack.ErrorSnack( "Problem in uploading the image. Please try again");
                                });
                        }
                        count++;
                    });
                }
                else {
                    this.snack.ErrorSnack( "You can Upload Only 5 Files");
                }
            }
            else {
                this.snack.ErrorSnack( "File Size Exceeds the 5MB Limit");
            }
        }
        else {
            this.snack.ErrorSnack( "File Type is not supported");
        }
    }
    returnReasonView() {

        this.returnReasonService.getAllDropDownReason().then(
            response => {
                this.returnreasonlist = JSON.parse(response).Items;
                
                
            }, error => this.errorMessage = <any>error);
    }

    public minusQuantity() {
        if (this.enteredQty > 1) {
            this.enteredQty = this.enteredQty - 1;
        }
    }
    public validQuantity() {
        if (this.enteredQty <= 0) {
            this.enteredQty = 1;
        } else if (this.enteredQty > this.returnQty) {
            this.enteredQty = this.returnQty;
        }
    }
    public plusQuantity() {
        if (this.enteredQty < this.returnQty) {
            this.enteredQty = this.enteredQty + 1;
        }
        else {
            this.snack.ErrorSnack('You can add ' + this.returnQty + ' product only');
        }
    }
    public showReturnedOrder(orderid,ordertype,prodid){
        this.refundReplaceOrder=[];
        var model = {
            orderid:orderid,
            orderType:ordertype,
            prodid:prodid
        }
        this.orderService.showReturnedOrder(model).then(
            response => {
                var res = JSON.parse(response);

                this.refundReplaceOrder.push(res);
                this.showPurchase=false;
                this.showOrderDetails=false;
                this.showReturnedOrderDiv=true;
            },
            error => this.errorMessage = <any>error);
    }
    getBankDetails() {
        if (this.bankdetailsModel.IfscCode.length > 0) {
            this.sellOnZgrooService.IFSCcodeToBankDetails(this.bankdetailsModel.IfscCode)
                .then(resdata => {
                    if (resdata.data == null) {
                        this.banksearchresult = "Please enter correct IFSC code and click search";
                        this.disableSaveBank = true;
                    }
                    else {
                        this.BankDetails = resdata.data;
                        this.bankdetailsModel.State = this.BankDetails.STATE;
                        this.bankdetailsModel.City = this.BankDetails.CITY;
                        this.bankdetailsModel.BankName = this.BankDetails.BANK;
                        this.bankdetailsModel.BranchName = this.BankDetails.BRANCH;
                        this.banksearchresult = "valid";
                        this.disableSaveBank = false;
                    }
                });
        }
    }
    ResetbankDetails() {
        this.bankdetailsModel.State = "";
        this.bankdetailsModel.City = "";
        this.bankdetailsModel.BankName = "";
        this.bankdetailsModel.BranchName = "";
        this.banksearchresult = "Please enter correct IFSC code and click search";
        this.disableSaveBank = true;
        this.editDetails = false;
        this.editBank = true;
    }
    selectState(event) {
        this.state = event
        this.sellOnZgrooService.DistrictList(this.state).then(
            Info => {
                this.branchListArray = Info.data;
                let sortlistArray = [];
                this.branchListArray.forEach((List: any) => {
                    if (List.BANK == this.selectedBankName) {
                        sortlistArray.push(List)
                    }
                });
                sortlistArray.forEach((List: any) => {

                    if (!this.cityListArray[List.CITY]) {
                        this.cityLists.push(List.CITY);
                        this.cityListArray[List.CITY] = [];
                    }
                    this.cityListArray[List.CITY].push(List);
                });
            }, error => this.errorMessage = <any>error);
    }
    getBankList() {
        this.sellOnZgrooService.BankSearch()
            .then(resdata => {
                this.bankListArray = resdata.data;
            });
    }

    selectBank(value) {
        this.selectedBankName = value;
    }
    back()
    {
        if(this.orderType==1)
        {
            this.ShowTicket = false;
            this.showOrderDetails = true;
        }
        else if(this.orderType==2)
        {
            this.ShowTicket = false;
            this.showReturnedOrderDiv = true;
        }
    }
}
