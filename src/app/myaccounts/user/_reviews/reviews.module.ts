import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReviewsComponent } from './reviews.component';
import { SharedModule } from '../../../shared/shared.module';

  
const reviews: Routes = [
    {path: '',component:ReviewsComponent}
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(reviews)
  ],
  declarations: [
    ReviewsComponent
  ],
})
export class ReviewsModule { }