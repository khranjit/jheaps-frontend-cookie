import { Component, OnInit,HostListener, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../services/user.service';


@Component({

    changeDetection: ChangeDetectionStrategy.Default,
    selector: 'reviews',
    providers: [UserService],
    styleUrls:['reviews.component.css'],
    templateUrl: './reviews.component.html'
})

export class ReviewsComponent {
    @HostListener("window:scroll", ["$event"])
    onWindowScroll() {
    let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    let max = document.documentElement.scrollHeight;
    var percentage = Math.round((pos/max) * 100) / 100;
    //console.log("percentage "+percentage)
     if(percentage  > 0.75 )   {
         this.callscroll();
      
     }
    }
    public reviewModel: ViewModels.IProductReviewViewModel;
    UserReviewList: any;
    userReviewsData = [];
    errorMessage: any;
    reviewChar: number;
    totalPages = 1;
    from = 0;
    size = 5;
    total = 0;
    pages = [];
    currentPage = 1;
    initialPage = 0;
    loader = false;
    pageLoader = false;
    limit = 6;

    public firstItem:boolean = false;
    constructor(private route: ActivatedRoute, private router: Router, private userService: UserService) {
        this.reviewChar = 200;
        this.Init();

    }
    public Init() {
        this.currentPage = 1;
        this.pages = [];
        this.userReviewsData = [];
        this.UserReviewList = [];
        this.pageLoader = true;
        this.userService.viewReview(this.initialPage,this.limit).then(
            response => {
                var userData = JSON.parse(response);
                if(userData.hasOwnProperty("Items"))
                {
                    this.UserReviewList = userData.Items;
                    this.userReviewsData = this.UserReviewList;
                    this.total = JSON.parse(response).TotalItems;
                    this.pageLoader = false;
                    this.firstItem=true;
                    this.userReviewsData.forEach(function (item) {
                        if (item.Review.length > 200)
                            item.readMore = true;
                        else
                            item.readMore = false;
                    });
                   
                    this.ValidateScrollContainer();
                }
                else
                {
                    this.pageLoader = false;
                }
            },

            error => this.errorMessage = <any>error);
    }
    async callscroll() {
        if (this.firstItem) {
            await   this.getReviewOnScroll();
        }
    }
    public getReviewOnScroll() {
        this.from += this.size;
        if (this.from < this.total) {
            this.initialPage += 1;
            this.loader= true;
            this.userService.viewReview(this.initialPage,this.limit).then(
                response => {
                    this.UserReviewList = JSON.parse(response);
                    this.total = JSON.parse(response).Items.total;
                    this.loader= false;
                    this.userReviewsData = this.userReviewsData.concat(JSON.parse(response).Items);
                    this.userReviewsData.forEach(function (item) {
                        if (item.Review.length > 200)
                            item.readMore = true;
                        else
                            item.readMore = false;
                    });
                },
                error => this.errorMessage = <any>error);
        }
    }
    LoadOnScroll() {
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

        if (scrollLength - scrollContainer.scrollTop < 1) {
            setTimeout(() => {
                this.getReviewOnScroll();
            }, 500)


        }

    }

    ResetPagination() {
        this.from = 0;
        this.total = 0;
        jQuery('#scroll-container').css("height", 500);
    }

    ValidateScrollContainer() {
        setTimeout(() => {
            var scrollContainer = document.getElementById('scroll-container');
            var scrollBox = document.getElementById('scroll-box');
        if(scrollContainer!=null && scrollBox!=null){
            var scrollContainerPos = scrollContainer.getBoundingClientRect();
            var scrollBoxPos = scrollBox.getBoundingClientRect();
            if (scrollBoxPos.height < scrollContainerPos.height) {
                jQuery('#scroll-container').css("height", scrollBoxPos.height);
            }
        }
        }, 1000)
    }

}