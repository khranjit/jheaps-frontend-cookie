import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FavouritesComponent } from './favourites.component';
import { SharedModule } from '../../../shared/shared.module';
  
  
const favorites: Routes = [
    {path: '', component:FavouritesComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(favorites)
  ],
  declarations: [
    FavouritesComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class FavouritesModule { }