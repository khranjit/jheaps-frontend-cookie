import { Component, OnInit ,HostListener, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { HomeService } from '../../../services/home.service';
import { NotifyService } from '../../../services/notify.service';
@Component({ 
    changeDetection: ChangeDetectionStrategy.Default,
    encapsulation: ViewEncapsulation.Emulated,
    selector: 'favourites-list',
    providers:[ UserService, HomeService, NotifyService ],
    templateUrl: './favourites.component.html'
  })
  export class FavouritesComponent{
      @HostListener("window:scroll", ["$event"])
      onWindowScroll() {
      let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
      let max = document.documentElement.scrollHeight;
      var percentage = Math.round((pos/max) * 100) / 100;
      //console.log("percentage "+percentage)
       if(percentage  > 0.75 )   {
           this.callproductscroll();
           this.callshopscroll();
         
         
       }
      }
      public productModel: ViewModels.IFavoriteProductViewModel;
      public shopModel : ViewModels.IFavoriteShopViewModel;
      favouriteShopList: any;
      public firstproduct:boolean = false;
      public firstshop:boolean=false;
      favouriteShops:any =[];
      favouriteProducts:any = [];
      favouriteProductList: any;
      errorMessage: any;
      totalPages = 1;
      pages=[];
      currentPage = 0;
      pageNumber = this.currentPage;
      from = 0;
      total = 0;
      size = 12;
      initialPage=0;
      PageFlag=1;
      loader = false;
      pageLoader = false;
      
      constructor(private route: ActivatedRoute, private router: Router,private userService:UserService, private homeService: HomeService,
      private toasterPopService:NotifyService) {
          this.Init();
          //this.Product();
        }
        
        LoadOnScroll(){
          var scrollContainer = document.getElementById('scroll-container');
          var scrollBox = document.getElementById('scroll-box');
          var scrollContainerPos = scrollContainer.getBoundingClientRect();
          var scrollBoxPos = scrollBox.getBoundingClientRect();
          var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);
              
          if(scrollLength - scrollContainer.scrollTop < 1) 
              
          {
                  setTimeout(()=>{
                   this.showFavouriteProductOnScroll();
                 //  this.showFavouriteShopOnScroll();
                  },500)
              
              
          }
          
       }
   
       ResetPagination()
       {
           this.from = 0;
           this.total = 0;
           this.pageNumber = 0;
          //  jQuery('#scroll-container').css("height",500);
       }
  
       ValidateScrollContainer()
      {
        setTimeout(()=>{
          var scrollContainer = document.getElementById('scroll-container');
          var scrollBox = document.getElementById('scroll-box');
      if(scrollContainer!=null && scrollBox!=null){
          var scrollContainerPos = scrollContainer.getBoundingClientRect();
          var scrollBoxPos = scrollBox.getBoundingClientRect();
          if(scrollBoxPos.height < scrollContainerPos.height)
          {
              jQuery('#scroll-container').css("height",scrollBoxPos.height);
          }
      }
        },1000)
      }
  
      
      ngOnInit() {
        //  alert(1)  
      }
          
        public Init() {
          this.productModel = <ViewModels.IFavoriteProductViewModel>{}; 
          this.shopModel = <ViewModels.IFavoriteShopViewModel>{};  
          this.showFavouriteProduct(); 
         // this.showFavouriteShop(); 
        }
   
       public showFavouriteProduct() {
           this.ResetPagination();
           if(this.favouriteProductList != 0){
         // this.clear();
         // this.PageFlag=1;
         this.pageLoader = true;
          this.userService.viewFavProduct(this.currentPage,this.size).then(
              response => {
                  this.favouriteProductList = JSON.parse(response).Items;
                  this.pageLoader = false;
                  this.firstproduct=true;
                  this.favouriteProducts = this.favouriteProductList;
                  this.total = JSON.parse(response).TotalItems;
                 
                  this.ValidateScrollContainer();
              },
              error => this.errorMessage = <any>error);
      }
       }
    async callproductscroll() {
        if (this.firstproduct) {
            await  this.showFavouriteProductOnScroll();
        }
    }
      public showFavouriteProductOnScroll(){
          this.from += this.size; 
          if(this.from < this.total)
          {
              this.pageNumber += 1;
              this.loader = true;
          this.userService.viewFavProduct(this.pageNumber,this.size).then(
              response => {
                  this.favouriteProductList = JSON.parse(response);
                  this.total = this.favouriteProductList.TotalItems;
                  this.loader = false;
                  this.favouriteProducts = this.favouriteProducts.concat(this.favouriteProductList.Items);
              },
              error => this.errorMessage = <any>error);
              }
          }
      public showFavouriteShop() {
         // this.clear();
          //this.PageFlag=2; 
         // var ShopJson = [];
         
         if(this.favouriteShopList != 0){
          this.userService.viewFavouriteShop(this.currentPage,this.size).then(
               response => {
                  this.favouriteShopList = JSON.parse(response);
                  this.favouriteShops = this.favouriteShopList.Items;
                  console.log(this.favouriteShops.length);
                  console.log(this.favouriteShops);
                  this.total = this.favouriteShopList.TotalItems;
                  this.totalPages = Math.ceil(this.total / this.size);
                  this.firstshop=true;
                  for(let i = 1; i <= this.totalPages; i++)
                  {
                  this.pages.push(i);
                  console.log(i);
                  }
                 this.ValidateScrollContainer();
              },
              error => this.errorMessage = <any>error);
      } 
  }
  async callshopscroll() {
    if (this.firstshop) {
        await  this.showFavouriteShopOnScroll();
    }
}
      public showFavouriteShopOnScroll(){
          this.from += this.size; 
          if(this.from < this.total)
          {
              this.pageNumber += 1;
          this.userService.viewFavouriteShop(this.pageNumber,this.size).then(
              response => {
                  this.favouriteShopList = JSON.parse(response);
                  this.total = this.favouriteShopList.Items.total;                
                  this.favouriteShops = this.favouriteShops.concat(this.favouriteShopList.Items);
              },
              error => this.errorMessage = <any>error);
              }
          }
      
     ShowProductsByShop(shopId) {
          this.router.navigate(['/user/shop', shopId]);
      }
  
      
      public productInfo(product) {
          this.router.navigate(['/user/product', product._id]);  
      }
      
      FavouriteProductInfo() {
          this.router.navigate(['/user/product']);  
      }
      RemoveFavouriteProduct(productId) {
          this.homeService.addFavouriteProduct(productId).then(
              response => {
                  this.toasterPopService.ErrorSnack('Item removed from your favorites');
                  this.Init();
              });
      }
      RemoveFavouriteShop(ShopId) {
          // var Id = { Id: ShopId};
              this.homeService.addFavouriteShop(ShopId).then(
                  response => {
                      this.toasterPopService.ErrorSnack( 'Shop removed from your favorites');
                      this.showFavouriteShop();
                  });
  
      }
  
  }