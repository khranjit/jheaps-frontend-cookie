import { Component, OnInit, ChangeDetectionStrategy, ViewEncapsulation, ViewChild } from '@angular/core';
import { SellOnZgrooService } from '../../../services/sell-on-zgroo.service';
import { RegisterService } from '../../../services/register.service';
import { NotifyService } from '../../../services/notify.service';
import { UtilService } from '../../../services/util.service';
import { ShopService } from '../../../services/shop.service';
import * as Q from 'q';
import { UserService } from '../../../services/user.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { JsonpModule } from '@angular/http';



declare var jQuery: any;

@Component({

    changeDetection: ChangeDetectionStrategy.Default,
    encapsulation: ViewEncapsulation.Emulated,
    selector: 'sell-on-jheaps',
    providers: [SellOnZgrooService, RegisterService, NotifyService, UtilService, ShopService],
    templateUrl: './sell-on-jheaps.component.html'
})


export class SellOnJheapsComponent {
    public shopAccountModel= <any>{};
    public openshopModel: ViewModels.Shop.IOpenShopViewModel;
    public shopModel: ViewModels.Shop.IShopViewModel;
    public businessdetailsModel=<any>{};
    public bankdetailsModel: ViewModels.Shop.IBankDetailsViewModel;
    public profileModel = <any>{ Address: {} };
    public profileModelAddress=  <any>{};
    errorMessage: any;
    public productCurrency: any = { value: "USD" };
    IFSC_code: string;
    responseDataModel: any;
    BankDetails: any;
    IFSCsearch: boolean;
    BankSearch: any;
    StateSearch: any;
    CitySearch: any;
    BranchSearch: any;
    IFSCResult: any;
    bankListArray: any;
    stateListArray: any;
    cityListArray: any;
    branchListArray: any;
    statesArray: any;
    citiesArray: any;
    sfcitiesArray: any;
    countryId: any;
    stateId: any;
    state: string;
    cityLists: any;
    selectedCityName: any;
    selectedBranchName: any;
    selectedBankName: any;
    businessType: any;
    GSTIN: string;
    ShowUserAddress: any;
    attachments: any;
    shopId: any;
    getShopObj: any;
    loader: boolean;
    approved: boolean;
    banksearchresult = "result";
    progressbarvalue = 0;
    readonlyVar = false;
    accountIFSCCheck = false;
    value: any;
    disableSaveBank = false;
    OldEmail : string ="";
    getEmailPassword = false;
    EmailOTPVerify = false;
    EmailOTP:number;
    EmailExist = true;
    EmailOTPWrong= false;
    EmailPassword : string;
    EditEmail = false;
    UserEmail : string ="";
    UserMobile:number;
    getPassword = false;
    editDetails = true;
    editBank = false;
    OTPVerify = false;
    OTPWrong= false;
    size;
    height;
    width;
    previousName: any;
    previousDisplayName: any;
    previousGSTTIN: any;
    previousPAN: any;
    previousTAN: any; 
    detailshow: boolean;
    sameShop=false;
    submitenable=false;
    enablesave=false;
    editPhone = false;
    @ViewChild('GetBusinessDetailsForm') businessDetailsForm: HTMLFormElement;
    @ViewChild('bankform') bankDetailsForm: HTMLFormElement;

    constructor(private shopService: ShopService, private sellOnZgrooService: SellOnZgrooService,
        private registerService: RegisterService, private toasterPopService: NotifyService,
        private utilService: UtilService, private loaderComponent: NgxSpinnerService, private userService: UserService) {

        this.shopAccountModel = <any>{};
        this.openshopModel = <ViewModels.Shop.IOpenShopViewModel>{};
        this.shopModel = <ViewModels.Shop.IShopViewModel>{};
        this.businessdetailsModel = <any>{};
        this.bankdetailsModel = <ViewModels.Shop.IBankDetailsViewModel>{};
        this.profileModelAddress = <any>{};
        this.ShowUserAddress = <any>{};
        this.shopAccountModel.SameasShopAddress = false; 
        this.IFSC_code = "";
        this.BankDetails = {};
        this.attachments = [];
        this.getShopObj = {};
        this.responseDataModel = {};
        this.bankListArray = [];
        this.stateListArray = [];
        this.cityListArray = [];
        this.branchListArray = [];
        this.statesArray = []; 
        this.BankSearch = "";
        this.StateSearch = "";
        this.CitySearch = "";
        this.BranchSearch = "";
        this.IFSCResult = "";
        this.countryId = {};
        this.stateId = {};
        this.cityLists = [];
        this.IFSCsearch = true;
        this.loader = false;
        this.businessType = "";
        this.approved =  false;
        this.detailshow = false;
        this.progressbarvalue = 0;
        this.init();

    }

    init() {
        window.scrollTo(0, 20);
        this.progressbarvalue = 0;
        this.getBankList();
        this.countryId = { "CountryId": 101 }
        this.profileModelAddress.Country = "India";
        this.registerService.getStates(this.countryId).then(
            statesInfo => {
                this.statesArray = JSON.parse(statesInfo);
            }, error => this.errorMessage = <any>error);

    this.userService.showProfile().then(user =>{
            var res = JSON.parse(user);
            this.UserEmail=res.Items[0].Email;
            this.UserMobile=res.Items[0].Mobile;
            this.profileModelAddress.Phone =this.UserMobile;
            this.OldEmail=res.Items[0].Email;
        this.shopService.getShopID().then(
            response => {
                this.getShopObj = JSON.parse(response);
                if (this.getShopObj.hasOwnProperty('_id')) {
                    this.progressbarvalue = 0;
                    this.shopId = this.getShopObj._id;
                    if (this.getShopObj.editedBusinessDetails) {
                        // console.log("hasBusinessDetails");
                        this.previousName=this.getShopObj["editedBusinessDetails"].Name;
                        this.previousDisplayName=this.getShopObj["editedBusinessDetails"].DisplayName;

                        this.previousGSTTIN=this.getShopObj["editedBusinessDetails"].GstinProvisionalId;
                        this.previousPAN=this.getShopObj["editedBusinessDetails"].BusinessPanNumber;
                        this.previousTAN=this.getShopObj["editedBusinessDetails"].TanNumber;
                        this.businessdetailsModel = this.getShopObj["editedBusinessDetails"];
                        this.sameShop = this.shopAccountModel.SameasShopAddress = this.getShopObj.SameasShopAddress
                        if (this.businessdetailsModel.hasOwnProperty('Name') && this.businessdetailsModel.Name != "")
                        this.progressbarvalue += 4;
                        if (this.businessdetailsModel.hasOwnProperty('Type'))
                        this.progressbarvalue += 4;
                        if (this.businessdetailsModel.hasOwnProperty('GstinProvisionalId') && this.businessdetailsModel.GstinProvisionalId !="")
                        this.progressbarvalue += 4;
                        if (this.businessdetailsModel.hasOwnProperty('BusinessPanNumber') && this.businessdetailsModel.BusinessPanNumber !="")
                        this.progressbarvalue += 4;
                        if (this.businessdetailsModel.hasOwnProperty('Profile'))
                        {
                            if (this.businessdetailsModel.Profile.Address.hasOwnProperty('Phone') && this.businessdetailsModel.Profile.Address.Phone != null)
                            this.progressbarvalue += 4;
                            if (this.businessdetailsModel.Profile.Address.hasOwnProperty('Line1') && this.businessdetailsModel.Profile.Address.Line1 != "")
                            this.progressbarvalue += 4;
                            if (this.businessdetailsModel.Profile.Address.hasOwnProperty('Line2') && this.businessdetailsModel.Profile.Address.Line2 != "")
                            this.progressbarvalue += 4;
                            if (this.businessdetailsModel.Profile.Address.hasOwnProperty('State') && this.businessdetailsModel.Profile.Address.State != "")
                            this.progressbarvalue += 4;
                            if (this.businessdetailsModel.Profile.Address.hasOwnProperty('Zip') && this.businessdetailsModel.Profile.Address.Zip != "")
                            this.progressbarvalue += 4;
                            if (this.businessdetailsModel.Profile.Address.hasOwnProperty('City') && this.businessdetailsModel.Profile.Address.City != "")
                            this.progressbarvalue += 4;
                        }
                        if (this.businessdetailsModel.hasOwnProperty('ShipsFromAddress'))
                        {
                            if (this.businessdetailsModel.ShipsFromAddress.hasOwnProperty('Phone') && this.businessdetailsModel.ShipsFromAddress.Phone != null)
                            this.progressbarvalue += 4;
                            if (this.businessdetailsModel.ShipsFromAddress.hasOwnProperty('Line1') && this.businessdetailsModel.ShipsFromAddress.Line1 != "")
                            this.progressbarvalue += 4;
                            if (this.businessdetailsModel.ShipsFromAddress.hasOwnProperty('Line2') && this.businessdetailsModel.ShipsFromAddress.Line2 != "")
                            this.progressbarvalue += 4;
                            if (this.businessdetailsModel.ShipsFromAddress.hasOwnProperty('State') && this.businessdetailsModel.ShipsFromAddress.State != "")
                            this.progressbarvalue += 4;
                            if (this.businessdetailsModel.ShipsFromAddress.hasOwnProperty('City') && this.businessdetailsModel.ShipsFromAddress.City != "")
                            this.progressbarvalue += 4;
                            if (this.businessdetailsModel.ShipsFromAddress.hasOwnProperty('Zip') && this.businessdetailsModel.ShipsFromAddress.Zip != "")
                            this.progressbarvalue += 4;

                        }

                       
                        if (this.businessdetailsModel.hasOwnProperty('Signature'))
                            this.progressbarvalue += 4;
                        if (this.businessdetailsModel.hasOwnProperty('PanProof'))
                            this.progressbarvalue += 4;
                        if (this.businessdetailsModel.hasOwnProperty('AttachAddressProof'))
                            this.progressbarvalue += 4;
                        if (this.businessdetailsModel.hasOwnProperty('GstinProof'))
                            this.progressbarvalue += 4;
                        this.profileModel = this.businessdetailsModel["Profile"];
                        this.profileModelAddress = this.profileModel["Address"];
                        // this.stateChange(this.profileModelAddress.State);
                        this.profileModelAddress.sfLine1 = this.businessdetailsModel.ShipsFromAddress.Line1;
                        this.profileModelAddress.sfLine2 = this.businessdetailsModel.ShipsFromAddress.Line2;
                        this.profileModelAddress.sfState = this.businessdetailsModel.ShipsFromAddress.State;
                        // this.sfstateChange(this.profileModelAddress.sfState);
                        this.profileModelAddress.sfCity = this.businessdetailsModel.ShipsFromAddress.City;
                        this.profileModelAddress.sfPhone = this.businessdetailsModel.ShipsFromAddress.Phone;
                        this.profileModelAddress.sfZip = this.businessdetailsModel.ShipsFromAddress.Zip;
                        if(this.getShopObj.ShopApproved){
                            this.approved = true;
                        }else{
                            this.approved = false;
                        }
                        // this.progressbarvalue += 45;
                        // if (this.businessdetailsModel.hasOwnProperty('AttachAddressProof'))
                        //     this.progressbarvalue += 5;
                      
                    }
                    if (this.getShopObj.editedBankDetails) {
                        this.bankdetailsModel = this.getShopObj["editedBankDetails"];
                        if (this.bankdetailsModel.hasOwnProperty('IfscCode')) {
                            this.getBankDetails();
                            this.progressbarvalue += 8;
                        }
                        if (this.bankdetailsModel.hasOwnProperty('AccountHolderName')) 
                        this.progressbarvalue += 4;
                        if (this.bankdetailsModel.hasOwnProperty('AccountNumber')) 
                        this.progressbarvalue += 4;
                        if (this.bankdetailsModel.hasOwnProperty('CancelledCheckCopy')) 
                        this.progressbarvalue += 4;

                       
                    }
                    this.resetSubmitButton();

                }else{
                    this.detailshow = true;
                }
            }, error => this.errorMessage = <any>error);
        })



    }
    CheckBusinessDisplayName() {
        this.shopService.CheckBusinessDisplayName(this.businessdetailsModel.DisplayName).then(
            res => {
                var newError = <any>{};
                var oldError = this.businessDetailsForm.form.controls['Displayname'].errors;
                //console.log(obj);
                if (JSON.parse(res).found && this.previousDisplayName!=this.businessdetailsModel.Displayname) {
                    for (var key in oldError) {
                        // console.log(key);
                        // console.log(oldError[key]);
                        newError[key] = oldError[key]
                    }
                    newError.incorrect = true;
                    // console.log(newError);
                    this.businessDetailsForm.form.controls['Displayname'].setErrors(newError);
                }

                else {
                    if (oldError.incorrect)
                        oldError.incorrect = false;
                    this.businessDetailsForm.form.controls['Displayname'].setErrors(oldError);
                }
            }
        )
    }
    sameasShop(){
        if(!this.sameShop){
            this.shopAccountModel.SameasShopAddress = true; 
            this.profileModelAddress.sfLine1 = this.profileModelAddress.Line1;
            this.profileModelAddress.sfLine2 = this.profileModelAddress.Line2;
            this.profileModelAddress.sfState = this.profileModelAddress.State;
            // if(this.profileModelAddress.State!= ""){
            //     this.sfstateChange(this.profileModelAddress.sfState)
            // }
            this.profileModelAddress.sfCity = this.profileModelAddress.City;
            this.profileModelAddress.sfZip = this.profileModelAddress.Zip;
            this.profileModelAddress.sfPhone = this.profileModelAddress.Phone;
            this.profileModelAddress.sfCountry = "India";
            this.resetSubmitButton();
        }else{
            this.shopAccountModel.SameasShopAddress = false; 
            this.profileModelAddress.sfLine1 = "";
            this.profileModelAddress.sfLine2 = "";
            this.profileModelAddress.sfState = "";
            this.profileModelAddress.sfCity ="";
            this.profileModelAddress.sfZip = "";
            this.profileModelAddress.sfPhone = "";
            this.resetSubmitButton();
        }
    }

    selectBranch(event) {
        this.selectedBranchName = event;
        // console.log(this.selectedBranchName, this.selectedCityName, this.selectedBankName)
        this.cityListArray[this.selectedCityName].forEach((List: any) => {
            // console.log("entered")
            if (List.BRANCH == this.selectedBranchName && List.BANK == this.selectedBankName) {
                // console.log("Found");
                this.IFSCResult = List.IFSC;
                this.banksearchresult = "valid"
                this.disableSaveBank = false;
            }
        });
    }

    selectCity(event) {
        this.selectedCityName = event;
    }
    EmailChange(){
        jQuery('#Email1').removeClass('removeEmailpadding').addClass('addEmailpadding');
        this.getEmailPassword=false;
        this.EditEmail=true;
        if(this.UserEmail != "") this.OldEmail = this.UserEmail;
    }
    sendEmailOTP(){
        this.EmailOTPVerify = false;
        if(this.OldEmail == "" || this.OldEmail == this.UserEmail){
            this.toasterPopService.ErrorSnack( 'Email has not updated');
            jQuery('#Email1').removeClass('addEmailpadding').addClass('removeEmailpadding');
            this.EditEmail =false;
        }else if(this.UserEmail != null){
            jQuery('#Email1').removeClass('addEmailpadding').addClass('removeEmailpadding');
            this.EditEmail =false;
            this.OTPVerify = false;
            this.OTPWrong = false;
            this.EmailOTP = null;
            this.EmailPassword = "";
            this.registerService.SendEmailOTP(this.UserEmail).then(data=>{
                var dat = JSON.parse(data);
                if(dat.mes){
                    this.toasterPopService.SuccessSnack( 'OTP has been sent to your Email');
                    this.getEmailPassword=true;
                }else{
                    jQuery('#Email1').removeClass('addEmailpadding').addClass('removeEmailpadding');
                    this.EmailExist=false;
                    this.toasterPopService.ErrorSnack( dat.message);

                }
            });

        }else{
            jQuery('#Email1').removeClass('addEmailpadding').addClass('removeEmailpadding');
            this.toasterPopService.ErrorSnack( 'Something Went wrong');
        }
    }
    EmailOTPCheck(){
        this.EmailOTPWrong = false;
        this.EmailOTPVerify = false;
        if(this.EmailOTP!=null)
        {
            var numb= this.EmailOTP.toString().length;
        }
        else{
            var numb=0; 
        }
       
        var model = {
            Email:this.UserEmail,
            OTP:this.EmailOTP
        }
        if(numb == 6){
            this.registerService.EmailOTPCheck(model).then(result=>{
                var res=JSON.parse(result);
                if(res.mes){
                    this.EmailOTPWrong = false;
                    this.EmailOTPVerify=true;
                }else{
                    this.EmailOTPWrong = true;
                    if(res.message != "Wrong OTP"){
                        this.toasterPopService.ErrorSnack( res.message);
                    }
                }
            });
        }
    }

    SaveEmail(){
        var model = {
            Email:this.UserEmail,
            Password:this.EmailPassword,
            OTP: this.EmailOTP
        }
        this.registerService.SaveEmail(model).then(result=>{
            var res=JSON.parse(result);
            if(res.mes){
                this.EmailOTP = null;
                this.getEmailPassword = false;
                this.EmailPassword = "";
                this.EditEmail= false;
                this.OldEmail = this.UserEmail;
                this.EmailOTPVerify=false;
                this.EmailOTPWrong= false;
                this.toasterPopService.SuccessSnack( 'Email has been updated Successfully');
            }else{
                this.toasterPopService.ErrorSnack(res.message);
            }
        });
    }

    EmailExistreset(){
        this.EmailExist = true;
    }
    selectState(event) {
        this.state = event
        this.sellOnZgrooService.DistrictList(this.state).then(
            Info => {
                this.branchListArray = Info.data;
                let sortlistArray = [];
                this.branchListArray.forEach((List: any) => {
                    if (List.BANK == this.selectedBankName) {
                        sortlistArray.push(List)
                    }
                });
                sortlistArray.forEach((List: any) => {

                    if (!this.cityListArray[List.CITY]) {
                        this.cityLists.push(List.CITY);
                        this.cityListArray[List.CITY] = [];
                    }
                    this.cityListArray[List.CITY].push(List);
                });
            }, error => this.errorMessage = <any>error);
    }
    
    getBankDetails() {
        if (this.bankdetailsModel.IfscCode.length == 11) {
            this.sellOnZgrooService.IFSCcodeToBankDetails(this.bankdetailsModel.IfscCode)
                .then(resdata => {
                    // console.log(resdata);
                    resdata=JSON.parse(resdata);
                        this.BankDetails = resdata;
                        //this.bankdetailsModel.IfscCode = this.BankDetails.Bank;
                        this.bankdetailsModel.State = this.BankDetails.STATE;
                        this.bankdetailsModel.City = this.BankDetails.CITY;
                        this.bankdetailsModel.BankName = this.BankDetails.BANK;
                        this.bankdetailsModel.BranchName = this.BankDetails.BRANCH;
                        this.banksearchresult = "valid";
                        this.disableSaveBank = false;
                        // console.log(this.BankDetails);
                    
                }
                ,error=>{
                    this.banksearchresult = "Please enter correct IFSC code and click search";
                    this.disableSaveBank = true;
                });
        }

    }

    ResetbankDetails() {
        this.bankdetailsModel.State = "";
        this.bankdetailsModel.City = "";
        this.bankdetailsModel.BankName = "";
        this.bankdetailsModel.BranchName = "";
        this.banksearchresult = "Please enter correct IFSC code and click search";
        this.disableSaveBank = true;
        this.editDetails = false;
        this.editBank = true;
    }

    getBankList() {
        this.sellOnZgrooService.BankSearch()
            .then(resdata => {
                // console.log(resdata);
                this.bankListArray = resdata.data;
                // console.log(this.bankListArray);

            });
    }

    selectBank(value) {
        this.selectedBankName = value;
        // console.log(this.selectedBankName)

    }

    ValidateCouriers(zip, paymentMode): Q.Promise<any> {
        var defer = Q.defer();
        this.loaderComponent.show();
        this.utilService.ValidateCouriers(zip, paymentMode).then(res => {
            this.loaderComponent.hide();
            var response = JSON.parse(res);
            if (response.status == true)
                defer.resolve({ status: 'success' });
            else
                defer.resolve({ status: 'failure' })
        }, err => {
            this.loaderComponent.hide();
            this.toasterPopService.ErrorSnack("Something went wrong, please try again after some time");
        })

        return defer.promise;
    }

    UpdatePhoneNumber(){
        var model = {
            Phone : this.profileModelAddress.Phone,
            PhoneSf : this.profileModelAddress.sfPhone
        }
        this.shopService.updateBusinessPhoneNumber(this.shopId,model).then(result=>{
            var res= JSON.parse(result)
            if(res.flag == false){
                this.toasterPopService.ErrorSnack(res.message)
            }else{
                this.toasterPopService.SuccessSnack("Mobile Number Updated Successfully");
                this.init();
                this.disableSaveBank = false;
                this.editDetails = true;
                this.editBank = false;
            }
           
        })
    }


    getBusinessDetails(buttontype) {
        if(buttontype == "submit"){
            this.shopAccountModel.SubmitBusiness = true;
        }else{
            this.shopAccountModel.SubmitBusiness = false;
        }
        if (this.getShopObj.hasOwnProperty('_id')) {
                    var Address = { Address: this.profileModelAddress };
                    this.businessdetailsModel.Profile = Address;
                    this.businessdetailsModel.ShipsFromAddress = {
                        Line1:this.profileModelAddress.sfLine1,
                        Line2:this.profileModelAddress.sfLine2,
                        State:this.profileModelAddress.sfState,
                        City:this.profileModelAddress.sfCity,
                        Zip:this.profileModelAddress.sfZip,
                        Phone:this.profileModelAddress.sfPhone
                    }
    
                    this.shopAccountModel.editedBusinessDetails = this.businessdetailsModel
                    this.shopAccountModel.isEditedBusinessDetails = true;
                    this.openshopModel["ShopAccount"] = this.shopAccountModel;
                    this.shopId = this.getShopObj._id;
                    if (this.shopAccountModel.hasOwnProperty('BankDetails'))
                        delete this.shopAccountModel.BankDetails;
                    this.shopService.putShopDetailBusiness(this.shopId, this.shopAccountModel)
                        .then(resdata => {
                            var res = JSON.parse(resdata);
                            if(res.flag == false){
                                this.toasterPopService.ErrorSnack(res.message)
                            }else{
                                this.toasterPopService.SuccessSnack("Business Details Saved Updated Successfully");
                                this.progressbarvalue=0;
                                this.init();
                                jQuery("#addBusinessDetails").modal("hide");
                                this.disableSaveBank = false;
                                this.editDetails = true;
                                this.editBank = false;
                            }
                           
                        });
    
        }
        else {
                    var Address = { Address: this.profileModelAddress };
                    this.businessdetailsModel.Profile = Address;
                    this.businessdetailsModel.ShipsFromAddress = {
                        Line1:this.profileModelAddress.sfLine1,
                        Line2:this.profileModelAddress.sfLine2,
                        State:this.profileModelAddress.sfState,
                        City:this.profileModelAddress.sfCity,
                        Zip:this.profileModelAddress.sfZip,
                        Phone:this.profileModelAddress.sfPhone
                    }
                    this.shopAccountModel.isEditedBusinessDetails = true;
                    this.shopAccountModel.editedBusinessDetails = this.businessdetailsModel;
                    this.openshopModel["Domain"] = "in";
                    this.openshopModel["ShopAccount"] = this.shopAccountModel;
                   
                    this.shopService.shopDetails(this.openshopModel)
                        .then(resdata => {
                            var result = JSON.parse(resdata);
                            this.getShopObj == JSON.parse(resdata);
                            this.progressbarvalue=0;
                            this.shopId = JSON.parse(resdata)._id;
                            if(result.flag == false){
                                this.toasterPopService.ErrorSnack(result.message)
                            }else{
                                // console.log("Shop ID from Business Details", this.shopId);
                                this.toasterPopService.SuccessSnack("Business Details Saved Successfully");
                                // console.log(resdata);
                                this.progressbarvalue=0;
                                this.init();
                                jQuery("#addBusinessDetails").modal("hide");
                                this.editDetails = true;
                            }
                           
                        });

        }
    }

    // sfstateChange(state){
    //     this.loaderComponent.show();
    //     var obj={StateId:state}
    //     this.registerService.getCities(obj).then(
    //         citiesInfo => {
    //              this.loaderComponent.hide();
    //              this.sfcitiesArray = JSON.parse(citiesInfo);
    //            }, error => this.errorMessage = <any>error);
    // }

    triggerFileUpload() {
        jQuery('#pan_proof').trigger('click');
    }

    // stateChange(state){
    //     this.loaderComponent.show();
    //     var obj={StateId:state}
    //     this.registerService.getCities(obj).then(
    //         citiesInfo => {
    //              this.loaderComponent.hide();
    //              this.citiesArray = JSON.parse(citiesInfo);
    //            }, error => this.errorMessage = <any>error);
    // }

    getSellerBankDetails() {
        if (this.getShopObj.hasOwnProperty('_id')) {
            if (this.IFSCsearch) {
                // this.bankdetailsModel["IfscCode"]=this.IFSC_code;
                // this.bankdetailsModel["BankName"]=this.BankDetails.BANK;
                // this.bankdetailsModel["State"]=this.BankDetails.STATE;
                // this.bankdetailsModel["City"]=this.BankDetails.CITY;
                // this.bankdetailsModel["District"]=this.BankDetails.DISTRICT;
                // this.bankdetailsModel["BranchName"]=this.BankDetails.BRANCH;
                // let tempArray = [];
                // tempArray.push(this.bankdetailsModel);
                this.shopAccountModel["editedBankDetails"] = this.bankdetailsModel;
                // this.openshopModel["Shop"]={"Currency":"INR"};;
                this.shopAccountModel["isEditedBankDetails"] = true;
                this.openshopModel["ShopAccount"] = this.shopAccountModel;
            }
            else {
                this.bankdetailsModel["IfscCode"] = this.IFSCResult;
                this.bankdetailsModel["BankName"] = this.selectedBankName;
                this.bankdetailsModel["State"] = this.state;
                this.bankdetailsModel["City"] = this.selectedCityName;
                this.bankdetailsModel["BranchName"] = this.selectedBranchName;
                // let tempArray = [];
                // tempArray.push(this.bankdetailsModel);
                this.shopAccountModel["editedBankDetails"] = this.bankdetailsModel;
                //this.openshopModel["Shop"]={"Currency":"INR"};;
                this.openshopModel["ShopAccount"] = this.shopAccountModel;
            }


            // this.shopService.CheckAccount(this.bankdetailsModel.AccountNumber, this.bankdetailsModel.IfscCode).then(
            //     res =>{

            //         //console.log(obj);
            //         if(JSON.parse(res).found)
            //         {
            //             this.accountIFSCCheck = true;
            //             this.toasterPopService.ErrorSnack("Account Number and IFSC code already exist");
            //             return;
            //         }

            //         else
            //         {
            this.accountIFSCCheck = false;
            // console.log("this.bankdetailsModel Save", this.shopAccountModel);
            if (this.shopAccountModel.hasOwnProperty('editedBusinessDetails'))
                delete this.shopAccountModel.editedBusinessDetails;
            this.shopId = this.getShopObj._id;
            this.shopService.putShopDetailBank(this.shopId, this.shopAccountModel)
                .then(resdata => {


                    //console.log(obj);
                    if (JSON.parse(resdata).found) {
                        this.accountIFSCCheck = true;
                        this.toasterPopService.ErrorSnack("Account Number and IFSC code already exist");
                        return;
                    }

                    else {
                        this.shopId = JSON.parse(resdata)._id;
                        console.log(resdata);
                        this.toasterPopService.SuccessSnack("Bank Details Updated Successfully");
                        jQuery("#addBankDetails").modal("hide");
                        this.init();
                        this.detailshow = true;
                        this.disableSaveBank = false;
                        this.editDetails = true;
                        this.editBank = false;
                    }


                    // }

                }
                )


        }
        else {
            if (this.IFSCsearch) {
                // this.bankdetailsModel["IfscCode"]=this.IFSC_code;
                // this.bankdetailsModel["BankName"]=this.BankDetails.BANK;
                // this.bankdetailsModel["State"]=this.BankDetails.STATE;
                // this.bankdetailsModel["City"]=this.BankDetails.CITY;
                // this.bankdetailsModel["District"]=this.BankDetails.DISTRICT;
                // this.bankdetailsModel["BranchName"]=this.BankDetails.BRANCH;
                // let tempArray = [];
                // tempArray.push(this.bankdetailsModel);
                this.shopAccountModel["Domain"] = "in";
                this.shopAccountModel["editedBankDetails"] = this.bankdetailsModel;
                //this.openshopModel["Shop"]={"Currency":"INR"};;
                this.openshopModel["ShopAccount"] = this.shopAccountModel;
            }
            else {
                this.bankdetailsModel["IfscCode"] = this.IFSCResult;
                this.bankdetailsModel["BankName"] = this.selectedBankName;
                this.bankdetailsModel["State"] = this.state;
                this.bankdetailsModel["City"] = this.selectedCityName;
                this.bankdetailsModel["BranchName"] = this.selectedBranchName;
                // let tempArray = [];
                // tempArray.push(this.bankdetailsModel);
                this.shopAccountModel["editedBankDetails"] = this.bankdetailsModel;
                //  this.openshopModel["Shop"]={"Currency":"INR"};;
                this.openshopModel["Domain"] = "in";
                this.openshopModel["ShopAccount"] = this.shopAccountModel;
            }

            this.shopService.CheckAccount(this.bankdetailsModel.AccountNumber, this.bankdetailsModel.IfscCode).then(
                res => {

                    //console.log(obj);
                    if (JSON.parse(res).found) {
                        this.accountIFSCCheck = true;
                        this.toasterPopService.ErrorSnack("Account Number and IFSC code already exist");
                        return;
                    }

                    else {
                        this.accountIFSCCheck = false;
                        // console.log("this.bankdetailsModel Update", this.openshopModel);
                        this.shopService.shopDetails(this.openshopModel)
                            .then(resdata => {
                                this.getShopObj == JSON.parse(resdata);
                                this.shopId = JSON.parse(resdata)._id;
                                this.toasterPopService.SuccessSnack("Business Details Saved Successfully");
                                jQuery("#addBankDetails").modal("hide");
                                console.log(resdata);
                                this.init();
                                this.detailshow = true;
                            });
                    }

                }
            )



        }
    }

    ChangeListener($event, Name) {

        //    this.loader=true;
        this.loaderComponent.show();
        // console.log("change listenser called ", $event)
        let files = [].slice.call($event.target.files);
        var file: File = files[0];
        // console.log('file', file);
        if(!file){
            this.loaderComponent.hide()
            return
        }
        this.size = file.size;
        var fSExt = new Array('Bytes', 'KB', 'MB', 'GB'),
        i=0;while(this.size>900){this.size/=1024;i++;}
        var exactSize = (Math.round(this.size*100)/100)+' '+fSExt[i];
        // console.log(exactSize);
        var ext = file.name.split(".")[1];
        if (ext == "jpg" || ext == "png" || ext == "jpeg" || ext == "JPG" || ext == "JPEG" || ext == "PNG" || ext == "docx" || ext == "pdf") {
            if(ext == "jpg" || ext == "png" || ext == "JPG" || ext == "PNG"){
                var myReader: FileReader = new FileReader();
                myReader.readAsDataURL(file);
                myReader.onloadend = (e: any) => {
                    var i = new Image();
                    i.src = e.target.result;
                    var self = this;
                    i.onload = function () {
                        
                            if (self.size < 5e+6) {
                                let fileName = file.name.split(".")[0];
                                let ext = file.name.split(".")[1]
                                let data = myReader.result.toString().split(',')[1]
                                self.fileupload(fileName,ext,data,Name,file);
                                
                            } else {
                                self.loaderComponent.hide();
                                
                                self.toasterPopService.ErrorSnack("Kindly check your file size and upload again");
                            }  
                        
                       
                        //    console.log("From Service ,",i.width,i.height)
                    }
                }
            }
            else{
                if(this.size < 5e+6){
                    var myReader: FileReader = new FileReader();
                    myReader.readAsDataURL(file);
                    myReader.onloadend = (e: any) => {
                        let fileName = file.name.split(".")[0];
                        let ext = file.name.split(".")[1]
                        let data = myReader.result.toString().split(',')[1]
                        this.fileupload(fileName,ext,data,Name,file);
                    }
                    
                }else{
                    this.loaderComponent.hide();
                    this.toasterPopService.ErrorSnack("Kindly check your file size and upload again");
                }
            }
            
        } else if (ext == "pdf" || ext == "docx") {
            let reader = new FileReader();
            if ($event.target.files && $event.target.files.length > 0) {
                let file = $event.target.files[0];
                reader.readAsDataURL(file);
                reader.onload = () => {
                    let fileName = file.name.split(".")[0];
                    let ext = file.name.split(".")[1]
                    let data = reader.result.toString().split(',')[1]
                    // console.log("data " + data, "name " + fileName, "ext " + ext);
                    this.fileupload(fileName,ext,data,Name,file);
                };
            }
        } else {
            this.loaderComponent.hide();
            this.toasterPopService.ErrorSnack("Kindly upload proper format");
        }
    }

    fileupload(fileName, ext, data, Name,file) {
        // this.loaderComponent.show();
        this.utilService.uploadBase64AndGetUrlFile(fileName, ext, data,file).then(res => {
            // console.log("response " + res)
            var result = JSON.parse(res)
            this.loaderComponent.hide();
            if(result.Output){
                if (Name == "CancelledCheckCopy") {
                
                    this.bankdetailsModel[Name] = JSON.parse(res).ImageData;
                    this.resetSubmitButton();
                    this.resetSaveButton();
                }
                else {
                   
                    this.businessdetailsModel[Name] = JSON.parse(res).ImageData;
                    this.resetSubmitButton();
                    this.resetSaveButton();
                }
            }else{
                this.resetSubmitButton();
                this.toasterPopService.ErrorSnack('Something went wrong')
            }

        })
    }

    validateFileSize(file) {
        var myReader: FileReader = new FileReader();
        myReader.readAsDataURL(file);
        myReader.onloadend = (e: any) => {
            var i = new Image();
            i.src = e.target.result;
            // var self = this;
            return i.onload = function () {
                if (i.width < 500 && i.width > 100 && i.height < 500 && i.width > 100) {
                    return 1
                } else {
                    return 0
                }
                //    console.log("From Service ,",i.width,i.height)
            }
        }

    }

    CheckBusinessName() {
        this.shopService.CheckBusinessName(this.businessdetailsModel.Name).then(
            res => {
                var newError = <any>{};
                var oldError = this.businessDetailsForm.form.controls['BusinessName'].errors;
                //console.log(obj);
                if (JSON.parse(res).found && this.previousName!=this.businessdetailsModel.Name) {
                    for (var key in oldError) {
                        // console.log(key);
                        // console.log(oldError[key]);
                        newError[key] = oldError[key]
                    }
                    newError.incorrect = true;
                    // console.log(newError);
                    this.businessDetailsForm.form.controls['BusinessName'].setErrors(newError);
                }

                else {
                    if (oldError.incorrect)
                        oldError.incorrect = false;
                    this.businessDetailsForm.form.controls['BusinessName'].setErrors(oldError);
                }
            }
        )
    }
    CheckGSTIN() {
        this.shopService.CheckGSTIN(this.businessdetailsModel.GstinProvisionalId).then(
            res => {
                var newError = <any>{};
                var oldError = this.businessDetailsForm.form.controls['GSTINProvisionalID'].errors;
                //console.log(obj);
                if (JSON.parse(res).found && this.previousGSTTIN!=this.businessdetailsModel.GstinProvisionalId) {
                    for (var key in oldError) {
                        console.log(key);
                        console.log(oldError[key]);
                        newError[key] = oldError[key]
                    }
                    newError.incorrect = true;
                    // console.log(newError);
                    this.businessDetailsForm.form.controls['GSTINProvisionalID'].setErrors(newError);
                }

                else {
                    if (oldError.incorrect)
                        oldError.incorrect = false;
                    this.businessDetailsForm.form.controls['GSTINProvisionalID'].setErrors(oldError);
                }
            }
        )
    }
    CheckTAN() {
        this.shopService.CheckTAN(this.businessdetailsModel.TanNumber).then(
            res => {
                var newError = <any>{};
                var oldError = this.businessDetailsForm.form.controls['TAN'].errors;
                //console.log(obj);
                if (JSON.parse(res).found && this.previousTAN!=this.businessdetailsModel.TanNumber) {
                    for (var key in oldError) {
                        // console.log(key);
                        // console.log(oldError[key]);
                        newError[key] = oldError[key]
                    }
                    newError.incorrect = true;
                    // console.log(newError);
                    this.businessDetailsForm.form.controls['TAN'].setErrors(newError);
                }
                else {
                    if (oldError.incorrect)
                        oldError.incorrect = false;
                    this.businessDetailsForm.form.controls['TAN'].setErrors(oldError);
                }
            }
        )
    }
    CheckPAN() {
        this.shopService.CheckPAN(this.businessdetailsModel.BusinessPanNumber).then(
            res => {
                var newError = <any>{};
                var oldError = this.businessDetailsForm.form.controls['Bpan'].errors;
                //console.log(obj);
                if (JSON.parse(res).found && this.previousPAN!=this.businessdetailsModel.BusinessPanNumber) {
                    for (var key in oldError) {
                        // console.log(key);
                        // console.log(oldError[key]);
                        newError[key] = oldError[key]
                    }
                    newError.incorrect = true;
                    // console.log(newError);
                    this.businessDetailsForm.form.controls['Bpan'].setErrors(newError);
                }

                else {
                    if (oldError.incorrect)
                        oldError.incorrect = false;
                    this.businessDetailsForm.form.controls['Bpan'].setErrors(oldError);
                }
            }
        )
    }
    CheckPersonalPAN() {
        this.shopService.CheckPersonalPAN(this.businessdetailsModel.PersonalPanNumber).then(
            res => {
                var newError = <any>{};
                var oldError = this.businessDetailsForm.form.controls['pan'].errors;
                //console.log(obj);
                if (JSON.parse(res).found) {
                    for (var key in oldError) {
                        // console.log(key);
                        // console.log(oldError[key]);
                        newError[key] = oldError[key]
                    }
                    newError.incorrect = true;
                    // console.log(newError);
                    this.businessDetailsForm.form.controls['pan'].setErrors(newError);
                }

                else {
                    if (oldError.incorrect)
                        oldError.incorrect = false;
                    this.businessDetailsForm.form.controls['pan'].setErrors(oldError);
                }
            }
        )
    }
    CheckPhone() {
        this.shopService.CheckPhone(this.profileModelAddress.Phone).then(
            res => {
                var newError = <any>{};
                var oldError = this.businessDetailsForm.form.contprols['Phone'].errors;
                //console.log(obj);
                if (JSON.parse(res).found) {
                    for (var key in oldError) {
                        // console.log(key);
                        // console.log(oldError[key]);
                        newError[key] = oldError[key]
                    }
                    newError.incorrect = true;
                    // console.log(newError);
                    this.businessDetailsForm.form.controls['Phone'].setErrors(newError);
                }

                else {
                    if (oldError.incorrect)
                        oldError.incorrect = false;
                    this.businessDetailsForm.form.controls['Phone'].setErrors(oldError);
                }

            }
        )
    }

    CheckAccountNumber() {
        var newError = <any>{};
        this.disableSaveBank = false;
        var oldError = this.bankDetailsForm.form.controls['ConfirmAccountNumber'].errors;
        if (this.bankdetailsModel.ConfirmAccountNumber != this.bankdetailsModel.AccountNumber) {
            this.disableSaveBank = true;
            for (var key in oldError) {
                // console.log(key);
                // console.log(oldError[key]);
                newError[key] = oldError[key]
            }
            newError.incorrect = true;
            // console.log(newError);
            this.bankDetailsForm.form.controls['ConfirmAccountNumber'].setErrors(newError);
        }

        else {
            if (oldError.incorrect)
            oldError.incorrect = false;
            this.disableSaveBank = false;
            this.bankDetailsForm.form.controls['ConfirmAccountNumber'].setErrors(oldError)
        }
    }
    goBack() {
        this.editDetails = true;
        this.editBank = false;
    }
    resetSubmitButton()
    {
      
        if(this.businessdetailsModel.hasOwnProperty("GstinProof") && this.businessdetailsModel.hasOwnProperty("PanProof") && this.businessdetailsModel.hasOwnProperty("Signature") && this.businessdetailsModel.hasOwnProperty("AttachAddressProof"))
        {
            this.submitenable=true;
        }
        
    }
    resetSaveButton()
    {
      
        if(this.bankdetailsModel.hasOwnProperty("CancelledCheckCopy"))
        {
            this.enablesave=true;
            console.log("Testing....",this.enablesave);
        }
        
    }


}