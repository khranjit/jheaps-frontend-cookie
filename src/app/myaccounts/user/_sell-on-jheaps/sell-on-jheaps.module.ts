import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SellOnJheapsComponent } from './sell-on-jheaps.component';
import { SharedModule } from '../../../shared/shared.module';
import {NgPipesModule} from 'ngx-pipes';
import { MatInputModule } from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';


  
const sell: Routes = [
    {path: '',component:SellOnJheapsComponent}
]
@NgModule({
  imports: [
    SharedModule,
    NgPipesModule,
    RouterModule.forChild(sell),
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule
  ],
  declarations: [
    SellOnJheapsComponent
  ],
})
export class SellOnJheapsModule { }