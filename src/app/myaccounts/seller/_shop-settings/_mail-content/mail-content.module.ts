import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { MailContentComponent } from './mail-content.component';
import { Routes, RouterModule } from '@angular/router';


const mailcontent: Routes = [
  {path: '', component:MailContentComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(mailcontent)
  ],
  declarations: [
    MailContentComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class MailContentModule { }