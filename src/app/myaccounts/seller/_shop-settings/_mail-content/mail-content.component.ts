import { Component,Inject} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ShopSettingService } from '../../../../services/shop-settings.service';
import { NotifyService } from '../../../../services/notify.service';

@Component({ 
  
  selector: 'mailcontent',
  providers:[ShopSettingService,NotifyService],
  templateUrl: './mail-content.component.html'
})

export class MailContentComponent{
  public shopSettingModel:ViewModels.Shop.IShopSettingsViewModel;
  errorMessage:any;

  constructor(private route: ActivatedRoute, private toasterPopService:NotifyService,private router: Router,private shopsettingService:ShopSettingService) {
  this.Init();
  }
  public Init() {
    this.shopSettingModel = <ViewModels.Shop.IShopSettingsViewModel>{};
    this.shopSettingModel.Messages = <ViewModels.Shop.IMessageViewModel>{
      MessagesToBuyer:'',
      ShopAnouncement:'',
      // MessagesToDigitalProductsBuyers:'',
      // ReceiptInfo:''
    };
    this.shopsettingService.getAll().then(res=>{
      var data:any = JSON.parse(res);
      if(data.Items.length > 0)
      {
        data = data.Items[0];
      }
      else
      {
        data ={};
      }
      if(data.hasOwnProperty('Messages'))
      {
        this.shopSettingModel = data;
        this.shopSettingModel.Messages = <ViewModels.Shop.IMessageViewModel>{
          MessagesToBuyer:data.Messages.MessagesToBuyer?data.Messages.MessagesToBuyer:'',
          ShopAnouncement:data.Messages.ShopAnouncement?data.Messages.ShopAnouncement:'',
          // MessagesToDigitalProductsBuyers:data.Messages.MessagesToDigitalProductsBuyers?data.Messages.MessagesToDigitalProductsBuyers:'',
          // ReceiptInfo:data.Messages.ReceiptInfo?data.Messages.ReceiptInfo:''
        };
      }
      else{
        this.shopSettingModel = data;
        this.shopSettingModel.Messages = <ViewModels.Shop.IMessageViewModel>{
          MessagesToBuyer:'',
          ShopAnouncement:'',
          // MessagesToDigitalProductsBuyers:'',
          // ReceiptInfo:''
        };
      }

      console.log(this.shopSettingModel.Messages);
    }, error=> this.errorMessage = <any>error);
  }
  saveMessageContent(){
    
    this.shopsettingService.putShopSettings(this.shopSettingModel).then(
      x=> this.toasterPopService.SuccessSnack('Message saved succesfully'),
      error=>this.toasterPopService.ErrorSnack('Unable to save message')
  );
    // this.shopSettingModel.Messages.MessagesToBuyer='';
    // this.shopSettingModel.Messages.ShopAnouncement='';
    // this.shopSettingModel.Messages.MessagesToDigitalProductsBuyers='';
    // this.shopSettingModel.Messages.ReceiptInfo='';
  }
}