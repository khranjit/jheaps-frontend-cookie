import { Component, Inject} from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ShippingProfileService } from '../../../../services/shipping-profile.service';

declare var jQuery: any;

@Component({
    
    selector: 'shippingprofile',
    providers: [ShippingProfileService],
    templateUrl: './shipping-profile.component.html'
})

export class ShippingProfileComponent {
    public ShippingProfilesList: any;
    public ShippingProfiles: any;
    public errorMessage: any;
    public deleteprofileId: any;
    deleteModel:any;
    deleteModelBody:any;
    pageLoader = false;
    constructor(private route: ActivatedRoute, private router: Router, private shippingprofileService: ShippingProfileService) {
        this.ShippingProfiles=[];
        this.Init();
    }

    public Init() {
         window.scrollTo(0,20); 
         this.deleteModel = false;
         this.deleteModelBody = 0;
         this.pageLoader = true;
        this.shippingprofileService.getAll()
            .then(
            shippingprofiles => {
                this.ShippingProfilesList = JSON.parse(shippingprofiles);
                this.pageLoader = false;
                console.log(" this.ShippingProfilesList", this.ShippingProfilesList);
                this.ShippingProfiles = this.ShippingProfilesList.Profiles;
                console.log(this.ShippingProfiles);
            },
            error => this.errorMessage = <any>error);
    }


    edit(shippingprofile) {
        let navimodel: NavigationExtras = {
            queryParams: { 'ShippingProfile': JSON.stringify(shippingprofile) }
        }
        this.router.navigate(['/myaccounts/seller/shop-settings/list/shipping-profile/add'], navimodel);
        console.log(shippingprofile)
    }
    confirmdelete(shippingprofile){
         console.log("Delete Shipping id", shippingprofile);
         
         this.deleteprofileId=shippingprofile;
         this.deleteModel = false;
         this.deleteModelBody = 0;
         this.shippingprofileService.checkShippingProfile(shippingprofile).then(res=> {
           var count = JSON.parse(res).count;
           if(count == 0)
           {
             this.deleteModel = false;
             this.deleteModelBody = 0;
           }
           else
           {
             this.deleteModel = true;
             this.deleteModelBody = count;
             
           }
     
           jQuery('#deleteModal').modal("show");
         }, 
           error=> this.errorMessage = <any>error);
    }
    delete() {
        console.log("Delete Shipping id",  this.deleteprofileId)
       this.shippingprofileService.removeShippingProfile(this.deleteprofileId).then(response => {
            this.Init();
        });
         jQuery('#deleteModal').modal("hide");
    }

    addNew() {
        this.router.navigate(['/myaccounts/seller/shop-settings/list/shipping-profile/add']);
    }
}