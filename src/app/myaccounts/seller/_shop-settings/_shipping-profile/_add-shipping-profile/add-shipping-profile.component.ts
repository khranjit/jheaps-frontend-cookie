import { Component, Inject, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ShippingProfileService } from '../../../../../services/shipping-profile.service';
import { UtilService } from '../../../../../services/util.service';
import { ZoneService } from '../../../../../services/zone.service';
import { ShopService } from '../../../../../services/shop.service';
import { NotifyService } from '../../../../../services/notify.service';

declare var jQuery: any;

@Component({

    selector: 'addshippingprofile',
    //changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [ShippingProfileService, UtilService, ZoneService, ShopService, NotifyService],
    templateUrl: './add-shipping-profile.component.html'
})

export class AddShippingProfileComponent implements OnInit {
    public shippingprofileModel: ViewModels.IShippingProfileViewModel;
    public shippingprofileModelCustom: ViewModels.IShippingProfileViewModel;
    shipsToCountry = [];
    public DataSepZone = [];
    public LoopZones: any = [];
    public productModel: any;
    public ShippingProfilesList: any;
    public ShippingProfiles = [];
    private errorMessage: any;
    methodModel = <ViewModels.IShippingMethodViewModel>{};
    detailModel = <ViewModels.IShippingDetailViewModel>{};
    shipsToModel = <ViewModels.IShipsToViewModel>{};
    edit: Boolean = false;


    constructor(private route: ActivatedRoute, private router: Router, private shopService: ShopService,
        private shippingProfileService: ShippingProfileService, private zoneService: ZoneService, private toaster: NotifyService, private toasterService: NotifyService,
        private utilService: UtilService) {
        //this.init();
        this.Init();
        // this.getAllCountries();

    }
    public Init() {
        this.shippingprofileModel = <ViewModels.IShippingProfileViewModel>{};
        this.shippingprofileModelCustom = <ViewModels.IShippingProfileViewModel>{};
        this.shippingprofileModelCustom.IsCustom = true;
        this.shippingprofileModelCustom.ShippingCountries = [];
        this.shippingprofileModelCustom.Detail = <ViewModels.IShippingDetailViewModel[]>[];
        this.route
            .queryParams
            .subscribe(params => {
                if (params['ShippingProfile']) {
                    this.edit = true;
                    this.shippingprofileModelCustom = JSON.parse(params['ShippingProfile']);
                    this.methodModel = this.shippingprofileModelCustom.Detail[0].ShippingMethod[0];
                    this.shipsToModel = this.shippingprofileModelCustom.Detail[0].ShippingMethod[0].ShipsTo[0];
                }
                else {
                    this.edit = false;
                    this.ShipsToChanged();
                }
            })
    }

    ngOnInit() {

    }

    ShipsToChanged() {
        console.log(event);
        //this.shipsTo = event;
        //console.log(this.shipsTo);

        this.shipsToCountry.push("India");


        if (this.shipsToCountry != null) {
            if (this.shipsToCountry.length > this.shippingprofileModelCustom.ShippingCountries.length) {
                var country;
                if (this.shippingprofileModelCustom.ShippingCountries.length > 0) {
                    country = this.findItem(this.shippingprofileModelCustom.ShippingCountries, this.shipsToCountry);

                }
                else {
                    country = this.shipsToCountry[0];

                }
                this.zoneService.CheckMultiCountry(this.shipsToCountry)
                    .then(result => {
                        var ResData = JSON.parse(result);
                        this.DataSepZone = ResData.Items;
                        console.log('multicountry', this.DataSepZone);
                        this.DataSepZone.forEach(ele => {
                            this.LoopZones.push(ele.Zone);
                        });
                        console.log('loopzones', this.LoopZones);
                    });
                this.detailModel.Country = country;
                this.methodModel.Name = "";
                this.methodModel.ProcessingTime = "";
                this.methodModel.Default = true;
                this.methodModel.ShipsTo = [];
                this.shipsToModel.Zone = "";
                this.shipsToModel.CostWithAnotherItem = 0;
                this.shipsToModel.Cost = 0;
                this.detailModel.ShippingMethod = [];
                this.shipsToModel.FreeShippingTotalValue = 0;
                this.methodModel.ShipsTo.push(this.shipsToModel);
                this.detailModel.ShippingMethod.push(this.methodModel);
                //console.log("shiping changed" + JSON.stringify(detailModel)); 
                this.shippingprofileModelCustom.Detail.push(this.detailModel);
                //console.log("shiping changed" + JSON.stringify(this.shippingprofileModel));
                this.shippingprofileModelCustom.ShippingCountries = this.shipsToCountry;
                //console.log("shiping changed" + JSON.stringify(this.shippingprofileModel));
            }

            else {
                var country = this.findItem(this.shipsToCountry, this.shippingprofileModelCustom.ShippingCountries);
                var pos = this.shippingprofileModelCustom.Detail.findIndex(ele => ele.Country == country);
                this.shippingprofileModelCustom.Detail.splice(pos, 1);
                this.shippingprofileModelCustom.ShippingCountries = this.shipsToCountry;
                //console.log("shiping changed" + JSON.stringify(this.shippingprofileModel));
            }
        }
        else {
            this.shippingprofileModelCustom.Detail = [];
            this.shippingprofileModelCustom.ShippingCountries = [];
        }

    }
    findItem(CurrentArray, PreviousArray) {

        var CurrentArrSize = CurrentArray.length;
        var PreviousArrSize = PreviousArray.length;

        // loop through previous array
        for (var j = 0; j < PreviousArrSize; j++) {

            // look for same thing in new array
            if (CurrentArray.indexOf(PreviousArray[j]) == -1)
                return PreviousArray[j];

        }

        return null;

    }

    add() {
        if (this.shippingprofileModelCustom.Name) {
            if (this.methodModel.ProcessingTime) {
                if (this.shipsToModel.Cost != null && this.shipsToModel.Cost.toString().trim().length != 0) {
                    if (this.shipsToModel.CostWithAnotherItem != null && this.shipsToModel.CostWithAnotherItem.toString().trim().length != 0) {
                        if (this.shipsToModel.FreeShippingTotalValue != null && this.shipsToModel.FreeShippingTotalValue.toString().trim().length != 0) {
                            this.shippingProfileService.addShippingProfile(this.shippingprofileModelCustom).then(
                                response => {
                                    console.log(response);
                                    var data = JSON.parse(response);
                                    this.shippingprofileModel = data;
                                    this.shippingProfileService.getAll()
                                        .then(
                                            shippingprofiles => {
                                                this.ShippingProfilesList = JSON.parse(shippingprofiles);
                                                this.ShippingProfiles = this.ShippingProfilesList.Profiles;
                                                this.toasterService.SuccessSnack("Shipping profile saved successfully");
                                                this.router.navigate(['/myaccounts/seller/shop-settings/list/shipping-profile']);
                                            },
                                            error => this.errorMessage = <any>error);
                                    this.toasterService.ErrorSnack("Error while saving shipping profile");
                                },
                                error => this.errorMessage = <any>error);
                        } else {
                            this.toasterService.ErrorSnack("Please fill Free shipping total value");
                        }
                    } else {
                        this.toasterService.ErrorSnack("Please fill Cost with another item");
                    }
                } else {
                    this.toasterService.ErrorSnack("Please fill shipping cost");
                }
            } else {
                this.toasterService.ErrorSnack("Processing time is required");
            }
        } else {
            this.toasterService.ErrorSnack("Please fill profile name");
        }
    }
    RefreshShippingEditModel() {
        this.shippingprofileModelCustom = <ViewModels.IShippingProfileViewModel>{};
        this.shippingprofileModelCustom.IsCustom = true;
        this.shippingprofileModelCustom.ShippingCountries = [];
        this.shippingprofileModelCustom.Detail = <ViewModels.IShippingDetailViewModel[]>[];
        this.ShipsToChanged();

    }
    goBack() {
        this.router.navigate(['/myaccounts/seller/shop-settings/list/shipping-profile']);
        setTimeout(() => {
            jQuery('#shopInfo').removeClass('active');
            jQuery('#shippingprofile').addClass('active');
        }, 500);
    }
    editShippingProfile() {
        if (this.shippingprofileModelCustom.Name) {
            if (this.methodModel.ProcessingTime) {
                if (this.shipsToModel.Cost != null && this.shipsToModel.Cost.toString().trim().length != 0) {
                    if (this.shipsToModel.CostWithAnotherItem != null && this.shipsToModel.CostWithAnotherItem.toString().trim().length != 0) {
                        if (this.shipsToModel.FreeShippingTotalValue != null && this.shipsToModel.FreeShippingTotalValue.toString().trim().length != 0) {
                            this.shippingprofileModelCustom.IsCustom = true;
                            this.shippingprofileModelCustom.ProcessingTime = this.shippingprofileModelCustom.Detail[0].ShippingMethod[0].ProcessingTime;
                            this.shippingprofileModel = this.shippingprofileModelCustom;
                            this.shippingprofileModel.CODPrice = "0";
                            this.shippingProfileService.putShippingProfile(this.shippingprofileModel).then(response => {
                                this.router.navigate(['/myaccounts/seller/shop-settings/list/shipping-profile']);
                            });
                        } else {
                            this.toasterService.ErrorSnack("Please fill Free shipping total value");
                        }
                    } else {
                        this.toasterService.ErrorSnack("Please fill Cost with another item");
                    }
                } else {
                    this.toasterService.ErrorSnack("Please fill shipping cost");
                }
            } else {
                this.toasterService.ErrorSnack("Processing time is required");
            }
        } else {
            this.toasterService.ErrorSnack("Please fill profile name");
        }
    }

}