import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { ShippingProfileComponent } from './shipping-profile.component';
import { AddShippingProfileComponent } from './_add-shipping-profile/add-shipping-profile.component';
import { Routes, RouterModule } from '@angular/router';
import { Select2Module } from '../../../../../assets/ng2-select2';
import { MatInputModule } from '@angular/material';
import {MatSelectModule} from '@angular/material/select';



const shippingprofile: Routes = [
  {path: '', component:ShippingProfileComponent},
  {path: 'add', component: AddShippingProfileComponent},

]
@NgModule({
  imports: [
    SharedModule,
    Select2Module,
    MatInputModule,
    MatSelectModule,
    RouterModule.forChild(shippingprofile)
  ],
  declarations: [
    ShippingProfileComponent,
    AddShippingProfileComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ShippingProfileModule { }