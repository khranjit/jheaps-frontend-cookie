import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { ShopSettingsComponent } from './shop-settings.component';
import { Routes, RouterModule } from '@angular/router';

const shopsettings: Routes = [
  {path:'',redirectTo:'list',pathMatch:'full'},
  {
    path: 'list', component:ShopSettingsComponent,
      children:
        [
          {path:'',redirectTo:'shop-info',pathMatch:'full'},
          {path:'shop-info',loadChildren:'./_shop-info/shop-info.module#ShopInfoModule'},
          {path:'mail',loadChildren:'./_mail-content/mail-content.module#MailContentModule'},
          {path:'shipping-profile',loadChildren:'./_shipping-profile/shipping-profile.module#ShippingProfileModule'},
          {path:'package-detail',loadChildren:'./_package-detail/package-detail.module#PackageDetailModule'},
        ]
  },
]

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(shopsettings)
  ],
  declarations: [
    ShopSettingsComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ShopSettingsModule { }