import { Component, Inject} from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ShippingProfileService } from '../../../../services/shipping-profile.service';
import { PackageDetailService } from '../../../../services/package-detail.service';
import { NotifyService } from '../../../../services/notify.service';
import { elementStyleProp } from '@angular/core/src/render3';
declare var jQuery: any;

@Component({
    
    selector: 'packagedetail',
    providers: [ShippingProfileService,PackageDetailService,NotifyService],
    templateUrl: './package-detail.component.html'
})

export class PackageDetailComponent {
  
    pageLoader = false;
    public packageDetailList=[];
    constructor(private route: ActivatedRoute, private router: Router, private shippingprofileService: ShippingProfileService,private packageDetailService: PackageDetailService,private toaster: NotifyService) {
      
        this.Init();
    }

    public Init() {
         window.scrollTo(0,20); 
        
         this.pageLoader = true;
        this.packageDetailService.getAll()
            .then(
            packagedetail => {
                this.packageDetailList = JSON.parse(packagedetail);
                this.pageLoader = false;
               
            }
           );
    }


    edit(packagedetail) {
        let navimodel: NavigationExtras = {
            queryParams: { 'PackageDetails': JSON.stringify(packagedetail) }
        }
        this.router.navigate(['/myaccounts/seller/shop-settings/list/package-detail/add'], navimodel);
        console.log(packagedetail)
    }
   
    delete(deletepackageeId) {
      
       this.packageDetailService.removePackageDetail(deletepackageeId).then(result => {
           var res=JSON.parse(result);
           if(res.flag==true)
           {
            this.toaster.SuccessSnack(res.msg);
            this.Init();
           }
          else{
            this.toaster.ErrorSnack(res.msg); 
          }
           
        });
        //  jQuery('#deleteModal').modal("hide");
    }

    addNew() {
        this.router.navigate(['/myaccounts/seller/shop-settings/list/package-detail/add']);
    }
}