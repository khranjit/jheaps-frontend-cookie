import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { PackageDetailComponent } from './package-detail.component';
import { AddPackageDetailComponent } from './_add-package-detail/add-package-detail.component';
import { Routes, RouterModule } from '@angular/router';
import { Select2Module } from '../../../../../assets/ng2-select2';
import { MatInputModule } from '@angular/material';
import {MatSelectModule} from '@angular/material/select';



const packagedetail: Routes = [
  {path: '', component:PackageDetailComponent},
  {path: 'add', component: AddPackageDetailComponent},

]
@NgModule({
  imports: [
    SharedModule,
    Select2Module,
    MatInputModule,
    MatSelectModule,
    RouterModule.forChild(packagedetail)
  ],
  declarations: [
    PackageDetailComponent,
    AddPackageDetailComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class PackageDetailModule { }