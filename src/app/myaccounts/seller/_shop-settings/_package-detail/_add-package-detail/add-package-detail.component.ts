import { Component, Inject, OnInit,ChangeDetectionStrategy} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PackageDetailService } from '../../../../../services/package-detail.service';
import { NotifyService } from '../../../../../services/notify.service';

declare var jQuery:any;

@Component({
    
    selector: 'addpackagedetail',
    //changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [PackageDetailService,NotifyService],
    templateUrl: './add-package-detail.component.html'
})

export class AddPackageDetailComponent implements OnInit{
    
    edit:Boolean = false;
    editbutton:Boolean=false;
    orderPackageDetailsModel= <ViewModels.IPackageDetailViewModel>{};


    constructor(private route: ActivatedRoute, private router: Router,
       private toaster: NotifyService,
      private packageDetailService: PackageDetailService) {
        //this.init();
        this.Init();
        // this.getAllCountries();
        
    }
    public Init()
    {
        
        this.route
        .queryParams
        .subscribe(params => {
            if (params['PackageDetails']) 
            {
                // this.edit = true;
                this.orderPackageDetailsModel=JSON.parse(params['PackageDetails']);
                this.editbutton=true;
                
               
            }
            else
            {
                // this.edit = false;
                this.editbutton=false;
            }
        })
    }

    ngOnInit(){

    }

   
       

            addPackageDetail() {
                this.packageDetailService.addPackageDetail(this.orderPackageDetailsModel).then(
                    response => {
                        console.log(response);
                        var data = JSON.parse(response);
                        if(data.flag==true){
                           
                            this.toaster.SuccessSnack(data.msg);
                            this.router.navigate(['/myaccounts/seller/shop-settings/list/package-detail']);
                        }
                        else
                        {
                            this.toaster.ErrorSnack(data.msg);
                        }

                        
                        
                        
                    });
             
            }
               
                goBack() {
                    this.router.navigate(['/myaccounts/seller/shop-settings/list/package-detail']);
                  
                }
                editPackageDetail() {
                    
                    this.packageDetailService.putPackageDetail(this.orderPackageDetailsModel).then(response=>{
                        var result=JSON.parse(response);
                        console.log(result);
                        if(result.flag==true){
                            this.toaster.SuccessSnack(result.msg);
                            this.router.navigate(['/myaccounts/seller/shop-settings/list/package-detail']);
                        }
                        else{
                            this.toaster.ErrorSnack(result.msg);
                        }

                       
                    })
                }

}