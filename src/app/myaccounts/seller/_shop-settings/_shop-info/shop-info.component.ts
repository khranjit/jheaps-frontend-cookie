import { Component, Inject} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ShopService } from '../../../../services/shop.service';
import { ProductService } from '../../../../services/product.service';
import { ElasticService } from '../../../../services/elastic.service';
import { DOCUMENT } from '@angular/platform-browser';
import { NotifyService } from '../../../../services/notify.service';
import { UtilService } from '../../../../services/util.service';
import { NgxSpinnerService } from 'ngx-spinner';
import * as Q from 'q';

declare var jQuery : any;
declare var Cropper : any;
declare var Croppie : any;

@Component({
    
    selector: 'shippinfodash',
    providers: [ShopService, ProductService, ElasticService, NotifyService, UtilService],
    templateUrl: './shop-info.component.html'
})

export class ShopInfoDashComponent {
    //totalHitList: any[];
    public shopModel: any;
    private errorMessage: any;
    hitsList: any;
    topproductsList: any;
    Products = [];
    topProducts = [];
    searchTerm = '';
    totalPages = 1;
    currentPage = 1;
    from = 0;
    size = 10;
    total = 0;
    public pages: any[];

    totalPages1 = 1;
    currentPage1 = 1;
    from1 = 0;
    size1 = 10;
    total1 = 0;
    public pages1: any[];


    selectedProduct: any;
    currency: string;
    public logoLoader = false;
    public bannerLoader = false;
    getBase64fromURL_Logo: any;
    getBase64fromURL_Banner: any;
    selectedImageForCropping: any;
    public imageTransitions  = <any>{};
    public tempImageBase64Logo = [];
    public tempImageBase64Banner = [];
    tempCurrentFile_Logo;
    tempCurrentFile_Banner;
    public cropper: any;
    public croppie: any;
    public zoomRatio = 0;
    public editLogo:boolean;
    public editBanner:boolean;
    public imageFlag:string;
    fileName: string;
    CroppedDimension: any;
    topProductIdList=[];
    
    constructor( @Inject(DOCUMENT) private document: any, private toasterService:NotifyService, private route: ActivatedRoute, private router: Router, private ShopService: ShopService,
        private ProductService: ProductService, private ElasticService: ElasticService, private loadingservice:NgxSpinnerService,
        private toaster: NotifyService, private utilService: UtilService) {
        this.editLogo=false;
        this.editBanner=false;
        this.init();
        this.shopModel = {};
        this.shopModel.Images = [];
        this.shopModel.Summary = '';
        this.topproductsList = []
        this.currency = this.document.location.origin.split(".");
        this.currency = this.currency[this.currency.length - 1];
        this.currency = this.currency == "in" ? "INR" : "USD"
        //console.log(this.currency);
    }

    init() {
        this.shopModel = <ViewModels.Shop.IShopViewModel>{};
       
        this.from = 0; 
        this.size = 10;
        this.getOwnShopDetails();
    }

    ValidateCouriers():Q.Promise<any>{
        var defer = Q.defer();
        this.loadingservice.show();
        this.utilService.ValidateCouriersCODSeller().then(res =>{
            this.loadingservice.hide();
            var response = JSON.parse(res);
            if(response.status == true)
            defer.resolve({status:'success'});
            else
            defer.resolve({status:'failure'})
        }, err=>{
            this.loadingservice.hide();
            this.toaster.ErrorSnack("Something went wrong, please try again after some time");
        })

        return defer.promise; 
    }

    ValidateZipCode(event)
    {
        
        if(event == true)
        {
            this.shopModel.CashOnDeliveryAllowed = true;
            this.ValidateCouriers().then(data => {
                if (data.status == 'success') {
                    
                    this.shopModel.CashOnDeliveryAllowed = event;
                }
    
                else {
                    
                    this.toaster.ErrorSnack( "Courier COD service is not available for your shop Pin Code");
                    setTimeout(()=>{
                        this.shopModel.CashOnDeliveryAllowed = false;
                    },1000);
                }
            })
        }

        else
        {
            this.shopModel.CashOnDeliveryAllowed = event;
        }
       
    }

    getOwnShopDetails() {
        this.loadingservice.show();
        this.ShopService.getOwnShopDetails()
            .then(
            shop => {
                var result = JSON.parse(shop);
                this.shopModel=result[0];
                if(!this.shopModel.Summary)
                {
                    this.shopModel.Summary = '';
                }
                //this.topproductsList = this.shopModel.TopProducts;
                this.loadingservice.hide();
                this.getSuggestedProducts(this.searchTerm,this.from, this.size);
                this.getTopProducts(this.searchTerm, this.from1, this.size1);
                this.getTopProductsListId(this.searchTerm);
                if(!this.shopModel.hasOwnProperty('CashOnDeliveryAllowed'))
                {
                   this.shopModel.CashOnDeliveryAllowed = false;
                }
                console.log("Own shop", this.shopModel, this.topproductsList);
                if(this.shopModel.hasOwnProperty('Logo')){
                 console.log("URL", this.shopModel.Logo);
                var Logofilename = this.shopModel.Logo.split('/').pop();
                console.log("URL split , ",Logofilename);
                this.getBase64fromURL_Logo={"filename":Logofilename};
                console.log("this.getBase64fromURL Logo,",this.getBase64fromURL_Logo);
                 this.utilService.getBase64FromUrlforShopInfo(this.getBase64fromURL_Logo).then(
                    response=>{
                      // console.log("this.utilService.getBase64FromUrl(this.shopModel.Logo)",response);
                       this.tempCurrentFile_Logo="data:image/png;base64,"+JSON.parse(response).ImageDataBase64;
                       this.tempImageBase64Logo.push(this.tempCurrentFile_Logo);
                       console.log(this.tempImageBase64Logo)
                       if(this.shopModel.hasOwnProperty('Banner')){
                        var Bannerfilename = this.shopModel.Banner.split('/').pop();
                        console.log("URL split , ",Bannerfilename);
                        this.getBase64fromURL_Banner={"filename":Bannerfilename};
                        console.log("this.getBase64fromURL Banner,",this.getBase64fromURL_Banner);
                        this.utilService.getBase64FromUrlforShopInfo(this.getBase64fromURL_Banner).then(
                            response=>{
                                //console.log("this.utilService.getBase64FromUrl(this.shopModel.Banner)",response);
                                this.tempCurrentFile_Banner="data:image/png;base64,"+JSON.parse(response).ImageDataBase64;
                                this.tempImageBase64Banner.push(this.tempCurrentFile_Banner);
                                console.log(this.tempImageBase64Banner);
                                
                                });
                          }
                       
                        });
                }
               
        },error => {
            this.errorMessage = <any>error,
            this.loadingservice.hide();
        }
        );
    }
    searchProducts()
{
    this.currentPage = 1;
    this.currentPage1 = 1;
    this.getSuggestedProducts(this.searchTerm,0,10);
    this.getTopProducts(this.searchTerm,0,10)
}
    public updateShop() {
        console.log(this.shopModel)
        // var topProductIdArray = []
        // for (var i = 0; i < this.topproductsList.length; i++) {
        //     topProductIdArray.push(this.topproductsList[i]._id)
        // }
        // this.shopModel.TopProducts = topProductIdArray
        if (this.shopModel.Summary){
        this.ShopService.updateShop(this.shopModel)
            .then(
            shop => {
                this.toasterService.SuccessSnack( "Successfully updated");
                window.scrollTo(0, 0);
                this.shopModel = JSON.parse(shop);
                this.init();
                //console.log(JSON.parse(shop));
            },
            error => this.errorMessage = <any>error);
        } else {
            this.toasterService.ErrorSnack( "Please fill shop summary");
        }
    }

    LoadOnScroll() {
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

        if (scrollLength - scrollContainer.scrollTop < 1) {
            setTimeout(() => {
                this.getSuggestedProductsOnScroll();
            }, 500)
        }
    }

    ResetPagination() {
        this.from = 0;
        this.total = 0;
        // jQuery('#scroll-container').css("height", 500);
    }

    ValidateScrollContainer() {
        setTimeout(() => {
            var scrollContainer = document.getElementById('scroll-container');
            var scrollBox = document.getElementById('scroll-box');
        if(scrollContainer!=null && scrollBox!=null){
            var scrollContainerPos = scrollContainer.getBoundingClientRect();
            var scrollBoxPos = scrollBox.getBoundingClientRect();
            // if (scrollBoxPos.height < scrollContainerPos.height) {
            //     jQuery('#scroll-container').css("height", scrollBoxPos.height);
            // }
        }
        }, 1000)
    }

    public getSuggestedProducts(searchTerm,from, size) {
        console.log(this.shopModel._id)
        this.hitsList = [];
        this.pages = [];
        //Get the List of Products
            this.ElasticService.getProductByShopId(this.searchTerm,this.shopModel._id, from, size,false).then(
                products => {
                    this.hitsList = JSON.parse(products).hits.hits;   
                     this.Products = this.hitsList;                          
                    this.total = JSON.parse(products).hits.total;
                    
                    this.ValidateScrollContainer();
                  
                }, error => this.errorMessage = <any>error);
        
    }

    public getSuggestedProductsOnScroll() {
        this.from += this.size;
        if (this.from < this.total) {
        console.log(this.shopModel._id)
        // this.hitsList = [];
        this.pages = [];
        //Get the List of Products
            this.ElasticService.getProductByShopId(this.searchTerm,this.shopModel._id, this.from, this.size,false).then(
                products => {
                    this.hitsList = JSON.parse(products);   
                    this.total = JSON.parse(products).hits.total;
                    // this.Products = this.Products.concat(this.hitsList.hits.hits);
                    this.hitsList = this.Products.concat(this.hitsList.hits.hits);

                }, error => this.errorMessage = <any>error);
            }
    }

    public getTopProducts(searchTerm,from,size){
        this.topproductsList = [];
        this.pages1 = [];
        this.ElasticService.getProductByShopId(searchTerm, this.shopModel._id, from, size,true).then(
            products => {
                this.topproductsList = JSON.parse(products).hits.hits;
                this.topProducts = this.topproductsList;
                this.total1 = JSON.parse(products).hits.total;
                this.totalPages1 = Math.ceil(this.total1 / size);
                for(let i = 1; i <= this.totalPages1; i++){
                    this.pages1.push(i);
                }              
                this.ValidateScrollContainer();
            }, error => this.errorMessage = <any>error);
    }

    public getTopProductsOnScroll(searchTerm,from,size){
        this.from += this.size;
        if (this.from < this.total) {
        this.topproductsList = [];
        this.pages1 = [];
        this.ElasticService.getProductByShopId(searchTerm, this.shopModel._id, from, size,true).then(
            products => {
                this.topproductsList = JSON.parse(products).hits.hits;               
                this.total1 = JSON.parse(products).hits.total;                         
                this.topProducts = this.topProducts.concat(this.topproductsList.hits.hits);

            }, error => this.errorMessage = <any>error);
    }
}

updateTopProducts(list)
{
//Update the new list of top products


this.ShopService.updateShop({TopProducts:list})
.then(
shop => {

console.log(JSON.parse(shop));
},
error => this.errorMessage = <any>error);
}

selectionChange(prod){
    this.addTopproducts(prod._id);
}

getTopProductsListId(searchTerm)
{

    this.ElasticService.getProductByShopId(searchTerm, this.shopModel._id, null, null,true).then(
        products => {
            var list = JSON.parse(products).hits.hits
            if(list.length > 0)
            {
                list.forEach(x=>this.topProductIdList.push(x._id));
           }
        }, error=>this.errorMessage = <any>error)
    
}

    public deleteTopproducts(id) {
        //console.log("Test", index);    
        //this.topproductsList.splice(index, 1);
        //this.compareTopProducts(this.topproductsList); 

        var self = this;

        this.ElasticService.updateProductById(id,false).then(res=>{
            setTimeout(()=>{
                self.getSuggestedProducts(this.searchTerm,((this.currentPage - 1)*this.size), this.size);
                self.getTopProducts(this.searchTerm, ((this.currentPage1- 1)*this.size1), this.size1);
                var index = self.topProductIdList.findIndex(x => x == id);
                self.topProductIdList.splice(index,1);
                self.updateTopProducts(self.topProductIdList);
                // jQuery('#scroll-container').css("height", 500);
            }, 1000)
            
        }, error => this.errorMessage = <any>error);
    }

    public addTopproducts(id) {
        //console.log("Add", this.hitsList[index]);
        var self = this;
        this.ElasticService.updateProductById(id,true).then(res=>{
            setTimeout(()=>{
                self.getSuggestedProducts(this.searchTerm,((this.currentPage - 1)*this.size), this.size);
                self.getTopProducts(this.searchTerm, ((this.currentPage1- 1)*this.size1), this.size1);
                self.topProductIdList.push(id);
                self.updateTopProducts(self.topProductIdList);
                // jQuery('#scroll-container').css("height", 500);

                
            }, 1000)
        }, error => this.errorMessage = <any>error);
    }

    CheckForTopProducts(){
        
        let classes = {
            "col-md-6":(this.topproductsList.length > 0 || this.searchTerm.length>0),
            "col-md-12":(this.topproductsList.length == 0 && this.searchTerm.length ==0)
        }
      return classes;
    }

    public ChangeListener($event, value): void {
        if(value == 'Logo'){
            let files = [].slice.call($event.target.files);
            this.tempImageBase64Logo=[];
            console.log("this.tempImageBase64Logo=",this.tempImageBase64Logo)
            console.log("files logo ",files)
            this.logoReadThis(files, "Logo");
       }
        else{
            let files = [].slice.call($event.target.files);
            this.tempImageBase64Banner=[];
             console.log("this.tempImageBase64Banner=",this.tempImageBase64Banner)
            console.log("files banner ",files)
            this.logoReadThis(files, "Banner");
        }
       
    }

    public productUpdate(product) {
        if (this.shopModel.TopProducts.indexOf(product._id) < 0) {
            this.shopModel.TopProducts.push(product._id);
            //console.log(this.shopModel.TopProducts);
        }
    }
    
    triggerFileUpload(value) {
        if(value == 'Logo')
            jQuery('#logofileUpload').trigger('click');
       else
           jQuery('#bannerfileUpload').trigger('click');  
    }

    logoReadThis(inputValue: any, flag: string): any {
       if(flag == 'Logo'){
            this.logoLoader = true;
           var file: File = inputValue[0];
           this.fileName = file.name.split(".")[0]
           this.utilService.convertToBase64Change(file, 186, 172 , file.size);
           console.log(file);
       }
       else{
           this.bannerLoader = true;
           var file: File = inputValue[0];
           this.fileName = file.name.split(".")[0]
           this.utilService.convertToBase64Change(file, 1296, 280, file.size);
           console.log(file);
       } 
       let count = 0;
       this.utilService.invokeEvent.subscribe(value => {
           if(count == 0){
               if(value == "error"){
                     this.logoLoader = false;
                     this.bannerLoader = false;
                     this.toaster.ErrorSnack("Problem in File Size or Dimension. Please upload Some other Images ")
               }else{
                    this.utilService.uploadBase64AndGetUrl(this.fileName, value).then(
                        response => {
                           console.log("Extract Imagedata from uploadBase64AndGetUrl",JSON.parse(response).ImageData);
                             if (flag == "Logo"){
                                 this.logoLoader = false;
                                 this.shopModel.Logo = JSON.parse(response).ImageData;
                                 this.shopModel.LogoDimensions={
                                    "editedImage": JSON.parse(response).ImageData,
                                 };
                                 this.tempImageBase64Logo.push(value);
                                 }
                               else{
                                  this.bannerLoader = false;
                                  this.shopModel.Banner = JSON.parse(response).ImageData;
                                  this.tempImageBase64Banner.push(value);
                                  this.shopModel.BannerDimensions={
                                    "editedImage": JSON.parse(response).ImageData,
                                   };
                                 }
                              
                               console.log( " this.tempImageBase64Logo : ",this.tempImageBase64Logo,"Flag",flag)
                               console.log( " this.tempImageBase64BBanner : ",this.tempImageBase64Banner,"Flag",flag) 
                               this.toaster.SuccessSnack("Image uploaded Successfully");
                             //  console.log(this.productModel);
                           
                        },
                        error => {
                            this.toaster.ErrorSnack("Problem in uploading the image. Please try again");
                             this.logoLoader = false;
                            this.bannerLoader = false;
                        }
                    );
                       
               }
               count++;
           }
       });            
    }
    
    setCroppie(selectedImage,flag){
        if(flag == "Logo"){
            if( this.shopModel.hasOwnProperty('LogoDimensions')){
                if( this.shopModel.LogoDimensions.hasOwnProperty('Dimensions')){
                    let setValue = { "url": selectedImage, 
                                    "zoom": this.shopModel.LogoDimensions.Dimensions.zoom,
                                    "points":this.shopModel.LogoDimensions.Dimensions.points };
                    console.log(setValue);
                    return setValue;
                }
                else{ 
                    let setValue = { "url": selectedImage, "zoom": 0 };
                    console.log(setValue);
                    return setValue
                }
             }
        }
        else{
            if( this.shopModel.hasOwnProperty('BannerDimensions')){
                 if( this.shopModel.BannerDimensions.hasOwnProperty('Dimensions')){
                    let setValue = { "url": selectedImage, 
                                    "zoom": this.shopModel.BannerDimensions.Dimensions.zoom,
                                    "points":this.shopModel.BannerDimensions.Dimensions.points };
                    console.log(setValue);
                    return setValue;
                }
                else{ 
                    let setValue = { "url": selectedImage, zoom: 0 };
                    console.log(setValue);
                    return setValue;
                }
            }
        }
    }
    
    cropImage(selectedImage,flag){
        this.imageFlag=flag;
        let self = this;
        this.selectedImageForCropping = selectedImage;
        console.log("Croppie");
        if(this.croppie){
            this.croppie.destroy();
        }
        jQuery("#editImageModal").modal("show");
        if(flag=="Logo"){
        setTimeout(function(){
            let image = document.getElementById('EditableImage');
            //console.log("image ", image);
            self.croppie = new Croppie(image, {
                viewport: {
                    width: 186,
                    height: 172
                },
                 boundary: { width: 800, height: 500 },
                enableOrientation: true
                
            });
            self.croppie.bind(self.setCroppie(selectedImage,flag)).then(function(blob) {
                // do something with cropped blob
            });
        },1000);
            }
        else{
            setTimeout(function(){
            let image = document.getElementById('EditableImage');
            //console.log("image ", image);
            var getwidth = image.clientWidth;
            self.croppie = new Croppie(image, {
                viewport: {
                    width:getwidth,
                    height: 280
                },
                boundary: { width: 800, height: 500 },
                enableOrientation: true
                
            });
                self.croppie.bind({
                url: selectedImage,
                zoom: 0
            }).then(function(blob) {
                // do something with cropped blob
            });
        },1000);
            
            }
    }
        
   
    cropperImageReset(){
        this.cropper.reset(); 
    }
    cropperImageZoomIn(){
        let croppieZoom = this.croppie.get();
        this.zoomRatio = croppieZoom.zoom;
        console.log(croppieZoom.zoom, this.zoomRatio)
        if(croppieZoom.zoom >= this.zoomRatio){
            this.zoomRatio = this.zoomRatio + 0.1;
            console.log(this.zoomRatio);
            console.log();
            this.croppie.setZoom(this.zoomRatio);
        }
    }
    cropperImageZoomOut(){
        let croppieZoom = this.croppie.get();
        this.zoomRatio = croppieZoom.zoom;
        if(this.zoomRatio > 0){
            this.zoomRatio = this.zoomRatio - 0.1;
            console.log(this.zoomRatio);
            this.croppie.setZoom(this.zoomRatio);  
        }
    }
    cropperImageRotateLeft(){
        this.croppie.rotate(-90);        
    }
    cropperImageRotateRight(){
        this.croppie.rotate(90);        
    }

    
   saveCroppedImage(){
       
       jQuery("#editImageModal").modal("hide");
       let self = this;
       self.CroppedDimension = self.croppie.get()
       console.log("hi" ,self.CroppedDimension)
       if(this.imageFlag=="Logo"){
        this.logoLoader = true;
        this.shopModel.LogoDimensions = this.imageTransitions;
        jQuery("#editImageModal").modal("hide");
        this.croppie.result('base64', 'viewport', 'png', 0.5, false).then(function(response) {
           // console.log("response", response);
            self.utilService.uploadBase64AndGetUrl(self.shopModel.LogoDimensions.Name, response).then(
                response => {
                   self.shopModel.LogoDimensions={"editedImage":JSON.parse(response).ImageData,
                                                   "Dimensions" :self.CroppedDimension };
                   self.editLogo=self.shopModel.LogoDimensions.hasOwnProperty('editedImage');
                   console.log("uploadBase64AndGetUrl :",self.shopModel);
                   self.logoLoader = false
                   self.toaster.SuccessSnack("Edited image uploaded Successfully");
                },
                error => {
                   self.logoLoader = false
                   self.toaster.ErrorSnack("Problem in uploading the image. Please try again");
                }
            );
        });
           }
       else{
           this.bannerLoader = true ;
            this.shopModel.BannerDimensions = this.imageTransitions;
        jQuery("#editImageModal").modal("hide");
        this.croppie.result('base64', 'viewport', 'png', 1, false).then(function(response) {
            self.utilService.uploadBase64AndGetUrl(self.shopModel.BannerDimensions.Name, response).then(
                response => {
                   self.shopModel.BannerDimensions={"editedImage":JSON.parse(response).ImageData,
                                                    "Dimensions" :self.CroppedDimension };
                   self.editBanner=self.shopModel.BannerDimensions.hasOwnProperty('editedImage');
                   console.log("uploadBase64AndGetUrl :",self.shopModel);
                   self.bannerLoader = false
                   self.toaster.SuccessSnack("Edited image uploaded Successfully");
                },
                error => {
                    self.bannerLoader = false
                   self.toaster.ErrorSnack("Problem in uploading the image. Please try again");
                }
            );
        });
       }
    }
    
}
