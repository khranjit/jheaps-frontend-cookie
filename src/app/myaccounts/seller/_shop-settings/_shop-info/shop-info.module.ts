import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { ShopInfoDashComponent } from './shop-info.component';
import { Routes, RouterModule } from '@angular/router';
import { MatInputModule } from '@angular/material';


const shopinfo: Routes = [
  {path: '', component:ShopInfoDashComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(shopinfo),
    MatInputModule
  ],
  declarations: [
    ShopInfoDashComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ShopInfoModule { }