import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { InactiveProductsComponent } from './inactive-products.component';
import { SharedModule } from '../../../../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';


const inactiveproduct: Routes = [
  {path: '', component:InactiveProductsComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(inactiveproduct)
  ],
  declarations: [
    InactiveProductsComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class InactiveProductsModule { }