import { Component,Inject } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'listingmanager',
  providers: [UserService],
  templateUrl: './listing-manager.component.html'
})

export class ListingManagerComponent {
  ShowUserRole: any;
  ShowUserProfile: any;
  constructor(private userService: UserService, private router: Router) {

    window.scrollTo(0, 20);
    this.userService.showProfile().then(
      response => {
        this.ShowUserProfile = JSON.parse(response);
        this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
        console.log('type of user' + this.ShowUserRole)
        if (this.ShowUserRole == 'seller') {

        }
        else {
          this.router.navigate(['']);
        }
      }
    );
  }
}