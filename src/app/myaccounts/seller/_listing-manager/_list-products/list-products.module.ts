import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { ListProductComponent } from './list-products.component';
import { Routes, RouterModule } from '@angular/router';


const listproducts: Routes = [
  {path: '', component:ListProductComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(listproducts)
  ],
  declarations: [
    ListProductComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ListProductsModule { }