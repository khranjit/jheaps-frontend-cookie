import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { DraftProductsComponent } from './draft-products.component';
import { Routes, RouterModule } from '@angular/router';


const draftproduct: Routes = [
  {path: '', component:DraftProductsComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(draftproduct)
  ],
  declarations: [
    DraftProductsComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class DraftProductsModule { }