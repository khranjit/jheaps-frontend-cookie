import { Component, HostListener, ChangeDetectionStrategy, ViewEncapsulation, Inject } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ProductService } from '../../../../services/product.service';
import { ElasticService } from '../../../../services/elastic.service';
import { UserService } from '../../../../services/user.service';

declare var jQuery: any;

@Component({

    changeDetection: ChangeDetectionStrategy.Default,
    encapsulation: ViewEncapsulation.Emulated,
    selector: 'draftproducts',
    providers: [ProductService, ElasticService, UserService],
    templateUrl: './draft-products.component.html'
})
export class DraftProductsComponent {
    @HostListener("window:scroll", ["$event"])
    onWindowScroll() {
        let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
        let max = document.documentElement.scrollHeight;
        var percentage = Math.round((pos / max) * 100) / 100;
        //console.log("percentage "+percentage)
        if (percentage > 0.75) {
            this.callscroll();
          
        }
    }
    Products = [];
    ProductsList: any;
    deletableContent: any;
    errorMessage: string;
    public pages: any[];
    public shopId: string;
    public firstItem:boolean = false;
    searchTerm = '';
    totalPages = 1;
    currentPage = 1;
    from = 0;
    size = 10;
    total = 0;
    sort_type: any;
    isAscending: any;
    loader = false;
    pageLoader = false;

    constructor(private route: ActivatedRoute, private router: Router,
        private productService: ProductService,
        private elasticService: ElasticService,
        private userService: UserService) {
        this.init();
        this.sort_type = '';
        this.isAscending = [];
    }

    public init() {
        this.pageLoader = true;
        this.getUserDetails();
    }

    public getOrderedData() {
        this.searchProducts(this.searchTerm, this.from, this.size);
    }

    public getUserDetails() {
        this.userService.showProfile().then(
            userInfo => {
                this.shopId = JSON.parse(userInfo).Items[0].Shop;
                this.searchProducts(this.searchTerm, this.from, this.size);
            }, error => this.errorMessage = <any>error);
    }

    public tableSorting(sort) {
        this.sort_type = sort;
        if (!this.isAscending[sort]) {
            this.isAscending[sort] = true;
        } else {
            this.isAscending[sort] = !this.isAscending[sort];
        }
        this.getOrderedData();
    }

    public searchProducts(searchTerm, from, size) {
        this.Products = [];
        this.pages = [];
        this.elasticService.getProductByShopIdWithSorting(searchTerm, this.shopId, from, size, this.sort_type, this.isAscending, "Draft").then(
            products => {
                this.ProductsList = JSON.parse(products);
                this.Products = this.ProductsList.hits.hits;
                this.total = JSON.parse(products).hits.total;
                this.pageLoader = false;
                this.firstItem = true;
                this.ValidateScrollContainer();
            }, error => this.errorMessage = <any>error);
    }
    async callscroll()
    {
      if(this.firstItem)
      {
        await   this.searchProductsOnscroll();;
      }
    }
    public searchProductsOnscroll() {
        this.from += this.size;
        if (this.from < this.total) {
            this.loader = true;
            this.elasticService.getProductByShopIdWithSorting(this.searchTerm, this.shopId, this.from, this.size, this.sort_type, this.isAscending, "Draft").then(
                products => {
                    this.ProductsList = JSON.parse(products);
                    this.total = this.ProductsList.hits.total;
                    this.loader = false;
                    this.Products = this.Products.concat(this.ProductsList.hits);
                }, error => this.errorMessage = <any>error);
        }
    }

    edit(product) {
        let navimodel: NavigationExtras = {
            queryParams: { 'ProductId': product._id }
        }
        this.router.navigate(['/myaccounts/seller/listing/addproduct'], navimodel);
        //console.log(product)
    }

    delete() {
        this.productService.deleteProduct(this.deletableContent)
            .then(
                Status => {
                    if (Status == 200) { this.init(); }
                },
                error => this.errorMessage = <any>error);
    }
    confirmDelete(product) {


        this.deletableContent = product._id;


        jQuery("#confirmDeleteModal").modal('show');
    }

    confirmDeleteSmall(product) {


        this.deletableContent = product._id;


        jQuery("#deleteModal").modal('show');
    }

    LoadOnScroll() {
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

        if (scrollLength - scrollContainer.scrollTop < 1) {
            setTimeout(() => {
                this.searchProductsOnscroll();
            }, 500)
        }
    }

    ResetPagination() {
        this.from = 0;
        this.total = 0;
        jQuery('#scroll-container').css("height", 500);
    }

    ValidateScrollContainer() {
        setTimeout(() => {
            var scrollContainer = document.getElementById('scroll-container');
            var scrollBox = document.getElementById('scroll-box');
            if (scrollContainer != null && scrollBox != null) {
                var scrollContainerPos = scrollContainer.getBoundingClientRect();
                var scrollBoxPos = scrollBox.getBoundingClientRect();
                if (scrollBoxPos.height < scrollContainerPos.height) {
                    jQuery('#scroll-container').css("height", scrollBoxPos.height);
                }
            }
        }, 1000)
    }

    addNew() {
        this.router.navigate(['/myaccounts/seller/listing/addproduct']);
    }
}