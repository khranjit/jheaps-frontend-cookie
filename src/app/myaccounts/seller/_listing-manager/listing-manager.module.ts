import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { ListingManagerComponent } from './listing-manager.component';
import { Routes, RouterModule } from '@angular/router';


const listingmanager: Routes = [
  {
    path: '',component:ListingManagerComponent,
      children:
        [
          {path:'',redirectTo:'listproducts', pathMatch:'full'},
          {path:'listproducts', loadChildren:'./_list-products/list-products.module#ListProductsModule'},
          // {path:'listproducts', component:ListProductComponent},
          {path:'addproduct', loadChildren:'./_add-product/add-product.module#AddProductModule'},
          {path:'draftproducts', loadChildren:'./_draft-products/draft-products.module#DraftProductsModule'},
          {path:'inactiveproducts', loadChildren:'./_inactive-products/inactive-products.module#InactiveProductsModule'},
          {path:'soldoutproducts', loadChildren:'./_soldout-products/soldout-products.module#SoldOutProductsModule'}
        ]
  }
]

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(listingmanager)
  ],
  declarations: [
    ListingManagerComponent
    ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ListingManagerModule { }