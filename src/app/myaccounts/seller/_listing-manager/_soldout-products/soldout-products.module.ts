import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { SoldoutProductsComponent } from './soldout-products.component';
import { Routes, RouterModule } from '@angular/router';


const soloutproducts: Routes = [
  {path: '', component:SoldoutProductsComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(soloutproducts)
  ],
  declarations: [
    SoldoutProductsComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class SoldOutProductsModule { }