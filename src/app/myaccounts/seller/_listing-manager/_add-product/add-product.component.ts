import { Component, Inject, ViewChild } from '@angular/core';
import { ProductService } from '../../../../services/product.service';
import { CategoryService } from '../../../../services/category.service';
import { SectionService } from '../../../../services/section.service';
import { ShippingProfileService } from '../../../../services/shipping-profile.service';
import { NotifyService } from '../../../../services/notify.service';
import { UtilService } from '../../../../services/util.service';
import { ShopService } from '../../../../services/shop.service';
import { UserService } from '../../../../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ZoneService } from '../../../../services/zone.service';
import { SelectItem } from 'primeng/api';
import * as Q from 'q'
import ImageCompressor from 'image-compressor.js';
import { AttributeService } from '../../../../services/attribute.service';
import { element } from '@angular/core/src/render3';
// import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';


declare var jQuery: any;
declare var Cropper: any;
declare var Croppie: any;

@Component({

    selector: 'addproduct',
    providers: [ZoneService, ProductService, CategoryService, SectionService, ShopService, ShippingProfileService, NotifyService, UtilService, AttributeService],
    templateUrl: "./add-product.component.html"
})

export class AddProductComponent {
    // public Editor = ClassicEditor;
    codAvailable: boolean;
    showSaveButton = true;
    cropNormalImage: any;
    categorySelected: any;
    categoriesToSelect: SelectItem[] = [];
    subCategorySelected: any;
    subCategoriesToSelect: SelectItem[] = [];
    showSelectCategoriesSection = false;
    selectedAttribute1: any;
    isChecked: boolean;
    attriadddetect = false;

    @ViewChild('addAttribute') addAttributeForm: HTMLFormElement;
    homeMessage: any;
    // newCategoriesList:SelectItem[] = [
    //     {label: 'Audi', value: '1'},
    //     {label: 'BMW', value: '2'},
    //     {label: 'Fiat', value: '3'},
    //     {label: 'Ford', value: '4'},
    //     {label: 'Honda', value: '5'},
    //     {label: 'Jaguar', value: '6'},
    //     {label: 'Mercedes', value: '7'},
    //     {label: 'Renault', value: '8'},
    //     {label: 'VW', value: '9'},
    //     {label: 'Volvo', value: '10'}
    // ];
    //selectedVariationType: any;
    //selectedVariationIndex: any;
    selectedVariationForCropping: any;
    cropVariationImage: boolean;
    selected:any;
    variationImageArray = [];
    AutoFillIndex: any;
    AutoFillValue: any;
    AutoFillArray: any;
    AutoFillArrayPrice: any;
    AutoFillArraySKU: any;
    AutoFillArrayQty: any;
    public shippingprofileModel: ViewModels.IShippingProfileViewModel;
    public shippingprofileModelCustom: ViewModels.IShippingProfileViewModel;
    newbutton = true;
    editbutton = false;
    addattribute: boolean = false;
    attributeModel = <any>{};
    //productSKU:any = '';
    sepVariationArray = [];
    //variationContent = [{name:''},{name:''}];
    //variationPrice:any;
    productAttributesList = [];
    //variationQuantity:any;
    variationPriceArray = [];
    //public ShipsTo : ViewModels.IShipsToViewModel;
    public VariationList = [];
    public productModel: any;
    public productModel1: any;
    public productEditModel: any;
    public selectedCategory: any;
    public selectedAttribute: any;
    public CategoriesList: any;
    public SubCategoryList: any;
    public SectionsList: any;
    public ShippingProfilesList: any;
    public Categories: any;
    public Sections: any;
    public AttributesList = [];
    customShippingProfile: any;
    public ShippingProfiles = [];
    public attributeVariation: any;
    public variationObject = <any>{};
    public temparray = [];
    public showAttribute = [];
    private errorMessage: any;
    allCountries: any;
    allCities: any;
    shipsToCountry = [];
    allCountriesList = [];
    allCitiesList: any = [];
    shipsToDisplay: String[] = [];
    customId: any;
    ShippingSubProfile: any;
    selectedSubProfileIndex: any;
    shippingProfileType: any;
    selectedImageForCropping: any;
    public imageTransitions = <any>{};
    //public tempImageBase64 = [];
    public dataAvailable = false;
    public productCurrency: any = { value: "USD" };

    public cropper: any;
    public croppie: any;
    public zoomRatio = 0;


    public loader = false;
    public Zones: any;
    public ZoneList: any;
    public CountryData: any = [];
    public LoopZones: any = [];
    public DataSepZone = [];
    fillAllVariations = false;
    fillAllSepVariations = [false, false];
    public options: Object = {
        // placeholderText: 'Edit Your Content Here!',
        charCounterCount: false,
        listAdvancedTypes: true,
        toolbarButtons: ['bold', 'italic', 'underline', 'paragraphFormat', 'alert', 'fontFamily', 'fontSize', 'formatOL', 'formatUL'],
        toolbarButtonsXS: ['bold', 'italic', 'underline', 'paragraphFormat', 'alert', 'fontFamily', 'fontSize', 'formatOL', 'formatUL'],
        toolbarButtonsSM: ['bold', 'italic', 'underline', 'paragraphFormat', 'alert', 'fontFamily', 'fontSize', 'formatOL', 'formatUL'],
        toolbarButtonsMD: ['bold', 'italic', 'underline', 'paragraphFormat', 'alert', 'fontFamily', 'fontSize', 'formatOL', 'formatUL'],
        quickInsertTags: [],



    }


    constructor(private route: ActivatedRoute, private shopService: ShopService, private router: Router, private productService: ProductService,
        private categoryService: CategoryService, private sectionService: SectionService, private shippingProfileService: ShippingProfileService,
        private toaster: NotifyService, private utilService: UtilService, private zoneService: ZoneService, public attributeService: AttributeService, private userService: UserService) {


        this.Init()
        // this.getAllCountries();
        // this.getAllCitiesList();
        //this.shipsTo = this.shipsToDisplay;  
    }

    companies: any[] = [];
    loading = false;
    selectedCompanyCustom: any;
    companiesNames = ['Miškas', 'Žalias', 'Flexigen'];
    lengthValues = [10, 20, 30];

    ngOnInit() {
        this.companiesNames.forEach((c, i) => {
            this.companies.push({ id: i, name: c });
        });
    }

    addTag(name) {
        this.selectedAttribute1 = "";
        this.selectedAttribute1 = name;
        return name;
    }
    addAttr(attr) {
        return false;
    }

    addTagPromise(name) {
        return new Promise((resolve) => {
            this.loading = true;
            setTimeout(() => {
                resolve({ id: 5, name: name, valid: true });
                this.loading = false;
            }, 1000);
        })
    }

    clickEvt(e, event) {
        function arrayRemove(arr, value) {

            return arr.filter(function (ele) {
                return ele != value;
            });

        }
        if (e.selectedAttribute.length == 0) {
            event.preventDefault();
            this.toaster.ErrorSnack("Kindly select minimum only value from attribute")
        } else {
            console.log(this.showAttribute)
            if (!event.srcElement.checked) {
                var exist = this.showAttribute.find(x => x == e.Name)
                if (exist) {
                    this.showAttribute = arrayRemove(this.showAttribute, e.Name);
                } else {
                    this.showAttribute.push(e.Name)
                }
            } else {
                if (this.showAttribute.length >= 5) {
                    event.preventDefault();
                    this.toaster.ErrorSnack("Can add 5 attributes to overview")
                } else {

                    var exist = this.showAttribute.find(x => x == e.Name)
                    if (exist) {
                        this.showAttribute = arrayRemove(this.showAttribute, e.Name);
                    } else {
                        this.showAttribute.push(e.Name)
                    }
                    console.log(this.showAttribute)

                }
            }
        }



    }


    checkAttributeName() {
        this.attributeService.checkAttributeName(this.attributeModel).then(res => {
            if (JSON.parse(res).found) {
                var newError = <any>{};
                var oldError = this.addAttributeForm.form.controls['Name'].errors;
                //console.log(obj);
                if (JSON.parse(res).found) {
                    for (var key in oldError) {
                        console.log(key);
                        console.log(oldError[key]);
                        newError[key] = oldError[key]
                    }
                    newError.incorrect = true;
                    console.log(newError);
                    this.addAttributeForm.form.controls['Name'].setErrors(newError);
                }

                else {
                    if (oldError.incorrect)
                        oldError.incorrect = false;
                    this.addAttributeForm.form.controls['Name'].setErrors(oldError);
                }
            }
        },
            error => this.errorMessage = <any>error);
    }

    newAttribute() {
        var CategoryId;
        if (this.subCategorySelected) {
            CategoryId = this.subCategorySelected._id;
        } else if (this.categorySelected) {
            CategoryId = this.categorySelected.Id;
        }
        this.attributeModel.CategoryId = CategoryId;
        this.attributeService.setAttribute1(this.attributeModel).then
            (Status => {
                this.toaster.SuccessSnack('Attribute created Successfully!!');
                this.categorychange();
                this.attriadddetect = true;
                this.addattribute = false;
                this.attributeModel.Name = "";
                this.attributeModel.Values = [];
            },
                error => {
                    this.errorMessage = <any>error;
                    this.toaster.ErrorSnack('Attribute already exists');
                    console.log("error ", this.errorMessage)
                });
    }
    categorychange() {
        this.productModel.Categories[0] = <any>{};
        console.log(this.categorySelected);
        var categoryObj = this.CategoriesList.find(category => category.Parent._id == this.categorySelected.Id);
        if (categoryObj) {
            this.productModel.Categories[0] = categoryObj.Parent;
            this.addToCategoriesArray();
            if (categoryObj.Children.length == 0) {
                this.subCategorySelected = null;
                this.subCategoriesToSelect = [];
                this.showSelectCategoriesSection = false;
            }
        }
        else {
            this.toaster.ErrorSnack("Selected Category cannot be added");
        }
    }
    categoriesChanged(event) {
        this.productModel.Categories[0] = <any>{};
        console.log(this.categorySelected);
        var categoryObj = this.CategoriesList.find(category => category.Parent._id == this.categorySelected.Id);
        if (categoryObj) {
            this.productModel.Categories[0] = categoryObj.Parent;
            this.addToCategoriesArray();
            if (categoryObj.Children.length == 0) {
                this.subCategorySelected = null;
                this.subCategoriesToSelect = [];
                this.showSelectCategoriesSection = false;
            }
        }
        else {
            this.toaster.ErrorSnack("Selected Category cannot be added");
        }
    }

    subCategoriesChanged(event) {
        if (this.subCategorySelected) {
            var attributes = this.subCategorySelected.Attributes;
            attributes.forEach(item => {
                var index = this.productAttributesList.findIndex(prodattr => item.Name == prodattr.Name)
                if (index != -1) {
                    this.productAttributesList.splice(index, 1);
                }

                var index1 = this.VariationList.findIndex(variation => item.Name == variation.Name)
                if (index1 != -1) {
                    this.VariationList.splice(index, 1);
                }
            })
        }

        this.subCategorySelected = event.value;
        this.productModel.Categories[0].SubCategories[0] = this.subCategorySelected;
        this.ResetVariationList();
        this.addToSubCategoriesArray();
        this.showSelectCategoriesSection = false;
    }
    // addVariation()
    // {
    //      var toAdd = true;
    //      if(this.variationPriceArray.length>0)
    //      {
    //      this.variationPriceArray.forEach(ele =>{
    //          if(ele.Name1)
    //          {
    //              if(ele.Name2)
    //              {
    //                  if(ele.Value1 == this.variationContent[0].name && ele.Value2 == this.variationContent[1].name)
    //                  {
    //                      //Duplicate Item
    //                      toAdd = false;
    //                  }
    //              }

    //              else if(ele.Value1 == this.variationContent[0].name)
    //              {
    //                  //Duplicate Item
    //                  toAdd = false;
    //              }
    //          } 
    //      });
    //      }

    //      if(toAdd)
    //      {
    //         if(this.variationQuantity > 0)
    //         {
    //             var InStock = true;
    //         }
    //         else if(this.variationQuantity == 0)
    //         {
    //             var InStock = false;
    //         }
    //         var newVariation = {};
    //         if(this.variationContent[1].name)
    //         {
    //            newVariation = {
    //                Name1:this.VariationList[0].Name,
    //                Value1:this.variationContent[0].name,
    //                Name2:this.VariationList[1].Name,
    //                Value2:this.variationContent[1].name,
    //                Price:this.variationPrice,
    //                Qty: this.variationQuantity,
    //                InStock:InStock

    //             }
    //         }

    //         else{
    //            newVariation = {
    //                Name1:this.VariationList[0].Name,
    //                Value1:this.variationContent[0].name,
    //                // Name2:this.VariationList[1]?.Name,
    //                // Value2:this.variationContent[1].name,
    //                Price:this.variationPrice,
    //                Qty: this.variationQuantity,
    //                InStock:InStock

    //             }
    //         }

    //         console.log("New Variation " + JSON.stringify(newVariation));
    //         this.productModel.QuantityAvailable += this.variationQuantity;
    //         this.variationPriceArray.push(newVariation);
    //         this.productModel.Variations = this.variationPriceArray;

    //         this.variationContent = [{name:''},{name:''}];
    //         this.variationPrice = '';
    //         this.variationQuantity = '';
    //      }
    // }

    // removePriceVariation(item)
    // {
    //     this.variationPriceArray = this.variationPriceArray.filter(ele =>{
    //         if(this.isEquivalent(ele,item))
    //         {
    //             console.log("Item removed");
    //             this.productModel.QuantityAvailable -= item.Qty;
    //             return false;
    //         }
    //         return true;
    //     });
    //     this.productModel.Variations = this.variationPriceArray;

    // }

    //  isEquivalent(a, b) {
    //     // Create arrays of property names
    //     var aProps = Object.getOwnPropertyNames(a);
    //     var bProps = Object.getOwnPropertyNames(b);

    //     // If number of properties is different,
    //     // objects are not equivalent
    //     if (aProps.length != bProps.length) {
    //         return false;
    //     }

    //     for (var i = 0; i < aProps.length; i++) {
    //         var propName = aProps[i];

    //         // If values of same property are not equal,
    //         // objects are not equivalent
    //         if (a[propName] !== b[propName]) {
    //             return false;
    //         }
    //     }

    //     // If we made it this far, objects
    //     // are considered equivalent
    //     return true;
    // }

    // ValidateProductName()
    // {
    //     this.productModel.Name = 
    // }


    //Function to dynamically generate the variations

    ValidateVariations(Attr, event, isParent) {

        Attr.IsQuantityVariation = true;
        var remove = false;
        if (this.VariationList != null) {

            //If Parent check and delete if it is exist
            if (isParent) {
                this.VariationList.forEach(ele => {
                    if (ele.Name == Attr.Name) {
                        var index = this.VariationList.indexOf(ele);
                        if (index > -1) {
                            this.VariationList.splice(index, 1);
                            remove = true;
                        }
                    }
                });


                //If length less than 2, add to the array

                if (this.VariationList.length < 2) {
                    if (!remove)
                        this.VariationList.push(Attr);
                }

                //Else throw an alert to user

                else if (this.VariationList != null && this.VariationList.length >= 2) {

                    this.toaster.ErrorSnack("Can only add two variations");
                    //console.log(event);
                    event.target.checked = false;
                    //console.log(event);
                    Attr.IsVariation = false;
                    //event.target.value = false;

                }
            }

            else {
                //Child Attribute
                this.VariationList.forEach(ele => {
                    if (ele.Name == Attr.Name) {
                        var index = this.VariationList.indexOf(ele);
                        if (index > -1) {
                            //Remove the old
                            this.VariationList.splice(index, 1);

                            //Push the updated
                            this.VariationList.push(Attr);
                        }
                    }
                });

            }




            //Method to generate the variation model
            this.generateVariationModel(this.VariationList);
            this.productModel.VariationList = this.VariationList;

        }


    }

    ShipsToChanged() {
        console.log(event);
        //this.shipsTo = event;
        //console.log(this.shipsTo);

        this.shipsToCountry.push("India");


        if (this.shipsToCountry != null) {
            if (this.shipsToCountry.length > this.shippingprofileModelCustom.ShippingCountries.length) {
                var country;
                if (this.shippingprofileModelCustom.ShippingCountries.length > 0) {
                    country = this.findItem(this.shippingprofileModelCustom.ShippingCountries, this.shipsToCountry);

                }
                else {
                    country = this.shipsToCountry[0];

                }
                // this.zoneService.CheckMultiCountry(this.shipsToCountry)
                //     .then(result => {
                //         var ResData = JSON.parse(result);
                //         this.DataSepZone = ResData.Items;
                //         console.log('multicountry', this.DataSepZone);
                //         this.DataSepZone.forEach(ele => {
                //             this.LoopZones.push(ele.Zone);
                //         });
                //         console.log('loopzones', this.LoopZones);
                //     });
                var detailModel = <ViewModels.IShippingDetailViewModel>{};
                var methodModel = <ViewModels.IShippingMethodViewModel>{};
                var shipsToModel = <ViewModels.IShipsToViewModel>{};
                detailModel.Country = country;
                methodModel.Name = "";
                methodModel.ProcessingTime = "";
                methodModel.Default = true;
                methodModel.ShipsTo = [];
                shipsToModel.Zone = "";
                shipsToModel.CostWithAnotherItem = 0;
                shipsToModel.Cost = 0;
                detailModel.ShippingMethod = [];
                shipsToModel.FreeShippingTotalValue = 0;
                methodModel.ShipsTo.push(shipsToModel);
                detailModel.ShippingMethod.push(methodModel);
                //console.log("shiping changed" + JSON.stringify(detailModel)); 
                this.shippingprofileModelCustom.Detail.push(detailModel);
                //console.log("shiping changed" + JSON.stringify(this.shippingprofileModel));
                this.shippingprofileModelCustom.ShippingCountries = this.shipsToCountry;
                //console.log("shiping changed" + JSON.stringify(this.shippingprofileModel));
            }

            else {
                var country = this.findItem(this.shipsToCountry, this.shippingprofileModelCustom.ShippingCountries);
                var pos = this.shippingprofileModelCustom.Detail.findIndex(ele => ele.Country == country);
                this.shippingprofileModelCustom.Detail.splice(pos, 1);
                this.shippingprofileModelCustom.ShippingCountries = this.shipsToCountry;
                //console.log("shiping changed" + JSON.stringify(this.shippingprofileModel));
            }
        }
        else {
            this.shippingprofileModelCustom.Detail = [];
            this.shippingprofileModelCustom.ShippingCountries = [];
        }

    }

    findItem(CurrentArray, PreviousArray) {

        var CurrentArrSize = CurrentArray.length;
        var PreviousArrSize = PreviousArray.length;

        // loop through previous array
        for (var j = 0; j < PreviousArrSize; j++) {

            // look for same thing in new array
            if (CurrentArray.indexOf(PreviousArray[j]) == -1)
                return PreviousArray[j];

        }

        return null;

    }

    //Method to generate the variation model

    generateVariationModel(items: any) {
        
        var nameModel = [];
        var valueModel = [];
        var variationModel = [];
        this.sepVariationArray = [];
        this.variationPriceArray = [];
        this.productModel.seperateVariation.VariationArray = [];
        this.fillAllVariations = false;
        this.fillAllSepVariations = [false, false];
        this.route.queryParams.subscribe(params => {
            if (params['ProductId']) {
                var example = JSON.stringify(this.productModel1.Variations);
                variationModel = JSON.parse(example);
                this.productModel.VariationQuantityAvailable = this.productModel1.VariationQuantityAvailable;
            }else{
                this.productModel.VariationQuantityAvailable = 0;
            }
        });

        if (items.length > 1) {
            this.productModel.useDefaultPrice = !(items[0].IsPriceVariation || items[1].IsPriceVariation);
            this.productModel.useDefaultQuantity = !(items[0].IsQuantityVariation || items[1].IsQuantityVariation);
            this.productModel.useDefaultSKU = !(items[0].IsSKUVariation || items[1].IsSKUVariation);

            var priceMatch = (items[0].IsPriceVariation == items[1].IsPriceVariation) ? items[1].IsPriceVariation : false;
            var qtyMatch = (items[0].IsQuantityVariation == items[1].IsQuantityVariation) ? items[1].IsQuantityVariation : false;
            var SKUMatch = (items[0].IsSKUVariation == items[1].IsSKUVariation) ? items[1].IsSKUVariation : false;
            var imageMatch = (items[0].IsImageVariation == items[1].IsImageVariation) ? items[1].IsImageVariation : false;

            if (priceMatch || qtyMatch || SKUMatch || imageMatch) {
                items.forEach(ele => {
                    nameModel.push(ele.Name);
                    valueModel.push(ele.Value);
                });


                this.productModel.seperateVariation.status = false;

                for (var i = 0; i < valueModel[0].length; i++) {
                    for (var j = 0; j < valueModel[1].length; j++) {
                        var tempItem = <any>{};

                        tempItem.Active = true;
                        tempItem.Name1 = nameModel[0];
                        tempItem.Name2 = nameModel[1];
                        tempItem.Value1 = valueModel[0][i];
                        tempItem.Value2 = valueModel[1][j];
                        if (items[0].IsPriceVariation || items[1].IsPriceVariation)
                            tempItem.Price = 0;
                        if (items[0].IsQuantityVariation || items[1].IsQuantityVariation)
                            tempItem.Qty = 0;
                        if (items[0].IsSKUVariation || items[1].IsSKUVariation)
                            tempItem.SKU = '';
                        if (items[0].IsImageVariation || items[1].IsImageVariation)
                            tempItem.Image = '';
                        tempItem.InStock = false;

                       
                        var check = variationModel.find(ele=>{
                            if(ele.Value2 === tempItem.Value2 && ele.Value1 === tempItem.Value1){
                                return ele
                            }
                        })
                       
                        if (!check) {
                            variationModel.push(tempItem);
                        } 

                        // variationModel.push(tempItem);
                    }
                }
                var temparr= []
                var varQty = 0
                for (var i = 0; i < valueModel[0].length; i++) {
                    for (var j = 0; j < valueModel[1].length; j++) {
                        var check1 = variationModel.find(ele => {
                            if(ele.Value1 === valueModel[0][i] && ele.Value2 === valueModel[1][j]){
                                return ele
                            }
                        })
                        if(check1){
                            varQty += check1.Qty;
                            if (items[0].IsImageVariation){
                                check1.Image = check1.Image ? check1.Image : '';
                            }else{
                                delete check1.Image
                            }
                            temparr.push(check1)
                        }
                    }
                }
                variationModel = temparr
    
                this.productModel.VariationQuantityAvailable = varQty;
                this.productModel.Variations = variationModel;
                this.variationPriceArray = variationModel;
                //this.variationImageArray = variationModel;
            }

            else {

                this.productModel.seperateVariation.status = true;

                items.forEach(ele => {

                    var tempVar = [];
                    for (var i = 0; i < ele.Value.length; i++) {

                        var tempItem = <any>{};

                        tempItem.Active = true;
                        tempItem.Name1 = ele.Name;
                        //tempItem.Name2 = nameModel[1];
                        tempItem.Value1 = ele.Value[i];
                        //tempItem.Value2 = valueModel[0][j];
                        if (ele.IsPriceVariation)
                            tempItem.Price = 0;
                        if (ele.IsQuantityVariation)
                            tempItem.Qty = 0;
                        if (ele.IsSKUVariation)
                            tempItem.SKU = '';
                        if (ele.IsImageVariation)
                            tempItem.Image = '';
                        tempItem.InStock = false;
                        tempVar.push(tempItem);


                    }

                    this.sepVariationArray.push(tempVar);
                })

                this.productModel.seperateVariation.VariationArray = this.sepVariationArray;
                //this.variationImageArray = this.generateVariationImageArray(items);
            }
        }

        else if (items.length > 0) {
            this.productModel.useDefaultPrice = !(items[0].IsPriceVariation);
            this.productModel.useDefaultQuantity = !(items[0].IsQuantityVariation);
            this.productModel.useDefaultSKU = !(items[0].IsSKUVariation);

            items.forEach(ele => {
                nameModel.push(ele.Name);
                valueModel.push(ele.Value);
            });
            this.productModel.seperateVariation.status = false;
            for (var i = 0; i < valueModel[0].length; i++) {
                var tempItem = <any>{};

                tempItem.Active = true;
                tempItem.Name1 = nameModel[0];
                //tempItem.Name2 = nameModel[1];
                tempItem.Value1 = valueModel[0][i];
                //tempItem.Value2 = valueModel[0][j];
                if (items[0].IsPriceVariation)
                    tempItem.Price = 0;
                if (items[0].IsQuantityVariation)
                    tempItem.Qty = 0;
                if (items[0].IsSKUVariation)
                    tempItem.SKU = '';
                if (items[0].IsImageVariation)
                    tempItem.Image = '';

                tempItem.InStock = false;

                var check = variationModel.find(ele => ele.Value1 === tempItem.Value1)
            
                if (!check) {
                    variationModel.push(tempItem);
                } 
              
            }
            var temparr= []
            var varQty = 0
            for (var i = 0; i < valueModel[0].length; i++) {
                var check1 = variationModel.find(ele => ele.Value1 === valueModel[0][i])
                if(check1){
                    varQty += check1.Qty;
                    if (items[0].IsImageVariation){
                        check1.Image = check1.Image ? check1.Image : '';
                    }else{
                        delete check1.Image
                    }
                    temparr.push(check1)
                }
            }
            variationModel = temparr

            this.productModel.VariationQuantityAvailable = varQty;
            this.productModel.Variations = variationModel;
            this.variationPriceArray = variationModel;
            // this.variationImageArray = variationModel;
        }
    }

    // generateVariationImageArray(items)
    // {
    //     var nameModel = [];
    //     var valueModel = [];
    //     var variationImageArray =[];

    //     items.forEach(ele => {
    //         nameModel.push(ele.Name);
    //         valueModel.push(ele.Value);
    //     });

    //     for (var i = 0; i < valueModel[0].length; i++) {
    //         for (var j = 0; j < valueModel[1].length; j++) {
    //             var tempItem = <any>{};

    //             tempItem.Active = true;
    //             tempItem.Name1 = nameModel[0];
    //             tempItem.Name2 = nameModel[1];
    //             tempItem.Value1 = valueModel[0][i];
    //             tempItem.Value2 = valueModel[0][j];
    //             if (items[0].IsPriceVariation)
    //                 tempItem.Price = 0;
    //             if (items[0].IsQuantityVariation)
    //                 tempItem.Qty = 0;
    //             if (items[0].IsSKUVariation)
    //                 tempItem.SKU = '';
    //             tempItem.InStock = false;



    //             variationImageArray.push(tempItem);
    //         }
    //     }


    // }

    RefreshShippingEditModel() {
        this.shippingprofileModelCustom = <ViewModels.IShippingProfileViewModel>{};
        this.shippingprofileModelCustom.IsCustom = true;
        this.shippingprofileModelCustom.ShippingCountries = [];
        this.shippingprofileModelCustom.Detail = <ViewModels.IShippingDetailViewModel[]>[];
        this.ShipsToChanged();

    }

    public Init() {
        //console.log("init func called");
        this.shippingprofileModel = <ViewModels.IShippingProfileViewModel>{};
        this.shippingprofileModel.IsCustom = true;

        this.shippingprofileModel.ShipsFrom = "";
        this.shippingprofileModel.ShippingCountries = [];
        this.shippingprofileModel.Detail = <ViewModels.IShippingDetailViewModel[]>[];
        this.RefreshShippingEditModel();
        this.productModel = <any>{};
        this.productModel1 = <any>{};
        this.productModel.Name = '';
        this.productModel.useVariation = false;
        this.productModel.useDefaultPrice = true;
        this.productModel.useDefaultQuantity = true;
        this.productModel.useDefaultSKU = true;
        this.productModel.productSKU = '';
        this.productModel.Description = '';
        this.productModel.IsNew = true;
        this.SubCategoryList = [];
        this.productModel.Categories = <any[]>[];
        this.productModel.Categories[0] = <any>{};
        this.productModel.Categories[0].SubCategories = <any[]>[];
        this.productModel.Categories[0].SubCategories[0] = <any>{};
        this.productModel.seperateVariation = <any>{};
        this.productModel.Attributes = <any[]>[];
        this.productModel.Media = <any[]>[];
        this.productModel.Variations = <any[]>[];
        this.productModel.VariationList = <any[]>[];
        this.productModel.Images = <any>[];
        this.productModel.QuantityAvailable = 0;
        this.productModel.VariationQuantityAvailable = 0;
        this.productModel.Dimensions = {};
        this.homeMessage = this.productModel.Description;

        //this.ProductSKU = '';
        // this.productModel.Images.push("https://s3.amazonaws.com/zgroobucket/ForHer_1500886201.png");
        this.getCategoryList();

        // this.getAllZones();
        // this.ShipsToChanged()
        // this.checkCODAvailable();
        // this.Editor.stylesSet.add('my_styles', [
        //     {
        //       element: 'ul',
        //       name: 'BulletedList',
        //       styles: { 'list-style-type': 'circle' }

        //     }
        //   ]);
        // this.Editor.config.stylesSet = 'my_styles';

    }


    checkCODAvailable() {
        return this.shopService.checkCODAvailable().then(res => {
            var data = JSON.parse(res);
            this.codAvailable = data.CODAvailable;
            return data;
        },
            error => {
                this.codAvailable = false;
                return { data: 'Empty' };
            });
    }


    //Method to toggle the variation price and normal price sections
    UseVariationChanged() {
        if (this.productModel.useVariation) {

            this.generateVariationModel(this.VariationList);
            jQuery("#showAvailbleVariation").modal("show");

        }
        else {
            this.productModel.useDefaultPrice = true;
            this.productModel.useDefaultQuantity = true;
            this.productModel.useDefaultSKU = true;
        }


    }

    //Method to Fill the check box
    AutoFillVariations(items, value) {

        items.forEach(ele => {
            if (ele.Active)
                ele.Select = value;
        })
        // console.log('Inside autoFill');
    }

    //Method to Fill the values
    AutoFill(item, value, index?) {
        this.AutoFillArrayPrice = '';
        this.AutoFillArrayQty = '';
        this.AutoFillArraySKU = '';
        this.AutoFillValue = '';
        var self = this;
        setTimeout(() => {
            self.AutoFillArray = item;
            self.AutoFillValue = value;
            self.AutoFillIndex = index ? index : 0;

            jQuery("#showAutoFillVariation").modal("show");
        }, 100)
    }

    //Method to Save the autoFill
    SaveAutoFill() {
        if (this.AutoFillValue == 'Price') {
            if (this.AutoFillArray == 'variation') {
                let selectedArray = this.variationPriceArray.filter(x => x.Select == true);
                selectedArray.forEach(ele => {
                    ele.Price = this.AutoFillArrayPrice;
                })
            }

            else if (this.AutoFillArray == 'seperateVariation' && this.AutoFillIndex == 0) {
                let selectedArray = this.productModel.seperateVariation.VariationArray[0].filter(x => x.Select == true);
                selectedArray.forEach(ele => {
                    ele.Price = this.AutoFillArrayPrice;
                })
            }

            else {
                let selectedArray = this.productModel.seperateVariation.VariationArray[1].filter(x => x.Select == true);
                selectedArray.forEach(ele => {
                    ele.Price = this.AutoFillArrayPrice;
                })
            }
        }



        else if (this.AutoFillValue == 'Qty') {
            if (this.AutoFillArray == 'variation') {
                let selectedArray = this.variationPriceArray.filter(x => x.Select == true);
                selectedArray.forEach(ele => {
                    this.productModel.VariationQuantityAvailable -= ele.Qty;
                    this.productModel.VariationQuantityAvailable += this.AutoFillArrayQty;
                    ele.Qty = this.AutoFillArrayQty;
                    if (this.AutoFillArrayQty > 0) {
                        ele.InStock = true;
                    }
                    else {
                        ele.InStock = false;
                    }
                })
            }

            else if (this.AutoFillArray == 'seperateVariation' && this.AutoFillIndex == 0) {
                let selectedArray = this.productModel.seperateVariation.VariationArray[0].filter(x => x.Select == true);
                selectedArray.forEach(ele => {
                    this.productModel.VariationQuantityAvailable -= ele.Qty;
                    this.productModel.VariationQuantityAvailable += this.AutoFillArrayQty;
                    ele.Qty = this.AutoFillArrayQty;
                    if (this.AutoFillArrayQty > 0) {
                        ele.InStock = true;
                    }
                    else {
                        ele.InStock = false;
                    }
                })
            }

            else {
                let selectedArray = this.productModel.seperateVariation.VariationArray[1].filter(x => x.Select == true);
                selectedArray.forEach(ele => {
                    this.productModel.VariationQuantityAvailable -= ele.Qty;
                    this.productModel.VariationQuantityAvailable += this.AutoFillArrayQty;
                    ele.Qty = this.AutoFillArrayQty;
                    if (this.AutoFillArrayQty > 0) {
                        ele.InStock = true;
                    }
                    else {
                        ele.InStock = false;
                    }
                })
            }
        }



        else {
            if (this.AutoFillArray == 'variation') {
                let selectedArray = this.variationPriceArray.filter(x => x.Select == true);
                selectedArray.forEach(ele => {
                    ele.SKU = this.AutoFillArraySKU;
                })
            }

            else if (this.AutoFillArray == 'seperateVariation' && this.AutoFillIndex == 0) {
                let selectedArray = this.productModel.seperateVariation.VariationArray[0].filter(x => x.Select == true);
                selectedArray.forEach(ele => {
                    ele.SKU = this.AutoFillArraySKU;
                })
            }

            else {
                let selectedArray = this.productModel.seperateVariation.VariationArray[1].filter(x => x.Select == true);
                selectedArray.forEach(ele => {
                    ele.SKU = this.AutoFillArraySKU;
                })
            }
        }



        jQuery("#showAutoFillVariation").modal("hide");

    }


    CheckVariationSelected(items, name, index?) {
        if (name == 'variation') {
            this.fillAllVariations = items.some(obj => obj.Select == true);
        }

        else if (name == 'seperateVariation') {
            this.fillAllSepVariations[index] = items.some(obj => obj.Select == true);
        }

        // else{
        //     this.fillAllVariations[1] = items.some(obj=> obj.Select == true);
        // }
    }

    addEmptyShippingMethod(detail) {

        var methodModel = <ViewModels.IShippingMethodViewModel>{};
        // methodModel.Name = "";
        methodModel.ProcessingTime = "";
        methodModel.Default = false;
        methodModel.ShipsTo = [];
        this.addEmptyShipsToByMethod(methodModel);
        detail.ShippingMethod.push(methodModel);
        // console.log("ShippingMethod on Empty" + this.shippingprofileModel.ShippingMethod);
        // this.shippingprofileModel.ShippingMethod[this.shippingprofileModel.ShippingMethod.length - 1].ShipsTo = [];
        // this.addEmptyShipsToByMethod(this.shippingprofileModel.ShippingMethod[this.shippingprofileModel.ShippingMethod.length - 1]);

    }
    public getAllZones() {
        this.zoneService.GetZones()
            .then(
                zone => {
                    this.ZoneList = JSON.parse(zone);
                    console.log(this.ZoneList.Items);
                    var listItems = this.ZoneList.Items;
                    listItems.forEach(ele => {
                        this.CountryData.push(ele.Country);
                        //this.Zones.push(ele.Zone);
                    });
                    console.log(this.CountryData);
                    this.dataAvailable = true;
                },
                error => this.errorMessage = <any>error);
    }
    public getAllCountries() {
        this.utilService.getCountriesList().then(countries => {
            this.allCountries = JSON.parse(countries);
            //console.log("First Country" + this.allCountries);
            // this.allCountries.forEach(ele => {
            //     this.allCountriesList.push(ele.name);

            // });
            //console.log("Countries" + this.allCountriesList);
        },
            error => this.errorMessage = <any>error);
        //console.log(this.allCountries);
    }
    public getAllCitiesList() {
        this.utilService.getAllCitiesList().then(cities => {
            this.allCountries = JSON.parse(cities);
            this.allCountriesList = this.allCountries;




            // this.allCities = JSON.parse(cities);
            // this.allCitiesList=  this.allCities
            //console.log("First Country" + this.allCountries);

            //console.log("Countries" + this.allCountriesList);
        },
            error => this.errorMessage = <any>error);
        //console.log(this.allCountries);
    }

    public getCategoryList() {
        this.categoryService.getCategoryInTreeView()
            .then(
                categories => {
                    this.CategoriesList = JSON.parse(categories);
                    if (this.CategoriesList.length > 0) {
                        this.CategoriesList.forEach(category => {
                            this.categoriesToSelect.push({ label: category.Parent.Name, value: { Id: category.Parent._id, subCategory: category.Children } })
                        })

                    }
                    console.log("Categories");
                    console.log(this.categoriesToSelect);
                    this.getSectionList();
                },
                error => this.errorMessage = <any>error);
    }

    public getSectionList() {
        this.sectionService.getAll(0)
            .then(
                sections => {
                    this.SectionsList = JSON.parse(sections);
                    this.Sections = this.SectionsList.Items;
                    this.getShippingProfileList();
                    //console.log("Add Product Sections " ,this.Sections);
                },
                error => this.errorMessage = <any>error);
    }

    public getShippingProfileList() {
        this.shippingProfileService.getAll()
            .then(
                shippingprofiles => {
                    this.ShippingProfilesList = JSON.parse(shippingprofiles);
                    this.ShippingProfiles = this.ShippingProfilesList.Profiles;
                    //console.log("Add Product ShippingProfiles ", this.ShippingProfiles);
                    //this.addEmptySubProfile();
                    this.route.queryParams.subscribe(params => {
                        if (params['ProductId']) {
                            console.log(params['ProductId'])
                            this.productService.getProductById(params['ProductId']).then(
                                product => {
                                    this.productEditModel = JSON.parse(product);
                                    console.log("model", JSON.stringify(this.productEditModel));
                                    this.productModel = this.productEditModel;
                                    this.productModel1 = JSON.parse(product);
                                    this.assignValuesToModelForEdit();
                                },
                                error => this.errorMessage = <any>error);
                            this.editbutton = true;
                            this.newbutton = false;
                        }
                    });
                },
                error => this.errorMessage = <any>error);
    }

    assignValuesToModelForEdit() {
        this.selected = this.productEditModel.Show;
        this.ShippingProfiles.forEach(shippingProfile => {
            if (shippingProfile._id == this.productEditModel.ShippingProfile) {
                this.shippingprofileModel = shippingProfile;
            }
        });

        // Shop Section Start
        if (this.productModel.ShopSection) {
            this.productModel.ShopSection = this.Sections.filter(section => section._id == this.productModel.ShopSection.Id)[0];
            if (this.productModel.ShopSection) {
                this.productModel.ShopSection.Id = this.productModel.ShopSection._id;
            }
        }

        // Shop Section End
        console.log(this.CategoriesList);
        //Category Start
        let userSelectedCategory = this.CategoriesList.filter(
            category => category.Parent._id === this.productModel.Categories[0].Id)[0];
        console.log("userSelectedCategory", userSelectedCategory);
        let selectedSubCategory = this.productModel.Categories[0].SubCategories[0];
        this.productModel.Categories[0] = userSelectedCategory.Parent;
        this.categorySelected = { Id: userSelectedCategory.Parent._id, subCategory: userSelectedCategory.Children };
        this.productModel.Categories[0].Id = this.productModel.Categories[0]._id;
        if (userSelectedCategory.Children.length == 0)
            this.showSelectCategoriesSection = false;
        //Category End

        //SubCategory Start
        this.productModel.Categories[0].SubCategories = <any[]>[];
        this.productModel.Categories[0].SubCategories[0] = <any>{};
        this.subCategoriesToSelect = [];

        this.SubCategoryList = this.CategoriesList.filter(category => category.Parent._id == this.productModel.Categories[0]._id)[0].Children;
        if (this.SubCategoryList.length > 0) {
            this.SubCategoryList.forEach(subCat => {
                this.subCategoriesToSelect.push({ label: subCat.Name, value: subCat });
            })



            var userSelectedSubCategory = this.SubCategoryList.filter(
                category => category._id === selectedSubCategory._id)[0];

            this.subCategorySelected = userSelectedSubCategory;
            this.productModel.Categories[0].SubCategories[0] = userSelectedSubCategory;
            if (this.productModel.Categories[0].SubCategories[0]) {
                this.addToSubCategoriesArray();
                this.showSelectCategoriesSection = false;
            }

            // this.productModel.Categories[0].SubCategories[0].Id = this.productModel.Categories[0].SubCategories[0]._id;
            else {
                // Object.assign(this.AttributesList, this.productModel.Categories[0].Attributes);
                this.productModel.Categories[0].Attributes.forEach(attribute => {
                    var check = this.AttributesList.find(ele => ele.Name === attribute.Name)
                    if (!check) {
                        attribute.selectedAttribute = [];
                        var newArr = [];
                        attribute.Values.forEach(element => {
                            newArr.push(element.Value);
                        })
                        attribute.Values = newArr;
                        this.AttributesList.push(attribute);
                    }
                })
                // this.addToCategoriesArray();
                this.productModel.Categories[0].SubCategories[0] = <any>{};
                this.subCategoriesToSelect = [];
                this.subCategorySelected = null;
                this.showSelectCategoriesSection = false;
            }


        }


        //console.log(JSON.stringify(this.productModel.Attributes));
        //SubCategory End

        //Attributes Start
        // this.AttributesList = this.productModel.Attributes;
        if (userSelectedCategory.Children.length != 0) {
            var model1 = {
                CategoryId: userSelectedCategory.Children[0]._id
            }
        } else {
            var model1 = {
                CategoryId: userSelectedCategory.Parent.Id
            }
        }

        this.attributeService.fetchAttributes(model1).then
            (result => {
                var results = JSON.parse(result);
                results.forEach(attribute => {
                    var check = this.AttributesList.find(ele => ele.Name === attribute.Name)
                    if (!check) {
                        attribute.selectedAttribute = [];
                        var newArr = [];
                        attribute.Values.forEach(element => {
                            newArr.push(element.Value);
                        })
                        attribute.Values = newArr;
                        this.AttributesList.push(attribute);
                    }
                })
                this.productModel.Attributes.forEach(userSelectedAttribute => {
                    let filteredAttribute = this.AttributesList.findIndex(attribute => attribute.Name === userSelectedAttribute.Name);
                    // if (filteredAttribute =! null || filteredAttribute != undefined) {
                    this.AttributesList[filteredAttribute].selectedAttribute = userSelectedAttribute.Value;
                    this.AttributesList[filteredAttribute].ShowAttribute = userSelectedAttribute.ShowAttribute;

                    //         console.log("Attributes ");
                    // console.log( filteredAttribute);
                    // console.log("Selectec Attributes ");
                    // console.log( filteredAttribute.selectedAttribute);
                    // if (this.productModel.Variation.Name == userSelectedAttribute.Name) {
                    //     //this.attributeVariation = filteredAttribute;
                    // }
                    // }

                });
            },
                error => {
                    this.errorMessage = <any>error;
                    this.toaster.ErrorSnack('Attribute already exists');
                    console.log("error ", this.errorMessage)
                });




        //Attributes Start
        this.onProfileSelected(this.productModel.ShippingProfile);
        //this.temparray = this.productModel.Variation.Items;
        //this.productSKU = this.productModel.ProductSKU;
        this.VariationList = this.productModel.VariationList;
        //this.generateVariationModel(this.VariationList);
        this.productAttributesList = this.productModel.Attributes;
        var locarr;
        this.productAttributesList.forEach(v => {
            var exist = this.showAttribute.find(x => x == v.Name)
            if (!exist && v.ShowAttribute) {
                this.showAttribute.push(v.Name);
            } 
        })

        if (!this.productModel.seperateVariation.status)
            this.variationPriceArray = this.productModel.Variations;
        //console.log("Variations Array" + this.variationPriceArray);
        //this.convertToBase64FromUrl();

        //        if(this.productModel.Media){
        //            this.productModel.Media.forEach(mediaFile => {
        //               this.productModel.Images.push(mediaFile.Image); 
        //            });
        //        }

    }

    // convertToBase64FromUrl() {
    //     jQuery(".loader-mask").css("visibility", "visible");
    //     this.tempImageBase64 = [];
    //     this.utilService.getBase64FromUrl(this.productModel.Media[0].Image).then(
    //         response => {
    //             let temp = JSON.parse(response);
    //             var binary = '';
    //             var bytes = new Uint8Array(temp.ImageData.data);
    //             var len = bytes.byteLength;
    //             for (var i = 0; i < len; i++) {
    //                 binary += String.fromCharCode(bytes[i]);
    //             }
    //             let base64 = "data:image/png;base64," + window.btoa(binary);
    //             this.tempImageBase64.push(base64);
    //             jQuery(".loader-mask").css("visibility", "hidden");
    //         },
    //         error => {
    //         }
    //     );
    // }

    convertToBase64FromImage(image) {
        var defer = Q.defer();
        //jQuery(".loader-mask").css("visibility", "visible");

        this.utilService.getBase64FromUrl(image).then(
            response => {
                let temp = JSON.parse(response);
                var binary = '';
                var bytes = new Uint8Array(temp.ImageData.data);
                var len = bytes.byteLength;
                for (var i = 0; i < len; i++) {
                    binary += String.fromCharCode(bytes[i]);
                }
                let base64 = "data:image/png;base64," + window.btoa(binary);
                defer.resolve(base64);

            },
            error => {
                this.toaster.ErrorSnack("Problem in uploading the image. Please try again");

            }
        );

        return defer.promise;
    }

    ResetVariationList() {
        this.sepVariationArray = [];
        this.variationPriceArray = [];
        this.productModel.seperateVariation.VariationArray = [];
        this.fillAllVariations = false;
        this.fillAllSepVariations = [false, false];
        this.productModel.VariationQuantityAvailable = 0;
        this.productModel.useVariation = false;
    }

    addToCategoriesArray() {
        this.productAttributesList = [];
        this.ResetVariationList();
        // if(this.variationPriceArray.length > 0)
        // {
        //     this.variationPriceArray.forEach(ele=>{
        //         this.productModel.QuantityAvailable -= ele.Qty;
        //     });
        //     this.variationPriceArray =[];
        // }
        if (!this.attriadddetect) {
            this.productModel.Attributes = [];
            this.AttributesList = [];
        }
        this.VariationList = [];
        //this.variationPriceArray =[];
        this.productModel.Categories[0].Id = this.productModel.Categories[0]._id;
        this.SubCategoryList = this.CategoriesList.filter(category => category.Parent._id == this.productModel.Categories[0].Id)[0].Children;
        this.subCategoriesToSelect = [];
        if (this.SubCategoryList.length > 0) {
            this.productModel.Categories[0].SubCategories = <any[]>[];
            this.productModel.Categories[0].SubCategories[0] = <any>{};
            this.SubCategoryList.forEach(subCat => {
                this.subCategoriesToSelect.push({ label: subCat.Name, value: subCat });
            })
        }

        var CategoryId;
        var model = <any>{};
        if (this.subCategorySelected) {
            CategoryId = this.subCategorySelected._id;
        } else if (this.categorySelected) {
            CategoryId = this.categorySelected.Id;
        }
        model.CategoryId = CategoryId;
        this.attributeService.fetchAttributes(model).then
            (result => {
                var results = JSON.parse(result);
                results.forEach(attribute => {
                    attribute.selectedAttribute = [];
                    var newArr = [];
                    attribute.Values.forEach(element => {
                        newArr.push(element.Value);
                    })
                    attribute.Values = newArr;
                    this.AttributesList.push(attribute);
                })
                if (this.attriadddetect) {
                    this.route.queryParams.subscribe(params => {
                        if (params['ProductId']) {
                            console.log(params['ProductId'])
                            this.productService.getProductById(params['ProductId']).then(
                                product => {
                                    this.productEditModel = JSON.parse(product);
                                    console.log("model", JSON.stringify(this.productEditModel));
                                    this.productModel = this.productEditModel;
                                    this.assignValuesToModelForEdit();
                                },
                                error => this.errorMessage = <any>error);
                            this.editbutton = true;
                            this.newbutton = false;
                            this.attriadddetect = false;
                        }
                    });
                }
            },
                error => {
                    this.errorMessage = <any>error;
                    this.toaster.ErrorSnack('Attribute already exists');
                    console.log("error ", this.errorMessage)
                });


        //Object.assign(this.AttributesList, this.productModel.Categories[0].Attributes);

        console.log("Category", this.productModel.Categories[0]);
    }

    addToSubCategoriesArray() {
        //this.productAttributesList =[];
        this.AttributesList = [];
        //this.VariationList = [];
        //this.ResetVariationList();
        // Object.assign(this.AttributesList, this.productModel.Categories[0].Attributes);

        // if (this.SubCategoryList.length > 0) {
        //     this.productModel.Categories[0].SubCategories[0].Id = this.productModel.Categories[0].SubCategories[0]._id;
        //     this.productModel.Categories[0].SubCategories[0].Attributes.forEach(attribute => {
        //         attribute.selectedAttribute = [];
        //         this.AttributesList.push(attribute);
        //     });
        // }

        var CategoryId;
        var model = <any>{};
        if (this.subCategorySelected) {
            CategoryId = this.subCategorySelected._id;
        } else if (this.categorySelected) {
            CategoryId = this.categorySelected.Id;
        }
        model.CategoryId = CategoryId;
        this.route.queryParams.subscribe(params => {
            // if (!params['ProductId']) {
            this.attributeService.fetchAttributes(model).then
                (result => {

                    var results = JSON.parse(result);
                    results.forEach(attribute => {
                        var check = this.AttributesList.find(ele => ele.Name === attribute.Name)
                        if (!check) {
                            attribute.selectedAttribute = [];
                            var newArr = [];
                            attribute.Values.forEach(element => {
                                newArr.push(element.Value);
                            })
                            attribute.Values = newArr;
                            this.AttributesList.push(attribute);
                        }
                    })
                },
                    error => {
                        this.errorMessage = <any>error;
                        this.toaster.ErrorSnack('Attribute already exists');
                        console.log("error ", this.errorMessage)
                    });
            // }
        })

        console.log("Sub Category", this.productModel.Categories[0]);
    }

    addToAttributes1(attribute, value) {
        this.productAttributesList = [];
        this.VariationList = [];
        let filteredAttribute = this.productModel.Attributes.filter(filterAttribute => filterAttribute.Name == attribute.Name);
        console.log(filteredAttribute)
        if (filteredAttribute.length > 0) {
            if (filteredAttribute[0].Name == 'length' || filteredAttribute[0].Name == 'Length') {
                filteredAttribute[0].LengthValue = value;
            }
        }
    }
    addToAttributes(attribute, value) {
        console.log(value);
        // if(this.variationPriceArray.length > 0)
        // {
        //     this.variationPriceArray.forEach(ele=>{
        //         this.productModel.QuantityAvailable -= ele.Qty;
        //     });
        //     this.variationPriceArray =[];
        // }
        this.productAttributesList = [];
        this.VariationList = [];
        let filteredAttribute = this.productModel.Attributes.filter(filterAttribute => filterAttribute.Name == attribute.Name);
        console.log(filteredAttribute)

        if (filteredAttribute.length > 0) {
            filteredAttribute[0].Id = attribute._id
            if (attribute.IsSystemDefined) {
                filteredAttribute[0].Value = value;
                // filteredAttribute[0].Value = this.selectedAttribute1;

            } else {
                filteredAttribute[0].Value = value;
                filteredAttribute[0].UnitOfMeasure = value;
                // filteredAttribute[0].UnitOfMeasure = this.selectedAttribute1;

            }
        } else {
            let attributeObject = <any>{};
            attributeObject.Name = attribute.Name;
            attributeObject.IsVariation = false;
            attributeObject.IsPriceVariation = false;
            attributeObject.IsQuantityVariation = false;
            attributeObject.IsSKUVariation = false;
            attributeObject.IsImageVariation = false;
            attributeObject.Id = attribute._id;

            if (attribute.IsSystemDefined) {
                attributeObject.Value = value;
                // attributeObject.Value = this.selectedAttribute1;
            } else {
                attributeObject.Value = value;
                attributeObject.UnitOfMeasure = value;
                // attributeObject.UnitOfMeasure = this.selectedAttribute1;
            }
            //this.productAttributesList.push(attributeObject);
            this.productModel.Attributes.push(attributeObject);
        }
        //this.addDefaultVariation();
        var index = this.productModel.Attributes.findIndex(x => x.Value == null)
        if (index != -1)
            this.productModel.Attributes.splice(index, 1);
        this.productAttributesList = this.productModel.Attributes;
        this.productAttributesList.forEach(ele => {
            if (ele.IsVariation && ele.Value) {
                this.VariationList.push(ele);
            }
        });
        console.log(this.productModel.Categories[0])
        this.generateVariationModel(this.VariationList);
        console.log(this.productAttributesList);
        console.log(this.productModel);
    }

    // removeVariationFromTemp(index) {
    //     this.temparray.splice(index, 1);
    // }
    VariationQuantityChange(event, obj) {
        console.log(event, obj.Qty);
        //this.productModel.VariationQuantityAvailable = 0;
        if (!(this.productModel.VariationQuantityAvailable >= 0)) {
            this.productModel.VariationQuantityAvailable = 0;
        }
        this.productModel.VariationQuantityAvailable -= obj.Qty;
        this.productModel.VariationQuantityAvailable += event;
        obj.Qty = event;
        obj.InStock = obj.Qty > 0;

    }
    variationClicked(obj, id) {




        if (obj.Active) {
            if (obj.Qty && obj.Qty >= 0)
                this.productModel.VariationQuantityAvailable += obj.Qty;
            if (obj.Select)
                jQuery(id).trigger('click');
            //jQuery('#objImageDiv'+i).children().off('click');
        }

        else {
            if (obj.Qty && obj.Qty >= 0)
                this.productModel.VariationQuantityAvailable -= obj.Qty;
            //jQuery('#objImageDiv'+i).children().on('click');
        }



    }
    // addDefaultVariation() {
    //     if (this.attributeVariation) {
    //          this.temparray = [];
    //         let filterSelectedAttribute = this.productModel.Attributes.filter(attribute => attribute.Name == this.attributeVariation.Name)[0];
    //         if (filterSelectedAttribute) {
    //             filterSelectedAttribute.Value.forEach((value, key) => {
    //                 this.variationObject = <any>{};
    //                 this.variationObject.Value = value;
    //                 if (key == 0) {
    //                     //this.variationObject._id = 
    //                     this.variationObject.Price = this.productModel.Price ? parseInt(this.productModel.Price, 10) : 0;
    //                     this.variationObject.Qty = this.productModel.QuantityAvailable ? this.productModel.QuantityAvailable : 0;
    //                     this.variationObject.InStock = this.variationObject.Qty > 0;
    //                 } else {
    //                     this.variationObject.Price = 0;
    //                     this.variationObject.Qty = 0;
    //                     this.variationObject.InStock = this.variationObject.Qty > 0;
    //                 }
    //                 if(this.attributeVariation.hasOwnProperty('_id'))
    //                     this.variationObject._id = this.attributeVariation._id;
    //                 this.temparray.push(this.variationObject);
    //             });
    //             this.productModel.Variation = <any>{};
    //             this.productModel.Variation.Name = this.attributeVariation.Name;
    //             this.productModel.Variation.Items = this.temparray;
    //             console.log(this.temparray);
    //         } else {
    //         }
    //     }

    //     //        let checkDuplicate = this.temparray.filter(addedVariation => addedVariation.Value == this.attributeVariation.selectedAttribute);
    //     //        if(checkDuplicate.length > 0){
    //     //        }
    //     //        else if(this.attributeVariation){
    //     //            this.temparray = [];
    //     //            this.variationObject.Value = this.attributeVariation.selectedAttribute;
    //     //            this.variationObject.Price = this.productModel.Price?this.productModel.Price:0;
    //     //            this.variationObject.Qty = this.productModel.QuantityAvailable?this.productModel.QuantityAvailable:0;
    //     //            this.variationObject.InStock = this.productModel.QuantityAvailable > 0;
    //     //            this.temparray[0] = this.variationObject;
    //     //            this.productModel.Variation = <any>{};
    //     //            this.productModel.Variation.Name = this.attributeVariation.Name;
    //     //            this.productModel.Variation.Items = this.temparray;
    //     //            this.variationObject = {}; 
    //     //        }
    // }

    // addToVariationArray() {
    //     //this.productModel.QuantityAvailable = this.productModel.QuantityAvailable + this.variationObject.Qty;

    //     let checkDuplicate = this.temparray.filter(addedVariation => addedVariation.Value == this.variationObject.Value);
    //     if (checkDuplicate.length == 0) {
    //         this.temparray.push(this.variationObject);
    //         this.productModel.Variation = <any>{};
    //         this.productModel.Variation.Name = this.attributeVariation.Name;
    //         this.productModel.Variation.Items = this.temparray;
    //         this.variationObject = {};
    //     } else {
    //     }
    //     console.log("Product Model ", this.productModel);
    // }

    addToShopSectionArray() {
        this.productModel.ShopSection.Id = this.productModel.ShopSection._id;
    }

    add() {
        this.shippingProfileService.addShippingProfile(this.shippingprofileModelCustom).then(
            response => {
                console.log(response);
                var data = JSON.parse(response);
                this.productModel.ShippingProfile = data._id;
                this.shippingprofileModel = data;
                this.shippingProfileService.getAll()
                    .then(
                        shippingprofiles => {
                            this.ShippingProfilesList = JSON.parse(shippingprofiles);
                            this.ShippingProfiles = this.ShippingProfilesList.Profiles;
                        },
                        error => this.errorMessage = <any>error);
                this.RefreshShippingEditModel();
                this.toaster.SuccessSnack("Shipping Profile Added Sucessfully!!! ");
            },
            error => this.errorMessage = <any>error);
    }

    newProduct() {
        // this.userService.getUserBlock().then(
        //     userInfo => {
        //         var userblock = JSON.parse(userInfo);
        //         if (userblock.msg == true) {
        //             this.toaster.ErrorSnack("Your account is currently blocked");
        //         }
        //         else {
        if (this.productModel.Show == 'Active') {
            if (this.productModel.Images.length == 0) {
                this.toaster.ErrorSnack("Please add atleast one image");
                return false;
            }
            if (this.productAttributesList.length == 0) {
                this.toaster.ErrorSnack("Please select atleast one attribute");
                return false;
            }
            var activeIndex;
            if (this.productModel.useVariation && this.productModel.seperateVariation.status == false) {
                if (this.VariationList.length > 0) {
                    activeIndex = this.variationPriceArray.findIndex(ele => ele.Active == true);
                    if (activeIndex == -1) {
                        this.toaster.ErrorSnack("Please make atleast one variation active");
                        return false;
                    }
                }
                else {
                    this.toaster.ErrorSnack("Please select atleast one variation from the variations check list");
                    return false;
                }
            }

            else if (this.productModel.useVariation && this.productModel.seperateVariation.status == true) {
                if (this.productModel.seperateVariation.VariationArray[0].length > 0) {
                    activeIndex = this.productModel.seperateVariation.VariationArray[0].findIndex(ele => ele.Active == true);
                    if (activeIndex == -1) {
                        this.toaster.ErrorSnack("Please make atleast one variation active in " + this.productModel.seperateVariation.VariationArray[0][0].Name1);
                        return false;
                    }
                }

                if (this.productModel.seperateVariation.VariationArray[1].length > 0) {
                    activeIndex = this.productModel.seperateVariation.VariationArray[1].findIndex(ele => ele.Active == true);
                    if (activeIndex == -1) {
                        this.toaster.ErrorSnack("Please make atleast one variation active in " + this.productModel.seperateVariation.VariationArray[1][0].Name1);
                        return false;
                    }
                }
            }
        }
        if (this.productModel.useVariation) {
            if (this.productModel.seperateVariation.status) {
                var variationModel = [];
                var model = []

                this.productModel.seperateVariation.VariationArray.forEach(ele => {
                    model.push({ value: ele, attribute: this.productAttributesList.find(x => x.Name == ele[0].Name1) })
                });

                for (var i = 0; i < model[0].value.length; i++) {
                    for (var j = 0; j < model[1].value.length; j++) {
                        var tempItem = <any>{};

                        tempItem.Active = model[0].value[i].Active && model[1].value[j].Active;
                        tempItem.Name1 = model[0].value[i].Name1;
                        tempItem.Name2 = model[1].value[j].Name1;
                        tempItem.Value1 = model[0].value[i].Value1;
                        tempItem.Value2 = model[1].value[j].Value1;
                        if (model[0].attribute.IsPriceVariation)
                            tempItem.Price = model[0].value[i].Price;
                        else if (model[1].attribute.IsPriceVariation)
                            tempItem.Price = model[1].value[j].Price;
                        if (model[0].attribute.IsQuantityVariation)
                            tempItem.Qty = model[0].value[i].Qty;
                        else if (model[1].attribute.IsQuantityVariation)
                            tempItem.Qty = model[1].value[j].Qty;
                        if (model[0].attribute.SKU)
                            tempItem.SKU = model[0].value[i].SKU;
                        else if (model[1].attribute.IsSKUVariation)
                            tempItem.SKU = model[1].value[j].SKU;
                        if (model[0].attribute.IsImageVariation)
                            tempItem.Image = model[0].value[i].Image;
                        else if (model[1].attribute.IsImageVariation)
                            tempItem.Image = model[1].value[j].Image;
                        if (tempItem.Qty > 0)
                            tempItem.InStock = true;
                        else
                            tempItem.InStock = false;



                        variationModel.push(tempItem);
                    }
                }


                variationModel.forEach(ele => {
                    if (this.productModel.useDefaultPrice) {
                        ele.Price = this.productModel.Price
                    }

                    if (this.productModel.useDefaultQuantity) {
                        ele.Qty = this.productModel.QuantityAvailable;
                        this.productModel.VariationQuantityAvailable = this.productModel.QuantityAvailable;
                        if (ele.Qty > 0)
                            ele.InStock = true;
                    }

                    if (this.productModel.useDefaultSKU) {
                        ele.SKU = this.productModel.productSKU
                    }
                })

                this.productModel.Variations = variationModel;

            }
            else {
                this.variationPriceArray.forEach(ele => {
                    if (this.productModel.useDefaultPrice) {
                        ele.Price = this.productModel.Price
                    }

                    if (this.productModel.useDefaultQuantity) {
                        ele.Qty = this.productModel.QuantityAvailable;
                        this.productModel.VariationQuantityAvailable = this.productModel.QuantityAvailable;
                        if (ele.Qty > 0)
                            ele.InStock = true;
                    }

                    if (this.productModel.useDefaultSKU) {
                        ele.SKU = this.productModel.productSKU
                    }
                })

                this.productModel.Variations = this.variationPriceArray;
            }

            var index = this.productModel.Variations.findIndex(ele => ele.Active && ele.InStock);
            if (index != -1) {
                this.productModel.Price = this.productModel.Variations[index].Price;
            }
            else {
                index = this.productModel.Variations.findIndex(ele => ele.Active);
                this.productModel.Price = this.productModel.Variations[index].Price;
            }
        }
        this.productModel.VariationList = this.VariationList;
        console.log(this.showAttribute);
        var arr = this.showAttribute;
        this.productAttributesList.map(function (x) {
            var result = arr.filter(a1 => a1 == x.Name);
            if (result.length > 0) {
                x.ShowAttribute = true
            }
            return x
        })
        this.productModel.Attributes = this.productAttributesList;
        this.productModel.ShipsFrom = "India";
        this.productService.setProduct(this.productModel).then(res => {
            var data = JSON.parse(res);
            if (data == null || data == "undefined") {
                this.toaster.ErrorSnack("Product add failed, please try again. ")
            }
            else if (data.Show == "Draft") {
                this.toaster.SuccessSnack("Product Added To The Draft Sucessfully!!! ")
                this.router.navigate(['/myaccounts/seller/listing']);
            }
            else if (data.flag == false){
                this.toaster.ErrorSnack(data.msg) 
            }
            else {
                this.toaster.SuccessSnack("Product Added Sucessfully!!! ")
                this.router.navigate(['/myaccounts/seller/listing']);
            }
        },
        error => {
           
            this.toaster.ErrorSnack(error)
        }
        );
        //     }
        // });
    }

    editProduct() {
        this.userService.getUserBlock().then(
            userInfo => {
                var userblock = JSON.parse(userInfo);
                if (userblock.msg == true) {
                    this.toaster.ErrorSnack("Your account is currently blocked");
                }
                else {
                    if (this.productModel.Show == 'Active') {
                        if (this.productModel.Images.length == 0) {
                            this.toaster.ErrorSnack("Please add atleast one image");
                            return false;
                        }
                        if (this.productAttributesList.length == 0) {
                            this.toaster.ErrorSnack("Please select atleast one attribute");
                            return false;
                        }
                        var activeIndex;
                        if (this.productModel.useVariation && this.productModel.seperateVariation.status == false) {
                            if (this.VariationList.length > 0) {
                                activeIndex = this.variationPriceArray.findIndex(ele => ele.Active == true);
                                if (activeIndex == -1) {
                                    this.toaster.ErrorSnack("Please make atleast one variation active");
                                    return false;
                                }
                            }
                            else {
                                this.toaster.ErrorSnack("Please select atleast one variation from the variations check list");
                                return false;
                            }
                        }

                        else if (this.productModel.useVariation && this.productModel.seperateVariation.status == true) {
                            if (this.productModel.seperateVariation.VariationArray[0].length > 0) {
                                activeIndex = this.productModel.seperateVariation.VariationArray[0].findIndex(ele => ele.Active == true);
                                if (activeIndex == -1) {
                                    this.toaster.ErrorSnack("Please make atleast one variation active in " + this.productModel.seperateVariation.VariationArray[0][0].Name1);
                                    return false;
                                }
                            }

                            if (this.productModel.seperateVariation.VariationArray[1].length > 0) {
                                activeIndex = this.productModel.seperateVariation.VariationArray[1].findIndex(ele => ele.Active == true);
                                if (activeIndex == -1) {
                                    this.toaster.ErrorSnack("Please make atleast one variation active in " + this.productModel.seperateVariation.VariationArray[1][0].Name1);
                                    return false;
                                }
                            }
                        }
                    }


                    //this.productModel.PrimaryImage = this.productModel.Images[0];
                    if (this.productModel.useVariation) {
                        if (this.productModel.seperateVariation.status) {

                            // var nameModel = [];
                            // var valueModel = [];
                            var variationModel = [];
                            var model = []

                            this.productModel.seperateVariation.VariationArray.forEach(ele => {
                                model.push({ value: ele, attribute: this.productAttributesList.find(x => x.Name == ele[0].Name1) })
                            });

                            for (var i = 0; i < model[0].value.length; i++) {
                                for (var j = 0; j < model[1].value.length; j++) {
                                    var tempItem = <any>{};

                                    tempItem.Active = model[0].value[i].Active && model[1].value[j].Active;
                                    tempItem.Name1 = model[0].value[i].Name1;
                                    tempItem.Name2 = model[1].value[j].Name1;
                                    tempItem.Value1 = model[0].value[i].Value1;
                                    tempItem.Value2 = model[1].value[j].Value1;
                                    if (model[0].attribute.IsPriceVariation)
                                        tempItem.Price = model[0].value[i].Price;
                                    else if (model[1].attribute.IsPriceVariation)
                                        tempItem.Price = model[1].value[j].Price;
                                    if (model[0].attribute.IsQuantityVariation)
                                        tempItem.Qty = model[0].value[i].Qty;
                                    else if (model[1].attribute.IsQuantityVariation)
                                        tempItem.Qty = model[1].value[j].Qty;
                                    if (model[0].attribute.SKU)
                                        tempItem.SKU = model[0].value[i].SKU;
                                    else if (model[1].attribute.IsSKUVariation)
                                        tempItem.SKU = model[1].value[j].SKU;
                                    if (model[0].attribute.IsImageVariation)
                                        tempItem.Image = model[0].value[i].Image;
                                    else if (model[1].attribute.IsImageVariation)
                                        tempItem.Image = model[1].value[j].Image;
                                    if (tempItem.Qty > 0)
                                        tempItem.InStock = true;
                                    else
                                        tempItem.InStock = false;



                                    variationModel.push(tempItem);
                                }
                            }


                            variationModel.forEach(ele => {
                                if (this.productModel.useDefaultPrice) {
                                    ele.Price = this.productModel.Price
                                }

                                if (this.productModel.useDefaultQuantity) {
                                    ele.Qty = this.productModel.QuantityAvailable;
                                    // this.productModel.VariationQuantityAvailable += this.productModel.QuantityAvailable;
                                    this.productModel.VariationQuantityAvailable = this.productModel.QuantityAvailable;
                                    if (ele.Qty > 0)
                                        ele.InStock = true;
                                }

                                if (this.productModel.useDefaultSKU) {
                                    ele.SKU = this.productModel.productSKU
                                }
                            })

                            this.productModel.Variations = variationModel;

                        }
                        else {
                            this.variationPriceArray.forEach(ele => {
                                if (this.productModel.useDefaultPrice) {
                                    ele.Price = this.productModel.Price
                                }

                                if (this.productModel.useDefaultQuantity) {
                                    ele.Qty = this.productModel.QuantityAvailable;
                                    this.productModel.VariationQuantityAvailable = this.productModel.QuantityAvailable;
                                    if (ele.Qty > 0)
                                        ele.InStock = true;
                                }

                                if (this.productModel.useDefaultSKU) {
                                    ele.SKU = this.productModel.productSKU
                                }
                            })

                            this.productModel.Variations = this.variationPriceArray;
                        }

                        var index = this.productModel.Variations.findIndex(ele => ele.Active && ele.InStock);
                        if (index != -1) {
                            this.productModel.Price = this.productModel.Variations[index].Price;
                        }
                        else {
                            index = this.productModel.Variations.findIndex(ele => ele.Active);
                            this.productModel.Price = this.productModel.Variations[index].Price;
                        }
                    }

                    this.productModel.VariationList = this.VariationList;
                    //this.productModel.ProductSKU = this.ProductSKU;
                    var arr = this.showAttribute;
                    this.productAttributesList.forEach(function (v) { delete v.ShowAttribute });
                    this.productAttributesList.map(function (x) {
                        var result = arr.filter(a1 => a1 == x.Name);
                        if (result.length > 0) { x.ShowAttribute = true }
                        return x
                    })
                    this.productModel.Attributes = this.productAttributesList;
                    this.productModel.ShipsFrom = "India"
                    this.productService.putProduct(this.productModel).then(
                        response => {

                            this.toaster.SuccessSnack('You have successfully updated the Product');
                            this.router.navigate(['/myaccounts/seller/listing']);
                            jQuery("body").animate({ scrollTop: 0 }, "fast");
                        },
                        error => {
                            // var res = JSON.parse(error);
                            // console.log(res);
                            this.toaster.ErrorSnack(error)
                        }
                    );


                }
            });

    }

    goBack() {
        this.router.navigate(['/myaccounts/seller/listing']);
    }

    /*  Product Image Upload and Edit Helper Functions Start */

    triggerFileUpload() {
        jQuery('#productUploadImage').trigger('click');
    }

    triggerFileUploadVariation(id) {
        jQuery(id).trigger('click');
    }

    changeListenerVariation($event, variaiton, self): void {
        let files = [].slice.call($event.target.files);
        // this.readThisVariation(files, variaiton);
        new ImageCompressor(files[0], {
            quality: .8,
            success(result) {
                console.log(result);
                const formData = new FormData();
                // self.readThisVariation(files[0], variaiton);
                self.readThisVariation(result, variaiton);


                // formData.append('file', result, result.name);

                // Send the compressed image file to server with XMLHttpRequest.

            },
            error(e) {
                console.log(e.message);
            },
        });
    }

    changeListener($event, self): void {
        let files = [].slice.call($event.target.files);
        console.log(files);

        new ImageCompressor(files[0], {
            quality: .8,
            success(result) {
                console.log(result);
                const formData = new FormData();
                // self.readThis(files[0]);
                self.readThis(result);


                // formData.append('file', result, result.name);

                // Send the compressed image file to server with XMLHttpRequest.

            },
            error(e) {
                console.log(e.message);
            },
        });
    }

    cropImageVariation(variation, index) {
        let self = this;

        var originalImage = this.productModel.Media.find(x => x.EditedImage == variation.Image).OriginalImage;
        this.selectedVariationForCropping = variation;
        //this.selectedVariationIndex = index;
        this.cropVariationImage = true;

        var base64Array = [];
        base64Array.push(this.convertToBase64FromImage(originalImage));
        Q.all(base64Array).then(base64data => {

            this.selectedImageForCropping = base64data;
            if (this.croppie) {
                this.croppie.destroy();

            }
            //console.log(this.selectedImageForCropping);
            jQuery("#editImageModal").modal("show");
            setTimeout(function () {
                let image = document.getElementById('EditableImage');
                //console.log("image ", image);
                self.croppie = new Croppie(image, {
                    viewport: {
                        width: 215,
                        height: 172
                    },
                    boundary: { width: 800, height: 600 },
                    enableOrientation: true

                });
                self.croppie.bind({
                    url: self.selectedImageForCropping,
                    zoom: 0
                }).then(function (blob) {
                    // do something with cropped blob

                });
            }, 500);
        })



    }


    cropImage(selectedImage, index) {
        let self = this;

        var originalImage = this.productModel.Media.find(x => x.EditedImage == selectedImage).OriginalImage;
        this.cropNormalImage = selectedImage;
        this.cropVariationImage = false;
        var base64Array = [];
        base64Array.push(this.convertToBase64FromImage(originalImage));
        Q.all(base64Array).then(base64data => {
            this.selectedImageForCropping = base64Array;
            if (this.croppie) {
                this.croppie.destroy();
                // this.croppie.bind({
                //     url:this.selectedImageForCropping,
                //     zoom:0
                // })

                // return;
            }
            jQuery("#editImageModal").modal("show");
            setTimeout(function () {

                let image = document.getElementById('EditableImage');
                //console.log("image ", image);
                self.croppie = new Croppie(image, {
                    viewport: {
                        width: 215,
                        height: 172
                    },
                    boundary: { width: 800, height: 600 },
                    enableOrientation: true

                });
                self.croppie.bind({
                    url: self.selectedImageForCropping,
                    zoom: 0
                }).then(function (blob) {
                    //jQuery("#editImageModal").modal("show");
                    // do something with cropped blob
                });


            }, 500);
        })
        //console.log(this.selectedImageForCropping);convertToBase64FromImage


    }

    makePrimary(i) {
        // console.log(this.productModel);
        //this.productModel.Dimensions = {};

        // var oldMedia = this.productModel.Media.find(x => x.EditedImage == this.productModel.Images[i]);
        // oldMedia.IsPrimary = false;
        this.productModel.Images[0] = this.productModel.Images.splice(i, 1, this.productModel.Images[0])[0];

        var index = this.productModel.Media.findIndex(element => element.EditedImage == this.productModel.Images[0])
        for (var j = 0; j < this.productModel.Media.length; j++) {
            if (j == index) {
                this.productModel.Media[j].IsPrimary = true;
            }
            else {
                this.productModel.Media[j].IsPrimary = false;
            }
        }

        //    oldMedia.IsPrimary = false;

        // var newMedia = this.productModel.Media.find(x => x.EditedImage == this.productModel.Images[0]);
        // newMedia.IsPrimary = true;
        //this.productModel.Media[0] = this.productModel.Media.splice(i, 1, this.productModel.Media[0])[0];
        //this.convertToBase64FromUrl();
        //this.tempImageBase64[0] = this.tempImageBase64.splice(i, 1, this.tempImageBase64[0])[0];
        // if (this.croppie) {
        //     this.croppie.destroy();
        // }
        console.log(this.productModel);
    }

    cropperImageReset() {
        this.cropper.reset();
    }

    cropperImageZoomIn() {
        let croppieZoom = this.croppie.get();
        this.zoomRatio = croppieZoom.zoom;
        console.log(croppieZoom.zoom, this.zoomRatio)
        if (croppieZoom.zoom >= this.zoomRatio) {
            this.zoomRatio = this.zoomRatio + 0.1;
            console.log(this.zoomRatio);
            console.log();
            this.croppie.setZoom(this.zoomRatio);
        }
    }

    cropperImageZoomOut() {
        let croppieZoom = this.croppie.get();
        this.zoomRatio = croppieZoom.zoom;
        if (this.zoomRatio > 0) {
            this.zoomRatio = this.zoomRatio - 0.1;
            console.log(this.zoomRatio);
            this.croppie.setZoom(this.zoomRatio);
        }
    }
    cropperImageRotateLeft() {
        this.croppie.rotate(-90);
    }
    cropperImageRotateRight() {
        this.croppie.rotate(90);
    }

    saveCroppedImage() {

        if (this.cropVariationImage) {
            let self = this;
            this.selectedVariationForCropping.loader = true;
            // this.productModel.Dimensions = {};
            // this.productModel.Dimensions = this.imageTransitions;

            let media = this.productModel.Media.find(x => x.EditedImage == this.selectedVariationForCropping.Image);

            jQuery("#editImageModal").modal("hide");
            this.croppie.result('base64', 'viewport', 'png', 1, false).then(function (response) {
                self.utilService.uploadBase64AndGetUrlproduct(media.Name, response,"products").then(
                    response => {



                        var index = self.productModel.Media.findIndex(x => x.Image == self.selectedVariationForCropping.Image);
                        if (index != -1) {
                            self.productModel.Media.splice(index, 1);
                        }
                        media.EditedImage = JSON.parse(response).ImageData;
                        //self.productModel.Dimensions.editedImage = JSON.parse(response).ImageData;
                        self.selectedVariationForCropping.Image = JSON.parse(response).ImageData;
                        //console.log(self.productModel);

                        self.selectedVariationForCropping.loader = false
                        self.toaster.SuccessSnack("Edited image uploaded Successfully");
                    },
                    error => {
                        self.toaster.ErrorSnack("Problem in uploading the image. Please try again");
                    }
                );
            });
        }

        else {
            let self = this;
            this.loader = true;
            // this.productModel.Dimensions = {};
            // this.productModel.Dimensions = this.imageTransitions;
            let media = this.productModel.Media.find(x => x.EditedImage == this.cropNormalImage);
            var index = this.productModel.Images.findIndex(x => x == this.cropNormalImage);
            if (index != -1) {
                this.productModel.Images.splice(index, 1);
            }
            jQuery("#editImageModal").modal("hide");
            this.croppie.result('base64', 'viewport', 'png', 1, false).then(function (response) {
                self.utilService.uploadBase64AndGetUrlproduct(media.Name, response,"products").then(
                    response => {
                        // self.productModel.Dimensions.editedImage = JSON.parse(response).ImageData;
                        self.productModel.Images.push(JSON.parse(response).ImageData);
                        media.EditedImage = JSON.parse(response).ImageData;
                        // self.productModel.Media.push({
                        //     "Type": "image",
                        //     "Image": JSON.parse(response).ImageData,
                        //     "Name": JSON.parse(response).Name
                        // });

                        console.log(self.productModel)
                        self.loader = false
                        self.toaster.SuccessSnack("Edited image uploaded Successfully");
                    },
                    error => {
                        self.toaster.ErrorSnack("Problem in uploading the image. Please try again");
                    }
                );
            });
        }

    }


    readThisVariation(inputValue: any, variation): any {


        this.showSaveButton = false;
        variation.loader = true;
        var file: File = inputValue;
        let fileName = file.name.split(".")[0]
        this.utilService.convertToBase64(file);
        console.log(file);
        let count = 0;
        this.utilService.invokeEvent.subscribe(value => {
            if (count == 0) {
                if (value == "error") {
                    this.toaster.ErrorSnack("Upload images with above resolution 750 X 500!!! ")
                    variation.loader = false;
                    this.showSaveButton = true;
                } else {
                    this.utilService.uploadBase64AndGetUrlproduct(fileName, value,"products").then(
                        response => {
                            //console.log(this.productModel.Images, JSON.parse(response).ImageData);

                            //variation.ImageData = value;
                            var originalImage = JSON.parse(response).ImageData;


                            // if (this.croppie) {
                            //     this.croppie.destroy();
                            // }

                            this.selectedImageForCropping = value;
                            var self = this;
                            setTimeout(function () {
                                let imageObj = <HTMLImageElement>document.getElementById('EditableImage');
                                //console.log("image ", imageObj);
                                var c = document.createElement('canvas');
                                var iw = 215;
                                var ih = 172;
                                c.width = iw;
                                c.height = ih;
                                var ctx = c.getContext('2d');

                                ctx.drawImage(imageObj, 0, 0, iw, ih);
                                var scaledImg = new Image();
                                scaledImg.onload = function () {
                                    // scaledImg is a scaled imageObject for upload/download
                                    // For testing, just append it to the DOM
                                    document.body.appendChild(scaledImg);
                                }
                                // console.log(c.toDataURL());

                                //window.open(c.toDataURL());
                                self.utilService.uploadBase64AndGetUrlproduct(fileName, c.toDataURL(),"products").then(response => {
                                    //console.log("Dimention image", response);       
                                    // self.productModel.Dimensions = {};
                                    // self.productModel.Dimensions.editedImage = JSON.parse(response).ImageData;
                                    //console.log(JSON.parse(response).ImageData)

                                    // variation.Image = JSON.parse(response).ImageData;
                                    variation.Image = originalImage;

                                    self.productModel.Media.push({
                                        "Type": "image",
                                        // "EditedImage": JSON.parse(response).ImageData,
                                        "EditedImage": originalImage,
                                        "OriginalImage": originalImage,
                                        "Name": JSON.parse(response).Name
                                    });

                                    variation.loader = false;
                                    self.showSaveButton = true;
                                    self.toaster.SuccessSnack("Image uploaded Successfully");
                                });
                            }, 0);


                            //console.log(this.productModel);

                        },
                        error => {
                            this.toaster.ErrorSnack("Problem in uploading the image. Please try again");
                            variation.loader = false;
                            this.showSaveButton = true;
                        }
                    );

                }
                count++;
            }
        });
    }


    readThis(inputValue: any): any {
        this.loader = true;
        var file: File = inputValue;
        let fileName = file.name.split(".")[0]
        this.utilService.convertToBase64(file);
        console.log(file);
        let count = 0;
        this.utilService.invokeEvent.subscribe(value => {
            if (count == 0) {
                if (value == "error") {
                    this.toaster.ErrorSnack("Upload images with above resolution 750 X 500!!! ")
                    this.loader = false;
                } else {
                    this.utilService.uploadBase64AndGetUrlproduct(fileName, value,"products").then(
                        response => {
                            console.log(this.productModel.Images, JSON.parse(response).ImageData);
                            if (this.productModel.Images.indexOf(JSON.parse(response).ImageData) == -1) {
                                //this.tempImageBase64.push(value);

                                var originalImage = JSON.parse(response).ImageData;

                                // if (this.croppie) {
                                //     this.croppie.destroy();
                                // }

                                this.selectedImageForCropping = value;
                                var self = this;
                                setTimeout(function () {
                                    let imageObj = <HTMLImageElement>document.getElementById('EditableImage');
                                    console.log("image ", imageObj);
                                    var c = document.createElement('canvas');
                                    var iw = 215;
                                    var ih = 172;
                                    c.width = iw;
                                    c.height = ih;
                                    var ctx = c.getContext('2d');

                                    ctx.drawImage(imageObj, 0, 0, iw, ih);
                                    var scaledImg = new Image();
                                    scaledImg.onload = function () {
                                        // scaledImg is a scaled imageObject for upload/download
                                        // For testing, just append it to the DOM
                                        document.body.appendChild(scaledImg);
                                    }
                                    console.log(c.toDataURL());

                                    //window.open(c.toDataURL());
                                    self.utilService.uploadBase64AndGetUrlproduct(fileName, c.toDataURL(),"products").then(response => {
                                        //console.log("Dimention image", response); 
                                        // self.productModel.Images.push(JSON.parse(response).ImageData);
                                        self.productModel.Images.push(originalImage);

                                        if (self.productModel.Media.length == 0) {
                                            self.productModel.Media.push({
                                                "Type": "image",
                                                "OriginalImage": originalImage,
                                                // "EditedImage": JSON.parse(response).ImageData,
                                                "EditedImage": originalImage,
                                                "Name": JSON.parse(response).Name,
                                                "IsPrimary": true
                                            });
                                        }

                                        else {
                                            self.productModel.Media.push({
                                                "Type": "image",
                                                "OriginalImage": originalImage,
                                                "EditedImage": originalImage,
                                                // "EditedImage": JSON.parse(response).ImageData,
                                                "Name": JSON.parse(response).Name,
                                                "IsPrimary": false
                                            });
                                        }
                                        self.loader = false;
                                        self.toaster.SuccessSnack("Image uploaded Successfully");
                                        // self.productModel.Dimensions = {};
                                        // self.productModel.Dimensions.editedImage = JSON.parse(response).ImageData;
                                        //console.log(JSON.parse(response).ImageData)
                                    });
                                }, 0);


                                console.log(this.productModel);
                            }
                        },
                        error => {
                            this.toaster.ErrorSnack("Problem in uploading the image. Please try again");
                            this.loader = false;
                        }
                    );

                }
                count++;
            }
        });
    }

    removeImage(i, image) {
        this.productModel.Images.splice(i, 1);

        var index = this.productModel.Media.findIndex(x => x.EditedImage == image);
        if (index != -1) {
            this.productModel.Media.splice(index, 1);
        }

        if (this.productModel.Images.length > 0) {
            var newPrimary = this.productModel.Images[0];
            let media = this.productModel.Media.find(x => x.EditedImage == newPrimary);
            media.IsPrimary = true;
        }

        // if (this.croppie) {
        //     this.croppie.destroy();
        // }
    }


    removeImageVariation(obj) {

        var index = this.productModel.Media.findIndex(x => x.EditedImage == obj.Image);
        if (index != -1) {
            this.productModel.Media.splice(index, 1);
        }

        obj.Image = '';
    }
    /*  Product Image Upload and Edit Helper Functions End */

    /*  Shipping Profile Helper Functions Start */



    public addEmptyShipsToByMethod(shippingMethod) {

        var shipsToModel = <ViewModels.IShipsToViewModel>{};
        // shipsToModel.Zone = "6";
        shipsToModel.CostWithAnotherItem = 0;
        shipsToModel.Cost = 0;
        shipsToModel.FreeShippingTotalValue = 0;
        shippingMethod.ShipsTo.push(shipsToModel);
        //console.log(shippingMethod.ShipsTo);
    }


    public deleteShippingMethod(detail, index) {
        detail.ShippingMethod.splice(index, 1);
    }

    public deleteShipsTo(shippingMethod, index) {
        shippingMethod.ShipsTo.splice(index, 1);
    }

    onProfileSelected(id) {
        this.ShippingProfiles.forEach(profile => {
            if (profile._id == id) {
                console.log(profile)
                this.shippingprofileModel = profile;
            }
        });
    }

    subProfileValueChanged(value) {
        console.log(value)
    }

    /*getCustomeProfileId() {
        this.shippingProfileService.addShippingProfile(this.shippingprofileModel).then(
            response => {
                console.log(response);
            },
            error => {
            }
        );
    }*/

    /*  Shipping Profile Helper Functions End */

}
