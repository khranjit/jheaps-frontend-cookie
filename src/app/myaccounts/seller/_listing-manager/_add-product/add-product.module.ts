import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
// import { UiSwitchModule } from 'ngx-toggle-switch';
import { SharedModule } from '../../../../shared/shared.module';
import { AddProductComponent } from './add-product.component';
import { Select2Module } from '../../../../../assets/ng2-select2';
import {ListboxModule} from 'primeng/listbox';
import { Routes, RouterModule } from '@angular/router';
import {DropdownModule} from 'primeng/dropdown';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
// import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';

import {MatSlideToggleModule} from '@angular/material/slide-toggle';
// import { NgxEditorModule } from 'ngx-editor';
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';


const addproduct: Routes = [
  {path: '', component:AddProductComponent},
]
@NgModule({
  imports: [
    SharedModule,
    Select2Module,
    ListboxModule,
    // UiSwitchModule,
    DropdownModule,
    FroalaEditorModule.forRoot(), 
    FroalaViewModule.forRoot(),
    RouterModule.forChild(addproduct),
    NgSelectModule,
    FormsModule,
    MatSlideToggleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule
    // NgxEditorModule
  ],
  declarations: [
    AddProductComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class AddProductModule { }