import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { InTransitComponent } from './in-transit.component';
import { Routes, RouterModule } from '@angular/router';


const intransit: Routes = [
  {path: '', component:InTransitComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(intransit)
  ],
  declarations: [
    InTransitComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class InTransitModule { }