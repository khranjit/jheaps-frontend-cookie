import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { OrderComponent } from './orders.component';
import { Routes, RouterModule } from '@angular/router';


const orders: Routes = [
  {path:'',redirectTo:'list',pathMatch:'full'},
  {
    path:'list',component:OrderComponent, 
      children:
        [
          {path:'', redirectTo:'open-order',pathMatch:'full'},
          {path: 'open-order', loadChildren:'./_open-order/open-order.module#OpenOrderModule'},
          {path: 'ready-for-shipment', loadChildren:'./_ready-for-shipment/ready-for-shipment.module#ReadyForShipmentModule'},
          {path: 'all-order', loadChildren:'./_all-order/all-order.module#AllOrderModule'},
          {path: 'in-transit', loadChildren:'./_in-transit/in-transit.module#InTransitModule'},
          {path: 'cancelled-order', loadChildren:'./_cancelled-order/cancelled-order.module#CancelledOrderModule'},
          {path: 'delivered', loadChildren:'./_delivered/delivered.module#DeliveredModule'},
          {path: 'return-request', loadChildren:'./_return-request/return-request.module#ReturnRequestModule'},
        ]
  }
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(orders)
  ],
  declarations: [
      OrderComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})

export class OrdersModule { }