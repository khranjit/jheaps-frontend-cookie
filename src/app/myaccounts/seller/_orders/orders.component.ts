import { Component, OnInit, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { OrderService } from '../../../services/order.service';
import { UtilService } from '../../../services/util.service';
import { NotifyService } from '../../../services/notify.service';
import { UserService } from '../../../services/user.service';
import { Router, NavigationExtras, NavigationEnd, ActivatedRoute } from '@angular/router';

declare var jQuery: any;

@Component({
    
    changeDetection: ChangeDetectionStrategy.Default,
    encapsulation: ViewEncapsulation.Emulated,
    selector: 'order-list',
    providers: [OrderService, NotifyService, UtilService,UserService],
    templateUrl: './orders.component.html'
})

export class OrderComponent {

    errorMessage: any;
    paymentStatusInfo: any;
    paymentStatus: any;
    products: any;
    public productCurrency: any = { value: "USD" };
    orderStatus: any;
    pageNumber: number;
    totalPages: number;
    viewAllOrders: any;
    orderList: any;
    showPurchase: boolean;
    showAll: boolean;
    showDeliver: boolean;
    showCancel: boolean;
    showReturn: boolean;
    purchase: any;
    cancelOrder: any;
    DisplayCancelOrder: boolean;
    orderArray: any;
    viewCancelledOrder: any;
    orderStatusModel: any;
    placedOrderList: any;
    selectedOrder: any;
    viewDeliveredOrder: any;
    viewReturnedOrder: any;
    returnSelectedOrder: any;
    returnResponse: any;
    getReturnStatus: any;
    returnOrderId: any;
    returnProductId: any;
    returnShopId: any;
    returnProductIndex: any;
    returnSellerSubject: any;
    returnSellerDetails:any;
    attachments = [];
    returnMail: any;
    getUserToken : any;
    ShowUserProfile : any;
    ShowUserRole : any;
    sellerRejectReasons: any;
    
    constructor(private userService: UserService,private router: Router,private orderService: OrderService, private toaster: NotifyService, private utilService: UtilService) {
        this.orderList = [];
        this.orderStatus = {};
        this.pageNumber = 0;
        this.totalPages = 0;
        this.viewAllOrders = {};
        this.viewDeliveredOrder = [];
        this.returnSelectedOrder = [];
        this.showPurchase = true;
        this.showAll = true;
        this.showDeliver = true;
        this.showCancel = true;
        this.showReturn = true;
        this.purchase = { "Order": "Ordered" };
        this.cancelOrder = { "Order": "ShopCancelledOrders" }
        this.DisplayCancelOrder = false;
        this.orderArray = [];
        this.placedOrderList = [];
        this.viewReturnedOrder = [];
        this.viewCancelledOrder=[];
        this.selectedOrder = [];
        this.returnResponse="";
        this.returnSellerSubject="";
        this.returnSellerDetails=""; 
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response); 
                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                    console.log('type of user'+this.ShowUserRole)
                    if(this.ShowUserRole== 'seller'){
                        this.Init();
                    }
                    else{
                        this.router.navigate(['']);
                    }
            },
            error => this.errorMessage = <any>error);
    }

    public Init() {
      
        this.orderService.getDeliveredOrder(0,10).then(
            response => {
                this.viewDeliveredOrder = JSON.parse(response).Items;
                console.log("deliver order list: " + this.viewDeliveredOrder);
            }, error => this.errorMessage = <any>error);

        
    }

    getShopkey(item) {
        return Object.keys(item);
    }

    status(value) {
        this.orderStatus = value;
    }
    
    returnStatus(value){
    this.returnResponse=value;
    }
    
    setStatus(id) {
        this.orderStatusModel = { "OrderId": id, "orderStatus": this.orderStatus }
        this.orderService.setOrderStatus(this.orderStatusModel).then(
            response => {
                console.log("deliver: " + response);
                this.Init();
            }, error => this.errorMessage = <any>error);
    }

    checkedOrder(value, event) {
        if (event.target.checked) {
            this.orderArray.push(value);
        }
        else if (!event.target.checked) {
            let indexx = this.orderArray.indexOf(value);
            this.orderArray.splice(indexx, 1);
        }
        console.log(this.orderArray);
    }

    cancel(id,model) {
        this.orderService.cancelOrderBuyer(model).then(
            response => {
                console.log("response", response);
            },

            error => this.errorMessage = <any>error);
    }

    purchasedItem(paymentStatusInfo) {
        this.selectedOrder.push(paymentStatusInfo);
    }
    
    returnOrderSeller(orderId, productId, shopId, i){
        if(this.returnResponse=='Reject Requested'){
            this.getReturnStatus={"OrderId" :orderId , "ShopId" :shopId ,"ProductId" : productId,
            "ProductIndexInShop" : i,"productStatus" : this.returnResponse };   
             jQuery("#RejectMailModal").modal("show");
            }
        else if((this.returnResponse=='Replacement Approved')){
            this.getReturnStatus={"OrderId" :orderId , "ShopId" :shopId ,"ProductId" : productId,
            "ProductIndexInShop" : i,"productStatus" : this.returnResponse,"EmailContent" :{} };
         
        console.log( this.getReturnStatus);
         this.orderService.ReturnOrderReplace( this.getReturnStatus).then(
            response => {
                console.log("RETURN Replacement Mail response", response);
                this.Init();
                this.showReturn=true;
            },
             error => this.errorMessage = <any>error);
            }
        else {
         this.getReturnStatus={"OrderId" :orderId , "ShopId" :shopId ,"ProductId" : productId,
            "ProductIndexInShop" : i,"productStatus" : this.returnResponse,"EmailContent" :{} };
        console.log(this.getReturnStatus);
         this.orderService.ReturnOrderStatus(this.getReturnStatus).then(
            response => {
                console.log("RETURN Mail Refund response", response);
                this.Init();
                this.showReturn=true;
            },
             error => this.errorMessage = <any>error);
            }
    }
    
    sendMailtoAdmin(){
        this.getReturnStatus["EmailContent"]={
        
        "Replies": {
            "Message" : this.sellerRejectReasons,
            "Attachment" : this.attachments
        }};
        console.log("this.getReturnStatus",this.getReturnStatus)
    this.orderService.mailToAdmin(this.getReturnStatus).then(
            response => {
                console.log("RETURN Reject Mail response", response);
                this.Init();
                this.showReturn=true;
            },
             error => this.errorMessage = <any>error);
           
    }
    
     test(value, i) {
        this.attachments.splice(i, 1);
        console.log("Pop", this.attachments);
    }
    
    ChangeListener($event) {
        console.log("change listenser called")
        let files = [].slice.call($event.target.files);
        var file: File = files[0];
        let fileName = file.name.split(".")[0];
        this.utilService.convertToBase64(file);
        let count = 0;
        this.utilService.invokeEvent.subscribe(value => {
            if (count == 0) {
                this.utilService.uploadBase64AndGetUrl(fileName, value).then(
                    response => {
                        console.log("Extract Imagedata from uploadBase64AndGetUrl", JSON.parse(response));
                        let obj = { "Name": fileName, "URL": JSON.parse(response).ImageData };
                        this.attachments.push(obj);
                        console.log(" this.attachments ", this.attachments);

                    },
                    error => {
                        this.toaster.ErrorSnack( "Problem in uploading the image. Please try again");
                    });
            }
            count++;
        });

    }
    
    returnSellerQuestion(orderId, productId, shopId, i){
        this.returnOrderId = orderId;
        this.returnProductId = productId;
        this.returnShopId = shopId;
        this.returnProductIndex = i;
        jQuery("#returnSellerMail").modal("show");
    }
    
    confirmReturnMail(){
     this.returnMail = {"OrderId": this.returnOrderId, "ShopId": this.returnShopId,
            "ProductId": this.returnProductId, "ProductIndexInShop": this.returnProductIndex, 
            "productStatus":  this.returnResponse,
           "EmailContent" : {"Subject": this.returnSellerSubject,
            "Replies": {
                "Message": this.returnSellerDetails,
                "Attachment": this.attachments
            } }
        };  
         console.log(this.returnMail);
       this.orderService.ReturnOrderStatus(this.returnMail).then(
            response => {
                console.log("RETURN Mail response", response);
                this.Init();
                this.showReturn=true;
                jQuery("#returnSellerMail").modal("hide");
            },
             error => this.errorMessage = <any>error);  
    }
    
    pageSelectionNext() {
        this.pageNumber = this.pageNumber + 1;
        console.log("Current Page Number :" + this.pageNumber);
        this.Init();
    }
    pageSelectionPrev() {
        this.pageNumber = this.pageNumber - 1;
        console.log("Current Page Number :" + this.pageNumber);
        this.Init();

    }

}