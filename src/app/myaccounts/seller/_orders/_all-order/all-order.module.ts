import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { AllOrderComponent } from './all-order.component';
import { Routes, RouterModule } from '@angular/router';


const allorders: Routes = [
  {path: '', component:AllOrderComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(allorders)
  ],
  declarations: [
    AllOrderComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class AllOrderModule { }