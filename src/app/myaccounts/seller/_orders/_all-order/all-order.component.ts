import { Component, HostListener, OnInit, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { OrderService } from '../../../../services/order.service';
import { UtilService } from '../../../../services/util.service';
import { NotifyService } from '../../../../services/notify.service';

declare var jQuery: any;

@Component({

    changeDetection: ChangeDetectionStrategy.Default,
    encapsulation: ViewEncapsulation.Emulated,
    selector: 'all-order',
    providers: [OrderService, NotifyService, UtilService],
    templateUrl: './all-order.component.html'
})

export class AllOrderComponent {
    @HostListener("window:scroll", ["$event"])
    onWindowScroll() {
        let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
        let max = document.documentElement.scrollHeight;
        var percentage = Math.round((pos / max) * 100) / 100;
        //console.log("percentage "+percentage)
        if (percentage > 0.75) {
            this.callscroll();
           
        }
    }
    cancelOrderId: any;
    purchaseCancelOrder: any;
    cancelReasonModel: any = {
        subject: "",
        message: ""
    }
    orderStatus: any;
    public firstItem:boolean = false;
    errorMessage: any;
    placedOrderList = [];
    viewAllOrders = <any>{};
    purchase = { "Order": "Ordered", "User": "Seller","Search":""};
    public productCurrency: any = { value: "₹" };
    orderList = [];
    selectedOrder = [];
    showAll = true;
    size = 12;
    from = 0;
    total = 0;
    totalPages = 0;
    currentPage = 1;
    tempinfo: any;
    pages = [];
    initialPage = 0;
    loader = false;
    pageLoader = false;
    sort_type = 1;
    searchTerm:any;
    constructor(private orderService: OrderService, private toaster: NotifyService, private utilService: UtilService) {
        this.orderStatus = {};
        this.searchTerm = "";
        this.purchase.Search = this.searchTerm;
        //this.viewAllOrders = {};
        this.init();
    }

    init() {
        //    console.log('Initial invoked');
        this.showAll = true;
        this.placedOrder();
    }
    cancel(product,order) {
        this.cancelOrderId = order._id;
        this.purchaseCancelOrder = order;
        jQuery("#cancelOrderModal").modal("show");
    }
    confirmCancelOrder() {
        jQuery("#cancelOrderModal").modal("hide");
        jQuery("#confirmCancelOrderModal").modal("show");
    }
    ProceedToCancel() {
        jQuery("#confirmCancelOrderModal").modal("hide");

        // var Attachment = {
        //     Name: this.purchaseCancelOrder.Items[0].Product.Name,
        //     URL: this.purchaseCancelOrder.Items[0].Product.Media.Image
        // };
        var Replies = {
            // Attachment: Attachment,
            Message: this.cancelReasonModel.message
        };

        var EmailContent = {
            Replies: Replies,
            Subject: this.cancelReasonModel.subject
        };

        var cancelModel = {
            // ShopId: this.purchaseCancelOrder.Items[0].Shop.Id,
            EmailContent: EmailContent
        }
        console.log("Finally in cancel " + JSON.stringify(cancelModel));
        this.orderService.cancelOrderSeller(this.cancelOrderId, cancelModel).then(
            response => {
                console.log("response", response);
                jQuery("#cancelOrderModal").modal("hide");
                this.toaster.SuccessSnack("Order has been cancelled");
                this.init();

            },

            error => this.errorMessage = <any>error);
    }


    getShopkey(item) {
        return Object.keys(item);
    }

    placedOrder() {
        this.ResetPagination;
        this.pageLoader = true;
        this.purchase.Search = this.searchTerm;
        this.orderService.viewOrder(this.from, this.size,this.sort_type, this.purchase).then(
            response => {
                this.viewAllOrders = JSON.parse(response).Items;
                this.orderList = this.viewAllOrders;
                this.total = JSON.parse(response).TotalItems;
                this.pageLoader = false;
                this.firstItem=true;
                this.orderList.forEach((tempProduct: any) => {
                    if (tempProduct.OrderStatus == 'Placed') {
                        this.pageLoader = false;
                        this.placedOrderList.push(tempProduct);
                    }
                });
                this.ValidateScrollContainer();
            },
            error => this.errorMessage = <any>error);
    }
    public MediaImage(Product) {
        if (Product.hasOwnProperty("Variation")) {
            if (Product.Variation.Image != "")
                return Product.Variation.Image;
            else
                return Product.Media.find(x => x.IsPrimary).EditedImage;

        }
        else {
            return Product.Media.find(x => x.IsPrimary).EditedImage;
        }
    }
    placedOrderOnScroll() {
        this.from += this.size;
        if (this.from < this.total) {
            this.loader = true;
            this.orderService.viewOrder(this.currentPage, this.size, this.sort_type, this.purchase).then(
                response => {
                    this.viewAllOrders = JSON.parse(response);
                    this.total = this.viewAllOrders.TotalItems;
                    this.loader = false;
                    this.orderList = this.orderList.concat(this.viewAllOrders.Items);
                    this.orderList.forEach((tempProduct: any) => {
                        if (tempProduct.OrderStatus == 'Placed') {
                            this.placedOrderList.push(tempProduct);
                        }
                    });
                },
                error => this.errorMessage = <any>error);
        }
        this.currentPage += 1;
    }
    async callscroll()
    {
      if(this.firstItem)
      {
        await   this.placedOrderOnScroll();
      }
    }

    purchasedItem(paymentStatusInfo) {

        this.selectedOrder.push(paymentStatusInfo);
    }
    LoadOnScroll() {
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

        if (scrollLength - scrollContainer.scrollTop < 1) {
            setTimeout(() => {
                this.placedOrderOnScroll();
            }, 500)
        }
    }

    ResetPagination() {
        this.currentPage = 0;
        this.total = 0;
        this.from = 0;
        this.size = 12;
        jQuery('#scroll-container').css("height", 500);
    }

    ValidateScrollContainer() {
        setTimeout(() => {
            var scrollContainer = document.getElementById('scroll-container');
            var scrollBox = document.getElementById('scroll-box');
            if (scrollContainer != null && scrollBox != null) {
                var scrollContainerPos = scrollContainer.getBoundingClientRect();
                var scrollBoxPos = scrollBox.getBoundingClientRect();
                if (scrollBoxPos.height < scrollContainerPos.height) {
                    jQuery('#scroll-container').css("height", scrollBoxPos.height);
                }
            }
        }, 1000)
    }
    public tableSorting() {
        if(this.sort_type == 1) {
            this.sort_type = -1;
            this.placedOrder();

        }
        else {
            this.sort_type = 1;
            this.placedOrder();
        }
    }
    public searchOrder()
    {
        var test = isNaN(this.searchTerm);
        console.log(test);
        if(isNaN(this.searchTerm) || this.searchTerm == "") {

            this.placedOrder();

        }
        else {

            if(this.searchTerm.length == 13) {

                this.placedOrder();
            }
            
        }
    }

}