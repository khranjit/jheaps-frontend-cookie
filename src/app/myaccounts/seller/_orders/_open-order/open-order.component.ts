import { Component, HostListener, OnInit, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { OrderService } from '../../../../services/order.service';
import { UtilService } from '../../../../services/util.service';
import { NotifyService } from '../../../../services/notify.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { PackageDetailService } from '../../../../services/package-detail.service';
import { json } from 'express';

declare var jQuery: any;

@Component({

    changeDetection: ChangeDetectionStrategy.Default,
    encapsulation: ViewEncapsulation.Emulated,
    selector: 'open-order',
    providers: [OrderService, NotifyService, UtilService,PackageDetailService],
    templateUrl: './open-order.component.html'
})

export class OpenOrderComponent {
    @HostListener("window:scroll", ["$event"])
    onWindowScroll() {
        let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
        let max = document.documentElement.scrollHeight;
        var percentage = Math.round((pos / max) * 100) / 100;
        //console.log("percentage "+percentage)
        if (percentage > 0.75) {
            this.callscroll();
           
        }
    }
    cancelOrderId: any;
    purchaseCancelOrder: any;
    cancelReasonModel: any = {
        subject: "",
        message: ""
    }
    orderStatus: any;
    public orderDetailModel: ViewModels.IOrderDetailViewModel;
    public orderParamsModel: ViewModels.IOrderParamsViewModel;
    public orderPackageDetailsModel: ViewModels.IOrderPackageDetailsViewModel;
    public orderPackageDimensionModel: ViewModels.IOrderPackageDimensionViewModel;
    public packageModel: any;
    public param: any;
    public packageData: any
    package_dimension = <any>{};
    errorMessage: any;
    placedOrderList = [];
    viewAllOrders = <any>{};
    public productCurrency: any = { value: "₹" };
    purchase = { "Order": "Placed", "User": "Seller" ,"Search" : ""};
    orderList = [];
    selectedOrder = [];
    placedOrderIds = [];
    orderArray = [];
    showOrder = true;
    selectedPrintOrders = false;
    selectedPurchase = false;
    showPackageForm = false;
    orderDetailsId: any;
    editbutton = false;
    temparray = [];
    selectOptions: string;
    printOrderList = [];
    size = 10;
    from = 0;
    total = 0;
    totalPages = 0;
    currentPage = 1;
    pages = [];
    initialPage = 0;
    loader1 = false;
    pageLoader = false;
    public printDocument = false;
    public firstItem:boolean = false;
    orderNumber: any;
    packageDetails=[];
    packageDetailValue:any;
    sort_type = 1;
    searchTerm:any;
    // validateToShow = false;
    constructor(private orderService: OrderService, private toaster: NotifyService, private loader: NgxSpinnerService, private utilService: UtilService,private packageDetailService: PackageDetailService) {
        this.orderDetailModel = <ViewModels.IOrderDetailViewModel>{};
        this.orderParamsModel = <ViewModels.IOrderParamsViewModel>{};
        this.orderPackageDetailsModel = <ViewModels.IOrderPackageDetailsViewModel>{};
        this.orderPackageDimensionModel = <ViewModels.IOrderPackageDimensionViewModel>{};
        //this.viewAllOrders = {};
        this.packageModel = <any>{};
        this.orderStatus = {};
        this.param = <any>{};
        this.packageData = <any>[];
        this.searchTerm = "";
        this.purchase.Search = this.searchTerm;
    
        //this.packageData.package_dimension.push(this.package_dimension);
        //this.package_dimension = <ViewModels.IOrderDetailViewModel.Params.PackageDetails[].package_dimension>{};
        this.init();
    }

    init() {
        window.scrollTo(0, 20);
        this.packageDetailService.getAll().then(res=>{
            var packagearray=JSON.parse(res);
            this.packageDetails=packagearray;

        });
        this.placedOrder();
    

    }
    cancel(order, id) {

        this.cancelOrderId = id;
        this.purchaseCancelOrder = order;
        jQuery("#cancelOrderModal").modal("show");
    }
    confirmCancelOrder() {
        jQuery("#cancelOrderModal").modal("hide");
        jQuery("#confirmCancelOrderModal").modal("show");
    }
    ProceedToCancel() {
        jQuery("#confirmCancelOrderModal").modal("hide");
        //console.log("Finally inside Cancel " + JSON.stringify(this.cancelReasonModel));
        //console.log("Cancel order " + JSON.stringify(this.purchaseCancelOrder));
        // var Attachment = {
        //     Name:this.purchaseCancelOrder.Name,
        //     URL:this.MediaImage(this.purchaseCancelOrder)
        // };
        var Replies = {
            // Attachment : Attachment,
            Message: this.cancelReasonModel.message
        };

        var EmailContent = {
            Replies: Replies,
            Subject: this.cancelReasonModel.subject
        };

        var cancelModel = {
            // ShopId:this.purchaseCancelOrder.Shop.Id,
            EmailContent: EmailContent
        }
        this.orderService.cancelOrderSeller(this.cancelOrderId, cancelModel).then(
            response => {
                this.init();
                var res = JSON.parse(response);
                jQuery("#cancelOrderModal").modal("hide");
                if(res.flag == false){
                    this.toaster.ErrorSnack("Sorry Order already Cancelled");
                }else{
                    this.toaster.SuccessSnack("Order has been cancelled");
                }
                this.back();
                this.selectedPurchase = false;
                //this.showOrder=true;
            },

            error => this.errorMessage = <any>error);
    }



    placedOrder() {
        this.ResetPagination();
        this.pageLoader = true;
        this.purchase.Search = this.searchTerm;
        this.orderService.viewOrder(this.from, this.size,this.sort_type,this.purchase).then(
            response => {
                this.placedOrderList = [];
                this.viewAllOrders = JSON.parse(response).Items;
                this.orderList = this.viewAllOrders;
                this.total = JSON.parse(response).TotalItems;
                this.firstItem=true;
                this.pageLoader = false;
                this.orderList.forEach((tempProduct: any) => {
                    if (tempProduct.OrderStatus == 'Placed') {
                        this.placedOrderList.push(tempProduct);
                    }
                });
                this.placedOrderIds = [];
                this.placedOrderList.forEach((tempProduct: any) => {
                    if (tempProduct.OrderDetailId == '')
                        this.placedOrderIds.push(tempProduct._id);
                });
                this.ValidateScrollContainer();
            },
            error => this.errorMessage = <any>error);
    }
    async callscroll()
    {
      if(this.firstItem)
      {
        await   this.placedOrderOnScroll();
      }
    }
    placedOrderOnScroll() {
        this.from += this.size;
        if (this.from < this.total) {
            this.loader1 = true;
            this.orderService.viewOrder(this.currentPage, this.size,this.sort_type,this.purchase).then(
                response => {
                    this.placedOrderList = [];
                    this.viewAllOrders = JSON.parse(response);
                    this.total = this.viewAllOrders.TotalItems;
                    this.loader1 = false;
                    this.orderList = this.orderList.concat(this.viewAllOrders.Items);
                    this.orderList.forEach((tempProduct: any) => {
                        if (tempProduct.OrderStatus == 'Placed') {
                            this.placedOrderList.push(tempProduct);
                        }
                    });
                    this.placedOrderIds = [];
                    this.placedOrderList.forEach((tempProduct: any) => {
                        if (tempProduct.OrderDetailId == '')
                            this.placedOrderIds.push(tempProduct._id);
                    });
                },
                error => this.errorMessage = <any>error);
        }
        this.currentPage += 1;

    }

    LoadOnScroll() {
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

        if (scrollLength - scrollContainer.scrollTop < 1) {
            setTimeout(() => {
                this.placedOrderOnScroll();
            }, 500)
        }
    }

    ResetPagination() {
        this.currentPage = 1;
        this.total = 0;
        this.from = 0;
        this.size = 10;
        jQuery('#scroll-container').css("height", 500);
    }

    ValidateScrollContainer() {
        setTimeout(() => {
            var scrollContainer = document.getElementById('scroll-container');
            var scrollBox = document.getElementById('scroll-box');
            if (scrollContainer != null && scrollBox != null) {
                var scrollContainerPos = scrollContainer.getBoundingClientRect();
                var scrollBoxPos = scrollBox.getBoundingClientRect();
                if (scrollBoxPos.height < scrollContainerPos.height) {
                    jQuery('#scroll-container').css("height", scrollBoxPos.height);
                }
            }
        }, 1000)
    }

    checkedOrder(item, event) {

        //this.placedOrderList.forEach(ele=>ele.checked = false);
        if (event.target.checked) {

            this.orderArray.push(item._id);
            //      console.log(this.orderArray);
            item.checked = true;

            //this.validateToShow = true;
        }
        else if (!event.target.checked) {
            let indexx = this.orderArray.indexOf(item._id);
            this.orderArray.splice(indexx, 1);
            //    console.log(this.orderArray);
            item.checked = false;
            //this.validateToShow = false;
        }
        console.log(this.orderArray);
    }

    // validateToShow(id)
    // {
    //     var found = this.orderArray.indexOf(value);
    //     if(found == -1)
    //     return false;
    //     else
    //     return true;
    // }

    getShopkey(item) {
        return Object.keys(item);
    }

    selected(order) {
        this.selectedOrder = [];
        this.showOrder = false;
        this.selectedPurchase = true;
        this.showPackageForm = false;
        this.selectedPrintOrders = false;
        this.selectedOrder.push(order);
        console.log(this.selectedOrder);
    }
    public MediaImage(Product) {
        if (Product.hasOwnProperty("Variation")) {
            if (Product.Variation.Image != "")
                return Product.Variation.Image;
            else
                return Product.Media.find(x => x.IsPrimary).EditedImage;

        }
        else {
            return Product.Media.find(x => x.IsPrimary).EditedImage;
        }
    }
    order() {
        this.showOrder = true;
        this.selectedPurchase = false;
        this.showPackageForm = false;
        this.selectedPrintOrders = false;
    }

    printOrderDiv() {
        this.showOrder = true;
        this.selectedPurchase = false;
        this.showPackageForm = false;
        this.selectedPrintOrders = false;
    }

    back() {
        this.editbutton = false;
        this.order();
        jQuery('#PackageForm')[0].reset();
    }

    action(orderId) {
        //   console.log("Action");

        //  if (this.orderArray.length>0) {
        //     let model = {
        //         "OrderIds": this.orderArray
        //     }
        let model = {
            "OrderIds": [orderId]
        }
        this.loader.show();
        this.orderService.shippingLabel(model).then(
            response => {
                //   console.log("Shipping Label ", JSON.parse(response))
                var res = JSON.parse(response);
                this.loader.hide();
                this.toaster.SuccessSnack("Shipping Label generated successfully");
                //console.log(response[0].shipments[0].shipmentPackages.forward_label);
                console.log(res[0][0].shipments[0].shipmentPackages.forward_label);
                //console.log(response[0].shipments[0].shipmentPackages.forward_label);
                this.downloadlink(res[0][0].shipments[0].shipmentPackages.forward_label);
                this.init();
                /*this.orderService.setOrderStatus(this.orderStatusModel).then(
                response => {
                    console.log("deliver: " + response);
                    this.Init();
                }, error => this.errorMessage = <any>error);*/
            },
            error => {
                this.errorMessage = <any>error;
                this.loader.hide();
            });
        //}
    }
    downloadlink(link){
        window.open(link, "_blank");
    }
    PrintInvoice(orderId){
        let model = {
            OrderId: orderId
        }
        this.loader.show();
        this.orderService.PrintInvoice(model).then(
            response => {
                //   console.log("Shipping Label ", JSON.parse(response))
                var res = JSON.parse(response);
                this.loader.hide();
                this.toaster.SuccessSnack("Print successfully");
                this.downloadlink(res.ImageData)
                this.init();
               
            },
            error => {
                this.errorMessage = <any>error;
                this.loader.hide();
            });
    }

    PrintOrders() {
        if (this.orderArray.length > 0) {
            //this.orderArray = ["59de2f74c5fd78c030dee0f3"]
            let model = {
                "PrintOrderIds": this.orderArray
            }

            this.orderService.printOrders(model).then(
                response => {
                    //             console.log("printOrders ", JSON.parse(response))
                    this.printOrderList = JSON.parse(response);
                    this.printOrderDiv();
                    // this.download(res[0].shipments[0].courier_logo_url);
                    /*this.orderService.setOrderStatus(this.orderStatusModel).then(
                    response => {
                        console.log("deliver: " + response);
                        this.Init();
                    }, error => this.errorMessage = <any>error);*/
                },
                error => this.errorMessage = <any>error);
        }
    }

    download(res) {
        var link = document.createElement("a");
        link.download = res;
        link.target = '_blank';
        link.href = res;
        document.body.appendChild(link);
        link.click();

    }

    form(id, order) {
        this.showOrder = false;
        this.selectedPurchase = false;
        this.showPackageForm = true;
        //this.packageModel.OrderId = id;
        this.selectedOrder = [];
        this.selectedOrder.push(order);
        //  this.packageData.no_of_items = order.Items[0][order.ShopIds].length;
        //  console.log(JSON.stringify(order));
        this.orderDetailModel.OrderId = order._id;
        this.orderNumber=order.OrderId;
        this.orderPackageDetailsModel.no_of_items = order.Items.length;
        // this.packageData.
    }

    // anotherOrder(event) {
    //    // console.log(event.target.value);
    //     this.selectedOrder = [];
    //     let model = <any>{};
    //     this.packageModel.OrderId = event.target.value;
    //     this.placedOrderList.forEach((tempProduct: any) => {
    //         if (tempProduct._id == event.target.value) {
    //             this.selectedOrder.push(tempProduct);
    //             model = tempProduct;
    //         }
    //     });
    //     this.packageData.no_of_items = model.Items[0][model.ShopIds].length;
    //     console.log("Another New Order: " + this.selectedOrder);
    // }

    ValidateCharacters(event) {
        return ((event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46)
    }

    ValidateDimensionModel() {
        var length, width, height, weight;
        length = parseFloat(this.orderPackageDimensionModel.length.toString());
        width = parseFloat(this.orderPackageDimensionModel.width.toString());
        height = parseFloat(this.orderPackageDimensionModel.height.toString());
        weight = parseFloat(this.orderPackageDimensionModel.weight.toString());
        if (weight == NaN) {
            this.toaster.ErrorSnack("Invalid weight");
            return false;
        }
        else if (weight <= 0 || weight > 70) {
            this.toaster.ErrorSnack("Invalid weight");
            return false;
        }
        else if (length == NaN) {
            this.toaster.ErrorSnack("Invalid length");
            return false;
        }
        else if (length < 1) {
            this.toaster.ErrorSnack("Invalid length");
            return false;
        }
        else if (width == NaN) {
            this.toaster.ErrorSnack("Invalid width");
            return false;
        }
        else if (width < 1) {
            this.toaster.ErrorSnack("Invalid width");
            return false;
        } 
        else if (height == NaN) {
            this.toaster.ErrorSnack("Invalid height");
            return false;
        }
        else if (height < 1) {
            this.toaster.ErrorSnack("Invalid height");
            return false;
        }
        this.orderPackageDimensionModel.length = length;
        this.orderPackageDimensionModel.height = height;
        this.orderPackageDimensionModel.width = width;
        this.orderPackageDimensionModel.weight = weight;
        return true;
    }

    addPackageDetail() {
        if (!this.ValidateDimensionModel())
            return;
        this.temparray = [];
        this.orderPackageDetailsModel.package_dimension = this.orderPackageDimensionModel;
        this.orderPackageDetailsModel.package_content = "Sample Content";
        this.temparray.push(this.orderPackageDetailsModel);
        this.orderParamsModel.package_details = this.temparray;
        this.orderDetailModel.Params = this.orderParamsModel;
        // console.log("Package Details ", this.orderDetailModel);
        this.orderService.addPackageDetails(this.orderDetailModel).then(
            response => {
                //       console.log("Added ", JSON.parse(response))
                jQuery('#PackageForm')[0].reset();
                this.init();
                this.packageModel = <any>{};
                this.toaster.SuccessSnack('Added Successfully');
                //this.orderArray = <any>[];
                this.back();
                //this.order();
            },
            error => this.errorMessage = <any>error);
    }

    edit() {
        if (!this.ValidateDimensionModel())
            return;
        this.temparray = [];
        this.orderPackageDetailsModel.package_dimension = this.orderPackageDimensionModel;
        this.temparray.push(this.orderPackageDetailsModel);
        this.orderParamsModel.package_details = this.temparray;
        this.orderDetailModel.Params = this.orderParamsModel;
        //let model = { "orderDetailsId": this.orderDetailsId, "orderDetails": this.orderDetailModel };
        //    console.log("Edit", model);
        this.orderService.updatePackageDetails(this.orderDetailModel).then(
            response => {
                //          console.log("Edited ", JSON.parse(response))
                jQuery('#PackageForm')[0].reset();
                this.init();
                this.packageModel = <any>{};
                this.toaster.SuccessSnack('Updated Successfully');
                this.order();
                this.back();
            },
            error => this.errorMessage = <any>error);
    }

    editPackageDetails(id, order) {
        this.editbutton = true;
        //      console.log("Edited");

        this.orderService.getPackageDetails(id).then(
            response => {
                //          console.log("getPackageDetails", JSON.parse(response));
                this.packageModel = JSON.parse(response);
                //            console.log(JSON.stringify(this.packageModel));
                this.orderDetailsId = JSON.parse(response)._id;

                this.orderPackageDetailsModel.no_of_items = this.packageModel.Params.package_details[0].no_of_items;
                this.orderPackageDetailsModel.package_content = this.packageModel.Params.package_details[0].package_content;
                this.orderPackageDimensionModel.weight = this.packageModel.Params.package_details[0].package_dimension.weight;
                this.orderPackageDimensionModel.height = this.packageModel.Params.package_details[0].package_dimension.height;
                this.orderPackageDimensionModel.length = this.packageModel.Params.package_details[0].package_dimension.length;
                this.orderPackageDimensionModel.width = this.packageModel.Params.package_details[0].package_dimension.width;
                // console.log("package model",this.packageModel);
            },
            error => this.errorMessage = <any>error);
        this.form(order._id, order);
    }
generate()
{
    if(this.printDocument)
    {
        this.printDocument = false;
        console.log("print sucessfully");
        setTimeout(() => {
            let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        this.loader.hide();
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
           
          <style>
             table {
                border-collapse: collapse;
            }
            
            table, td, th {
                border: 1px solid black;
            }
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
        );
        popupWin.document.close();
        }, 1000);
       
      
    }
}
    print(): void {
    
        if (this.orderArray.length > 0) {
            //this.orderArray = ["59de2f74c5fd78c030dee0f3"]
            let model = {
                "PrintOrderIds": this.orderArray
            }
            this.loader.show();
            this.orderService.printOrders(model).then(
                response => {
                    //             console.log("printOrders ", JSON.parse(response))
                    this.printOrderList = JSON.parse(response);
                    this.showOrder = true;
                    this.printDocument = true;
                    this.selectedPrintOrders = true;
                
                    // this.download(res[0].shipments[0].courier_logo_url);
                    /*this.orderService.setOrderStatus(this.orderStatusModel).then(
                    response => {
                        console.log("deliver: " + response);
                        this.Init();
                    }, error => this.errorMessage = <any>error);*/
                },
                error => this.errorMessage = <any>error);
        }
    
    }
    onPackageSelected(packagevalue)
    {
        console.log(packagevalue);
        this.orderPackageDimensionModel=this.packageDetails.find(x=>x._id==packagevalue);
        console.log(this.orderPackageDimensionModel);
    }
    public tableSorting() {
        if(this.sort_type == 1) {
            this.sort_type = -1;
            this.placedOrder();

        }
        else {
            this.sort_type = 1;
            this.placedOrder();
        }
    }
    public searchOrder()
    {
       
        if(isNaN(this.searchTerm) || this.searchTerm == "") {

            this.placedOrder();

        }
        else {

            if(this.searchTerm.length == 13) {

                this.placedOrder();
            }
            
        }
    }
}

