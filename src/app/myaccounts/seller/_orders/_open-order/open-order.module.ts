import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { OpenOrderComponent } from './open-order.component';
import { Routes, RouterModule } from '@angular/router';
import { MatInputModule } from '@angular/material';



const openorder: Routes = [
  {path: '', component:OpenOrderComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(openorder),
    MatInputModule
  ],
  declarations: [
    OpenOrderComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class OpenOrderModule { }