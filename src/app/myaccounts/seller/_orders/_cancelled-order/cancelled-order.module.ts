import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { CancelledOrderComponent } from './cancelled-order.component';
import { Routes, RouterModule } from '@angular/router';


const cancelledorder: Routes = [
  {path: '', component:CancelledOrderComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(cancelledorder)
  ],
  declarations: [
    CancelledOrderComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class CancelledOrderModule { }