import { Component, HostListener, OnInit, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { OrderService } from '../../../../services/order.service';
import { UtilService } from '../../../../services/util.service';
import { ToasterPopService } from '../../../../services/toasterpop.service';

declare var jQuery: any;

@Component({

    changeDetection: ChangeDetectionStrategy.Default,
    encapsulation: ViewEncapsulation.Emulated,
    selector: 'cancelled-order',
    providers: [OrderService, ToasterPopService, UtilService],
    templateUrl: './cancelled-order.component.html'
})

export class CancelledOrderComponent {
    @HostListener("window:scroll", ["$event"])
    onWindowScroll() {
        let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
        let max = document.documentElement.scrollHeight;
        var percentage = Math.round((pos / max) * 100) / 100;
        //console.log("percentage "+percentage)
        if (percentage > 0.75) {
            this.callscroll();
           
        }
    }
    errorMessage: any;
    placedOrderList = [];
    orderList = [];
    viewAllOrders = <any>{};
    public productCurrency: any = { value: "₹" };
    viewCancelledOrder = <any>[];
    selectedOrder = [];
    showCancel = true;
    size = 12;
    from = 0;
    total = 0;
    totalPages = 0;
    currentPage = 1;
    pages = [];
    initialPage = 0;
    purchase = { "Order": "Cancelled", "User": "Seller","Search":"" };
    loader = false;
    pageLoader = false;
    public firstItem:boolean = false;
    sort_type = 1;
    searchTerm : any;
    constructor(private orderService: OrderService, private toaster: ToasterPopService, private utilService: UtilService) {
        //this.viewAllOrders = {};
        this.searchTerm = "";
        this.purchase.Search = this.searchTerm;
        this.init();
    }

    init() {
        this.cancelledOrder();
    }

    getShopkey(item) {
        return Object.keys(item);
    }
    public MediaImage(Product) {
        if (Product.hasOwnProperty("Variation")) {
            if (Product.Variation.Image != "")
                return Product.Variation.Image;
            else
                return Product.Media.find(x => x.IsPrimary).EditedImage;

        }
        else {
            return Product.Media.find(x => x.IsPrimary).EditedImage;
        }
    }
    cancelledOrder() {
        this.ResetPagination;
        this.pageLoader = true;
        this.purchase.Search = this.searchTerm;
        this.orderService.viewOrder(this.from, this.size,this.sort_type, this.purchase).then(
            response => {
                this.viewCancelledOrder = JSON.parse(response).Items;
                this.orderList = this.viewCancelledOrder;
                this.total = JSON.parse(response).TotalItems;
                this.firstItem=true;
                this.pageLoader = false;
                this.ValidateScrollContainer();
            },
            error => this.errorMessage = <any>error);

    }
    async callscroll()
    {
      if(this.firstItem)
      {
        await    this.cancelledOrderOnScroll();
      }
    }
    cancelledOrderOnScroll() {
        this.from += this.size;
        if (this.from < this.total) {
            this.loader = true;
            this.orderService.viewOrder(this.currentPage, this.size,this.sort_type,this.purchase).then(
                response => {
                    this.viewCancelledOrder = JSON.parse(response);
                    this.total = this.viewCancelledOrder.TotalItems;
                    this.loader = false;
                    this.orderList = this.orderList.concat(this.viewCancelledOrder.Items);
                },
                error => this.errorMessage = <any>error);
        }
        this.currentPage += 1;
    }

    purchasedItem(paymentStatusInfo) {
        this.selectedOrder.push(paymentStatusInfo);
    }
    LoadOnScroll() {
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

        if (scrollLength - scrollContainer.scrollTop < 1) {
            setTimeout(() => {
                this.cancelledOrderOnScroll();
            }, 500)
        }
    }

    ResetPagination() {
        this.currentPage = 0;
        this.total = 0;
        this.from = 0;
        this.size = 12;
        jQuery('#scroll-container').css("height", 500);
    }

    ValidateScrollContainer() {
        setTimeout(() => {
            var scrollContainer = document.getElementById('scroll-container');
            var scrollBox = document.getElementById('scroll-box');
            if (scrollContainer != null && scrollBox != null) {
                var scrollContainerPos = scrollContainer.getBoundingClientRect();
                var scrollBoxPos = scrollBox.getBoundingClientRect();
                if (scrollBoxPos.height < scrollContainerPos.height) {
                    jQuery('#scroll-container').css("height", scrollBoxPos.height);
                }
            }
        }, 1000)
    }
    public tableSorting() {
        if(this.sort_type == 1) {
            this.sort_type = -1;
            this.cancelledOrder();

        }
        else {
            this.sort_type = 1;
            this.cancelledOrder();
        }
    }
    public searchOrder()
    {
        var test = isNaN(this.searchTerm);
        console.log(test);
        if(isNaN(this.searchTerm) || this.searchTerm == "") {

            this.cancelledOrder();

        }
        else {

            if(this.searchTerm.length == 13) {

                this.cancelledOrder();
            }
            
        }
    }

}