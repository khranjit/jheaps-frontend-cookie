import { Component, HostListener, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { OrderService } from '../../../../services/order.service';
import { UtilService } from '../../../../services/util.service';
import { MessageService } from '../../../../services/messages.service';
import { NotifyService } from '../../../../services/notify.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { hasOwnProp } from 'ngx-bootstrap/chronos/utils/type-checks';
import { parse } from 'url';

declare var jQuery: any;

@Component({

    changeDetection: ChangeDetectionStrategy.Default,
    encapsulation: ViewEncapsulation.Emulated,
    selector: 'return-request',
    providers: [OrderService, NotifyService, UtilService, MessageService],
    templateUrl: './return-request.component.html'
})

export class ReturnRequestComponent {
    @HostListener("window:scroll", ["$event"])
    onWindowScroll() {
        let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
        let max = document.documentElement.scrollHeight;
        var percentage = Math.round((pos / max) * 100) / 100;
        //console.log("percentage "+percentage)
        if (percentage > 0.75) {
            this.callscroll();
          
        }
    }
    errorMessage: any;
    public productCurrency: any = { value: "₹" };
    placedOrderList = [];
    orderList = [];
    order = [];
    purchaseReturnOrder: any;
    viewReturnedOrder = <any>[];
    selectedOrder = [];
    showReturn = true;
    returnResponse: any;
    getReturnStatus: any;
    showReturnedOrderDiv=false;
    returnOrderId: any;
    returnProductId: any;
    returnShopId: any;
    returnProductIndex: any;
    returnSellerSubject: any;
    returnSellerDetails: any;
    to: any;
    attachments = [];
    UserTypes = [{
        "value": 1,
        "label": "Admin"
    },
    {
        "value": 0,
        "label": "Buyer"
    }];
    returnMail: any;
    returnReason: any;
    returnDetails: any;
    sellerRejectReasons: any;
    public firstItem:boolean = false;
    size = 12;
    from = 0;
    total = 0;
    totalPages = 0;
    currentPage = 1;
    pages = [];
    initialPage = 0;
    productIndex = 0;
    ShowTicket = false;
    ShowDatas = true;
    Messages: any = [];
    Ticket: any = {};
    replyMail = false;
    SellerTickets = <any>{};
    Admin: any;
    loader = false;
    pageLoader = false;
    orderArray = [];
    refundReplaceOrder=[];
    public printOrderList=[];
    public printDocument=false;
    sort_type = 1;
    searchTerm : any;
    constructor(private orderService: OrderService, private messageService: MessageService, private toaster: NotifyService, private utilService: UtilService, private loadingservice: NgxSpinnerService) {
        //this.viewAllOrders = {};
        this.searchTerm="";
        this.init();
        this.ShowDatas = true;
    }

    init() {
        this.returnOrder();
    }

    getShopkey(item) {
        return Object.keys(item);
    }

    returnStatus(value) {
        this.returnResponse = value;
    }

    returnOrder() {
        this.ResetPagination();
        this.pageLoader = true;
        this.orderService.getReturnOrderListforSeller(this.initialPage, this.size , this.sort_type,this.searchTerm).then(
            response => {
                this.viewReturnedOrder = JSON.parse(response).Items;
                this.orderList = this.viewReturnedOrder;
                this.total = JSON.parse(response).TotalItems;
                this.pageLoader = false;
                this.firstItem=true;
                
            }, error => this.errorMessage = <any>error);
    }
    async callscroll()
    {
      if(this.firstItem)
      {
        await   this.returnOrderOnScroll();
      }
    }
    returnOrderOnScroll() {
        this.from += this.size;
        if (this.from < this.total) {
            this.loader = true;
            this.orderService.getReturnOrderListforSeller(this.currentPage, this.size ,this.sort_type,this.searchTerm).then(
                response => {
                    this.viewReturnedOrder = JSON.parse(response).Items;
                    this.total = JSON.parse(response).TotalItems;
                    this.loader = false;
                    this.orderList = this.orderList.concat(this.viewReturnedOrder);
                }, error => this.errorMessage = <any>error);

        }
        this.currentPage += 1;
    }

    purchasedItem(paymentStatusInfo) {
        this.selectedOrder.push(paymentStatusInfo);
    }
    public MediaImage(Product) {
        if (Product.hasOwnProperty("Variation")) {
            if (Product.Variation.Image != "")
                return Product.Variation.Image;
            else
                return Product.Media.find(x => x.IsPrimary).EditedImage;

        }
        else {
            return Product.Media.find(x => x.IsPrimary).EditedImage;
        }
    }
    LoadOnScroll() {
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

        if (scrollLength - scrollContainer.scrollTop < 1) {
            setTimeout(() => {
                this.returnOrderOnScroll();
            }, 500)
        }
    }

    ResetPagination() {
        this.currentPage = 1;
        this.total = 0;
        this.from = 0;
        this.size = 12;
        jQuery('#scroll-container').css("height", 500);
    }

    

    showReturnModal(i) {
        this.productIndex = i;
        jQuery("#MailModal").modal("show");

    }
    returnOrderSeller(orderId, productId, shopId, order, i) {
        if(this.returnResponse=="Reject Requested" && (typeof this.returnReason==="undefined" || this.returnReason == this.returnReason.trim()))
        {
            this.toaster.ErrorSnack('Reject reason is required!')
        }
        else
        {
            this.purchaseReturnOrder = order;
            this.getReturnStatus = {
                "OrderId": orderId,
                "ItemId": productId,
                "ProductStatus": this.returnResponse,
                "EmailContent": {
                    "Reason": this.returnReason,
                    "Replies": {
                        "Description": this.returnDetails,
                        "Attachment": this.attachments
                    },
                }
            };
            this.orderService.ReturnOrderReplace(this.getReturnStatus).then(
                response => {
                    console.log("RETURN Replacement Mail response", response);
                    jQuery("#MailModal").modal("hide");
                    var msg;
                    var res = JSON.parse(response);
                    if(res.flag == false){
                        this.toaster.ErrorSnack("Item already processed");
                    }
                    else if (this.returnResponse == "Replacement Approved") {
                        msg = "The return request has been Approved for Replacement";
                        this.toaster.SuccessSnack(msg);
                    }
                    else if (this.returnResponse == "Refund Approved") {
                        msg = "The return request has been Approved for Refund";
                        this.toaster.SuccessSnack(msg);
                    }
                    else {
                        msg = "The return request has been rejected";
                        this.toaster.SuccessSnack(msg);
                    }
                    this.showReturn = true;
                    this.ShowDatas = true;
                    this.init();
                },
                error => this.errorMessage = <any>error);
        }
    }
    //End Refund Approved
    sendMailtoAdmin() {
        this.getReturnStatus["EmailContent"] = {
            "Replies": {
                "Message": this.sellerRejectReasons,
                "Attachment": this.attachments
            }
        };
        //     console.log("this.getReturnStatus",this.getReturnStatus)
        this.orderService.mailToAdmin(this.getReturnStatus).then(
            response => {
                //           console.log("RETURN Reject Mail response", response);
                this.init();
                this.showReturn = true;
                this.ShowDatas = true;
            },
            error => this.errorMessage = <any>error);

    }

    test(value, i) {
        this.attachments.splice(i, 1);
        // console.log("Pop", this.attachments);
    }

    public showReturnedOrder(orderid,ordertype,prodid){
        this.refundReplaceOrder=[];
        var model = {
            orderid:orderid,
            orderType:ordertype,
            prodid:prodid
        }
        this.orderService.showReturnedOrder(model).then(
            response => {
                var res = JSON.parse(response);

                this.refundReplaceOrder.push(res);
                this.showReturn=false;
                this.ShowDatas=true;
                this.showReturnedOrderDiv=true;

                console.log(res);
                

            },

            error => this.errorMessage = <any>error);

    }
    

    public action(orderid){
        let model = {
            OrderId: orderid
        }
        this.loadingservice.show();
        this.orderService.PrintReturnInvoice(model).then(
            response => {
                //   console.log("Shipping Label ", JSON.parse(response))
                var res = JSON.parse(response);
                this.loadingservice.hide();
                this.toaster.SuccessSnack("Print successfully");
                this.downloadlink(res.ImageData)
                this.init();
               
            },
            error => {
                this.errorMessage = <any>error;
                this.loadingservice.hide();
            });
    }

    downloadlink(link){
        window.open(link, "_blank");
    }

    ChangeListener($event) {
        let files = [].slice.call($event.target.files);
        var file: File = files[0];
        var type = file.name.split('.').pop();
        // if (type == "jpeg" || type == "jpg" || type =="png" || type == "bmp" || type == "pdf" || type == "docx" || type == "txt" || type == "csv" || type == "xlsx" || type == "xlm" || type == "xlsm" || type == "xsl" || type =="doc" || type =="rtf") {
        if (type == "jpeg" || type == "jpg" || type == "png" || type == "bmp") {
            if (file.size < 5 * 1024 * 1024) {
                if (this.attachments.length < 5) {
                    this.loadingservice.show();
                    let fileName = file.name.split(".")[0];
                    this.utilService.convertToBase64(file);
                    let count = 0;
                    this.utilService.invokeEvent.subscribe(value => {
                        if (count == 0) {
                            this.utilService.uploadBase64AndGetUrl(fileName, value).then(
                                response => {
                                    //              console.log("Extract Imagedata from uploadBase64AndGetUrl", JSON.parse(response));
                                    let obj = { "Name": fileName, "URL": JSON.parse(response).ImageData };
                                    this.attachments.push(obj);
                                    this.loadingservice.hide();
                                    //            console.log(" this.attachments ", this.attachments);

                                },
                                error => {
                                    this.toaster.ErrorSnack("Problem in uploading the image. Please try again");
                                });

                        }
                        count++;
                    });
                }
                else {
                    this.toaster.ErrorSnack("You can Upload Only 5 Files");
                }
            }
            else {
                this.toaster.ErrorSnack("File Size Exceeds the 5MB Limit");
            }
        }
        else {
            this.toaster.ErrorSnack("File Type is not supported");
        }
    }


    returnSellerQuestion(orderId, productId, shopId, i) {
        this.returnOrderId = orderId;
        this.returnProductId = productId;
        this.returnShopId = shopId;
        this.returnProductIndex = i;
        jQuery("#returnSellerMail").modal("show");
    }

    confirmReturnMail() {
        this.returnMail = {
            "OrderId": this.returnOrderId, "ShopId": this.returnShopId,
            "ProductId": this.returnProductId, "ProductIndexInShop": this.returnProductIndex,
            "productStatus": this.returnResponse,
            "EmailContent": {
                "Reason": this.returnReason,
                "Replies": {
                    "Description": this.returnDetails,
                    "Attachment": this.attachments
                },
            }
        };
        // console.log(this.returnMail);
        this.orderService.ReturnOrderStatus(this.returnMail).then(
            response => {
                //          console.log("RETURN Mail response", response);
                var res = JSON.parse(response);
                this.init();
                this.showReturn = true;
                this.ShowDatas = true;
                if(res.flag == false){
                    this.toaster.ErrorSnack("Sorry, Item is already processed");
                }
                jQuery("#returnSellerMail").modal("hide");
            },
            error => this.errorMessage = <any>error);
    }
    //Tickets
    viewTicket(TicketId) {
        this.SellerTickets = {};
        this.attachments = [];
        this.replyMail = false;
        this.ShowTicket = true;
        this.showReturn = false;
        this.ShowDatas = true;
        this.messageService.getSellerTickets(TicketId)
            .then(
                tickets => {
                    var data = JSON.parse(tickets);
                    this.Ticket = data;
                    this.Messages = this.Ticket.Messages;
                },
                error => this.errorMessage = <any>error);
    }
    ReplyMessage() {
        if (this.SellerTickets.UserType) {
            this.Admin = 1;
        } else {
            this.Admin = 2;
        }
        var replyModel = {
            To: this.Ticket.User.Id,
            Reason: this.SellerTickets.Reason,
            Description: this.SellerTickets.Description,
            Attachment: this.attachments,
            id: this.Ticket._id,
            Admin: this.Admin,
            TicketId : this.Ticket.TicketId
        }
        this.messageService.replyMessage(replyModel).then(
            res => {
                var response = JSON.parse(res);
                if(response.hasOwnProperty('flag'))
                {
                    this.toaster.ErrorSnack(response.msg);
                }
                else{
                    this.toaster.SuccessSnack("Your queries successfully Submitted");
                }
                this.viewTicket(this.Ticket._id);
            },
            error => this.errorMessage = <any>error);
    }
    SetUsername(Ticket) {
        if (Ticket.IsAdmin) {
            return "Admin";
        } else {
            return Ticket.To.Profile.FirstName;
        }
    }
    checkedOrder(item, event) {

        //this.placedOrderList.forEach(ele=>ele.checked = false);
        if (event.target.checked) {

            this.orderArray.push(item._id);
            //      console.log(this.orderArray);
            item.checked = true;

            //this.validateToShow = true;
        }
        else if (!event.target.checked) {
            let indexx = this.orderArray.indexOf(item._id);
            this.orderArray.splice(indexx, 1);
            //    console.log(this.orderArray);
            item.checked = false;
            //this.validateToShow = false;
        }
        console.log(this.orderArray);
    }
    generate()
    {
        if(this.printDocument)
        {
            this.printDocument = false;
            console.log("print sucessfully");
            setTimeout(() => {
                let printContents, popupWin;
            printContents = document.getElementById('print-section').innerHTML;
            this.loadingservice.hide();
            popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
            popupWin.document.open();
            popupWin.document.write(`
          <html>
            <head>
              <title>Print tab</title>
               
              <style>
                 table {
                    border-collapse: collapse;
                }
                
                table, td, th {
                    border: 1px solid black;
                }
              </style>
            </head>
        <body onload="window.print();window.close()">${printContents}</body>
          </html>`
            );
            popupWin.document.close();
            }, 1000);
           
          
        }
    }
        print(): void {
        
            if (this.orderArray.length > 0) {
                //this.orderArray = ["59de2f74c5fd78c030dee0f3"]
                let model = {
                    "PrintOrderIds": this.orderArray
                }
                this.loadingservice.show();
                this.orderService.printOrders(model).then(
                    response => {
                        //             console.log("printOrders ", JSON.parse(response))
                        this.printOrderList = JSON.parse(response);
                 
                        this.printDocument = true;
                        
                    
                        // this.download(res[0].shipments[0].courier_logo_url);
                        /*this.orderService.setOrderStatus(this.orderStatusModel).then(
                        response => {
                            console.log("deliver: " + response);
                            this.Init();
                        }, error => this.errorMessage = <any>error);*/
                    },
                    error => this.errorMessage = <any>error);
            }
        
        }
        public tableSorting() {
            if(this.sort_type == 1) {
                this.sort_type = -1;
                this.returnOrder();
    
            }
            else {
                this.sort_type = 1;
                this.returnOrder();
            }
        }
        public searchOrder()
        {
            var test = isNaN(this.searchTerm);
            console.log(test);
            if(isNaN(this.searchTerm) || this.searchTerm == "") {
    
                this.returnOrder();
    
            }
            else {
    
                if(this.searchTerm.length == 13) {
    
                    this.returnOrder();
                }
                
            }
        }

}