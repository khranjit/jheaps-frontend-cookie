import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { ReturnRequestComponent } from './return-request.component';
import { Routes, RouterModule } from '@angular/router';
import { MatInputModule } from '@angular/material';



const returnrequest: Routes = [
  {path: '', component:ReturnRequestComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(returnrequest),
    MatInputModule
  ],
  declarations: [
    ReturnRequestComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ReturnRequestModule { }