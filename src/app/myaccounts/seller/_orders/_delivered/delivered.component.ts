import { Component, HostListener, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { OrderService } from '../../../../services/order.service';
import { UtilService } from '../../../../services/util.service';
import { ToasterPopService } from '../../../../services/toasterpop.service';

declare var jQuery: any;

@Component({

    changeDetection: ChangeDetectionStrategy.Default,
    encapsulation: ViewEncapsulation.Emulated,
    selector: 'delivered',
    providers: [OrderService, ToasterPopService, UtilService],
    templateUrl: './delivered.component.html'
})

export class DeliveredOrderComponent {
    @HostListener("window:scroll", ["$event"])
    onWindowScroll() {
        let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
        let max = document.documentElement.scrollHeight;
        var percentage = Math.round((pos / max) * 100) / 100;
        //console.log("percentage "+percentage)
        if (percentage > 0.75) {
            this.callscroll();
            
        }
    }
    errorMessage: any;
    placedOrderList = [];
    viewAllOrders = <any>[];
    public productCurrency: any = { value: "₹" };
    purchase = { "Order": "Delivered", "User": "Seller" ,"Search" : "" };
    orderList = [];
    selectedOrder = [];
    size = 12;
    from = 0;
    total = 0;
    totalPages = 0;
    currentPage = 1;
    pages = [];
    initialPage = 0;
    showDeliver = true;
    loader = false;
    pageLoader = false;
    sort_type = 1;
    searchTerm : any;
    public firstItem:boolean = false;
    constructor(private orderService: OrderService, private toaster: ToasterPopService, private utilService: UtilService) {
        //this.viewAllOrders = {};
        this.searchTerm = "";
        this.purchase.Search = this.searchTerm;
        this.init();
    }

    init() {
        this.deliverOrder();
    }
    deliverOrder() {
        this.ResetPagination;
        this.pageLoader = true;
        this.purchase.Search = this.searchTerm;
        this.orderService.viewOrder(this.from, this.size,this.sort_type, this.purchase).then(
            response => {
                this.viewAllOrders = JSON.parse(response).Items;
                this.orderList = this.viewAllOrders;
                this.firstItem=true;
                this.total = JSON.parse(response).TotalItems;
                this.pageLoader = false;
                this.ValidateScrollContainer();
            },
            error => this.errorMessage = <any>error);
    }
    public MediaImage(Product) {
        if (Product.hasOwnProperty("Variation")) {
            if (Product.Variation.Image != "")
                return Product.Variation.Image;
            else
                return Product.Media.find(x => x.IsPrimary).EditedImage;

        }
        else {
            return Product.Media.find(x => x.IsPrimary).EditedImage;
        }
    }
    deliverOrderOnScroll() {
        this.from += this.size;
        if (this.from < this.total) {
            this.loader = true;
            this.orderService.viewOrder(this.currentPage, this.size,this.sort_type, this.purchase).then(
                response => {
                    this.viewAllOrders = JSON.parse(response);
                    this.total = this.viewAllOrders.TotalItems;
                    this.loader = false;
                    this.orderList = this.orderList.concat(this.viewAllOrders.Items);
                },
                error => this.errorMessage = <any>error);
        }
        this.currentPage +=  1;
    }
    async callscroll()
    {
      if(this.firstItem)
      {
        await  this.deliverOrderOnScroll();
      }
    }
    purchasedItem(paymentStatusInfo) {
        this.selectedOrder.push(paymentStatusInfo);
    }
    LoadOnScroll() {
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

        if (scrollLength - scrollContainer.scrollTop < 1) {
            setTimeout(() => {
                this.deliverOrderOnScroll();
            }, 500)
        }
    }

    ResetPagination() {
        this.currentPage = 0;
        this.total = 0;
        this.from = 0;
        this.size = 12;
        jQuery('#scroll-container').css("height", 500);
    }

    ValidateScrollContainer() {
        setTimeout(() => {
            var scrollContainer = document.getElementById('scroll-container');
            var scrollBox = document.getElementById('scroll-box');
            if (scrollContainer != null && scrollBox != null) {
                var scrollContainerPos = scrollContainer.getBoundingClientRect();
                var scrollBoxPos = scrollBox.getBoundingClientRect();
                if (scrollBoxPos.height < scrollContainerPos.height) {
                    jQuery('#scroll-container').css("height", scrollBoxPos.height);
                }
            }
        }, 1000)
    }
    public tableSorting() {
        if(this.sort_type == 1) {
            this.sort_type = -1;
            this.deliverOrder();

        }
        else {
            this.sort_type = 1;
            this.deliverOrder();
        }
    }
    public searchOrder()
    {
        var test = isNaN(this.searchTerm);
        console.log(test);
        if(isNaN(this.searchTerm) || this.searchTerm == "") {

            this.deliverOrder();

        }
        else {

            if(this.searchTerm.length == 13) {

                this.deliverOrder();
            }
            
        }
    }
}