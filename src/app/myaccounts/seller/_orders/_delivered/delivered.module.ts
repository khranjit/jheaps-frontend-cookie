import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { DeliveredOrderComponent } from './delivered.component';
import { Routes, RouterModule } from '@angular/router';


const deliveredorder: Routes = [
  {path: '', component:DeliveredOrderComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(deliveredorder)
  ],
  declarations: [
    DeliveredOrderComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class DeliveredModule { }