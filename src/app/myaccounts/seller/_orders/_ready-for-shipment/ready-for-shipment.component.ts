import { Component, HostListener, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { OrderService } from '../../../../services/order.service';
import { UtilService } from '../../../../services/util.service';
import { NotifyService } from '../../../../services/notify.service';
import { NgxSpinnerService } from 'ngx-spinner';
declare var jQuery: any;

@Component({

    changeDetection: ChangeDetectionStrategy.Default,
    encapsulation: ViewEncapsulation.Emulated,
    selector: 'ready-for-shipment',
    providers: [OrderService, NotifyService, UtilService],
    templateUrl: './ready-for-shipment.component.html'
})

export class ReadyForShipmentComponent {
    @HostListener("window:scroll", ["$event"])
    onWindowScroll() {
        let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
        let max = document.documentElement.scrollHeight;
        var percentage = Math.round((pos / max) * 100) / 100;
        //console.log("percentage "+percentage)
        if (percentage > 0.75) {
            this.callscroll();
           
        }
    }
    errorMessage: any;
    placedOrderList = [];
    viewAllOrders = <any>{};
    public productCurrency: any = { value: "USD" };
    purchase = { "Order": "Pickup Scheduled", "User": "Seller","Search" : ""};
    orderList = [];
    selectedOrder = [];
    pageNumber = 0;
    showShipped = true;
    size = 12;
    total = 0;
    from = 0;
    totalPages = 0;
    currentPage = 1;
    pages = [];
    initialPage = 0;
    loader1 = false;
    orderArray = [];
    pageLoader = false;
    printOrderList = [];
    public firstItem:boolean = false;
    public printDocument = false;
    sort_type = 1;
    searchTerm : any;
    constructor(private orderService: OrderService, private loader: NgxSpinnerService, private toaster: NotifyService, private utilService: UtilService) {
        //this.viewAllOrders = {};
        this.searchTerm = "";
        this.purchase.Search = this.searchTerm;
        this.init();
    }

    init() {
        this.readyShip();
    }

    readyShip() {
        this.ResetPagination;
        this.pageLoader = true;
        this.purchase.Search = this.searchTerm;
        this.orderService.viewOrder(this.from, this.size, this.sort_type,this.purchase).then(
            response => {
                this.placedOrderList=[];
                this.viewAllOrders = JSON.parse(response).Items;
                this.orderList = this.viewAllOrders;
                this.total = JSON.parse(response).TotalItems;
                this.pageLoader = false;
                this.firstItem=true;
                this.orderList.forEach((tempProduct: any) => {
                    if (tempProduct.OrderStatus == 'Ready For Shipment' || tempProduct.OrderStatus == 'Pickup Scheduled') {
                        this.placedOrderList.push(tempProduct);
                    }
                });
                this.ValidateScrollContainer();
            },
            error => this.errorMessage = <any>error);
    }
    async callscroll()
    {
      if(this.firstItem)
      {
        await   this.readyShipOnScroll();
      }
    }
    downloadlink(link){
        window.open(link, "_blank");
    }
    PrintInvoice(orderId){
        let model = {
            OrderId: orderId
        }
        this.loader.show();
        this.orderService.PrintInvoice(model).then(
            response => {
                //   console.log("Shipping Label ", JSON.parse(response))
                var res = JSON.parse(response);
                this.loader.hide();
                this.toaster.SuccessSnack("Print successfully");
                this.init();
               
            },
            error => {
                this.errorMessage = <any>error;
                this.loader.hide();
            });
    }
    readyShipOnScroll() {
        this.from += this.size;
        if (this.from < this.total) {
            this.loader1 = true;
            this.orderService.viewOrder(this.currentPage, this.size,this.sort_type, this.purchase).then(
                response => {
                    this.placedOrderList=[];
                    this.viewAllOrders = JSON.parse(response).Items;
                    this.total = this.viewAllOrders.TotalItems;
                    this.loader1 = false;

                    this.orderList = this.orderList.concat(this.viewAllOrders);
                    this.orderList.forEach((tempProduct: any) => {
                        if (tempProduct.OrderStatus == 'Ready For Shipment' || tempProduct.OrderStatus == 'Pickup Scheduled') {
                            this.placedOrderList.push(tempProduct);
                        }
                    });
                    
                },
                error => this.errorMessage = <any>error);
        }
        this.currentPage += 1;
    }

    purchasedItem(paymentStatusInfo) {
        this.selectedOrder.push(paymentStatusInfo);
    }
    LoadOnScroll() {
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

        if (scrollLength - scrollContainer.scrollTop < 1) {
            setTimeout(() => {
                this.readyShipOnScroll();
            }, 500)
        }
    }
    checkedOrder(item, event) {

        //this.placedOrderList.forEach(ele=>ele.checked = false);
        if (event.target.checked) {

            this.orderArray.push(item._id);
            //      console.log(this.orderArray);
            item.checked = true;

            //this.validateToShow = true;
        }
        else if (!event.target.checked) {
            let indexx = this.orderArray.indexOf(item._id);
            this.orderArray.splice(indexx, 1);
            //    console.log(this.orderArray);
            item.checked = false;
            //this.validateToShow = false;
        }
        console.log(this.orderArray);
    }
    generate()
{
    if(this.printDocument)
    {
        this.printDocument = false;
        console.log("print sucessfully");
        setTimeout(() => {
            let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        this.loader.hide();
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
           
          <style>
             table {
                border-collapse: collapse;
            }
            
            table, td, th {
                border: 1px solid black;
            }
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
        );
        popupWin.document.close();
        }, 1000);
       
      
    }
}
    print(): void {
    
        if (this.orderArray.length > 0) {
            //this.orderArray = ["59de2f74c5fd78c030dee0f3"]
            let model = {
                "PrintOrderIds": this.orderArray
            }
            this.loader.show();
            this.orderService.printOrders(model).then(
                response => {
                    //             console.log("printOrders ", JSON.parse(response))
                    this.printOrderList = JSON.parse(response);
             
                    this.printDocument = true;
                    
                
                    // this.download(res[0].shipments[0].courier_logo_url);
                    /*this.orderService.setOrderStatus(this.orderStatusModel).then(
                    response => {
                        console.log("deliver: " + response);
                        this.Init();
                    }, error => this.errorMessage = <any>error);*/
                },
                error => this.errorMessage = <any>error);
        }
    
    }
    ResetPagination() {
        this.currentPage = 0;
        this.total = 0;
        this.from = 0;
        this.size = 12;
        jQuery('#scroll-container').css("height", 500);
    }

    ValidateScrollContainer() {
        setTimeout(() => {
            var scrollContainer = document.getElementById('scroll-container');
            var scrollBox = document.getElementById('scroll-box');
            if (scrollContainer != null && scrollBox != null) {
                var scrollContainerPos = scrollContainer.getBoundingClientRect();
                var scrollBoxPos = scrollBox.getBoundingClientRect();
                if (scrollBoxPos.height < scrollContainerPos.height) {
                    jQuery('#scroll-container').css("height", scrollBoxPos.height);
                }
            }
        }, 1000)
    }
    public MediaImage(Product) {
        if (Product.hasOwnProperty("Variation")) {
            if (Product.Variation.Image != "")
                return Product.Variation.Image;
            else
                return Product.Media.find(x => x.IsPrimary).EditedImage;

        }
        else {
            return Product.Media.find(x => x.IsPrimary).EditedImage;
        }
    }
    download(res) {
        var link = document.createElement("a");
        link.download = res;
        link.target = '_blank';
        link.href = res;
        document.body.appendChild(link);
        link.click();

    }
    downloadlabel(orderId) {
        var model: any = {
            orderId: orderId
        };
        this.orderService.donloadShippingLabel(model).then(
            response => {
                var res = JSON.parse(response);
                this.download(res.TransactionDetails.shipments[0].shipmentPackages.forward_label);
            });
    }
    public tableSorting() {
        if(this.sort_type == 1) {
            this.sort_type = -1;
            this.readyShip();

        }
        else {
            this.sort_type = 1;
            this.readyShip();
        }
    }
    public searchOrder()
    {
       
        if(isNaN(this.searchTerm) || this.searchTerm == "") {
 
            this.readyShip();

        }
        else {

            if(this.searchTerm.length == 13) {

                this.readyShip();
            }
            
        }
    }

}