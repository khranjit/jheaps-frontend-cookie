import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { ReadyForShipmentComponent } from './ready-for-shipment.component';
import { Routes, RouterModule } from '@angular/router';


const readyforshipment: Routes = [
  {path: '', component:ReadyForShipmentComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(readyforshipment)
  ],
  declarations: [
    ReadyForShipmentComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ReadyForShipmentModule { }