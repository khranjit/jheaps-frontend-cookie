import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Invoice } from './invoice.component';
import { SharedModule } from '../../../shared/shared.module';
  
  
const invoice: Routes = [
    {path: '',redirectTo:'view',pathMatch:'full'},
    {path: 'view', component:Invoice},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(invoice)
  ],
  declarations: [
    Invoice
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class InvoiceModule { }