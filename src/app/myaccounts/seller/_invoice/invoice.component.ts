import { Component } from '@angular/core';
import { ForumService } from '../../../services/forum.service';
import { UserService } from '../../../services/user.service';
import { OrderService } from '../../../services/order.service';
import { InvoiceService } from '../../../services/invoice.service';
import { Router } from '@angular/router';

@Component({

    selector: 'Invoice',
    styleUrls:['invoice.component.css'],
    templateUrl: './invoice.component.html',
    providers: [ForumService,InvoiceService, UserService, OrderService]

})

export class Invoice {
    ShowUserRole: any;
    ShowUserProfile: any;
    size = 10;
    from = 0;
    total = 0;
    totalPages = 0;
    currentPage = 1;
    pages = [];
    initialPage = 0;
    getAllInvoice: any;
    viewAllInvoice = <any>{};
    errorMessage: any;
    showInvoice = true;
    showselectedInvoice = false;
    selectedInvoice =<any>{};

    constructor(private invoiceService: InvoiceService, private orderService: OrderService, private forumService: ForumService, private userService: UserService, private router: Router) {
        window.scrollTo(0, 20);
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response);
                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                console.log('type of user' + this.ShowUserRole)
                if (this.ShowUserRole == 'seller') {
                    this.viewInvoice();
                }
                else {
                    this.router.navigate(['']);
                }
            }
        );
    }

    viewInvoice() {
        // this.ResetPagination();
        this.invoiceService.getInvoice().then(response => {
            this.getAllInvoice =  JSON.parse(response);
            this.viewAllInvoice = this.getAllInvoice;
        })
    }

    selectedInvoiceDetails(data) {
        this.selectedInvoice = [];
        this.showInvoice = false;
        this.showselectedInvoice = true;
        this.selectedInvoice.push(data);
    }

    goback(){
        this.showInvoice = true;
        this.showselectedInvoice = false;
    }

    LoadOnScroll() {
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

        if (scrollLength - scrollContainer.scrollTop < 1) {
            setTimeout(() => {
                // this.placedOrderOnScroll();
            }, 500)
        }
    }

    ResetPagination() {
        this.currentPage = 0;
        this.total = 0;
        this.from = 0;
        this.size = 10;
        jQuery('#scroll-container').css("height", 500);
    }

    ValidateScrollContainer() {
        setTimeout(() => {
            var scrollContainer = document.getElementById('scroll-container');
            var scrollBox = document.getElementById('scroll-box');
            if(scrollContainer!=null && scrollBox!=null){
            var scrollContainerPos = scrollContainer.getBoundingClientRect();
            var scrollBoxPos = scrollBox.getBoundingClientRect();
            if (scrollBoxPos.height < scrollContainerPos.height) {
                jQuery('#scroll-container').css("height", scrollBoxPos.height);
            }
        }
        }, 1000)
    }
}