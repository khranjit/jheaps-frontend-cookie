import { Component } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { FinanceService } from '../../../../services/finance.service';
import { NotifyService } from '../../../../services/notify.service';

@Component({
	selector: 'addcoupons',
	providers: [FinanceService, NotifyService],
	templateUrl: './add-coupons.component.html',
	styleUrls: ['./add-coupons.component.css']
})

export class AddCouponsComponent {
	couponModel: any = {
		Products: []
	};
	errorMessage: any;
	newbutton = true;
	editbutton = false;
	searchTerm = "";
	totalPages = 1;
	public pages: any[];
	currentPage = 1;
	from = 0;
	size = 18;
	total = 0;
	productsShow=true;
	couponProducts: any;
	cProducts = [];
	constructor(private route: ActivatedRoute, private router: Router, private financeService: FinanceService, private toasterPopService: NotifyService) {
		this.Init();
	}
	public Init() {
		//this.couponModel = <ViewModels.Shop.ICouponViewModel>{};
		this.route.queryParams.subscribe(params => {
			if (params['Coupon']) {
				this.couponModel = JSON.parse(params['Coupon']);
				console.log(this.couponModel);
				this.couponModel.StartDate = new Date(this.couponModel.StartDate);
				//	this.couponModel.CreatedDate = new Date	(this.couponModel.CreatedDate )	;
				this.couponModel.ExpiryDate = new Date(this.couponModel.ExpiryDate);
				this.editbutton = true;
				this.newbutton = false;
			}
		});
		var searchModel = {
			SearchQuery: this.searchTerm,
			from: 0,
			size: this.size,
		}
		this.getCoupons(searchModel);
	}

	selectionChange(item) {
		console.log("Inside selection change" + item);
		var toAdd = true;
		this.couponModel.Products.forEach((ele, i) => {
			if (ele == item._id) {
				toAdd = false;
				this.couponModel.Products.splice(i, 1);
			}
		});

		if (toAdd) {
			this.couponModel.Products.push(item._id);
		}
	}


	public searchProducts(searchTerm) {
		this.ResetPagination();
		var searchModel = {
			SearchQuery: searchTerm,
			from: 0,
			size: this.size,
		}
		this.getCoupons(searchModel);
	}

	checkDiscount()
	{
		if(this.couponModel.DiscountValue > 100)
		{
			this.couponModel.DiscountValue = 95;
			this.toasterPopService.ErrorSnack("You can give upto 95%");
		}
	}
	selectAllChange(event)
    {
        if(event.target.checked == true)
        {
			this.productsShow = false;
			this.couponModel.Products=[];
        }
        
        else{
            this.productsShow = true;
        }

    }

	LoadOnScroll() {
		var scrollContainer = document.getElementById('scroll-container');
		var scrollBox = document.getElementById('scroll-box');
		var scrollContainerPos = scrollContainer.getBoundingClientRect();
		var scrollBoxPos = scrollBox.getBoundingClientRect();
		var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

		if (scrollLength - scrollContainer.scrollTop < 1) {
			setTimeout(() => {
				var searchModel = {
					from: this.from,
					size: this.size,
					SearchQuery: this.searchTerm
				}
				this.getCouponsOnScroll(searchModel);
			}, 500)
		}
	}

	ResetPagination() {
		this.from = 0;
		this.total = 0;
		jQuery('#scroll-container').css("height", 500);
	}

	ValidateScrollContainer() {
		setTimeout(() => {
			var scrollContainer = document.getElementById('scroll-container');
			var scrollBox = document.getElementById('scroll-box');
			if (scrollContainer != null && scrollBox != null) {
				var scrollContainerPos = scrollContainer.getBoundingClientRect();
				var scrollBoxPos = scrollBox.getBoundingClientRect();
				if (scrollBoxPos.height < scrollContainerPos.height) {
					jQuery('#scroll-container').css("height", scrollBoxPos.height);
				}
			}
		}, 1000)
	}


	getCoupons(searchModel) {

		this.financeService.getCouponProducts(searchModel).then(
			res => {
				this.couponProducts = JSON.parse(res).Items;
				this.cProducts = this.couponProducts;
				this.total = JSON.parse(res).TotalItems;

				var found = false;
				this.cProducts.forEach(ele => {
					found = this.couponModel.Products.some(item => item == ele._id);
					if (found) {
						ele.isSelected = true;
					}
					else {
						ele.isSelected = false;
					}
				});

				this.ValidateScrollContainer();
			},
			error => {
				this.errorMessage = <any>error;
			}
		);
	}

	getCouponsOnScroll(searchModel) {
		this.from += this.size;
		if (this.from < this.total) {

			this.financeService.getCouponProducts(searchModel).then(
				res => {
					this.couponProducts = JSON.parse(res);
					this.total = JSON.parse(res).TotalItems;
					this.cProducts = this.cProducts.concat(this.couponProducts.Items);
					var found = false;
					this.cProducts.forEach(ele => {
						found = this.couponModel.Products.some(item => item == ele._id);
						if (found) {
							ele.isSelected = true;
						}
						else {
							ele.isSelected = false;
						}
					});

				},
				error => {
					this.errorMessage = <any>error;
				}
			);
		}
	}


	AddCoupon() {
		var date = new Date();
		var startTime = new Date(this.couponModel.StartDate.toString());
		var endTime = new Date(this.couponModel.ExpiryDate.toString()); 
		endTime.setHours(23,59,59);
		this.couponModel.ExpiryDate.setHours(23,59,59);
		// this.couponModel.ExpiryDate = this.couponModel.ExpiryDate / 1000;
		// this.couponModel.StartDate = this.couponModel.StartDate / 1000;
		this.couponModel.DiscountType="Percentage";

		if (endTime > startTime) {
			if (startTime.getDate() < date.getDate()) {
				this.toasterPopService.ErrorSnack('Start Date Cannot be less than Present Date and Time');
			}
			else {
				this.financeService.addCoupon(this.couponModel).then(
					Result => {
						var data = JSON.parse(Result);

						if (data.msg) {
							this.toasterPopService.ErrorSnack(data.msg);
						}
						else {
							this.toasterPopService.SuccessSnack('New Coupon created Successfully!!');
							this.router.navigate(['/myaccounts/seller/coupons/view']);
						}
					},
					error => {
						this.errorMessage = <any>error;
						this.toasterPopService.ErrorSnack('Coupon already exists');
						console.log("error ", this.errorMessage)
					});
			}

		}
		else {
			this.toasterPopService.ErrorSnack('End Date Cannot be less than Start Date and Time');
		}
	}
	UpdateCoupon() {
		if (this.couponModel.ExpiryDate >= this.couponModel.StartDate) {
			this.financeService.updateCoupon(this.couponModel).then(
				Status => {
					this.toasterPopService.SuccessSnack('Coupon Updated Successfully!!');
					this.router.navigate(['/myaccounts/seller/coupons/view']);
				},
				error => {
					this.errorMessage = <any>error;
					this.toasterPopService.ErrorSnack('There is an error in your coupon');
					console.log("error ", this.errorMessage)
				});
		}
		else {
			this.toasterPopService.ErrorSnack('Please enter Valid date');
		}
	}


	goBack() {
		this.router.navigate(['/myaccounts/seller/coupons/view']);
	}
}