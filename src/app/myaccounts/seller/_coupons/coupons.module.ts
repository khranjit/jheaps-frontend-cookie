import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CouponsComponent } from './coupons.component';
import { AddCouponsComponent } from './_add-coupons/add-coupons.component';
import { SharedModule } from '../../../shared/shared.module';
import {CalendarModule} from 'primeng/primeng';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';

import { MatInputModule } from '@angular/material';


  
  
const coupons: Routes = [
    {path: '',redirectTo:'view',pathMatch:'full'},
    {path: 'view', component:CouponsComponent},
    {path: 'add', component:AddCouponsComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(coupons),
    CalendarModule,
    MatNativeDateModule,
    MatInputModule,
    MatDatepickerModule
  ],
  declarations: [
    CouponsComponent,
    AddCouponsComponent
  ],
  providers: [  
    MatDatepickerModule,  
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class CouponsModule { }