import { Component } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { FinanceService } from '../../../services/finance.service';
import { ToasterPopService } from '../../../services/toasterpop.service';

import { NotifyService } from '../../../services/notify.service';

import { UserService } from '../../../services/user.service';

declare var jQuery: any;

@Component({
	selector: 'coupons',
	providers: [FinanceService, ToasterPopService, UserService],
	styleUrls: ['coupons.component.css'],
	templateUrl: './coupons.component.html'
})

export class CouponsComponent {
	public couponModel: ViewModels.Shop.ICouponViewModel;
	ShowCouponsList: any;

	errorMessage: any;
	deletedCoupon: any;
	ShowUserProfile: any;
	ShowUserRole: any;
	size = 11;
	totalPages = 0;
	from = 0;
	total = 0;
	currentPage = 1;
	pages = [];
	initialPage = 0;
	pageLoader = true;
	ShowCoupons = [];

	constructor(private userService: UserService, private route: ActivatedRoute, private router: Router, private financeService: FinanceService, private toasterPopService: ToasterPopService, private snack: NotifyService) {
		window.scrollTo(0, 20);
		this.userService.showProfile().then(
			response => {
				this.ShowUserProfile = JSON.parse(response);
				this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
				console.log('type of user' + this.ShowUserRole)
				if (this.ShowUserRole == 'seller') {
					this.pageLoader = true;
					this.Init();
					this.ShowCoupon(this.from, this.size);

				}
				else {
					this.router.navigate(['']);
				}
			},
			error => this.errorMessage = <any>error);

	}
	public Init() {
		this.couponModel = <ViewModels.Shop.ICouponViewModel>{};
	}

	LoadOnScroll() {
		var scrollContainer = document.getElementById('scroll-container');
		var scrollBox = document.getElementById('scroll-box');
		var scrollContainerPos = scrollContainer.getBoundingClientRect();
		var scrollBoxPos = scrollBox.getBoundingClientRect();
		var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

		if (scrollLength - scrollContainer.scrollTop < 1) {
			setTimeout(() => {
				this.ShowCouponOnScroll();
			}, 500)
		}
	}

	ResetPagination() {
		this.from = 0;
		this.total = 0;
		jQuery('#scroll-container').css("height", 500);
	}

	ValidateScrollContainer() {
		setTimeout(() => {
			var scrollContainer = document.getElementById('scroll-container');
			var scrollBox = document.getElementById('scroll-box');
			if (scrollContainer != null && scrollBox != null) {
				var scrollContainerPos = scrollContainer.getBoundingClientRect();
				var scrollBoxPos = scrollBox.getBoundingClientRect();
				if (scrollBoxPos.height < scrollContainerPos.height) {
					jQuery('#scroll-container').css("height", scrollBoxPos.height);
				}
			}
		}, 1000)
	}

	public ShowCoupon(from, size) {

		this.financeService.showCoupon(this.from, this.size).then(
			response => {
				this.ShowCouponsList = JSON.parse(response).Items;
				this.ShowCoupons = this.ShowCouponsList;
				this.total = JSON.parse(response).TotalItems;
				this.pageLoader = false;
				this.ValidateScrollContainer();
			},
			error => this.errorMessage = <any>error);
	}

	public ShowCouponOnScroll() {
		this.from += this.size;
		if (this.from < this.total) {
			this.financeService.showCoupon(this.from, this.size).then(
				response => {
					this.ShowCouponsList = JSON.parse(response);
					this.total = JSON.parse(response).TotalItems;
					this.ShowCoupons = this.ShowCoupons.concat(this.ShowCouponsList.Items);
				},
				error => this.errorMessage = <any>error);
		}
	}

	public editCoupon(couponModel) {
		let navimodel: NavigationExtras = {
			queryParams: { 'Coupon': JSON.stringify(couponModel) }
		}
		this.router.navigate(['/myaccounts/seller/coupons/add'], navimodel);
	}

	public deleteCoupon(couponModel) {
		this.deletedCoupon = couponModel;
		jQuery("#confirmDeleteModal").modal('show');
	}
	delete() {
		this.financeService.deleteCoupon(this.deletedCoupon).then(
			response => {
				if (response == 200) {
					 this.ShowCoupon(this.from, this.size); }
				this.snack.SuccessSnack('coupon deleted Successfully!!');
				this.Init();
			},
			error => {
				this.snack.ErrorSnack('There is an error in deleting your coupon');
			}
		);
	}
	addNew() {
		this.router.navigate(['/myaccounts/seller/coupons/add']);
	}

}
