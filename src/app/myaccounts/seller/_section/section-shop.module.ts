import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ShopSectionComponent } from './section-shop.component';
import { AddSectionComponent } from './_add-section/add-section.component';
import { SharedModule } from '../../../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { MatInputModule } from '@angular/material';

  
const section: Routes = [
    {path: '', component:ShopSectionComponent},
    {path: 'add', component:AddSectionComponent}
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(section),
    MatInputModule
  ],
  declarations: [
   ShopSectionComponent,
   AddSectionComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ShopSectionModule { }