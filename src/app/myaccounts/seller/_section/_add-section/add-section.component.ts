import { Component, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SectionService } from '../../../../services/section.service';
import { NotifyService } from '../../../../services/notify.service';


@Component({
   
    selector: 'addsection',
    providers: [SectionService,NotifyService],
    templateUrl: './add-section.component.html'
})

export class AddSectionComponent {
    public sectionModel: any;
    public sectionEditModel: any;
    public SectionsList: any;
    public Sections: any;
    newbutton = true;
    editbutton = false;
    existsCheck: any;
    errorMessage: any;
    sectionList: any;
    sectionId: any;
    showErrorMsg = false;
    errorMsg: string;

    constructor(private route: ActivatedRoute,private toaster: NotifyService, private router: Router, private sectionService: SectionService) {
        this.Init();
    }
    public Init() {
        window.scrollTo(0,20); 
        this.sectionModel = <any>{};
        this.route
            .queryParams
            .subscribe(params => {
                if (params['id']) {
                  //  this.sectionEditModel = JSON.parse(params['id']);
                     this.sectionService.getSectionById(params['id'])
                    .then(
                    sections => {
                        console.log(sections);
                        this.sectionModel = JSON.parse(sections).Items[0];
                     },
                    error => this.errorMessage = <any>error);
                    //this.sectionModel = this.sectionEditModel;
                    this.editbutton = true;
                    this.newbutton = false;
                }
            });
    }
    newSection() {
        this.sectionService.setSection(this.sectionModel)
            .then(
            Status => {
                this.sectionList = JSON.parse(Status);
                this.sectionId = this.sectionList._id;
                console.log("status", this.sectionId)
                if (this.sectionId != null) {
                    this.showErrorMsg = false;
                    this.router.navigate(['/myaccounts/seller/section']);
                }
                else {
                    this.errorMsg = this.sectionList.message;
                    this.showErrorMsg = true;
                    this.router.navigate(['/myaccounts/seller/section/add']);
                }

            },
            error => this.errorMessage = <any>error);

    }
    editSection() {
        this.sectionService.putSection(this.sectionModel)
        .then(
            Status => {
                var res=JSON.parse(Status);
                if (res.message=="Already Exists") {
                    // this.showErrorMsg = false;
                    this.errorMsg = res.message;
                    this.showErrorMsg = true;
                    this.toaster.ErrorSnack(res.msg);
                    // this.router.navigate(['/myaccounts/seller/section']);
                }
                else {
                    // this.toaster.SuccessSnack("Edited sucessfully");
                     this.showErrorMsg = false;
                     this.router.navigate(['/myaccounts/seller/section']);
                    // this.errorMsg = this.sectionList.message;
                    // this.showErrorMsg = true;
                    // this.router.navigate(['/myaccounts/seller/section/add']);
                }

            },
            error => this.errorMessage = <any>error);
        // this.router.navigate(['/myaccounts/seller/section']);
    }

    goBack() {
        this.router.navigate(['/myaccounts/seller/section']);
    }


}