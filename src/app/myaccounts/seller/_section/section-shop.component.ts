import { Component,HostListener } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { SectionService } from '../../../services/section.service';
import { UserService } from '../../../services/user.service';
declare var jQuery: any;

@Component({
    
    selector: 'sectionshop',
    providers: [SectionService,UserService],
    styleUrls:['section-shop.component.css'],
    templateUrl: './section-shop.component.html'
})

export class ShopSectionComponent {
    @HostListener("window:scroll", ["$event"])
    onWindowScroll() {
    let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    let max = document.documentElement.scrollHeight;
    var percentage = Math.round((pos/max) * 100) / 100;
    console.log("percentage "+percentage)
     if(percentage  > 0.75 )   {
         this.callscroll();
       
     }
    }
    public firstItem:boolean = false;
    public Sections: any;
    public SectionsList: any;
    public errorMessage: any;
    public deletableContent: any;
    public pages: any[];
    sectionName:any;
    totalPages: number;
    pageNumber: number;
    ShowUserRole : any;
    ShowUserProfile : any;
    currentPage = 1;
    from = 0;
    size = 10;
    total = 0;
    productLinkCount: number;
    loader = false;
    pageLoader = true;

    constructor(private userService: UserService,private route: ActivatedRoute, private router: Router, private sectionService: SectionService) {
        this.totalPages = 0;
        this.pageNumber = 0;
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response); 
                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                    console.log('type of user'+this.ShowUserRole)
                    if(this.ShowUserRole== 'seller'){
                     this.init();  
                    }
                    else{
                        this.router.navigate(['']);
                    }
            }
           );

    }

    public init() {
        window.scrollTo(0,20); 
        let initialPage = 0;
        this.from = 0 ;
        
        this.getSectionData(this.from,this.size);
       
    }

    getSectionData(from,size){
    this.pageLoader = true;
    this.sectionService.getAll(this.from)
    .then(
    sections => {
        this.SectionsList = JSON.parse(sections).Items;
        this.Sections = this.SectionsList;
        this.total = JSON.parse(sections).TotalItems;
        this.firstItem=true;
        this.pageLoader = false;
        this.ValidateScrollContainer();
    },
    error => this.errorMessage = <any>error);
}
async callscroll()
{
  if(this.firstItem)
  {
    await  this.getSectionDataOnScroll();
  }
}
getSectionDataOnScroll(){
    this.from += this.size;
    if (this.from < this.total) {
        this.loader = true;
        this.sectionService.getAll(this.from)
        .then(
        sections => {
            this.SectionsList = JSON.parse(sections);
            this.total = this.SectionsList.TotalItems;
            this.loader = false;
            this.Sections = this.Sections.concat(this.SectionsList.Items);            
        },
    error => this.errorMessage = <any>error);        
    }
}

    edit(section) {
        let navimodel: NavigationExtras = {
            queryParams: { 'id': section._id }
        }
        this.router.navigate(['/myaccounts/seller/section/add'], navimodel);
      //  console.log(section)
    }
    
    confirmDelete(section,name) {
        this.deletableContent = section;
        this.sectionName=name;
        this.sectionService.displayProductLinkToSection(section._id)
            .then(
            sections => {
                this.productLinkCount = JSON.parse(sections).TotalItems;
        //        console.log("this.productLinkCount",this.productLinkCount);
                //if(JSON.parse(sections).TotalItems)
                jQuery("#confirmDeleteModal").modal('show');
                console.log("section", this.deletableContent)
                
            },
            error => this.errorMessage = <any>error);
        
    }

    delete() {
        this.sectionService.deleteSection(this.deletableContent._id).then(result=>{
            this.init();    
        },
        error => this.errorMessage = <any>error);
    }


    addNew() {
        this.router.navigate(['/myaccounts/seller/section/add']);
    }

    LoadOnScroll() {
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

        if (scrollLength - scrollContainer.scrollTop < 1) {
            setTimeout(() => {
                this.getSectionDataOnScroll();
            }, 500)
        }
    }

    ResetPagination() {
        this.from = 0;
        this.total = 0;
        jQuery('#scroll-container').css("height", 500);
    }

    ValidateScrollContainer() {
        setTimeout(() => {
            var scrollContainer = document.getElementById('scroll-container');
            var scrollBox = document.getElementById('scroll-box');
        if(scrollContainer!=null && scrollBox!=null){
            var scrollContainerPos = scrollContainer.getBoundingClientRect();
            var scrollBoxPos = scrollBox.getBoundingClientRect();
            if (scrollBoxPos.height < scrollContainerPos.height) {
                jQuery('#scroll-container').css("height", scrollBoxPos.height);
            }
        }
        }, 1000)
    }

}