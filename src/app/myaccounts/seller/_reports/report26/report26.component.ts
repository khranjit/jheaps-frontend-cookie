import { Component, Input } from '@angular/core';
import { ReportsService } from '../../../../services/reports.service';
import { ProductDescriptionService } from '../../../../services/product-description.service';

import { UserService } from '../../../../services/user.service';
import { Router } from '@angular/router';
import { ToasterPopService } from '../../../../services/toasterpop.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FinanceService } from '../../../../services/finance.service';
import { tempservice } from '../../../../services/cart.service';
import { Location } from '@angular/common';


@Component({
    selector: 'report26',

    templateUrl: 'report26.component.html',
    providers: [tempservice, ReportsService, UserService, ToasterPopService, FinanceService, ProductDescriptionService]
})

export class Report26Component {
    reportsModel = <any>{};
    public OrderList: any;
    productsShow: boolean = true;
    Products: any = [];
    reportsReady: boolean = false;
    Years = [];
    ReportsLink: any;
    searchTerm = '';
    errorMessage: any;
    reportName: any = '';
    reportType = <any>{};
    Months = [];
    TotalRFSBreach = <any>{};
    reportData = [];
    getreportData: any;
    productData: any;
    awsname: any;
    ShowUserRole: any;
    ShowUserProfile: any;
    from = 0;
    index = 0;
    size = 18;
    total = 0;
    Orders = [];
    obj_id: any;
    showReport = true;
    showselectedReport = false;
    selectedReport = <any>{};
    Product: any = [];
    public imageUrls: any;
    public Month='';
    public Year='';
    public showDate=true;

    constructor(private loader: NgxSpinnerService, private location: Location, private financeService: FinanceService, private productDescriptionService: ProductDescriptionService, private toasterPopService: ToasterPopService, private reportsService: ReportsService, private tempservice: tempservice, private userService: UserService, private router: Router) {
        // this.resetProperties();
        window.scrollTo(0, 20);
        this.Months = [{ value: 1, name: "JAN" }, { value: 2, name: "FEB" }, { value: 3, name: "MAR" }, { value: 4, name: "APR" }, { value: 5, name: "MAY" }, { value: 6, name: "JUN" },
        { value: 7, name: "JUL" }, { value: 8, name: "AUG" }, { value: 9, name: "SEP" }, { value: 10, name: "OCT" }, { value: 11, name: "NOV" }, { value: 12, name: "DEC" }];
        this.TotalRFSBreach = {
            totalNoOfOrders: 0,
            totalorderswithRFS: 0,
            TotalRFSCalc:0,
            TotalRFSBreachRate:0,                        


        }
       
        this.reportsModel = this.tempservice.getData();
        this.GetReports();





    }

    selected(data) {
        this.router.navigate(['/user/product/data.Product_Id']);
    }
    goBack() {
        this.location.back();
    }

    GetReports() {
        this.reportsModel.dwnloadFlag=0;
        this.loader.show();
        this.reportsModel.ReportHeading="Ready For Shipment Breach Report";
        this.reportsService.getReports(this.reportsModel).then(
            res => {
                this.loader.hide();
                this.reportData = JSON.parse(res);
                if(this.reportsModel.ReportType.SelectedMonth!="" || this.reportsModel.ReportType.SelectedMonth==0 && this.reportsModel.ReportType.SelectedYear!="")
                {
                    this.showDate=false;
                    this.Year=this.reportsModel.ReportType.SelectedYear;
                

                        for (var j = 0; j < this.Months.length; j++) {
    
                            if ( this.reportsModel.ReportType.SelectedMonth == this.Months[j].value)
                            {
                                this.Month = this.Months[j].name;
                            }
                         
                        }
                      
                     
                  
                }
                this.reportData.sort(function (a, b) {
                    return a.Month - b.Month;
                  });
                  console.log(this.reportData);
                  for (var i = 0; i < this.reportData.length; i++) {

                    for (var j = 0; j < this.Months.length; j++) {

                        if (this.reportData[i].Month == this.Months[j].value)
                            this.reportData[i].Month = this.Months[j].name;
                    }
                }

                    var num=this.reportData.length-1;
                    this.TotalRFSBreach.totalNoOfOrders=this.reportData[num].Totalorders;
                    this.TotalRFSBreach.totalorderswithRFS=this.reportData[num].TotalBreachOrders;
                    this.TotalRFSBreach.TotalRFSBreachRate=this.reportData[num].RFSBreachRate;
                    this.reportData=this.reportData.slice(0,num);
                
               
                // this.TotalRFSBreach.TotalRFSBreachRate= this.TotalRFSBreach.totalorderswithRFS/ this.TotalRFSBreach.totalNoOfOrders;
                



            },
            error => {
                this.errorMessage = <any>error;

            })
    }
    DownloadReport(){
        this.loader.show();
        this.reportsModel.dwnloadFlag=1;
        this.reportsService.downloadReport(this.reportsModel).then(res=>{
        var result = JSON.parse(res); 
        if(result.ImageData){
        this.loader.hide();
        window.location.href=result.ImageData;
        
    
        
        }else{
        
        }
        }); 
        
        }
}


