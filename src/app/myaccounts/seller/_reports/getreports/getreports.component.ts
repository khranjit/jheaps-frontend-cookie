import { Component, ViewChild, AfterViewInit} from '@angular/core';
import { ReportsService} from '../../../../services/reports.service';
import { UserService } from '../../../../services/user.service';
import { Router} from '@angular/router';
import { NotifyService } from '../../../../services/notify.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FinanceService } from '../../../../services/finance.service';
import { tempservice } from '../../../../services/cart.service';



@Component({ 
   selector: 'getreports',
   styleUrls:['getreports.component.css'],
   templateUrl:'getreports.component.html', 
          
   providers : [tempservice,ReportsService,UserService,NotifyService,FinanceService]
})

export class GetReportsComponent implements AfterViewInit {
    productsShow: boolean = true;
     Products: any = [];
     reportsReady:boolean = false;
     Years = [];
     ReportsLink:any;
     searchTerm = '';
     errorMessage:any;
     reportName:any ='';
     reportType = <any>{};
     reportsModel:any={};
     Months =[];
     reportData:any;
     awsname:any;   
     ShowUserRole : any;
     ShowUserProfile : any;  
     from =0;
     size = 18;
     total=0;
    gotreport=<any>{};

    constructor(private loader:NgxSpinnerService,private financeService:FinanceService, private toasterPopService:NotifyService,private reportsService : ReportsService,private tempservice:tempservice,private userService: UserService,private router: Router)
    {
        this.resetProperties();
        //this.tempservice.clearData();
        window.scrollTo(0, 20);
        //Prepare months
        this.Months = [{value:0,name:"JAN"},{value:1,name:"FEB"},{value:2,name:"MAR"},{value:3,name:"APR"},{value:4,name:"MAY"},{value:5,name:"JUN"},
                       {value:6,name:"JUL"},{value:7,name:"AUG"},{value:8,name:"SEP"},{value:9,name:"OCT"},{value:10,name:"NOV"},{value:11,name:"DEC"}];
        
        //Prepare years               
        var dt = new Date().getUTCFullYear();
        for(var i = dt; i>=dt-10; i--)
        {
            this.Years.push({value:i,name:i});
        }


        // this.userService.showProfile().then(
        //     response => {
        //         this.ShowUserProfile = JSON.parse(response); 
        //         this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
        //             console.log('type of user'+this.ShowUserRole)
        //             if(this.ShowUserRole== 'seller'){
                       
        //            }
        //            else{
        //                this.router.navigate(['']);
        //            }
        //     }
        //    );
    }  
    
    ngAfterViewInit() {

      }
    resetProperties(){
        this.reportType = <any>{};
        this.reportName = '';
        this.reportType.Condition = "Condition2";
        this.reportType.Name = "";
        this.reportType.SelectedMonth = "";
        this.reportType.SelectedYear = "";
        this.reportType.selectedProducts = [];
        this.productsShow = true;
        this.searchTerm = '';

    }  

    

    //GetReports(){
        //this.router.navigate(['/myaccounts/user/seller-reports/report1']);

   // }

    public searchProducts(searchTerm) {
		var searchModel = {
			SearchQuery: searchTerm,
			from: 0,
			size: this.size,
        }
        this.ResetPagination();
        this.getProducts(searchModel);
    }
    
    LoadOnScroll() {
		var scrollContainer = document.getElementById('scroll-container');
		var scrollBox = document.getElementById('scroll-box');
		var scrollContainerPos = scrollContainer.getBoundingClientRect();
		var scrollBoxPos = scrollBox.getBoundingClientRect();
		var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

		if (scrollLength - scrollContainer.scrollTop < 1) {
			setTimeout(() => {
                this.from += this.size;
				var searchModel = {
					from: this.from,
					size: this.size,
					SearchQuery: this.searchTerm
				}
				this.getProductsOnScroll(searchModel);
			}, 500)
		}
    }
    
    ResetPagination() {
		this.from = 0;
		this.total = 0;
		jQuery('#scroll-container').css("height", 500);
	}

	ValidateScrollContainer() {
		setTimeout(() => {
			var scrollContainer = document.getElementById('scroll-container');
            var scrollBox = document.getElementById('scroll-box');
            if(scrollContainer!=null && scrollBox!=null){
			var scrollContainerPos = scrollContainer.getBoundingClientRect();
			var scrollBoxPos = scrollBox.getBoundingClientRect();
			if (scrollBoxPos.height < scrollContainerPos.height) {
				jQuery('#scroll-container').css("height", scrollBoxPos.height);
            }
        }
		}, 1000)
    }
    
    getProductsOnScroll(searchModel) {
		
		if (searchModel.from < this.total) {
			
			this.financeService.getCouponProducts(searchModel).then(
				res => {
					var data = JSON.parse(res).Items;
					this.total = JSON.parse(res).TotalItems;
					this.Products = this.Products.concat(data.Items);
					

				},
				error => {
					this.errorMessage = <any>error;
				}
			);
		}
    }
    
    getProducts(searchModel){
		
		this.financeService.getCouponProducts(searchModel).then(
			res => {
				this.Products = JSON.parse(res).Items;
				
				this.total = JSON.parse(res).TotalItems;
				
				this.ValidateScrollContainer();
			},
			error => {
				this.errorMessage = <any>error;
			}
		);
    }
    
    selectionChange(Product)
    {
        //console.log("Inside selection change" + item);
		var toAdd = true;
		this.reportType.selectedProducts.forEach((ele, i) => {
			if (ele == Product._id) {
				toAdd = false;
				this.reportType.selectedProducts.splice(i, 1);
			}
		});

		if (toAdd) {
			this.reportType.selectedProducts.push(Product._id);
		}
    }

    selectAllChange(event)
    {
        if(event.target.checked == true)
        {
            this.reportType.selectedProducts = [];
            this.productsShow = false;
        }
        
        else{
            this.productsShow = true;
        }

    }

    ReportsSelectionChanged(event)
    {
        console.log("Inside change")
        if(event.target.value == "Report-2")
        {
            var searchModel = {
                SearchQuery: this.searchTerm,
                from: 0,
                size: this.size,
            }
            this.getProducts(searchModel);
        }
    }

     GetReports()
     {   
       
         if ((this.reportName=="Report-1" ||this.reportName=="Report-2"||this.reportName=="Report-25") && this.reportType.Condition=="Condition2" && (this.reportType.StartDate > this.reportType.ExpiryDate)) {
            this.toasterPopService.ErrorSnack('Start date cannot be greater than Expiry date');
             return;
         }
         

        if((this.reportName=="Report-25"||this.reportName=="Report-26" ||this.reportName=="Report-27"  ||this.reportName=="Report-2" ) && this.reportType.Condition == "Condition3")
       {
            var selectedMonth = parseInt(this.reportType.SelectedMonth)
            var selectedYear =  parseInt(this.reportType.SelectedYear)
            var startDate = new Date(selectedYear, selectedMonth,1);
            var expiryDate = new Date(selectedYear, selectedMonth+1,0)
            this.reportType.Condition = "Condition2";
            this.reportType.StartDate = startDate;
            this.reportType.ExpiryDate = expiryDate;
         }
        // console.log(this.reportName, this.reportType);
         if(this.reportName && this.reportType){       
          this.reportsModel = {
            ReportType : this.reportType,
             ReportName :this.reportName
        };
        // console.log('reportmodel'+this.reportsModel);
        this.tempservice.setData(this.reportsModel);
     
          switch (this.reportName) {
            case 'Report-1': {
                //console.log("entered into case Report-1");
                this.router.navigate(['/myaccounts/seller/reports/report1']);
                break;
            }
            case 'Report-2': {
                //console.log("entered into case Report-2");
               this.router.navigate(['/myaccounts/seller/reports/report2']);
                break;
            }
            case 'Report-3': {
                //console.log("entered into case Report-2");
               this.router.navigate(['/myaccounts/seller/reports/report3']);
                break;
            }
            case 'Report-4': {
                //console.log("entered into case Report-2");
               this.router.navigate(['/myaccounts/seller/reports/report4']);
                break;
            }
            case 'Report-8': {
                //console.log("entered into case Report-2");
               this.router.navigate(['/myaccounts/seller/reports/report8']);
                break;
            }
            case 'Report-9': {
                //console.log("entered into case Report-2");
               this.router.navigate(['/myaccounts/seller/reports/report9']);
                break;
            }
            case 'Report-10': {
                //console.log("entered into case Report-2");
               this.router.navigate(['/myaccounts/seller/reports/report10']);
                break;
            }
            case 'Report-11': {
                //console.log("entered into case Report-2");
               this.router.navigate(['/myaccounts/seller/reports/report11']);
                break;
            }
            case 'Report-12': {
                //console.log("entered into case Report-2");
               this.router.navigate(['/myaccounts/seller/reports/report12']);
                break;
            }
            case 'Report-15': {
                //console.log("entered into case Report-2");
               this.router.navigate(['/myaccounts/seller/reports/report15']);
                break;
            }
            case 'Report-25': {
                //console.log("entered into case Report-2");
               this.router.navigate(['/myaccounts/seller/reports/report25']);
                break;
            }
            case 'Report-26': {
                //console.log("entered into case Report-2");
               this.router.navigate(['/myaccounts/seller/reports/report26']);
                break;
            }
            case 'Report-27': {
                //console.log("entered into case Report-2");
               this.router.navigate(['/myaccounts/seller/reports/report27']);
                break;
            }
            case 'Report-35': {
                //console.log("entered into case Report-2");
               this.router.navigate(['/myaccounts/seller/reports/report35']);
                break;
            }
           
         }


        }
        // this.loader.show();
    //      this.reportsService.getReports(this.reportsModel).then(
    //         res =>{
    //             console.log(res);
    //             this.reportData = JSON.parse(res);
    //             this.ReportsLink=this.reportData.ImageData;
    //              this.download(this.ReportsLink);
    //             this.reportsReady = true;
    //       this.resetProperties();
    //            this.loader.hide();
    //    },
    //     error => {
    //          this.errorMessage = <any>error;
    //              this.loader.hide();
    //          })
    //      }       
     }
     DownloadReport(){
        if ((this.reportName=="Report-1" ||this.reportName=="Report-2") && this.reportType.Condition=="Condition2" && (this.reportType.StartDate > this.reportType.ExpiryDate)) {
            this.toasterPopService.ErrorSnack('Start date cannot be greater than Expiry date');
             return;
         }
         

        if((this.reportName=="Report-25" ||this.reportName=="Report-2" ) && this.reportType.Condition == "Condition3")
       {
            var selectedMonth = parseInt(this.reportType.SelectedMonth)
            var selectedYear =  parseInt(this.reportType.SelectedYear)
            var startDate = new Date(selectedYear, selectedMonth,1);
            var expiryDate = new Date(selectedYear, selectedMonth+1,0)
            this.reportType.Condition = "Condition2";
            this.reportType.StartDate = startDate;
            this.reportType.ExpiryDate = expiryDate;
         }
        // console.log(this.reportName, this.reportType);
         if(this.reportName && this.reportType){       
          this.reportsModel = {
            ReportType : this.reportType,
             ReportName :this.reportName
        };
        // console.log('reportmodel'+this.reportsModel);
        this.tempservice.setData(this.reportsModel);   
    }

    // download(res) {
    //     var link = document.createElement("a");
    //     link.download = res;
    //     link.target = '_blank';
    //     link.href = res;
    //     document.body.appendChild(link);
    //     link.click();

    // }
   
    }
}