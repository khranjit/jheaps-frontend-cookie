import { RouterModule, Routes } from '@angular/router';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { Report1Component } from './report1/report1.component';
import { Report2Component } from './report2/report2.component';
import { Report3Component } from './report3/report3.component';
import { Report4Component } from './report4/report4.component';
import { Report8Component } from './report8/report8.component';
import { Report11Component } from './report11/report11.component';
import { Report12Component } from './report12/report12.component';
import { Report10Component } from './report10/report10.component';
import { Report9Component } from './report9/report9.component';
import { Report15Component } from './report15/report15.component';
import { GetReportsComponent } from './getreports/getreports.component';
import {CalendarModule} from 'primeng/primeng';
import { Report25Component } from './report25/report25.component';
import { Report26Component } from './report26/report26.component';
import { Report27Component } from './report27/report27.component';
import { Report35Component } from './report35/report35.component';


import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import { MatInputModule } from '@angular/material';


const sellerReports:Routes = [
  { path: '', redirectTo: 'getreports', pathMatch: 'full' },
  { path: 'getreports', component: GetReportsComponent },                    
  { path: 'report1', component: Report1Component },
  { path: 'report2', component: Report2Component },
  { path: 'report3', component: Report3Component },
  { path: 'report4', component: Report4Component },
  { path: 'report8', component: Report8Component },
  { path: 'report11', component: Report11Component },
  { path: 'report12', component: Report12Component },
  { path: 'report10', component: Report10Component },
  { path: 'report9', component: Report9Component },
  { path: 'report15', component: Report15Component },
  { path: 'report25', component: Report25Component },
  { path: 'report26', component: Report26Component },
  { path: 'report27', component: Report27Component },
  { path: 'report35', component: Report35Component }

]

@NgModule({
  imports: [
    CalendarModule,
    SharedModule,
    RouterModule.forChild(sellerReports),
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule
   ],
   providers: [  
    MatDatepickerModule,  
  ],
  declarations: [
    Report12Component,Report27Component,Report26Component,Report25Component,Report1Component,GetReportsComponent,Report2Component,Report3Component,Report10Component,Report9Component,Report11Component,Report4Component,Report15Component, Report8Component,Report35Component
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ],
})
export class ReportsModule { }
