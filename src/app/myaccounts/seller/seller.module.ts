import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

const seller: Routes = 
[
  {path: 'coupons', loadChildren:'./_coupons/coupons.module#CouponsModule'},
  {path: 'invoice', loadChildren:'./_invoice/invoice.module#InvoiceModule'},
  {path: 'listing', loadChildren:'./_listing-manager/listing-manager.module#ListingManagerModule'},
  {path: 'section', loadChildren:'./_section/section-shop.module#ShopSectionModule'},
  {path: 'vacation', loadChildren:'./_vacation-details/vacation-details.module#VacationDetailsModule'},
  {path: 'zmm', loadChildren:'./_zmmtoken/zmmtoken.module#ZmmTokenModule'},
  {path: 'order-tickets', loadChildren:'./_ticket/ticket.module#TicketModule'}, 
  {path: 'orders', loadChildren:'./_orders/orders.module#OrdersModule'}  ,
  {path: 'shop-settings', loadChildren:'./_shop-settings/shop-settings.module#ShopSettingsModule'},
  {path: 'reports', loadChildren:'./_reports/reports.module#ReportsModule'}
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(seller),

  ],
  declarations: [
  ]
})
export class SellerModule { }
