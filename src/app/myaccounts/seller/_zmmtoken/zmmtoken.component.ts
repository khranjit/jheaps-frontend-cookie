import { Component} from '@angular/core';
import { ZmmtokenServices} from '../../../services/zmmtoken.service';
import { NotifyService } from '../../../services/notify.service';
import { UserService } from '../../../services/user.service';
import { Router} from '@angular/router';

@Component({ 
   selector: 'zmmtoken',
  templateUrl: './zmmtoken.component.html',
  providers : [ZmmtokenServices,NotifyService,UserService]
})

export class ZmmComponent{
  errorMessage:any;
  ShowUserRole : any;
     ShowUserProfile : any;  
  constructor(private toaster : NotifyService,private zmmtokenServices : ZmmtokenServices,private userService: UserService,private router: Router)
 {
  window.scrollTo(0, 20);
  this.userService.showProfile().then(
    response => {
        this.ShowUserProfile = JSON.parse(response); 
        this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
            console.log('type of user'+this.ShowUserRole)
            if(this.ShowUserRole== 'seller'){
               
            }
            else{
                this.router.navigate(['']);
            }
    }
   );
 }
 
 GetZmmtoken(){
    this.zmmtokenServices.getZmmtoken().then(
    response =>{
      console.log(response);
      this.toaster.SuccessSnack("Please check your email to obtain token. Populate the  token in ZEPO market management portal.")    
      },
  error => this.errorMessage = <any>error)  
}
  
}

  
