import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ZmmComponent } from './zmmtoken.component';
import { SharedModule } from '../../../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
  
const zmm: Routes = [
    {path: '', component:ZmmComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(zmm)
  ],
  declarations: [
    ZmmComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ZmmTokenModule { }