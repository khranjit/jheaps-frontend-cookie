import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { TicketComponent } from './ticket.component';
import { SharedModule } from '../../../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { MatInputModule } from '@angular/material';
  

const tickets: Routes = [
    {path: '', component:TicketComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(tickets),
    MatInputModule
  ],
  declarations: [
    TicketComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class TicketModule { }