import { Component, HostListener } from '@angular/core';
import { NotifyService } from '../../../services/notify.service';
import { MessageService } from '../../../services/messages.service';
import { UtilService } from '../../../services/util.service';
import { UserService } from '../../../services/user.service';
import {  Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
declare var jQuery: any;

@Component({
    
    selector: 'sellertickets',
    providers: [NotifyService, UtilService, MessageService, UserService],
    templateUrl: './ticket.component.html'
})

export class TicketComponent {
 @HostListener("window:scroll", ["$event"])
  onWindowScroll() {
    let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    let max = document.documentElement.scrollHeight;
    var percentage = Math.round((pos / max) * 100) / 100;
    console.log("percentage " + percentage)
    if (percentage > 0.75) {
      this.callscroll();
    }
  }
    public MessageModel: ViewModels.ISellerCareViewModel;
    //public RepliesModel: ViewModels.IRepliesViewModel;

    Message: any;
    public TicketList: any;
    ShowUserProfile: any;
    ShowUserRole: any;
    errorMessage: any;
    Roles: any;
    public selectedInboxMessage: any;
    showAllMessages = true;
    showMail = false;
    replyMail = false
    Tickets = [];
    pageNumber: number;
    attachments = [];
    Replies: any;
    ticket_id: any;
    obj_id: any;
    sendReply: boolean;
    replyModel: any;
    hasAttach: boolean;
    replylen: number;
    totalPages = 1;
    pages=[];
    currentPage = 1;
    size = 10;
    from = 0;
    total = 0;
    initialPage=0;
    TicketsSpecific=[];
    searchTerm="";
    returnTicketid:any;
    readableId: any;
    public firstItem:boolean = false;
    show = false;
    descriptionButtonText='View Details';
    caret='View Details <i class="fa fa-caret-down" aria-hidden="true"></i>';
    userReplies:any;
    constructor(private router: Router,private loadingservice:NgxSpinnerService, private toaster: NotifyService, private messageService: MessageService, private utilService: UtilService, private userService: UserService) {

        //this.RepliesModel=<ViewModels.IRepliesViewModel>{};
        this.Message = { "Subject": "", "Replies": [{ "Message": "", "Attachments": [] }] };
        this.sendReply = false;
        this.totalPages = 0;
        this.pageNumber = 0;
        this.Replies = [];
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response); 
                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                    console.log('type of user'+this.ShowUserRole)
                    if(this.ShowUserRole!= 'Others'){
                     this.init();  
                    }
                    else{
                        this.router.navigate(['']);
                    }
            }
           );
        // this.ShowUserRole = [];  
    }
    init() {
        this.pages=[];
         window.scrollTo(0, 20);
        this.MessageModel = <ViewModels.ISellerCareViewModel>{};
        this.MessageModel.Replies = <ViewModels.IRepliesViewModel[]>{};
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response);
                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;

            },
            error => this.errorMessage = <any>error);
        this.TicketList = [];
        
        this.messageService.getUserTickets(this.searchTerm,this.from,this.size)
            .then(
            tickets => {
                this.TicketsSpecific=[];
                this.TicketList = JSON.parse(tickets).Items;
                this.firstItem = true;
                this.obj_id = this.TicketList._id;
                this.Tickets = this.TicketList;
                this.total = JSON.parse(tickets).TotalItems;
                this.Tickets.forEach(ele =>{
                        this.TicketsSpecific.push(ele.Tickets);
                })
                console.log(this.TicketsSpecific);
               
                // this.ValidateScrollContainer();
            },
            error => this.errorMessage = <any>error);
    }

    public getTicketOnScroll(){
       
        this.from += this.size;
        if (this.from < this.total) {
        this.messageService.getUserTickets(this.searchTerm,this.currentPage,this.size)
        .then(
        tickets => {
            this.TicketsSpecific=[];

            this.TicketList = JSON.parse(tickets).Items;;
                this.obj_id = this.TicketList._id;
                this.total = this.TicketList.TotalItems;                
                this.Tickets = this.Tickets.concat(this.TicketList);
                this.Tickets.forEach(ele =>{
                        this.TicketsSpecific.push(ele.Tickets);
                })
        },
        error => this.errorMessage = <any>error);        
        }
        this.currentPage+=1;
    }
async callscroll()
  {
    if(this.firstItem)
    {
      await this.getTicketOnScroll();
    }
  }
    test(value, i) {
        this.attachments.splice(i, 1);
        //console.log("Pop", this.attachments);
    }
    ChangeListener($event) {
        let files = [].slice.call($event.target.files);
        var file: File = files[0];
        var type=file.name.split('.').pop();
        // if (type == "jpeg" || type == "jpg" || type =="png" || type == "bmp" || type == "pdf" || type == "docx" || type == "txt" || type == "csv" || type == "xlsx" || type == "xlm" || type == "xlsm" || type == "xsl" || type =="doc" || type =="rtf") {
        if (type == "jpeg" || type == "jpg" || type =="png" || type == "bmp" ) {            
            if (file.size < 5*1024*1024) {
                if(this.attachments.length < 5)
                {
                    this.loadingservice.show();
                    let fileName = file.name.split(".")[0];
                    this.utilService.convertToBase64(file);
                    let count = 0;
                    this.utilService.invokeEvent.subscribe(value => {
                        if (count == 0) {
                            this.utilService.uploadBase64AndGetUrl(fileName, value).then(
                                response => {
                    //              console.log("Extract Imagedata from uploadBase64AndGetUrl", JSON.parse(response));
                                    let obj = { "Name": fileName, "URL": JSON.parse(response).ImageData };
                                    this.attachments.push(obj);
                                    this.loadingservice.hide();
                        //            console.log(" this.attachments ", this.attachments);
    
                                },
                                error => {
                                    this.toaster.ErrorSnack("Problem in uploading the image. Please try again");
                                });
                        }
                        count++;
                    });
                }
                else
                {
                    this.toaster.ErrorSnack("You can Upload Only 5 Files");
                }
            }
            else
            {
                this.toaster.ErrorSnack("File Size Exceeds the 5MB Limit");
            }
        }
        else
        {
            this.toaster.ErrorSnack("File Type is not supported");
        }
    }
    
   showMailView(){
    this.MessageModel.Subject="";
    this.MessageModel.Replies=<any>{};
    this.showMail=false;
    this.showAllMessages=false;
    this.replyMail=true;
   }
    SearchCheck(searchword,pageNum, size)
    {
            this.TicketList = [];
            this.Tickets = [];
            this.TicketsSpecific=[];
            this.pages=[];
            this.currentPage=1;
            this.messageService.getUserTickets(searchword,pageNum-1,size).then(res=>{
                this.TicketList = JSON.parse(res);
                this.obj_id = this.TicketList._id;
                this.Tickets = this.TicketList.Items;
                this.totalPages=this.TicketList.TotalPages;
                this.Tickets.forEach(ele =>{
                   // ele.Tickets.forEach(ticket=>{
                        this.TicketsSpecific.push(ele.Tickets);
                    //})
                })
                for(let i = 1; i <= this.totalPages; i++)
                {
                this.pages.push(i);
                console.log(i);
                }
                this.initialPage=pageNum-1;
            }, err=>{
                this.errorMessage = <any>err;
            });

    }
    mailMessage() {

        this.MessageModel.Domain = "com";
        this.MessageModel.Replies["Attachment"] = this.attachments;
        //this.MessageModel.Replies.Attachment.push(this.attachments);
        console.log("this.MessageModel", this.MessageModel);
        this.messageService.addTicket(this.MessageModel)
            .then(
            res => {
              //  console.log("Mail Send", res);
              var data=JSON.parse(res);
              if(data.Type)
              {
                this.toaster.ErrorSnack(data.msg);                
              }
              else
              {
                this.toaster.SuccessSnack("Mail Sent Successfully");
                this.attachments = [];
                this.showAllMessages = true;
                this.showMail = false;
                this.replyMail = false;
                this.MessageModel=<any>{}
                this.MessageModel.Replies=<any>{};
                this.init();
              }
            },
            error => this.errorMessage = <any>error);

    }

    ReplyMessage(Msg) {
        this.replyModel = { "UserId": this.selectedInboxMessage.UserId, "TicketId": this.ticket_id };
        this.replyModel.Replies = { "Message": Msg, "Attachment": this.attachments };
  //      console.log("this.replyModel", this.replyModel);
    //    console.log(this.obj_id, this.ticket_id);
        this.messageService.replyTicket(this.replyModel).then(
            res => {
      //          console.log("Reply Send", res);
                this.toaster.SuccessSnack( "Mail Sent Successfully");
                this.sendReply = false;
                this.showAllMessages = true;
                this.showMail = false;
                this.replyMail = false;
                //this.MessageModel=<any>{}
                this.MessageModel.Replies=<any>{};
                this.init();
               
            },
            error => this.errorMessage = <any>error);
        this.sendReply = true;
        this.showAllMessages=true;
    }
    public showMessage(message) {

        this.sendReply = true;
        this.MessageModel.Subject = message.Subject;
        this.messageService.getUserReplies(message)
        .then(
        replies => {
          this.userReplies=JSON.parse(replies);
          console.log(this.userReplies);

        },
        error => this.errorMessage = <any>error);     
        //console.log("this.MessageModel", this.MessageModel.Subject, message);
        this.ticket_id = message._id;
        this.readableId=message.TicketId;
        //console.log("this.ticket_id", this.ticket_id);
        this.showAllMessages = false;

        this.selectedInboxMessage = message;
        console.log(this.selectedInboxMessage);
    }
    public closeTicket(ticket)
    {
        this.messageService.closeTicket(ticket).then(
            res => {
                this.toaster.ErrorSnack("Ticket Successfully Closed");
                this.showMail=false;this.showAllMessages=true;this.replyMail=false;                
                this.init();
            });
    }
    public reopenTicket(ticket)
    {
        this.messageService.reopenTicket(ticket).then(
            res => {
                this.toaster.ErrorSnack("Ticket Successfully Reopened");
                this.showMail=false;this.showAllMessages=true;this.replyMail=false;
                this.init(); 
            });
    }
    // LoadOnScroll() {
    //     var scrollContainer = document.getElementById('scroll-container');
    //     var scrollBox = document.getElementById('scroll-box');
    //     if(scrollContainer!=null && scrollBox!=null){
    //     var scrollContainerPos = scrollContainer.getBoundingClientRect();
    //     var scrollBoxPos = scrollBox.getBoundingClientRect();
    //     var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

    //     if (scrollLength - scrollContainer.scrollTop < 1) {
    //         setTimeout(() => {
               
    //         }, 500)
    //     }
    // }
    // }
    confirmCloseModal()
    {
        jQuery("#confirmCloseModal").modal('show');
    }

    ResetPagination() {
        this.from = 0;
        this.total = 0;
        jQuery('#scroll-container').css("height", 500);
    }

    // ValidateScrollContainer() {
    //     setTimeout(() => {
    //         var scrollContainer = document.getElementById('scroll-container');
    //         var scrollBox = document.getElementById('scroll-box');
    //         var scrollContainerPos = scrollContainer.getBoundingClientRect();
    //         var scrollBoxPos = scrollBox.getBoundingClientRect();
    //         if (scrollBoxPos.height < scrollContainerPos.height) {
    //             jQuery('#scroll-container').css("height", scrollBoxPos.height);
    //         }
    //     }, 1000)
    // }
    updatePages(searchTerm){
        this.from=0;
        this.searchTerm=searchTerm;
        if(isNaN(searchTerm) || searchTerm=="")
        {
        this.init();
        }
        else{
        if(searchTerm.length==10)
        {
        this.init();
        }
        }
        
    }
    showMore(event)
    {

        var target = event.currentTarget;

        if(this.show==true)
        {
            this.show = false;     
            target.previousElementSibling.classList.add('container_show');
            target.previousElementSibling.classList.remove('show');
            // this.descriptionButtonText='show more';
            target.innerHTML="View Details";
            this.caret='View Details <i class="fa fa-caret-down" aria-hidden="true"></i>';
        }
        else
        {
            this.show = true;
            target.previousElementSibling.classList.remove('container_show');
            target.previousElementSibling.classList.add('show');
            // this.descriptionButtonText='show less';
            target.innerHTML="Hide Details";
            this.caret='Hide Details <i class="fa fa-caret-up" aria-hidden="true"></i>';
        }
    }
}
