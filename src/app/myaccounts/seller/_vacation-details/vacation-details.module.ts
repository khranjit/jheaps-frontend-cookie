import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { VacationDetailsComponent } from './vacation-details.component';
import { SharedModule } from '../../../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import {CalendarModule} from 'primeng/primeng';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import { MatInputModule } from '@angular/material';

const vacation: Routes = [
    {path: '', component:VacationDetailsComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(vacation),
    CalendarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule
  ],
  providers: [  
    MatDatepickerModule,  
  ],
  declarations: [
    VacationDetailsComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class VacationDetailsModule { }