import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ShopService } from '../../../services/shop.service';
import { NotifyService } from '../../../services/notify.service';
import { NgxSpinnerService } from 'ngx-spinner';
import {DateAdapter} from '@angular/material/core';
import * as Q from 'q';
declare var jQuery: any;
@Component({
    selector: 'vacationdetails',
    providers: [ShopService, NotifyService,],
    templateUrl: './vacation-details.component.html'

})

export class VacationDetailsComponent {
    public vacationModel = <any>{};
    Details = [];
    vacationDetails = <any>{};
    editmodel=<any>{};
    errorMessage: any;
    showlist: boolean;
    anyoneActive:boolean;
    deleteId:String;
    public vacationList: any;
    public vacation: any;
    editbutton=false;
    constructor(
      
        private toasterService: NotifyService, private route: ActivatedRoute,
        private router: Router, private ShopService: ShopService,
        private loadingservice: NgxSpinnerService, private toaster: NotifyService,private adapter: DateAdapter<any>
    )
     {
        
        this.adapter.setLocale('fr');
        window.scrollTo(0, 20);
        this.showlist = true;
        this.anyoneActive = false;
        this.getvacationDetails();
    }


    getvacationDetails() {

        this.ShopService.getvacationdetails().then(
            res => {
                this.vacationList = JSON.parse(res);
                this.vacation = this.vacationList;
                if(this.vacation.length!=0)
                {
                    this.showlist = true;
            
                }
                this.anyoneActive = this.vacation.some( detail => detail.Active == true );
            },
            err => {
                this.errorMessage = <any>err;
            });
    }

    // resetVacationDetail(){
       
    //     this.ShopService.resetVacationdetails().then(res => {
    //         this.toaster.SuccessSnack('Ended successfully')
    //         this.showlist = false;
    //         this.anyoneActive=false;
    //        this.getvacationDetails();
    //     });

    // }

    confirmDelete(id) 
    {
        this.deleteId = id;
        jQuery("#confirmDeleteModal").modal('show');
    }
    // addvacation()
    // {
    //     this.showlist=false;
    // }
    delete() {
        this.ShopService.deletevacation(this.deleteId).then(res=>{

            var result=JSON.parse(res);
            if(result.success==true)
            {
                this.toaster.SuccessSnack(result.msg);
                this.showlist=true;
                this.getvacationDetails();  
            }
            else{
                this.toaster.ErrorSnack(result.msg);
                this.getvacationDetails();  
            }    
        },
        error => this.errorMessage = <any>error);
    }
    goBack()
{
   
    this.showlist = true;
}
    
edit(vacationObj)
{ 

  this.editmodel={};
  this.showlist=false;
  this.editbutton=true;

  this.vacationDetails.vacationFrom=new Date(vacationObj.vacationFrom);
  this.vacationDetails.vacationTo=new Date(vacationObj.vacationTo);

  this.editmodel=vacationObj;
 

  console.log(vacationObj);
}
updateVacationDetail()
{
    var today = new Date();
    this.vacationDetails.vacationTo.setHours(23,59,59);
    this.editmodel.vacationFrom= this.vacationDetails.vacationFrom;
    this.editmodel.vacationTo= this.vacationDetails.vacationTo;
    if(today.toLocaleDateString()==this.vacationDetails.vacationFrom.toLocaleDateString() ||(this.vacationDetails.vacationTo >= this.vacationDetails.vacationFrom))
    {
        this.ShopService.updateVacationdetails(this.editmodel).then(res => {
            var result=JSON.parse(res);
            if(result.msg=="Created sucessfully")
            {
                this.toaster.SuccessSnack("Updated sucessfully");
                this.showlist=true;
                this.getvacationDetails();  
            }
            else{
                this.toaster.ErrorSnack(result.msg);
                this.getvacationDetails();  
            }
          
            
        });
       
          
            
      
    }
    else if((this.vacationDetails.vacationFrom < today))
    {
     this.toaster.ErrorSnack('Cannot add vacation for expired date');
    }
    // else if ((vacationModel.vacationTo >= vacationModel.vacationFrom)) {
    //     this.ShopService.updateVacationdetails(this.editmodel).then(res => {
    //         var result=JSON.parse(res);
    //         if(result.msg=="Updated sucessfully")
    //         {
    //             this.toaster.SuccessSnack(result.msg);
    //             this.showlist=true;
    //             this.getvacationDetails();  
    //         }
    //         else{
    //             this.toaster.ErrorSnack(result.msg);
    //             this.getvacationDetails();  
    //         }
          
            
    //     });
          
            
    //     });
    // }
    else{
        this.toaster.ErrorSnack('Start date should be greater than End date');
        err => {
            console.log(err);
            this.errorMessage = <any>err;
        }
    }
  

}
    vacationDetail() {
        var today = new Date();
        var vacationModel = {
            vacationFrom: this.vacationDetails.vacationFrom,
            vacationTo: this.vacationDetails.vacationTo,
        }
  
        this.vacationDetails.vacationTo.setHours(23,59,59);
        if(today.toLocaleDateString()==this.vacationDetails.vacationFrom.toLocaleDateString())
        {
            // console.log(todayDate);
            this.ShopService.vacationdetails(vacationModel).then(res => {
                var result=JSON.parse(res);
                if(result.msg=="Created sucessfully")
                {
                    this.toaster.SuccessSnack(result.msg);
                    this.showlist=true;
                    this.getvacationDetails();  
                }
                else{
                    this.toaster.ErrorSnack(result.msg);
                    this.getvacationDetails();  
                }
              
                
            });
            // this.vacationDetails.vacationFrom.setHours(hours,minutes,seconds);
        }
        else if((vacationModel.vacationFrom < today))
        {
         this.toaster.ErrorSnack('Cannot add vacation for expired date');
        }
        else if ((vacationModel.vacationTo >= vacationModel.vacationFrom)) {
            this.ShopService.vacationdetails(vacationModel).then(res => {
                var result=JSON.parse(res);
                if(result.msg=="Created sucessfully")
                {
                    this.toaster.SuccessSnack(result.msg);
                    this.showlist=true;
                    this.getvacationDetails();  
                }
                else{
                    this.toaster.ErrorSnack(result.msg);
                    this.getvacationDetails();  
                }
              
                
            });
        }
        else{
            this.toaster.ErrorSnack('Start date should be greater than End date');
            err => {
                console.log(err);
                this.errorMessage = <any>err;
            }
        }
    }
    addNew()
    {
        this.vacationDetails={};
        this.showlist=false;
        this.editbutton=false;

        
    }
}


