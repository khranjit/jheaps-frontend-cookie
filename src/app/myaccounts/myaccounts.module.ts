import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { myaccountComponent } from './myaccounts.component'
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import {NgPipesModule} from 'ngx-pipes';
import { AuthGuard } from '../auth/auth-guard.guard';
import { AuthGuardAdmin } from '../auth/admin-auth-guard.guard';


const myaccounts: Routes = 
[
  {path:'user', component:myaccountComponent,loadChildren:'./user/user.module#UserModule', canActivate:[AuthGuard]},
  {path:'seller', component:myaccountComponent,loadChildren:'./seller/seller.module#SellerModule', canLoad:[AuthGuard]},
  {path:'admin', component:myaccountComponent,loadChildren:'./admin/admin.module#AdminModule', canActivate:[AuthGuardAdmin]}
];

@NgModule({
  imports: [
    SharedModule,
    NgPipesModule,
    RouterModule.forChild(myaccounts)
  ],
  declarations: [
    myaccountComponent
  ]
})
export class MyAccountsModule { }
