import { Component, ChangeDetectionStrategy, ViewEncapsulation, Inject } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { FeaturedProductService } from '../../../services/featured.service';
import { NotifyService } from '../../../services/notify.service';

declare var jQuery: any;

@Component({ 
  
  selector: 'featured-product',
  providers: [FeaturedProductService,NotifyService],
  templateUrl: './featured-product.component.html'
})

export class FeaturedProductComponent {
  public featuredProductsList: any;
  public FeaturedProducts: any;
  public errorMessage: any;
  public deletableContent: any;

  constructor(private route: ActivatedRoute, private router: Router,private toasterService: NotifyService, private featuredProductService: FeaturedProductService) {
    this.Init();
  }

  public Init() {
    window.scrollTo(0,20);
    this.featuredProductService.getAll()
      .then(
      featuredProducts => {
        this.featuredProductsList = JSON.parse(featuredProducts);
        this.FeaturedProducts = this.featuredProductsList.Items;
      },
      error => {
        this.toasterService.ErrorSnack( 'You are unauthorized !!..');
          }
      );
  }
  confirmDelete(featuredProduct) {
    this.deletableContent = featuredProduct;
    jQuery("#confirmDeleteModal").modal('show');
  }

  delete() {
    this.featuredProductService.deleteFeaturedProduct(this.deletableContent);
    this.Init();

  }
  addNew() {
    this.router.navigate(['/myaccounts/admin/featured-product/add']);
  }

}