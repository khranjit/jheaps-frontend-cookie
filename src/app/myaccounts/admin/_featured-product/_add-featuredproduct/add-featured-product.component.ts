import { Component, Inject, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../../../../services/product.service';
import { FeaturedProductService } from '../../../../services/featured.service';
import { NotifyService } from '../../../../services/notify.service';

@Component({ 
  
  selector: 'addfeaturedproduct',
  providers: [ProductService,FeaturedProductService,NotifyService],
  templateUrl: './add-featured-product.component.html'
})

export class AddFeaturedProductComponent {
  public errorMessage: any;;
  public product: any;
  public productId: string;
  public featuredProduct:ViewModels.IFeaturedProductViewModel;

  constructor(private route: ActivatedRoute, private router: Router, private productService: ProductService, 
               private featuredProductService:FeaturedProductService,private toasterService: NotifyService) {
    this.Init();
  }
  public Init() {
    window.scrollTo(0,20);
    this.product = {};
    this.productId = "";
    this.featuredProduct =<ViewModels.IFeaturedProductViewModel>{};
    }
  Search(Productid) {
    this.productService.getProduct(Productid)
      .then(
      product => {
        this.product = JSON.parse(product);
      },
      error => {
        this.toasterService.ErrorSnack('You are unauthorized !!..');
          });
  }
  add() {
    this.featuredProduct.Product={
      Id:this.product._id,
      Name:this.product.Name,
      Price:this.product.Price
    }
    this.featuredProduct.Shop={
      Id:this.product.Shop.Id,
      Name:this.product.Shop.Name
    }
    this.featuredProductService.setFeaturedProduct(this.featuredProduct)
      .then(
      product => {
       console.log("faetured Product");
      },
      error => this.errorMessage = <any>error);
  }

  goBack() {
    this.router.navigate(['/myaccounts/admin/featured-product']);
  }
}