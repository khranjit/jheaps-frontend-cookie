import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeaturedProductComponent } from './featured-product.component';
import { AddFeaturedProductComponent } from './_add-featuredproduct/add-featured-product.component';
import { SharedModule } from '../../../shared/shared.module';
  
  
const featuredproduct: Routes = [
    {path: '',redirectTo:'view',pathMatch:'full'},
    {path: 'view', component:FeaturedProductComponent},
    {path: 'add', component:AddFeaturedProductComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(featuredproduct)
  ],
  declarations: [
    FeaturedProductComponent,
    AddFeaturedProductComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class FeauteredProductModule { }