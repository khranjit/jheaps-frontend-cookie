import { Component,Input} from '@angular/core';
import { ReportsService} from '../../../../services/reports.service';
import {  ProductDescriptionService} from '../../../../services/product-description.service';

import { UserService } from '../../../../services/user.service';
import { Router} from '@angular/router';
import { ToasterPopService } from '../../../../services/toasterpop.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FinanceService } from '../../../../services/finance.service';
import { tempservice } from '../../../../services/cart.service';
import { Location } from '@angular/common';


@Component({ 
   selector: 'report18',
//    styleUrls:['seller-reports.component.css'],
   templateUrl: 'report18.component.html',
   providers : [tempservice,ReportsService,UserService,ToasterPopService,FinanceService, ProductDescriptionService]
})

export class Report18Component{
     reportsModel=<any>{} ;
    public OrderList: any;
    productsShow: boolean = true;
     Products: any = [];
     ShopDetails: any = [];
     OrderDetails:any = [];
     reportsReady:boolean = false;
     Years = [];
     ReportsLink:any;
     searchTerm = '';
     errorMessage:any;
     reportName:any ='';
     reportType = <any>{};
     Months =[];
     reportData:any;
     productData:any;
     awsname:any;   
     ShowUserRole : any;
     ShowUserProfile : any; 
     public Month='';
     public Year='';
     public showDate=true; 
     from =0;
     index=0;
     size = 18;
     total=0;
     Orders = [];
        obj_id: any;
        showShop = true;
        showselectedReport = false;
        showOrderDetails = false;
        selectedReport =<any>{};
        Product:any = [] ;
        public imageUrls:any;

    constructor(private loader:NgxSpinnerService,private location: Location,private financeService:FinanceService,private productDescriptionService: ProductDescriptionService, private toasterPopService:ToasterPopService,private reportsService : ReportsService,private tempservice:tempservice,private userService: UserService,private router: Router)
    {
       // this.resetProperties();
        window.scrollTo(0, 20);
        this.Months = [{ value: 0, name: "JAN" }, { value: 1, name: "FEB" }, { value: 2, name: "MAR" }, { value: 3, name: "APR" }, { value: 4, name: "MAY" }, { value: 5, name: "JUN" },
        { value: 6, name: "JUL" }, { value: 7, name: "AUG" }, { value: 8, name: "SEP" }, { value: 9, name: "OCT" }, { value: 10, name: "NOV" }, { value: 11, name: "DEC" }];
        this.reportsModel=this.tempservice.getData();
        this.GetReports();

     
  
   

   
    }

    DownloadReport(){
        this.loader.show();
        this.reportsModel.ReportHeading="Admin-Seller Order Details Report";
        this.reportsService.downloadReport(this.reportsModel).then(res=>{
            var result = JSON.parse(res);    
            if(result.ImageData){
                this.loader.hide();
                window.location.href=result.ImageData;
    
                // window.open(result.ImageData);
               
            }else{
    
            }
        });  
    
    }
    selectedOrder(order) {
        this.selectedReport ={};
        this.showShop = false;
        this.showOrderDetails = false;
        this.showselectedReport = true;
        //this.showPackageForm = false;
        //this.selectedPrintOrders = false;
        this.selectedReport=order;
          this.productDescriptionService.getProductById(this.selectedReport.ProductId)
                        .then(
                        product => {
                            this.Product = JSON.parse(product);
                    
                            for (var i = 0; i < this.Product.Media.length; i++) 
                            {
                                this.imageUrls=this.Product.Media[i].EditedImage
                            } 
                            console.log(this.imageUrls);
                        }
                    );
    

        console.log(this.selectedReport);
    }
    selectedshop(data){
        this.OrderDetails=[];
        for(var i=0;i<this.reportData.length;i++)
        {
            if(this.reportData[i].length>0)
            {
                for(var j=0;j<this.reportData[i].length;j++)
                {
               if( this.reportData[i][j].ShopId==data.Shopid)
               {
                this.OrderDetails.push(this.reportData[i][j]);
             
               }
                }

            }
        }
        console.log(this.OrderDetails);
       this.showOrderDetails = true;
       this.showShop=false;
    }
    gotoShop() {
        this.showShop= true;
        this.showOrderDetails = false;
        this.showselectedReport = false;
       
    }
    gotoReport()
    {
        this.showShop= false;
        this.showOrderDetails = true;
        this.showselectedReport = false;



    }
    topscroll(){
        window.scrollTo(0,0);
    }
goBack()
{
    this.location.back();
}

    GetReports()
    {   
       
        this.loader.show();
      
        this.reportsService.getReports(this.reportsModel).then(
            res =>{
               // console.log(res);
                this.reportData = JSON.parse(res);
                this.loader.hide();
                for(var i=0 ;i<this.reportData.length;i++)
                {
                    if(this.reportData[i].length>0)
                    {
                   var shopid=this.reportData[i][0].ShopId;
                   var shopname=this.reportData[i][0].ShopName;
                   this.ShopDetails.push({Shopid:shopid,Shopname: shopname});
                   console.log(this.ShopDetails);

                    }
                
                   
                }
                if(this.reportsModel.ReportType.SelectedMonth!="" || this.reportsModel.ReportType.SelectedMonth==0 && this.reportsModel.ReportType.SelectedYear!="")
                {
                    this.showDate=false;
                    this.Year=this.reportsModel.ReportType.SelectedYear;
                

                        for (var j = 0; j < this.Months.length; j++) {
    
                            if ( this.reportsModel.ReportType.SelectedMonth == this.Months[j].value)
                            {
                                this.Month = this.Months[j].name;
                            }
                         
                        }
                      
                     
                  
                }
            
               
                this.reportsReady = true;
               
            },
            error => {
                this.errorMessage = <any>error;
                this.loader.hide();
            })
        }       
    }
   
