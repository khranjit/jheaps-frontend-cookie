import { RouterModule, Routes } from '@angular/router';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { Report5Component } from './report5/report5.component';
import { Report6Component } from './report6/report6.component';
import { Report7Component } from './report7/report7.component';
import { Report18Component } from './report18/report18.component';
import { Report20Component } from './report20/report20.component';
import { Report21Component } from './report21/report21.component';
import { Report30Component } from './report30/report30.component';
import { Report29Component } from './report29/report29.component';
import { Report28Component } from './report28/report28.component';
import { Report24Component } from './report24/report24.component';
import { Report23Component } from './report23/report23.component';
import { Report22Component } from './report22/report22.component';
import { Report19Component } from './report19/report19.component';
import { Report13Component } from './report13/report13.component';
import { Report14Component } from './report14/report14.component';
import { Report31Component } from './report31/report31.component';
import { GetAdminReportsComponent } from './getadminreports/getadminreports.component';
import {CalendarModule} from 'primeng/primeng';
import {DropdownModule} from 'primeng/dropdown';


const AdminReports:Routes = [
  { path: '', redirectTo: 'getreports', pathMatch: 'full' },
  { path: 'getreports', component: GetAdminReportsComponent },                    
  { path: 'report5', component: Report5Component },
  { path: 'report6', component: Report6Component },
  { path: 'report7', component: Report7Component },
  { path: 'report13', component: Report13Component },
  { path: 'report14', component: Report14Component },
  { path: 'report18', component: Report18Component },
  { path: 'report19', component: Report19Component },
  { path: 'report20', component: Report20Component },
  { path: 'report21', component: Report21Component },
  { path: 'report22', component: Report22Component },
  { path: 'report23', component: Report23Component },
  { path: 'report24', component: Report24Component },
  { path: 'report28', component: Report28Component },
  { path: 'report29', component: Report29Component },
  { path: 'report30', component: Report30Component },
  { path: 'report31', component: Report30Component }

]

@NgModule({
  imports: [
    CalendarModule,
    SharedModule,
    SharedModule,
    DropdownModule,
    RouterModule.forChild(AdminReports) 
  ],
  declarations: [
    GetAdminReportsComponent,Report5Component,Report6Component,Report7Component,Report18Component,Report20Component,Report21Component,Report30Component,Report29Component,Report28Component,Report24Component,Report23Component,Report22Component,Report19Component,Report13Component,Report14Component,Report31Component
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ],
})
export class AdminReportsModule { }
