import { Component,Input} from '@angular/core';
import { ReportsService} from '../../../../services/reports.service';
import {  ProductDescriptionService} from '../../../../services/product-description.service';
import { UserService } from '../../../../services/user.service';
import { Router} from '@angular/router';
import { ToasterPopService } from '../../../../services/toasterpop.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FinanceService } from '../../../../services/finance.service';
import { tempservice } from '../../../../services/cart.service';
import { Location } from '@angular/common';


@Component({ 
   selector: 'report22',
//    styleUrls:['seller-reports.component.css'],
   templateUrl: 'report22.component.html',
   providers : [tempservice,ReportsService,UserService,ToasterPopService,FinanceService, ProductDescriptionService]
})

export class Report22Component{
    reportsModel=<any>{} ;
    public OrderList: any;
    productsShow: boolean = true;
     Products: any = [];
     reportsReady:boolean = false;
     Years = [];
     ReportsLink:any;
     searchTerm = '';
     errorMessage:any;
     reportName:any ='';
     reportType = <any>{};
     Months =[];
     reportData:any=[];
     getreportData:any;
     productData:any;
     awsname:any;   
     ShowUserRole : any;
     ShowUserProfile : any;  
     showlist=true;
     from =0;
     index=0;
     size = 18;
     total=0;
     shopAverage:any;
     Orders = [];
        obj_id: any;
        showReport = true;
        showProduct = true;

        showselectedReport = false;
        selectedReport =<any>{};
        Product:any = [] ;
        public imageUrls:any;

    constructor(private loader:NgxSpinnerService,private location: Location,private financeService:FinanceService,private productDescriptionService: ProductDescriptionService, private toasterPopService:ToasterPopService,private reportsService : ReportsService,private tempservice:tempservice,private userService: UserService,private router: Router)
    {
       // this.resetProperties();
        window.scrollTo(0, 20);
        this.reportsModel=this.tempservice.getData();
        this.GetReports();


    }
    viewProductlist(){
        this.showlist=true;
        this.showProduct=false;
    }
    hideProductlist(){
        this.showlist=false;
        this.showProduct=true;

    }

    DownloadReport(){
        this.loader.show();
        this.reportsModel.ReportHeading="Admin-Market place Commission";
        this.reportsService.downloadReport(this.reportsModel).then(res=>{
            var result = JSON.parse(res);    
            if(result.ImageData){
                this.loader.hide();
                window.location.href=result.ImageData;
    
                // window.open(result.ImageData);
               
            }else{
    
            }
        });  
    
    }
    selectedProduct(data){
        this.selectedReport = data;
        this.showlist = false;
    }
goBack()
{
    this.location.back();
    // this.showlist = true;
}
// goBack1()
// {
//     this.location.back();
   
// }


    GetReports()
    {   
    this.loader.show();
    this.reportsService.getReports(this.reportsModel).then(
            res =>{
               // console.log(res);
                this.reportData = JSON.parse(res);
                this.loader.hide();
                 var length = this.reportData.length;
                 this.showlist = true;
            },
            error => {
                this.errorMessage = <any>error;
                this.loader.hide();
            })
        }       
    }
   
