import { Component,Input, Output, EventEmitter} from '@angular/core';
import { ReportsService} from '../../../../services/reports.service';
import {  ProductDescriptionService} from '../../../../services/product-description.service';

import { UserService } from '../../../../services/user.service';
import { Router} from '@angular/router';
import { ToasterPopService } from '../../../../services/toasterpop.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FinanceService } from '../../../../services/finance.service';
import { tempservice } from '../../../../services/cart.service';
import { Location } from '@angular/common';


@Component({ 
   selector: 'report14',

   templateUrl: 'report14.component.html',
   providers : [tempservice,ReportsService,UserService,ToasterPopService,FinanceService, ProductDescriptionService]
})

export class Report14Component{    
    reportsModel=<any>{} ;
    @Output() exampleOutput = new EventEmitter<string>();
    public OrderList: any;
    productsShow: boolean = true;
     Products: any = [];
     reportsReady:boolean = false;
     Years = [];
     ReportsLink:any;
     searchTerm = '';
     errorMessage:any;
     reportName:any ='';
     reportType = <any>{};
     Months =[];
     reportData:any=[];
     getreportData:any;
     productData:any;
     awsname:any;   
     ShowUserRole : any;
     ShowUserProfile : any;  
     showlist=false;
     from =0;
     index=0;
     size = 18;
     total=0;
     shopAverage:any;
     public Month='';
     public Year='';
     public showDate=true;
     public showMonth=false;
     public showAllTime=false;
     Orders = [];
        obj_id: any;
        showReport = true;
        showProduct = true;

        showselectedReport = false;
        selectedReport =<any>{};
        Product:any = [] ;
        public imageUrls:any;

    constructor(private loader:NgxSpinnerService,private location: Location,private financeService:FinanceService,private productDescriptionService: ProductDescriptionService, private toasterPopService:ToasterPopService,private reportsService : ReportsService,private tempservice:tempservice,private userService: UserService,private router: Router)
    {
       // this.resetProperties();
        window.scrollTo(0, 20);
        this.Months = [{ value: 0, name: "JAN" }, { value: 1, name: "FEB" }, { value: 2, name: "MAR" }, { value: 3, name: "APR" }, { value: 4, name: "MAY" }, { value: 5, name: "JUN" },
        { value: 6, name: "JUL" }, { value: 7, name: "AUG" }, { value: 8, name: "SEP" }, { value: 9, name: "OCT" }, { value: 10, name: "NOV" }, { value: 11, name: "DEC" }];
        this.reportsModel=this.tempservice.getData();
        this.GetReports();


    }
    viewProductlist(){
        this.showlist=true;
        this.showProduct=false;
    }
    hideProductlist(){
        this.showlist=false;
        this.showProduct=true;

    }

    DownloadReport(){
        this.loader.show();
        this.reportsModel.ReportHeading="Admin-Product Review Report";
        this.reportsService.downloadReport(this.reportsModel).then(res=>{
            var result = JSON.parse(res);    
            if(result.ImageData){
                this.loader.hide();
                window.location.href=result.ImageData;
    
                // window.open(result.ImageData);
               
            }else{
    
            }
        });  
    
    }
goBack()
{
    this.location.back();
}
selectedProduct(data){
    this.router.navigate(['/user/product/'+data.ProductId]);
}


GetReports()
{ 
this.loader.show();

this.reportsService.getReports(this.reportsModel).then(
res =>{
// console.log(res);
this.reportData = JSON.parse(res);
this.loader.hide();
if(this.reportsModel.ReportType.Condition=="Condition4")
{
this.showAllTime=true;
this.showDate=false;
}

if(this.reportsModel.ReportType.SelectedMonth!="" || this.reportsModel.ReportType.SelectedMonth==0 && this.reportsModel.ReportType.SelectedYear!="")
{ 


this.showDate=false;
this.showMonth=true;
this.showAllTime=false;
this.Year=this.reportsModel.ReportType.SelectedYear;


for (var j = 0; j < this.Months.length; j++) {

if ( this.reportsModel.ReportType.SelectedMonth == this.Months[j].value)
{
this.Month = this.Months[j].name;
}

}



}


var num=this.reportData.length-1;
this.shopAverage=this.reportData[num].AverageRating;
this.reportData=this.reportData.slice(0,num);
this.reportData.sort(function (a, b) {
return b.AverageRating-a.AverageRating;
});
// console.log(this.reportData);
// console.log(this.shopAverage);

},
error => {
this.errorMessage = <any>error;

}) 

}    
    }
   
   
