import { Component,Input} from '@angular/core';
import { ReportsService} from '../../../../services/reports.service';
import {  ProductDescriptionService} from '../../../../services/product-description.service';
import { UserService } from '../../../../services/user.service';
import { Router} from '@angular/router';
import { ToasterPopService } from '../../../../services/toasterpop.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FinanceService } from '../../../../services/finance.service';
import { tempservice } from '../../../../services/cart.service';
import { Location } from '@angular/common';


@Component({ 
   selector: 'report29',
//    styleUrls:['seller-reports.component.css'],
   templateUrl: 'report29.component.html',
   providers : [tempservice,ReportsService,UserService,ToasterPopService,FinanceService, ProductDescriptionService]
})

export class Report29Component{
     reportsModel=<any>{} ;
    public OrderList: any;
    productsShow: boolean = true;
     Products: any = [];
     ShopProfitDetails: any = [];
     UnsoldDetails:any = [];
     reportsReady:boolean = false;
     Years = [];
     ReportsLink:any;
     searchTerm = '';
     errorMessage:any;
     reportName:any ='';
     reportType = <any>{};
     Months =[];
     reportData:any;
     productData:any;
     awsname:any;   
     ShowUserRole : any;
     ShowUserProfile : any;  
     from =0;
     index=0;
     size = 18;
     total=0;
     Results1 =<any>{};
     Orders = [];
        obj_id: any;
        showShop = true;
        showUnsold=false;
        selectedDetail: any;
        selectedReport =<any>{};
        Product:any = [] ;
        ShopDetails: any = [];
        public imageUrls:any;
        public Month='';
        public Year='';
        public showDate=true;

    constructor(private loader:NgxSpinnerService,private location: Location,private financeService:FinanceService,private productDescriptionService: ProductDescriptionService, private toasterPopService:ToasterPopService,private reportsService : ReportsService,private tempservice:tempservice,private userService: UserService,private router: Router)
    {
       // this.resetProperties();
        window.scrollTo(0, 20);
        this.Months = [{ value: 1, name: "JAN" }, { value: 2, name: "FEB" }, { value: 3, name: "MAR" }, { value: 4, name: "APR" }, { value: 5, name: "MAY" }, { value: 6, name: "JUN" },
        { value: 7, name: "JUL" }, { value: 8, name: "AUG" }, { value: 9, name: "SEP" }, { value: 10, name: "OCT" }, { value: 11, name: "NOV" }, { value: 12, name: "DEC" }];
        this.reportsModel=this.tempservice.getData();
        this.GetReports();   
    }
    
    selectedShop(data){
        this.UnsoldDetails=[];
        this.selectedDetail=data.Result[0];
        this.showShop=false;
        this.showUnsold=true;  
    }
    selectedItem(data){
        this.router.navigate(['/product/data.Product_Id']);
    }
    gotoShop() {
        this.showShop= true;
       this.showUnsold=false;

       
    }
    topscroll(){
        window.scrollTo(0,0);
    }
    DownloadReport(){
        this.reportsModel.ReportHeading="Admin-Ready For Shipment Breach Report";
        this.loader.show();
        this.reportsModel.dwnloadFlag=1;
        this.reportsService.downloadReport(this.reportsModel).then(res=>{
        var result = JSON.parse(res); 
        if(result.ImageData){
        this.loader.hide();
        window.location.href=result.ImageData;
        }else{
        
        }
        }); 
    }
goBack()
{
    this.showShop= true;
    this.showUnsold=false;
}    
goBack1()
{
    this.location.back();
}                        

    GetReports()
    {   
       
        this.loader.show();
        this.reportsService.getReports(this.reportsModel).then(
            res =>{
               // console.log(res);
               var result= JSON.parse(res);
               this.loader.hide();
               this.Results1 = result[0];
                this.reportData = result[0].Result;   
                if(this.reportsModel.ReportType.SelectedMonth!="" || this.reportsModel.ReportType.SelectedMonth==0 && this.reportsModel.ReportType.SelectedYear!="")
                {
                    this.showDate=false;
                    this.Year=this.reportsModel.ReportType.SelectedYear;
                

                        for (var j = 0; j < this.Months.length; j++) {
    
                            if ( this.reportsModel.ReportType.SelectedMonth == this.Months[j].value)
                            {
                                this.Month = this.Months[j].name;
                            }
                         
                        }
                      
                     
                  
                }      
                this.reportsReady = true;

               
            },
            error => {
                this.errorMessage = <any>error;
                this.loader.hide();
            })
        }       
    }
   
