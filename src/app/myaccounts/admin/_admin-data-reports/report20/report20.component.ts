import { Component,Input} from '@angular/core';
import { ReportsService} from '../../../../services/reports.service';
import {  ProductDescriptionService} from '../../../../services/product-description.service';

import { UserService } from '../../../../services/user.service';
import { Router} from '@angular/router';
import { ToasterPopService } from '../../../../services/toasterpop.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FinanceService } from '../../../../services/finance.service';
import { tempservice } from '../../../../services/cart.service';
import { Location } from '@angular/common';


@Component({ 
   selector: 'report20',
//    styleUrls:['seller-reports.component.css'],
   templateUrl: 'report20.component.html',
   providers : [tempservice,ReportsService,UserService,ToasterPopService,FinanceService, ProductDescriptionService]
})


export class Report20Component{
    
    reportsModel=<any>{} ;
    public OrderList: any;
    productsShow: boolean = true;
     Products: any = [];
     reportsReady:boolean = false;
     Years = [];
     ReportsLink:any;
     searchTerm = '';
     errorMessage:any;
     reportName:any ='';
     reportType = <any>{};
     Months =[];
     reportData:any=[];
     getreportData:any;
     productData:any;
     awsname:any;   
     ShowUserRole : any;
     ShowUserProfile : any;  
     from =0;
     index=0;
     size = 18;
     total=0;
     Orders = [];
        obj_id: any;
        showReport = true;
        showselectedReport = false;
        selectedReport =<any>{};
        Product:any = [] ;
        public imageUrls:any;
        public Month='';
        public Year='';
        public showDate=true;


    constructor(private loader:NgxSpinnerService,private location: Location,private financeService:FinanceService,private productDescriptionService: ProductDescriptionService, private toasterPopService:ToasterPopService,private reportsService : ReportsService,private tempservice:tempservice,private userService: UserService,private router: Router)
    {
       // this.resetProperties();
        window.scrollTo(0, 20);
        this.reportsModel=this.tempservice.getData();
        this.Months = [{ value: 0, name: "JAN" }, { value: 1, name: "FEB" }, { value: 2, name: "MAR" }, { value: 3, name: "APR" }, { value: 4, name: "MAY" }, { value: 5, name: "JUN" },
        { value: 6, name: "JUL" }, { value: 7, name: "AUG" }, { value: 8, name: "SEP" }, { value: 9, name: "OCT" }, { value: 10, name: "NOV" }, { value: 11, name: "DEC" }];
        this.GetReports();

        //Prepare months
        // this.Months = [{value:0,name:"JAN"},{value:1,name:"FEB"},{value:2,name:"MAR"},{value:3,name:"APR"},{value:4,name:"MAY"},{value:5,name:"JUN"},
        //                {value:6,name:"JUL"},{value:7,name:"AUG"},{value:8,name:"SEP"},{value:9,name:"OCT"},{value:10,name:"NOV"},{value:11,name:"DEC"}];
        
        //Prepare years               
        // var dt = new Date().getUTCFullYear();
        // for(var i = dt; i>=dt-10; i--)
        // {
        //     this.Years.push({value:i,name:i});
        // }


    //     this.userService.showProfile().then(
    //         response => {
    //             this.ShowUserProfile = JSON.parse(response); 
    //             this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
    //                 console.log('type of user'+this.ShowUserRole)
    //                 if(this.ShowUserRole== 'seller'){
    //                     this.getsellerorderdetails();
    //                 }
    //                 else{
    //                     this.router.navigate(['']);
    //                 }
    // //         }
    // //        );
    // // }   
    // // getsellerorderdetails(){
    //     var reportsModel = {
    //                 ReportType : "Report-1",
    //           ReportName : "current-fiscal-year"
    //         } 
    //     this.OrderList = [];
    //     this.reportsService.getReport1(reportsModel)
    //         .then(
    //             order => {
    //                 console.log(order);
    //                 this.OrderList = JSON.parse(order);
    //                 this.obj_id = this.OrderList._id;
    //                 this.Orders = this.OrderList;
    //                 //this.ValidateScrollContainer();
    //             },
    //             error => this.errorMessage = <any>error);
    // }

    // resetProperties(){
    //     this.reportType = <any>{};
    //     this.reportName = '';
    //     this.reportType.Condition = "Condition1";
    //     this.reportType.Name = "";
    //     this.reportType.SelectedMonth = "";
    //     this.reportType.SelectedYear = "";
    //     this.reportType.selectedProducts = [];
    //     this.productsShow = true;
    //     this.searchTerm = '';

    // }  
    }
    // GetReports(){
    //     this.router.navigate(['/myaccounts/user/reports/seller-reports']);

    // }

    // public searchProducts(searchTerm) {
	// 	var searchModel = {
	// 		SearchQuery: searchTerm,
	// 		from: 0,
	// 		size: this.size,
    //     }
    //     this.ResetPagination();
    //     this.getProducts(searchModel);
    // }
    
    // LoadOnScroll() {
	// 	var scrollContainer = document.getElementById('scroll-container');
	// 	var scrollBox = document.getElementById('scroll-box');
	// 	var scrollContainerPos = scrollContainer.getBoundingClientRect();
	// 	var scrollBoxPos = scrollBox.getBoundingClientRect();
	// 	var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

	// 	if (scrollLength - scrollContainer.scrollTop < 1) {
	// 		setTimeout(() => {
    //             this.from += this.size;
	// 			var searchModel = {
	// 				from: this.from,
	// 				size: this.size,
	// 				SearchQuery: this.searchTerm
	// 			}
	// 			this.getProductsOnScroll(searchModel);
	// 		}, 500)
	// 	}
    // }
    
    // ResetPagination() {
	// 	this.from = 0;
	// 	this.total = 0;
	// 	jQuery('#scroll-container').css("height", 500);
	// }

	// ValidateScrollContainer() {
	// 	setTimeout(() => {
	// 		var scrollContainer = document.getElementById('scroll-container');
	// 		var scrollBox = document.getElementById('scroll-box');
	// 		var scrollContainerPos = scrollContainer.getBoundingClientRect();
	// 		var scrollBoxPos = scrollBox.getBoundingClientRect();
	// 		if (scrollBoxPos.height < scrollContainerPos.height) {
	// 			jQuery('#scroll-container').css("height", scrollBoxPos.height);
	// 		}
	// 	}, 1000)
    // }
    
    // getProductsOnScroll(searchModel) {
		
	// 	if (searchModel.from < this.total) {
			
	// 		this.financeService.getCouponProducts(searchModel).then(
	// 			res => {
	// 				var data = JSON.parse(res).Items;
	// 				this.total = JSON.parse(res).TotalItems;
	// 				this.Products = this.Products.concat(data.Items);
					

	// 			},
	// 			error => {
	// 				this.errorMessage = <any>error;
	// 			}
	// 		);
	// 	}
    // }
    
    // getProducts(searchModel){
		
	// 	this.financeService.getCouponProducts(searchModel).then(
	// 		res => {
	// 			this.Products = JSON.parse(res).Items;
				
	// 			this.total = JSON.parse(res).TotalItems;
				
	// 			this.ValidateScrollContainer();
	// 		},
	// 		error => {
	// 			this.errorMessage = <any>error;
	// 		}
	// 	);
    // }
    
    // selectionChange(Product)
    // {
    //     //console.log("Inside selection change" + item);
	// 	var toAdd = true;
	// 	this.reportType.selectedProducts.forEach((ele, i) => {
	// 		if (ele == Product._id) {
	// 			toAdd = false;
	// 			this.reportType.selectedProducts.splice(i, 1);
	// 		}
	// 	});

	// 	if (toAdd) {
	// 		this.reportType.selectedProducts.push(Product._id);
	// 	}
    // }

    // selectAllChange(event)
    // {
    //     if(event.target.checked == true)
    //     {
    //         this.productsShow = false;
    //     }
        
    //     else{
    //         this.productsShow = true;
    //     }

    // }

    // ReportsSelectionChanged(event)
    // {
    //     console.log("Inside change")
    //     if(event.target.value == "Report-2")
    //     {
    //         var searchModel = {
    //             SearchQuery: this.searchTerm,
    //             from: 0,
    //             size: this.size,
    //         }
    //         this.getProducts(searchModel);
    //     }
    // }

    DownloadReport(){
        this.loader.show();
        this.reportsModel.ReportHeading="Admin-Profitability Report";
        this.reportsService.downloadReport(this.reportsModel).then(res=>{
            var result = JSON.parse(res);    
            if(result.ImageData){
                this.loader.hide();
                window.location.href=result.ImageData;
    
                // window.open(result.ImageData);
               
            }else{
    
            }
        });  
    
    }

    selected(data) {
        this.selectedReport ={};
        this.showReport = false;
        this.showselectedReport = true;
        //this.showPackageForm = false;
        //this.selectedPrintOrders = false;
        this.selectedReport=data;
          this.productDescriptionService.getProductById(this.selectedReport.ProductId)
                        .then(
                        product => {
                            this.Product = JSON.parse(product);
                    
                            for (var i = 0; i < this.Product.Media.length; i++) 
                            {
                                this.imageUrls=this.Product.Media[i].EditedImage
                            } 
                            // console.log(this.imageUrls);
                        }
                    );
    

        // console.log(this.selectedReport);
    }
    report() {
        this.showReport = true;
        this.showselectedReport = false;
    }
  
goBack()
{
    this.location.back();
}

    GetReports()
    {   
       
       this.loader.show();
        this.reportsService.getReports(this.reportsModel).then(
            res =>{
               // console.log(res);
                this.reportData = JSON.parse(res);
                this.loader.hide();
                if(this.reportsModel.ReportType.SelectedMonth!="" || this.reportsModel.ReportType.SelectedMonth==0 && this.reportsModel.ReportType.SelectedYear!="")
                {
                    this.showDate=false;
                    this.Year=this.reportsModel.ReportType.SelectedYear;
                

                        for (var j = 0; j < this.Months.length; j++) {
    
                            if ( this.reportsModel.ReportType.SelectedMonth == this.Months[j].value)
                            {
                                this.Month = this.Months[j].name;
                            }
                         
                        }
                      
                     
                  
                    }
                //this.ReportsLink=this.reportData.ImageData;
                //this.download(this.ReportsLink);
                this.reportsReady = true;
                //this.reportsModel=this.tempservice.clearData();
                //this.resetProperties();
                //this.loader.hide();
            },
            error => {
                this.errorMessage = <any>error;
                this.loader.hide();
            })
        }    
    }
   
