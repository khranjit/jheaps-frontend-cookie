import { Component,ViewChild,AfterViewInit} from '@angular/core';
import { ReportsService} from '../../../../services/reports.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyService } from '../../../../services/notify.service';
import { FinanceService } from '../../../../services/finance.service';
import { UserService } from '../../../../services/user.service';
import { tempservice } from '../../../../services/cart.service';
import {Report14Component} from '../report14/report14.component';

@Component({ 
   selector: 'getadminreports',
   styleUrls:['getadminreports.component.css'],
  templateUrl: './getadminreports.component.html',
  providers : [tempservice,ReportsService,UserService,NotifyService,FinanceService]
})

export class GetAdminReportsComponent implements AfterViewInit {
    @ViewChild(Report14Component) childReference;

    productsShow: boolean = true;
    public userName: string;
     Products: any = [];
     display:false;
     BatchId : string;
     OrderId : any;
     InputType: any;
     testrun : boolean;
     reportsReady:boolean = false;
     enableSelection:boolean = false;
     Years = [];
     ReportsLink:any;
     searchTerm = '';
     errorMessage:any;
     reportsModel:any={};
     reportName:any ='';
     reportType = <any>{};
     Months =[];
     reportData:any;
     awsname:any;   
     ShowUserRole : any;
     ShowUserProfile : any;  
     from =0;
     size = 18;
     total=0;
     currentPage = 1;
     public getAllSellerList: any;
     getSellerList = [{ "label": "Select a shop" ,"value":""}];
     ngAfterViewInit() {

    }
    constructor(private loader:NgxSpinnerService,private tempservice:tempservice,private financeService:FinanceService, private toasterPopService:NotifyService,private reportsService : ReportsService,private userService: UserService,private router: Router)
    {
        this.resetProperties();
        window.scrollTo(0, 20);
        //Prepare months
        this.Months = [{value:0,name:"JAN"},{value:1,name:"FEB"},{value:2,name:"MAR"},{value:3,name:"APR"},{value:4,name:"MAY"},{value:5,name:"JUN"},
                       {value:6,name:"JUL"},{value:7,name:"AUG"},{value:8,name:"SEP"},{value:9,name:"OCT"},{value:10,name:"NOV"},{value:11,name:"DEC"}];
        
        //Prepare years               
        var dt = new Date().getUTCFullYear();
        for(var i = dt; i>=dt-10; i--)
        {
            this.Years.push({value:i,name:i});
        }

        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response); 
                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                if(this.ShowUserProfile.Items[0].UserType !=3){
                    this.router.navigate(['user/home']);
                }
                    console.log('type of user'+this.ShowUserRole)
                    // if(this.ShowUserRole== 'masteradmin'){
                        // this.getSellerList=[];
            }
           );
           this.setsellers();

    }  
   

    setsellers(){
        this.userService.getAllShop().then(
            response => {
                this.getAllSellerList = JSON.parse(response);
                var shopdetail=this.getAllSellerList;
                //console.log('shopdetails',shopdetail);
                shopdetail.forEach(value => {
                                let obj = { "label": value.shopName, "value": value.shopId};
                                this.getSellerList.push(obj);
                            });
                    console.log('shopdetails',this.getSellerList);
            },
            error => this.errorMessage = <any>error);
    }
    onGroupSelect(value){

        let obj = this.getSellerList.find(obj => obj.value == value);
        this.reportType.ShopName = obj.label;
        var searchModel = {
			from: 0,
            size: this.size,
            Id: this.reportType.SellerId
        }
        this.Products = [];
        this.getProducts(searchModel);
    }

    ViewSettlementDetails(){
        var model;
        if(this.InputType == 1){
             model = {
                OrderId : this.OrderId
            }
        }else if(this.InputType == 2){      
             model = {
                BatchId : this.OrderId
            }
        }else{
            model = {
                SettlementId : this.OrderId
            }
        }
        this.loader.show();
        this.reportsService.ViewSettlementReport(model).then(res=>{
          var result = JSON.parse(res);  
          this.loader.hide();
          if(result.flag == false){
            this.toasterPopService.ErrorSnack(result.msg)
          }  
          else if(result.ImageData){
              window.location.href=result.ImageData;
              // window.open(result.ImageData);
          }else{
  
          }
        }) 
    }
    GenerateCODUserReport(){
        this.loader.show();
        this.reportsService.downloadCODSettlementReport(this.BatchId,this.testrun).then(res=>{
          var result = JSON.parse(res);    
          if(result.ImageData){
              this.loader.hide();
              window.location.href=result.ImageData;
  
              // window.open(result.ImageData);
             
          }else{
              this.loader.hide();
              if(result.message){
                  this.toasterPopService.ErrorSnack(result.message)
              }else{
                  this.toasterPopService.ErrorSnack("No Results")

              }
          }
        }) 
    }

    GenerateSettlementReport(){
        this.loader.show();
          this.reportsService.downloadSettlementReport(this.BatchId,this.testrun).then(res=>{
            var result = JSON.parse(res);    
            if(result.ImageData){
                this.loader.hide();
                window.location.href=result.ImageData;
    
                // window.open(result.ImageData);
               
            }else{
                this.loader.hide();
                if(result.message){
                    this.toasterPopService.ErrorSnack(result.message)
                }else{
                    this.toasterPopService.ErrorSnack("No Results")

                }
            }
          })
      }
    
    resetProperties(){
        this.reportType = <any>{};
        this.reportName = '';
        this.reportType.Condition = "Condition2";
        this.reportType.Name = "";
        this.reportType.SelectedMonth = "";
        this.reportType.SelectedYear = "";
        this.reportType.selectedProducts = [];
        this.productsShow = true;
        this.searchTerm = '';

    }  

    public searchProducts(searchTerm) {
		var searchModel = {
			SearchQuery: searchTerm,
			from: 0,
            size: this.size,
            Id: this.reportType.SellerId
        }
        this.ResetPagination();
       this.getProducts(searchModel);
          window.scrollTo(0,20);
        this.currentPage = 1;
        this.from = 0;
        // this.GetSellerList(this.userName,this.currentPage -1);
    }
    
    // LoadOnScroll() {
	// 	var scrollContainer = document.getElementById('scroll-container');
	// 	var scrollBox = document.getElementById('scroll-box');
	// 	var scrollContainerPos = scrollContainer.getBoundingClientRect();
	// 	var scrollBoxPos = scrollBox.getBoundingClientRect();
	// 	var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

	// 	if (scrollLength - scrollContainer.scrollTop < 1) {
	// 		setTimeout(() => {
    //             this.from += this.size;
	// 			var searchModel = {
	// 				from: this.from,
	// 				size: this.size,
	// 				SearchQuery: this.searchTerm
	// 			}
    //             this.getProductsOnScroll();

    //             this.currentPage = 1;
    //             this.from = 0;
    //             this.GetSellerList(this.userName,this.currentPage -1);
                
                
	// 		}, 500)
	// 	}
    // }

    LoadOnScroll() {
		var scrollContainer = document.getElementById('scroll-container');
		var scrollBox = document.getElementById('scroll-box');
		var scrollContainerPos = scrollContainer.getBoundingClientRect();
		var scrollBoxPos = scrollBox.getBoundingClientRect();
		var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

		if (scrollLength - scrollContainer.scrollTop < 1) {
			setTimeout(() => {
                this.from += this.size;
				var searchModel = {
					from: this.from,
					size: this.size,
                    SearchQuery: this.searchTerm,
                    Id: this.reportType.SellerId
				}
				this.getProductsOnScroll(searchModel);
			}, 500)
		}
    }
    
    ResetPagination() {
		this.from = 0;
		this.total = 0;
		jQuery('#scroll-container').css("height", 500);
	}

	// ValidateScrollContainer() {
	// 	setTimeout(() => {
	// 		var scrollContainer = document.getElementById('scroll-container');
    //         var scrollBox = document.getElementById('scroll-box');
    //         if(scrollContainer!=null && scrollBox!=null){
	// 		var scrollContainerPos = scrollContainer.getBoundingClientRect();
	// 		var scrollBoxPos = scrollBox.getBoundingClientRect();
	// 		if (scrollBoxPos.height < scrollContainerPos.height) {
	// 			jQuery('#scroll-container').css("height", scrollBoxPos.height);
    //         }
    //     }
	// 	}, 1000)
    // }
    
    // getProductsOnScroll() {
		
	// 	//if (searchModel.from < this.total) {
	// 		var pageNum=this.currentPage;
    //         this.userService.getAllSellers(this.userName, pageNum).then(
	// 			res => {
	// 				var data = JSON.parse(res).Items;
	// 				this.total = JSON.parse(res).TotalItems;
    //                 this.getSellerList = this.Products.concat(data.Items);
					

	// 			},
	// 			error => {
	// 				this.errorMessage = <any>error;
	// 			}
	// 		);
		
    // }
    GetSellerList(userName, pageNum)
    {
       this.userService.getAllSellers(this.userName, pageNum).then(
           response => {
       //        console.log("response", response);
               this.getAllSellerList = JSON.parse(response);
               this.getSellerList = this.getAllSellerList.Items;
               console.log("seller-list",  this.getSellerList);
                 //this.totalPages = res.TotalPages;
   
                 this.total = JSON.parse(response).TotalItems;
				
                 this.ValidateScrollContainer();
             },
        
           error => this.errorMessage = <any>error);
    }
    
    
    // selectionChange(Product)
    // {
    //     //console.log("Inside selection change" + item);
	// 	var toAdd = true;
	// 	this.reportType.selectedProducts.forEach((ele, i) => {
	// 		if (ele == Product.Shop) {
	// 			toAdd = false;
	// 			this.reportType.selectedProducts.splice(i, 1);
	// 		}
	// 	});

	// 	if (toAdd) {
	// 		this.reportType.selectedProducts.push(Product.Shop);
    //     }

    // }


    // selectAllChange(event)
    // {
    //     if(event.target.checked == true)
    //     {
    //         this.productsShow = false;
    //     }
        
    //     else{
    //         this.productsShow = true;
    //     }

    // }

    // ReportsSelectionChanged(event)
    // {
    //     console.log("Inside change")
    
    //     if(event.target.value == "Report-18")
    //     {
    //         this.enableSelection=true;
    //         var searchModel = {
    //             SearchQuery: this.searchTerm,
    //             from: 0,
    //             size: this.size,
    //         }
    //         //this.getProducts(searchModel);
         
    //         this.currentPage = 1;
    //         this.from = 0;
    //         this.GetSellerList(this.userName,this.currentPage -1);
            
    //     }
    //     else{
    //         this.enableSelection=false;
    //     }
    // }

    ValidateScrollContainer() {
		setTimeout(() => {
			var scrollContainer = document.getElementById('scroll-container');
            var scrollBox = document.getElementById('scroll-box');
            if(scrollContainer!=null && scrollBox!=null){
			var scrollContainerPos = scrollContainer.getBoundingClientRect();
			var scrollBoxPos = scrollBox.getBoundingClientRect();
			if (scrollBoxPos.height < scrollContainerPos.height) {
				jQuery('#scroll-container').css("height", scrollBoxPos.height);
            }
        }
		}, 1000)
    }
    
    getProductsOnScroll(searchModel) {
		
		if (searchModel.from < this.total) {
			
			this.financeService.getCouponProducts(searchModel).then(
				res => {
					var data = JSON.parse(res).Items;
					this.total = JSON.parse(res).TotalItems;
					this.Products = this.Products.concat(data);
				},
				error => {
					this.errorMessage = <any>error;
				}
			);
		}
    }
    
    getProducts(searchModel){
		
		this.financeService.getCouponProducts(searchModel).then(
			res => {
				this.Products = JSON.parse(res).Items;
				
				this.total = JSON.parse(res).TotalItems;
				
				this.ValidateScrollContainer();
			},
			error => {
				this.errorMessage = <any>error;
			}
		);
    }
    
    selectionChange(Product)
    {
        //console.log("Inside selection change" + item);
		var toAdd = true;
		this.reportType.selectedProducts.forEach((ele, i) => {
			if (ele == Product._id) {
				toAdd = false;
				this.reportType.selectedProducts.splice(i, 1);
			}
		});

		if (toAdd) {
			this.reportType.selectedProducts.push(Product._id);
		}
    }

    selectAllChange(event)
    {
        if(event.target.checked == true)
        {
            this.productsShow = false;
        }
        
        else{
            this.productsShow = true;
        }

    }

    ReportsSelectionChanged(event)
    {
        console.log("Inside change")
        if(event.target.value == "Report-2")
        {
            var searchModel = {
                SearchQuery: this.searchTerm,
                from: 0,
                size: this.size,
                Id : this.reportType.SellerId
            }
            this.getProducts(searchModel);
        }
    }

    Report33(){

        this.loader.show();
        this.reportsService.Report33(this.reportsModel).then(res=>{
          var result = JSON.parse(res);  
          this.loader.hide();
          if(result.flag == false){
            this.toasterPopService.ErrorSnack(result.msg)
          }  
          else if(result.ImageData){
              window.location.href=result.ImageData;
              // window.open(result.ImageData);
          }else{
  
          }
        }) 
    }

    GetReports()
    {   
       
        if (this.reportType.Condition=="Condition2" && (this.reportType.StartDate > this.reportType.ExpiryDate)) {
            this.toasterPopService.ErrorSnack('Start date cannot be greater than Expiry date');
            return;
        }

        if(this.reportType.Condition == "Condition3")
        {
           var selectedMonth = parseInt(this.reportType.SelectedMonth)
           var selectedYear =  parseInt(this.reportType.SelectedYear)
           var startDate = new Date(selectedYear, selectedMonth,1);
           var expiryDate = new Date(selectedYear, selectedMonth+1,0)
           this.reportType.Condition = "Condition2";
           this.reportType.StartDate = startDate;
           this.reportType.ExpiryDate = expiryDate;
        }
        console.log(this.reportName, this.reportType);
        if(this.reportName && this.reportType){       
        this.reportsModel = {
            ReportType : this.reportType,
            ReportName : this.reportName,
            id: this.reportType.SellerId,
            ShopName : this.reportType.ShopName
        } 
        this.tempservice.setData(this.reportsModel);
     
        switch (this.reportName) {
          case 'Report-5': {
              //console.log("entered into case Report-1");
              this.router.navigate(['/myaccounts/admin/reports/report5']);
              break;
          }
          case 'Report-6': {
            //console.log("entered into case Report-1");
            this.router.navigate(['/myaccounts/admin/reports/report6']);
            break;
        }
        case 'Report-7': {
            //console.log("entered into case Report-1");
            this.router.navigate(['/myaccounts/admin/reports/report7']);
            break;
        }
          case 'Report-13': {
            //console.log("entered into case Report-1");
            this.router.navigate(['/myaccounts/admin/reports/report13']);
            break;
        }
        case 'Report-14': {
            //console.log("entered into case Report-1");
            this.router.navigate(['/myaccounts/admin/reports/report14']);
            break;
        }
          case 'Report-18': {
            //console.log("entered into case Report-1");
            this.router.navigate(['/myaccounts/admin/reports/report18']);
            break;
        }
        case 'Report-19': {
            //console.log("entered into case Report-1");
            this.router.navigate(['/myaccounts/admin/reports/report19']);
            break;
        }
        case 'Report-20': {
            //console.log("entered into case Report-1");
            this.router.navigate(['/myaccounts/admin/reports/report20']);
            break;
        }
        case 'Report-21': {
            //console.log("entered into case Report-1");
            this.router.navigate(['/myaccounts/admin/reports/report21']);
            break;
        }
        case 'Report-22': {
            //console.log("entered into case Report-1");
            this.router.navigate(['/myaccounts/admin/reports/report22']);
            break;
        }
        case 'Report-23': {
            //console.log("entered into case Report-1");
            this.router.navigate(['/myaccounts/admin/reports/report23']);
            break;
        }
        case 'Report-24': {
            //console.log("entered into case Report-1");
            this.router.navigate(['/myaccounts/admin/reports/report24']);
            break;
        }
        case 'Report-29': {
            //console.log("entered into case Report-1");
            this.router.navigate(['/myaccounts/admin/reports/report29']);
            break;
        }
        case 'Report-28': {
            //console.log("entered into case Report-1");
            this.router.navigate(['/myaccounts/admin/reports/report28']);
            break;
        }
        case 'Report-30': {
            //console.log("entered into case Report-1");
            this.router.navigate(['/myaccounts/admin/reports/report30']);
            break;
        }
        case 'Report-33': {
            //console.log("entered into case Report-1");
            this.Report33();
            break;
        }
   
      
       }    
    }}
}

