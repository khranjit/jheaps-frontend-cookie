
import { Component,Input} from '@angular/core';
import { ReportsService} from '../../../../services/reports.service';
import {  ProductDescriptionService} from '../../../../services/product-description.service';

import { UserService } from '../../../../services/user.service';
import { Router} from '@angular/router';
import { ToasterPopService } from '../../../../services/toasterpop.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FinanceService } from '../../../../services/finance.service';
import { tempservice } from '../../../../services/cart.service';
import { Location } from '@angular/common';


@Component({ 
   selector: 'report19',
//    styleUrls:['seller-reports.component.css'],
   templateUrl: 'report19.component.html',
   providers : [tempservice,ReportsService,UserService,ToasterPopService,FinanceService, ProductDescriptionService]
})

export class Report19Component{
    reportsModel=<any>{} ;
    public OrderList: any;
    productsShow: boolean = true;
     Products: any = [];
     reportsReady:boolean = false;
     Years = [];
     ReportsLink:any;
     searchTerm = '';
     errorMessage:any;
     reportName:any ='';
     reportType = <any>{};
     Months =[];
     reportData=[];
     getreportData:any;
     productData:any;
     awsname:any;   
     ShowUserRole : any;
     ShowUserProfile : any;  
    public TotalNetRevenue;
    public TotalGrossRevenue;
     from =0;
     index=0;
     size = 18;
     total=0;
     public Month='';
     public Year='';
     public showDate=true;
     Orders = [];
        obj_id: any;
        showReport = true;
        showselectedReport = false;
        selectedReport =<any>{};
        Product:any = [] ;
        public imageUrls:any;

    constructor(private loader:NgxSpinnerService,private location: Location,private financeService:FinanceService,private productDescriptionService: ProductDescriptionService, private toasterPopService:ToasterPopService,private reportsService : ReportsService,private tempservice:tempservice,private userService: UserService,private router: Router)
    {
       // this.resetProperties();
        window.scrollTo(0, 20);
        this.Months = [{ value: 0, name: "JAN" }, { value: 1, name: "FEB" }, { value: 2, name: "MAR" }, { value: 3, name: "APR" }, { value: 4, name: "MAY" }, { value: 5, name: "JUN" },
        { value: 6, name: "JUL" }, { value: 7, name: "AUG" }, { value: 8, name: "SEP" }, { value: 9, name: "OCT" }, { value: 10, name: "NOV" }, { value: 11, name: "DEC" }];
        this.reportsModel=this.tempservice.getData();
        this.GetReports();

        
    }
    
    selected(data) {
        this.selectedReport ={};
        this.showReport = false;
        this.showselectedReport = true;
        //this.showPackageForm = false;
        //this.selectedPrintOrders = false;
        this.selectedReport=data;
          this.productDescriptionService.getProductById(this.selectedReport.ProductId)
                        .then(
                        product => {
                            this.Product = JSON.parse(product);
                    
                            for (var i = 0; i < this.Product.Media.length; i++) 
                            {
                                this.imageUrls=this.Product.Media[i].EditedImage
                            } 
                            // console.log(this.imageUrls);
                        }
                    );
    

        // console.log(this.selectedReport);
    }
    report() {
        this.showReport = true;
        this.showselectedReport = false;
    }
  
goBack()
{
    this.location.back();
}
DownloadReport(){
    this.loader.show();
    this.reportsModel.ReportHeading="Admin-Sales Report By Product";
    this.reportsService.downloadReport(this.reportsModel).then(res=>{
        var result = JSON.parse(res);    
        if(result.ImageData){
            this.loader.hide();
            window.location.href=result.ImageData;

            // window.open(result.ImageData);
           
        }else{

        }
    });  

}
    GetReports()
    {   
       
       
        this.loader.show();
        this.reportsService.getReports(this.reportsModel).then(
            res =>{
               // console.log(res);
                this.getreportData = JSON.parse(res);
                var i;
                for(i=0;i<this.getreportData.length;i++)
                {
                    if(this.getreportData[i]!=null)
                    {
                        this.reportData.push(this.getreportData[i]);
                    }
                }
                this.loader.hide();
                if(this.reportsModel.ReportType.SelectedMonth!="" || this.reportsModel.ReportType.SelectedMonth==0 && this.reportsModel.ReportType.SelectedYear!="")
                {
                    this.showDate=false;
                    this.Year=this.reportsModel.ReportType.SelectedYear;
                

                        for (var j = 0; j < this.Months.length; j++) {
    
                            if ( this.reportsModel.ReportType.SelectedMonth == this.Months[j].value)
                            {
                                this.Month = this.Months[j].name;
                            }
                         
                        }
                      
                     
                  
                }
                var num=this.reportData.length - 1;
                this.TotalGrossRevenue = this.reportData[num].GrossRevenue;
                this.TotalNetRevenue = this.reportData[num].NetRevenue;

                this.reportData = this.reportData.slice(0,num);
                // this.getreportData = this.reportData.find(function(element) {
                //     return element!=null;
                //   });
                  
                //this.ReportsLink=this.reportData.ImageData;
                //this.download(this.ReportsLink);
                this.reportsReady = true;
                //this.reportsModel=this.tempservice.clearData();
                //this.resetProperties();
                //this.loader.hide();
            },
            error => {
                this.errorMessage = <any>error;
                this.loader.hide();
            })
        }        
    }
   
