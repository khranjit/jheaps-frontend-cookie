import { Component,Input} from '@angular/core';
import { ReportsService} from '../../../../services/reports.service';
import {  ProductDescriptionService} from '../../../../services/product-description.service';
import { UserService } from '../../../../services/user.service';
import { Router} from '@angular/router';
import { ToasterPopService } from '../../../../services/toasterpop.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FinanceService } from '../../../../services/finance.service';
import { tempservice } from '../../../../services/cart.service';
import { Location } from '@angular/common';
import { DatePipe } from '@angular/common';



@Component({ 
   selector: 'report31',
//    styleUrls:['seller-reports.component.css'],
   templateUrl: 'report31.component.html',
   providers : [tempservice,ReportsService,UserService,ToasterPopService,FinanceService, ProductDescriptionService,DatePipe]
})

export class Report31Component{
    ShowUserRole : any;
     ShowUserProfile : any;
     showShop:false;
     Results=<any>{};
     showUnsold:false;
     settlentDetail=<any>{};
     settlementList = [{ "label": "Select a Settlement" ,"value":""}];

  constructor(private loader:NgxSpinnerService,private userService: UserService,private report:ReportsService,private datePipe: DatePipe){
    this.settlentDetail.Id = '';

    this.userService.showProfile().then(
      response => {
          this.ShowUserProfile = JSON.parse(response); 
          this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
              console.log('type of user'+this.ShowUserRole)
      }
      
     );
     this.report.fetchSettlementReports().then(res=>{
        var result = JSON.parse(res);
        result.forEach(element => {
           let obj = { "label": element.SettlementId + ',' +this.datePipe.transform(element.SettlementDate, 'dd-MM-yy'), "value": element.SettlementId };
            this.settlementList.push(obj)
        });

    })

  }
  onGroupSelect(value){
    this.settlentDetail.Id = value;
  }
  DownloadSettlementReport(){
    var model = {
      SettlementId : this.settlentDetail.Id
    }
    this.loader.show();
    this.report.fetcholdsettlementReports(model).then(res=>{

      var result = JSON.parse(res); 
        if(result.ImageData){
        this.loader.hide();
        window.location.href=result.ImageData;
        }else{
        
        }
    });
  }
  DownloadReport(){

  }
  GenerateSettlementReport(){
    this.loader.show();
    // //   this.report.downloadSettlementReport(this.settlentDetail.Id).then(res=>{
    // //     var result = JSON.parse(res);    
    // //     if(result.ImageData){
    // //         this.loader.hide();
    // //         window.location.href=result.ImageData;

    // //         // window.open(result.ImageData);
           
    // //     }else{

    // //     }
    //   })
  }      
    }
   
