import { Component } from '@angular/core';
import { AdminCriteriaService } from '../../../services/admin-criteria.service';
import { HomeService } from '../../../services/home.service';
import { ProductService } from '../../../services/product.service';
import { NotifyService } from '../../../services/notify.service';
import { UtilService } from '../../../services/util.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../services/user.service';

declare var jQuery: any;

@Component({ 
    
    selector: 'three-column',
    providers: [AdminCriteriaService, ProductService, HomeService, NotifyService, UtilService,UserService],
    templateUrl: './three-column.component.html'
})

export class ThreeColumnComponent{
    public errorMessage: any;
    adminCriteriaList = [];
    threeColumnData = [];
    ShowUserRole : any;
    ShowUserProfile : any;
    firstLoader = false;
    secondLoader = false;
    thirdLoader = false;
    firstColumn = <any>{};
    secondColumn = <any>{};
    thirdColumn = <any>{};
    //columnValue: string;
        
    constructor(private adminCriteriaService: AdminCriteriaService, 
                    private homeService: HomeService,
                    private userService: UserService,private router: Router,
                    private productService: ProductService,
                    private toasterService: NotifyService,
                    private utilService: UtilService) {
                        this.userService.showProfile().then(
                            response => {
                                this.ShowUserProfile = JSON.parse(response); 
                                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                                    console.log('type of user'+this.ShowUserRole)
                                    // if(this.ShowUserRole== 'masteradmin'||this.ShowUserRole== 'admin'){
                                        this.init();
                                    // }
                                    // else{
                                    //     this.router.navigate(['']);
                                    // }
                            }
                           );
      
    }
    
    init(){
        window.scrollTo(0,20);
        this.adminCriteriaList  = [];
        this.threeColumnData = [];
        this.adminCriteriaService.getAdminCriteriaList().then(adminCriterias => {
            this.adminCriteriaList = JSON.parse(adminCriterias).Items;  
            this.homeService.getThreeColumnsData().then(threeColumnData => {
                this.threeColumnData = JSON.parse(threeColumnData).Items;
                if(this.threeColumnData.length > 0){
                this.firstColumn = this.threeColumnData[0];
                }
                if(this.threeColumnData.length > 1)
                {
                    this.secondColumn = this.threeColumnData[1];
                    
                }
                if(this.threeColumnData.length > 2)
                {
                    this.thirdColumn = this.threeColumnData[2];
                }
                //console.log("Three column Data ",this.threeColumnData);
            });    
        }, error => this.errorMessage = <any>error); 
        
        
          
    }
    
    changeImage(value, column, e): void {
        //console.log("Change Image Called")
       // this.columnValue = value;
        // let files = [].slice.call($event.target.files);
        if(value  == "first")
            this.firstLoader=true;
        else if(value  == "second")
            this.secondLoader=true;
        else
            this.thirdLoader=true;

            const width = 500;
        const height = 500;
        const fileName = e.target.files[0].name;
        const reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);
        reader.onload = e => {
            const img = new Image();
            var event:any = e;
            img.src = event.target.result;
            img.onload = () => {
                    const elem = document.createElement('canvas');
                    elem.width = width;
                    elem.height = height;
                    const ctx = elem.getContext('2d');
                    // img.width and img.height will contain the original dimensions
                    ctx.drawImage(img, 0, 0, width, height);
                    ctx.canvas.toBlob((blob) => {
                        const file = new File([blob], fileName, {
                            type: 'image/jpeg',
                            lastModified: Date.now()
                        });
                        // this.readThis(file);
                        this.readThis(value, column, file);

                    }, 'image/jpeg', 0.5);

                },
                reader.onerror = error => console.log(error);
        };
        // this.readThis(value, column, files);
    }
    
    readThis(columnName, column, inputValue: any): any {
      

        // this.loader = true;
        // var file: File = inputValue[0];
        // let fileName = file.name.split(".")[0]
        // this.utilService.convertToBase64(file);
    
          var file: File = inputValue;
          this.utilService.convertToBase64AndUploadImage(file,400,270);
          //this.utilService.convertToBase64(file);
          let count = 0;
          this.utilService.invokeEvent.subscribe(value => {
             if(count == 0){
              if(value == "error"){
                if(columnName  == "first")
                    this.firstLoader = false;
                else if(columnName  == "second")
                    this.secondLoader = false;
                else
                    this.thirdLoader = false;
                this.toasterService.ErrorSnack( "Upload images with above resolution 400 X 320!!! ")
               }
            else{
                    //    console.log("Extract Imagedata from uploadBase64AndGetUrl",JSON.parse(response).ImageData);
                column.Image = value;
                console.log(this.firstColumn);
                if(columnName  == "first")
                    this.firstLoader = false;
                else if(columnName  == "second")
                    this.secondLoader = false;
                else
                    this.thirdLoader = false;
              }
         count++; 
          }  
                       
        });
            
     
        inputValue=[];
 }
    
    updateColumnData(columnInfo){
        this.homeService.updateColumnData(columnInfo).then(response => {
            this.toasterService.SuccessSnack( "Updated Successfully");
        }, error => this.errorMessage = <any>error);
    }
    
    saveColumnData(columnInfo){
        this.homeService.saveColumnData(columnInfo).then(response => {
            this.toasterService.SuccessSnack( "Added Successfully");
        }, error => this.errorMessage = <any>error);
    }
    
    initSelection(element, callback){
        
    }
    
    triggerFileUpload(columnName){
   //     console.log("trigger");
        if(columnName  == "first")
            {
            jQuery('#FirstColumnUploadImage').trigger('click');
            jQuery('#productUploadImageFirst').trigger('click');
        }
        else if(columnName  == "second")
        {
            jQuery('#SecondColumnUploadImage').trigger('click');
            jQuery('#productUploadImageSecond').trigger('click');
        }
        else{
            jQuery('#ThreeColumnUploadImage').trigger('click');
            jQuery('#productUploadImageThird').trigger('click');
        }
      }
    
    
}