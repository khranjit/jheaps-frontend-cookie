import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ThreeColumnComponent } from './three-column.component';
import { SharedModule } from '../../../shared/shared.module';
  
  
const threecolumn: Routes = [
    {path: '', component:ThreeColumnComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(threecolumn)
  ],
  declarations: [
    ThreeColumnComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ThreeColumnModule { }