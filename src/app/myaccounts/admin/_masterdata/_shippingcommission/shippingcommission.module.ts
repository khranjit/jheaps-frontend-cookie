import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShippingCommissionComponent } from './shippingcommission.component';
import { SharedModule } from '../../../../shared/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import { MatInputModule } from '@angular/material';
  
const attribute: Routes = [
    {path: '',redirectTo:'shippingcommission',pathMatch:'full'},
    {path: 'shippingcommission', component:ShippingCommissionComponent},
]
@NgModule({
  imports: [
    SharedModule,
    NgSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    RouterModule.forChild(attribute)
  ],
  providers: [  
    MatDatepickerModule,  
  ],
  declarations: [
    ShippingCommissionComponent,
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ShippingCommissionModule { }