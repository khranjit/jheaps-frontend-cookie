import { Component, Inject, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { NotifyService } from '../../../../services/notify.service';
import { AttributeService } from '../../../../services/attribute.service';
import {DateAdapter} from '@angular/material/core';
import { ShopService } from '../../../../services/shop.service';

declare var jQuery: any;

@Component({ 
  
  selector: 'shipping',
  providers: [AttributeService,NotifyService,ShopService],
  templateUrl: "./shippingcommission.component.html"
})

export class ShippingCommissionComponent {
  public vacationModel = <any>{};
  Details = [];
  vacationDetails = <any>{};
  shippingDetails = <any>{};
  editmodel=<any>{};
  errorMessage: any;
  showlist: boolean;
  anyoneActive:boolean;
  deleteId:String;
  public vacationList: any;
  public vacation: any;
  public shipping: any;
  editbutton=false;
  constructor(
    
      private toasterService: NotifyService, private route: ActivatedRoute,
      private router: Router,private ShopService: ShopService,
       private toaster: NotifyService,private adapter: DateAdapter<any>
  )
   {
      
      this.adapter.setLocale('fr');
      window.scrollTo(0, 20);
      this.showlist = true;
      this.anyoneActive = false;
      this.getShippingDetails();
  }
  getvacationDetails() {

    this.ShopService.getvacationdetails().then(
        res => {
            this.vacationList = JSON.parse(res);
            this.vacation = this.vacationList;
            if(this.vacation.length!=0)
            {
                this.showlist = true;
        
            }
            this.anyoneActive = this.vacation.some( detail => detail.Active == true );
        },
        err => {
            this.errorMessage = <any>err;
        });
}

getShippingDetails() {

      this.ShopService.getShippingDetails().then(
          res => {
              this.vacationList = JSON.parse(res);
              this.vacation = this.vacationList;
              if(this.vacation.length!=0)
              {
                this.showlist = true;
              }
              // this.anyoneActive = this.vacation.some( detail => detail.Active == true );
          },
          err => {
              this.errorMessage = <any>err;
          });
  }

  // resetVacationDetail(){
     
  //     this.ShopService.resetVacationdetails().then(res => {
  //         this.toaster.SuccessSnack('Ended successfully')
  //         this.showlist = false;
  //         this.anyoneActive=false;
  //        this.getvacationDetails();
  //     });

  // }

  confirmDelete(id) 
  {
      this.deleteId = id;
      jQuery("#confirmDeleteModal").modal('show');
  }
  // addvacation()
  // {
  //     this.showlist=false;
  // }
  delete() {
      this.ShopService.deletevacation(this.deleteId).then(res=>{

          var result=JSON.parse(res);
          if(result.success==true)
          {
              this.toaster.SuccessSnack(result.msg);
              this.showlist=true;
              this.getvacationDetails();  
          }
          else{
              this.toaster.ErrorSnack(result.msg);
              this.getvacationDetails();  
          }    
      },
      error => this.errorMessage = <any>error);
  }
  goBack()
{
 
  this.showlist = true;
}
  
edit(vacationObj)
{ 

this.editmodel={};
this.showlist=false;
this.editbutton=true;

this.shippingDetails.StartDate=new Date(vacationObj.startDate);
this.shippingDetails.EndDate=new Date(vacationObj.endDate);
this.shippingDetails.Amount=vacationObj.amount;


this.editmodel=vacationObj;


console.log(vacationObj);
}
updateShippingDetail(){
    var today = new Date();
    this.editmodel.startDate= this.shippingDetails.StartDate;
    this.editmodel.endDate= this.shippingDetails.EndDate;
    this.editmodel.amount =  this.shippingDetails.Amount;
    if(( this.editmodel.startDate.toLocaleDateString()< today.toLocaleDateString()))
    {
     this.toaster.ErrorSnack('Cannot add  for expired date');
    }
    else if( this.editmodel.startDate.toLocaleDateString() >  this.editmodel.endDate.toLocaleDateString())
    {
      this.toaster.ErrorSnack('Start date should be greater than End date');
    }
    else{
    this.ShopService.updateShippingdetails(this.editmodel).then(res => {
        var result=JSON.parse(res);
        if(result.msg=="Created sucessfully")
        {
            this.toaster.SuccessSnack("Updated sucessfully");
            this.showlist=true;
            this.shippingDetails = <any>{};
            this.getShippingDetails();  
        }
        else{
            this.toaster.ErrorSnack(result.msg);
            this.getShippingDetails();  
        }
      
        
    });
    }
}
  AddShippingAmount() {
      var today = new Date();
      var shippingModel = {
          startDate: this.shippingDetails.StartDate,
          endDate: this.shippingDetails.EndDate,
          amount:this.shippingDetails.Amount
      }
      if((shippingModel.startDate.toLocaleDateString() < today.toLocaleDateString()))
      {
       this.toaster.ErrorSnack('Cannot add  for expired date');
      }
      else if(shippingModel.startDate.toLocaleDateString() > shippingModel.endDate.toLocaleDateString())
      {
        this.toaster.ErrorSnack('Start date should be greater than End date');
      }
      else{
        this.ShopService.AddShippingDetails(shippingModel).then(res => {
            var result=JSON.parse(res);
            if(result.msg=="Created sucessfully")
            {
                this.toaster.SuccessSnack(result.msg);
                this.showlist=true;
                this.getShippingDetails();  
            }
            else{
                this.toaster.ErrorSnack(result.msg);
                this.getShippingDetails();  
            }
          });
      }
    

      // this.vacationDetails.vacationTo.setHours(23,59,59);
  
  }
  addNew()
  {
      this.vacationDetails={};
      this.showlist=false;
      this.editbutton=false;

      
  }
}
