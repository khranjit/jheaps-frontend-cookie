import { Component,ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Router} from '@angular/router';
@Component({ 
  
  selector: 'admin-masterdata',
  providers:[UserService],
  templateUrl: './masterdata.component.html'
})
export class MasterDataComponent{
  ShowUserRole : any;
     ShowUserProfile : any;
  constructor(private userService: UserService,private router: Router){
    this.userService.showProfile().then(
      response => {
          this.ShowUserProfile = JSON.parse(response); 
          this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
              console.log('type of user'+this.ShowUserRole)
              // if(this.ShowUserRole== 'masteradmin'){
                 
              // }
              // else{
              //     this.router.navigate(['']);
              // }
      }
     );

  }
}