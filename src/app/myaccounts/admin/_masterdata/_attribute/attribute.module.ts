import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AttributeComponent } from './attribute.component';
import { AddAttributeComponent } from './_add-attribute/add-attribute.component';
import { SharedModule } from '../../../../shared/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';

  
const attribute: Routes = [
    {path: '',redirectTo:'view',pathMatch:'full'},
    {path: 'view', component:AttributeComponent},
    {path: 'add', component:AddAttributeComponent},
]
@NgModule({
  imports: [
    SharedModule,
    NgSelectModule,
    RouterModule.forChild(attribute)
  ],
  declarations: [
    AttributeComponent,
    AddAttributeComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class AttributeModule { }