import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MasterDataComponent } from './masterdata.component';
import { SharedModule } from '../../../shared/shared.module';

  
  
const masterdata: Routes = [
    {path: '',redirectTo:'list',pathMatch:'full'},
    {path:'list',component:MasterDataComponent,
      children:
        [
          {path: '',redirectTo:'attribute',pathMatch:'full'},
          {path: 'attribute', loadChildren:'./_attribute/attribute.module#AttributeModule'},
          {path: 'category', loadChildren:'./_category/category.module#CategoryModule'},
          {path: 'cod-limit', loadChildren:'./_cod-limit/cod-limit.module#CodLimitModule'},
          {path: 'userattribute', loadChildren:'./_userattribute/userattribute.module#UserAttributeModule'},
          {path: 'shippingcommission', loadChildren:'./_shippingcommission/shippingcommission.module#ShippingCommissionModule'},
        ]
    }
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(masterdata)
  ],
  declarations: [
    MasterDataComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class MasterDataModule { }