import { Component, ChangeDetectionStrategy, ViewEncapsulation, Inject } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { CategoryService } from '../../../../services/category.service';
import { DomainService } from '../../../../services/domain.service';

declare var jQuery: any;

@Component({
    
    selector: 'category',
    providers: [CategoryService, DomainService],
    templateUrl: './category.component.html'
})
export class CategoryComponent {
    public Categories = [];
    public CategoriesList: any;
    public errorMessage: string;
    public deletableContent: any;
    domainModel:any;
    public pages: any[];
    totalPages = 0;
    currentPage = 1;
    from = 0;
    size = 10;
    total = 0;
    deleteModel =false;
    deleteModelBody = 0;

    constructor(private route: ActivatedRoute, private router: Router, 
    private categoryService: CategoryService,private domainService : DomainService) {
        this.Init();
    }


    Init() {
        window.scrollTo(0,20);
        this.currentPage = 1;
        this.pages = [];
         this.domainModel = { "Domain": this.domainService.getDomain() };
         this.getCategoryData(this.domainModel, this.from, this.size);           
    }
    edit(Id) {
        let navimodel: NavigationExtras = {
            queryParams: { 'Id': Id }
        }
        this.router.navigate(['/myaccounts/admin/master-data/list/category/add'], navimodel);
      //  console.log(Id);
    }
    confirmDelete(category) {
        this.deletableContent = category;
    this.deleteModel = false;
    this.deleteModelBody = 0;
    this.categoryService.checkCategory(category).then(res=> {
      var count = JSON.parse(res).count;
      if(count == 0)
      {
        this.deleteModel = false;
        this.deleteModelBody = 0;
      }
      else
      {
        this.deleteModel = true;
        this.deleteModelBody = count;
        
      }

      jQuery("#confirmDeleteModal").modal('show');
    }, 
      error=> this.errorMessage = <any>error);
    }

    Delete() {
        this.categoryService.deleteCategory(this.deletableContent)
            .then(
            deleted => {
                this.Init();
            },
            error => this.errorMessage = <any>error);
         jQuery("#confirmDeleteModal").modal('hide');
    }

   

    addNew() {
        this.router.navigate(['/myaccounts/admin/master-data/list/category/add']);
    }

    getCategoryData(domain, from, size)
    {
        this.categoryService.getAllListWithoutPagination(domain, from, size)
                        .then(
                        categories => {
  //              console.log("Categories", categories);
                this.CategoriesList = JSON.parse(categories);

                this.Categories = this.CategoriesList.Items;
                /*for (var i = 1; i <= this.CategoriesList.TotalPages; i++) {
                    this.pages.push(i);
                }*/
    //            console.log(this.CategoriesList)
                 this.pages = [] 
                 this.totalPages = Math.ceil(this.CategoriesList.TotalItems /this.size);

                for(let i = 1; i <= this.totalPages; i++){
                this.pages.push(i);
                 }

            },
            error => this.errorMessage = <any>error);
    }
    previousPage(){
        if(this.currentPage > 1){
            this.getCategoryData(this.domainModel, ((this.currentPage - 1)*this.size), this.size *this.currentPage); 
            this.currentPage -= 1;  
        }
    }
    
    nextPage(){
        if(this.currentPage < this.totalPages){
            this.getCategoryData(this.domainModel, ((this.currentPage )*this.size), this.size *(this.currentPage +1));
            this.currentPage += 1;  
        }
    }
    
    selectPage(PageNumber) {
        this.currentPage = PageNumber;
        this.getCategoryData(this.domainModel, ((this.currentPage - 1)*this.size), this.size * (this.currentPage));
        
    }
}