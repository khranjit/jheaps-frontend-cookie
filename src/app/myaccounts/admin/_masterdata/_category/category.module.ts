import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoryComponent } from './category.component';
import { AddCategoryComponent } from './_add-category/add-category.component';
import { SharedModule } from '../../../../shared/shared.module';
import { Select2Module } from '../../../../../assets/ng2-select2';
import { NgSelectModule } from '@ng-select/ng-select';

  
  
const category: Routes = [
    {path: '',redirectTo:'view',pathMatch:'full'},
    {path: 'view', component:CategoryComponent},
    {path: 'add', component:AddCategoryComponent},
]
@NgModule({
  imports: [
    SharedModule,
    Select2Module,
    RouterModule.forChild(category),
    NgSelectModule
  ],
  declarations: [
    CategoryComponent,
    AddCategoryComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class CategoryModule { }