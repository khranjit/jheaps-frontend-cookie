import { Component, Inject, ChangeDetectionStrategy, ViewEncapsulation , ViewChild} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from '../../../../../services/category.service';
import { AttributeService } from '../../../../../services/attribute.service';
import { NotifyService } from '../../../../../services/notify.service';
import { DomainService } from '../../../../../services/domain.service';
// import {SelectItem} from 'primeng/api';

declare var jQuery: any;

@Component({
    
    selector: 'addcategory',
    providers: [CategoryService, AttributeService, NotifyService, DomainService],
    templateUrl: './add-category.component.html'
})

export class AddCategoryComponent {
    public categoryModel: ViewModels.ICategoryViewModel;
    public AttributesList = [];
    public attributesList = [];
    public newAttributesList = [];
    public CategoriesList = [];
    public Categories: any;
    public errorMessage: any;
    public characters: any;
    isAttributesReady = false;
    newbutton = true;
    editbutton = false;
    selectedAttribute=[];
    totalAttributesList=[];
    getParamId="";
    disableName = false;
    categoryCount:any;
    //hide =  false;
    //show=true;
    public items: Array<any> = [{ 'id': 1, 'text': 'India' }, { 'id': 2, 'text': 'USA' }];
    @ViewChild('addCategory') addCategoryForm: HTMLFormElement;
    constructor(private route: ActivatedRoute, private router: Router, private toasterService: NotifyService,
        private categoryService: CategoryService, private attributeService: AttributeService, private domainService: DomainService) {
        this.Init();
    }

    addAttr(attr){
        return false;
    }
    ModelChange(event)
    {
        this.isAttributesReady = false;
        if(event == null ||  (this.totalAttributesList.length - event.length == 1))
            {

                
                var deletedValue = (event == null)? this.totalAttributesList[0].name : "undefined";
                //this.isAttributesReady = false;
    
               
                if(deletedValue == "undefined")
                {
                    deletedValue= this.totalAttributesList.filter(x=> {return !event.includes(x)})[0].name;
                    var index = this.totalAttributesList.findIndex(x=> x.name == deletedValue);
                    //deletedValue = this.categoryModel.Attributes[index];
                    this.totalAttributesList.splice(index,1);
                    
                   
                }
    
                if(deletedValue != "undefined")
                {
                    //deletedName = this.attributesList.find(x=>x.id == deletedValue).text;
                    this.categoryService.checkCategoryAttribute(this.categoryModel, deletedValue).then(res=> {
                        var count = JSON.parse(res).count;
                        if(count != 0)
                        {
    
                            this.toasterService.ErrorSnack("Cannot change attributes, since the category is linked with "+count+" products");
                           // this.categoryModel.Attributes.push(deletedValue);
                            this.selectedAttribute.push({code:deletedValue, name:deletedValue})
                            this.totalAttributesList.push({code:deletedValue, name:deletedValue})
                            
                            
                        }
                        
                        
                    },
                    error=> this.errorMessage = <any>error
                );
                    
                }
    
               
            }

        else if(this.totalAttributesList.length - event.length > 1)
        {
             var newTotalAttributelist = [];
             this.totalAttributesList.forEach(ele =>{
                
                this.categoryService.checkCategoryAttribute(this.categoryModel, ele.name).then(res=> {
                    var data = JSON.parse(res);
                    if(data.count != 0)
                    {

                        this.toasterService.ErrorSnack("Cannot change attributes, since the category is linked with "+data.count+" products");
                       // this.categoryModel.Attributes.push(deletedValue);
                       newTotalAttributelist.push({code:data.name, name:data.name})
                        
                        
                        
                    }
                    
                    
                },
                error=> this.errorMessage = <any>error
            );

           
             })

             this.selectedAttribute = newTotalAttributelist;
             this.totalAttributesList = newTotalAttributelist;
        }

            else if(this.totalAttributesList.length < event.length){
                this.totalAttributesList = this.selectedAttribute;
            }
            
            //console.log("Value Change", value);
            console.log(this.selectedAttribute);
        //console.log(event)

        this.isAttributesReady = true;
       
    }
    public Init() {
        window.scrollTo(0,20);
        this.isAttributesReady = false;
        this.categoryModel = <ViewModels.ICategoryViewModel>{};
        this.categoryModel.Attributes = [];
        this.selectedAttribute = [];
        this.categoryModel.ParentCategory = "";

        
        // this.characters = [
        //     { value: '0', label: 'Aech' },
        //     { value: '1', label: 'Art3mis' },
        //     { value: '2', label: 'Daito' },
        //     { value: '3', label: 'Parzival' },
        //     { value: '4', label: 'Shoto' }
        // ];
        this.attributeService.getAll().then(
            attributes => {
                var AttributeArray = [];
                var Attributes = [];
                var data = JSON.parse(attributes);
                for (var i = 0; i < data.Items.length; i++) {

                     AttributeArray[i] = { "code": data.Items[i]._id, "name": data.Items[i].Name };
                     this.AttributesList[i]= { "code": data.Items[i]._id, "name": data.Items[i].Name };
                     Attributes[i] = { "id": data.Items[i]._id, "name": data.Items[i].Name };
                   // AttributeArray.push(this.AttributesList.Items[i].Name);
                }

                this.attributesList = AttributeArray;
                this.newAttributesList = Attributes;
                let getCategoryList = { "Domain": this.domainService.getDomain() };
                this.categoryService.getCategoryList(getCategoryList)
                .then(
                categories => {
                    this.CategoriesList = JSON.parse(categories);
                  
                    console.log(this.CategoriesList);
                    var index = this.CategoriesList.findIndex(x => x.Parent._id == this.getParamId);
                    if(index != -1)
                    {
                        this.CategoriesList.splice(index,1);
                    }
                },
                error => this.errorMessage = <any>error);
                this.route.queryParams
                .subscribe(params => {
                    if (params['Id']) {
                        this.getParamId = params['Id'];
                        //this.categoryModel = this.getParamId;
                        this.editbutton = true;
                        this.newbutton = false;
                        let domainModel = { "Domain": this.domainService.getDomain() };
                        
                       
                        this.categoryService.getAllCategories(domainModel)
                            .then(
                            categories => {
                                let CategoryList = [];
                                CategoryList = JSON.parse(categories).Items;
                                 console.log("LLIST:", CategoryList);
                                CategoryList.forEach((List: any) => {
              //                      console.log("a");
                                    if (this.getParamId == List._id) {

                                        this.categoryModel = List;
                                        //this.oldNameCategory = this.categoryModel.Name;

                //                        console.log(List);
                                        //this.selectedAttribute=List.Attributes;
                                       // console.log(this.selectedAttribute);
                                        if(this.editbutton){
                   this.attributesList.forEach((attr: any) => {
                    List.Attributes.forEach((selected: any)=> {
                      if(selected==attr.code){
                        //   selected.code = List.id;
                        //   selected.name = List.name;
                        //   List['id']=selected._id;
                        //   List["text"]=selected.Name;
                          //List["selected"]=true;
                          //attr.code = attr.name;
                          this.selectedAttribute.push({code:attr.name, name:attr.name});
                      }
                   });
                       attr.code = attr.name;
                   });
                    
                }
               console.log("Attributes :", this.attributesList);
               this.totalAttributesList = this.selectedAttribute;
                                        this.isAttributesReady = true;
                                        //jQuery("#Attributes").val(Array);
                  //                       console.log("this.selectedAttribute",this.selectedAttribute)
                    //                    console.log("this.categoryModel",this.categoryModel)
                                    }
                                });


                                this.categoryService.checkCategory(this.categoryModel).then(res=> {
                                    var count = JSON.parse(res).count;
                                    if(count == 0)
                                    {
                                      this.disableName = false;
                                      this.categoryCount = 0;
                                    }
                                    else
                                    {
                                      this.disableName = true;
                                      this.categoryCount = count;
                                      
                                    }
                              
                                    //jQuery("#confirmDeleteModal").modal('show');
                                  }, 
                                    error=> this.errorMessage = <any>error);
                            
                            },
                            error => this.errorMessage = <any>error);
                      //  console.log("this.categoryModel", this.categoryModel);
                    }

                    else{
                        //this.isAttributesReady = true;
                        let getCategoryList = { "Domain": this.domainService.getDomain() };
                        this.categoryService.getCategoryList(getCategoryList)
                            .then(
                            categories => {
                                this.CategoriesList = JSON.parse(categories);
                                
                            },
                            error => this.errorMessage = <any>error);
                    }
                });


                
            },
            error => this.errorMessage = <any>error);
       

    }

    getSelectedAttributes()
    {
        var list = [];
        this.selectedAttribute.forEach(x=>list.push(this.AttributesList.find(y=> y.name == x.name).code));
        return list;    
    }
    newCategory() {
        //console.log("Added", this.categoryModel);
        //this.categoryModel.Attributes = this.getSelectedAttributes();
        this.categoryModel["Domain"] = this.domainService.getDomain();
        //console.log("CateG", this.categoryModel);
        
        this.categoryService.setCategory(this.categoryModel).then(
            res => {
                
                    this.toasterService.SuccessSnack( "Category Added Successfully");
                    this.router.navigate(['/myaccounts/admin/master-data/list/category']);
                    // this.attributeService.getAll().then(
                    //     categories => {
                    //         this.CategoriesList = JSON.parse(categories);
                    //         this.Categories = this.CategoriesList.Items;
                            
                    //     },
                    //     error => this.errorMessage = <any>error);
                
            },
            error => {
                this.toasterService.ErrorSnack( "Category already exists");
                this.errorMessage = <any>error
                this.router.navigate(['/myaccounts/admin/master-data/list/category']);
            });
    }

    editCategory() {
     //   console.log("Edit Category", this.categoryModel);
        this.categoryModel.Attributes = this.getSelectedAttributes();
        this.categoryService.updateCategory(this.categoryModel,this.getParamId).then(res =>{
            this.toasterService.SuccessSnack( "Category Updated Successfully");
            this.router.navigate(['/myaccounts/admin/master-data/list/category']);
        })
       
    }

    goBack() {
        this.router.navigate(['/myaccounts/admin/master-data/list/category']);
    }

    public attributeValueChanged(value: any): void {
        var newarr = [];
        value.forEach(element => {
            newarr.push(element.id);
        });
        console.log(value);
        if(value.length !=0)
        this.categoryModel.Attributes = newarr;
        else
        this.categoryModel.Attributes = [];
    }

    parentCategoryChanged(event){

        if(this.editbutton)
        {
            var oldValue = this.categoryModel.ParentCategory;
            var oldModel = this.categoryModel;
            
            
            this.categoryService.checkCategory(oldModel).then(res=> {
                var count = JSON.parse(res).count;
                if(count != 0)
                {
    
                    this.toasterService.ErrorSnack("Cannot change parent category, since the category is linked with "+count+" products");
                    this.categoryModel.ParentCategory = oldValue;
                    return;
                }
                
            },
            error=> this.errorMessage = <any>error
        );
        this.categoryModel.ParentCategory = event;
        }

        else{

            this.categoryModel.ParentCategory = event;
        }
       

    }

    checkCategoryName()
    {

        
            this.categoryService.checkCategoryName(this.categoryModel).then(res =>{
            
                var newError = <any>{};
                var oldError = this.addCategoryForm.form.controls['Name'].errors;
                //console.log(obj);
                if(JSON.parse(res).found)
                {
                    for (var key in oldError) {
                        console.log(key);
                        console.log(oldError[key]);
                        newError[key] = oldError[key]
                    }
                    newError.incorrect = true;
                    console.log(newError);
                    this.addCategoryForm.form.controls['Name'].setErrors(newError);
                }
                
                else
                {
                    if(oldError.incorrect)
                    oldError.incorrect = false;
                    this.addCategoryForm.form.controls['Name'].setErrors(oldError);
                }
               
            }, 
            error=>this.errorMessage = <any>error);
        }
        
    
}