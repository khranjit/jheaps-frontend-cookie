import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CODLimitComponent } from './cod-limit.component';
import { SharedModule } from '../../../../shared/shared.module';
  
  
const codlimit: Routes = [
    {path: '', component:CODLimitComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(codlimit)
  ],
  declarations: [
    CODLimitComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class CodLimitModule { }