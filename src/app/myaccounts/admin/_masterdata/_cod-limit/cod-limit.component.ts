import { Component } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { NotifyService } from '../../../../services/notify.service';
import { CODLimitService} from "../../../../services/cod-limit.service";

declare var jQuery: any;

@Component({

  selector: 'codlimit',
  providers: [NotifyService, CODLimitService],
  templateUrl: "./cod-limit.component.html"
})

export class CODLimitComponent {

  codLimitModel =<any>{};
  errorMessage : any;

  constructor(private codLimitService:CODLimitService, private route: ActivatedRoute, private router: Router, private toasterPopService: NotifyService) {
    this.codLimitModel.codLimitValue = '';
    this.codLimitModel.mincodLimitValue = '';
    this.Init();
  }


  public Init() {
    window.scrollTo(0, 20);
    
    this.codLimitService.getCODLimit().then(res =>{
      console.log(JSON.parse(res));
      var response = JSON.parse(res);
      if(response.hasOwnProperty('message') && response.message == "No Entry found"){
        this.codLimitModel.codLimitValue = '';
        this.codLimitModel.mincodLimitValue = '';
      }
      else
      this.codLimitModel = response;
    }, err=>{
      this.errorMessage = <any>err;
    })
  }

  public Save()
  {
    if(this.codLimitModel.hasOwnProperty('_id'))
    {
      this.codLimitService.updateCODLimit(this.codLimitModel).then(success=>{
        this.toasterPopService.SuccessSnack( "Updated successfully");

      },err=>{
        this.errorMessage = <any>err;
        this.toasterPopService.ErrorSnack( "Unable to Update");
        this.Init();
      })
    }
    else
    {
      this.codLimitService.saveCODLimit(this.codLimitModel).then(success=>{
        this.toasterPopService.SuccessSnack( "Saved successfully");

      },err=>{
        this.errorMessage = <any>err;
        this.toasterPopService.ErrorSnack( "Unable to Save");
        this.Init();
      })
    }
  }

}
