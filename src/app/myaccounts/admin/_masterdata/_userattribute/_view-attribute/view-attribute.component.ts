import { Component, Inject, ChangeDetectionStrategy, ViewEncapsulation, ViewChild } from '@angular/core';
import { AttributeService } from '../../../../../services/attribute.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyService } from '../../../../../services/notify.service';
import { json } from 'body-parser';
//declare var jQuery: any;

@Component({
    
    selector: 'addattribute',
    providers: [AttributeService, NotifyService],
    templateUrl: "./view-attribute.component.html"
})

export class ViewAttributeComponent {

    public attributeModel: ViewModels.IAttributeViewModel;
   // public attributeEditModel: ViewModels.IAttributeViewModel;
    public AttributesList: any;
    public Attributes: any;
    //isSystemDefined: boolean = true;
    newbutton = true;
    editbutton = false;
    alertShow = false;
    errorMessage: any;
    disableName = false;
    newArr =[];
    Attributeedit = [];

    @ViewChild('addAttribute') addAttributeForm: HTMLFormElement;
    constructor(private route: ActivatedRoute, private router: Router,
        private attributeService: AttributeService, private toasterPopService: NotifyService) {
        this.Init();
    }
//     addElement(item)
//     {
// console.log(this.attributeModel.Values);
// console.log(item);
//     }

    removeElement(item)
    {
console.log(this.attributeModel.Values);
console.log(item);

     if(this.editbutton)
     {
        this.attributeService.checkAttributeValue(this.attributeModel,item).then(res=>{
            var count = JSON.parse(res).count;
            if(count != 0)
        {
          this.toasterPopService.ErrorSnack( "Cannot remove the value since it is linked with "+count+" products.");
          this.attributeModel.Values.push(item);
        }
       
        },error=>this.errorMessage = <any>error);
     }
    }

    addTag(name) {
        return name;
    }  

    checkAttributeName()
    {
        this.attributeService.checkAttributeName(this.attributeModel).then(res =>{
           if(JSON.parse(res).found)
           {
            var newError = <any>{};
            var oldError = this.addAttributeForm.form.controls['Name'].errors;
            //console.log(obj);
            if(JSON.parse(res).found)
            {
                for (var key in oldError) {
                    console.log(key);
                    console.log(oldError[key]);
                    newError[key] = oldError[key]
                }
                newError.incorrect = true;
                console.log(newError);
                this.addAttributeForm.form.controls['Name'].setErrors(newError);
            }
            
            else
            {
                if(oldError.incorrect)
                oldError.incorrect = false;
                this.addAttributeForm.form.controls['Name'].setErrors(oldError);
            }
           }
        }, 
        error=>this.errorMessage = <any>error);
    }

    public Init() {
        window.scrollTo(0,20);
        this.attributeModel = <ViewModels.IAttributeViewModel>{};
        this.attributeModel.Values = [];
        console.log(this.attributeModel)
        this.attributeModel.IsSystemDefined = true;
        /*setTimeout(function() {
            jQuery("#Values").select2({
                tags: true,
                tokenSeparators: [',', ' ']
            })
        })*/
        // if(this.attributeModel.IsSystemDefined==false){
        //   this.systemdefined=false;
        // }
        //this.attributeModel= {IsSystemDefined: true};
        //console.log(this.attributeModel)

        this.route
            .queryParams
            .subscribe(params => {
                if (params['Attribute']) {
                    this.attributeModel = JSON.parse(params['Attribute']);
                    this.attributeModel.Values.forEach(element =>{
                        this.newArr.push(element.Value);
                    })
                    // this.attributeModel.Values = newArr;
                    // this.attributeModel = this.attributeEditModel;
                    // this.AttriValue=this.attributeEditModel.Values;
                    // console.log("attributeEditModel",this.attributeModel,"AttriValue", this.AttriValue);
                   
                    this.editbutton = true;
                    this.newbutton = false;

                    this.attributeService.checkAttribute(this.attributeModel).then(res=> {
                        var res = JSON.parse(res);
                        if(res.productCount == 0 && res.categoryCount == 0)
                        {
                          this.disableName = false;
                          // this.deleteModelBody = 0;
                        }
                        else
                        {
                          this.disableName = true;
                          // this.deleteModelBody = count;
                          
                        }
                  
                        //jQuery("#confirmDeleteModal").modal('show');
                      }, 
                        error=> this.errorMessage = <any>error);
                }
            });
    }

    /*onTypeChange(value) {
        this.isSystemDefined = value;
        console.log(this.isSystemDefined);
    }*/

    newAttribute() {
        // var temp;
        // var str = this.AttriValue;
        // temp = str.split(",");
        // temp.map(function(s) { return s.trim() });
        // this.attributeModel.Values = temp;
        console.log(this.attributeModel.Values)
        this.attributeService.setAttribute(this.attributeModel).then
            (Status => {
                this.toasterPopService.SuccessSnack( 'Attribute created Successfully!!');
                this.router.navigate(['/myaccounts/admin/master-data/list/userattribute']);
                this.attributeService.getAll()
                    .then(
                    attributes => {
                        this.AttributesList = JSON.parse(attributes);
                        this.Attributes = this.AttributesList.Items;
                    },
                    error => this.errorMessage = <any>error);
            },
            error => {
                this.errorMessage = <any>error;
                this.toasterPopService.ErrorSnack('Attribute already exists');
                console.log("error ", this.errorMessage)
            });
    }

    editAttribute() {
    //    console.log(this.attributeModel.Values)
       this.attributeService.putAttributeValues(this.attributeModel).then(
        Status => {

            this.router.navigate(['/myaccounts/admin/master-data/list/userattribute']);
            this.toasterPopService.SuccessSnack( 'Attribute Values Successfully!!');
        },
        error => {
            this.errorMessage = <any>error;
            this.toasterPopService.ErrorSnack('Attribute already exists');
            console.log("error ", this.errorMessage)
        });
    }

    goBack() {
        this.router.navigate(['/myaccounts/admin/master-data/list/userattribute']);
    }
}