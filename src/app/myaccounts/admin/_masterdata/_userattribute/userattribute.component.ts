import { Component, Inject, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { NotifyService } from '../../../../services/notify.service';
import { AttributeService } from '../../../../services/attribute.service';

declare var jQuery: any;

@Component({ 
  
  selector: 'attribute',
  providers: [AttributeService,NotifyService],
  templateUrl: "./userattribute.component.html"
})

export class UserAttributeComponent {
  public Attributes = [];
  public AttributesList:any;
  public errorMessage: any;
  public deletableContent: any;
  deleteModel:any;
  deleteModelBody:any;
  public pages: any[];
    totalPages = 0;
    currentPage = 1;
    from = 0;
    size = 10;
    total = 0;


  constructor(private route: ActivatedRoute, private router: Router, private toasterPopService:NotifyService,
      private attributeService: AttributeService) {
       
      this.Init();
  }
  public Init() {
    window.scrollTo(0,20);
    this.currentPage = 1;
    this.pages = [];
    this.getAttributeData(this.from, this.size);
  }
	edit(attribute){
    let navimodel: NavigationExtras = {
      queryParams: { 'Attribute': JSON.stringify(attribute) }
    }
    this.router.navigate(['/myaccounts/admin/master-data/list/userattribute/view'], navimodel);
    //console.log(attribute)
  }

  confirmDelete(attribute){
    this.deletableContent = attribute;
    this.deleteModel = false;
    this.deleteModelBody = 0;
    this.attributeService.checkAttribute(attribute).then(res=> {
      var res = JSON.parse(res);
      if(res.productCount == 0 && res.categoryCount == 0)
      {
        this.deleteModel = false;
        // this.deleteModelBody = 0;
      }
      else
      {
        this.deleteModel = true;
        // this.deleteModelBody = count;
        
      }

      jQuery("#confirmDeleteModal").modal('show');
    }, 
      error=> this.errorMessage = <any>error);
    //jQuery("#confirmDeleteModal").modal('show');
  }

  Delete(){
    this.attributeService.deleteAttribute(this.deletableContent);
    this.toasterPopService.SuccessSnack('Attribute deleted Successfully!!');
    this.Init();
       jQuery("#confirmDeleteModal").modal('hide');
  }
  //   addNew() {
  //   this.router.navigate(['/myaccounts/admin/master-data/list/attribute/add']);
  // }
  getAttributeData(from, size)
  {
   this.attributeService.getUserDefined(from, size)
   .then(
   attributes => {
     this.AttributesList = JSON.parse(attributes);
     this.Attributes=this.AttributesList.Items;
     this.pages = [] 
              this.totalPages = Math.ceil(this.AttributesList.TotalItems /this.size);

             for(let i = 1; i <= this.totalPages; i++){
             this.pages.push(i);
              }
     //console.log("Attributes",this.AttributesList)
   },
   error => this.errorMessage = <any>error);
  } 

     previousPage(){
      if(this.currentPage > 1){
          this.getAttributeData( ((this.currentPage - 1)*this.size), this.size *this.currentPage); 
          this.currentPage -= 1;  
      }
  }
  
  nextPage(){
      if(this.currentPage < this.totalPages){
          this.getAttributeData( ((this.currentPage )*this.size), this.size *(this.currentPage +1));
          this.currentPage += 1;  
      }
  }
  
  selectPage(PageNumber) {
      this.currentPage = PageNumber;
      this.getAttributeData( ((this.currentPage - 1)*this.size), this.size * (this.currentPage));
      
  }
}
