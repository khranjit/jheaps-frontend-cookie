import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserAttributeComponent } from './userattribute.component';
import { ViewAttributeComponent } from './_view-attribute/view-attribute.component';
import { SharedModule } from '../../../../shared/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';

  
const attribute: Routes = [
    {path: '',redirectTo:'userview',pathMatch:'full'},
    {path: 'userview', component:UserAttributeComponent},
    {path: 'view', component:ViewAttributeComponent},
]
@NgModule({
  imports: [
    SharedModule,
    NgSelectModule,
    RouterModule.forChild(attribute)
  ],
  declarations: [
    UserAttributeComponent,
    ViewAttributeComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class UserAttributeModule { }