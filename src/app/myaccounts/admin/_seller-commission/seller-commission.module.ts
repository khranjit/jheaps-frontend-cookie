import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SellerCommissionComponent } from './seller-commission.component';
import { SharedModule } from '../../../shared/shared.module';
import {CalendarModule} from 'primeng/primeng';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';

import { MatInputModule } from '@angular/material';
  
  
const sellercommission: Routes = [
    {path: '', component:SellerCommissionComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(sellercommission),
    CalendarModule,
    MatNativeDateModule,
    MatInputModule,
    MatDatepickerModule
  ],
  declarations: [
    SellerCommissionComponent
  ],
  providers: [  
    MatDatepickerModule,  
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class SellerCommissionModule { }