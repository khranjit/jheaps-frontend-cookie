import { Component } from '@angular/core';
import { NotifyService } from '../../../services/notify.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { SellerCommissionService } from '../../../services/seller-commission.service';
import { UserService } from '../../../services/user.service';
//import  * as _ from 'lodash';

declare var jQuery: any;

@Component({
    providers: [SellerCommissionService, NotifyService,UserService],
    selector: 'seller-commission',
    templateUrl: './seller-commission.component.html'
})
export class SellerCommissionComponent {
    errorMessage: any;
    public DefaultCommissionModel: ViewModels.IDefaultCommissionViewModel;
    public CommissionModel: ViewModels.ICommissionViewModel;
    public SpecificCommissionModel: ViewModels.ISpecificCommissionViewModel;
    public editSpecificCommissionModel: ViewModels.ISpecificCommissionViewModel;
    public getAllSellerList: any;
    public getSellerList: any;
    userDetailsList: any;
    userDetails: any;
    userName = "";
    getUserView: boolean;
    ShopDetails: any;
    ShopPlan = [];
    ViewShopDetails: boolean;
    UpdateCommission: any;
    DeleteObjid: any;
    DeletePlanid: any;
    EditObjId: any;
    ShowUserRole : any;
    ShowUserProfile : any;
    EditPlanId: any;
    ReArrangePlanid = [];
    public pages: any[];
    totalPages = 0;
    currentPage = 1;
    from = 0;
    size = 10;
    total = 0;
    deletePlanIndex: any;
    getDefaultCommission: ViewModels.IDefaultCommissionViewModel;

    constructor(private userService: UserService,private toasterPopService: NotifyService, private sellerCommissionService: SellerCommissionService, private router: Router) {
        this.DefaultCommissionModel = <ViewModels.IDefaultCommissionViewModel>{};
        this.CommissionModel = <ViewModels.ICommissionViewModel>{};
        this.SpecificCommissionModel = <ViewModels.ISpecificCommissionViewModel>{};
        this.editSpecificCommissionModel = <ViewModels.ISpecificCommissionViewModel>{};
        this.getSellerList = [];
        this.getUserView = false;
        this.ShopDetails = {};
        this.ShopPlan = [];
        this.ViewShopDetails = false;
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response); 
                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                    console.log('type of user'+this.ShowUserRole)
                    // if(this.ShowUserRole== 'masteradmin'||this.ShowUserRole== 'admin'){
                        
                        this.init();
                    // }
                    // else{
                    //     this.router.navigate(['']);
                    // }
            }
           );
        
    }
    init() {
        window.scrollTo(0, 20);
        this.GetAllSellerList(this.currentPage - 1, this.size, this.userName);
        this.sellerCommissionService.getDefaultCommission().then(
            response => {
                // console.log("response default commission", response);
                this.getDefaultCommission = JSON.parse(response).Items[0];
                if(!this.getDefaultCommission)
                this.DefaultCommissionModel = <ViewModels.IDefaultCommissionViewModel>{};
                else
                this.DefaultCommissionModel = this.getDefaultCommission;
            },
            error => this.errorMessage = <any>error);
    }
    setCommissionAll() {
        //  console.log(this.DefaultCommissionModel)
        this.sellerCommissionService.setAll(this.DefaultCommissionModel).then(
            response => {
                //        console.log("response", response);
                this.toasterPopService.SuccessSnack( 'Updated Successfully');
                this.init();
            },
            error => this.errorMessage = <any>error);
    }

    // setGSTAll() {
    //     console.log(this.DefaultCommissionModel)
    //     this.sellerCommissionService.setAll(this.DefaultCommissionModel).then(
    //         response => {
    //             console.log("response", response);
    //             this.init();
    //         },
    //         error => this.errorMessage = <any>error);
    // }
    // updateSellerCommission(id) {

    //     this.UpdateCommission = { "CommissionId": id, "Plan": this.SpecificCommissionModel };
    //     this.sellerCommissionService.updateCommissionbyShop(this.UpdateCommission).then(
    //         response => {
    //             this.init();

    //         },
    //         error => this.errorMessage = <any>error);
    // }
    searchShop() {
        // this.sellerCommissionService.viewShopbyModel(this.userName).then(
        //     response => {
        //         this.userDetails = JSON.parse(response);

        //         if (this.userDetails.hasOwnProperty('_id')) {
        //             this.getUserView = true;

        //   //          console.log("this.userDetails", this.userDetails);
        //         }
        //     },
        //     error => this.errorMessage = <any>error);

        this.GetAllSellerList(this.currentPage - 1, this.size, this.userName);
    }
    OpenPlans(list) {
        this.ShopDetails = list;
        list.Plans.forEach(plan =>{
            plan.StartDate = new Date(plan.StartDate);
            plan.ExpiryDate = new Date(plan.ExpiryDate);
        })
        this.ShopPlan = list.Plans;

    }
    AddPlans() {
        var model = {
            SpecificCommission: this.ShopDetails.DefaultCommission,
            CourierCommission: this.ShopDetails.CourierCommission,
            StartDate: new Date,
            ExpiryDate: new Date,
            edit: true
        }

        this.ShopPlan.push(model);
    }
    // closeFnc(){

    //     //console.log("ShopPlan1",this.ShopPlan);
    //     jQuery("#editCommission").modal("hide");
    // }

    // confirmEdit(plan, objid, planid){
    //     //console.log("Plan",plan);
    //     //console.log("editSpecificCommissionModel",this.editSpecificCommissionModel);
    //     Object.assign(this.editSpecificCommissionModel, plan);
    //     this.EditObjId=objid;
    //     this.EditPlanId=planid;
    //     jQuery("#editCommission").modal("show");
    // }

    back()
    {
        
        //this.init();
        this.ViewShopDetails=false;
        //jQuery('#navTabs a[href="#forShop"]').tab('show');
        setTimeout(()=>{
            jQuery('#firstTab').removeClass('active');
            jQuery('#secondTab').addClass('active');
        },1000);
       

    }
    edit(model, commisionId, planId, index) {
        //console.log(this.editSpecificCommissionModel);
        //console.log("ShopPlan",this.ShopPlan);
        //  this.sellerCommissionService.updatePlan(commisionId, planId,model).then(
        //     response => {
        //   //     console.log("edit response",response);
        //        this.ShopPlan = JSON.parse(response).Plans;
        //         //jQuery("#editCommission").modal("hide");
        //     },
        //     error => this.errorMessage = <any>error);
        var isDup = false;
        this.ShopPlan.splice(index, 1);
        var date=new Date();
        if(model.StartDate.toLocaleDateString() < date.toLocaleDateString())
        {
            this.toasterPopService.ErrorSnack( 'Cannot add for Expired date');
            return;
        }
       else if (model.StartDate.toLocaleDateString() > model.ExpiryDate.toLocaleDateString()) {
            this.toasterPopService.ErrorSnack( 'Start date cannot be greater than Expiry date');
            return;
        }
        if (this.ShopPlan.length > 0) {
            isDup = this.ShopPlan.some(ele => {

                var startDate1 = new Date(ele.StartDate);
                var startDate2 = new Date(model.StartDate);

                var endDate1 = new Date(ele.ExpiryDate);
                var endDate2 = new Date(model.ExpiryDate);

                var sameInterval = (startDate1.toDateString() === startDate2.toDateString() && endDate1.toDateString() === endDate2.toDateString());
                var intersectInterval = (startDate2 >= startDate1 && startDate2 <= endDate1) || (endDate2 >= startDate1 && endDate2 <= endDate1)

                return (sameInterval || intersectInterval)
            });

            if (isDup) {

                this.toasterPopService.ErrorSnack('Cannot enter same commission for intersecting interval');
                return;
            }
        }




        if (planId) {
            this.sellerCommissionService.updatePlan(commisionId, planId, model).then(
                response => {
                    //     console.log("edit response",response);
                    var plans = JSON.parse(response).Plans;
                    plans.forEach(plan =>{
                        plan.StartDate = new Date(plan.StartDate);
                        plan.ExpiryDate = new Date(plan.ExpiryDate);
                    })

                    this.ShopPlan = plans;
                    this.toasterPopService.SuccessSnack('Specific Commission Saved');
                    
                    //jQuery("#editCommission").modal("hide");
                },
                error => this.errorMessage = <any>error);
        }

        else {
            this.UpdateCommission = { "CommissionId": commisionId, "Plan": model };
            this.sellerCommissionService.updateCommissionbyShop(this.UpdateCommission).then(
                response => {
                    //this.init();
                    var plans = JSON.parse(response).Plans;
                    plans.forEach(plan =>{
                        plan.StartDate = new Date(plan.StartDate);
                        plan.ExpiryDate = new Date(plan.ExpiryDate);
                    })

                    this.ShopPlan = plans;
                    this.toasterPopService.SuccessSnack( 'Specific Commission Saved');
                    

                },
                error => this.errorMessage = <any>error);
        }



    }
    confirmDelete(objid, planid, index) {
        this.DeleteObjid = objid;
        this.DeletePlanid = planid;
        this.deletePlanIndex = index;
        jQuery("#deleteCommission").modal("show");
    }
    DeleteCommisssion() {
        // this.sellerCommissionService.deleteCommission(this.DeleteObjid, this.DeletePlanid).then(
        //     response => {
        //         this.ShopPlan = JSON.parse(response).Items[0].Plans;
        //     },
        //     error => this.errorMessage = <any>error);
        // jQuery("#deleteCommission").modal("hide");
        console.log(this.DeleteObjid, this.DeletePlanid);
        if (this.DeletePlanid) {
            this.sellerCommissionService.deleteCommission(this.DeleteObjid, this.DeletePlanid).then(
                response => {
                    if(JSON.parse(response).Items.length == 0){
                        this.ShopPlan = [];
                    }else{
                        this.ShopPlan = JSON.parse(response).Items[0].Plans;
                    }
                    this.toasterPopService.ErrorSnack('Specific Commission Removed');
                },
                error => this.errorMessage = <any>error);
            jQuery("#deleteCommission").modal("hide");
        }
        else {
            this.ShopPlan.splice(this.deletePlanIndex, 1);
            jQuery("#deleteCommission").modal("hide");
            this.toasterPopService.ErrorSnack( 'Specific Commission Removed');
        }
    }


    GetAllSellerList(from, size, userName) {
        this.sellerCommissionService.viewAll(from, size, userName).then(
            response => {
                //console.log("response viewAll", response);
                this.getAllSellerList = JSON.parse(response);
                this.getSellerList = this.getAllSellerList.Items;
                this.totalPages = this.getAllSellerList.TotalPages;
                this.pages = []
                //this.totalPages = res.TotalPages;

                for (let i = 1; i <= this.totalPages; i++) {
                    this.pages.push(i);
                }

            },
            error => this.errorMessage = <any>error);
    }

    previousPage() {
        if (this.currentPage > 1) {
            this.currentPage -= 1;
            this.GetAllSellerList((this.currentPage - 1), this.size, this.userName)

        }
    }

    nextPage() {
        if (this.currentPage < this.totalPages) {
            this.GetAllSellerList(this.currentPage, this.size, this.userName);
            this.currentPage += 1;
        }
    }

    selectPage(PageNumber) {
        this.currentPage = PageNumber;
        this.GetAllSellerList((this.currentPage - 1), this.size, this.userName);

    }

   

}
