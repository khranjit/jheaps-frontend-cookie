import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SpecialOfferComponent } from './special-offer.component';
import { SharedModule } from '../../../shared/shared.module';
import { MatInputModule } from '@angular/material';
  
const specialoffers: Routes = [
    {path: '', component:SpecialOfferComponent},
]
@NgModule({
  imports: [
    SharedModule,
    MatInputModule,
    RouterModule.forChild(specialoffers)
  ],
  declarations: [
    SpecialOfferComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class SpecialOfferModule { }