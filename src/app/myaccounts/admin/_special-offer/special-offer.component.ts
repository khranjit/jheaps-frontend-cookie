import { Component } from '@angular/core';
import { ProductService } from '../../../services/product.service';
import { ElasticService } from '../../../services/elastic.service';
import { NotifyService } from '../../../services/notify.service';
import { UserService } from '../../../services/user.service';
import { Router} from '@angular/router';

@Component({
  
  providers: [ ProductService, ElasticService, NotifyService,UserService],
  selector: 'special-offer',
  templateUrl: './special-offer.component.html'
})
export class SpecialOfferComponent{ 

  public errorMessage: any;
  SpecialOfferTitlesList = <any>{
      ColumnOne : "",
      ColumnTwo : "",
      ColumnThree : "",
      ColumnFour : "",
      Limit : 4
  };
     columnOne=5;
     columnTwo=2;
     columnThree=3;
     columnFour=4;
    public pages: any[];
    totalPages = 0;
    currentPage = 1;
    from = 0;
    size = 10;
    total = 0;  
  updatedSpecialOfferList: any;
  hitsList = [];
  searchTerm = "";
  ShowUserRole : any;
  ShowUserProfile : any;
  

    
  constructor(private userService: UserService,private router: Router, private productService: ProductService, private elasticService: ElasticService,  private toasterService:NotifyService) {
    this.userService.showProfile().then(
        response => {
            this.ShowUserProfile = JSON.parse(response); 
            this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                console.log('type of user'+this.ShowUserRole)
                // if(this.ShowUserRole== 'masteradmin'||this.ShowUserRole== 'admin'){
                   this.Init();
                // }
                // else{
                //     this.router.navigate(['']);
                // }
        }
       );
  }

  public Init() {
      this.currentPage=1;
    window.scrollTo(0,20);
    this.productService.getSpecialOfferTitles()
        .then(
        offers => {
            var data = JSON.parse(offers);
            if(data.Items.length > 0){
                this.SpecialOfferTitlesList = data.Items[0];
                this.getSuggestedProducts();
            }
           // console.log("SpecialOfferTitles", this.SpecialOfferTitlesList);
        },
        error => this.errorMessage = <any>error);
  }
   
  public addSpecialOfferTitles(){
        // this.updatedSpecialOfferList = {};
        // this.updatedSpecialOfferList.ColumnOne = this.SpecialOfferTitles1;
        // this.updatedSpecialOfferList.ColumnTwo = this.SpecialOfferTitles2;
        // this.updatedSpecialOfferList.ColumnThree = this.SpecialOfferTitles3;
        // this.updatedSpecialOfferList.ColumnFour = this.SpecialOfferTitles4;
        // this.updatedSpecialOfferList.Limit = this.productDisplayLimit;
        this.productService.addSpecialOfferTitles(this.SpecialOfferTitlesList)
        .then(offers => {  
            this.Init();
            this.toasterService.SuccessSnack( "Special offer title added successfully");
        },
        error => this.errorMessage = <any>error);
  }
  
    
//   public updateSpecialOfferTitles(){
//         this.updatedSpecialOfferList = {};
//         this.updatedSpecialOfferList.ColumnOne = this.SpecialOfferTitles1;
//         this.updatedSpecialOfferList.ColumnTwo = this.SpecialOfferTitles2;
//         this.updatedSpecialOfferList.ColumnThree = this.SpecialOfferTitles3;
//         this.updatedSpecialOfferList.ColumnFour = this.SpecialOfferTitles4;
//         this.updatedSpecialOfferList.Limit = this.productDisplayLimit;
//   //      console.log(this.SpecialOfferTitlesList);  
//         this.productService.updateSpecialOfferTitles(this.SpecialOfferTitlesList.Items[0]._id, this.updatedSpecialOfferList)
//         .then(offers => {  
//             this.Init();
//         },
//         error => this.errorMessage = <any>error);
//   }


previousPage(){
    if(this.currentPage > 1){
        this.getSuggestedProductsFromElastic(this.searchTerm, ((this.currentPage - 1)*this.size), this.size); 
        this.currentPage -= 1;  
    }
}

nextPage(){
    if(this.currentPage < this.totalPages){
        this.getSuggestedProductsFromElastic(this.searchTerm, ((this.currentPage )*this.size), this.size);
        this.currentPage += 1;  
    }
}

selectPage(PageNumber) {
    this.currentPage = PageNumber;
    this.getSuggestedProductsFromElastic(this.searchTerm, ((this.currentPage - 1)*this.size), this.size);
    
}

 public getSuggestedProducts()
 {
     this.getSuggestedProductsFromElastic(this.searchTerm, this.from, this.size);
 }
  
  public getSuggestedProductsFromElastic(searchTerm, from, size){
        this.hitsList = [];
        this.pages = []
            this.elasticService.getSearchSuggestionsWithPaging(searchTerm,from,size).then(
            products => {
                var items = JSON.parse(products).hits;

                this.hitsList = items.hits;

                this.totalPages = Math.ceil(items.total / size);

                for(let i = 1; i <= this.totalPages; i++){
                this.pages.push(i);
                 }

                for(var i= 0; i< this.hitsList.length; i++){
                    if(this.hitsList[i]._source.ColumnOne){
                        this.hitsList[i]._source.ColumnOne = 1;
                    }else if(this.hitsList[i]._source.ColumnTwo){
                        this.hitsList[i]._source.ColumnTwo = 2;
                    }else if(this.hitsList[i]._source.ColumnThree){
                        this.hitsList[i]._source.ColumnThree = 3;
                    }else if(this.hitsList[i]._source.ColumnFour){
                        this.hitsList[i]._source.ColumnFour = 4;
                    } 
                }
    //            console.log(this.hitsList)
            },error => this.errorMessage = <any>error);
        
  }
  
  public productUpdate(product, index){
      //console.log(product)
      product._source.ColumnOne = false;
      product._source.ColumnTwo = false;
      product._source.ColumnThree = false;
      product._source.ColumnFour = false;
      if(index == 5){
        product._source.ColumnOne = true;
      }else if(index == 2){
        product._source.ColumnTwo = true;
      }else if(index == 3){
        product._source.ColumnThree = true;
      }else if(index == 4){
        product._source.ColumnFour = true;
      }
      console.log(product);
      this.productService.updateproduct(product._id, product._source)
        .then(offers => {  
        },
        error => this.errorMessage = <any>error); 
  }
  public CheckedProduct(product, index){
    if(index == 5 && product._source.ColumnOne == 1){
        return true;
    }else if(index == 2 && product._source.ColumnTwo == 2){
        return true;
    }else if(index == 3 && product._source.ColumnThree == 3){
        return true;
    }else if(index == 4 && product._source.ColumnFour == 4){
      return true;
    }
    else
    {
        return false;
    } 
}
    
  public resetProductsByColumnName(columnName){
      let columnInfo = {
        "Column" : columnName  
      };
      this.searchTerm = '';
      this.productService.resetAdminCriteriaByColumnName(columnInfo)
        .then(resetInfo => {  
            this.toasterService.SuccessSnack("Products reset Succesfully");
            this.Init();
        },
        error => this.errorMessage = <any>error);  
  }
}