import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SellerTierComponent } from './seller-tier.component';
import { SharedModule } from '../../../shared/shared.module';
  
  
const sellertier: Routes = [
    {path: '', component:SellerTierComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(sellertier)
  ],
  declarations: [
    SellerTierComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class SellerTierModule { }