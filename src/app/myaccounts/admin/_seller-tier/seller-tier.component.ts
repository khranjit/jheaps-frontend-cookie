import { Component } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { NotifyService } from '../../../services/notify.service';
import { UserService } from '../../../services/user.service';

@Component({
    
    providers: [UserService, NotifyService],
    selector: 'seller-tier',
    templateUrl: './seller-tier.component.html'
})
export class SellerTierComponent {

    public userName: string;
    public tierValue: number;
    public errorMessage: any;
    public userDetails: any;
    public userNotFound: any;
    public userDetailsLength: any;
    public getAllSellerList: any;
    public getSellerList = [];
    ShowUserRole : any;
     ShowUserProfile : any;
    public pages: any[];
    totalPages = 0;
    currentPage = 1;
    from = 0;
    size = 10;
    total = 0; 

    constructor(private route: ActivatedRoute, private router: Router, private toasterPopService: NotifyService, private userService: UserService) {
      //  console.log("Test Seller Tier");
      this.userService.showProfile().then(
        response => {
            this.ShowUserProfile = JSON.parse(response); 
            this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                console.log('type of user'+this.ShowUserRole)
                // if(this.ShowUserRole== 'masteradmin'||this.ShowUserRole== 'admin'){
                    this.getSellerList=[];
                       this.Init();
                // }
                // else{
                //     this.router.navigate(['']);
                // }
        }
       );
        //this.userNotFound = "";
       
    }

    Init() {
        window.scrollTo(0,20);
        this.currentPage = 1;
        this.from = 0;
        this.GetSellerList(this.userName,this.currentPage -1);
        
    }
 GetSellerList(userName, pageNum)
 {
    this.userService.getAllSellers(this.userName, pageNum).then(
        response => {
    //        console.log("response", response);
            this.getAllSellerList = JSON.parse(response);
            this.getSellerList = this.getAllSellerList.Items;
            this.totalPages = this.getAllSellerList.TotalPages;
            this.pages = [] 
              //this.totalPages = res.TotalPages;

             for(let i = 1; i <= this.totalPages; i++){
             this.pages.push(i);
              }
        },
        error => this.errorMessage = <any>error);
 }
 UpdateSellerInfo(list){
     if(list.SellerTier)
     {
         if(list.SellerTier > 3 || list.SellerTier <1){
            list.SellerTier = 1;
            return;
         }

     }

     else{
        list.SellerTier = 1;
        return;
     }


    this.userService.changeSellerTier(list.SellerTier, list._id).then(
        response => {
          //  console.log(response);
            this.toasterPopService.SuccessSnack( 'Tier Status Updated');
        },
        error => {
            this.toasterPopService.ErrorSnack( 'Tier Status Not Updated');
        });
 }
    makeTierOneSeller() {
        //console.log("name", this.userName);
        this.userNotFound = "";
        this.userService.getUserDetailsByNameOrEmail(this.userName).then(
            response => {
          //      console.log("res", response);
                this.userDetails = JSON.parse(response);
                if (this.userDetails.hasOwnProperty("msg")) {
                    this.userNotFound = this.userDetails.msg;
                    this.userDetails = {};
                }
                this.userDetailsLength = Object.keys(this.userDetails).length
            //    console.log("Details", this.userDetailsLength, this.userNotFound, this.userDetails);
            },
            error => this.errorMessage = <any>error);
    }

    // convertToTierTwo() {
    //     //console.log("name and value", this.userName, this.tierValue, this.userDetails._id);
    //     this.userService.changeSellerTier(this.tierValue, this.userDetails._id).then(
    //         response => {
    //           //  console.log(response);
    //         },
    //         error => {
    //         });
    // }

    //  
    
    
    previousPage(){
        if(this.currentPage > 1){
            this.currentPage -= 1; 
            this.GetSellerList( this.userName,(this.currentPage - 1))
             
        }
    }
    
    nextPage(){
        if(this.currentPage < this.totalPages){
            this.GetSellerList( this.userName,this.currentPage);
            this.currentPage += 1;  
        }
    }
    
    selectPage(PageNumber) {
        this.currentPage = PageNumber;
        this.GetSellerList(  this.userName,(this.currentPage - 1));
        
    }

}