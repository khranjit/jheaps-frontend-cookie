import { NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentCompleteController } from './payment-complete.component';
import { SharedModule } from '../../../shared/shared.module';
import { MatInputModule } from '@angular/material';
  
  
const paymentcomplete: Routes = [
    {path: '', component:PaymentCompleteController},
]
@NgModule({
  imports: [
    SharedModule,
    MatInputModule,
    RouterModule.forChild(paymentcomplete)
  ],
  declarations: [
    PaymentCompleteController
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class PaymentCompleteModule { }