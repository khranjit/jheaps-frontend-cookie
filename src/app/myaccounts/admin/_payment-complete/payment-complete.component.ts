import { Component } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { AdminCriteriaService } from '../../../services/admin-criteria.service';
import { NotifyService } from '../../../services/notify.service';
import { UtilService } from '../../../services/util.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../../../services/user.service';
import { ReportsService } from '../../../services/reports.service';

declare var jQuery: any;
declare var Cropper: any;
declare var Croppie: any;

@Component({
    selector: 'payment-complete',
    providers: [AdminCriteriaService, NotifyService, UtilService, UserService,ReportsService],
    templateUrl: './payment-complete.component.html'
})

export class PaymentCompleteController {
   
    SettlementId="";
    SettlementDetail = [];
    SettlementView = false;
    constructor(private userService: UserService, private route: ActivatedRoute, private loader: NgxSpinnerService, private router: Router, private adminCriteriaService: AdminCriteriaService,
        private toasterPopService: NotifyService, private utilService: UtilService,private payment:ReportsService) {
            
    }
    public init() {
        
    }
    public MakePayment(){
        var model = {
            SettlementId : this.SettlementId
        }
        this.loader.show();
        this.payment.PaymentComplete(model).then(result=>{
            var res = JSON.parse(result);
                if(res.msg){
                    this.loader.hide();
                    this.SettlementDetail = [];
                    this.SettlementView = false;
                    this.toasterPopService.ErrorSnack(res.msg)
                }else{
                    this.loader.hide();
                    this.SettlementDetail = res;
                    this.SettlementView = true;
                }
        })
    }
    public MakePaymentupdate(){
        var model = {
            SettlementId : this.SettlementId
        }
        this.loader.show();
        this.payment.PaymentCompleteUpdate(model).then(result=>{
            var res = JSON.parse(result);
                if(res.msg){
                    this.loader.hide();
                    this.SettlementDetail = [];
                    this.SettlementView = false;
                    this.toasterPopService.ErrorSnack(res.msg)
                }else{
                    this.loader.hide();
                    this.SettlementView = false;
                    this.SettlementDetail =[];
                    this.SettlementId = "";
                    this.toasterPopService.SuccessSnack("Payment Completed Updated Successfully")

                }
        })
    }

    
}