import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubAdminCreationComponent } from './sub-admin-creation.component';
import { SharedModule } from '../../../shared/shared.module';
  
  
const subadmin: Routes = [
    {path: '', component:SubAdminCreationComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(subadmin)
  ],
  declarations: [
    SubAdminCreationComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class SubAdminCreationModule { }