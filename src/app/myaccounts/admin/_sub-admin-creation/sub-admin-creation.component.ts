import { Component } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { AdminCriteriaService } from '../../../services/admin-criteria.service';
import { NotifyService } from '../../../services/notify.service';
import { UserService } from '../../../services/user.service';
declare var jQuery : any;

@Component({ 
  
  selector: 'sub-admin-creation',
  providers: [AdminCriteriaService,NotifyService,UserService],
  templateUrl: './sub-admin-creation.component.html'
})

export class SubAdminCreationComponent{
    public profileModel : ViewModels.IProfileViewModel;
    //ViewSubAdminProfile = <any>{};
    ViewSubAdminProfileList =[]
    errorMessage : any;
    ShowUserRole : any;
    ShowUserProfile : any;
    ListSubAdminProfiles = [];
    deletedSubAdmin : any;
    details = <any>{};
    showdetails: boolean;   
    public pages: any[];
    totalPages = 0;
    currentPage = 1;
    from = 0;
    size = 5;
    total = 0; 
    public pages1: any[];
    totalPages1= 0;
    currentPage1 = 1;
    from1 = 0;
    size1 = 5;
    total1 = 0;
    constructor(private userService: UserService,private route: ActivatedRoute, private router: Router,private adminCriteriaService: AdminCriteriaService, private toasterPopService: NotifyService  ) {
       
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response); 
                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                    console.log('type of user'+this.ShowUserRole)
                    // if(this.ShowUserRole== 'masteradmin'||this.ShowUserRole== 'admin'){
                        this.showdetails=false;
                        this.init();
                    // }
                    // else{
                    //     this.router.navigate(['/home']);
                    // }
            }
           );
       
       
    }
    
    public init() {
        window.scrollTo(0,20);
        this.currentPage = 1;
        this.from = 0;
        this.currentPage1 = 1;
        
        this.profileModel = <ViewModels.IProfileViewModel>{};
        // this.profileModel.Address = <ViewModels.IAddressViewModel>{};
        this.SubAdminProfile(this.currentPage1 -1, this.size1);
        this.ListSubAdmin(this.currentPage-1, this.size);
    }
    
    // getdetails(value){
    //     this.details={"UserNameOrEmail":value};
    // }
    
    public SubAdminProfile(from?,size?) {
        
        //console.log( this.details );
        this.adminCriteriaService.viewSubAdminProfile(this.details, from, size).then(
            response => {
                  var userData= JSON.parse(response);
                 if(userData.Items.length>0)
                 {
                    this.showdetails=true;
                    this.ViewSubAdminProfileList = userData.Items;
                    this.pages1 = [] 
                    this.totalPages1 = userData.TotalPages;
      
                   for(let i = 1; i <= this.totalPages1; i++){
                   this.pages1.push(i);
                    }
                    
                    //this.ViewSubAdminProfileAddress = this.ViewSubAdminProfile.Address;
                 }

                 else{
                    this.toasterPopService.ErrorSnack('No User Found');
                 }
          //       console.log("ViewSubAdminProfile",this.ViewSubAdminProfile);
                 
            //     console.log(this.ViewSubAdminProfileAddress);
             },
            error =>{
                 this.toasterPopService.ErrorSnack('No User Found');
            }
         );
     }
    
    public ListSubAdmin(from, size) {
      this.adminCriteriaService.listSubAdminProfile(from,size).then(
            response => { 
                 var res = JSON.parse(response);
                 this.ListSubAdminProfiles = res.Items;
                 this.pages = [] 
              this.totalPages = res.TotalPages;

             for(let i = 1; i <= this.totalPages; i++){
             this.pages.push(i);
              }

              //   console.log("this.ListSubAdminProfiles",this.ListSubAdminProfiles);
             },
            error => {
                 this.toasterPopService.ErrorSnack('No sub admin in the List!!..');
            }
         );
     }
    
    MakeSubAdmin(model) {
        this.adminCriteriaService.makeSubAdmin({UserNameOrEmail:model}).then(
            response => {
                 
                    this.toasterPopService.SuccessSnack('You have added a sub admin!!');
                    this.init(); 
                
             },
            error => {
                this.toasterPopService.ErrorSnack('Error please check the user');
            }
       );
    }
    
    public deleteSubAdmin(profileModel){
        this.deletedSubAdmin = {"UserNameOrEmail":profileModel._id};
        jQuery("#confirmDeleteModal").modal('show');
        
    }
    
    delete(){
       // console.log(this.deletedSubAdmin);
       this.adminCriteriaService.deleteSubAdmin(this.deletedSubAdmin).then(
            response => {
                //console.log(response);
                this.ListSubAdminProfiles = JSON.parse(response).Items;
                      this.init();
                    this.toasterPopService.SuccessSnack('Sub Admin have been deleted Successfully!!');
                   
               
            },
            error => {
                this.toasterPopService.ErrorSnack('There is an error in deleting your Sub Admin.Try after sometime..');
        });
    }

    previousPage(){
        if(this.currentPage > 1){
            this.currentPage -= 1;
            this.ListSubAdmin( (this.currentPage - 1), this.size ); 
              
        }
    }
    
    nextPage(){
        if(this.currentPage < this.totalPages){
            this.ListSubAdmin( this.currentPage , this.size);
            this.currentPage += 1;  
        }
    }
    
    selectPage(PageNumber) {
        this.currentPage = PageNumber;
        this.ListSubAdmin( (this.currentPage - 1), this.size );
        
    }

    previousPage1(){
        if(this.currentPage1 > 1){
            this.currentPage1-= 1;
            this.SubAdminProfile( (this.currentPage1 - 1), this.size1 ); 
              
        }
    }
    
    nextPage1(){
        if(this.currentPage1 < this.totalPages1){
            this.SubAdminProfile( this.currentPage1 , this.size1);
            this.currentPage1 += 1;  
        }
    }
    
    selectPage1(PageNumber) {
        this.currentPage1 = PageNumber;
        this.SubAdminProfile( (this.currentPage1 - 1), this.size1 );
        
    }
    
}