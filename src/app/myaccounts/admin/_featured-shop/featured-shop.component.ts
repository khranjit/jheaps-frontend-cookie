import { Component, ChangeDetectionStrategy, ViewEncapsulation, Inject } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { FeaturedShopService } from '../../../services/featured-shop.service';
import { UserService } from '../../../services/user.service';
declare var jQuery: any;

@Component({ 
  
  selector: 'featured-shop',
  providers: [FeaturedShopService,UserService],
  templateUrl: './featured-shop.component.html'
})

export class FeaturedShopComponent {
  public featuredShopsList: any;
  public FeaturedShops = [];
  public errorMessage: any;
  public deletableContent: any;
  ShowUserRole : any;
  ShowUserProfile : any;
  
  constructor(private userService: UserService,private route: ActivatedRoute, private router: Router, private featuredShopService: FeaturedShopService) {
    this.userService.showProfile().then(
      response => {
          this.ShowUserProfile = JSON.parse(response); 
          this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
              console.log('type of user'+this.ShowUserRole)
              // if(this.ShowUserRole== 'masteradmin' || this.ShowUserRole== 'admin'){
                 this.Init();
              // }
              // else{
              //     this.router.navigate(['']);
              // }
      }
     );
  }

  public Init() {
    window.scrollTo(0,20);
    this.featuredShopService.getAll()
      .then(
      featuredShops => {
        this.featuredShopsList = JSON.parse(featuredShops);
          console.log("featuredShopsList", this.featuredShopsList);
        this.FeaturedShops = this.featuredShopsList;
      },
      error => this.errorMessage = <any>error);
  }
  confirmDelete(featuredShop) {
      console.log("featuredShop id",featuredShop);
    this.deletableContent = featuredShop;
      console.log(" this.deletableContent", this.deletableContent)
    jQuery("#confirmDeleteModal").modal('show');
  }

  delete() {
  this.featuredShopService.deleteFeaturedShop(this.deletableContent)
    .then(
        status => {  this.Init();  },
      error => this.errorMessage = <any>error);
  
  

  }
  addNew() {
        this.router.navigate(['/myaccounts/admin/featured-shop/add']);
  }

}