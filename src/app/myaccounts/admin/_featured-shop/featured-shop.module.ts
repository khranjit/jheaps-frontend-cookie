import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { FeaturedShopComponent } from './featured-shop.component';
import { AddFeaturedShopComponent } from './_add-featuredshop/add-featured-shop.component';

const featuredshops: Routes = [
  {path: '',redirectTo:'view',pathMatch:'full'},
  {path: 'view', component:FeaturedShopComponent},
  {path: 'add', component:AddFeaturedShopComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(featuredshops)
  ],
  declarations: [
      FeaturedShopComponent,
      AddFeaturedShopComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class FeaturedShopModule { } 