import { Component, Inject, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// import { ToasterPopService } from '../../../services/toasterpop.service';
import { FeaturedShopService } from '../../../../services/featured-shop.service';
import { NotifyService } from '../../../../services/notify.service';


@Component({
    
    selector: 'addfeaturedshop',
    //providers: [ProductService],
    providers: [ FeaturedShopService],
    templateUrl: './add-featured-shop.component.html'
})

export class AddFeaturedShopComponent {

    public errorMessage: any;
    newbutton = true;
    editbutton = false;
    
    public ProductsList: any;
    public Products: any;
    getDetails: string;
    searchDetails: any;
    displayDetails = [];
    public addFeaturedShop: any;
    searchDisplay: boolean;
    searchTerm = "";

    public pages: any[];
    totalPages = 0;
    currentPage = 1;
    from = 0;
    size = 10;
    total = 0;


    constructor(private route: ActivatedRoute, private router: Router, private snack: NotifyService, private featuredShopService: FeaturedShopService) {
        this.searchDisplay = false;
        // this.getDetails="";
        this.displayDetails = [];
        this.Init();
    }

    public Init() {
        //Init Data
        window.scrollTo(0,20);
        this.currentPage = 1;
        this.pages = [];
        this.Search(this.searchTerm);
        
    }

    // getSearchValue(value) {
    //     this.getDetails = value;
    // }

    Search(event) {
        //console.log("Search Data");
        this.currentPage = 1;
        this.pages = [];
        this.getSearchData(event, this.from, this.size);
    }

    getSearchData(event, from, size)
    {
        this.searchDetails = { "Name": event };
        //console.log("Search Data", this.getDetails);
        this.featuredShopService.searchShop(this.searchDetails, from, size).then(
            Status => {
                //console.log("STatus", Status);
                var res = JSON.parse(Status);
                this.displayDetails = res.Items;
                this.pages = [] 
              this.totalPages = Math.ceil(res.TotalItems /this.size);

             for(let i = 1; i <= this.totalPages; i++){
             this.pages.push(i);
              }
            },
            error => {
                this.errorMessage = <any>error;
                this.snack.ErrorSnack("Shop not found");
            })
    }

    previousPage(){
        if(this.currentPage > 1){
            this.getSearchData( this.searchTerm,((this.currentPage - 1)*this.size), this.size *this.currentPage); 
            this.currentPage -= 1;  
        }
    }
    
    nextPage(){
        if(this.currentPage < this.totalPages){
            this.getSearchData(this.searchTerm, ((this.currentPage )*this.size), this.size *(this.currentPage +1));
            this.currentPage += 1;  
        }
    }
    
    selectPage(PageNumber) {
        this.currentPage = PageNumber;
        this.getSearchData(this.searchTerm, ((this.currentPage - 1)*this.size), this.size * (this.currentPage));
        
    }
    // key_down(e,searchshop) {
    //     //console.log(e.keyCode)
    //     if(e.keyCode === 13) {
    //         //console.log("hello",e.keyCode,searchshop)
    //         this.getSearchValue(searchshop);
    //         this.Search();
    //     }
    //   }

    add(id, name) {

        //console.log("Added", this.addFeaturedShop)
        this.addFeaturedShop = { "Shop": { "Id": id, "Name": name } };
        //console.log("Added", this.getDetails)
        this.featuredShopService.addFeaturedShop(this.addFeaturedShop).then(
            Status => {
          //      console.log("STatus", Status)
          this.snack.SuccessSnack("Item added to featured shops");

                this.Search(this.searchTerm);
            },
            error => this.errorMessage = <any>error);

    }

    goBack() {
        this.router.navigate(['/myaccounts/admin/featured-shop']);
    }

}