import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReturnComponent } from './return.component';
import { AddReturnReasonComponent } from './_add-reason/add-reason.component';
import { ReturnReasonComponent } from './_reason/return-reason.component';
import { SharedModule } from '../../../shared/shared.module';
  
  
const returnreason: Routes = [
  {path: '',redirectTo:'list',pathMatch:'full'},
  {path:'list',component:ReturnComponent,
    children:
      [
        {path: '',redirectTo:'view',pathMatch:'full'},
        {path: 'view', component:ReturnReasonComponent},
        {path: 'add', component:AddReturnReasonComponent}
      ]
  }
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(returnreason)
  ],
  declarations: [
    ReturnComponent,
    ReturnReasonComponent,
    AddReturnReasonComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ReturnModule { }