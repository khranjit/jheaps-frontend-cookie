import { Component, Inject, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { NotifyService } from '../../../../services/notify.service';
import { ReturnReasonService } from '../../../../services/return-reason.service';

declare var jQuery: any;

@Component({ 
  
  selector: 'return-reason',
  providers: [ReturnReasonService,NotifyService],
  templateUrl: "./return-reason.component.html"
})

export class ReturnReasonComponent {
  public Reasons = [];
  public ReasonsList:any;
  public errorMessage: any;
  public deletableContent: any;
  deleteModel:any;
  deleteModelBody:any;
  public pages: any[];
    totalPages = 0;
    currentPage = 1;
    from = 0;
    size = 10;
    total = 0;


  constructor(private route: ActivatedRoute, private router: Router, private toasterPopService:NotifyService,
      private returnReasonService: ReturnReasonService) {
       
      this.Init();
  }
  public Init() {
    window.scrollTo(0,20);
    this.currentPage = 1;
    this.pages = [];
    this.getReasons(this.from, this.size);
  }
	edit(reason){
    let navimodel: NavigationExtras = {
      queryParams: { 'Reason': JSON.stringify(reason) }
    }
    this.router.navigate(['/myaccounts/admin/return-reason/list/add'], navimodel);
    //console.log(attribute)
  }

  confirmDelete(reason){
    this.deletableContent = reason._id;
    this.deleteModel = false;
    this.deleteModelBody = 0;
    this.returnReasonService.checkReason(reason._id).then(res=> {
      var res = JSON.parse(res);
      jQuery("#confirmDeleteModal").modal('show');
    }, 
      error=> this.errorMessage = <any>error);
    //jQuery("#confirmDeleteModal").modal('show');
  }

  Delete(){
    this.returnReasonService.deleteReason(this.deletableContent).then(result =>{
      this.toasterPopService.SuccessSnack('Reason deleted Successfully!!');
      jQuery("#confirmDeleteModal").modal('hide');
      this.Init();
    });
  }
    addNew() {
    this.router.navigate(['/myaccounts/admin/return-reason/list/add']);
  }
  getReasons(limit, offset)
  {
   this.returnReasonService.getAll(limit, offset)
   .then(
   attributes => {
     this.ReasonsList = JSON.parse(attributes);
     this.Reasons=this.ReasonsList.Items;
     this.pages = [] 
     this.totalPages = this.ReasonsList.TotalPages
     for(let i = 1; i <= this.totalPages; i++)
     {
        this.pages.push(i);
     }
     //console.log("Attributes",this.AttributesList)
   },
   error => this.errorMessage = <any>error);
  } 

     previousPage(){
      if(this.currentPage > 1){
          this.getReasons(this.currentPage - 2, this.size); 
          this.currentPage -= 1;  
      }
  }
  
  nextPage(){
      if(this.currentPage < this.totalPages){
          this.getReasons( this.currentPage, this.size);
          this.currentPage += 1;  
      }
  }
  
  selectPage(PageNumber) {
      this.currentPage = PageNumber;
      this.getReasons(this.currentPage - 1, this.size);
      
  }
}
