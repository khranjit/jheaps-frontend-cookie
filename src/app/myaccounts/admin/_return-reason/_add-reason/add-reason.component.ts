import { Component, Inject, ChangeDetectionStrategy, ViewEncapsulation, ViewChild } from '@angular/core';
import { ReturnReasonService } from '../../../../services/return-reason.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyService } from '../../../../services/notify.service';
import { json } from 'body-parser';
//declare var jQuery: any;

@Component({
    
    selector: 'add-reason',
    providers: [ReturnReasonService],
    templateUrl: "./add-reason.component.html"
})

export class AddReturnReasonComponent {

    public ReasonModel: ViewModels.IReasonViewModel;
    public ReasonsList: any;
    public Reasons: any;
    newbutton = true;
    editbutton = false;
    alertShow = false;
    errorMessage: any;
    disableName = false;
        constructor(private route: ActivatedRoute, private router: Router,
        private returnReasonService: ReturnReasonService, private snack: NotifyService) {
        this.Init();
    }



    public Init() {
        this.ReasonModel = <ViewModels.IReasonViewModel>{};
        this.route
            .queryParams
            .subscribe(params => {
                if (params['Reason']) {
                    this.ReasonModel = JSON.parse(params['Reason']);
                    this.editbutton = true;
                    this.newbutton = false;
                }
            });
    }

    newAttribute() {
        this.returnReasonService.createReason(this.ReasonModel).then
            (Status => {
                var result= JSON.parse(Status);
                if(result.flag == 1)
                {
                    this.snack.SuccessSnack(result.msg);
                    this.router.navigate(['/myaccounts/admin/return-reason/list']);
                    // this.returnReasonService.getAll()
                    //     .then(
                    //     reasons => {
                    //         this.ReasonModel = JSON.parse(reasons);
                    //     },
                    //     error => this.errorMessage = <any>error);
                }
                else
                {
                    this.snack.ErrorSnack(result.msg);
                }
            },
            error => {
                this.errorMessage = <any>error;
                this.snack.ErrorSnack('Its already exists');
                console.log("error ", this.errorMessage)
            });
    }

    editAttribute() {
        this.returnReasonService.editReason(this.ReasonModel).then(
            Status => {
                var result= JSON.parse(Status);
                if(result.flag==1)
                {
                    this.snack.SuccessSnack(result.msg);
                    this.router.navigate(['/myaccounts/admin/return-reason/list']);
                    // this.returnReasonService.getAll()
                    //     .then(
                    //     reasons => {
                    //         this.ReasonModel = JSON.parse(reasons);
                    //     },
                    //     error => this.errorMessage = <any>error);
                }
                else
                {
                    this.snack.ErrorSnack(result.msg);
                }
            },
            error => {
                this.errorMessage = <any>error;
                this.snack.ErrorSnack('Its already exists');
                console.log("error ", this.errorMessage)
            });
    }

    goBack() {
        this.router.navigate(['/myaccounts/admin/return-reason/list']);
    }
}