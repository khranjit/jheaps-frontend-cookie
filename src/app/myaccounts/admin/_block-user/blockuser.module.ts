import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlockUserComponent } from './blockuser.component';
import { SharedModule } from '../../../shared/shared.module';
import {CalendarModule} from 'primeng/primeng';
  
const blockuser: Routes = [
    {path: '', component:BlockUserComponent},
]
@NgModule({
  imports: [
    SharedModule,
    CalendarModule,
    RouterModule.forChild(blockuser)
  ],
  declarations: [
    BlockUserComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class BlockUserModule { }