import { Component } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { NotifyService } from '../../../services/notify.service';
import { init } from 'nconf';
declare var jQuery: any;

@Component({
    

    selector: 'block-user',
    providers: [UserService, NotifyService],
    templateUrl: './blockuser.component.html'
})


export class BlockUserComponent {
    public userModel = <any>{};
    putBlockuser: any;
    unBlockeduser: any;
    viewBlockedusers = [];
    userList=[];
    errorMessage:any;
    public pages: any[];
    totalPages = 0;
    currentPage = 1;
    from = 0;
    size = 5;
    total = 0;
    public pages1: any[];
    totalPages1 = 0;
    currentPage1 = 1;
    from1 = 0;
    size1 = 5;
    ShowUserRole : any;
  ShowUserProfile : any;
  showUserProfileDetail : any;
    total1 = 0;
    showList = true;
    sort_type = 1;
    showBlockList=true;
   constructor(private route: ActivatedRoute, private router: Router, private UserService: UserService, private snack: NotifyService) {
    this.UserService.showProfile().then(
        response => {
            this.ShowUserProfile = JSON.parse(response); 
            this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                console.log('type of user'+this.ShowUserRole)
                if(this.ShowUserRole== 'masteradmin'||this.ShowUserRole== 'admin'){
                    this.init();

                }
                else{
                    this.router.navigate(['/home']);
                }
        }
       );    
    
    
        }
    
        Blockuser(UserName) {
         			 this.UserService.putBlockuser({UserName:UserName}).then(
          			  response => {      
          			      this.putBlockuser = JSON.parse(response).Items;
                 		 if (response == 200) { 
                   		 this.snack.SuccessSnack('The user has been blocked successfully.');
                   		 this.init();
                 }
             },
            error => {
                this.snack.ErrorSnack('User not found!');
            }
       );
    }

    GetUserProfile(from,size)
    {
        var today = new Date();
        // if (this.userModel.StartDate != "" && this.userModel.StartDate < today) {
        
        //     this.snack.SuccessSnack('Start Date should be greater than today date');
        
        // }
       if (this.userModel.StartDate != "" && this.userModel.EndDate !="" && this.userModel.StartDate > this.userModel.EndDate) {
            
            this.snack.SuccessSnack('Start date should not be greater than end date');
        }
        else {
            this.UserService.GetAllUsers(this.userModel,from , size,this.sort_type).then(
                response => {
                    var list =  JSON.parse(response);  
                    this.userList = list.Items ;
                    this.totalPages1 =list.TotalPages;
                    this.pages1 = [] 
                      //this.totalPages = res.TotalPages;
        
                     for(let i = 1; i <= this.totalPages1; i++){
                     this.pages1.push(i);
                      }
                    //this.init();
         
           },
    error => {
        this.snack.ErrorSnack('User not found!');
    }
    );

        }
    
    }
        public UnBlockuser(UserName) {
//       		 console.log("Test"); 
       			 this.UserService.unBlockeduser({UserName:UserName}).then(
          			  response => {      
          			      //this.unBlockeduser = JSON.parse(response).Items;
                 		 if (response == 200) { 
                   		 this.snack.SuccessSnack('The user has been unblocked successfully.');
                   		 this.init();
                 }
             },
            error => {
                this.snack.ErrorSnack('User not found!');
            }
       );
    }
   public ListBlockedusers(pageNumber,size) {
      this.UserService.viewBlockedusers(this.userModel, pageNumber,size).then(
            response => {
                 var list= JSON.parse(response);
                 this.viewBlockedusers  = list.Items
                 this.totalPages =list.TotalPages;
            this.pages = [] 
              //this.totalPages = res.TotalPages;

             for(let i = 1; i <= this.totalPages; i++){
             this.pages.push(i);
              }
  //               console.log(this.viewBlockedusers);
             },
            error => {
                 this.snack.ErrorSnack('You are unauthorized!!..');
            }
         );
     }
    public init() {
        this.userModel = {
            UserName : undefined,
            StartDate :"",
            EndDate : ""


        };
        window.scrollTo(0,20);
        // this.sort_type = 1;
        // this.isAscending = [];
        this.GetUserProfile(this.currentPage1 - 1, this.size1)
        this.currentPage = 1;
        // this.userModel = <ViewModels.IUserViewModel>{};
        this.userList = [];
        this.viewBlockedusers = []
        this.ListBlockedusers(this.currentPage - 1, this.size);
    }

    previousPage(){
        if(this.currentPage > 1){
            this.currentPage -= 1;
            this.ListBlockedusers((this.currentPage - 1),this.size)
              
        }
    }
    
    nextPage(){
        if(this.currentPage < this.totalPages){
            this.ListBlockedusers(this.currentPage,this.size);
            this.currentPage += 1;  
        }
    }
    
    selectPage(PageNumber) {
        this.currentPage = PageNumber;
        this.ListBlockedusers( (this.currentPage - 1),this.size);
        
    }

    previousPage1(){
        if(this.currentPage1 > 1){
            this.currentPage1 -= 1;
            this.GetUserProfile((this.currentPage1 - 1),this.size1)
              
        }
    }
    
    nextPage1(){
        if(this.currentPage1 < this.totalPages1){
            this.GetUserProfile(this.currentPage1,this.size1);
            this.currentPage1 += 1;  
        }
    }
    
    selectPage1(PageNumber) {
        this.currentPage1 = PageNumber;
        this.GetUserProfile( (this.currentPage1 - 1),this.size1);
        
    }
    ViewUserDetail(user)
    {
        this.showList = false;
        this.showUserProfileDetail = user;
        console.log(user);
    }
    ViewBlockUserDetail(user)
    {
        this.showBlockList = false;
        this.showUserProfileDetail = user;
        console.log(user);

    }
    GoBackUser()
    {
        this.showList = true;
    }
    GoBackBlockUser()
    {
        this.showBlockList = true;
    }
    public tableSorting() {
        if(this.sort_type == 1) {
            this.sort_type = -1;
            this.GetUserProfile(this.currentPage1 - 1, this.size1);

        }
        else {
            this.sort_type = 1;
            this.GetUserProfile(this.currentPage1 - 1, this.size1);
        }
    }
}
