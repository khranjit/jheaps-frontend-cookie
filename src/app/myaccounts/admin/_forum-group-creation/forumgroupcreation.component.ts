import { Component} from '@angular/core';
import { UserService } from '../../../services/user.service';
import { ForumService } from '../../../services/forum.service';
import { NotifyService } from '../../../services/notify.service';
import { ActivatedRoute, Router } from '@angular/router';

declare var jQuery: any;

@Component({ 
  selector: 'createforumgroup',
  templateUrl: './forumgroupcreation.component.html',
  providers: [UserService, ForumService, NotifyService],
})

export class ForumGroupComponent{
      public forumGroupModel: ViewModels.IForumGroupTitleViewModel;
      errorMessage : any; 
      viewGroupForm  : boolean; 
      updateButton : boolean;
      getGroupNameList=[];
      GroupforEdit={};
      deleteGroup={};
      showSection = false;
      public pages: any[];
    totalPages = 0;
    currentPage = 1;
    from = 0;
    size = 10;
    ShowUserRole : any;
    ShowUserProfile : any;
    total = 0;
    
     constructor(private userService: UserService, private forumService: ForumService,private router: Router, 
        private toaster: NotifyService) {
            this.userService.showProfile().then(
                response => {
                    this.ShowUserProfile = JSON.parse(response); 
                    this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                        console.log('type of user'+this.ShowUserRole)
                        if(this.ShowUserRole== 'masteradmin'||this.ShowUserRole== 'admin'){
                            this.forumGroupModel=<ViewModels.IForumGroupTitleViewModel>{};
                            this.viewGroupForm=false;
                            this.updateButton=false;
                            this.init();
                        }
                        else{
                            this.router.navigate(['user/home']);
                        }
                }
               );
       
     }
    
    init(){
        window.scrollTo(0,20);
        this.from =0;
        this.currentPage = 1;
     this.getForumData(this.currentPage -1, this.size);
        
    }
    
    getForumData(from, size)
    {
        this.forumService.viewForumGroup(from,size).then(
            response => {
             //   console.log('getGroupNameList response',response);
               

                var res = JSON.parse(response);
                console.log(res);
                this.getGroupNameList = res.Items;
                this.pages = [] 
             this.totalPages = res.TotalPages;

            for(let i = 1; i <= this.totalPages; i++){
            this.pages.push(i);
            }
            },
            error => this.errorMessage = <any>error);
    }

    previousPage(){
        if(this.currentPage > 1){
            this.getForumData( (this.currentPage - 1), this.size ); 
            this.currentPage -= 1;  
        }
    }
    
    nextPage(){
        if(this.currentPage < this.totalPages){
            this.getForumData(this.currentPage, this.size);
            this.currentPage += 1;  
        }
    }
    
    selectPage(PageNumber) {
        this.currentPage = PageNumber;
        this.getForumData( (this.currentPage - 1), this.size );
        
    }

    addGroup(){
        //console.log("this.forumGroupModel",this.forumGroupModel);
        this.forumService.createForumGroup(this.forumGroupModel).then(
            response => {
               // console.log('response',response);
                this.init();
                this.viewGroupForm=false;
                this.toaster.SuccessSnack( "You Successfully Add a Group");
                this.forumGroupModel=<any>{};
            },
            error => this.errorMessage = <any>error);
    }
    
    edit(group){
        this.GroupforEdit=group._id;
        this.forumGroupModel=group; 
        this.viewGroupForm=true;
        this.updateButton=true;
     }
    
    UpdateGroup(){
    //console.log("Updated:", this.forumGroupModel);
        this.forumService.updateForumGroup(this.GroupforEdit,this.forumGroupModel).then(
            response => {
                //console.log('response',response);
                this.init();
                this.viewGroupForm=false;
                this.updateButton=false;
                this.toaster.SuccessSnack( "You Successfully Update a Group");
                this.forumGroupModel=<any>{};
            },
            error => this.errorMessage = <any>error);
    }
    
    confirmDelete(group){
        this.deleteGroup=group;
        jQuery("#confirmDeleteModal").modal('show');
    }
    
    deletefunc(){
        //console.log('this.deleteGroup',this.deleteGroup);
        this.forumService.deleteForumGroup(this.deleteGroup).then(
            response => {
                //console.log('response',response);
                this.init();
                this.viewGroupForm=false;
                this.forumGroupModel=<any>{};
                this.toaster.SuccessSnack("Deleted Successfully");
                jQuery("#confirmDeleteModal").modal('hide');
            },
            error => this.errorMessage = <any>error);   
    }
}