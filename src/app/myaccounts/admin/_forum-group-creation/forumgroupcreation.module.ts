import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ForumGroupComponent } from './forumgroupcreation.component';
import { SharedModule } from '../../../shared/shared.module';
import { MatInputModule } from '@angular/material';
  
  
const forumgroup: Routes = [
    {path: '', component:ForumGroupComponent},
]
@NgModule({
  imports: [
    SharedModule,
    MatInputModule,
    RouterModule.forChild(forumgroup)
  ],
  declarations: [
    ForumGroupComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ForumGroupModule { }