import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MegaMenuChooseComponent } from './mega-menu-choose.component';
import { SharedModule } from '../../../shared/shared.module';
  
  
const megamenu: Routes = [
    {path: '', component:MegaMenuChooseComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(megamenu)
  ],
  declarations: [
    MegaMenuChooseComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class MegaMenuModule { }