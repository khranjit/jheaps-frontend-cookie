import { Component } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { CategoryService } from '../../../services/category.service';
import { NotifyService } from '../../../services/notify.service';
import { UserService } from '../../../services/user.service';

@Component({ 
  
  selector: 'mega-menu-choose',
  providers: [CategoryService,NotifyService,UserService],
  templateUrl: "mega-menu-choose.component.html"
})

export class MegaMenuChooseComponent {
    
      public categoryModel : ViewModels.ICategoryViewModel;
      public productCategoryModel : ViewModels.IProductCategoryViewModel;
      showCategoryList : any;
      categories : any;
      ShowUserRole : any;
      ShowUserProfile : any;
      categoryLevel : any;
      level : any;
      categoryNames : any;
      parentCategory : any;
      errorMessage : any;
      categoryId : any;
      categoryName : any;
      categoryValue : any;  
      selectedCategory : any;
      parentCategoryId : any;
      parentCategoryNames : any;
      value : any;
      megaChooseValue :any;
     
    constructor(private userService: UserService,private router: Router, private activatedRouter: ActivatedRoute, private categoryService: CategoryService,
     private toasterPopService: NotifyService ) {
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response); 
                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                    console.log('type of user'+this.ShowUserRole)
                    // if(this.ShowUserRole== 'masteradmin'||this.ShowUserRole== 'admin'){
                        this.Init();
                        this.category();
                    // }
                    // else{
                    //     this.router.navigate(['user/home']);
                    // }
            }
           );
       
    }
 
    public Init() {
        window.scrollTo(0,20);
        this.categoryModel = <ViewModels.ICategoryViewModel>{};
        this.productCategoryModel = <ViewModels.IProductCategoryViewModel>{};
        this.categoryNames = [];
        this.categoryLevel = [];
        this.parentCategory = [];
        this.parentCategoryNames = [];
        this.selectedCategory = [];
        //this.homePage = [];
        this.megaChooseValue=[]
    }
    
    public selectHomeCategory() {
        //console.log("click",)
        for(let i=0;i<this.parentCategoryNames.length;i++){
            for(let j=0;j<this.selectedCategory.length;j++){
                if(this.parentCategoryNames[i]['_id']==this.selectedCategory[j]['Id']){
                    this.parentCategoryNames[i]['HomePage']==this.selectedCategory[j]['Value']
                }
            }
        }
        var homePage = {};
        homePage['HomePage']=[];
        for(let i=0;i<this.parentCategoryNames.length;i++){
           if(this.parentCategoryNames[i]['HomePage']!=0){
            this.megaChooseValue={};
            this.megaChooseValue['Id']=this.parentCategoryNames[i]['_id'];
            this.megaChooseValue['Value']=this.parentCategoryNames[i]['HomePage'];
            homePage['HomePage'].push(this.megaChooseValue);
           }   
        }      
        //console.log("this.value12",homePage);
        this.categoryService.selectCategory(homePage).then(
            response => {
                //console.log(response);
                if (response == 200) { this.category(); }
                this.toasterPopService.SuccessSnack('Selected parent category will be displayed in home page!!');
                //this.router.navigate(['/home']);        
             },
            error =>{
                this.toasterPopService.ErrorSnack('There is an error.Try again..');
            });
            
       } 
    
    public category() {
        this.parentCategoryNames = [];
        this.categoryService.showCategories()
            .then(
            response => {
                this.showCategoryList = JSON.parse(response);
                //console.log("parent",this.showCategoryList);
                let categories = this.showCategoryList;
                for(let j=0; j < categories.length;j++){
                      this.parentCategoryNames.push(this.showCategoryList[j].Parent);
                  }
            },
            error => this.errorMessage = <any>error);
       } 
    
//    public Category() {
//       var CategoryJson = [];
//        this.categoryService.showCategories()
//            .then(
//            response => {
//                  this.ShowCategoryList = JSON.parse(response); 
//                  let priority = this.ShowCategoryList;
//                  for(let j=0; j < priority.length;j++){
//                      this.parentCategoryNames.push(this.ShowCategoryList[j].Parent.Name);
//                      //console.log(this.parentCategoryNames);
//                      this.CategoryNames.push(this.ShowCategoryList[j].Parent.HomePage);
//                      //console.log(this.CategoryNames);
//                      if(this.ShowCategoryList[j].Parent.HomePage > 0){
//                          this.CategoriesHomePage.push(this.ShowCategoryList[j].Parent.Name);
//                          //console.log(this.CategoriesHomePage);
//                    }
//                }//console.log(this.ParentCategory);
//            },
//            error => this.errorMessage = <any>error);
//       }
    
    
    public checkBox(event,category){
        //var home = [];
        //this.home = this.homePage;
        if(event > 7)
        {
            event = 0
        }
        if(event !=0)
        {
            var catIndex = this.parentCategoryNames.findIndex(x => x.HomePage == event);
            if(catIndex != -1)
            {
                this.parentCategoryNames.find( x=> x.HomePage == event).HomePage = 0;
                var selIndex = this.selectedCategory.findIndex(x=> x.Value == event);
                if(selIndex != -1)
                {
                    this.selectedCategory.splice(selIndex,1);
                }
            }

        }
        if(event == null)
        category.HomePage = 0;
        else
        category.HomePage = event;
        this.selectedCategory.push({"Id" : category._id,"Value" : category.HomePage  });
    //    if(this.selectedCategory.length < 7){
    //         if(this.selectedCategory.indexOf(id) < 0){
                
    //         }
    //         //console.log("select",this.selectedCategory)
    //     }       
    }
    
 } 