import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListZoneComponent } from './zone.component';
import { AddZoneComponent } from './_add-zone/add-zone.component';
import { SharedModule } from '../../../shared/shared.module';
  
  
const zone: Routes = [
    {path: '',redirectTo:'view',pathMatch:'full'},
    {path: 'view', component:ListZoneComponent},
    {path: 'add', component:AddZoneComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(zone)
  ],
  declarations: [
    ListZoneComponent,
    AddZoneComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ZoneModule { }