import { Component, Inject, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ZoneService } from '../../../../services/zone.service';
import { NotifyService } from '../../../../services/notify.service';
import { UtilService } from '../../../../services/util.service';

declare var jQuery: any;

@Component({
    
    selector: 'add-zone',
    providers: [ZoneService, NotifyService],
    templateUrl: './add-zone.component.html'
})

export class AddZoneComponent {
    allCountries: any = [];
    allCountriesList:any =[];
    dataAvailable = false;
    errorMessage;
    saveButton =true;
    updateButton = false;
    addZone = <any>{};
    deletableContent:any;
    deleteModel = false;
    deleteModelBody:any;
    
    ZoneType: any = [
        {
            id:1,
            Type:"Intra City",
            Selected:false
        },
        {
            id:2,
            Type:"Intra Region",
            Selected:false
        },
        {
            id:3,
            Type:"Inter Metro",
            Selected:false
        },
        {
            id:4,
            Type:"Rest of India",
            Selected:false
        },
        {
            id:5,
            Type:"Special Locations & NE & J&K",
            Selected:false
        },
        {
            id:6,
            Type:"All Over India",
            Selected:false
        }
    ];

    constructor(private route: ActivatedRoute, private router: Router, private toasterService: NotifyService,
        private zoneService: ZoneService, private utilService: UtilService) {
        this.Init();
    }

    public Init() {
        
        window.scrollTo(0,20);
        this.route.queryParams.subscribe(params => {
            if (params['zoneId']) {
                console.log(params['zoneId'])
                this.zoneService.GetZoneById(params['zoneId'])
                .then(result => {
                    var DataList=JSON.parse(result);
                    this.addZone=DataList;
                    this.saveButton = false;
                    this.updateButton = true;
                    this.getAllCountries();
                });
            }
            else{
                this.addZone.Zone =[];
                this.addZones();
                this.getAllCountries();
            }
            
        });
    }

    CountryOptionChange(event){
        console.log(event);
    }

    public countryChanged(event) {
        this.dataAvailable = false;
        this.zoneService.CheckDelete((this.addZone.Country)? this.addZone.Country : event.target.value).then(res =>{
            var count = JSON.parse(res).count;
            if(count == 0)
            {
              this.addZone.Country = event.target.value;
              this.dataAvailable = true;
            }
            else
            {
                this.toasterService.ErrorSnack( count + ' shipping profiles use this Country');
                //this.Init();
                this.dataAvailable = true;
            }
        })
        //this.addZone.Country = event;
    }
    // public zonetypeChanged(event) {
    //     this.ZoneType[event-1].Selected=true;
    // }
    public getAllCountries() {
        this.utilService.getCountriesList().then(countries => {
            this.allCountries = JSON.parse(countries);
            this.allCountries.forEach(ele => {
                this.allCountriesList.push(ele.name);
                this.dataAvailable = true;
            });          
        },
            error => this.errorMessage = <any>error);
    }

    confirmDeleteZone(zone, country, index) {
        this.deletableContent = index;
        this.deleteModel = false;
        this.deleteModelBody = 0;
        if(zone.Type == "")
        {
            this.deleteModel = false;
            jQuery("#confirmDeleteModal").modal('show');
            return;
        }
        this.zoneService.CheckDeleteType(zone.Type, country).then(res =>{
          var count = JSON.parse(res).count;
          if(count == 0)
          {
            this.deleteModel = false;
            this.deleteModelBody = 0;
            
          }
          else
          {
            this.deleteModel = true;
            this.deleteModelBody = count;
            
          }

          jQuery("#confirmDeleteModal").modal('show');
        }, err=>{});
        
    }

    DeleteZone() {
         //var index = this.addZone.Zone.findIndex(x=> x.Type == this.deletableContent);
         this.addZone.Zone.splice(this.deletableContent,1);
         jQuery("#confirmDeleteModal").modal('hide');
    }

    // public Generate()
    // {
    //     this.GenData=true;
    //     this.addZone.Zone=[];
    //     for(let i=1; i<=this.Nzonevalue; i++)
    //     {
    //         var data = {
    //             Name :""
    //         };
    //         this.addZone.Zone.push(data);
    //     }
    //     this.Zones=this.addZone.Zone;
    // }
    public save()
    {
        this.zoneService.CheckCountry(this.addZone)
        .then(result => {
            var ResData = JSON.parse(result);
            if(ResData.TotalItems==0)
            {
                this.zoneService.SaveZone(this.addZone)
                .then(result => {
                this.toasterService.SuccessSnack( 'Zone Created Successfully!!');
                //this.Init();
                this.router.navigate(['/myaccounts/admin/zone']);
                });
            }
            else
            {
                this.toasterService.ErrorSnack('Country Already Exist!!');
            }
        });
    }

    public update()
    {
        this.zoneService.UpdateZone(this.addZone)
        .then(result => {
        this.toasterService.SuccessSnack( 'Update Successfully Done!!');
        this.router.navigate(['/myaccounts/admin/zone']);
        //this.Init();
        });
    }

    addZones()
    {
    var model = {
        Name: "",
        Type: ""
    }

    this.addZone.Zone.push(model);
    }

    deleteZone(Zones, i)
    {
        Zones.splice(i,1);
    }
    goBack() {
        this.router.navigate(['/myaccounts/admin/zone']);
    }

}