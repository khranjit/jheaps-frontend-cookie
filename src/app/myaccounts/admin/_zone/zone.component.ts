import { Component, ChangeDetectionStrategy, ViewEncapsulation, Inject } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ZoneService } from '../../../services/zone.service';
import { DomainService } from '../../../services/domain.service';
import { UserService } from '../../../services/user.service';

declare var jQuery: any;

@Component({
    
    selector: 'zone',
    providers: [ZoneService, DomainService,UserService],
    templateUrl: './zone.component.html'
})
export class ListZoneComponent {
    public Zones = [];
    public ZoneList: any;
    public errorMessage: string;
    public deletableContent: any;
    deleteModel = false;
    deleteModelBody:any;
    totalPages = 1;
    pages: any[];
    currentPage = 1;
    size = 10;
    initialPage=0;
    ShowUserRole : any;
    ShowUserProfile : any;

    constructor(private userService: UserService,private route: ActivatedRoute, private router: Router, 
    private zoneService: ZoneService) {
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response); 
                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                    console.log('type of user'+this.ShowUserRole)
                    // if(this.ShowUserRole== 'masteradmin'||this.ShowUserRole== 'admin'){
                     this.Init(); 
                    // }
                    // else{
                    //     this.router.navigate(['']);
                    // }
            }
           );
    }


    Init() {
        window.scrollTo(0,20);
        this.pages = [];
        this.zoneService.GetAllZone(this.initialPage,this.size)
                        .then(
                        zone => {
                this.ZoneList = JSON.parse(zone);
                this.Zones = this.ZoneList.Items;
                this.totalPages = this.ZoneList.TotalPages;
                for(let i = 1; i <= this.totalPages; i++)
                {
                this.pages.push(i);
                console.log(i);
                }
                console.log(this.pages);

            },
            error => this.errorMessage = <any>error);
    }
    edit(zone) {
        // let navimodel: NavigationExtras = {
        //     queryParams: { 'Id': Id }
        // }
        
            let navimodel: NavigationExtras = {
                queryParams: { 'zoneId': zone._id }
            }
            this.router.navigate(['/myaccounts/admin/zone/add'], navimodel);
      //      console.log(product)
        
        
      //  console.log(Id);
    }
    confirmDelete(zone) {
        this.deletableContent = zone._id;
        this.deleteModel = false;
        this.deleteModelBody = 0;
        this.zoneService.CheckDelete(zone.Country).then(res =>{
          var count = JSON.parse(res).count;
          if(count == 0)
          {
            this.deleteModel = false;
            this.deleteModelBody = 0;
            
          }
          else
          {
            this.deleteModel = true;
            this.deleteModelBody = count;
            
          }

          jQuery("#confirmDeleteModal").modal('show');
        }, err=>{});
        
    }

    Delete() {
        this.zoneService.DeleteZone(this.deletableContent)
            .then(
            deleted => {
                this.Init();
            },
            error => this.errorMessage = <any>error);
         jQuery("#confirmDeleteModal").modal('hide');
    }

    selectPage(PageNumber) {
        this.currentPage = PageNumber;
        // this.searchProducts(this.searchTerm, ((this.currentPage - 1)*this.size), this.size);
        this.updateZones(this.currentPage, this.size);
      }
    previousPage()
    {
        if(this.currentPage > 1){
                this.updateZones(this.currentPage -1, this.size);
                this.currentPage -=1;   
        }
    }

    nextPage()
    {
        if(this.currentPage < this.totalPages){
                this.updateZones(this.currentPage + 1, this.size);
                this.currentPage +=1;

        }
    }
    updateZones(pageNum, size)
    {
          console.log("Page Num " + pageNum);
          console.log("Size " + size);
          console.log("Current Page" + this.currentPage);

          this.zoneService.GetAllZone(pageNum-1,this.size).then(res=>{
              var data = JSON.parse(res).Items;
              this.Zones = data;
              this.initialPage=pageNum-1;
          }, err=>{
              this.errorMessage = <any>err;
          });
    }

    addNew() {
        this.router.navigate(['/myaccounts/admin/zone/add']);
    }
}