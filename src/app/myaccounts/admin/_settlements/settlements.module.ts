import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { SettlementComponent } from './settlements.component';
import {DropdownModule} from 'primeng/dropdown';


  
  
const settlements: Routes = [
  {path: '', component:SettlementComponent},
]
@NgModule({
  imports: [
    SharedModule,
    DropdownModule,
    RouterModule.forChild(settlements)
  ],
  declarations: [
    SettlementComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class SettlementModule { }