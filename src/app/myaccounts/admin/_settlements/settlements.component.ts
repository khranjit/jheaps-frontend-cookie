import { Component,ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { DatePipe } from '@angular/common';

import { UserService } from '../../../services/user.service';
import { ReportsService } from '../../../services/reports.service';
import { Router} from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({ 
  
  selector: 'settlement',
  providers:[UserService,ReportsService,DatePipe],
  templateUrl: './settlements.component.html'
})
export class SettlementComponent{
  ShowUserRole : any;
     ShowUserProfile : any;
     settlentDetail=<any>{};
     settlementList = [{ "label": "Select a Settlement" ,"value":""}];

  constructor(private loader:NgxSpinnerService,private userService: UserService,private report:ReportsService,private datePipe: DatePipe){
    this.settlentDetail.Id = '';

    this.userService.showProfile().then(
      response => {
          this.ShowUserProfile = JSON.parse(response); 
          this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
              console.log('type of user'+this.ShowUserRole)
      }
      
     );
     this.report.fetchSettlementReports().then(res=>{
        var result = JSON.parse(res);
        result.forEach(element => {
           let obj = { "label": element.SettlementId + ',' +this.datePipe.transform(element.SettlementDate, 'dd-MM-yy'), "value": element.SettlementId };
            this.settlementList.push(obj)
        });

    })

  }
  onGroupSelect(value){
    this.settlentDetail.Id = value;
  }
  DownloadSettlementReport(){
    var model = {
      SettlementId : this.settlentDetail.Id
    }
    this.loader.show();
    this.report.fetcholdsettlementReports(model).then(res=>{

      var result = JSON.parse(res); 
        if(result.ImageData){
        this.loader.hide();
        window.location.href=result.ImageData;
        }else{
        
        }
    });
  }
  GenerateSettlementReport(){
    this.loader.show();
      // this.report.downloadSettlementReport().then(res=>{
      //   var result = JSON.parse(res);    
      //   if(result.ImageData){
      //       this.loader.hide();
      //       window.location.href=result.ImageData;

      //       // window.open(result.ImageData);
           
      //   }else{

      //   }
      // })
  }
}