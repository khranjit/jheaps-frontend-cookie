import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminCriteriaComponent } from './admin-criteria.component';
import { SharedModule } from '../../../shared/shared.module';
import { Select2Module } from '../../../../assets/ng2-select2';

  
const admincriteria: Routes = [
    {path: '', component:AdminCriteriaComponent},
]
@NgModule({
  imports: [
    SharedModule,
    Select2Module,
    RouterModule.forChild(admincriteria)
  ],
  declarations: [
    AdminCriteriaComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class AdminCriteriaModule { }