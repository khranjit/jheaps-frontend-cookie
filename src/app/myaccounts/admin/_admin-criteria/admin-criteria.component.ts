import { Component } from '@angular/core';
import { AdminCriteriaService } from '../../../services/admin-criteria.service';
import { CategoryService } from '../../../services/category.service';
import { AttributeService } from '../../../services/attribute.service';
import { NotifyService } from '../../../services/notify.service';
import { ShopService } from '../../../services/shop.service';
import { UtilService } from '../../../services/util.service';
import { ElasticService } from '../../../services/elastic.service';
import { ElasticHelperService } from '../../../services/elastic-helper.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../services/user.service';

declare var jQuery : any;

@Component({
    
    providers: [AdminCriteriaService,UserService, NotifyService, CategoryService, AttributeService, UtilService, ShopService, ElasticService, ElasticHelperService],
    selector: 'admin-criteria',
    templateUrl: './admin-criteria.component.html'
})
export class AdminCriteriaComponent {
    private value:any = ['India'];
    SelectedSubCategoriesList:any =[];
    singleCategory:any;
    ShowUserRole : any;
    ShowUserProfile : any;
    ShipsFrom:any = [];
    ShopsReady = false;
    public errorMessage : any;
    public discountType = 'Amount';
    public adminCriteriaList = [];
    public attributesList = [];
    public parentCategoriesList = [];
    public subCategoriesList = [];
    public countryList = [];
    public shopList = [];
    Shop = [];
    Categories = [];
    SubCategories =[];
    public columnList  = [];
    public genderList  =   ["Male", "Female", "UniSex"];
    public adminCriteriaShopByViewModel = <ViewModels.IAdminShopbyModel>{};
    public isUpdate = false;
    addCriteria = false;
    showCreation = false;
    getDeleteId: string;
    public pages: any[];
    totalPages = 0;
    currentPage = 1;
    from = 0;
    size = 10;
    total = 0;
    bannerLinkCount: number;
    bannerQuote:any;
    //shopListReady = false;
    
    constructor(private activatedRoute: ActivatedRoute, private router: Router,
                private adminCriteriaService: AdminCriteriaService, 
                private userService: UserService,
                private snack: NotifyService,
                private categoryService: CategoryService,
                private attributeService: AttributeService,
                private utilService: UtilService,
                private shopService: ShopService,
                private elasticService: ElasticService,
                private elasticHelperService: ElasticHelperService) {

                    this.userService.showProfile().then(
                        response => {
                            this.ShowUserProfile = JSON.parse(response); 
                            this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                                console.log('type of user'+this.ShowUserRole)
                                if(this.ShowUserRole== 'masteradmin'||this.ShowUserRole== 'admin'){
                                    this.init();
                                    // this.initJqueryFunctions();
                                     this.adminCriteriaShopByViewModel.Criteria = <any>{};
                                     this.adminCriteriaShopByViewModel.Criteria.Price = <any>{};
                                     this.adminCriteriaShopByViewModel.Criteria.Discount = <any>{};
                                }
                                else{
                                    this.router.navigate(['/home']);
                                }
                        }
                       );
        
    }
    
    SingleCategoryChanged()
    {
        if(this.singleCategory != "")
        {
            //console.log(singleCategory);
            this.adminCriteriaShopByViewModel.Criteria.Categories = []
            this.adminCriteriaShopByViewModel.Criteria.Categories.push(this.singleCategory);
            this.SelectedSubCategoriesList = this.subCategoriesList.filter(ele => ele.ParentCategory == this.singleCategory);
        }
    }
      
    public changeDiscountType(){
        this.adminCriteriaShopByViewModel.Criteria.Discount = <any>{};
    }
    
    public init() {
        this.bannerLinkCount = 0;
        window.scrollTo(0,20);
        this.from = 0;
        this.currentPage = 1;
        this.getAdminCriteriaData(this.currentPage -1, this.size);
        this.getAttributeList();
        this.getCategoryList();
        this.getCountryList();
        this.getShopList();
    }
    
    public getAttributeList(){
        this.attributeService.getAll().then(attributes => {
            this.attributesList = JSON.parse(attributes).Items;
            //console.log(this.attributesList)
            this.columnList = ["SoldInLastThreeSixtyFiveDays", "Condition", "Karat", "Size", "SellerRating", "Categories", "Shop"];
            this.attributesList.forEach(attribute => {
                this.columnList.push("Attributes-"+attribute.Name);
            });
            //console.log(this.columnList);
            //console.log("Attribute List ", this.attributesList);        
        }, error => this.errorMessage = <any>error);
    }
    
    public getCategoryList(){
        this.categoryService.getCategoryInTreeView().then(categories => {
            this.parentCategoriesList = [];
            this.subCategoriesList = [];
            //console.log(JSON.stringify(JSON.parse(categories)));
            JSON.parse(categories).forEach(category => {
                category.Parent.id = category.Parent._id;
                category.Parent.text = category.Parent.Name;
                this.parentCategoriesList.push(category.Parent);
                category.Children.forEach(childrenCategory => {
                    childrenCategory.id = childrenCategory._id;
                    childrenCategory.text = childrenCategory.Name;
                    this.subCategoriesList.push(childrenCategory);
                });
            });            
            //console.log("Category List ", this.parentCategoriesList); 
            console.log("Sub Category List ", this.subCategoriesList);        
        }, error => this.errorMessage = <any>error);

    }
    
    public getCountryList(){
        this.countryList = this.utilService.getCountries();
     //   console.log(this.countryList);
    }
    
    public getShopList(){
        this.shopService.getShops().then(shops => {
       //     console.log("shops", shops);
            this.shopList = JSON.parse(shops);
            this.shopList.forEach(shop => {
                shop.id = shop._id;
                shop.text = shop.Name;
                
            });
            this.ShopsReady =true;
            //this.shopListReady = true;
         //   console.log("Shops List ", this.shopList);        
        }, error =>{
                this.snack.ErrorSnack('You are unauthorized !!..');
                }
         );
    }
    
    // public initJqueryFunctions() {
    //     var self = this;
    //     setTimeout(function(){
    //        jQuery("#adminCriteriaPriceRange").ionRangeSlider({
    //             min: 0,
    //             max: 10000,
    //             from: 1000,
    //             to: 9000,
    //             type: 'double',
    //             prefix: "$",
    //             grid: true,
    //             grid_num: 10,
    //             onFinish : function (data) {
    //                 self.setPrice(data);
    //             }
    //         }); 
    //     },1000);
    // }
    
      
    editAdminCriteria(adminCriteria){
        //console.log(adminCriteria);
        this.adminCriteriaShopByViewModel = adminCriteria;
        this.Shop = adminCriteria.Criteria.Shop;
        if(this.adminCriteriaShopByViewModel.Criteria.TypeOfCategory == 'Multiple')
        {
            this.Categories = this.adminCriteriaShopByViewModel.Criteria.Categories? this.adminCriteriaShopByViewModel.Criteria.Categories:"";
        }
        else 
        {
            this.singleCategory =   this.adminCriteriaShopByViewModel.Criteria.Categories? this.adminCriteriaShopByViewModel.Criteria.Categories[0]:""; 
            this.SelectedSubCategoriesList = this.subCategoriesList.filter(ele => ele.ParentCategory == this.singleCategory);
        }
        this.SubCategories = this.adminCriteriaShopByViewModel.Criteria.SubCategories;


        // this.ShipsFrom = this.adminCriteriaShopByViewModel.Criteria.ShipsFrom;
        this.isUpdate = true;
       
        //this.addCriteria=true;
    } 
    
    setPrice(data){
        this.adminCriteriaShopByViewModel.Criteria.Price = {
            "From" : data.from,
            "To" : data.to  
        };
       // console.log(this.adminCriteriaShopByViewModel);
    }
    
    public countryValueChanged(value:any):void {
        this.adminCriteriaShopByViewModel.Criteria.Country = value.value
        console.log('this.adminCriteriaShopByViewModel: ', this.adminCriteriaShopByViewModel);
    }
    
    // public shipsFromValueChanged(value:any):void {
    //     this.adminCriteriaShopByViewModel.Criteria.ShipsFrom = value.value
    //     console.log('this.adminCriteriaShopByViewModel: ', this.adminCriteriaShopByViewModel);
    // }
    
    public shipsToValueChanged(value:any):void {
        this.adminCriteriaShopByViewModel.Criteria.ShipsTo = value.value
        //console.log('this.adminCriteriaShopByViewModel: ', this.adminCriteriaShopByViewModel);
    }
    
    public genderValueChanged(value:any):void {
        this.adminCriteriaShopByViewModel.Criteria.Gender = value.value
        //console.log('this.adminCriteriaShopByViewModel: ', this.adminCriteriaShopByViewModel);
    }
    
    public categoriesValueChanged(value:any):void {
        if(value.value){
            this.adminCriteriaShopByViewModel.Criteria.Categories = value.value
           console.log('this.adminCriteriaShopByViewModel: ', this.adminCriteriaShopByViewModel);
        }
    }
    
    public subCategoriesValueChanged(value:any):void {
        if(value.value){
            this.adminCriteriaShopByViewModel.Criteria.SubCategories = value.value
            console.log('this.adminCriteriaShopByViewModel: ', this.adminCriteriaShopByViewModel);
        }
    }
    
    public shopValueChanged(value:any):void {
        if(value.value){
            this.adminCriteriaShopByViewModel.Criteria.Shop = value.value
            console.log('this.adminCriteriaShopByViewModel: ', this.adminCriteriaShopByViewModel);
        }
        else
        this.adminCriteriaShopByViewModel.Criteria.Shop=[];

    }
    
    public attributeValueChanged(attribute:any, value:any):void{
        this.adminCriteriaShopByViewModel.Criteria[attribute.Name] = value.value
       // console.log('this.adminCriteriaShopByViewModel: ', this.adminCriteriaShopByViewModel);
    }
    
    public mandatoryFieldsValueChanged(value:any):void {
        if(value.value){
            this.adminCriteriaShopByViewModel.Criteria.MandatoryFields = value.value
         //   console.log('this.adminCriteriaShopByViewModel: ', this.adminCriteriaShopByViewModel);
        }
    }
    
    // public generateQuery(){
    //     let query = this.elasticHelperService.getQueryForAdminCriteria(this.adminCriteriaShopByViewModel);
    //     this.adminCriteriaShopByViewModel.Criteria.Query = JSON.stringify(query);
    //     this.elasticService.getRecordsByQuery(query, 0, 1).then(queryResponse =>{
    //         if(queryResponse){
    //         }
    //     }, error => this.errorMessage = <any>error);
    // }
    ClearModel(){
        // this.adminCriteriaShopByViewModel.Name='';
        // this.adminCriteriaShopByViewModel.Criteria.Price.From='';
        // this.adminCriteriaShopByViewModel.Criteria.Price.To='';
        // //this.adminCriteriaShopByViewModel.Show='';
        // this.adminCriteriaShopByViewModel.Criteria.Condition='';
        // this.adminCriteriaShopByViewModel.Criteria.SellerRating='';
        // this.adminCriteriaShopByViewModel.Criteria.ShipsFrom='';
        this.singleCategory='';
        this.adminCriteriaShopByViewModel=<ViewModels.IAdminShopbyModel>{};
        this.adminCriteriaShopByViewModel.Criteria = <any>{};
        this.adminCriteriaShopByViewModel.Criteria.Price = <any>{};
        this.adminCriteriaShopByViewModel.Criteria.Discount = <any>{};
        this.isUpdate=false;
        
    }
    public addAdminCriteria(){
        
        //console.log("form"+this.adminCriteriaShopByViewModel);
        this.adminCriteriaService.addAdminCriteria(this.adminCriteriaShopByViewModel).then(addCriteriasResponse => {
            if(addCriteriasResponse){
                this.snack.SuccessSnack("Criteria Added Successfully")
                this.init(); 
                this.addCriteria=false; 
            }
        }, error => this.errorMessage = <any>error);
    }
    
    public updateAdminCriteria(){
       // console.log(this.adminCriteriaShopByViewModel)
        this.adminCriteriaService.updateAdminCriteria(this.adminCriteriaShopByViewModel).then(updateCriteriasResponse => {
            if(updateCriteriasResponse){
                this.snack.SuccessSnack("Criteria Updated Successfully")
                this.init();  
                this.addCriteria=false;
                this.isUpdate = false;
            }
                
        }, error => this.errorMessage = <any>error);
    }
    
    confirmDelete(id){
        this.getDeleteId=id;
        this.bannerLinkCount=0;
        this.adminCriteriaService.displayBannerLinkToCriteria(id)
            .then(
            banners => {
                console.log(banners);
                var res = JSON.parse(banners);
                if(res.result){
                    this.bannerLinkCount = 1
                    this.bannerQuote=res.link;
                }else{
                    this.bannerLinkCount = 0
                }
        //        console.log("this.productLinkCount",this.productLinkCount);
                jQuery("#deleteAdminCriteria").modal('show');
                console.log("banners", this.bannerLinkCount)
                
            },
            error => this.errorMessage = <any>error);

        //jQuery("#deleteAdminCriteria").modal('show');
    }
    
    public deleteAdminCriteria(){
        //console.log(this.adminCriteriaShopByViewModel)
        this.adminCriteriaService.deleteAdminCriteria(this.getDeleteId).then(response => {
            if(response){
                this.snack.SuccessSnack( "Criteria Deleted Successfully")
                this.init(); 
                jQuery("#deleteAdminCriteria").modal('hide');
                 this.addCriteria=false;  
            }
                
        }, error => this.errorMessage = <any>error);
    }

    previousPage(){
        if(this.currentPage > 1){
            this.currentPage -= 1; 
            this.getAdminCriteriaData( (this.currentPage - 1), this.size ); 
             
        }
    }
    
    nextPage(){
        if(this.currentPage < this.totalPages){
            this.getAdminCriteriaData( this.currentPage , this.size);
            this.currentPage += 1;  
        }
    }
    
    selectPage(PageNumber) {
        this.currentPage = PageNumber;
        this.getAdminCriteriaData( (this.currentPage - 1), this.size );
        
    }

    getAdminCriteriaData(from, size)
    {
        this.adminCriteriaService.getAdminCriteriaList(from,size).then(adminCriterias => {

            var res = JSON.parse(adminCriterias);
            this.adminCriteriaList = res.Items;

            this.pages = [] 
              this.totalPages = res.TotalPages;

             for(let i = 1; i <= this.totalPages; i++){
             this.pages.push(i);
              }
            //console.log("Admin Criteria List ", this.adminCriteriaList);        
        }, error => this.errorMessage = <any>error);
    }

}