import { Component} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HolidayService } from '../../../../services/holidays.service';
import { NotifyService } from '../../../../services/notify.service';
declare var jQuery: any;

@Component({
    selector: 'edit-holidays',
    providers: [HolidayService, NotifyService],
    templateUrl: './edit-holidays.component.html'
})



export class EditHolidaysComponent{
    dateModel = [];
    
    errorMessage:any;  
    states = [];  
    totalPages = 1;
    pages: any[];
    currentPage = 1;
    size = 10;
    deleteHolidayId:any;


      constructor(private toasterPop: NotifyService, private holidayService: HolidayService ){
          this.Init();

      }

      Init()
      {
        window.scrollTo(0,20);
        this.currentPage = 1;
        this.holidayService.GetHolidays({page:0,size:this.size}).then(res=>{
            var data = JSON.parse(res);
            this.dateModel = data.Items;
            this.totalPages = data.TotalPages;

            this.pages = [];
        for(let i = 1; i <= this.totalPages; i++)
        {
        this.pages.push(i);
        }
    }, err=>{
        this.errorMessage = <any>err;
    });
 
      }

      ngOnInit() {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        console.log("Inside ngOn Init");
        this.holidayService.GetStates().then(res=>{
          var data = JSON.parse(res);
          console.log(JSON.stringify(data));    
          this.states = data;
          console.log(JSON.stringify(this.states));
        },err=>{
            this.errorMessage = <any>err;
        });

        


        
    }

    confirmDelete(holidayId) {
      this.deleteHolidayId = holidayId;
      jQuery("#confirmDeleteModal").modal('show');
    }

    delete()
    {
        
        this.holidayService.DeleteHoliday(this.deleteHolidayId).then(res=>{
            this.toasterPop.ErrorSnack('Holiday deleted Successfully!!');
            var afterDelete = this.dateModel.filter(ele=> ele._id != this.deleteHolidayId);
            if(afterDelete.length>0)
            this.dateModel = afterDelete;
            else
            this.Init();
                }, err=>{
            this.errorMessage = <any>err;
        });
    }
    updateItem(data)
    {
        this.holidayService.UpdateHoliday(data).then(res => {
            this.toasterPop.SuccessSnack('Holiday updated Successfully!!');
        }, err =>{
            this.errorMessage = <any>err;
        });  
    }
      previousPage(){
                if(this.currentPage > 1){
                        this.updateHolidays(this.currentPage -1, this.size);
                        this.currentPage -=1;   
                }
            }
        
        nextPage(){
            if(this.currentPage < this.totalPages){
                    this.updateHolidays(this.currentPage + 1, this.size);
                    this.currentPage +=1;
        
            }
          }
        
        selectPage(PageNumber) {
            this.currentPage = PageNumber;
            // this.searchProducts(this.searchTerm, ((this.currentPage - 1)*this.size), this.size);
            this.updateHolidays(this.currentPage, this.size);
          }
        
          updateHolidays(pageNum, size)
          {
                console.log("Page Num " + pageNum);
                console.log("Size " + size);
                console.log("Current Page" + this.currentPage);

                this.holidayService.GetHolidays({page:pageNum-1,size:this.size}).then(res=>{
                    var data = JSON.parse(res).Items;
                    this.dateModel = data;
                }, err=>{
                    this.errorMessage = <any>err;
                });
          }
}