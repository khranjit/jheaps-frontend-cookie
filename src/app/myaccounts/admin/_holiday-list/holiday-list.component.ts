import { Component,ChangeDetectionStrategy, ViewEncapsulation} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../services/user.service';
declare var jQuery: any;

@Component({
    selector: 'holidays',
    providers: [UserService],
    templateUrl: './holiday-list.component.html'
})



export class HolidayListComponent{
    ShowUserRole : any;
  ShowUserProfile : any;
    constructor(private userService: UserService,private router: Router){
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response); 
                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                    console.log('type of user'+this.ShowUserRole)
                    if(this.ShowUserRole== 'masteradmin'||this.ShowUserRole== 'admin'){
                      
                    }
                    else{
                        this.router.navigate(['user/home']);
                    }
            }
           );
    }
    
   
}