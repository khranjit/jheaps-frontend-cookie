import { Component} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HolidayService } from '../../../../services/holidays.service';
import { NotifyService } from '../../../../services/notify.service';
declare var jQuery: any;

@Component({
    selector: 'create-holidays',
    providers: [HolidayService, NotifyService],
    templateUrl: './create-holidays.component.html'
})



export class CreateHolidaysComponent{
    
    selectedDates: Date[];
    dateModel: any = [];
    states = [];
    errorMessage:any;
    // totalPages = 1;
    // pages: any[];
    // currentPage = 1;
    // size = 5;
    constructor(private holidayService: HolidayService, private toasterPop: NotifyService){}

    Init()
    {
        // this.pages = [];
        // for(let i = 1; i <= this.totalPages; i++)
        // {
        // this.pages.push(i);
        // }

        window.scrollTo(0,20);
    }

    ngOnInit() {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        console.log("Inside ngOn Init");
        this.holidayService.GetStates().then(res=>{
          var data = JSON.parse(res);
          console.log(JSON.stringify(data));    
          this.states = data;
          console.log(JSON.stringify(this.states));
        },err=>{
            this.errorMessage = <any>err;
        });
    }

    SaveHolidays()
    {
        this.holidayService.GetAllHolidays({}).then(res=>{
            var dateList = JSON.parse(res);
            var newDateList = this.dateModel.filter(ele =>{
                return !dateList.some(item =>{
                    var date1 = new Date(item.Date);
                    var date2 = new Date(ele.Date);
                     
                    return (+date1=== +date2 && item.State == ele.State)
                    
                })

                
            });

            this.holidayService.SaveHolidays(newDateList).then(res=>{
                this.dateModel = [];
                this.selectedDates = [];
                this.toasterPop.SuccessSnack('Holidays saved Successfully!!');
            },err =>{
                this.errorMessage = <any>err;
                
                this.toasterPop.ErrorSnack('Error while saving holidays!!');
            });
        },
        err=>{
            this.errorMessage = <any>err;
        });
        
    }


    DateSelectionChanged()
    {
        //console.log(event);
       var tempModel = [];
       this.selectedDates.forEach(ele =>{
           var isFound = false;
           for(var i = 0 ; i< this.dateModel.length ; i++)
           {
               if(ele == this.dateModel[i].date)
               {
                   isFound = true;
                   tempModel.push(this.dateModel[i]);
                   break;
               }
           }

           if(!isFound)
           {
            var model = {
                Date:ele,
                Holiday:"",
                State :""
              }
 
            tempModel.push(model);
           }
           
       });

       this.dateModel = tempModel;
       //console.log(this.dateModel);
  }

    ConfirmDelete(id, date)
    {
        this.dateModel.splice(id,1);
        this.selectedDates = this.selectedDates.filter(ele => ele != date);
        console.log(this.selectedDates);
    }

//     previousPage(){
//         if(this.currentPage > 1){
//                 this.updateHolidays(this.currentPage -1, this.size);
//                 this.currentPage -=1;   
//         }
//     }

// nextPage(){
//     if(this.currentPage < this.totalPages){
//             this.updateHolidays(this.currentPage + 1, this.size);
//             this.currentPage +=1;

//     }
//   }

// selectPage(PageNumber) {
//     this.currentPage = PageNumber;
//     // this.searchProducts(this.searchTerm, ((this.currentPage - 1)*this.size), this.size);
//     this.updateHolidays(this.currentPage, this.size);
//   }

//   updateHolidays(pageNum, size)
//   {

//   }

}