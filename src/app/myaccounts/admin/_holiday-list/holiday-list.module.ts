import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HolidayListComponent } from './holiday-list.component';
import { CreateHolidaysComponent } from './_create-holidays/create-holidays.component';
import { EditHolidaysComponent } from './_edit-holidays/edit-holidays.component';
import { SharedModule } from '../../../shared/shared.module';
  
import {CalendarModule} from 'primeng/primeng';

const holidays: Routes = [
    {path: '',redirectTo:'list',pathMatch:'full'},
    {path:'list',component:HolidayListComponent,children:[
      {path: '',redirectTo:'edit',pathMatch:'full'},
      {path: 'add', component:CreateHolidaysComponent},
      {path: 'edit', component:EditHolidaysComponent}
    ]}
]
@NgModule({
  imports: [
    SharedModule,
    CalendarModule,
    RouterModule.forChild(holidays)
  ],
  declarations: [
    HolidayListComponent,
    CreateHolidaysComponent,
    EditHolidaysComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class HolidayListModule { }