import { Component } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { AdminCriteriaService } from '../../../services/admin-criteria.service';
import { NotifyService } from '../../../services/notify.service';
import { UtilService } from '../../../services/util.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../../../services/user.service';

declare var jQuery: any;
declare var Cropper: any;
declare var Croppie: any;

@Component({
    selector: 'upload-banner',
    providers: [AdminCriteriaService, NotifyService, UtilService, UserService],
    templateUrl: './upload-banner.component.html'
})

export class UploadBannerComponent {
    showCreation: boolean;
    public bannerModel: ViewModels.IBannerViewModel;
    public errorMessage: any;
    bannerImage: any;
    bannerNew = <any>{ "Dimensions": { "editedImage": "" } };
    bannerEdit = <any>{ "Dimensions": { "editedImage": "" } };
    showAdd: boolean;
    bannerList = [];
    adminCriteriaList = [];
    selectedImageForCropping: any;
    public imageTransitions = <any>{};
    public cropper: any;
    public croppie: any;
    public zoomRatio = 0;
    bannerName: string;
    selectBanner = <any>{};
    //loader : boolean
    tempImageBase64Banner = [];
    mode: string;
    selectedBanner: string;
    confirmDeleteBannerId: any;
    public pages: any[];
    totalPages = 0;
    currentPage = 1;
    from = 0;
    size = 5;
    total = 0;
    ShowUserRole: any;
    ShowUserProfile: any;

    constructor(private userService: UserService, private route: ActivatedRoute, private loader: NgxSpinnerService, private router: Router, private adminCriteriaService: AdminCriteriaService,
        private toasterPopService: NotifyService, private utilService: UtilService) {

        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response);
                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                console.log('type of user' + this.ShowUserRole)
                // if(this.ShowUserRole== 'masteradmin'||this.ShowUserRole== 'admin'){
                this.showAdd = true;
                this.init();

                // }
                // else{
                //     this.router.navigate(['']);
                // }
            }
        );
        //this.loader = false ;
    }

    public init() {
        this.currentPage = 1;
        this.pages = []
        window.scrollTo(0, 20);
        this.bannerModel = <ViewModels.IBannerViewModel>{};
        this.getBannerList(this.currentPage - 1, this.size);
        this.bannerNew = <any>{ "Dimensions": { "editedImage": "" } };
    }

    getBannerList(pageNumber, size) {
        this.adminCriteriaService.getBannerList(pageNumber, size).then(banners => {
            if (JSON.parse(banners).Items.length > 0) {
                this.pages = [];
                var data = JSON.parse(banners)
                this.bannerList = data.Items;
                this.totalPages = data.TotalPages;

                for (let i = 1; i <= this.totalPages; i++) {
                    this.pages.push(i);
                }
                //            console.log(this.bannerList)
            }
        }),
            error => this.errorMessage = <any>error;
        this.adminCriteriaService.getAdminCriteriaList().then(adminCriterias => {
            this.adminCriteriaList = JSON.parse(adminCriterias).Items;
        }, error => this.errorMessage = <any>error);
    }

    updateBanner(banner) {
        //  console.log("updated",this.bannerNew,this.selectedBanner)
        this.adminCriteriaService.updateBanner(this.bannerNew, this.selectedBanner).then(response => {
            this.toasterPopService.SuccessSnack('Banner Updated Successfully!!');
            this.init();
        }),
            error => this.errorMessage = <any>error;
    }

    confirmDeleteBanner(banner) {
        this.confirmDeleteBannerId = banner._id;
        jQuery("#deleteBanner").modal('show');

    }

    deleteBanner() {
        jQuery("#deleteBanner").modal('hide');
        this.adminCriteriaService.deleteBanner(this.confirmDeleteBannerId).then(response => {
            this.toasterPopService.SuccessSnack('Banner Deleted Successfully!!');
            this.init();
        },
            error => this.errorMessage = <any>error);
    }
    addBanner() {
        console.log(this.bannerNew);
        if(this.bannerNew.Dimensions.editedImage=="")
        {
        this.toasterPopService.ErrorSnack('Please choose file');
        }
        else
        {
            if (this.bannerNew.device != "default") {
                this.adminCriteriaService.addBanner(this.bannerNew).then(response => {
                    this.toasterPopService.SuccessSnack('Banner Added Successfully!!');
                    this.init();
                    this.showAdd = true;
                }),
                    error => this.errorMessage = <any>error;
            } else {
                this.toasterPopService.ErrorSnack('Select Device');
            }
        }
       
        // console.log(this.bannerNew);

    }
    async uploadBanner(value,fileName,filetype)
    {
        return await this.utilService.uploadBase64AndGetUrl(fileName, value).then(
            response => {
                if (this.bannerNew.device == "1") {
                    if(filetype == 'edited'){
                        this.bannerNew.Dimensions.editedImage = JSON.parse(response).ImageData;
                    }else{
                        this.bannerNew.BannerUrl = JSON.parse(response).ImageData;
                    }
                } else {
                    if(filetype == 'edited'){
                        this.bannerNew.Dimensions.editedImage = JSON.parse(response).ImageData;
                    }else{
                        this.bannerNew.MobileUrl = JSON.parse(response).ImageData;
                    }
                }
                this.bannerName = JSON.parse(response).Name
                this.loader.hide();
                return true;
            },
            error => {
                this.toasterPopService.ErrorSnack("Problem in uploading the image. Please try again");
            });
    }
    changeBannerImage(banner, value, e) {
        this.mode = value;
        let count = 0;
        if (this.mode == "Add") {

            if (this.bannerNew.device) {

                let files = [].slice.call(e.target.files);
                var file: File = files[0];
                let fileName = file.name.split(".")[0];
                var width = 180;
                var height = 120;
                var Event = e;
                const reader = new FileReader();
                reader.readAsDataURL(e.target.files[0]);
                reader.onload = e => {
                    const img = new Image();
                    var event:any = e;
                    img.src = event.target.result;
                    img.onload = () => {
                            const elem = document.createElement('canvas');
                            width = img.width;
                            height =  img.height;
                            elem.width = width;
                            elem.height = height;
                            const ctx = elem.getContext('2d');
                            // img.width and img.height will contain the original dimensions
                            ctx.drawImage(img, 0, 0, width, height);
                            ctx.canvas.toBlob((blob) => {
                                const filesData = new File([blob], fileName, {
                                    type: 'image/webp',
                                    lastModified: Date.now()
                                });
                                // var fileData: File = filesData[0];
                                let fileNameData = filesData.name.split(".")[0];
                                this.readThis(file);
                                var fileDatatype = 'original';
                                var i = 0;
                                this.utilService.invokeEvent.subscribe(dataValue1 => {
                                    this.loader.show();
                                    if (i == 0) {
                                        if (dataValue1 == "error") {
                                            this.loader.hide();
                                            this.toasterPopService.ErrorSnack("Problems in uploading the Image Dimension Or Size . Please try again");
                                        }
                                        else {
                                            this.uploadBanner(dataValue1,fileNameData,fileDatatype).then(res=>{
                                                var width = 180;
                                                var height = 120;
                                                const reader = new FileReader();
                                                reader.readAsDataURL(Event.target.files[0]);
                                                reader.onload = e => {
                                                    const img = new Image();
                                                    var event:any = e;
                                                    img.src = event.target.result;
                                                    img.onload = () => {
                                                            const elem = document.createElement('canvas');
                                                            width = img.width / 20;
                                                            height =  img.height / 20;
                                                            elem.width = width;
                                                            elem.height = height;
                                                            const ctx = elem.getContext('2d');
                                                            // img.width and img.height will contain the original dimensions
                                                            ctx.drawImage(img, 0, 0, width, height);
                                                            ctx.canvas.toBlob((blob) => {
                                                                const filesData = new File([blob], fileName, {
                                                                    type: 'image/jpeg',
                                                                    lastModified: Date.now()
                                                                });
                                                                // var fileData: File = filesData[0];
                                                                let fileNameData = filesData.name.split(".")[0];
                                                                this.readThis(filesData);
                                                                var fileDatatype = 'edited';
                                                                var i = 0;
                                                                this.utilService.invokeEvent.subscribe(dataValue => {
                                                                    this.loader.show();
                                                                    if (i == 0) {
                                                                        if (dataValue == "error") {
                                                                            this.loader.hide();
                                                                            this.toasterPopService.ErrorSnack("Problem1 in uploading the Image Dimension Or Size . Please try again");
                                                                        }
                                                                        else {
                                                                            this.uploadBanner(dataValue,fileNameData,fileDatatype)
                                                                        }
                                                                    }
                                                                    i++;
                                                                    });
                                                            }, 'image/jpeg', 1);
                                
                                                        },
                                                        reader.onerror = error => console.log(error);
                                                }; 
                                            })
                                        }
                                    }
                                    i++;
                                    });
                            }, 'image/jpeg', 1);

                        },
                        reader.onerror = error => console.log(error);
                };
                //
                // let filetype = 'original';
                // this.readThis(file)

                //     this.utilService.invokeEvent.subscribe(value => {
                //         this.loader.show();
                //         if (count == 0) {
                //             if (value == "error") {
                //                 this.loader.hide();
                //                 this.toasterPopService.ErrorSnack("Problem in uploading the Image Dimension Or Size . Please try again");
                //             }
                //             else {
                //                 this.uploadBanner(value,fileName,filetype).then(result=>{
                //                     var width = 180;
                //                     var height = 120;
                //                     const reader = new FileReader();
                //                     reader.readAsDataURL(e.target.files[0]);
                //                     reader.onload = e => {
                //                         const img = new Image();
                //                         var event:any = e;
                //                         img.src = event.target.result;
                //                         img.onload = () => {
                //                                 const elem = document.createElement('canvas');
                //                                 width = img.width / 20;
                //                                 height =  img.height / 20;
                //                                 elem.width = width;
                //                                 elem.height = height;
                //                                 const ctx = elem.getContext('2d');
                //                                 // img.width and img.height will contain the original dimensions
                //                                 ctx.drawImage(img, 0, 0, width, height);
                //                                 ctx.canvas.toBlob((blob) => {
                //                                     const filesData = new File([blob], fileName, {
                //                                         type: 'image/jpeg',
                //                                         lastModified: Date.now()
                //                                     });
                //                                     // var fileData: File = filesData[0];
                //                                     let fileNameData = filesData.name.split(".")[0];
                //                                     this.readThis(filesData);
                //                                     var fileDatatype = 'edited';
                //                                     var i = 0;
                //                                     this.utilService.invokeEvent.subscribe(dataValue => {
                //                                         this.loader.show();
                //                                         if (i == 0) {
                //                                             if (dataValue == "error") {
                //                                                 this.loader.hide();
                //                                                 this.toasterPopService.ErrorSnack("Problem in uploading the Image Dimension Or Size . Please try again");
                //                                             }
                //                                             else {
                //                                                 this.uploadBanner(dataValue,fileNameData,fileDatatype)
                //                                             }
                //                                         }
                //                                         i++;
                //                                         });
                //                                 }, 'image/jpeg', 1);
                    
                //                             },
                //                             reader.onerror = error => console.log(error);
                //                     };  
                //                 });
                //             }
                //         }
                //         count++;
                //     });
            } else {
                this.toasterPopService.ErrorSnack("Kindly select device");

            }
        }
        else {
            this.selectedBanner = banner._id;
            this.bannerNew.BannerUrl = banner.BannerUrl;
            var filename = banner.BannerUrl.split('/').pop();
            //  console.log("URL split , ",filename);
            let model = { "filename": filename };
            // console.log("this.getBase64fromURL Logo,",model);
            //console.log("banner.BannerURL",banner.BannerUrl);
            this.utilService.getBase64FromUrlforShopInfo(model).then(
                response => {
                    this.tempImageBase64Banner = [];
                    this.tempImageBase64Banner.push("data:image/png;base64," + JSON.parse(response).ImageDataBase64);
                    //  console.log(this.tempImageBase64Banner);
                    this.cropImage(this.tempImageBase64Banner[0]);
                });
        }
    }
      readThis(file: any){
        if (this.bannerNew.device == "1") {
            // this.utilService.convertToBase64ChangeBanner(file, 1600, 310, file.size);
            this.utilService.convertToBase64ChangeBanner(file);


        } else {
            // this.utilService.convertToBase64ChangeBanner(file, 1170, 600, file.size);
            this.utilService.convertToBase64ChangeBanner(file);

        }
    }
    EditbannerImage(banner,$event){

        if(banner.BannerUrl){
            this.bannerEdit.device = "1";
        }else{
            this.bannerEdit.device = "2";
        }
        this.bannerEdit.Id = banner._id;
        let files = [].slice.call($event.target.files);
                var file: File = files[0];
                let fileName = file.name.split(".")[0];
                let count = 0;
                if (this.bannerEdit.device == "1") {
                    this.utilService.convertToBase64ChangeBanner(file);

                } else {
                    this.utilService.convertToBase64ChangeBanner(file);
                }
                this.utilService.invokeEvent.subscribe(value => {
                    this.loader.show();
                    if (count == 0) {
                        if (value == "error") {
                            this.loader.hide();
                            this.toasterPopService.ErrorSnack("Problem in uploading the Image Dimension Or Size . Please try again");
                        }
                        else {
                            this.utilService.uploadBase64AndGetUrl(fileName, value).then(
                                response => {
                                    if (this.bannerEdit.device == "1") {
                                        this.bannerEdit.BannerUrl = JSON.parse(response).ImageData;
                                        this.bannerEdit.Dimensions.editedImage = JSON.parse(response).ImageData;
                                        
                                    } else {
                                        this.bannerEdit.MobileUrl = JSON.parse(response).ImageData;
                                        this.bannerEdit.Dimensions.editedImage = JSON.parse(response).ImageData;
                                    }
                                    //                console.log("Extract Imagedata from uploadBase64AndGetUrl", JSON.parse(response));
                                    this.bannerName = JSON.parse(response).Name
                                    this.loader.hide();
                                    this.utilService.updatebannerImage(this.bannerEdit).then(result=>{
                                        this.toasterPopService.SuccessSnack("Banner Updated Successfully");
                                    })
                                    // this.editcropImage(value);
                                },
                                error => {

                                    this.toasterPopService.ErrorSnack("Problem in uploading the image. Please try again");
                                });
                        }
                    }
                    count++;
                });
    }

    cropImage(selectedImage) {
        var wid,heig;
        let self = this;
        this.selectedImageForCropping = selectedImage;
        //console.log("Croppie");
        if (this.croppie) {
            this.croppie.destroy();
        }
        // if(imageEdit && imageEdit.style) {
        //     imageEdit.style.height = '310px';
        //     imageEdit.style.width = '1296px';
        // }
        jQuery("#editImageModal").modal("show");
        setTimeout(function () {
            let image = document.getElementById('EditableImage');
            //         console.log("image ", image);
            if(self.bannerNew.device == "1"){
                image.style.height = '310px';
                image.style.width = '1296px';
                wid = 1296;
                heig = 310;
            } else{
                image.style.height = '600px';
                image.style.width = '1170px';
                wid = 1170;
                heig = 600;
            }
            
            //image.height = '310px';
            //image.width = '1296px';
            var getwidth = image.clientWidth;
            self.croppie = new Croppie(image, {
                viewport: {
                    width: wid,
                    height: heig
                },
                boundary: { width: wid, height: heig },
                enableOrientation: true

            });
            self.croppie.bind({
                url: selectedImage,
                zoom: 0,
                quality: 0.2,
                format:'jpeg'
            }).then(function (blob) {
                console.log(blob);
                // do something with cropped blob
            });
        }, 1000);
        //  this.saveCroppedImage();
    }

    editcropImage(selectedImage) {
        var wid,heig;
        let self = this;
        this.selectedImageForCropping = selectedImage;
        //console.log("Croppie");
        if (this.croppie) {
            this.croppie.destroy();
        }
        // if(imageEdit && imageEdit.style) {
        //     imageEdit.style.height = '310px';
        //     imageEdit.style.width = '1296px';
        // }
        jQuery("#editImageModal1").modal("show");
        setTimeout(function () {
            let image = document.getElementById('EditableImage1');
            //         console.log("image ", image);
            if(self.bannerEdit.device == "1"){
                image.style.height = '310px';
                image.style.width = '1296px';
                wid = 1296;
                heig = 310;
            } else{
                image.style.height = '600px';
                image.style.width = '1170px';
                wid = 1170;
                heig = 600;
            }
            
            //image.height = '310px';
            //image.width = '1296px';
            var getwidth = image.clientWidth;
            self.croppie = new Croppie(image, {
                viewport: {
                    width: wid,
                    height: heig
                },
                boundary: { width: wid, height: heig },
                enableOrientation: true

            });
            self.croppie.bind({
                url: selectedImage,
                zoom: 0,
                quality: 0.2,
                format:'jpeg'
            }).then(function (blob) {
                console.log(blob);
                // do something with cropped blob
            });
        }, 1000);
        //  this.saveCroppedImage();
    }

    cropperImageReset() {
        this.cropper.reset();
    }
    cropperImageZoomIn() {
        let croppieZoom = this.croppie.get();
        this.zoomRatio = croppieZoom.zoom;
        //   console.log(croppieZoom.zoom, this.zoomRatio)
        if (croppieZoom.zoom >= this.zoomRatio) {
            this.zoomRatio = this.zoomRatio + 0.1;
            //   console.log(this.zoomRatio);
            //     console.log();
            this.croppie.setZoom(this.zoomRatio);
        }
    }
    cropperImageZoomOut() {
        let croppieZoom = this.croppie.get();
        this.zoomRatio = croppieZoom.zoom;
        if (this.zoomRatio > 0) {
            this.zoomRatio = this.zoomRatio - 0.1;
            // console.log(this.zoomRatio);
            this.croppie.setZoom(this.zoomRatio);
        }
    }
    cropperImageRotateLeft() {
        this.croppie.rotate(-90);
    }
    cropperImageRotateRight() {
        this.croppie.rotate(90);
    }


    saveCroppedImage() {

        this.loader.show();
        jQuery("#editImageModal").modal("hide");
        let self = this;
        //  console.log("this.imageTransitions",this.imageTransitions);
        jQuery("#editImageModal").modal("hide");
        this.croppie.result('base64', 'viewport', 'png',0.5).then(function (response) {
            console.log(response);
            self.utilService.uploadBase64AndGetUrl(self.bannerName, response).then(
                response => {
                    self.loader.hide();
                    self.bannerNew.Dimensions.editedImage = JSON.parse(response).ImageData;
                    //            console.log("uploadBase64AndGetUrl :",self.bannerNew);
                },
                error => {
                    self.loader.hide();
                    self.toasterPopService.ErrorSnack("Problem in uploading the image. Please try again");
                }
            );
        });


    }
    saveCroppedImage1(){
        this.loader.show();
        jQuery("#editImageModal1").modal("hide");
        let self = this;
        //  console.log("this.imageTransitions",this.imageTransitions);
        jQuery("#editImageModal1").modal("hide");
        this.croppie.result('base64', 'viewport', 'png', 1, false).then(function (response) {
            self.utilService.uploadBase64AndGetUrl(self.bannerName, response).then(
                response => {
                    self.loader.hide();
                    self.bannerEdit.Dimensions.editedImage = JSON.parse(response).ImageData;
                    //            console.log("uploadBase64AndGetUrl :",self.bannerNew);
                    self.utilService.updatebannerImage(self.bannerEdit).then(result=>{
                        self.toasterPopService.SuccessSnack("Banner Updated Successfully");
                    })
                },
                error => {
                    self.loader.hide();
                    self.toasterPopService.ErrorSnack("Problem in uploading the image. Please try again");
                }
            );
        });
    }

    previousPage() {
        if (this.currentPage > 1) {
            this.currentPage -= 1;
            this.getBannerList((this.currentPage - 1), this.size);

        }
    }

    nextPage() {
        if (this.currentPage < this.totalPages) {
            this.getBannerList(this.currentPage, this.size);
            this.currentPage += 1;
        }
    }

    selectPage(PageNumber) {
        this.currentPage = PageNumber;
        this.getBannerList((this.currentPage - 1), this.size);

    }

}