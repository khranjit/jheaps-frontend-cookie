import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadBannerComponent } from './upload-banner.component';
import { SharedModule } from '../../../shared/shared.module';
  
  
const uploadbanner: Routes = [
    {path: '', component:UploadBannerComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(uploadbanner)
  ],
  declarations: [
    UploadBannerComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class UploadBannerModule { }