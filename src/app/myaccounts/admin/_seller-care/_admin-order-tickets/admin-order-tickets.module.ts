import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminOrderTickets } from './admin-order-tickets.component';
import { SharedModule } from '../../../../shared/shared.module';
  
  
const ordertickets: Routes = [
    {path: '', component:AdminOrderTickets},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ordertickets)
  ],
  declarations: [
    AdminOrderTickets
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class AdminOrderTicketsModule { }