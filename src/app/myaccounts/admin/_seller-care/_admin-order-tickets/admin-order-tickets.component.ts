import { Component} from '@angular/core';
import { NotifyService } from '../../../../services/notify.service';
import { MessageService } from '../../../../services/messages.service';
import { UtilService } from '../../../../services/util.service';
import { UserService } from '../../../../services/user.service';
import {  Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
declare var jQuery: any;

@Component({
    
    selector: 'AdminOrderTickets',
    providers: [NotifyService, UtilService, MessageService, UserService],
    templateUrl: './admin-order-tickets.component.html'
})


export class AdminOrderTickets {
    public OrderTicketsViewModel: ViewModels.IOrderTicketsMessagesViewModel;
    Message: any;
    public productCurrency: any = { value: "₹" };
    public TicketList: any;
    ShowUserProfile: any;
    ShowUserRole: any;
    errorMessage: any;
    Roles: any;
    AdminResponse = <any>{};
    public selectedInboxMessage: any;
    showAllMessages = true;
    showMail = false;
    replyMail = false
    Tickets = [];
    AdminTickets = <any>{};
    pageNumber: number;
    attachments = [];
    Replies: any;
    ticket_id: any;
    ResponseTypes = [{
        "value": 1,
        "label": "Reject Accept"
    },
    {
        "value": 2,
        "label": "Reconsider"
    },
    {
        "value": 3,
        "label": "Refund"
    }];
    to: any;
    obj_id: any;
    sendReply: boolean;
    replyModel: any;
    hasAttach: boolean;
    replylen: number;
    totalPages = 1;
    pages = [];
    currentPage = 1;
    size = 10;
    from = 0;
    total = 0;
    initialPage = 0;
    TicketsSpecific = [];
    searchTerm = "";


    constructor(private router: Router, private loadingservice: NgxSpinnerService, private snack: NotifyService, private messageService: MessageService, private utilService: UtilService, private userService: UserService) {
        this.Message = { "Reason": "", "Description": "", "Attachment": "", };
        this.sendReply = false;
        this.totalPages = 0;
        this.pageNumber = 0;
        this.Replies = [];
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response);
                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                console.log('type of user' + this.ShowUserRole)
                if (this.ShowUserRole != 'Others') {
                    this.getAdminTickets();
                }
                else {
                    this.router.navigate(['']);
                }
            }
        );
    }
    getAdminTickets() {
        this.TicketList = [];
        this.ResetPagination();
        this.messageService.getAdminTickets(this.searchTerm, this.from, this.size)
            .then(
                tickets => {
                    this.TicketList = JSON.parse(tickets);
                    this.obj_id = this.TicketList._id;
                    this.Tickets = this.TicketList;
                    this.ValidateScrollContainer();
                },
                error => this.errorMessage = <any>error);
    }
    test(value, i) {
        this.attachments.splice(i, 1);
    }


    showMailView() {
        this.OrderTicketsViewModel.Reason = "";
        this.showMail = false;
        this.showAllMessages = false;
        this.replyMail = true;
    }
    SearchCheck(searchword, pageNum, size) {
        this.TicketList = [];
        this.Tickets = [];
        this.TicketsSpecific = [];
        this.pages = [];
        this.currentPage = 1;
        this.messageService.getUserTickets(searchword, pageNum - 1, size).then(res => {
            this.TicketList = JSON.parse(res);
            this.obj_id = this.TicketList._id;
            this.Tickets = this.TicketList.Items;
            this.totalPages = this.TicketList.TotalPages;
            this.Tickets.forEach(ele => {
                // ele.Tickets.forEach(ticket=>{
                this.TicketsSpecific.push(ele.Tickets);
                //})
            })
            for (let i = 1; i <= this.totalPages; i++) {
                this.pages.push(i);
                console.log(i);
            }
            this.initialPage = pageNum - 1;
        }, err => {
            this.errorMessage = <any>err;
        });

    }

    ChangeListener($event) {
        let files = [].slice.call($event.target.files);
        var file: File = files[0];
        var type = file.name.split('.').pop();
        if (type == "jpeg" || type == "jpg" || type == "png" || type == "bmp") {
            if (file.size < 5 * 1024 * 1024) {
                if (this.attachments.length < 5) {
                    this.loadingservice.show();
                    let fileName = file.name.split(".")[0];
                    this.utilService.convertToBase64(file);
                    let count = 0;
                    this.utilService.invokeEvent.subscribe(value => {
                        if (count == 0) {
                            this.utilService.uploadBase64AndGetUrl(fileName, value).then(
                                response => {
                                    let obj = { "Name": fileName, "URL": JSON.parse(response).ImageData };
                                    this.attachments.push(obj);
                                    this.loadingservice.hide();
                                },
                                error => {
                                    this.snack.ErrorSnack("Problem in uploading the image. Please try again");
                                });
                        }
                        count++;
                    });
                }
                else {
                    this.snack.ErrorSnack("You can Upload Only 5 Files");
                }
            }
            else {
                this.snack.ErrorSnack( "File Size Exceeds the 5MB Limit");
            }
        }
        else {
            this.snack.ErrorSnack( "File Type is not supported");
        }
    }

    ReplyMessage() {
        var replyModel = {
            To: this.to,
            Reason: this.AdminTickets.Reason,
            Description: this.AdminTickets.Description,
            Attachment: this.attachments,
            id: this.ticket_id,
            IsAdmin: this.AdminTickets.IsAdmin,
            UserId: this.selectedInboxMessage.Messages[0].From._id
        } 
        this.messageService.replyMessage(replyModel).then(
            res => {
                this.sendReply = false;
                this.showAllMessages = true;
                this.showMail = false;
                this.replyMail = false;
                this.AdminTickets.IsAdmin = true;
                this.getAdminTickets();
            },
            error => this.errorMessage = <any>error);
        // this.sendReply = false;
            
    }


    public showMessage(message) {
        this.showMail=true;this.showAllMessages=false;this.replyMail=false;
        this.sendReply = true;
        this.selectedInboxMessage = message;
        this.ticket_id = message._id;
        this.to = message.Messages[0].To;
            }
    SetUsername(Ticket){
        if(Ticket.IsAdmin){
            return "Admin";
        }else{
            return Ticket.To.Profile.FirstName;
        }
    }
    adminResponse(){
        var message1= {
            message:this.selectedInboxMessage,
            adminResponse:this.AdminResponse.response
        }
        this.messageService.adminResponse(message1).then(
            res => {
                var result = JSON.parse(res);
                if(result.msg){
                    this.getAdminTickets();
                    this.showMail=false;this.showAllMessages=true;this.replyMail=false;
                    if(result.flag == 1){
                        this.snack.SuccessSnack("Reject has been approved Successfully");
                    }else if(result.flag == 2){
                        this.snack.SuccessSnack("Refund has been approved Successfully");
                    }else{
                        this.snack.SuccessSnack("Reconsider request has been made successfully");
                    }
                }
            },
            error => this.errorMessage = <any>error);

    }

    LoadOnScroll() {
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

        if (scrollLength - scrollContainer.scrollTop < 1) {
            setTimeout(() => {
                // this.getTicketOnScroll();
            }, 500)
        }
    }

    ResetPagination() {
        this.from = 0;
        this.total = 0;
        jQuery('#scroll-container').css("height", 500);
    }

    ValidateScrollContainer() {
        setTimeout(() => {
            var scrollContainer = document.getElementById('scroll-container');
            var scrollBox = document.getElementById('scroll-box');
            if(scrollContainer!=null && scrollBox!=null){
            var scrollContainerPos = scrollContainer.getBoundingClientRect();
            var scrollBoxPos = scrollBox.getBoundingClientRect();
            if (scrollBoxPos.height < scrollContainerPos.height) {
                jQuery('#scroll-container').css("height", scrollBoxPos.height);
            }
        }
        }, 1000)
    }
}