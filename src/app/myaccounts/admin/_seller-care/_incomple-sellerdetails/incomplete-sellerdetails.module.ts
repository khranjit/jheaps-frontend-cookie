import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IncompleteSellerDetailsComponent } from './incomplete-sellerdetails.component';
import { SharedModule } from '../../../../shared/shared.module';
  
  
const approveseller: Routes = [
    {path: '', component:IncompleteSellerDetailsComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(approveseller)
  ],
  declarations: [
    IncompleteSellerDetailsComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})

export class IncompleteSellerModule { }