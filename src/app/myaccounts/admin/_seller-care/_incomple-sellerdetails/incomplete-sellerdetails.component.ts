
import { Component} from '@angular/core';
import { NotifyService } from '../../../../services/notify.service';
import { UtilService } from '../../../../services/util.service';
import { UserService } from '../../../../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ApproveSellerService } from '../../../../services/approve-seller.service';

declare var jQuery : any;

@Component({ 
   
   selector: 'incomplete-sellerdetails',
    providers: [NotifyService,UtilService,UserService,ApproveSellerService],
   templateUrl: './incomplete-sellerdetails.component.html'
})

export class IncompleteSellerDetailsComponent{
     errorMessage : any;
     waitingList = [];
     updatedWaitingList = [];
     incompleteList = [];
     updatedList = [];
     showUpdatedList : boolean;
     showWaitingList: boolean;
     shopDetails: any;
    // updateDetails: any;
     shopID:any;
     checkDetail=<any>{
        BankDetails:[],
        BusinessDetails:[]
     };
     checkUpdateDetail= <any>{};
     approveUpdateModel = <any>{};
     getDetails : string;
     searchDetails = <any>{};
   
     totalPages = 1;
     pages: any[];
     Approvepages: any[];
     currentPage = 1;
     size = 4;
     initialPage=0;
     SearchTerm: string;
     ShowUserRole : any;
     ShowUserProfile : any;
     flag=0;
     searchTerm:any; 
    constructor(private router: Router,private snack: NotifyService, private utilService: UtilService,
          private userService: UserService,private approveSellerService: ApproveSellerService) {
            this.userService.showProfile().then(
                response => {
                    this.ShowUserProfile = JSON.parse(response); 
                    this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                        console.log('type of user'+this.ShowUserRole)
                        // if(this.ShowUserRole== 'masteradmin'||this.ShowUserRole== 'admin'){
                             this.showUpdatedList = true;
                             this.showWaitingList = true;
                             this.searchTerm = "";
                             this.init()
                      
                }
               );
      
     }

    init(){
         window.scrollTo(0,20);
         this.IncompleteList(0,10);
    }

    selectPage(PageNumber) {
        this.currentPage = PageNumber;
        var offset = this.currentPage - 1
        // this.searchProducts(this.searchTerm, ((this.currentPage - 1)*this.size), this.size);
        this.IncompleteList(offset, this.size);
      }

    public IncompleteList(offset,limit){
        this.flag=0;
        this.pages = [];
        this.totalPages=0;
        this.waitingList=[];
        this.approveSellerService.getincompleteList(offset,this.size,this.searchTerm)
        .then(
        response => {
            this.waitingList = JSON.parse(response).Items;
            this.totalPages = JSON.parse(response).TotalPages;
            this.pages = [];
            for(let i = 1; i <= this.totalPages; i++)
            {
            this.pages.push(i);
            //console.log(i);
            }
         },
        error => this.errorMessage = <any>error);
    }

    previousPage()
    {
        if(this.currentPage > 1){
                var offset = this.currentPage -2;
                this.IncompleteList(offset, this.size);
                this.currentPage -=1;   
        }
    }

    nextPage()
    {
        if(this.currentPage < this.totalPages){
                var offset = this.currentPage;
                this.IncompleteList(offset, this.size);
                this.currentPage +=1;

        }
    }

    ViewList(id){
        this.showWaitingList=false;
        this.checkDetail = this.waitingList.find(ele => ele._id == id);
        console.log(this.checkDetail);
    }
    searchSeller()
    {
        if(isNaN(this.searchTerm) || this.searchTerm == "") {

            this.init();

        }
        else {

            if(this.searchTerm.length == 10) {

                this.init();
            }
            
        }
    }
  
}
