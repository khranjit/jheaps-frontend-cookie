import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditBusinessSellerComponent } from './editbusinessseller.component';
import { SharedModule } from '../../../../shared/shared.module';
import { MatInputModule } from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
  
const approveseller: Routes = [
    {path: '', component:EditBusinessSellerComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(approveseller),
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule
  ],
  declarations: [
    EditBusinessSellerComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})

export class EditBusinessSellerModule { }