
import { Component,ViewChild,ChangeDetectionStrategy, ViewEncapsulation} from '@angular/core';
import { NotifyService } from '../../../../services/notify.service';
import { UtilService } from '../../../../services/util.service';
import { UserService } from '../../../../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ApproveSellerService } from '../../../../services/approve-seller.service';
import { RegisterService } from '../../../../services/register.service';
import { ShopService } from '../../../../services/shop.service';
import { NgxSpinnerService } from 'ngx-spinner';

declare var jQuery : any;

@Component({ 
    changeDetection: ChangeDetectionStrategy.Default,
    encapsulation: ViewEncapsulation.Emulated,
    selector: 'editbusinessseller',
    providers: [NotifyService,UtilService,UserService,ApproveSellerService,ShopService],
    templateUrl: './editbusinessseller.component.html'
})

export class EditBusinessSellerComponent{
    public shopAccountModel= <any>{};
     errorMessage : any;
     public businessdetailsModel=<any>{};
     public profileModel = <any>{ Address: {} };
     public profileModelAddress=  <any>{};
     waitingList = [];
     updatedWaitingList = [];
     incompleteList = [];
     updatedList = [];
     showUpdatedList : boolean;
     showWaitingList: boolean;
     shopDetails: any;
     statesArray:any;
     sameShop=false;
     submitenable=false;

    // updateDetails: any;
     shopID:any;
     checkDetail=<any>{
        BankDetails:[],
        BusinessDetails:[]
     };
     checkUpdateDetail= <any>{};
     approveUpdateModel = <any>{};
     getDetails : string;
     searchDetails = <any>{};
     showEditSection: boolean;
     totalPages = 1;
     pages: any[];
     Approvepages: any[];
     currentPage = 1;
     size = 8;
     initialPage=0;
     previousName: any;
     previousDisplayName: any;
     previousGSTTIN: any;
     previousPAN: any;
     SearchTerm: string;
     ShowUserRole : any;
     ShowUserProfile : any;
     flag=0;
     approved : false;
     getShopObj = <any>{}
     editTrue:boolean;
     searchTerm:any; 
     @ViewChild('GetBusinessDetailsForm1') businessDetailsForm: HTMLFormElement;
    
     countryId:any;
     
    constructor(private shopService: ShopService,private router: Router,private toasterPopService: NotifyService, private utilService: UtilService,
          private userService: UserService,private approveSellerService: ApproveSellerService,private registerService: RegisterService,private loaderComponent: NgxSpinnerService) {
              this.showEditSection = false;
              this.businessdetailsModel = <any>{};
              this.profileModelAddress = <any>{};
            this.userService.showProfile().then(
                response => {
                    this.ShowUserProfile = JSON.parse(response); 
                    this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                        console.log('type of user'+this.ShowUserRole)
                        // if(this.ShowUserRole== 'masteradmin'||this.ShowUserRole== 'admin'){
                             this.showUpdatedList = true;
                             this.showWaitingList = true;
                             this.searchTerm = "";
                             this.init()
                      
                }
               );
      
     }

    init(){
         window.scrollTo(0,20);
         this.countryId = { "CountryId": 101 }
         this.registerService.getStates(this.countryId).then(
            statesInfo => {
                this.statesArray = JSON.parse(statesInfo);
            }, error => this.errorMessage = <any>error);
         this.editBusinessList(0,10);
    }

    selectPage(PageNumber) {
        this.currentPage = PageNumber;
        var offset = this.currentPage - 1
        // this.searchProducts(this.searchTerm, ((this.currentPage - 1)*this.size), this.size);
        this.editBusinessList(offset, this.size);
      }
    EditList(id){
        this.approveSellerService.getDocumentUpdatedSellerDetails(id)
        .then(res=>{
            var result = JSON.parse(res);
            this.shopID = id;
            this.businessdetailsModel = result["editedBusinessDetails"];
            this.editTrue = result.isEditedBusinessDetails
            this.sameShop = this.shopAccountModel.SameasShopAddress = result.SameasShopAddress
            this.previousName=result["editedBusinessDetails"].Name;
            this.previousGSTTIN=result["editedBusinessDetails"].DisplayName;

            this.previousGSTTIN=result["editedBusinessDetails"].GstinProvisionalId;
            this.previousPAN=result["editedBusinessDetails"].BusinessPanNumber;
            this.profileModel = this.businessdetailsModel["Profile"];
            this.profileModelAddress = this.profileModel["Address"];
            // this.stateChange(this.profileModelAddress.State);
            this.profileModelAddress.sfLine1 = this.businessdetailsModel.ShipsFromAddress.Line1;
            this.profileModelAddress.sfLine2 = this.businessdetailsModel.ShipsFromAddress.Line2;
            this.profileModelAddress.sfState = this.businessdetailsModel.ShipsFromAddress.State;
            // this.sfstateChange(this.profileModelAddress.sfState);
            this.profileModelAddress.sfCity = this.businessdetailsModel.ShipsFromAddress.City;
            this.profileModelAddress.sfPhone = this.businessdetailsModel.ShipsFromAddress.Phone;
            this.profileModelAddress.sfZip = this.businessdetailsModel.ShipsFromAddress.Zip;
            this.showEditSection = true;
            this.showWaitingList = false;
        })
    }

    public editBusinessList(offset,limit){
        this.flag=0;
        this.pages = [];
        this.totalPages=0;
        this.waitingList=[];
        this.approveSellerService.getEditBusinessList(offset,this.size, this.searchTerm )
        .then(
        response => {
            this.waitingList = JSON.parse(response).Items;
            this.totalPages = JSON.parse(response).TotalPages;
            this.pages = [];
            for(let i = 1; i <= this.totalPages; i++)
            {
            this.pages.push(i);
            //console.log(i);
            }
         },
        error => this.errorMessage = <any>error);
    }

    previousPage()
    {
        if(this.currentPage > 1){
                var offset = this.currentPage -2;
                this.editBusinessList(offset, this.size);
                this.currentPage -=1;   
        }
    }

    nextPage()
    {
        if(this.currentPage < this.totalPages){
                var offset = this.currentPage;
                this.editBusinessList(offset, this.size);
                this.currentPage +=1;

        }
    }

    ViewList(id){
        this.showWaitingList=false;
        this.checkDetail = this.waitingList.find(ele => ele._id == id);
        console.log(this.checkDetail);
    }

    CheckBusinessName() {
        this.shopService.CheckBusinessName(this.businessdetailsModel.Name).then(
            res => {
                var newError = <any>{};
                var oldError = this.businessDetailsForm.form.controls['BusinessName'].errors;
                //console.log(obj);
                if (JSON.parse(res).found && this.previousName!=this.businessdetailsModel.Name) {
                    for (var key in oldError) {
                        // console.log(key);
                        // console.log(oldError[key]);
                        newError[key] = oldError[key]
                    }
                    newError.incorrect = true;
                    // console.log(newError);
                    this.businessDetailsForm.form.controls['BusinessName'].setErrors(newError);
                }

                else {
                    if (oldError.incorrect)
                        oldError.incorrect = false;
                    this.businessDetailsForm.form.controls['BusinessName'].setErrors(oldError);
                }
            }
        )
    }
    // CheckBusinessDisplayName() {
    //     this.shopService.CheckBusinessDisplayName(this.businessdetailsModel.DisplayName).then(
    //         res => {
    //             var newError = <any>{};
    //             var oldError = this.businessDetailsForm.form.controls['Displayname'].errors;
    //             //console.log(obj);
    //             if (JSON.parse(res).found && this.previousDisplayName!=this.businessdetailsModel.DisplayName) {
    //                 for (var key in oldError) {
    //                     // console.log(key);
    //                     // console.log(oldError[key]);
    //                     newError[key] = oldError[key]
    //                 }
    //                 newError.incorrect = true;
    //                 // console.log(newError);
    //                 this.businessDetailsForm.form.controls['Displayname'].setErrors(newError);
    //             }

    //             else {
    //                 if (oldError.incorrect)
    //                     oldError.incorrect = false;
    //                 this.businessDetailsForm.form.controls['Displayname'].setErrors(oldError);
    //             }
    //         }
    //     )
    // }
    // CheckGSTIN() {
    //     this.shopService.CheckGSTIN(this.businessdetailsModel.GstinProvisionalId).then(
    //         res => {
    //             var newError = <any>{};
    //             var oldError = this.businessDetailsForm.form.controls['GSTINProvisionalID'].errors;
    //             //console.log(obj);
    //             if (JSON.parse(res).found && this.previousGSTTIN!=this.businessdetailsModel.GstinProvisionalId) {
    //                 for (var key in oldError) {
    //                     console.log(key);
    //                     console.log(oldError[key]);
    //                     newError[key] = oldError[key]
    //                 }
    //                 newError.incorrect = true;
    //                 // console.log(newError);
    //                 this.businessDetailsForm.form.controls['GSTINProvisionalID'].setErrors(newError);
    //             }

    //             else {
    //                 if (oldError.incorrect)
    //                     oldError.incorrect = false;
    //                 this.businessDetailsForm.form.controls['GSTINProvisionalID'].setErrors(oldError);
    //             }
    //         }
    //     )
    // }
    // CheckPAN() {
    //     this.shopService.CheckPAN(this.businessdetailsModel.BusinessPanNumber).then(
    //         res => {
    //             var newError = <any>{};
    //             var oldError = this.businessDetailsForm.form.controls['Bpan'].errors;
    //             //console.log(obj);
    //             if (JSON.parse(res).found && this.previousPAN!=this.businessdetailsModel.BusinessPanNumber) {
    //                 for (var key in oldError) {
    //                     // console.log(key);
    //                     // console.log(oldError[key]);
    //                     newError[key] = oldError[key]
    //                 }
    //                 newError.incorrect = true;
    //                 // console.log(newError);
    //                 this.businessDetailsForm.form.controls['Bpan'].setErrors(newError);
    //             }

    //             else {
    //                 if (oldError.incorrect)
    //                     oldError.incorrect = false;
    //                 this.businessDetailsForm.form.controls['Bpan'].setErrors(oldError);
    //             }
    //         }
    //     )
    // }
    // CheckPersonalPAN() {
    //     this.shopService.CheckPersonalPAN(this.businessdetailsModel.PersonalPanNumber).then(
    //         res => {
    //             var newError = <any>{};
    //             var oldError = this.businessDetailsForm.form.controls['pan'].errors;
    //             //console.log(obj);
    //             if (JSON.parse(res).found) {
    //                 for (var key in oldError) {
    //                     // console.log(key);
    //                     // console.log(oldError[key]);
    //                     newError[key] = oldError[key]
    //                 }
    //                 newError.incorrect = true;
    //                 // console.log(newError);
    //                 this.businessDetailsForm.form.controls['pan'].setErrors(newError);
    //             }

    //             else {
    //                 if (oldError.incorrect)
    //                     oldError.incorrect = false;
    //                 this.businessDetailsForm.form.controls['pan'].setErrors(oldError);
    //             }
    //         }
    //     )
    // }

    CheckPhone() {
        this.shopService.CheckPhone(this.profileModelAddress.Phone).then(
            res => {
                var newError = <any>{};
                var oldError = this.businessDetailsForm.form.contprols['Phone'].errors;
                //console.log(obj);
                if (JSON.parse(res).found) {
                    for (var key in oldError) {
                        // console.log(key);
                        // console.log(oldError[key]);
                        newError[key] = oldError[key]
                    }
                    newError.incorrect = true;
                    // console.log(newError);
                    this.businessDetailsForm.form.controls['Phone'].setErrors(newError);
                }

                else {
                    if (oldError.incorrect)
                        oldError.incorrect = false;
                    this.businessDetailsForm.form.controls['Phone'].setErrors(oldError);
                }

            }
        )
    }

    getBusinessDetails(){
        var Address = { Address: this.profileModelAddress };
        this.businessdetailsModel.Profile = Address;
        this.businessdetailsModel.ShipsFromAddress = {
            Line1:this.profileModelAddress.sfLine1,
            Line2:this.profileModelAddress.sfLine2,
            State:this.profileModelAddress.sfState,
            City:this.profileModelAddress.sfCity,
            Zip:this.profileModelAddress.sfZip,
            Phone:this.profileModelAddress.sfPhone
        }

        this.shopAccountModel.editedBusinessDetails = this.businessdetailsModel
        this.shopAccountModel.isEditedBusinessDetails = true;
        this.shopAccountModel.SubmitBusiness = true;
        // this.openshopModel["ShopAccount"] = this.shopAccountModel;
        // this.shopID = this.getShopObj._id;
        if (this.shopAccountModel.hasOwnProperty('BankDetails'))
            delete this.shopAccountModel.BankDetails;
        this.shopService.putShopDetailBusinessByAdmin(this.shopID, this.shopAccountModel)
            .then(resdata => {
                var res = JSON.parse(resdata);
                if(res.flag == false){
                    this.toasterPopService.ErrorSnack(res.message)
                }else{
                    this.showWaitingList = true;
                    this.showEditSection = false;
                    this.toasterPopService.SuccessSnack("Business Details Saved Updated Successfully");
                    this.init();
                }
               
            });
    }

    ChangeListener($event, Name) {

        //    this.loader=true;
        this.loaderComponent.show();
        // console.log("change listenser called ", $event)
        let files = [].slice.call($event.target.files);
        var file: File = files[0];
        // console.log('file', file);
        if(!file){
            this.loaderComponent.hide()
            return
        }
        this.size = file.size;
        var fSExt = new Array('Bytes', 'KB', 'MB', 'GB'),
        i=0;while(this.size>900){this.size/=1024;i++;}
        var exactSize = (Math.round(this.size*100)/100)+' '+fSExt[i];
        // console.log(exactSize);
        var ext = file.name.split(".")[1];
        if (ext == "jpg" || ext == "png" || ext == "jpeg" || ext == "JPG" || ext == "JPEG" || ext == "PNG" || ext == "docx" || ext == "pdf") {
            if(ext == "jpg" || ext == "png" || ext == "JPG" || ext == "PNG"){
                var myReader: FileReader = new FileReader();
                myReader.readAsDataURL(file);
                myReader.onloadend = (e: any) => {
                    var i = new Image();
                    i.src = e.target.result;
                    var self = this;
                    i.onload = function () {
                        
                            if (self.size < 5e+6) {
                                let fileName = file.name.split(".")[0];
                                let ext = file.name.split(".")[1]
                                let data = myReader.result.toString().split(',')[1]
                                self.fileupload(fileName,ext,data,Name,file);
                                
                            } else {
                                self.loaderComponent.hide();
                                
                                self.toasterPopService.ErrorSnack("Kindly check your file size and upload again");
                            }  
                        
                       
                        //    console.log("From Service ,",i.width,i.height)
                    }
                }
            }
            else{
                if(this.size < 5e+6){
                    var myReader: FileReader = new FileReader();
                    myReader.readAsDataURL(file);
                    myReader.onloadend = (e: any) => {
                        let fileName = file.name.split(".")[0];
                        let ext = file.name.split(".")[1]
                        let data = myReader.result.toString().split(',')[1]
                        this.fileupload(fileName,ext,data,Name,file);
                    }
                    
                }else{
                    this.loaderComponent.hide();
                    this.toasterPopService.ErrorSnack("Kindly check your file size and upload again");
                }
            }
            
        } else if (ext == "pdf" || ext == "docx") {
            let reader = new FileReader();
            if ($event.target.files && $event.target.files.length > 0) {
                let file = $event.target.files[0];
                reader.readAsDataURL(file);
                reader.onload = () => {
                    let fileName = file.name.split(".")[0];
                    let ext = file.name.split(".")[1]
                    let data = reader.result.toString().split(',')[1]
                    // console.log("data " + data, "name " + fileName, "ext " + ext);
                    this.fileupload(fileName,ext,data,Name,file);
                };
            }
        } else {
            this.loaderComponent.hide();
            this.toasterPopService.ErrorSnack("Kindly upload proper format");
        }
    }

    fileupload(fileName, ext, data, Name,file) {
        // this.loaderComponent.show();
        this.utilService.uploadBase64AndGetUrlFile(fileName, ext, data,file).then(res => {
            // console.log("response " + res)
            var result = JSON.parse(res)
            this.loaderComponent.hide();
            if(result.Output){
                if (Name == "CancelledCheckCopy") {
                    this.resetSubmitButton();
                }
                else {
                   
                    this.businessdetailsModel[Name] = JSON.parse(res).ImageData;
                    this.resetSubmitButton();
                }
            }else{
                this.resetSubmitButton();
                this.toasterPopService.ErrorSnack('Something went wrong')
            }

        })
    }

    resetSubmitButton()
    {
      
        if(this.businessdetailsModel.hasOwnProperty("GstinProof") && this.businessdetailsModel.hasOwnProperty("PanProof") && this.businessdetailsModel.hasOwnProperty("Signature") && this.businessdetailsModel.hasOwnProperty("AttachAddressProof"))
        {
            this.submitenable=true;
        }
        
    }
    searchSeller()
    {
        if(isNaN(this.searchTerm) || this.searchTerm == "") {

            this.init();

        }
        else {

            if(this.searchTerm.length == 10) {

                this.init();
            }
            
        }
    }

  
}
