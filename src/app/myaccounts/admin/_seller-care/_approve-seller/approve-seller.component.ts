import { Component} from '@angular/core';
import { NotifyService } from '../../../../services/notify.service';
import { UtilService } from '../../../../services/util.service';
import { UserService } from '../../../../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ApproveSellerService } from '../../../../services/approve-seller.service';

declare var jQuery : any;

@Component({ 
   
   selector: 'approve-seller',
    providers: [NotifyService,UtilService,UserService,ApproveSellerService],
   templateUrl: './approve-seller.component.html'
})

export class ApproveSellerComponent{
     errorMessage : any;
     waitingList = [];
     updatedWaitingList = [];
     updatedList: any[];
    
     showUpdatedList : boolean;
     showWaitingList: boolean;
     tabActiveBusi:boolean;
     tabActiveBank:boolean;     
     shopDetails: any;
    // updateDetails: any;
     shopID:any;
     checkDetail=<any>{
        BankDetails:[],
        BusinessDetails:[]
     };
     checkUpdateDetail= <any>{};
     approveUpdateModel = <any>{};
     getDetails : any;
     searchDetails = <any>{};
     UnApprovetotalPages = 0;
     UnApprovepageNumber = 0;
     ApprovetotalPages = 0;
     ApprovepageNumber = 0;
     totalPages = 1;
     pages: any[];
     Approvepages: any[];
     currentPage = 1;
     size = 10;
     initialPage=0;
     SearchTerm: any;
     ShowUserRole : any;
     ShowUserProfile : any;
     flag=0;
     Phone:any;
     PhoneSf:any;
     
    constructor(private router: Router,private snack: NotifyService, private utilService: UtilService,
          private userService: UserService,private approveSellerService: ApproveSellerService) {
            // jQuery('#Business').removeClass('active in')
            // jQuery('#busi').removeClass('active')
            // jQuery('#banking').removeClass('active')
            // jQuery('#Bank').removeClass('active in')
            this.updatedList =[{
                editedBankDetails:true,
                editedBusinessDetails:true
            }];
            this.userService.showProfile().then(
                response => {
                    this.ShowUserProfile = JSON.parse(response); 
                    this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                        console.log('type of user'+this.ShowUserRole)
                        // if(this.ShowUserRole== 'masteradmin'||this.ShowUserRole== 'admin'){
                             this.showUpdatedList = true;
                             this.showWaitingList = true;
                             this.getDetails = "";
                             this.SearchTerm = "";
                             this.init()
                        // }
                        // else{
                        //     this.router.navigate(['']);
                        // }
                }
               );
      
     }
    
    init(){
         window.scrollTo(0,20);
         this.unApprovedList();
         //this.documentUpdatedSellerList();
    }

    unApprovedList(){
        this.flag=0;
        this.pages = [];
        this.totalPages=0;
        this.waitingList=[];
        if(this.getDetails != "")
        {
            this.initialPage=0;
        }
        this.approveSellerService.getSellerWaitingList(this.initialPage,this.getDetails)
        .then(
        response => {
  //          console.log("UnAPproved response",response);
            //this.UnApprovetotalPages = JSON.parse(response).TotalPages;
            this.waitingList = JSON.parse(response).Items;
            this.totalPages = JSON.parse(response).TotalPage;
            this.pages = [];
            //console.log(this.totalPages);
            for(let i = 1; i <= this.totalPages; i++)
            {
            this.pages.push(i);
            //console.log(i);
            }
            //console.log('Pages',this.pages);
            console.log((this.flag==0), (this.waitingList.length >0))
         },
        error => this.errorMessage = <any>error);
    }
    selectPage(PageNumber) {
        this.currentPage = PageNumber;
        // this.searchProducts(this.searchTerm, ((this.currentPage - 1)*this.size), this.size);
        this.updateUnApprovedItems(this.currentPage, this.size);
      }
    previousPage()
    {
        if(this.currentPage > 1){
                this.updateUnApprovedItems(this.currentPage -1, this.size);
                this.currentPage -=1;   
        }
    }

    nextPage()
    {
        if(this.currentPage < this.totalPages){
                this.updateUnApprovedItems(this.currentPage + 1, this.size);
                this.currentPage +=1;

        }
    }
    updateUnApprovedItems(pageNum, size)
    {
        if(this.flag==0)
        {
            this.approveSellerService.getSellerWaitingList(pageNum,this.getDetails)
            .then( res =>{
                this.waitingList = JSON.parse(res).Items;
                  this.initialPage=pageNum-1;
              }, err=>{
                  this.errorMessage = <any>err;
              });
        }
        else
        {
            this.approveSellerService.getDocumentUpdatedSellerList(this.ApprovepageNumber,this.SearchTerm)
            .then(
            response => {
                this.updatedWaitingList = JSON.parse(response).Items;
                this.initialPage=pageNum-1;
             },
            error => { this.errorMessage = <any>error});
        }
    }
    // getSearchValue(value) {
    //     this.getDetails = value;
    // }

    // Search(value) {
    //     console.log("Search Data");
    //     this.searchDetails = { "SearchQuery": this.getDetails };
    //     //console.log("Search Data", this.getDetails);
    //     if(value == "UnApprove"){
    //     this.approveSellerService.searchShopUnApprove(this.searchDetails).then(
    //         Status => {
    // //            console.log("STatus", Status);
    //             this.waitingList = <any>{};
    //             this.waitingList = JSON.parse(Status);
    //         },
    //         error => this.errorMessage = <any>error);
    //     }
    //     else if(value == "Approve"){
    //         this.approveSellerService.searchShopApprove(this.searchDetails).then(
    //             Status => {
    //   //              console.log("STatus", Status);
    //                 this.updatedWaitingList = <any>{};
    //                 this.updatedWaitingList = JSON.parse(Status);
    //             },
    //             error => this.errorMessage = <any>error);   
    //         }
    // }

    
    // key_down(e,searchshop,value) {
    //     //console.log(e.keyCode)
    //     if(e.keyCode === 13) {
    //     //    console.log("hello",e.keyCode,searchshop)
    //         this.getSearchValue(searchshop);
    //         this.Search(value);
    //     }
    //   }

    documentUpdatedSellerList(){
        this.flag=1;
        this.pages = [];
        this.totalPages=0;
        this.updatedWaitingList=[];
        if(this.SearchTerm != "")
        {
            this.initialPage=0;
        }
        this.approveSellerService.getDocumentUpdatedSellerList(this.initialPage,this.SearchTerm)
        .then(
        response => {
            this.updatedWaitingList = <any>{};
          //  console.log("getDocumentUpdatedSellerDetails response",response);
            this.totalPages = JSON.parse(response).TotalPages;
            this.updatedWaitingList = JSON.parse(response).Items;
            this.pages = [];
            for(let i = 1; i <= this.totalPages; i++)
            {
            this.pages.push(i);
            //console.log(i);
            }
            console.log((this.flag==1), (this.updatedWaitingList.length >0))
         },
        error => this.errorMessage = <any>error);
    }
    ApproveUpdatedBusinessDetails(){
        var model = {
            ShopAccountId : this.shopID,
            Phone : this.Phone,
            PhoneSf: this.PhoneSf
        }
        this.approveSellerService.approveUpdatedPhone(model)
        .then(res=>{
            var result = JSON.parse(res);
            if(result.flag){
                this.snack.SuccessSnack("Approved Successfully");
                this.init();
                this.showUpdatedList =  true;
            }
        })

    }

    viewUpdatedList(id){
        jQuery('#Business').removeClass('active in')
        jQuery('#Bank').removeClass('active in')
        jQuery('#busi').removeClass('active')
        jQuery('#banking').removeClass('active')
        
        //console.log(id);
        this.shopID=id;
        this.showUpdatedList=false;
        this.approveSellerService.getDocumentUpdatedSellerDetails(id)
            .then(
            response => {
                var res = JSON.parse(response)
                this.updatedList = [];        
                this.updatedList.push(JSON.parse(response));
                if(res.isEditedBankDetails){
                    this.checkUpdateDetail.BankDetails = this.updatedList[0].editedBankDetails;
                    this.checkUpdateDetail.ShopAccountId = this.updatedList[0]._id;
                }
                if(res.isEditedBusinessDetails){
                    this.Phone = this.updatedList[0].editedBusinessDetails.Profile.Address.Phone;
                    this.PhoneSf = this.updatedList[0].editedBusinessDetails.ShipsFromAddress.Phone;

                }
                if(res.isEditedBankDetails && res.isEditedBusinessDetails){
                    // jQuery('#busi').addClass('active')
                    this.tabActiveBusi = true;
                    this.tabActiveBank = false;
                    jQuery('#Business').addClass('active in')
                }else if(res.isEditedBankDetails){
                    this.tabActiveBusi = false;
                    this.tabActiveBank = true;
                    // jQuery('#banking').addClass('active')
                    jQuery('#Bank').addClass('active in')
                }else{
                    this.tabActiveBusi = true;
                    this.tabActiveBank = false;
                    // jQuery('#busi').addClass('active')
                    jQuery('#Business').addClass('active in')
                }
            //    console.log(",dddddd",this.updatedList)
               
                // if(this.updatedList[0].IsApprovedBankDetails == "UpdatedAfterApproval" &&  this.updatedList[0].IsApprovedBusinessDetails == "UpdatedAfterApproval")
                // {
                //     this.checkUpdateDetail = <any>{};
                //     this.checkUpdateDetail.Id = this.updatedList[0]._id;
                //     this.checkUpdateDetail.User = this.updatedList[0].User;
                //     this.checkUpdateDetail.BankDetails = this.updatedList[0].BankDetails[0];
                //     this.checkUpdateDetail.BusinessDetails =  this.updatedList[0].BusinessDetails[0];
                //     this.checkUpdateDetail.Shop = this.updatedList[0].Shop;
                // }
                // else if(this.updatedList[0].IsApprovedBusinessDetails == "UpdatedAfterApproval" ){
                //   //  console.log('Only Business details Modified');
                //     this.checkUpdateDetail = <any>{};
                //     this.checkUpdateDetail.Id = this.updatedList[0]._id;
                //     this.checkUpdateDetail.User = this.updatedList[0].User;
                //     this.checkUpdateDetail.BusinessDetails =  this.updatedList[0].BusinessDetails[0];
                //     this.checkUpdateDetail.Shop = this.updatedList[0].Shop;
                //     //console.log(this.checkUpdateDetail)
                // }
                // else if(this.updatedList[0].IsApprovedBankDetails == "UpdatedAfterApproval" ){
                //     this.checkUpdateDetail = <any>{};
                //     //console.log("b",this.updatedList[0]["BankDetails"][0]);
                //     this.checkUpdateDetail.Id = this.updatedList[0]._id;
                //     this.checkUpdateDetail.User = this.updatedList[0].User;
                //     this.checkUpdateDetail.BankDetails = this.updatedList[0].BankDetails[0];
                //     this.checkUpdateDetail.Shop = this.updatedList[0].Shop;
                //     //console.log('Only Bank details Modified');
                //     //console.log(this.checkUpdateDetail)
                // }
                //this.checkUpdateDetail= this.shopDetails[0];
               // this.WaitingList=JSON.parse(response);
             },
            error => this.errorMessage = <any>error);
    }

    ViewList(id){
        //console.log(id);
        // this.shopID=id;
        this.showWaitingList=false;
        // this.approveSellerService.getSellerDetails(id)
        //     .then(
        //     response => {
        //         console.log("response",response);
        //         //this.checkDetail=JSON.parse(response).Items;
        //        //    console.log(" this.checkDetail", this.checkDetail);
                
        //         this.shopDetails=JSON.parse(response).Items;
        //         this.checkDetail= this.shopDetails[0];
        //        // this.WaitingList=JSON.parse(response);
        //      },
        //     error => this.errorMessage = <any>error);

        this.checkDetail = this.waitingList.find(ele => ele._id == id);
        console.log(this.checkDetail);
    }

    ApproveSeller(id){
        //console.log("entered");

        let approveModel={_id : id}
        //console.log(approveModel)
        this.approveSellerService.approveSeller(approveModel)
            .then(
            response => {
          //      console.log("response",response);
                this.snack.SuccessSnack("Seller Approved Successfully")
                this.unApprovedList();
                this.showWaitingList=true;
               
             },
            error => this.errorMessage = <any>error);
    }

    ApproveUpdatedDetails(){
        //console.log("Approved Updated");
        this.approveUpdateModel = <any>{};

        // if(this.updatedList[0].BankDetails.length >1 && this.updatedList[0].BusinessDetails.length > 1 ){
        //     this.approveUpdateModel = {
        //         "ApprovalDetails" : "Both",
        //         "ShopAccountId" : this.checkUpdateDetail.Id,
        //         "UserId" : this.checkUpdateDetail.User,
        //         "ShopId" : this.checkUpdateDetail.Shop.Id,
        //         "ShopName" : this.checkUpdateDetail.Shop.Name,
        //         "UserProfile" : this.checkUpdateDetail.BusinessDetails.Profile
        //     }
            
        // }
        // else if(this.updatedList[0].BusinessDetails.length >1){
        //     this.approveUpdateModel = {
        //         "ApprovalDetails" : "BusinessDetails",
        //         "ShopAccountId" : this.checkUpdateDetail.Id,
        //         "UserId" : this.checkUpdateDetail.User,
        //         "ShopId" : this.checkUpdateDetail.Shop.Id,
        //         "ShopName" : this.checkUpdateDetail.Shop.Name,
        //         "UserProfile" : this.checkUpdateDetail.BusinessDetails.Profile
        //     }
        // } 
        
        // else{
        //     this.approveUpdateModel = {
        //         "ApprovalDetails" : "BankDetails",
        //         "ShopAccountId" : this.checkUpdateDetail.Id,
        //         //"UserId" : this.checkUpdateDetail.User,
        //         //"ShopId" : this.checkUpdateDetail.Shop.Id,
        //         //"UserProfile" : this.checkUpdateDetail.BusinessDetails.Profile
        //     }
        // }
        this.approveUpdateModel = {
            "ApprovalDetails" : "BankDetails",
            "ShopAccountId" : this.checkUpdateDetail.ShopAccountId,
            //"UserId" : this.checkUpdateDetail.User,
            //"ShopId" : this.checkUpdateDetail.Shop.Id,
            //"UserProfile" : this.checkUpdateDetail.BusinessDetails.Profile
        }
        //console.log("Req Param ",this.approveUpdateModel)
        this.approveSellerService.approveUpdatedSeller(this.approveUpdateModel)
            .then(
            response => {
                this.approveUpdateModel = <any>{};
//                console.log("response",response);
                this.snack.SuccessSnack( "Seller Approved Successfully")
                this.showUpdatedList = true;
                this.documentUpdatedSellerList();
                this.showWaitingList=true;
               
             },
            error => this.errorMessage = <any>error);
    }

    pageSelectionNext(value) {
        if (value == "UnApprove"){
            this.UnApprovetotalPages = this.UnApprovetotalPages;
            this.UnApprovepageNumber = this.UnApprovepageNumber + 1;
            this.unApprovedList();
        }
        else if(value == "Approve"){
            this.ApprovetotalPages = this.ApprovetotalPages;
            this.ApprovepageNumber = this.ApprovepageNumber + 1;
            this.documentUpdatedSellerList();
       }

    }
    pageSelectionPrev(value) {
        if (value == "UnApprove"){
            this.UnApprovepageNumber = this.UnApprovepageNumber - 1;
            this.unApprovedList();
        }
        else if(value == "Approve"){
            this.UnApprovepageNumber = this.UnApprovepageNumber - 1;
            this.documentUpdatedSellerList();
        }

    }
    searchSeller()
    {
        if(isNaN(this.getDetails) || this.getDetails == "") {

            this.init();

        }
        else {

            if(this.getDetails.length == 10) {

                this.init();
            }
            
        }
    }
    searchSellerUpdates()
    {

        if(isNaN(this.SearchTerm) || this.SearchTerm == "") {

            this.documentUpdatedSellerList();

        }
        else {

            if(this.SearchTerm.length == 10) {

                this.documentUpdatedSellerList();
            }
            
        }
    }
}
