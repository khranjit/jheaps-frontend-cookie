import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApproveSellerComponent } from './approve-seller.component';
import { SharedModule } from '../../../../shared/shared.module';
  
  
const approveseller: Routes = [
    {path: '', component:ApproveSellerComponent},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(approveseller)
  ],
  declarations: [
    ApproveSellerComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ApproveSellerModule { }