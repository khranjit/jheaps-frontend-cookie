import { Component} from '@angular/core';
import { NotifyService } from '../../../../services/notify.service';
import { MessageService } from '../../../../services/messages.service';
import { UtilService } from '../../../../services/util.service';
import { UserService } from '../../../../services/user.service';
import {  Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CreditDebitNoteService } from '../../../../services/credit-debit-note.service';
import { any } from 'nconf';
declare var jQuery: any;

@Component({
    
    selector: 'seller-tickets',
    providers: [NotifyService, UtilService, MessageService, UserService,CreditDebitNoteService],
    templateUrl: './seller-tickets.component.html'
})

export class SellerTicketsComponent {
    public MessageModel: ViewModels.ISellerCareViewModel;
    //public RepliesModel: ViewModels.IRepliesViewModel;

    Message: any;
    public TicketList: any;
    ShowUserProfile: any;
    ShowUserRole: any;
    errorMessage: any;
    Roles: any;
    public selectedInboxMessage: any;
    showAllMessages = true;
    showMail = false;
    replyMail = false
    Tickets = [];
    pageNumber: number;
    attachments = [];
    Replies: any;
    ticket_id: any;
    obj_id: any;
    sendReply: boolean;
    replyModel: any;
    hasAttach: boolean;
    replylen: number;
    totalPages = 1;
    pages=[];
    currentPage = 0;
    size = 10;
    from = 0;
    total = 0;
    initialPage=0;
    TicketsSpecific=[];
    searchTerm="";
    TicketListsOnScroll:any;
    public searchlist=[];
    public suggestList=[];
    displayShop:any;
    searchProducts="";
    UserId:any;
    userReplies:any;
    show = false;
    descriptionButtonText='View Details';
    caret='View Details <i class="fa fa-caret-down" aria-hidden="true"></i>';

    constructor(private router: Router,private loadingservice:NgxSpinnerService,private CreditDebitNoteService: CreditDebitNoteService, private toaster: NotifyService, private messageService: MessageService, private utilService: UtilService, private userService: UserService) {

        //this.RepliesModel=<ViewModels.IRepliesViewModel>{};
        this.Message = { "Subject": "", "Replies": [{ "Message": "", "Attachments": [] }] };
        this.sendReply = false;
        this.totalPages = 0;
        this.pageNumber = 0;
        this.Replies = [];
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response); 
                this.UserId=this.ShowUserProfile.Items[0]._id;
                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                    console.log('type of user'+this.ShowUserRole)
                    if(this.ShowUserRole!= 'Others'){
                     this.init();  
                    }
                    else{
                        this.router.navigate(['']);
                    }
            }
           );
        // this.ShowUserRole = [];  
    }
    init() {
        this.pages=[];
         window.scrollTo(0, 20);
        this.searchProducts='';
        this.findShop(this.searchProducts)

        this.MessageModel = <ViewModels.ISellerCareViewModel>{};
        this.MessageModel.Replies = <ViewModels.IRepliesViewModel[]>{};
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response);
                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;

            },
            error => this.errorMessage = <any>error);
        this.TicketList = [];
    
        this.from=0;
        this.size=10;
        this.messageService.getUserTickets(this.searchTerm,this.from,this.size)
            .then(
            tickets => {
                this.TicketsSpecific=[];
                this.currentPage=0;
                this.TicketList = JSON.parse(tickets).Items;
                this.obj_id = this.TicketList._id;
                this.Tickets = this.TicketList;
                this.total = JSON.parse(tickets).TotalItems;
                this.Tickets.forEach(ele =>{
                        this.TicketsSpecific.push(ele.Tickets);
                })
                console.log(this.TicketsSpecific);
               
                this.ValidateScrollContainer();
            },
            error => this.errorMessage = <any>error);
    }

    public getTicketOnScroll(){
        this.from += this.size;
        if (this.from < this.total) {
            this.currentPage += 1;
        this.messageService.getUserTickets(this.searchTerm,this.currentPage,this.size)
        .then(
        tickets => {
            this.TicketsSpecific=[];
            this.TicketList= JSON.parse(tickets);
                this.obj_id = this.TicketList._id;
                this.total = this.TicketList.TotalItems;                
                this.Tickets = this.Tickets.concat(this.TicketList.Items);
                this.Tickets.forEach(ele =>{
                        this.TicketsSpecific.push(ele.Tickets);
                       this. ValidateScrollContainer();
                })
                console.log(this.TicketsSpecific);
        },
        error => this.errorMessage = <any>error);        
    }
}
    test(value, i) {
        this.attachments.splice(i, 1);
        //console.log("Pop", this.attachments);
    }
    ChangeListener($event) {
        let files = [].slice.call($event.target.files);
        var file: File = files[0];
        var type=file.name.split('.').pop();
        // if (type == "jpeg" || type == "jpg" || type =="png" || type == "bmp" || type == "pdf" || type == "docx" || type == "txt" || type == "csv" || type == "xlsx" || type == "xlm" || type == "xlsm" || type == "xsl" || type =="doc" || type =="rtf") {
        if (type == "jpeg" || type == "jpg" || type =="png" || type == "bmp" ) {            
            if (file.size < 5*1024*1024) {
                if(this.attachments.length < 5)
                {
                    this.loadingservice.show();
                    let fileName = file.name.split(".")[0];
                    this.utilService.convertToBase64(file);
                    let count = 0;
                    this.utilService.invokeEvent.subscribe(value => {
                        if (count == 0) {
                            this.utilService.uploadBase64AndGetUrl(fileName, value).then(
                                response => {
                    //              console.log("Extract Imagedata from uploadBase64AndGetUrl", JSON.parse(response));
                                    let obj = { "Name": fileName, "URL": JSON.parse(response).ImageData };
                                    this.attachments.push(obj);
                                    this.loadingservice.hide();
                        //            console.log(" this.attachments ", this.attachments);
    
                                },
                                error => {
                                    this.toaster.ErrorSnack("Problem in uploading the image. Please try again");
                                });
                        }
                        count++;
                    });
                }
                else
                {
                    this.toaster.ErrorSnack("You can Upload Only 5 Files");
                }
            }
            else
            {
                this.toaster.ErrorSnack("File Size Exceeds the 5MB Limit");
            }
        }
        else
        {
            this.toaster.ErrorSnack("File Type is not supported");
        }
    }
    
   showMailView(){
    this.MessageModel.Subject="";
    this.MessageModel.Replies=<any>{};
    this.showMail=false;
    this.showAllMessages=false;
    this.replyMail=true;
   }
    SearchCheck(searchword,pageNum, size)
    {
            this.TicketList = [];
            this.Tickets = [];
           
            this.pages=[];
            this.currentPage=1;
            this.messageService.getUserTickets(searchword,pageNum-1,size).then(res=>{
                this.TicketsSpecific=[];
                this.TicketList = JSON.parse(res);
                this.obj_id = this.TicketList._id;
                this.Tickets = this.TicketList.Items;
                this.totalPages=this.TicketList.TotalPages;
                this.Tickets.forEach(ele =>{
                   // ele.Tickets.forEach(ticket=>{
                        this.TicketsSpecific.push(ele.Tickets);
                    //})
                })
                console.log(this.TicketsSpecific);
                for(let i = 1; i <= this.totalPages; i++)
                {
                this.pages.push(i);
                console.log(i);
                }
                this.initialPage=pageNum-1;
            }, err=>{
                this.errorMessage = <any>err;
            });

    }
    mailMessage() {

        this.MessageModel.Domain = "com";
        this.MessageModel.Replies["Attachment"] = this.attachments;
        if(this.searchProducts=="")
        {
            this.toaster.ErrorSnack("Please choose shop");
            return;
               

        }

        for(var i=0;i<this.searchlist.length;i++)
        {
           if(this.searchProducts==this.searchlist[i].ShopName)
           {
            this.MessageModel.UserId=this.searchlist[i].UserId;
            // this.MessageModel.Shop=this.searchlist[i].ShopName;
            this.MessageModel.Email=this.searchlist[i].Email;
            this.MessageModel.ShopId=this.searchlist[i].shopId;
            this.MessageModel.To=this.searchlist[i].UserId;
            this.MessageModel.Shop=this.searchlist[i].ShopName;
           } 
        }
     
        //this.MessageModel.Replies.Attachment.push(this.attachments);
        console.log("this.MessageModel", this.MessageModel);
        this.messageService.addTicket(this.MessageModel)
            .then(
            res => {
              //  console.log("Mail Send", res);
              var data=JSON.parse(res);
              if(data.Type)
              {
                this.toaster.ErrorSnack(data.msg);                
              }
              else
              {
                this.toaster.SuccessSnack("Mail Sent Successfully");
                this.attachments = [];
                this.showAllMessages = true;
                this.showMail = false;
                this.replyMail = false;
                this.MessageModel=<any>{}
                this.MessageModel.Replies=<any>{};
                
                this.init();
              }
            },
            error => this.errorMessage = <any>error);

    }

    ReplyMessage(Msg) {
        this.replyModel = { "UserId": this.selectedInboxMessage.UserId, "TicketId": this.ticket_id };
        this.replyModel.Replies = { "Message": Msg, "Attachment": this.attachments };
  //      console.log("this.replyModel", this.replyModel);
    //    console.log(this.obj_id, this.ticket_id);
        this.messageService.replyTicket(this.replyModel).then(
            res => {
      //          console.log("Reply Send", res);
                this.toaster.SuccessSnack( "Mail Sent Successfully");
                this.sendReply = false;
                this.showAllMessages = true;
                this.showMail = false;
                this.replyMail = false;
                //this.MessageModel=<any>{}
                this.MessageModel.Replies=<any>{};
                this.init();
               
            },
            error => this.errorMessage = <any>error);
        this.sendReply = false;
    }
    public showMessage(message) {
        this.sendReply = true;
        this.MessageModel.Subject = message.Subject;
        this.messageService.getUserReplies(message)
        .then(
        replies => {
          this.userReplies=JSON.parse(replies);
          console.log(this.userReplies);

        },
        error => this.errorMessage = <any>error);    
        //console.log("this.MessageModel", this.MessageModel.Subject, message);
        this.ticket_id = message._id;
        //console.log("this.ticket_id", this.ticket_id);
        this.showAllMessages = false;
        this.showMail=true;

        this.selectedInboxMessage = message;
    }
    public closeTicket(ticket)
    {
        this.messageService.closeTicket(ticket).then(
            res => {
                this.toaster.ErrorSnack("Ticket Successfully Closed");
                this.showMail=false;this.showAllMessages=true;this.replyMail=false;                
                this.init();
            });
    }
    public reopenTicket(ticket)
    {
        this.messageService.reopenTicket(ticket).then(
            res => {
                this.toaster.ErrorSnack("Ticket Successfully Reopened");
                this.showMail=false;this.showAllMessages=true;this.replyMail=false;
                this.init(); 
            });
    }
    LoadOnScroll() {
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        if(scrollContainer!=null && scrollBox!=null){
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

        if (scrollLength - scrollContainer.scrollTop < 1) {
            setTimeout(() => {
                this.getTicketOnScroll();
            }, 500)
        }
    }
    }

    ResetPagination() {
        this.from = 0;
        this.total = 0;
        jQuery('#scroll-container').css("height", 500);
    }

    ValidateScrollContainer() {
        setTimeout(() => {
            var scrollContainer = document.getElementById('scroll-container');
            var scrollBox = document.getElementById('scroll-box');
            var scrollContainerPos = scrollContainer.getBoundingClientRect();
            var scrollBoxPos = scrollBox.getBoundingClientRect();
            if (scrollBoxPos.height < scrollContainerPos.height) {
                jQuery('#scroll-container').css("height", "625");
            }
        }, 1000)
    }
    updatePages(searchTerm,from,total){
        this.from=0;
        this.searchTerm=searchTerm;
        if(isNaN(searchTerm) || searchTerm=="")
        {
            this.init();
        }
        else{
            if(searchTerm.length==10)
            {
                this.init();
            }
        }
        
    } 
    findShop(searchProduct){
     
        // this.searchTerm=searchTerm;
        if(isNaN(searchProduct)||searchProduct=="")
        {
            this.CreditDebitNoteService.searchTerm(searchProduct).then(
                response => {
                    this.suggestList=[];
                    this.searchlist = JSON.parse(response);
                    // for(var i=0;i<this.searchlist.length;i++)
                    // {
                    //  this.suggestList.push(this.searchlist[i].ShopName)
                    // }
                    // for(var i=0;i<this.searchlist.length;i++)
                    // {
                   
                    //      this.displayShop=this.searchlist[i].ShopName;
                     
                    // }
                    this.displayShop=this.searchProducts;
                   console.log('searched term',this.searchlist);
                },
                error => this.errorMessage = <any>error);
    

        }
        else{
          
            if(searchProduct.length==10)
            this.CreditDebitNoteService.searchTerm(searchProduct).then(
                response => {
                    // this.suggestList=[];
                    this.searchlist = JSON.parse(response);
                    // this.displayShop=this.searchProducts;
                    // for(var i=0;i<this.searchlist.length;i++)
                    // {
                   
                    //      this.displayShop=this.searchlist[i].ShopName;
                     
                    // }
                   console.log('searched term',this.searchlist);
                },
                error => this.errorMessage = <any>error);
    
        }
        
       
    }
    displayDropDown()
    {
        jQuery('#inputvalue').removeClass('hidedropdown');
    }
    onItemSelect(selected) {
        if (selected) {
            this.searchProducts=selected;
        //  for(var i=0;i< this.searchlist.length;i++)
        //  {
        //      if(this.searchProducts==this.searchlist[i].ShopName)
        //      {
        //          this.SellerId=this.searchlist[i].UserId;
        //          this.shopid=this.searchlist[i].shopId;
        //          this.displayShop=this.searchlist[i].ShopName; 
        //         this.displayShop=this.searchProducts;
        //          jQuery('#inputvalue').addClass('hidedropdown');
        //      }
        //  }
        // this.displayShop=this.searchProducts;
        jQuery('#inputvalue').addClass('hidedropdown');
            console.log("Item Selected "+selected);
            // this.router.navigate(['/user/search', selected.originalObject]);
        }
    }
    showMore(event)
    {

        var target = event.currentTarget;

        if(this.show==true)
        {
            this.show = false;     
            target.previousElementSibling.classList.add('container_show');
            target.previousElementSibling.classList.remove('show');
            // this.descriptionButtonText='show more';
            target.innerHTML="View Details";
            this.caret='View Details <i class="fa fa-caret-down" aria-hidden="true"></i>';
        }
        else
        {
            this.show = true;
            target.previousElementSibling.classList.remove('container_show');
            target.previousElementSibling.classList.add('show');
            // this.descriptionButtonText='show less';
            target.innerHTML="Hide Details";
            this.caret='Hide Details <i class="fa fa-caret-up" aria-hidden="true"></i>';
        }
    }

}
