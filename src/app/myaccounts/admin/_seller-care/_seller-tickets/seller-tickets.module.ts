import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SellerTicketsComponent } from './seller-tickets.component';
import { SharedModule } from '../../../../shared/shared.module';
import { MatInputModule } from '@angular/material';


const sellertickets: Routes = [
{path: '', component:SellerTicketsComponent},
]
@NgModule({
imports: [
SharedModule,
RouterModule.forChild(sellertickets),
MatInputModule
],
declarations: [
SellerTicketsComponent
],
schemas:[
NO_ERRORS_SCHEMA
]
})
export class SellerTicketsModule { }