import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// import {CalendarModule} from 'primeng/primeng';
import { SharedModule } from '../../shared/shared.module';
// import {ToasterModule} from 'angular2-toaster';
// import { Ng2CompleterModule } from "ng2-completer";
// import { RatingModule } from "ngx-rating";
// import { Select2Module } from '../../../assets/ng2-select2';
// import {RlTagInputModule} from 'angular2-tag-input';
// import {MultiSelectModule} from 'primeng/primeng';
// import {DropdownModule} from 'primeng/dropdown';
import { Routes, RouterModule } from '@angular/router';
  
  
const admin: Routes = [
  {path: 'master-data', loadChildren:'./_masterdata/masterdata.module#MasterDataModule'},
  {path: 'settlement', loadChildren:'./_settlements/settlements.module#SettlementModule'},
  {path: 'featured-shop', loadChildren:'./_featured-shop/featured-shop.module#FeaturedShopModule'},
  {path: 'credit-debit', loadChildren:'./_credit-note/credit-note.module#CreditNoteModule'},
  {path: 'invoice-details', loadChildren:'./_invoice-admin/invoice-admin.module#InvoiceModule'},
  {path: 'special-offer', loadChildren:'./_special-offer/special-offer.module#SpecialOfferModule'},
  {path: 'holiday-list', loadChildren:'./_holiday-list/holiday-list.module#HolidayListModule'}, 
  {path: 'zone', loadChildren:'./_zone/zone.module#ZoneModule'},
  {path: 'admin-criteria', loadChildren:'./_admin-criteria/admin-criteria.module#AdminCriteriaModule'},
  {path: 'sub-admin', loadChildren:'./_sub-admin-creation/sub-admin-creation.module#SubAdminCreationModule'},
  {path: 'forum-group', loadChildren:'./_forum-group-creation/forumgroupcreation.module#ForumGroupModule'},
  {path: 'upload-banner', loadChildren:'./_upload-banner/upload-banner.module#UploadBannerModule'},
  {path: 'payment-complete', loadChildren:'./_payment-complete/payment-complete.module#PaymentCompleteModule'},
  {path: 'mega-menu', loadChildren:'./_mega-menu-choose/mega-menu-choose.module#MegaMenuModule'},
  {path: 'three-column', loadChildren:'./_three-column/three-column.module#ThreeColumnModule'},
  {path: 'block-user', loadChildren:'./_block-user/blockuser.module#BlockUserModule'},
  {path: 'reports', loadChildren:'./_admin-data-reports/admin-data-reports.module#AdminReportsModule'},
  {path: 'return-reason', loadChildren:'./_return-reason/return.module#ReturnModule'},
  {path: 'seller-tier', loadChildren:'./_seller-tier/seller-tier.module#SellerTierModule'},
  {path: 'seller-commission', loadChildren:'./_seller-commission/seller-commission.module#SellerCommissionModule'},
  {path: 'order-tickets', loadChildren:'./_seller-care/_admin-order-tickets/admin-order-tickets.module#AdminOrderTicketsModule'},
  {path: 'seller-tickets', loadChildren:'./_seller-care/_seller-tickets/seller-tickets.module#SellerTicketsModule'},
  {path: 'approve-seller', loadChildren:'./_seller-care/_approve-seller/approve-seller.module#ApproveSellerModule'},
  {path: 'incomplete-seller', loadChildren:'./_seller-care/_incomple-sellerdetails/incomplete-sellerdetails.module#IncompleteSellerModule'},
  {path: 'editbusinessseller', loadChildren:'./_seller-care/_edit-business-seller/editbusinessseller.module#EditBusinessSellerModule'}

]
@NgModule({
  imports: [
    SharedModule,
    // ToasterModule,
    // Ng2CompleterModule,
    // Select2Module,
    // RatingModule,
    // CalendarModule,
    // RlTagInputModule,
    // MultiSelectModule,
    // DropdownModule,
    RouterModule.forChild(admin)
  ],
  declarations: [
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class AdminModule { }