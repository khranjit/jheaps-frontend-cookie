import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvoiceAdmin } from './invoice-admin.component';
import { SharedModule } from '../../../shared/shared.module';
  
  
const invoiceadmin: Routes = [
    {path: '', component:InvoiceAdmin},
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(invoiceadmin)
  ],
  declarations: [
    InvoiceAdmin
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class InvoiceModule { }