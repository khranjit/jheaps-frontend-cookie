import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreditNote } from './credit-note.component';
import { AddNewNote } from './_add-newNote/add-newNote.component';
import { SharedModule } from '../../../shared/shared.module';
import {DropdownModule} from 'primeng/dropdown';
import { Ng2CompleterModule } from "ng2-completer";
  
const creditnote: Routes = [
    {path: '',redirectTo:'view',pathMatch:'full'},
    {path: 'view', component:CreditNote},
    {path: 'add', component:AddNewNote},
]
@NgModule({
  imports: [
    SharedModule,
    DropdownModule,
    Ng2CompleterModule,
    RouterModule.forChild(creditnote)
  ],
  declarations: [
    CreditNote,
    AddNewNote
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class CreditNoteModule { }