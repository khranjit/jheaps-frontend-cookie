import { Component } from '@angular/core';
import { NotifyService } from '../../../../services/notify.service';
import { MessageService } from '../../../../services/messages.service';
import { UtilService } from '../../../../services/util.service';
import { UserService } from '../../../../services/user.service';
import { CreditDebitNoteService } from '../../../../services/credit-debit-note.service';
import { ActivatedRoute, NavigationExtras } from '@angular/router';
import { Router } from '@angular/router';
import { CompleterItem } from 'ng2-completer';

@Component({

    selector: 'addnewnote',
    providers: [NotifyService, UtilService, MessageService, UserService, CreditDebitNoteService],
    templateUrl: './add-newNote.component.html'
})


export class AddNewNote {

    getSellerList = [{ "label": "Select a shop" }];
    sellerName: any;
    SellerId: string = "" ;
    Credit: Boolean;
    Debit: Boolean;
    errorMessage: any;
    newbutton = true;
	editbutton = false;
    public CreditNoteModel = <any>{};
    public creditDetails = <any>{};
    public editdetails:any;
    public userName: string;
    public getAllSellerList: any;
    public pages: any[];
    ShowUserRole: any;
    ShowUserProfile: any;
    totalPages = 0;
    currentPage = 1;
    from = 0;
    size = 10;
    total = 0;
    title = "Credit";
    searchShop="";
    public searchlist=[];
    public suggestList=[];
    public shopid;
    searchProducts:any;
    displayShop:any;
    constructor(private userService: UserService, private route: ActivatedRoute, private toasterPopService: NotifyService, private CreditDebitNoteService: CreditDebitNoteService, private router: Router) {

        // this.userService.getAllSellers(this.userName, 0).then(
        //     response => {
        //         this.getAllSellerList = JSON.parse(response).Items;
        //         this.getAllSellerList.forEach(value => {
        //             let obj = { "label": value.UserName, "value": value.UserName };
        //             this.getSellerList.push(obj);
        //         });
        //     },
        //     error => this.errorMessage = <any>error);
    
        this.userService.getAllShop().then(
            response => {
                this.getAllSellerList = JSON.parse(response);
                var shopdetail=this.getAllSellerList;
                //console.log('shopdetails',shopdetail);
                shopdetail.forEach(value => {
                                let obj = { "label": value.shopName, "value": value.shopName };
                                this.getSellerList.push(obj);
                            });
                    console.log('shopdetails',this.getSellerList);
            },
            error => this.errorMessage = <any>error);
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response);
                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                console.log('type of user' + this.ShowUserRole)
                if (this.ShowUserRole == 'masteradmin' || this.ShowUserRole == 'admin') {

                }
                else {
                    this.router.navigate(['']);
                }
            }
        );
        this.Init();
    }
    public Init() {
        this.searchProducts='';
     
        this.route.queryParams.subscribe(params => {
			if (params['Editnote']) {
                this.editdetails = JSON.parse(params['Editnote']);
                this.creditDetails.Note=this.editdetails.Note;
                this.creditDetails.Price=this.editdetails.Price;
                console.log(this.creditDetails);

                if (this.editdetails.Credit == true) {
                    this.creditDetails.Credit = 0;
                }
                else {
                    this.creditDetails.Credit = 1;
                }
                this.SellerId=this.editdetails.SellerId._id;
             
        
                this.editbutton = true;
                this.newbutton = false;
            
				
			}
        });
        this.updatePages(this.searchProducts);
    }
    save() {
        if(this.SellerId=="" || this.shopid==null)
        {
            this.toasterPopService.ErrorSnack( 'Please choose shop')
        }
        this.Credit = false;
        this.Debit = false;
        if (this.creditDetails.Credit == 0) {
            this.Credit = true;
        }
        else {
            this.Debit = true;
        }
        var CreditNoteModel = {
            Credit: this.Credit,
            Debit: this.Debit,
            Note: this.creditDetails.Note,
            Price: this.creditDetails.Price,
            SellerId: this.SellerId,
            ShopId:this.shopid,
        }
        this.CreditDebitNoteService.setAll(CreditNoteModel).then(res => {
            this.toasterPopService.SuccessSnack( 'Added successfully')
            this.router.navigate(['/myaccounts/admin/credit-debit']);
        })
    }
    Update(){
        this.Credit = false;
        this.Debit = false;
        if (this.creditDetails.Credit == 0) {
            this.Credit = true;
        }
        else {
            this.Debit = true;
        }
        var CreditNoteModel = {
            Credit: this.Credit,
            Debit: this.Debit,
            Note: this.creditDetails.Note,
            Price: this.creditDetails.Price,
            SellerId: this.SellerId,
            ShopId:this.shopid,
            _id: this.editdetails._id
        }
        this.CreditDebitNoteService.update(CreditNoteModel).then(res => {
            
            this.toasterPopService.SuccessSnack( 'Updated successfully')
            this.router.navigate(['/myaccounts/admin/credit-debit']);
        })

    }

    goBack() {
        this.router.navigate(['/myaccounts/admin/credit-debit']);
      }

    titleChange(i)
    {
        this.title=i;
    }
    updatePages(searchProduct){
     
        // this.searchTerm=searchTerm;
        if(isNaN(searchProduct)||searchProduct=="")
        {
            this.CreditDebitNoteService.searchTerm(searchProduct).then(
                response => {
                    this.suggestList=[];
                    this.searchlist = JSON.parse(response);
                    // for(var i=0;i<this.searchlist.length;i++)
                    // {
                    //  this.suggestList.push(this.searchlist[i].ShopName)
                    // }
                    for(var i=0;i<this.searchlist.length;i++)
                    {
                     if(this.searchlist[i].UserId==this.SellerId)
                     {
                         this.displayShop=this.searchlist[i].ShopName;
                     }
                    }
                   console.log('searched term',this.searchlist);
                },
                error => this.errorMessage = <any>error);
    

        }
        else{
          
            if(searchProduct.length==10)
            this.CreditDebitNoteService.searchTerm(searchProduct).then(
                response => {
                    // this.suggestList=[];
                    this.searchlist = JSON.parse(response);
                    for(var i=0;i<this.searchlist.length;i++)
                    {
                     if(this.searchlist[i].UserId==this.SellerId)
                     {
                         this.displayShop=this.searchlist[i].ShopName;
                     }
                    }
                   console.log('searched term',this.searchlist);
                },
                error => this.errorMessage = <any>error);
    
        }
        
       
    }
    onItemSelect(selected) {
        if (selected) {
            this.searchProducts=selected;
         for(var i=0;i< this.searchlist.length;i++)
         {
             if(this.searchProducts==this.searchlist[i].ShopName)
             {
                 this.SellerId=this.searchlist[i].UserId;
                 this.shopid=this.searchlist[i].shopId;
                 this.displayShop=this.searchlist[i].ShopName; 
                 jQuery('#inputvalue').addClass('hidedropdown');
             }
         }
            console.log("Item Selected "+selected);
            // this.router.navigate(['/user/search', selected.originalObject]);
        }
    }
    displayDropDown()
    {
        jQuery('#inputvalue').removeClass('hidedropdown');
    }
    // key_down($event,searchProducts)
    // { 
        
    //     this.CreditDebitNoteService.searchTerm(searchProducts).then(
    //     response => {
    //         this.suggestList=[];
    //         this.searchlist = JSON.parse(response);
    //         for(var i=0;i<this.searchlist.length;i++)
    //         {
    //          this.suggestList.push(this.searchlist[i].ShopName)
    //         }
    //        console.log('searched term',this.searchlist);
    //     },
    //     error => this.errorMessage = <any>error);

    //     console.log(searchProducts); 
    // }
}
