import { Component } from '@angular/core';
// import { ToasterPopService } from '../../services/toasterpop.service';
import { ActivatedRoute,  NavigationExtras } from '@angular/router';
import { MessageService } from '../../../services/messages.service';
import { UtilService } from '../../../services/util.service';
import { UserService } from '../../../services/user.service';

import { NotifyService } from '../../../services/notify.service';
import { CreditDebitNoteService } from '../../../services/credit-debit-note.service';
import { Router } from '@angular/router';

@Component({
    selector: 'credit-note',
    providers: [NotifyService, UtilService, MessageService, UserService, CreditDebitNoteService],
    templateUrl: './credit-note.component.html'
})


export class CreditNote {    
    ShowUserRole: any;
    ShowUserProfile: any;
    CreditList: any;
    DebitList = [];
    pages = [];
    totalPages = 0;
    size = 10;
    currentPage = 1;
    from = 0;
    total = 0;   
    searchTerm="";
    public errorMessage: any;

    constructor(private userService: UserService, private toasterPopService:NotifyService,private route: ActivatedRoute, private CreditDebitNoteService: CreditDebitNoteService, private router: Router) {
        
        
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response);
                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
                console.log('type of user' + this.ShowUserRole)
                if (this.ShowUserRole == 'masteradmin' || this.ShowUserRole == 'admin') {
                    this.Init();
                }
                else {
                    this.router.navigate(['']);
                }
            }
        );
    }

    public Init() {
        window.scrollTo(0,20);
        this.currentPage = 1;
        this.pages = [];
        this.getCreditList(this.searchTerm,this.from, this.size);
      }

      getCreditList(searchTerm,from, size){
        this.CreditDebitNoteService.getAll(searchTerm,from, size).then(res=>{
            this.CreditList = JSON.parse(res)
            this.DebitList = this.CreditList;
            this.pages = [] 
            this.totalPages = Math.ceil(this.CreditList.TotalItems /this.size);

           for(let i = 1; i <= this.totalPages; i++){
           this.pages.push(i);
            }
        },
        error => this.errorMessage = <any>error);
      }

    addNew(){
    this.router.navigate(['/myaccounts/admin/credit-debit/add']);
    }
    edit(editNoteModel){
    	let navimodel: NavigationExtras = {
	    	queryParams: { 'Editnote': JSON.stringify(editNoteModel) }
	    }
		this.router.navigate(['/myaccounts/admin/credit-debit/add'], navimodel);
		}
    confirmDelete(value){
        this.CreditDebitNoteService.delete(value).then(
            response => {
                this.from = 0;
                this.getCreditList(this.searchTerm,this.from, this.size);
                this.toasterPopService.SuccessSnack('Deleted Successfully!!');
            },
            error => {
                this.toasterPopService.ErrorSnack('There is an error in deleting your credit note');
            }

        );

    }
   
    previousPage(){
        if(this.currentPage > 1){
            this.getCreditList( this.searchTerm,((this.currentPage - 1)*this.size), this.size *this.currentPage); 
            this.currentPage -= 1;  
        }
    }
    
    nextPage(){
        if(this.currentPage < this.totalPages){
            this.getCreditList( this.searchTerm,((this.currentPage )*this.size), this.size *(this.currentPage +1));
            this.currentPage += 1;  
        }
    }
    
    selectPage(PageNumber) {
        this.currentPage = PageNumber;
        this.getCreditList(this.searchTerm, ((this.currentPage - 1)*this.size), this.size * (this.currentPage));
        
    }
    updatePages(searchTerm){
        this.from=0;
        this.searchTerm=searchTerm;

        // this.init();
        if(isNaN(searchTerm)||searchTerm=="")
        {
        this.getCreditList(searchTerm,this.from, this.size)
        }
        else{
            if(searchTerm.length==10)
            {
                this.getCreditList(searchTerm,this.from, this.size)
            }
        }
    
        console.log("print");
    }
}
