import { Component, OnInit, HostListener, Directive, ElementRef, Input, Output, Renderer, ChangeDetectionStrategy, ViewEncapsulation, NgZone, Inject, ViewChild } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { LoginService } from './services/login.service';
import { ToasterService } from 'angular2-toaster';
import { ToasterPopService } from './services/toasterpop.service';
import { ProductDescriptionService } from './services/product-description.service';
import { RegisterService } from './services/register.service';
import { AddToCartService, TempCartService } from './services/cart.service';
import { CategoryService } from './services/category.service';
import { AdminCriteriaService } from './services/admin-criteria.service';
import { UserService } from './services/user.service';
import { UtilService } from './services/util.service';
import { ForumService } from './services/forum.service';
import { ElasticService } from './services/elastic.service';
// import { CookieService } from 'ngx-cookie';
import { HomeService } from './services/home.service';
import 'rxjs/add/operator/filter';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { CompleterItem } from 'ng2-completer';
import { LoginRedirectService, LogoutRedirectService } from './services/login.service';
import { NotifyService } from './services/notify.service';

import { CookieService } from 'ngx-cookie-service';

declare var jQuery: any;


/////////////////////////
// ** Example Directive
// Notice we don't touch the Element directly
@Directive({
    selector: '[xLarge]'
})
export class XLargeDirective {
    constructor(element: ElementRef, renderer: Renderer) {
        // ** IMPORTANT **
        // we must interact with the dom through -Renderer-
        // for webworker/server to see the changes
        renderer.setElementStyle(element.nativeElement, 'fontSize', 'x-large');
        window.scrollTo(0, 0);
        // ^^
    }
}
@Component({
    selector: 'app-root',
    providers: [Location, { provide: LocationStrategy, useClass: PathLocationStrategy }, ToasterService, CookieService,UserService, ProductDescriptionService, LoginService, RegisterService, AddToCartService, ForumService, UtilService, CategoryService, AdminCriteriaService, ElasticService, ToasterPopService, NotifyService],
    templateUrl: "app.component.html",
    encapsulation: ViewEncapsulation.None,
    // directives : [Ng4LoadingSpinnerComponent]
})
export class AppComponent implements OnInit {
    public loginModel: ViewModels.ILoginViewModel;

    public registerModel: ViewModels.IRegistrationViewModel;
    // public defaultRegisterModel: ViewModels.IRegistrationViewModel;
    public gloginModel: ViewModels.IThirdPartyLoginViewModel;
    public fbloginModel: ViewModels.IThirdPartyLoginViewModel;
    public googleCodeResponse: any;
    public defaultRegisterModel = { Profile: { Address: {} } };
    ShowUserProfile: any;
    ShowUserName = "";
    ShowUserRole = "";
    ShowDisplayPicture = "";
    loginInfo: any;
    cartProductsList: any;
    cartProducts = 0;
    registerInfo: any;
    errorMessage: any;
    ErrorCodes: any;
    loginshow = true;
    mobtrue = true;
    public logoutshow;
    myaccount = false;
    bool: boolean;
    ErrorUser = false;
    ErrorFacebook = false;
    cartSubTotal = 0;
    showCategoryList: any;
    level: any;
    getPassword = false;
    OTPVerify = false;
    OTP: any;
    numberExist = true;
    OTPWrong = false;
    OTPSent = false;
    forgotMobile: any;

    umail = ['ab@gmail.com', 'bc@gmail.com', 'cd@gmail.com'];
    uid = ['mani', 'moni', 'kani'];
    ex: number;

    parentCategoryNames: any;
    childrenCategoryNames: any;
    categoriesHomePage: any;
    moreCategoriesHomePage: any;
    categoryNames: any;
    parentCategoryIndex: any;
    viewShopByNames: any;
    isUserNameNotExist: boolean;
    isUserMailNotExist: boolean;
    parentIndex: any;
    categoryLength: any;
    shopByList: any;
    ipAddress: any;
    currentRoute: string = '';
    forgotDetails: boolean;
    countriesArray: any;
    statesArray: any;
    registerErrorCode: any;
    registerSuccessCode: any;
    fbcode = "code";
    submitAttempt = true
    forgotPasswordModel = {};
    getUserToken: any;
    searchProducts: any;
    public suggestionList = [];
    sortmenu = [];
    value: any;
    default: any;
    shopID: any;
    productID: any;
    hide:boolean = false;
    public searchArray = [];


    checkuser: boolean;
    @ViewChild('registerform') registerform: HTMLFormElement;
    constructor(private router: Router, private productDescriptionService: ProductDescriptionService, private loginRedirectService: LoginRedirectService, private logoutRedirectService: LogoutRedirectService, private tempCart: TempCartService, private activatedRouter: ActivatedRoute, private categoryService: CategoryService, private loginService: LoginService, private registerService: RegisterService,
        private adminCriteriaService: AdminCriteriaService, private addToCartService: AddToCartService, private utilService: UtilService,
        private _ngZone: NgZone, private _cookieService: CookieService, private forumService: ForumService, private location: Location,
        private elasticService: ElasticService, private homeService: HomeService, private tempCartService: TempCartService, private toasterPopService: ToasterPopService, private userService: UserService, private snack: NotifyService) {
        this.Category();
        this.parentCategoryNames = [];
        this.childrenCategoryNames = [];
        this.categoriesHomePage = [];
        this.moreCategoriesHomePage = [];
        this.categoryNames = [];
        this.parentCategoryIndex = [];
        this.showAdminShopBy();
        // this.getGeoLocation();
        this.countriesArray = [];
        this.statesArray = [];

        this.getUserToken = this._cookieService.get('userToken');
        this.getUserToken = this._cookieService.get('userToken')
       

        this.router.events.subscribe((route) => {
            let newRoute = this.location.path() || '/';
            if (this.currentRoute != newRoute) {
                this.currentRoute = newRoute;
                if (this.currentRoute == "/home" || this.currentRoute == "/") {
                    this.searchProducts = '';
                }
            }
        });
        setTimeout(function () {
            jQuery('.ng2-auto-complete-wrapper').addClass('input-group-lg');
        }, 200);
    }

    ngOnInit() {

        this.isUserNameNotExist = true;
        this.isUserMailNotExist = true;
        const allCookies: {} = this._cookieService.getAll();
        console.log("dasdasdasd");
        console.log(allCookies);

        // var createGuest = require('cross-domain-storage/guest')
        // var createHost = require('cross-domain-storage/host');

        // var storageHost = createHost([
        //     {
        //         origin: 'http://localhost:5200',
        //         allowedMethods: ['get', 'set', 'remove']
        //     },
        //     {
        //         origin: 'http://localhost:5000',
        //         allowedMethods: ['get']
        //     }
        // ]);
        // storageHost.close();

        // var bazStorage = createGuest('http://localhost:5000/accessStorage');                         
        // bazStorage.get('role', function(error, value) {
        //     console.log(value)
        // });

        //console.log("orgin:", location.origin);
        this.activatedRouter.queryParams.subscribe(
            (queryParam: any) => {
                this.fbcode = queryParam['code'];
                //console.log("orgin:", location.origin);
                if (this.fbcode != undefined) {
                    this.fbloginModel = <ViewModels.IThirdPartyLoginViewModel>{
                        "ReceiveZgrooNewsLetters": true,
                        "AcceptTermsAndConditions": true,
                        "IsRegistration": true,
                        "code": this.fbcode + '#_=_',
                        "redirectUri": location.origin + "/"
                    }
                    this.loginService.fblogin(this.fbloginModel).then(
                        loginInfo => {
                            this.loginInfo = JSON.parse(loginInfo);
                            if (this.loginInfo.Token) {
                                localStorage.setItem('userToken', this.loginInfo.Token);
                                this.IsUserAuthorized();
                                this.myaccount = !this.myaccount;
                                this.userName();
                                jQuery("#loginmodal").modal('hide');
                                this.cart();
                                this.loginRedirectService.newEvent();
                                /* Set Cookie for NodeBB*/
                                if (this._cookieService.get("auth")) {
                                    // this._cookieService.remove("auth");
                                }
                                let authObj = '{"HttpOnly": true,"domain": ".zgroo.com"}';
                                // return this._cookieService.put("auth", authObj);
                            }
                            else if (this.loginInfo.message == 'Email Not Found') {
                                jQuery("#loginmodal").modal("show");
                                jQuery('.nav-tabs a[href="#register_modal"]').tab('show');
                                this.snack.ErrorSnack("Kindly Register");
                            }
                            else {
                                this.ErrorUser = !this.ErrorUser;
                            }
                        }, error => this.errorMessage = <any>error);
                }
            });
        // this.loadScript('assets/js/slick.min.js');
        // this.loadScript('assets/js/custom.js');
        this.loginModel = <ViewModels.ILoginViewModel>{};
        this.gloginModel = <ViewModels.IThirdPartyLoginViewModel>{};
        this.fbloginModel = <ViewModels.IThirdPartyLoginViewModel>{};
        this.registerModel = <ViewModels.IRegistrationViewModel>{};
        this.registerModel.Profile = <any>{ Address: <any>{} };
        //this.cart();
        window.scrollTo(0, 0);
        this.getDetails();
        this.IsUserAuthorized();
        this.userName();
    }

    public IsUserAuthorized() {
        this.logoutshow = this.loginService.isLoggedIn();
        this.shopID = this.tempCartService.getShopID();
        this.productID = this.tempCartService.getProductID();
        if (this.shopID != null) {
            this.homeService.addFavouriteShop(this.shopID);
            this.tempCartService.clearShopID();

        }
        if (this.productID != null) {
            this.homeService.addFavouriteProduct(this.productID);
            this.tempCartService.clearProductID();

        }
    }

    GoToContactPage() {
        this.router.navigate(['/user/contact']);
        window.scrollTo(0, 0);
        
    }
    GoToAboutUs() {
        this.router.navigate(['/user/aboutus']);
        window.scrollTo(0, 0);
        
    }

    CheckPinCode() {
        var model = { country: this.registerModel.Profile.Address.Country, pincode: this.registerModel.PostalCode };
        this.registerService.checkPincode(model).then(result => {
            var pincode = JSON.parse(result);
            var newError = <any>{};
            var oldError = this.registerform.form.controls['PostalCode'].errors;
            if (pincode.flag == 2) {
                newError.incorrect = true;
                this.registerform.form.controls['PostalCode'].setErrors(newError);
            }
        });
    }

    public userName() {
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response).Items[0];
                if (this.ShowUserProfile.Profile.FirstName) {
                    this.ShowUserName = this.ShowUserProfile.Profile.FirstName;
                } else {
                    this.ShowUserName = '';
                }
                this.ShowDisplayPicture = this.ShowUserProfile.Profile.DisplayPicture;
                this.ShowUserRole = this.ShowUserProfile.Roles;
                localStorage.setItem('role', this.ShowUserRole);
            },
            error => {
                this.errorMessage = <any>error;
                this.logoutshow = false;
                localStorage.removeItem('userToken');
                localStorage.removeItem('role');
            });

    }

    public Category() {
        this.categoryService.showCategories().then(
            response => {
                this.showCategoryList = JSON.parse(response);
                for (let j = 0; j < this.showCategoryList.length; j++) {
                    if (this.showCategoryList[j].Parent.HomePage > 0) {

                        this.sortmenu.push(this.showCategoryList[j]);
                    }
                    else {
                        this.moreCategoriesHomePage.push(this.showCategoryList[j])
                    }
                }
                this.sortmenu.sort(function (a, b) {
                    return a.Parent.HomePage - b.Parent.HomePage;
                });
                console.log(this.sortmenu);

                this.categoriesHomePage = this.sortmenu;
            },
            error => this.errorMessage = <any>error);

    }
 
    public getGeoLocation() {
        let currentUrl = window.location.href;
        if (!currentUrl.match("localhost")) {
            let urlSplit = currentUrl.split('://')[1].split('.').slice(0, 3)[2].split('/');
            let Url = urlSplit[0];
        }

    }
    public loadScript(url) {
        let node = document.createElement('script');
        node.src = url;
        node.type = 'text/javascript';
        document.getElementsByTagName('head')[0].appendChild(node);
    }

    getSuggestions() {
        this.searchArray = [];
        let hitsList = [];
        var length = 3;
        this.suggestionList = [];
        if (this.searchProducts) {
            var searchString = new String(this.searchProducts);
            var ShopSearch = async(self,count) =>{
                if(searchString.length > count)
                {
                    return await self.elasticService.getShopSearchSuggestions(this.searchProducts)
                    .then(products => {
                        hitsList = JSON.parse(products).hits.hits;
                        if (hitsList.length > 0) {
                            hitsList.forEach(hit => {
                                if (hit._source.displayName.toLowerCase().indexOf(this.searchProducts.toLowerCase()) >= 0) {
                                    self.checkAndPushToSuggestions(hit._source.displayName+" in shop", false, true, hit._source.shopId);
                                }
                            });
                            return true;
                        }
                    },
                        error => this.errorMessage = <any>error);
                }
                else
                {
                    return true;
                }
            }
            ShopSearch(this,length).then(()=>{
                this.elasticService.getSearchSuggestions(this.searchProducts)
                .then(products => {
                    hitsList = JSON.parse(products).hits.hits;
                    if (hitsList.length > 0) {
                        hitsList.forEach(hit => {
                            if (hit._source.Name.toLowerCase().indexOf(this.searchProducts.toLowerCase()) >= 0) {
                                this.checkAndPushToSuggestions(hit._source.Name, true, false, hit._id);
                            }
                        });
                    }
                    else
                    {
                        if(this.suggestionList.length == 0) {
                            ShopSearch(this,0);
                        }
                    }
                },
                    error => this.errorMessage = <any>error);
            })
        }
    }

    favourite() {
        alert(1);
    }

    checkAndPushToSuggestions(suggestion, isCategory, shop, id) {
        if (this.suggestionList.indexOf(suggestion) < 0) {
            if (isCategory) {
                this.suggestionList.unshift(suggestion);
            } else {
                this.suggestionList.push(suggestion);
                this.searchArray.push({id:id,search:suggestion,shop:shop});
            }
        }
    }

    onItemSelect(selected: CompleterItem) {
        var url = '/user/search/' + selected.originalObject;
        this.searchArray.forEach(x=>{
            if(x.search == selected.originalObject)
            {
                if(x.shop)
                {
                    this.searchProducts = ""
                    url = 'user/shop/' + x.id;
                }
                else
                {
                    url = '/user/search/' + selected.originalObject;
                }
            }
        })
        if (selected) {
            this.searchArray = []
            this.router.navigate([url]);
        }
    }

    search(term) {
        if (term != 'null' && term != "") {
            this.router.navigate(['/user/search', term]);
        }
    }

    public auth2: any
    ngAfterViewInit() {
    }
    key_down(e, searchProducts) {
        if (e.keyCode === 13) {
            this.search(searchProducts);
        }
    }

    navigateToAdminCriteria(criteria) {
        let link = ['/user/criteria', criteria._id];
        this.router.navigate(link);
    }

    navigateToCategory(parent,child) {
        let link;
        if(child)
        {
            link = ['/user/search/'+child.Name+'/'+parent._id+'/'+child._id];
        }
        else
        {
            link = ['/user/search/'+parent.Name+'/'+parent._id];
        }
        this.router.navigate(link);
        // this.searchCategory.Set(parent,child).then(()=>{
            
        // });
    }

    public async cart() {
        this.getUserToken = this._cookieService.get('userToken');
        var msg;
        var i = 0;
        if (this.getUserToken != null) {
            var tempList = [];
            tempList = this.tempCart.GetTempCart();
            if (tempList != null && tempList.length > 0) {
                var totalItems = tempList.length;
                var process = async function (self) {
                    for (var k = 0; k < totalItems; k++) {
                        var cartProduct = tempList[k];
                        if (cartProduct != 'Empty') {
                            await self.addToCartService.setCartProduct(cartProduct)
                                .then(
                                    cartItems => {
                                        var cartData = JSON.parse(cartItems);
                                        if (cartData.hasOwnProperty("msg")) {
                                            if (cartData.msg != true) {
                                                msg = cartData.msg;
                                                self.snack.ErrorSnack(msg);
                                            }
                                        }
                                    },
                                    error => self.errorMessage = <any>error);
                        }

                    }
                    self.cartProducts = 0;
                    //Reload the cart Page again
                    var checkURL = self.loginRedirectService.GetRedirectURL();
                    if (isNaN(checkURL)) {
                        self.loginRedirectService.SetRedirectURL('cart');
                    }
                    return self._ngZone.run(() => {
                        self.addToCartService.getCartProductsCount()
                            .then(
                                cartProducts => {
                                    self.cartProducts = JSON.parse(cartProducts).count;
                                    return self.loginRedirectService.newEvent();
                                },
                                error => self.errorMessage = <any>error);
                    });
                }
                return process(this).then(x => {
                    var data = [];
                    this.addToCartService.getCartProductsCount()
                        .then(
                            cartProducts => {
                                this.cartProducts = JSON.parse(cartProducts).count;
                                data = cartProducts;
                            },
                            error => this.errorMessage = <any>error);
                    if (totalItems > 1) {
                        setTimeout(() => {
                            for (let j = 1; j < totalItems; j++) {
                            }
                        }, 1000);
                    }
                    this.tempCart.ClearTempCart();
                    return data;
                });
            }
            else {
                this.cartProducts = 0;
                return this._ngZone.run(() => {
                    return this.addToCartService.getCartProductsCount()
                        .then(
                            cartProducts => {
                                return this.cartProducts = JSON.parse(cartProducts).count;
                            },
                            error => this.errorMessage = <any>error);
                });
            }
        }

        else {
            this.cartProducts = 0;
            var tempList = [];
            tempList = this.tempCart.GetTempCart();
            if (tempList != null && tempList.length > 0) {
                this.cartProducts = tempList.length;
            }
            return true;
        }
    }
    public showAdminShopBy() {
        this.adminCriteriaService.getAdminCriteriaList().then(
            response => {
                this.viewShopByNames = JSON.parse(response);
                this.shopByList = this.viewShopByNames.Items;
            },
            error => this.errorMessage = <any>error);
    }
    loginModal()
    {
        this.getPassword=false;
        this.OTPVerify=false;
        this.loginModel.StaySignedIn=true;
        this.reset();
        jQuery("#loginmodal").modal("show");

    }
    login() {
        if (this.loginModel.UserName != null && this.loginModel.Password) {
            this.loginService.login(this.loginModel)
                .then(
                    loginInfo => {
                        //console.log(loginInfo)
                        this.loginInfo = JSON.parse(loginInfo);
                        if (this.loginInfo.Token) {
                            localStorage.setItem('userToken', this.loginInfo.Token);
                            this.IsUserAuthorized();
                            this.myaccount = !this.myaccount;
                            this.userName();
                            jQuery("#loginmodal").modal("hide");
                            this.cart().then(x => {
                                this.tempCart.updateBuyNow();
                                this.loginRedirectService.newEvent();
                                if(this.router.url == "/user/cart/view"){
                                    this.router.navigateByUrl('/user/cart/view');
                                    // alert(this.location.path())
                                    // this.router.navigateByUrl('/user/cart/view');
                                }
                                if(this.router.url == "/user/contact"){
                                    this.router.navigateByUrl('/user/contact');
                                }
                                /* Set Cookie for NodeBB*/
                                if (this._cookieService.get("auth")) {
                                    // this._cookieService.remove("auth");
                                }
                                let authObj = '{"HttpOnly": true,"domain": ".zgroo.com"}';
                                // return this._cookieService.put("auth", authObj);
                            })

                        }
                        else if (this.loginInfo.Blockuser) {
                            this.snack.ErrorSnack("The user is blocked");
                        }
                        else if (this.loginInfo.WrongUser) {
                            this.snack.ErrorSnack("Check user name and password");
                        }
                        else {
                            this.ErrorUser = !this.ErrorUser;
                        }
                    }, error => {
                        this.errorMessage = <any>error
                        this.snack.ErrorSnack("Check user name and password");
                    });
        } else {
            this.snack.ErrorSnack("User Name and Password Mandatory");
        }
    }
    logout() {
        this.cartProducts = 0;
        this.loginService.logout()
            .then(
                Status => {
                    if (Status == 200) {
                        localStorage.clear();
                        this.logoutRedirectService.newEvent();
                        this.IsUserAuthorized();
                        this.myaccount = !this.myaccount;
                        this.ShowUserRole = "";
                        this.router.navigate(['']);
                    } else { alert("Can't Logout"); }
                },
                error => this.errorMessage = <any>error);
    }

    forgot(fPasswordMailId) {
        var re = /@/gi;

        if (fPasswordMailId.search(re) == -1) {
            this.forgotPasswordModel["UserName"] = fPasswordMailId;
        } else {
            this.forgotPasswordModel["Email"] = fPasswordMailId;
        }

        this.registerService.forgotPassword(this.forgotPasswordModel)
            .then(
                Status => {
                    this.snack.SuccessSnack('Check Mail to Reset Password');
                }, error => {
                    this.forgotDetails = true
                });

        this.forgotPasswordModel = {};
    }

    getDetails() {
        this.registerService.getCountries()
            .then(
                countriesInfo => {
                    this.countriesArray = JSON.parse(countriesInfo);
                }, error => this.errorMessage = <any>error);
    }

    onChangeCountry(value) {
        var val = { "CountryId": value };
        this.registerService.getStates(val).then(
            statesInfo => {
                this.statesArray = JSON.parse(statesInfo);
            }, error => this.errorMessage = <any>error);
    }
    TestmailName() {
        this.registerService.UserMailtest(this.registerModel.Email)
            .then(res => {
                var result = JSON.parse(res);
                this.isUserMailNotExist = result.Type;
            }, err => { }
            );
    }
    register() {
        this.ex = 0;
        if (this.registerModel.Password) {
            this.registerService.register(this.registerModel)
                .then(
                    registerInfo => {
                        this.registerInfo = JSON.parse(registerInfo);
                        if (this.registerInfo.Token) {

                            this.snack.SuccessSnack("Register successfully");
                            this.registerSuccessCode = "Success"
                            this.reset();
                            jQuery("#loginmodal").modal("hide");
                            this.OTPVerify = false;
                            this.OTPSent = false;
                            localStorage.setItem('userToken', this.registerInfo.Token);
                            this.IsUserAuthorized();
                            this.myaccount = !this.myaccount;
                            this.userName();
                            jQuery("#loginmodal").modal("hide");
                            this.cart();
                            this.loginRedirectService.newEvent();
                            /* Set Cookie for NodeBB*/
                            if (this._cookieService.get("auth")) {
                                // this._cookieService.remove("auth");
                            }
                            let authObj = '{"HttpOnly": true,"domain": ".zgroo.com"}';
                            // return this._cookieService.put("auth", authObj);
                        }
                        else {
                            this.snack.ErrorSnack(this.registerInfo.msg);

                        }
                    },
                    error => {
                        this.snack.ErrorSnack("User Name / Mail Id Already Exist");
                        this.registerSuccessCode = "Failure";
                    }
                );
        }
        else {
            this.snack.ErrorSnack("Password / Mail Id Not Matched");
        }
    }
    reset() {
        this.hide = false;
        jQuery("#forgot_modaltab").removeClass('active');
        jQuery("#register_modal").removeClass('active');
        jQuery("#login_modaltab").addClass('active');
        jQuery("#signinTab").addClass('active');
        jQuery("#registerTab").removeClass('active');
        jQuery('#registerform')[0].reset();
    }
    goToTop() {
        window.scrollTo(0, 0);
    }
    goToForum() {
        this.forumService.forum().then(
            forumInfo => {
                window.location.href = 'http://forum.zgroo.in'; //Will take you to Google.
            });
    }
    public GoToSellOnZgroo() {
        this.getPassword=false;
        this.reset();
        this.getUserToken = this._cookieService.get('userToken');
        let link = ['myaccounts/user/sell-on-jheaps'];

        if (this.getUserToken != null) {
            this.router.navigate(link);

        }
        else {
            jQuery("#loginmodal").modal('show');
            this.loginRedirectService.SetRedirectURL(link);
            this.loginRedirectService.events$.forEach(event => {
                if (event == link) {
                    this.GoToSellOnZgroo();
                    this.loginRedirectService.ClearRedirectURL();
                }
            });
        }
    }
    public GoToMyAccount() {
        this.getUserToken = this._cookieService.get('userToken');
        let link = ['myaccounts/user/profile/change/profile_edit'];
        if (this.getUserToken != null) {
            this.router.navigate(link);
        }
        else {
            jQuery("#loginmodal").modal('show');
            this.loginRedirectService.SetRedirectURL(link);
            this.loginRedirectService.events$.forEach(event => {
                if (event == link) {
                    this.GoToMyAccount();
                    this.loginRedirectService.ClearRedirectURL();
                }
            });
        }
    }
    public checkFeedbackUser() {
        this.getUserToken = this._cookieService.get('userToken');
        let link = ['/user/feedback'];
        if (this.getUserToken != null) {
            this.router.navigate(link);
        }
        else {
            jQuery("#loginmodal").modal('show');
            this.loginRedirectService.SetRedirectURL(link);
            this.loginRedirectService.events$.forEach(event => {
                if (event == link) {
                    this.checkFeedbackUser();
                    this.loginRedirectService.ClearRedirectURL();
                }
            });
        }
    }
    public ForgotSendOTP() {
        if (this.forgotMobile != null) {
            var numb = this.forgotMobile.toString().length;
            if (numb == 10) {

                this.registerService.ForgotSendOTP(this.forgotMobile).then(data => {
                    var dat = JSON.parse(data);
                    if (!dat.mes) {
                        this.snack.ErrorSnack('Sorry Mobile Number Not Found');

                    } else {
                        this.snack.SuccessSnack('OTP has been sent to your mobile');
                        this.getPassword = true;
                        this.OTPSent = true;
                    }
                });
            } else {
                this.snack.ErrorSnack('Mobile number must be 10 digits');
            }
        } else {
            this.snack.ErrorSnack('Mobile number is mandatory');
        }
    }
    registerModal() {
        this.reset();
        this.getPassword = false;
        this.hide=false;
        jQuery("#forgot_modaltab").modal('hide');
    }
    ChangeNumber() {
        this.getPassword = false;
        this.forgotMobile = null;
        this.OTP = null;
        this.OTPVerify = false;
        this.OTPWrong = false;
        this.mobtrue = false;
        jQuery("#forgot_modaltab").addClass('active');
        jQuery("#login_modaltab").removeClass('active');
    }
    sendNewMobEmail() {
        if (this.forgotMobile == "" || this.forgotMobile == undefined) {
            this.snack.ErrorSnack("Please enter Mobile number/Email");
        }
        else {
            this.loginModel.UserName = this.forgotMobile;
            this.forgotPassword();
        }
    }
    forgotPassword() {
        var verifyuser = this.loginModel.UserName;
        this.OTPWrong = false;
        console.log(verifyuser);
        if (this.loginModel.UserName == "" || this.loginModel.UserName == undefined) {
            this.hide = true;
            this.getPassword = false;
            this.mobtrue = false;
            this.forgotMobile = this.loginModel.UserName;
            jQuery("#forgot_modaltab").addClass('active');
            jQuery("#login_modaltab").removeClass('active');
        }
        else {
            this.userService.verifyMobEmail(verifyuser).then(
                response => {
                    var res = JSON.parse(response);
                    if (res.mes) {
                        this.hide = true;
                        this.OTP = '';
                        this.OTPVerify = false;
                        this.registerModel.Password = '';
                        if (!isNaN(res.verifyuser)) {
                        this.getPassword = true;
                            this.forgotMobile = this.loginModel.UserName;
                            // this.hide = false;
                            this.snack.SuccessSnack("OTP has been sent to your mobile");
                            jQuery("#forgot_modaltab").addClass('active');
                            jQuery("#login_modaltab").removeClass('active');
                        }
                        else {
                            this.getPassword = true;
                            this.forgotMobile = this.loginModel.UserName;
                            this.snack.SuccessSnack("OTP has been sent to your email id");
                            jQuery("#forgot_modaltab").addClass('active');
                            jQuery("#login_modaltab").removeClass('active');
                        }
                    }
                    else {
                        this.snack.ErrorSnack("The entered email/phone number is not registered or invalid");
                    }
                },
                error => {
                    this.errorMessage = <any>error
                }

            );
        }
    }
    hideLogin() {
        this.OTP = null;
        this.getPassword = false;
        jQuery("#login_modaltab").addClass('active');
        jQuery("#forgot_modaltab").removeClass('active');
    }
    OTPCheckMobEmail() {
        if (this.forgotMobile == "" || this.loginModel.UserName == undefined) {
            this.snack.SuccessSnack("Please enter Mobile Number/Email");
        }
        else {
            if (!isNaN(this.forgotMobile)) {
                this.ForgetOTPCheck();
            }
            else {
                this.EmailOTPCheck();
            }
        }
    }
    public ForgetOTPCheck() {
        this.OTPWrong = false;
        this.OTPVerify = false;
        var numb = this.OTP.toString().length;
        var model = {
            Mobile: this.forgotMobile,
            OTP: this.OTP
        }
        if (numb == 6) {
            this.registerService.OTPCheck(model).then(result => {
                var res = JSON.parse(result);
                if (res.mes) {
                    this.OTPVerify = true;
                    this.OTPWrong = false;
                } else {
                    this.OTPWrong = true;
                }
            });
        }
    }
    EmailOTPCheck() {
        this.OTPWrong = false;
        this.OTPVerify = false;
        var numb = this.OTP.toString().length;
        var model = {
            Email: this.forgotMobile,
            OTP: this.OTP
        }
        if (numb == 6) {
            this.registerService.EmailOTPCheck(model).then(result => {
                var res = JSON.parse(result);
                if (res.mes) {
                    this.OTPWrong = false;
                    this.OTPVerify = true;
                } else {
                    this.OTPWrong = true;
                    if (res.message != "Wrong OTP") {
                        this.snack.ErrorSnack(res.message);
                    }
                }
            });
        }
    }
    resetPassword() {
        var passwordPattern = /^[a-zA-Z0-9!@#$%^&*]{6,16}$/;
        if(this.registerModel.Password.toString().match(passwordPattern))
        {
            var model = {
                Mobile: this.forgotMobile,
                Password: this.registerModel.Password
            }
            this.registerService.resetPassword(model).then(data => {
                var dat = JSON.parse(data);
                if (dat.Token) {
                    this.snack.SuccessSnack("Password reset successfully");
                    this.registerSuccessCode = "Success"
                    this.reset();
                    this.OTP = null;
                    this.getPassword = false;
                    jQuery("#forgot_modaltab").removeClass("active");

                    jQuery("#forgot_modaltab").modal("hide");

                    jQuery("#login_modaltab").addClass('active');

                    localStorage.setItem('userToken', dat.Token);

                    this.IsUserAuthorized();
                    this.myaccount = !this.myaccount;
                    this.userName();
                    jQuery("#loginmodal").modal("hide");
                    this.cart();
                    this.loginRedirectService.newEvent();
                    if (this._cookieService.get("auth")) {
                        // this._cookieService.remove("auth");
                    }
                    let authObj = '{"HttpOnly": true,"domain": ".zgroo.com"}';
                    // return this._cookieService.put("auth", authObj);
                } else {
                    this.snack.ErrorSnack(dat.message);

                }
            });
    }
    else
    {
        this.snack.ErrorSnack('Passwords must be at least 6 characters.');
    }
    }
    public SendOTP() {
        this.OTP="";
        this.OTPVerify=false;
        // var phonePattern = /^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$/;
        var phonePattern=/^[6789]\d{9}$/;
        var emailPattern = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9]{2,3}$/;
        var passwordPattern = /^[a-zA-Z0-9!@#$%^&*]{6,16}$/;
        if(this.registerModel.Mobile.toString().match(phonePattern))
        {
            if(this.registerModel.Email.toString().match(emailPattern))
            {
                if(this.registerModel.Password.toString().match(passwordPattern))
                {
                    this.registerService.UserMailtest(this.registerModel.Email).then(res => {
                        var result=JSON.parse(res);
                        if(result.Type==true)
                        {
                        // var numb = this.registerModel.Mobile.toString().length;
                        // if (numb == 10 && numb != null) {
                
                            this.registerService.SendOTP(this.registerModel.Mobile).then(data => {
                                var dat = JSON.parse(data);
                                if (dat.mes) {
                                    this.snack.SuccessSnack('OTP has been sent to your mobile');
                                    this.getPassword = true;
                                    this.OTPSent = true;
                                } else {
                                    this.numberExist = false;
                                    this.snack.ErrorSnack('A Jheaps account already exists with the mobile number');
                                }
                            });
                
                        // } else {
                        //     this.snack.ErrorSnack('Mobile number must be 10 digits');
                        // }
                    }
                    else
                    {
                        this.snack.ErrorSnack('A JHeaps account already exists with the email');
                    }
                    });
                }
                else
                {
                    this.snack.ErrorSnack('Passwords must be at least 6 characters.');
                }
            }
            else
            {
                this.snack.ErrorSnack('Enter a valid email id.');
            }
        }
        else
        {
            this.snack.ErrorSnack('Enter a valid mobile number.');
        }
    }
    OTPCheck() {
        this.OTPWrong = false;
        this.OTPVerify = false;
        var numb = this.OTP.toString().length;
        var model = {
            Mobile: this.registerModel.Mobile,
            OTP: this.OTP
        }
        if (numb == 6) {
            this.registerService.OTPCheck(model).then(result => {
                var res = JSON.parse(result);
                if (res.mes) {
                    this.OTPVerify = true;
                } else {
                    this.OTPWrong = true;
                }
            });
        }
    }
    Existreset() {
        this.numberExist = true;
    }
    numberChange() {
        this.OTPVerify = false;
        this.registerModel.Mobile = null;
        this.getPassword = false;
        this.OTP = "";
        this.OTPSent = false;
        this.registerModel.Password = "";
    }
}
