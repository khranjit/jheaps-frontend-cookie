import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[BindImage]'
})


export class ProductImageDirective {


    @Input('BindImage') product: any;

    constructor(private el: ElementRef) { }

    ngAfterViewInit() {

        this.el.nativeElement.src = this.getImageURL();
       
    }

    private getImageURL()
    {
        if(this.product.useVariation)
        {
            var variation;
            if(this.product.Variations.find(ele=> (ele.Active && ele.InStock && ele.Qty)))
            {
                variation = this.product.Variations.find(ele=> (ele.Active && ele.InStock && ele.Qty)) //set the active and instock Image
            }
 
            else
            {
                variation = this.product.Variations.find(ele=> (ele.Active)) //set the active image
            }
 
            if(variation.Image)
            return variation.Image;
            
        }
        return this.product.Media.find(x=>x.IsPrimary).EditedImage; //set the normal image
    }
   
}