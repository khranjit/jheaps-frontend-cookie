import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[BindPrice]'
})


export class ProductPriceDirective {

    productCurrency = {value: '₹'}
    @Input('BindPrice') product: any;

    constructor(private el: ElementRef) { }

    ngAfterViewInit() {
        var price= this.getPrice();
        this.el.nativeElement.innerHTML = this.productCurrency.value + price;
        this.el.nativeElement.title = this.productCurrency.value + price;
    }

    private getPrice()
    {
        if(this.product.useVariation)
        {
            var variation =  this.product.Variations.find(x=>x.Active && x.InStock  && x.Qty);
            if(variation)
            return variation.Price;  //First active and instock price
    
            else
            {
                return this.product.Variations.find(x=>x.Active).Price; //First active price
            }
        }

        else
        return this.product.Price; //Normal price
        
    }
   
}