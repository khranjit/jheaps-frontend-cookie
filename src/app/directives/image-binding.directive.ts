import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[ImageBind]'
})


export class ImageBinding {


    @Input('ImageBind') Product: any;

    constructor(private el: ElementRef) { }

    ngAfterViewInit() {

        this.el.nativeElement.src = this.getImageURL();
       
    }

    private getImageURL(){
        var Image = () => {
            return this.Product.Media.find(x=>x.IsPrimary).EditedImage;
        }
        if(this.Product.hasOwnProperty("Variation"))
        {
            if(this.Product.Variation.hasOwnProperty("Image"))
            {
                if(this.Product.Variation.Image !="")
                {
                    return this.Product.Variation.Image;
                }
                else
                {
                    return Image();
                }
            }
            else
            {
                return Image();
            }            
        }
        else
        {
            return Image();
        }
    }


   
}