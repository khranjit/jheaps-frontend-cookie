import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[CheckOutOfStock]'
})


export class ProductOutOfStockDirective {

    productCurrency = {value: '₹'}
    @Input('CheckOutOfStock') product: any;

    constructor(private el: ElementRef) { }

    ngAfterViewInit() {
        
        if(this.checkOutOfStock())
        this.el.nativeElement.style = "display:none";  //InStock
        
    }

   private checkOutOfStock()
   {
       return this.product.useDefaultQuantity ? (this.product.QuantityAvailable > 0) : (this.product.VariationQuantityAvailable > 0)
   }
   
}