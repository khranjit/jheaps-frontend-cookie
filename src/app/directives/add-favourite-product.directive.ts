import { HostListener, Directive, ElementRef, Input,Renderer2} from '@angular/core';
import { HomeService } from '../services/home.service';
import { ToasterPopService } from '../services/toasterpop.service';
import { SuccessSnackComponent,ErrorSnackComponent } from '../snack/snack.component';
import {MatSnackBar} from '@angular/material';
import { TempCartService } from '../services/cart.service';
//import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
//import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
declare var jQuery:any;

@Directive({
    selector: '[add-favourite-product]'
})
export class AddFavouriteProductDirective{
    
    @Input('add-favourite-product') productId;
    constructor(private el: ElementRef, private renderer : Renderer2, private homeService:HomeService, private tempCartService:TempCartService, private snack:MatSnackBar) { }

    ngAfterViewInit()
    {
       this.checkFavouriteProductStatus();
    }
    
    checkFavouriteProductStatus()
    {
        var getUserToken = localStorage.getItem('userToken');
        if (getUserToken != null) {
            this.homeService.checkFavouriteProduct(this.productId).then(response => {
                var res = JSON.parse(response);
                if(res.found)
                {
                    this.renderer.removeClass(this.el.nativeElement,'setNormal');
                    this.renderer.removeClass(this.el.nativeElement,'fa-heart-o');
                    this.renderer.addClass(this.el.nativeElement,'setFavourite');
                    this.renderer.addClass(this.el.nativeElement,'fa-heart');
                }
                else
                {
                    this.renderer.removeClass(this.el.nativeElement,'setFavourite');
                    this.renderer.removeClass(this.el.nativeElement,'fa-heart');
                    this.renderer.addClass(this.el.nativeElement,'setNormal');
                    this.renderer.addClass(this.el.nativeElement,'fa-heart-o');
                }
            }, error=>{
                this.renderer.removeClass(this.el.nativeElement,'setFavourite');
                this.renderer.removeClass(this.el.nativeElement,'fa-heart');
                this.renderer.addClass(this.el.nativeElement,'setNormal');
                this.renderer.addClass(this.el.nativeElement,'fa-heart-o');
            });
        }

        else{
            this.renderer.removeClass(this.el.nativeElement,'setFavourite');
            this.renderer.removeClass(this.el.nativeElement,'fa-heart');
            this.renderer.addClass(this.el.nativeElement,'setNormal');
            this.renderer.addClass(this.el.nativeElement,'fa-heart-o');
        }
    }

    @HostListener('click') onClick() {
        var getUserToken = localStorage.getItem('userToken');
        if (getUserToken != null) {
            this.homeService.addFavouriteProduct(this.productId).then(response => {
                var res = JSON.parse(response);
                if(res.remove) {
                    this.snack.openFromComponent(ErrorSnackComponent, {
                        duration: 5000,
                        data: "Item removed from your favorites"
                      });
                    this.renderer.removeClass(this.el.nativeElement,'setFavourite');
                    this.renderer.removeClass(this.el.nativeElement,'fa-heart');
                    this.renderer.addClass(this.el.nativeElement,'setNormal');
                    this.renderer.addClass(this.el.nativeElement,'fa-heart-o');
                }
                else {
                    this.snack.openFromComponent(SuccessSnackComponent, {
                        duration: 5000,
                        data: "Item added to your favorites"
                      });
                    this.renderer.removeClass(this.el.nativeElement,'setNormal');
                    this.renderer.removeClass(this.el.nativeElement,'fa-heart-o');
                    this.renderer.addClass(this.el.nativeElement,'setFavourite');
                    this.renderer.addClass(this.el.nativeElement,'fa-heart');
                    // this.location.go(this.location.path(), "refresh=1");
                    //this.router.navigateByUrl(this.location.path(), { skipLocationChange: true });
                    //this.router.navigate([this.location.path()+'?refresh=1']); 
                }
            },error=>{
                this.renderer.removeClass(this.el.nativeElement,'setFavourite');
                this.renderer.removeClass(this.el.nativeElement,'fa-heart');
                this.renderer.addClass(this.el.nativeElement,'setNormal');
                this.renderer.addClass(this.el.nativeElement,'fa-heart-o');
            });
        }
        else
        {
            this.tempCartService.setProductID(this.productId);
            jQuery("#loginmodal").modal('show');
        }
        
    }
}
