import { HostListener, Directive, Input, Injector } from '@angular/core';
import { AddToCartService, TempCartService } from '../services/cart.service';
import { ToasterPopService } from '../services/toasterpop.service';
import { ProductDescriptionService } from '../services/product-description.service';
import { AppComponent } from '../app.component';
import { SuccessSnackComponent, ErrorSnackComponent } from '../snack/snack.component';
import { MatSnackBar } from '@angular/material';
//import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
//import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
declare var jQuery: any;

@Directive({
    selector: '[add-to-cart]'
})
export class AddToCartDirective {

    @Input('add-to-cart') productId;
    constructor(private snack: MatSnackBar, private injector: Injector, private productDescriptionService: ProductDescriptionService, private tempCartService: TempCartService, private addToCartService: AddToCartService) { }



    @HostListener('click') onClick() {

        this.productDescriptionService.getProductById(this.productId)
            .then(
                product => {
                    var Product = JSON.parse(product);
                    var getUserToken = localStorage.getItem('userToken'), quantityAvailable;
                    if (Product.useVariation) {
                        quantityAvailable = Product.VariationQuantityAvailable;
                        if (quantityAvailable > 0) {

                            //Variation Product
                            var toCart = <any>{};
                            toCart.Product = Product._id;
                            toCart.BuyNow = false;
                            toCart.Qty = 1;
                            toCart.addToCart = true;
                            if (Product.Variations.length > 0) {
                                var activeVariation = this.getVariation(Product);
                                if (activeVariation) {
                                    toCart.Variation = activeVariation._id;
                                }
                            }
                            if (getUserToken != null) {
                                this.addToCartService.clearBuyNow().then(
                                    res => {
                                        this.addToCartService.setCartProduct(toCart).then(
                                            cartItems => {
                                                var cartData = JSON.parse(cartItems);
                                                var app = this.injector.get(AppComponent);
                                                app.cart();
                                                if (cartData.hasOwnProperty("msg")) {
                                                    if (cartData.msg != true) {
                                                        this.snack.openFromComponent(ErrorSnackComponent, {
                                                            duration: 5000,
                                                            data: 'Item already exists in your cart'
                                                        });
                                                    }
                                                    else
                                                    {
                                                        this.snack.openFromComponent(SuccessSnackComponent, {
                                                            duration: 5000,
                                                            data: 'Item added to cart!'
                                                        });
                                                    }

                                                }
                                                
                                            },
                                            error => {
                                                //this.errorMessage = <any>error;
                                                //                    console.log(error);
                                                this.snack.openFromComponent(ErrorSnackComponent, {
                                                    duration: 5000,
                                                    data: 'Error while adding the product to cart'
                                                });
                                            });
                                    },
                                    error => {
                                        this.snack.openFromComponent(ErrorSnackComponent, {
                                            duration: 5000,
                                            data: 'Error while adding the product to cart'
                                        });
                                    });

                            }

                            if (getUserToken == null) {

                                var message = "";
                                toCart = JSON.stringify(toCart);
                                message = this.tempCartService.PushTempCart(toCart);
                                if (message === "Item already present") {
                                    this.snack.openFromComponent(ErrorSnackComponent, {
                                        duration: 5000,
                                        data: 'Item already exists in your cart'
                                    });
                                }
                                else {
                                    var app = this.injector.get(AppComponent);
                                    app.cart();
                                }
                            }
                        }
                        else {
                            this.snack.openFromComponent(ErrorSnackComponent, {
                                duration: 5000,
                                data: 'This product is Out of stock'
                            });
                        }
                    }

                    else {
                        quantityAvailable = Product.QuantityAvailable;
                        //Normal Product
                        if (quantityAvailable > 0) {
                            var toCart = <any>{};
                            toCart.Product = Product._id;
                            toCart.BuyNow = false;
                            toCart.Qty = 1;
                            toCart.addToCart = true;
                            if (getUserToken != null) {
                                this.addToCartService.clearBuyNow().then(
                                    res => {
                                        this.addToCartService.setCartProduct(toCart).then(
                                            cartItems => {
                                                var cartData = JSON.parse(cartItems);
                                                var app = this.injector.get(AppComponent);
                                                app.cart();
                                                if (cartData.hasOwnProperty("msg")) {
                                                    if(cartData.msg != true){
                                                        this.snack.openFromComponent(ErrorSnackComponent, {
                                                            duration: 5000,
                                                            data: 'Item already exists in your cart'
                                                        });
                                                    }
                                                    else
                                                    {
                                                        this.snack.openFromComponent(SuccessSnackComponent, {
                                                            duration: 5000,
                                                            data: 'Item added to cart!'
                                                        });
                                                    }
                                                }
                                            },
                                            error => {
                                                this.snack.openFromComponent(ErrorSnackComponent, {
                                                    duration: 5000,
                                                    data: 'Error while adding the product to cart'
                                                });
                                            });
                                    },
                                    error => {
                                        this.snack.openFromComponent(ErrorSnackComponent, {
                                            duration: 5000,
                                            data: 'Error while adding the product to cart'
                                        });
                                    })

                            }

                            if (getUserToken == null) {

                                var message = "";
                                toCart = JSON.stringify(toCart);
                                message = this.tempCartService.PushTempCart(toCart);
                                if (message === "Item already present") {
                                    this.snack.openFromComponent(ErrorSnackComponent, {
                                        duration: 5000,
                                        data: 'Item already exists in your cart'
                                    });
                                }
                                else {
                                    var app = this.injector.get(AppComponent);
                                    app.cart(); 
                                }
                            }
                        }
                        else {
                            this.snack.openFromComponent(ErrorSnackComponent, {
                                duration: 5000,
                                data: 'This product is Out of stock'
                            });
                        }
                    }
                },
                error => {
                    this.snack.openFromComponent(ErrorSnackComponent, {
                        duration: 5000,
                        data: 'Error while adding the product to cart'
                    });
                })

    }

    private getVariation(product) {

        var variation;
        if (product.Variations.find(ele => (ele.Active && ele.InStock && ele.Qty))) {
            return variation = product.Variations.find(ele => (ele.Active && ele.InStock && ele.Qty))
        }
        else {
            return variation = product.Variations.find(ele => (ele.Active))
        }
    }
}
