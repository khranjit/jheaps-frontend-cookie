import { HostListener, Directive, ElementRef, Input,Renderer2} from '@angular/core';
import { HomeService } from '../services/home.service';
// import { NotifyService } from '../services/notify.service';
import { SuccessSnackComponent,ErrorSnackComponent } from '../snack/snack.component';
import {MatSnackBar} from '@angular/material';
import { TempCartService } from '../services/cart.service';


//import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
//import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
declare var jQuery:any;

@Directive({
    selector: '[add-favourite-shop]'
})
export class AddFavouriteShopDirective{
    
    @Input('add-favourite-shop') shopId;
    constructor(private el: ElementRef, private renderer : Renderer2, private homeService:HomeService, private tempCartService:TempCartService,private snack:MatSnackBar) { }

    ngAfterViewInit()
    {
       this.checkFavouriteShopStatus();
    }
    
    checkFavouriteShopStatus()
    {
        var getUserToken = localStorage.getItem('userToken');
        if (getUserToken != null) {
            this.homeService.checkFavouriteShopStatus(this.shopId).then(response => {
                var res = JSON.parse(response);
                if(res.found)
                {
                    this.renderer.removeClass(this.el.nativeElement,'setNormal');
                    this.renderer.addClass(this.el.nativeElement,'setFavourite');
                }
                
                //this.el.nativeElement.class = 'setFavouriteShop';
                else
                {
                    this.renderer.removeClass(this.el.nativeElement,'setFavourite');
                    this.renderer.addClass(this.el.nativeElement,'setNormal');
                }
                //this.el.nativeElement.class = 'setNormal';
            }, error=>{
                this.renderer.removeClass(this.el.nativeElement,'setFavourite');
                this.renderer.addClass(this.el.nativeElement,'setNormal');
            });
        }
        
        else{
            this.renderer.removeClass(this.el.nativeElement,'setFavourite');
            this.renderer.addClass(this.el.nativeElement,'setNormal');
        }
    }

    @HostListener('click') onClick() {

        var getUserToken = localStorage.getItem('userToken');
        if (getUserToken != null) {

            this.homeService.addFavouriteShop(this.shopId).then(response => {
                var res = JSON.parse(response);
                if(res.remove) {
                    this.snack.openFromComponent(ErrorSnackComponent, {
                        duration: 5000,
                        data: "Shop removed from your favorites"
                      });
                    // this.snack.ErrorSnack("Shop successfully removed from favourites");
                    this.renderer.removeClass(this.el.nativeElement,'setFavourite');
                    this.renderer.addClass(this.el.nativeElement,'setNormal');
                }
                else {
                    
                        this.snack.openFromComponent(SuccessSnackComponent, {
                          duration: 5000,
                          data: "Shop added to your favorites"
                        });
                
                    // this.snack.SuccessSnack("Shop successfully added to favourites");
                    this.renderer.removeClass(this.el.nativeElement,'setNormal');
                    this.renderer.addClass(this.el.nativeElement,'setFavourite');
                    // this.location.go(this.location.path(), "refresh=1");
                    //this.router.navigateByUrl(this.location.path(), { skipLocationChange: true });
                    //this.router.navigate([this.location.path()+'?refresh=1']); 
                }
            },error=>{
                this.renderer.removeClass(this.el.nativeElement,'setFavourite');
                this.renderer.addClass(this.el.nativeElement,'setNormal');
            });
        }

        else
        {
            this.tempCartService.setShopID(this.shopId);
            jQuery("#loginmodal").modal('show');

        }

        
    }
}
