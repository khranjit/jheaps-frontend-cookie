import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { NotFoundPageComponent } from './notfoundpage.component';
import { RouterModule, Routes } from '@angular/router';

const errorpage:Routes = [
  { path: '', component: NotFoundPageComponent }
]

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(errorpage)
  
  ],
  declarations: [ 
    NotFoundPageComponent
  ]
})
export class NotFoundPageModule { }