import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../../services/user.service';
import { ContactService } from '../../services/contact.service';
import { Router, NavigationEnd } from '@angular/router';
import { NotifyService } from '../../services/notify.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { any } from 'nconf';

declare var jQuery: any;

@Component({
  selector: 'contact',
  templateUrl: './contact.component.html',
  providers: [UserService, ContactService, NotifyService]
})

export class ContactComponent implements OnInit, OnDestroy {

  getUserToken: any;
  errorMessage: any;
  userProfile: any;
  message: any;
  userName: any;
  textAreaLength = 0;
  navigationSubscription
  Contactmodel: any = {
    Subject: "",
    Email: "",
    Mobile: "",
    Message: "",
  }

  ngOnInit() {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.

  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  constructor(private userService: UserService, private router: Router, private contactService: ContactService, private toasterService: NotifyService, private loader: NgxSpinnerService) {
    this.getUserToken = localStorage.getItem('userToken');
    if (this.getUserToken != null) {
      this.Init();
    }

    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.Init();
      }
    });

  }

  /**
   * Init
   */
  public Init() {
    this.GetUserDetails();
    this.getUserToken = localStorage.getItem('userToken');

  }

  GetUserDetails() {
    this.userService.showProfile().then(
      response => {
        this.userProfile = JSON.parse(response).Items[0];
        this.userName = this.userProfile.Profile.FirstName;
      },
      error => {
        this.errorMessage = <any>error;

      });
  }

  MessageChange(event) {
    this.textAreaLength = event.length;
  }

  SendMessage() {
    var messageModel = {
      Message: this.message
    }
    if (this.Contactmodel.Subject) {
      if (this.Contactmodel.Message) {
        this.loader.show()
        this.contactService.PostMessage(this.Contactmodel).then(
          res => {
            var result = JSON.parse(res);
            if (result.flag == 2) {
              this.loader.hide()
              this.toasterService.ErrorSnack('Kindly update your email');
            } else {
              this.loader.hide()
              this.toasterService.SuccessSnack('Sent successfully');
              this.Contactmodel.Subject = "";
              this.Contactmodel.Message = "";
            }

          },

          error => {
            this.loader.hide()
            this.errorMessage = <any>error;
            //this.toasterService.ErrorSnack( 'Error while sending the message');
          }
        )
      } else {
        this.toasterService.ErrorSnack('Please fill the message');
      }

    } else {
      this.toasterService.ErrorSnack('Please fill the subject');
    }
  }

  GuestSendMessage() {
    var phonePattern = /^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/;
    var emailPattern = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9]{2,3}$/;

    if (this.Contactmodel.Subject) {
      if (this.Contactmodel.Email.toString().match(emailPattern)) {
        if (this.Contactmodel.Mobile.toString().match(phonePattern)) {
          if (this.Contactmodel.Message) {
            this.loader.show()
            this.contactService.GuestPostMessage(this.Contactmodel).then(
              res => {
                var result = JSON.parse(res);
                if (result.flag == 2) {
                  this.loader.hide()
                  this.toasterService.ErrorSnack('Kindly update your email');
                } else {
                  this.loader.hide()
                  this.toasterService.SuccessSnack('Sent successfully');
                  this.Contactmodel = {
                    Subject: "",
                    Email: "",
                    Mobile: "",
                    Message: ""
                  }
                }

              },

              error => {
                this.loader.hide()
                this.errorMessage = <any>error;
                //this.toasterService.ErrorSnack('Error while sending the message');
              }
            )
          }
          else {
            this.toasterService.ErrorSnack('Please fill your message.');
          }
        }
        else {
          this.toasterService.ErrorSnack('Enter a valid mobile number.');
        }
      }
      else {
        this.toasterService.ErrorSnack('Enter a valid email id.');
      }
    } else {
      this.toasterService.ErrorSnack('Please fill the subject');
    }
  }
}