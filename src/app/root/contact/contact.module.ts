import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { ContactComponent } from './contact.component';
import { RouterModule, Routes } from '@angular/router';
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material';

const shop:Routes = [
  { path: '', component: ContactComponent }
]

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(shop),
    MatFormFieldModule,
    MatInputModule
  ],
  declarations: [ 
    ContactComponent
  ]
})
export class ContactModule { }