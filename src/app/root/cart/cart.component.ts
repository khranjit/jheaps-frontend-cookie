import { Component, OnInit, DoCheck, Injector, ChangeDetectorRef, ElementRef, ViewChild,OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { AddToCartService, TempCartService,BuyNowService } from '../../services/cart.service';
import { LoginRedirectService } from "../../services/login.service";
import { ProductDescriptionService } from '../../services/product-description.service';
import { AppComponent } from '../../app.component';
import { NotifyService } from '../../services/notify.service';
import { ShippingProfileService } from '../../services/shipping-profile.service';
import { UserService } from '../../services/user.service';
import { RegisterService } from '../../services/register.service';
import { DomainService } from '../../services/domain.service';
import { UtilService } from '../../services/util.service';
import { NgxSpinnerService } from 'ngx-spinner';

import { FormGroup,FormBuilder,Validators } from '@angular/forms';

import { InvisibleReCaptchaComponent } from '../../../../projects/ngx-captcha-lib/src/public_api';

declare var hljs: any;
declare var jQuery: any;

@Component({
    
    selector: 'cart',
    providers: [AddToCartService,UtilService, ProductDescriptionService, UserService,
        RegisterService, ShippingProfileService, DomainService],
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.css'],
})
export class CartComponent implements DoCheck, OnInit,OnDestroy {
    getUserToken: any;
    cartPrdVariationModel = [{name:''},{name:''}];
    ChangeVariationModel:any;
    protected aFormGroup: FormGroup;
    public readonly siteKey = '6Ld7xIoUAAAAAJjkB5u2_CFQZO4OoOO8k4S2okCA';
    
    public addressModel: ViewModels.IAddressViewModel;
    public tempaddressModel: ViewModels.IAddressViewModel;
    public CartProductsList: any;
    public CartProducts=[];
    public savedForLaterProducts: any;
    public errorMessage: any;
    public subTotalCosts: any;
    public TotalCosts: any;
    public shopLists: any;
    public shopListArray: any;
    public describedProduct: any;
    public selectedProductVariation: any;
    public productCurrency: any = { value: '₹' };
    toCart: ViewModels.ICartItemViewModel;
    public app: any;
    public selectedShopInfo = <any>{};
    public shippingCost: any;
    public selectedSeller:any;
    public couponDiscountAmount: any;
    ShowUserProfileDetails: any;
    ShowUser: any;
    colorFlag:boolean=false;
    ShowCouponDiscount: boolean = false;
   
    public captchaIsLoaded = false;
    public captchaSuccess = false;
    public captchaIsExpired = false;
    public captchaResponse?: string;
    public captchaIsReady = false;
    public Showcaptcha=false;
    public captchaDisplayed:any;
    public captchaEntered="";
    public captchaVerify=false;
    public captchaflag=true;
    public captchaerror=false;
    loader = false;

    @ViewChild('captchaElem') captchaElem: InvisibleReCaptchaComponent;
    @ViewChild('langInput') langInput: ElementRef;
  
    public theme: 'light' | 'dark' = 'light';
    public size: 'compact' | 'normal' = 'normal';
    public lang = 'en';
    public type: 'image' | 'audio';
    public useGlobalDomain: boolean = false;

    ShowUserProfile: any;
    qantitytest:boolean;
    ShowUserAddress: any;
    bool=false;
    vari:any;
    backbutton=false;
    newbutton=true;
    editbutton=false;
    bool1=false;
    bool2=false;
    bool3=false;
    ShippingProfile: any;
    ShippingAddress: any;
    DisplayShippingAddress: any;
    changeProfilePrimary: any;
    CartId: any;
    isBuyNowCart = false;
    ShowShippingAddress: boolean;
    AddShippingAddressTemp: boolean; 
    public IsOrderCreating: boolean;
    countriesArray: any;
    statesArray: any;
     public tempquantity=[];
    countryName: any;
    countryCode: any;
    cartCostDetails = <any>{};
    tempShippingAddress: boolean;
    UserShippingAddress: boolean;
    showError = false;
    paymentView :boolean;
    urlLocation : string;
    checkoutshopid : string = "1";
    paymentMethod:any;
    notAvailableCount = 0;
    errorMsg = "Please delete the unavailable product !";
    couponCode=<any>[];
    variation_id = "";
    ShopCOD = true;
    AddressSwapJson: any;
    buyNow = false;
    tempCartCount: any;
    citiesArray:any;
    guestUser:boolean = false;
    navigationSubscription;
    AddAddressflag=false;
    
    constructor(private productDescriptionService: ProductDescriptionService, private loadingservice:NgxSpinnerService, private utilService: UtilService, private loginRedirectService : LoginRedirectService, private buyNowService:BuyNowService, private tempCart : TempCartService, private addToCartService: AddToCartService, private route: ActivatedRoute, private router: Router,
        private registerService: RegisterService,private snack:NotifyService ,private Inj: Injector,private userService: UserService, private shippingProfileService: ShippingProfileService,
         private domainService: DomainService,private formBuilder: FormBuilder ,private cdr: ChangeDetectorRef) {

        this.addressModel = <ViewModels.IAddressViewModel>{};
        this.tempaddressModel = <ViewModels.IAddressViewModel>{};
        this.ShowUserAddress = {};
        this.AddressSwapJson = [];
        this.DisplayShippingAddress = {};
        this.ShowUserProfile = {};
        this.countryCode = {};
        this.countryName = "";
        this.countriesArray = [];
        this.statesArray = [];
        this.CartProducts=[];
        this.ShowShippingAddress = false;
        this.UserShippingAddress = false;
        this.AddShippingAddressTemp = false;
        this.tempShippingAddress=false;
        this.paymentMethod="Online";
        this.Init();
        this.ShippingAddressFunc();
        this.IsOrderCreating = false;
        this.qantitytest=false;
        this.navigationSubscription = this.router.events.subscribe((e: any) => {
            // If it is a NavigationEnd event re-initalise the component
            if (e instanceof NavigationEnd) {
              this.Init();
              this.ShippingAddressFunc();
              this.ShippingAddress =[] 
            }
        });

    }

    ngOnDestroy(){
        if (this.navigationSubscription) {  
            this.navigationSubscription.unsubscribe();
         }
    }
    
    Init() {
        this.ChangeVariationModel = {};
        this.variation_id = "";
        this.urlLocation=location.origin;
        if(this.urlLocation=="http://demo1.zgroo.com"){
            this.paymentView=true;
        }
        else if(this.urlLocation=="http://demo1.zgroo.in"){
            this.paymentView=false;
        }  
         
        this.registerService.getCountries()
            .then(
            countriesInfo => {
                this.countriesArray = JSON.parse(countriesInfo);
            }, error => this.errorMessage = <any>error);
        this.shopLists = [];
        this.CartProductsList = [];
        this.shopListArray = [];
        this.savedForLaterProducts = [];
        this.getUserToken=localStorage.getItem('userToken');
        if( this.getUserToken!=null) {
            this.guestUser = false;
            this.GetCart();                                           
        }
        else
        {
            this.guestUser = true;
            this.tempCartCount = this.tempCart.GetTempCart();
            if(this.tempCartCount) {
                this.tempCartDetails();
            }  
            else {
                this.CartProducts = [];
            }
        }
        
        jQuery(window).scroll(function(e){ 
          var $el = jQuery('.fixedElement'); 
          var isPositionFixed = ($el.css('position') == 'fixed');
          if (((jQuery(this).scrollTop() + $el.height()) >= jQuery('footer').offset().top)){ 
            jQuery('.fixedElement').css({'background':'#FFF', 'position': 'static', 'top': '0px', 'right': '0px'}); 
          }
          else if (jQuery(this).scrollTop() > 200 && !isPositionFixed){ 
            jQuery('.fixedElement').css({'background':'#FFF', 'position': 'fixed', 'top': '0px', 'right': '0px'}); 
          }
          else if (jQuery(this).scrollTop() < 200 && isPositionFixed){
            jQuery('.fixedElement').css({'background':'#FFF', 'position': 'static', 'top': '0px', 'right': '0px'}); 
          } 
        });
    }
   
    ngOnInit(){
        this.aFormGroup = this.formBuilder.group({
            recaptcha: ['', Validators.required]
          });
    }
    tempCartDetails()
    {
        this.tempCartCount = this.tempCart.GetTempCart();
        this.loadingservice.show();                                                                    
        this.addToCartService.showTempCart(this.tempCartCount).then(cartProducts=> {
            var allCartProducts = JSON.parse(cartProducts);
            this.CartProductsList = allCartProducts;
            this.CartProducts = this.CartProductsList.Items;
            this.CartId = this.CartProductsList._id;
            this.ShopCOD = this.CartProductsList.ShopCOD;
            this.CartProducts.forEach((cartProduct: any) => {
            if(cartProduct.Product.IsProductDeleted)
            this.notAvailableCount += 1;
            cartProduct.productEdit = false;
        });
        this.cartCostDetails = this.CartProductsList.cartCostDetails;
        this.shopLists = this.CartProductsList.shopLists;
            var shpModel = this.CartProductsList.shopListArray;
            shpModel.forEach(ele => {
                if (!this.shopListArray[ele.name]) {
                 this.shopListArray[ele.name] = [];
                    ele.arrayVal.CODAvail = ele.COD;
                    ele.arrayVal.find(x=>{
                        if(x.hasOwnProperty("Variation"))
                        {
                            if(x.Product.useVariation)
                            {
                                x.Product.Variations.find(y => {
                                    if(y._id == x.Variation)
                                    {
                                        x.Variation = y;
                                    }
                                })
                            }
                        }
                    })
                    this.shopListArray[ele.name].push(ele.arrayVal);
                }
            });
            this.loadingservice.hide(); 

        }, err=> {this.errorMessage = <any>err});
    }

    ngAfterViewInit(): void {
        this.captchaIsLoaded = true;
        this.cdr.detectChanges();
        this.highlight();
    }
    private highlight(): void {
        const highlightBlocks = document.getElementsByTagName('code');
        for (let i = 0; i < highlightBlocks.length; i++) {
          const block = highlightBlocks[i];
          hljs.highlightBlock(block);
        }
      }
      execute(): void {
        this.captchaElem.execute();
      }
    
      handleReset(): void {
        this.captchaSuccess = false;
        this.captchaResponse = undefined;
        this.captchaIsExpired = false;
      }
    
      handleSuccess(captchaResponse: string): void {
        this.captchaSuccess = true;
        this.captchaResponse = captchaResponse;
        this.captchaIsExpired = false;
        if(this.tempaddressModel)
        {
            jQuery("#selectAddress").modal('hide');
            if(this.checkoutshopid != "1")
            {
                this.CheckoutfromShop(this.checkoutshopid);
            }
            else
            {
                this.Checkout();
            }
        }
      }
    
      handleLoad(): void {
        this.captchaIsLoaded = true;
        this.captchaIsExpired = false;
        this.captchaSuccess = false;
      }
    
      handleExpire(): void {
        this.captchaSuccess = false;
        this.captchaIsExpired = true;
      }
    
      changeTheme(theme: 'light' | 'dark'): void {
        this.theme = theme;
      }
    
      changeSize(size: 'compact' | 'normal'): void {
        this.size = size;
      }
    
      changeType(type: 'image' | 'audio'): void {
        this.type = type;
      }
    
      setUseGlobalDomain(use: boolean): void {
        this.useGlobalDomain = use;
      }
      
    GetCart()
    {
        this.route.params.subscribe(params => {
            if(params.id != "buynow")
            {
                
                this.addToCartService.clearBuyNow().then(data => {
                    savecart(this);                        
                });
            }
            else
            {
                this.buyNow=true;
                savecart(this);
            }
            function savecart(self){
                self.loadingservice.show();                                                                    
                 self.addToCartService.getAll().then(cartProducts=> {
                    var allCartProducts = JSON.parse(cartProducts);
                    self.CartProductsList = allCartProducts;
                    self.CartProducts = self.CartProductsList.Items;
                    self.CartId = self.CartProductsList._id;
                    self.ShopCOD = self.CartProductsList.ShopCOD;
                    self.CartProducts.forEach((cartProduct: any) => {
                    if(cartProduct.Product.IsProductDeleted)
                        self.notAvailableCount += 1;
                    cartProduct.productEdit = false;
                });
                self.cartCostDetails = self.CartProductsList.cartCostDetails;
                self.shopLists = self.CartProductsList.shopLists;
                    var shpModel = self.CartProductsList.shopListArray;
                    shpModel.forEach(ele => {
                        if (!self.shopListArray[ele.name]) {
                            self.shopListArray[ele.name] = [];
                            ele.arrayVal.CODAvail = ele.COD;
                            ele.arrayVal.find(x=>{
                                if(x.hasOwnProperty("Variation"))
                                {
                                    if(x.Product.useVariation)
                                    {
                                        x.Product.Variations.find(y => {
                                            if(y._id == x.Variation)
                                            {
                                                x.Variation = y;
                                            }
                                        })
                                    }
                                }
                            })
                            self.shopListArray[ele.name].push(ele.arrayVal);
                        }
                    });
                    self.addToCartService.getSavedForLater().then(savedForLater => {
                        if(params.id == "buynow")
                        {
                            self.savedForLaterProducts = [];
                        }
                        else
                        {
                            self.savedForLaterProducts = JSON.parse(savedForLater).Items;
                        }
                        self.loadingservice.hide();                                                                    
                    }, error => self.errorMessage = <any>error);
                }, err=> {self.errorMessage = <any>err});
            }
            });
    }
    ShippingAddressFunc(){
        if(this.getUserToken !=null)
        {
                var PrimaryAddress = false;
                this.shippingProfileService.showShippingAddress().then(
                    resp => {
                        var index;
                        this.AddressSwapJson=[];
                        this.ShippingProfile = JSON.parse(resp);
                        this.ShippingAddress = this.ShippingProfile.ShippingAddress;
                        if(this.ShippingAddress.length > 0)
                        {
                            for (var i = 0; i < this.ShippingAddress.length; i++) 
                            {
                                if (this.ShippingAddress[i].IsPrimary == true) {
                                    this.AddressSwapJson.splice(0, 0, this.ShippingAddress[i]);
                                }
                                else {
                                    this.AddressSwapJson.push(this.ShippingAddress[i]);
                                }
                                if ( this.AddressSwapJson[i].IsPrimary) {
                                    PrimaryAddress = true;
                                    index = i;
                                }
                            }
                            if(PrimaryAddress)
                            {
                                this.ShowShippingAddress = true;
                                this.tempaddressModel= this.AddressSwapJson[index];
                            }
                            else
                            {
                                this.tempaddressModel= this.AddressSwapJson[0];
                            }
                        }
                    },
                    error => this.errorMessage = <any>error);
                }
    }

    ngDoCheck() {
        if (this.CartProducts) {
        }
    }

    public validateQuantity(cartProduct,from) {
        var AvailableQty = this.QtyCheck(cartProduct);        
        if (cartProduct.Qty < 0) {
            cartProduct.Qty = 1;
        }
        else if (cartProduct.Qty > 20) {
            this.snack.ErrorSnack('You can add only 20 quantities per user');
            cartProduct.Qty = 20;
            this.updateCartProduct(cartProduct);
        }
         else if (cartProduct.Qty > AvailableQty) {
            if(from == -1)
            {
                cartProduct.Qty = AvailableQty;
                this.updateCartProduct(cartProduct);
            }
            else
            {
                cartProduct.Qty -= 1;
                this.snack.ErrorSnack('You can add only '+ AvailableQty +' Product(s).');                
            }
        }
        else{
            this.updateCartProduct(cartProduct);
        }
    }
    updateCartProduct(cartProduct)
    {
        this.loadingservice.show();                
        var cartdetail:any = {};
        cartdetail.Product = cartProduct.Product._id;
        cartdetail.Qty = cartProduct.Qty; 
        cartdetail.BuyNow = cartProduct.BuyNow;    
        cartdetail.addToCart = false;           
        if(cartProduct.hasOwnProperty("Variation"))
        {
            cartdetail.Variation = cartProduct.Variation._id;
            if(this.variation_id)
                cartdetail.NewVariation = this.variation_id;                                    
        }
        if( this.getUserToken!=null)
        {
            this.addToCartService.setCartProduct(cartdetail)
            .then(result => {
                var data = JSON.parse(result);
                if(data.msg && data.flag != 81)
                {
                    this.snack.ErrorSnack(data.msg);
                }            
                this.Init();            
            });
        }
        else
        {
            this.tempCart.updateQtyAndVariation(cartdetail);
            this.Init();            
        }
    }
    public variationChange(model) 
    {
        var variation;
        if(model.Variation.hasOwnProperty("Value2"))
        {
            variation = model.Product.Variations.find(ele => ((ele.Value1 == this.cartPrdVariationModel[0].name) && (ele.Value2 == this.cartPrdVariationModel[1].name) && (ele.Active && ele.InStock && ele.Qty) ));
        }
        else
        {
            variation = model.Product.Variations.find(ele => ((ele.Value1 == this.cartPrdVariationModel[0].name) && (ele.Active && ele.InStock && ele.Qty)))            
        }
        if(variation)
        {
            if(variation.Qty < model.Qty)
            {
                this.snack.ErrorSnack('You can add '+variation.Qty+' for this Product. Please Decrease you Quantity and try again.');
            }
            else
            {
                this.variation_id = variation._id;
            }
        }
        else
        {
            this.snack.ErrorSnack('This variation is not available');
        }
    }

    public deleteCartProduct(cartProduct) {
        if( this.getUserToken!=null)
        {
            this.addToCartService.deleteCartProduct(cartProduct)
                .then(
                cartProducts => {
                    this.Init();
                    this.app = this.Inj.get(AppComponent);
                    this.app.cart();
                });
        }
        else
        {
            this.tempCart.removeProduct(cartProduct);
            this.Init(); 
            this.app = this.Inj.get(AppComponent);
            this.app.cart();
        }
    }
    public MediaImage(cartProduct)
    {
        var image = () =>{
            return cartProduct.Product.Media.find(x=>x.IsPrimary).EditedImage;
        }
        if(cartProduct.hasOwnProperty("Variation"))
        {
            if(cartProduct.Variation.hasOwnProperty("Image"))
            {
                if(cartProduct.Variation.Image !="")
                {
                    return cartProduct.Variation.Image;
                }
                else
                {
                    return image();
                }
            }
            else
            {
                return image();
            } 
        }
        else
        {
            return image();
        }
    }

    public deleteSavedForLater(cartProduct) {
        this.loadingservice.show();
        var query:any = {
            ProductId: cartProduct.Product._id
        }
        if(cartProduct.Variation)
        {
            query.VariationId = cartProduct.Variation._id;
        }
        this.addToCartService.deleteSavedForLater(query)
            .then(deleted => {
                this.Init();
            },
            error => this.errorMessage = <any>error);
    }

    public editCartProduct(cartProduct) 
    {
        
        if(cartProduct.Variation.Name1)
        {
            this.cartPrdVariationModel[0].name = cartProduct.Variation.Value1;
        }
        if(cartProduct.Variation.hasOwnProperty("Name2"))
        {
            this.cartPrdVariationModel[1].name = cartProduct.Variation.Value2;
        }
        cartProduct.productEdit = true;
        this.ChangeVariationModel = {};
    }
    validatesize(object)
  {
    if (object.value.length > object.maxLength)
      object.value = object.value.slice(0, object.maxLength)
  }
    public saveCartProduct(cartProduct,isSaveForLater) {
        this.loadingservice.show();
        var query:any = {
            ProductId: cartProduct.Product._id,
            Qty: cartProduct.Qty,
            BuyNow : false
        }
        var qry:any ={
            Product: cartProduct.Product._id,
            Qty: cartProduct.Qty,
            BuyNow : false,
            addToCart : false
        }
        if(cartProduct.Variation)
        {
            qry.Variation = cartProduct.Variation._id;            
            query.VariationId = cartProduct.Variation._id;
        }
        this.addToCartService.setCartProduct(qry)
            .then(
            cartItems => {
                var resData=JSON.parse(cartItems);
                if(resData.msg &&resData.flag != 81)
                {
                    this.snack.ErrorSnack(resData.msg);     
                    this.loadingservice.hide();               
                }
                else
                {
                    this.addToCartService.deleteSavedForLater(query).then(result=>{
                        this.app = this.Inj.get(AppComponent);
                         this.app.cart();
                          this.Init();                   
                    });
                }
            },
            error => this.errorMessage = <any>error);
    }
    public saveVariationChange()
    {
        this.addToCartService.cartChange(this.ChangeVariationModel).then(
            response =>{
                this.Init();
            },
            err =>{}
        );
    }
    public saveForLaterCartProduct(cartProduct) {
        var body:any = {};
        body.Qty = cartProduct.Qty;
        body.BuyNow = cartProduct.BuyNow;
        body._id = cartProduct._id;        
        body.Product = {_id:cartProduct.Product._id};
        if(cartProduct.hasOwnProperty("Variation"))
        {
            body.Variation = {_id:cartProduct.Variation._id};
        }
        this.addToCartService.saveForLaterCartProduct(body)
            .then(
            cartItems => {
                cartItems = JSON.parse(cartItems);
                if(cartItems.hasOwnProperty("msg"))
                {
                    this.snack.ErrorSnack(cartItems.msg);
                }
                else
                {
                    this.app = this.Inj.get(AppComponent);
                    this.app.cart();
                    this.Init();
                }
            },
            error => this.errorMessage = <any>error);
    }
    public formJsonLikeCart(productInfo): any {
        let cartProduct = <any>{};
        cartProduct.Shop = {
            Id: productInfo.Shop.Id,
            Name: productInfo.Shop.Name
        }
        cartProduct.Product = productInfo.Product;
        return cartProduct;
    }

    public openContactShopModal(shopInfo) {
        this.selectedSeller = shopInfo[0][0].Product.ShippingProfile.User;
        jQuery("#contactShopModal").modal('show');
    }

    public sendMessageToShop() {
        this.selectedShopInfo.To = this.selectedSeller;
        this.addToCartService.contactShop(this.selectedShopInfo)
            .then(
            messageResponse => {
                jQuery("#contactShopModal").modal('hide');
                this.snack.SuccessSnack( "Message Sent Successfuly");
            },
            error => this.errorMessage = <any>error);
    }

    public onChangeSubProfile(cartProductInfo) {
        var locationInfo = "";
        this.utilService.geoLocation().then(
            response => {
                locationInfo =  JSON.parse(response).country;
                var shippingMethodObject = {
                    "Profile": cartProductInfo.ShippingProfile.Profiles._id,
                    "ShippingMethod": cartProductInfo.ShippingMethod,
                    "Country": locationInfo
                };
                this.addToCartService.ChangeProductSubProfile(cartProductInfo.Id, shippingMethodObject)
                    .then(response => {
                        this.Init();
                    },
                    error => this.errorMessage = <any>error);
            }, error => this.errorMessage = <any>error);          
        
    }

public cancelCoupon(cart){
    this.loadingservice.show();                                                                        
    var product:any = { ProductId: cart.Product._id};
    product.Coupon = cart.Coupon._id;    
    if(cart.hasOwnProperty("Variation"))
    {
        product.VariationId = cart.Variation._id;
    }
    this.addToCartService.cancelCoupon(product)
        .then(
        response => {
            this.ShowCouponDiscount = false;
            this.Init();
        }, error => this.errorMessage = <any>error);
}
    public applyCoupon(couponCode,ShopId) {
        this.ShowCouponDiscount = false;
        if( couponCode == null || couponCode == " ")
        {
            this.snack.ErrorSnack( "Invalid coupon Code");
        }
        else{
        this.loadingservice.show();                                                                                    
        var product:any = { Coupon: couponCode,ShopId : ShopId};            
            this.addToCartService.applyCoupon(product)
            .then(
            response => {
                var resData = JSON.parse(response);
                if(resData.msg)
                {
                    this.snack.ErrorSnack(resData.msg);
                    this.loadingservice.hide();                                                                                    
                } 
               else
               {
                this.ShowCouponDiscount = true;
                this.snack.SuccessSnack("Coupon applied Successfuly");
                this.Init();
               }               
            }, error => this.errorMessage = <any>error);
        }
        }
        Checkout() {
            this.loadingservice.show();
            if (this.notAvailableCount == 0) {
                this.showError = false;
                this.IsOrderCreating = true;
                if (this.paymentMethod == "COD") {
                    let body = { "PaymentMethod": "COD","Captcha":this.captchaEntered, "ShippingAddress": this.tempaddressModel, "BuyNow": this.isBuyNowCart };
                    this.addToCartService.getPaymentApprovalUrlPaybiz(body, this.CartId)
                        .then(cart => {
                            let CartDetails = JSON.parse(cart);
                            this.IsOrderCreating = false;
                            if (CartDetails.msg && CartDetails.Flag != 8) {
                                this.snack.ErrorSnack(CartDetails.msg);
                                this.loadingservice.hide();
    
                            }
                            else if(CartDetails.Flag == 8)
                            {
                                this.loadingservice.hide();
                                this.snack.ErrorSnack(CartDetails.msg);
                                this.Init();
                                this.ShippingAddressFunc();
                            }
                            else {
                                this.router.navigate(['/user/success/' + CartDetails.groupId]);
                            }
                            this.loadingservice.hide();
    
                        },
                            error => {
                                this.errorMessage = <any>error;
                                this.loadingservice.hide();
                            });
                }
                else {
                    let body = { "PaymentMethod": "Online", "ShippingAddress": this.tempaddressModel, "BuyNow": this.isBuyNowCart };
                    this.addToCartService.getPaymentApprovalUrlPaybiz(body, this.CartId)
                        .then(cart => {
                            this.IsOrderCreating = false;
                            let CartDetails = JSON.parse(cart);
                            if (CartDetails.msg)
                                this.snack.ErrorSnack(CartDetails.msg);
                            else if (CartDetails.hasOwnProperty('URL'))
                                window.location.href = CartDetails.URL;
                            else
                                alert(CartDetails.message);
                            this.loadingservice.hide();
                        },
                            error => {
                                this.errorMessage = <any>error;
                                this.loadingservice.hide();
                            });
                }
                }
            else {
                this.showError = true;
                window.scrollTo(0, 20);
                this.loadingservice.hide();
            }
        }
    
        CheckoutfromShop(id) {
            this.loadingservice.show();
            if (this.notAvailableCount == 0) {
                this.showError = false;
                this.IsOrderCreating = true;
                if (this.paymentMethod == "COD") {
                    let body = { "PaymentMethod": "COD","Captcha":this.captchaEntered, "ShippingAddress": this.tempaddressModel, "BuyNow": this.isBuyNowCart, "ShopId": id };
                    this.addToCartService.getPaymentApprovalUrlPaybiz(body, this.CartId)
                        .then(cart => {
                            this.IsOrderCreating = false;
                            let CartDetails = JSON.parse(cart);
                            if (CartDetails.msg) {
                                if (CartDetails.hasOwnProperty("Code")) {
                                    this.snack.ErrorSnack('Sorry, Your ' + CartDetails.Code + ' has been expired. Please remove your coupon or add new coupon');
                                }
                                else {
                                    this.snack.ErrorSnack(CartDetails.msg);
                                }
                                this.loadingservice.hide();
                            }
                            else {
                                this.router.navigate(['/user/success/' + CartDetails.groupId]);
                            }
                            this.loadingservice.hide();
                        },
                            error => {
                                this.errorMessage = <any>error;
                                this.loadingservice.hide();
                            });
                }
                else {
                    let body = { "PaymentMethod": "Online", "ShippingAddress": this.tempaddressModel, "BuyNow": this.isBuyNowCart, "ShopId": id };
                    this.addToCartService.getPaymentApprovalUrlPaybiz(body, this.CartId)
                        .then(cart => {
                            this.IsOrderCreating = false;
                            let CartDetails = JSON.parse(cart);
                            if (CartDetails.msg) {
                                if (CartDetails.hasOwnProperty("Code")) {
                                    this.snack.ErrorSnack('Sorry, Your ' + CartDetails.Code + ' has been expired. Please remove your coupon or add new coupon');
                                }
                                else {
                                    this.snack.ErrorSnack(CartDetails.msg);
                                }
                                
                            }
                            else if (CartDetails.hasOwnProperty('URL'))
                                window.location.href = CartDetails.URL;
                            else
                                alert(CartDetails.message);

                                this.loadingservice.hide();
                        },
                            error => {
                                this.errorMessage = <any>error;
                                this.loadingservice.hide();
                            });
                }
            }
            else {
                this.showError = true;
                window.scrollTo(0, 20);
                this.loadingservice.show();
            }
        }

    changeAddress(id){
        if(this.guestUser)
        {
            jQuery("#loginmodal").modal('show');
        }
        else
        {
            this.onChangeCountry(101);
            this.checkoutshopid = id;
            if(this.ShippingAddress.length <= 0){
                 this.AddShippingAddressTemp=true;
                 jQuery("#selectAddress").modal('show');
             }
            else{
                 this.AddShippingAddressTemp=false;
                 jQuery("#selectAddress").modal('show');
            }
        }
    }
    proceed()
    {
        if(this.tempaddressModel)
            {
                jQuery("#selectAddress").modal('hide');
                if(this.checkoutshopid != "1")
                {
                    this.CheckoutfromShop(this.checkoutshopid);
                }
                else
                {
                    this.Checkout();
                }
            }
        
    }
    handleReady(){
        this.captchaIsReady = true;
    }
    setDefault(temp) {
        for (var i = 0; i < this.AddressSwapJson.length; i++) 
        {
                this.AddressSwapJson[i].IsPrimary=false;
        }
        temp.IsPrimary=true;
         this.tempaddressModel=temp;
    }
    onChangeCountry(countryid) {
        var val = { "CountryId": countryid };
        this.registerService.getStates(val).then(
            statesInfo => {
                this.statesArray = JSON.parse(statesInfo);
            }, error => this.errorMessage = <any>error);
    }
    
     public productInfo(product) {
        this.router.navigate(['/user/product', product]);  
    }
    
    addAddress() {
        console.log("test");
     
        this.addressModel.Country = "India";
        if (this.ShippingAddress.length != 5) {
            this.loadingservice.show();
                this.shippingProfileService.addShippingAddress(this.addressModel).then(
                        response => {
                            var result = JSON.parse(response);
                        if(result.hasOwnProperty('flag')){
                            this.loadingservice.hide();
                            this.snack.ErrorSnack( "Invalid Pincode");
                        }else{
                            this.ShippingAddressFunc();
                            this.loadingservice.hide();
                            this.tempaddressModel=this.addressModel;
                            this.AddShippingAddressTemp = false;
                        }
                            
                    },
                    error => error => this.errorMessage = <any>error);     
        }
        else {
            this.AddShippingAddressTemp = false;
            this.tempaddressModel=this.addressModel;
            jQuery("#selectAddress").modal('hide');
          }

    }
    CheckPinCode()
    {
        var model ={country:this.addressModel.Country,State:this.addressModel.State,pincode:this.addressModel.Zip};
        return this.registerService.checkPincode(model).then(result=>{
            var pincode= JSON.parse(result);
           return result;
        });
    }
    QtyCheck(cartProduct)
    {
        var AvailableQty;
        AvailableQty = cartProduct.Product.QuantityAvailable
        if(cartProduct.hasOwnProperty("Variation"))
        {
            if(!cartProduct.Product.useDefaultQuantity)
            {
                AvailableQty = cartProduct.Variation.Qty;
            }
        }
        return AvailableQty;
    }
    public plusQuantity(cartProduct) {
            cartProduct.Qty = cartProduct.Qty + 1;
            this.validateQuantity(cartProduct,1);
    }
    public minusQuantity(cartProduct) {
        if (cartProduct.Qty > 1) {
            cartProduct.Qty = cartProduct.Qty - 1;
            this.validateQuantity(cartProduct,-1);            
        }
    }
    editAddress(Id) {
       
        this.AddShippingAddressTemp=true;
        if(Id==null){
            this.editbutton = false;
              this.newbutton = true;   
            }
            else{
              this.editbutton = true;
              this.newbutton = false;
            }
        this.shippingProfileService.showShippingAddress().then(
            response => {
                 console.log("Shipping : ",response);
                 this.ShowUserProfile = JSON.parse(response);
                 console.log(this.ShowUserProfile);
                 this.ShowUserAddress = this.ShowUserProfile.ShippingAddress;
                 console.log("Total Address Length", this.ShowUserAddress.length);
                 
                 for(var i=0;i<this.ShowUserAddress.length;i++){
                     if(Id==this.ShowUserAddress[i]._id){
                         this.AddAddressflag=true;
                         console.log("Got ID Value")
                         this.addressModel=this.ShowUserAddress[i];
                         break;
                     }
                     }
                      console.log("Address Model : ",this.addressModel) 
                      this.countryName=this.addressModel.Country;
               
             });    
    }
    updateAddress(){
        
                this.shippingProfileService.putShippingAddress(this.addressModel).then(response => {
                    var result = JSON.parse(response);
                    if(result.hasOwnProperty('flag')){
                        this.loadingservice.hide();
                        this.snack.ErrorSnack( "Invalid Pincode");
                    }else{
                        this.snack.SuccessSnack('Edited Successfully');  
                        this.ShippingAddressFunc();
                        this.AddShippingAddressTemp = false; 
                    }
         });
    }
    addShippingAddress(){
        this.addressModel=<any>{};
          this.userService.getUserDetailsById().then(
                        result=>
                        {
                          var res=JSON.parse(result);
                          this.addressModel.FirstName=res.FirstName;
                          this.addressModel.LastName=res.LastName;
                          this.addressModel.Phone=res.Mobile;
                        });
        this.AddShippingAddressTemp = true;
        this.newbutton = true;
    }
    cashOnDelivery()
    {  
        this.paymentMethod='COD';
        this.generateCaptcha();
        this.Showcaptcha=true;
       
    }
    netBanking()
    {
        this.paymentMethod='Payu';
        this.Showcaptcha=false;
        this.captchaVerify=false;
    }
    generateCaptcha() {
               this.captchaVerify = true;
            this.loader = true;
            this.userService.generateCaptcha().then(res => {
                var result = JSON.parse(res);
                this.captchaEntered="";
                this.loader = false;
                this.captchaDisplayed = result.CaptchaImage;
            });
    }
    validateCaptcha()
    {
        if(this.captchaEntered.length==5)
        {
            this.userService.getCaptcha(this.captchaEntered).then(res=> {
            var result=JSON.parse(res);
            if(result.mes==true)
            {
              this.captchaVerify=false;
              this.captchaerror=false;
            }
            else{
            this.captchaerror=true;
            }
            })

        }
    }
}