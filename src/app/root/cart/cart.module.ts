import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { CartComponent } from './cart.component';
import {NgPipesModule} from 'ngx-pipes';
import { RouterModule, Routes } from '@angular/router';
import { MatInputModule } from '@angular/material';
import { NgxCaptchaModule } from 'ngx-captcha';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule,  } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';

import { NgxSpinnerModule } from 'ngx-spinner';


const cart:Routes = [
  { path: '', redirectTo:'view',pathMatch:'full' },
  { path: 'view', component: CartComponent },
  { path: 'buy/:id', component: CartComponent }
]
@NgModule({
  imports: [
    SharedModule,
    NgPipesModule,
    RouterModule.forChild(cart),
    MatInputModule,
    NgxCaptchaModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    MatFormFieldModule,
    MatSelectModule
  ],
  declarations: [ 
    CartComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class CartModule { }