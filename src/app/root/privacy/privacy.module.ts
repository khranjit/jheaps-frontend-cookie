import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import {PrivacyComponent } from './privacy.component';
import {BuyerComponent } from './_buyer-policy/buyer-policy.component';
import {SellerComponent } from './_seller-policy/seller-policy.component';
import {ThirdpartyComponent } from './_thirdparty-policy/thirdparty-policy.component';
import {PrivacyPolicyComponent } from './_privacy-policy/privacy-policy.component';
import { RouterModule, Routes } from '@angular/router';

const shop:Routes = [
  { path: 'list', component: PrivacyComponent,
    children:[
      { path: '', redirectTo:'buyer-policy', pathMatch:'full' },
      { path: 'buyer-policy', component: BuyerComponent },
      { path: 'seller-policy', component: SellerComponent },
      { path: 'thirdparty-policy', component: ThirdpartyComponent },
      { path: 'privacy-policy', component: PrivacyPolicyComponent }
    ] 
  },

]

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(shop)
  ],
  declarations: [ 
    PrivacyComponent,
    BuyerComponent,
    SellerComponent,
    ThirdpartyComponent,
    PrivacyPolicyComponent
  ]
})
export class PrivacyModule { }