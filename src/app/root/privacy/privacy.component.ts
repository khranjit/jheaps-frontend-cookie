import { Component} from '@angular/core';
import { Router, NavigationEnd} from '@angular/router'; 

@Component({ 
   selector: 'privacy',
  templateUrl: './privacy.component.html'
})

export class PrivacyComponent{
  constructor(private router: Router){
    window.scrollTo(0,0);
  }
}

