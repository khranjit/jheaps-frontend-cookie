import { Component} from '@angular/core';
import { Router, NavigationEnd} from '@angular/router'; 

@Component({ 
   selector: 'privacy-policy',
  templateUrl: './privacy-policy.component.html'
})

export class PrivacyPolicyComponent{
  constructor(private router: Router){
    window.scrollTo(0,0);
  }
}