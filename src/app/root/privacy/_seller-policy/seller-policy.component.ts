import { Component} from '@angular/core';
import { Router, NavigationEnd} from '@angular/router'; 

@Component({ 
   selector: 'seller-policy',
  templateUrl: './seller-policy.component.html'
})

export class SellerComponent{
  display1:boolean = false;
  constructor(private router: Router){
    window.scrollTo(0,0);
  }
}
