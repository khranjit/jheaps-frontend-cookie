import { Component} from '@angular/core';
import { Router, NavigationEnd} from '@angular/router'; 

@Component({ 
   selector: 'thirdparty-policy',
  templateUrl: './thirdparty-policy.component.html'
})

export class ThirdpartyComponent{
  constructor(private router: Router){
    window.scrollTo(0,0);
  }
}
