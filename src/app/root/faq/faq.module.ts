import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { FaqComponent } from './faq.component';
import { RouterModule, Routes } from '@angular/router';
import { BuyerComponent } from './buyer/buyer.component';
import { SellerComponent } from './seller/seller.component';

const shop:Routes = [
  {path:'', component:FaqComponent, children:[
    { path: '', redirectTo: 'buyer-faq', pathMatch: 'full'},
    { path:'buyer-faq', component:BuyerComponent },
    { path: 'seller-faq', component:SellerComponent }
  ]}
]

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(shop)
  ],
  declarations: [ 
    FaqComponent, BuyerComponent, SellerComponent
  ]
})
export class FaqModule { }