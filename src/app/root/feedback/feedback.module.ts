import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { FeedbackComponent } from './feedback.component';
import { RouterModule, Routes } from '@angular/router';
import { MatInputModule } from '@angular/material';

const shop:Routes = [
  { path: '', component: FeedbackComponent }
]

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(shop),
    MatInputModule,
  ],
  declarations: [ 
    FeedbackComponent
  ]
})
export class FeedbackModule { }