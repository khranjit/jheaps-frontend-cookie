import { Component} from '@angular/core';
import { UserService } from '../../services/user.service';
import { FeedbackService } from '../../services/feedback.service';
import { NotifyService } from '../../services/notify.service';

declare var jQuery : any;

@Component({
    
    selector: 'feedback',
    providers: [UserService, FeedbackService,NotifyService],
    templateUrl: './feedback.component.html'
})

export class FeedbackComponent {

    Pagenew : Number;
    public sentMessagesList: any;

    ShowUserProfile: any;
    ShowUserName: any;
    ShowUserRole: any;
    errorMessage: any;
    feedbackArray = [];
    feedbackObj = { "UserName": "", "Feedback": "" };
    commentObj={"FeedbackId": "","Comments":{"Comment": ""}};
    voteObj={"FeedbackId" : ""};
    setStatusObj={"FeedbackId" : "","Status" : ""};
    feed: any;
    comment: any;
    idea:string;
    getAllFeedbackList: any;
    getAllFeedback: any;
    getAllTopFeedbackList: any;
    getAllTopFeedback: any;
    getAllStatusFeedbackList: any;
    getAllStatusFeedback: any;
    getOwnFeedbackList: any;
    getOwnFeed: any;
    commentArray: any;
    owncommentArray: any;
    underReviewArray:any;
    plannedArray:any;
    startedArray:any;
    completedArray:any;
    declinedArray:any;
    UserIdArray:any;
    newidea: boolean;
    ownidea: boolean;
    topidea: boolean;
    underreviewVisible:boolean;
    plannedVisible:boolean;
    startedVisible:boolean;
    completedVisible:boolean;
    declinedVisible:boolean;
    adminViewFeedback:any;
    public pages: any[];
    public pagesnew: any[]; 
    selectedFeedback:any;
    selectedOwnFeedback:any;
    AdminStatus:any;
    ShowUserId:any;
    totalPages: number;
    pageNumber: number;
    currentPage = 1;
  
    from = 0;
    size = 10;
    from1 = 0;
    size1 = 10;
    from2 = 0;
    size2 = 10;
    total = 0;
    from3 = 0;
    size3 = 10;
    StatusValue : string;
    deleteFeedbackId: string;
    deleteCommentFeedbackId: string;
    deleteCommentId: string
    getUserToken: string;
    
    constructor(private userService: UserService, private feedbackService: FeedbackService,private toasterPopService :NotifyService) {
        //this.feedbackArray=["1","2","3"];
        this.getAllFeedbackList = [];
        this.getAllFeedback = [];
        this.getAllTopFeedbackList = [];
        this.getAllTopFeedback = [];
        this.getAllStatusFeedbackList = [];
        this.getAllStatusFeedback = [];
        this.getOwnFeedbackList = [];
        this.getOwnFeed = [];
        this.commentArray=[];
        this.underReviewArray=[];
        this.plannedArray=[];
        this.startedArray=[];
        this.completedArray=[];
        this.declinedArray=[];
        this.UserIdArray=[];
        this.selectedFeedback={};
        this.idea = "";
        this.ShowUserId="";
        this.newidea=true;
        this.ownidea=true;
        this.topidea=true;
        this.underreviewVisible=true;
        this.plannedVisible=true;
        this.startedVisible=true;
        this.completedVisible=true;
        this.declinedVisible=true;
        this.adminViewFeedback=true;
        this.totalPages = 0;
        this.pageNumber = 0;
        this.size = 10;
        this.init();
        

    }
    init() {
        window.scrollTo(0,0);       
        this.getUserToken=localStorage.getItem('userToken');
        this.from = 0 ;
        this.currentPage = 1;
        this.sentMessagesList = [];
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response);
                this.ShowUserName = this.ShowUserProfile.Items[0].Profile.FirstName;
                console.log('user type'+this.ShowUserProfile.Items[0].Roles);
                this.ShowUserRole=this.ShowUserProfile.Items[0].Roles;
                this.ShowUserId=this.ShowUserProfile.Items[0]._id;
             },
            error => this.errorMessage = <any>error);
             this.feedback(this.from,this.size);
     }
     currentPageFun(){
         this.currentPage=1;
     }
     feedback(from,size){
        console.log('feedbackfunction');
        this.feedbackService.getFeedback(from,this.size).then(
            response => {
                this.getAllFeedbackList = JSON.parse(response);
                this.totalPages = this.getAllFeedbackList.TotalPages;
                this.getAllFeedback = this.getAllFeedbackList.Items;
                  this.pages = [];
                for(let i=1; i<=this.totalPages; i++)
                {
                    this.pages.push(i);
                }
            },
            error => this.errorMessage = <any>error);
            
     }
    vote(id) {
        this.voteObj.FeedbackId=id; 
        this.feedbackService.countVote(this.voteObj).then(
            response => {
                this.init();
            },
            error => this.errorMessage = <any>error);  
    }
    
    postFeedback() {
       if(this.idea) {
           if(this.feed) {
            this.feedbackService.sendFeedback(this.idea,this.feed).then(
                response => {
                    this.toasterPopService.SuccessSnack("Your Feedback Posted Successfully");
                    this.idea = "";
                    this.feed = "";
                    this.init();
                },
                error => this.errorMessage = <any>error);
           }
           else {
            this.toasterPopService.ErrorSnack("Please fill the description");
            }
       }
       else {
            this.toasterPopService.ErrorSnack("Please fill your idea");
       }
    }
    ViewFeedback(feedback){
        this.newidea=false;
        this.selectedFeedback=feedback;
        this.commentArray=feedback.Comments;
        
    }
   
    postComment(feedbackId){
        this.commentObj.FeedbackId = feedbackId;
        this.commentObj.Comments = {"Comment":this.comment};
        this.feedbackService.putComment(this.commentObj).then(
            response => {
                this.commentArray=JSON.parse(response).Comments;
                this.toasterPopService.SuccessSnack( "Your Command Posted Successfully");
                
            },
            error => this.errorMessage = <any>error);
    }
    
    status(event){
        this.AdminStatus=event.target.value;
    }
    
    changeStatus(id)
    {   
        this.setStatusObj={"FeedbackId" : id,"Status" : this.AdminStatus};
       this.feedbackService.updateStatus(this.setStatusObj).then(
            response => {
                this.init();
            },
            error => this.errorMessage = <any>error);
    }
    
    openDeleteFeedbackModal(feedid){
        this.deleteFeedbackId=feedid;
        jQuery("#deleteFeedbackModal").modal("show");
    }
    
    Delete(){
        this.feedbackService.deleteFeedback(this.deleteFeedbackId).then(
            response => {
                this.init();
            }, error => this.errorMessage = <any>error);
      
       jQuery("#deleteFeedbackModal").modal("hide");
  
    }
     openDeleteCommentModal(feedid,commentid){
        this.deleteCommentFeedbackId=feedid;
        this.deleteCommentId=commentid;
        jQuery("#deleteCommentModal").modal("show");
    }
     DeleteComment(){
        this.feedbackService.deleteComment(this.deleteCommentFeedbackId,this.deleteCommentId).then(
            response => {
                this.commentArray=JSON.parse(response).Comments;
            },
            error => this.errorMessage = <any>error);
         jQuery("#deleteCommentModal").modal("hide");
    }
     NewIdea_NextPage() {

        if(this.currentPage < this.totalPages){
            this.feedback( (this.currentPage ), this.size);
            this.currentPage += 1;  
        }

    }
    selectnewPage(PageNumber){
        this.currentPage = PageNumber;
        console.log(PageNumber);
        console.log(this.currentPage);
        this.feedback( (this.currentPage - 1), this.size );
        this.pageNumber = 0;
    }
    NewIdea_PreviousPage() {
        if(this.currentPage > 1){
            this.currentPage -= 1; 
            this.feedback((this.currentPage - 1), this.size ); 
             
        }
    } 
    TopIdea_NextPage() {

        if(this.currentPage < this.totalPages){
            this.TopIdeaFunc( (this.currentPage ), this.size);
            this.currentPage += 1;  
        }
    }
    selectTopPage(pageNumber){
        this.currentPage = pageNumber;
        // console.log(pageNumber);
        // console.log(this.currentPage);
        this.TopIdeaFunc( (this.currentPage - 1), this.size );
        // this.pageNumber = 0;
       

    }
    TopIdea_PreviousPage() {
        if(this.currentPage > 1){
            this.currentPage -= 1; 
            this.TopIdeaFunc((this.currentPage - 1), this.size ); 
             
        }
    }
    MyIdea_NextPage() {
        if(this.currentPage < this.totalPages){
            this.TopIdeaFunc( (this.currentPage ), this.size);
            this.currentPage += 1;  
        }
    }
    MyIdea_PreviousPage() {
        if(this.currentPage > 1){
            this.currentPage -= 1; 
            this.MyFeedbackFunc((this.currentPage - 1), this.size ); 
        }
    }
    selectMyIdeaPage(pageNumber){
        this.currentPage = pageNumber;
        console.log('myidea'+pageNumber);
        console.log(this.currentPage);
        this.MyFeedbackFunc( (this.currentPage - 1), this.size );
        // this.pageNumber = 0;
    }
    NextPage() {

        if(this.currentPage < this.totalPages){
            this.statusFunc( (this.currentPage ), this.size);
            this.currentPage += 1;  
        }
    }
    selectPage(pageNumber){
        this.currentPage = pageNumber;
        console.log('myidea'+pageNumber);
        console.log(this.currentPage);
        this.statusFunc( (this.currentPage - 1), this.size );
        // this.pageNumber = 0;

    }
    PreviousPage() {
        if(this.currentPage > 1){
            this.currentPage -= 1; 
            this.statusFunc((this.currentPage - 1), this.size ); 
        }
    }
    ResetPageFunc(){
        this.totalPages = 0;
        this.pageNumber = 0;
    }
    MyFeedbackFunc(from2,size2){
        //this.currentPage = 1;
        this.feedbackService.getOwnFeedback(from2,this.size2).then(
            response => {
          //      console.log("Own Feedback response", response);
                this.getOwnFeedbackList = JSON.parse(response);
                this.totalPages = this.getOwnFeedbackList.TotalPages;
                this.getOwnFeed = this.getOwnFeedbackList.Items;
                this.pages = [];
                for(let i=1; i<=this.totalPages; i++)
                {
                    this.pages.push(i);
                }                
            },
            error => this.errorMessage = <any>error);
    }
    TopIdeaFunc(from1,size1){
       // this.currentPage = 1;
        console.log('TOpfun');
        this.feedbackService.getFeedback(from1,this.size1).then(
            response => {
                this.getAllTopFeedbackList = JSON.parse(response);
                this.totalPages = this.getAllTopFeedbackList.TotalPages;
                this.getAllTopFeedback = this.getAllTopFeedbackList.Items;
                this.pages = [];
                for(let i=1; i<=this.totalPages; i++)
                {
                   // console.log(i);
                    this.pages.push(i);
                }
              //  console.log('TopIdea',this.getAllTopFeedback);
            },
            error => this.errorMessage = <any>error);
    }
    statusFunc(from3,size3){
      //  this.currentPage = 1;
        console.log('status value in function:'+this.StatusValue);
        this.feedbackService.getStatusFeedback(from3,this.size3, this.StatusValue).then(
                response => {
                    this.getAllStatusFeedbackList = JSON.parse(response);
                    this.totalPages = this.getAllStatusFeedbackList.TotalPages;
                    this.getAllStatusFeedback = this.getAllStatusFeedbackList.Items;
                    this.pages = [];
                    for(let i=1; i<=this.totalPages; i++)
                    {
                       // console.log(i);
                        this.pages.push(i);
                    }
                //     console.log('getAllStatusFeedback',this.getAllStatusFeedback);
                },
                error => this.errorMessage = <any>error);
    }
    
}
