import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { AboutusComponent } from './aboutus.component';
import { RouterModule, Routes } from '@angular/router';

const shop:Routes = [
  { path: '', component: AboutusComponent }
]

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(shop)
  ],
  declarations: [  
    AboutusComponent
  ]
})
export class AboutusModule { }