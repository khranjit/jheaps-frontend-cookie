import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CheckoutService } from '../../services/checkout.service';

@Component({
  
  selector: 'failure',
  providers: [CheckoutService],
  templateUrl: './failure.component.html'
})

export class FailureComponent {
   errorMessage: any;
    paymentStatusInfo : any;
    paymentStatus : any;
    public shopLists:any;
    public shopListArray: any;
    products :any;
    grantTotal:any;
    shippingAddress:any;
    Name:any;
    Line1:any;
    Line2:any;
    Country:any;
    City:any;
    Zip:any;
    State:any;
    totalCost:any;
    productCost:any;
    shippingCost:any;
    couponDiscount:any;
    public productCurrency: any = {value: "USD"};
    orderStatus:any;
    
    constructor(private route: ActivatedRoute, private router: Router,private checkoutService: CheckoutService) {
        this.shopLists = [];
        this.shopListArray = [];
        this.grantTotal={};
        this.shippingAddress={};
        this.orderStatus={};
        this.Init();  
    }
 
    public Init() {
           
       this.orderStatus={"orderStatus":"failure"};
          
        // this.checkoutService.CheckPaymentStatus(this.orderStatus).then(
        //     response => {
        //         this.paymentStatusInfo=JSON.parse(response);
        //         //console.log("Payment Status  : ",this.paymentStatusInfo)
        //         this.products=this.paymentStatusInfo.Items;
        //         //console.log("Product length  : ",this.paymentStatusInfo.Items.length)
        //         this.grantTotal=this.paymentStatusInfo.Totals;
        //         //console.log("Grant Total : " ,this.grantTotal)
        //         this.shippingAddress=this.paymentStatusInfo.ShippingAddress;
        //         //console.log("Shipping Address : ",this.shippingAddress);
        //         this.Name=this.shippingAddress.FirstName + " " +this.shippingAddress.LastName;
        //         this.Line1=this.shippingAddress.Line1;
        //         this.Line2=this.shippingAddress.Line2;
        //         this.City=this.shippingAddress.City;
        //         this.State=this.shippingAddress.State;
        //         this.Country=this.shippingAddress.Country;
        //         this.totalCost=this.grantTotal.TotalCost;
        //         this.productCost=this.grantTotal.ProductCost;
        //         this.couponDiscount=this.grantTotal.CouponDiscount;
        //         this.shippingCost=this.grantTotal.ShippingCost;
        //         this.products.forEach((tempProduct: any) => {
        //           tempProduct.productEdit = false;
        //           if(!this.shopListArray[tempProduct.Shop.Name]){
        //             this.shopLists.push(tempProduct.Shop.Name);
        //             this.shopListArray[tempProduct.Shop.Name] = [];
        //           }
        //           this.shopListArray[tempProduct.Shop.Name].push(tempProduct);
        //       });
              //console.log("this.products", this.products);
              //console.log("Shop List:", this.shopLists);
              //console.log("Shop List Array:", this.shopListArray);
               
            // }, error => this.errorMessage = <any>error);
    }
    redirectToCheckout()
    {
       this.router.navigate(['/user/cart']); 
        }
      
}