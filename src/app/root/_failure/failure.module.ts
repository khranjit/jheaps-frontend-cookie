import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FailureComponent } from './failure.component';
import { SharedModule } from '../../shared/shared.module';

  
const failure: Routes = [
    {path: '',component:FailureComponent}
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(failure)
  ],
  declarations: [
    FailureComponent
  ],
})
export class FailureModule { }