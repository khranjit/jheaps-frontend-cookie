import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { SearchComponent } from './search.component';

import { RouterModule, Routes } from '@angular/router';

const shop:Routes = [
  { path: ':key/:catid', component: SearchComponent },
  { path: ':key/:catid/:subid', component: SearchComponent },
  { path: ':term', component: SearchComponent }
]

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(shop)
  ],
  declarations: [ 
    SearchComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class SearchModule { }