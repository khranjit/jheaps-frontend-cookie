//import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { ElasticHelperService } from '../../services/elastic-helper.service';
import { ElasticService } from '../../services/elastic.service';
import { AttributeService } from '../../services/attribute.service';
import { Component, HostListener, AfterViewInit, Injector } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { AddToCartService, TempCartService } from '../../services/cart.service';
import { HomeService} from '../../services/home.service';
import { LoginRedirectService } from '../../services/login.service';
import { ToasterPopService } from '../../services/toasterpop.service';
import { ProductDescriptionService } from '../../services/product-description.service';

declare var jQuery: any;

@Component({
    
    selector: 'search',
    providers: [ProductService, ProductDescriptionService,ElasticHelperService, ElasticService, AttributeService, AddToCartService, HomeService, ToasterPopService],
    templateUrl: './search.component.html',
    //directives : [Ng4LoadingSpinnerComponent]
}) 

export class SearchComponent {
    @HostListener("window:scroll", ["$event"])
    onWindowScroll() {
    let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    let max = document.documentElement.scrollHeight;
    var percentage = Math.round((pos/max) * 100) / 100;
    //console.log("percentage "+percentage)
     if(percentage  > 0.75 )   {
         this.callscroll();
     }
    }
    defaultSearch:any;
    app: any;
    toCart: any = {}
    getUserToken: any;
    loadtrigger = true;
    public Sections: any;
    public SectionsList: any;
    public Categories:any;
    public CategoryIds:any;
    public CategoriesList: any;
    public Products: any = [];
    public ProductsList: any;
    public productCurrency: any = {value: "USD"};   
    public errorMessage: any;
    public SelectedSection: string;
    public SelectedCategory:string;
    public SearchTerm: string;
    public sort_type = 'latest';
    public SelectedAttributes = <any>{}; 
    adminCriteria = <any>{};
    public Attributes: any;
    public AttributesList: any;
    totalPages = 0;
    currentPage = 1;
    from = 0;
    size = 12;
    total = 0;
    public pages: any[];
    filterTags: any;
    loadLoginDirectives = true;
    loader = false;
    pageLoader = true;
    public firstItem:boolean = false;
    constructor(private route: ActivatedRoute, private router: Router, private loadingservice:NgxSpinnerService,
        private productshop: ProductService, private ESHelperService:ElasticHelperService, private loginRedirectService:LoginRedirectService,
        private ESService: ElasticService,private productDescriptionService: ProductDescriptionService,private tempCart: TempCartService,private Inj: Injector, private AttributeService: AttributeService,private addToCartService: AddToCartService, private homeService: HomeService, private toasterPopService: ToasterPopService) {
        this.ResetPagination();
        this.getDistinctCategoriesAndCount();
        this.getAttributesList();
        this.adminCriteria.Criteria = [];
        this.adminCriteria.Criteria.Price = []; 
        this.adminCriteria.Criteria.Price.From = 0;
        this.adminCriteria.Criteria.Price.To = 30000;
        this.sort_type = 'latest';
        this.initScrollSpy();
        this.route.params.subscribe(params => {
                this.adminCriteria.firstLoad = true;
                if(params['key'])
                {
                    var obj = {
                        Id:'',
                        subCategory:''
                       }
                    obj.Id = params['catid'];
                    if(params['subid'])
                    {
                        obj.subCategory = params['subid'];
                    }
                    this.adminCriteria.Criteria.category = obj;
                    this.SearchTerm = params['key'];
                }
                else
                {
                    this.SearchTerm = params['term'];
                    this.adminCriteria.Criteria.Search = params['term']; 
                }
                this.adminCriteria.Criteria.MandatoryFields = [];
                this.adminCriteria.Criteria.MandatoryFields.push("Search");
                this.adminCriteria.Criteria.Sort = this.sort_type;
                this.defaultSearch = "Search";
                this.adminCriteria.Criteria.MandatoryFields = [];
                this.adminCriteria.Criteria.MandatoryFields.push("Search");
                this.gerDataFromElastic();
                this.loginRedirectService.events$.forEach(event => 
                    {
                        this.loadLoginDirectives = false;
                         setTimeout(()=>{
                           this.loadLoginDirectives = true;
                         },1000)
                         
                    });
        }
        );
    }

ngAfterViewInit()
{
    this.loginRedirectService.events$.forEach(events=>{
        
    })
}
LoadOnScroll(){
    var scrollContainer = document.getElementById('scroll-container');
    var scrollBox = document.getElementById('scroll-box');
    var scrollContainerPos = scrollContainer.getBoundingClientRect();
    var scrollBoxPos = scrollBox.getBoundingClientRect();
    var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);
        
    if(scrollLength - scrollContainer.scrollTop < 1) 
        
    {
            setTimeout(()=>{
             this.gerDataFromElasticOnScroll();
            },500)
        
        
    }
    
 }

 ResetPagination()
 {
     this.from = 0;
     this.total = 0;
     jQuery('#scroll-container').css("height",500);
 }

 ValidateScrollContainer()
    {
      setTimeout(()=>{
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        if(scrollContainer!=null && scrollBox!=null){
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        if(scrollBoxPos.height < scrollContainerPos.height)
        {
            jQuery('#scroll-container').css("height",scrollBoxPos.height);
        }
    }
      },1000)
    }
    public checkColorSelected(name, value){
        if(!this.SelectedAttributes[name]){
            return ''; 
        }
        if(this.SelectedAttributes[name].indexOf(value.toLowerCase()) > -1){
            return 'selected';
        }
        return '';    
    }
    
    public attributeFiltering(name, attributeValue){  
        let fieldName = "Attributes-"+name;
        attributeValue = attributeValue.toLowerCase()
        this.validateCriteria(name);
        if(!this.SelectedAttributes[name]){
            this.SelectedAttributes[name] = [];
        } 
        if(this.SelectedAttributes[name].indexOf(attributeValue) > -1){
            this.SelectedAttributes[name].splice(this.SelectedAttributes[name].indexOf(attributeValue), 1);
        }else{
            this.SelectedAttributes[name].push(attributeValue);
        }
        if(this.adminCriteria.Criteria.MandatoryFields.indexOf(fieldName) == -1){
            this.adminCriteria.Criteria.MandatoryFields.push(fieldName);
        }
        if(this.SelectedAttributes[name].length == 0){
            this.adminCriteria.Criteria.MandatoryFields.splice(this.adminCriteria.Criteria.MandatoryFields.indexOf(fieldName), 1);
            delete this.adminCriteria.Criteria[name]; 
        }
        this.adminCriteria = this.ESHelperService.formJsonLikeAdminCriteria(this.adminCriteria, name, this.SelectedAttributes[name]);
        this.getDataFromQuery(fieldName);
    }
    
    public validateCriteria(name){
        if(!this.adminCriteria.Criteria.MandatoryFields){
            this.adminCriteria.Criteria.MandatoryFields = [];
        }
        this.adminCriteria.Criteria.Sort = this.sort_type;
    }
    
    public getDataFromQuery(name) {

        this.validateCriteria(name);
        if (this.adminCriteria.Criteria.MandatoryFields.indexOf(name) == -1) {
            this.adminCriteria.Criteria.MandatoryFields.push(name);
        }
        this.updateFilterTags();;
        this.gerDataFromElastic();
    }
    
    public updateFilterTags(){
        this.filterTags = []; 
        this.adminCriteria.Criteria.MandatoryFields.forEach(field =>{           
            if(field != 'Shop' && field != 'Categories' && field != 'ShopSection' && field != 'Price' && field != 'Sort' && field != 'Search'){
                this.adminCriteria.Criteria[field.split("-")[1]].forEach(value =>{                     
                    if(field.split("-")[1] == "IsNew"){
                        let isnew = value == "true" ? "New" : "Used";
                        let obj = {"filter": field,"value": isnew};
                        this.filterTags.push(obj);        
                    }else{
                        let obj = {"filter": field,"value": value};
                        this.filterTags.push(obj);   
                    }                    
                });
            }else{                
                if(field == 'ShopSection'){
                    let obj = {"filter": field,"value": this.adminCriteria.Criteria[field]};
                    this.filterTags.push(obj);
                }else if(field == 'Sort'){
                    //this.filterTags.push(this.adminCriteria.Criteria[field]);
                }else if(field == 'Categories'){
                    let obj = {"filter": field,"value": this.adminCriteria.Criteria[field]};
                    this.filterTags.push(obj);
                }
                else if(field == 'Shop'){
                    //this.filterTags.push(this.adminCriteria.Criteria[field]);
                }
                else if(field == 'Search'){
                    //this.filterTags.push(this.adminCriteria.Criteria[field]);
                }
                else if(field == 'Price'){
                    let obj = {"filter": field,"value": "Price "+ this.adminCriteria.Criteria[field].From +' - '+ this.adminCriteria.Criteria[field].To};
                    this.filterTags.push(obj);
                } 
                else{
                    this.adminCriteria.Criteria[field].forEach(value =>{       
                        let obj = {"filter": field,"value": value};                  
                        this.filterTags.push(obj);                        
                    });     
                }
            }                                   
        });
        
    }
    
    public removeFilterTags(tag, index) {
        if (this.filterTags[index].filter != "Price" && this.filterTags[index].filter != "ShopSection" && this.filterTags[index].filter != "Categories") {
            let element = this.filterTags[index].filter.split("-")[1].split(' ').join('_') + "-" + this.filterTags[index].value;
            jQuery("#" + element.toLowerCase()).attr('checked', false);
            this.filterTags.splice(index, 1);
            this.attributeFiltering(tag.filter.split("-")[1], tag.value);
        }
        else {

            this.filterTags.splice(index, 1);
            var index = this.adminCriteria.Criteria.MandatoryFields.indexOf(tag.filter);
            this.adminCriteria.Criteria.MandatoryFields.splice(index, 1);
            this.gerDataFromElastic();

        }
    }

    public gerDataFromElastic() {
        this.ResetPagination();
        let query;
        query = this.ESHelperService.getQueryForShopCriteria(this.adminCriteria);
        this.loadingservice.show();
        this.pageLoader = true;
        this.ESService.getRecordsByQuery(query, this.from, this.size)
            .then(
                product => {
                    this.ProductsList = JSON.parse(product);
                    this.Products = this.ProductsList.hits.hits;
                    this.total = JSON.parse(product).hits.total;
                    this.pageLoader = false;
                    this.firstItem=true;
                    this.loadingservice.hide();
                    this.ValidateScrollContainer();
                },
                error => this.errorMessage = <any>error);
    }
    async callscroll() {
        if (this.firstItem) {
            await  this.gerDataFromElasticOnScroll();
        }
    }
    public gerDataFromElasticOnScroll() {
        this.from += this.size;
        if (this.from < this.total && this.loadtrigger) {
            this.loadtrigger = false;
            console.log("Loading");
            let query = this.ESHelperService.getQueryForShopCriteria(this.adminCriteria);
            // this.loadingservice.show();
            this.loader= true;
            this.ESService.getRecordsByQuery(query, this.from, this.size)
                .then(
                    product => {
                        this.ProductsList = JSON.parse(product);
                        this.total = this.ProductsList.hits.total;
                        // this.loadingservice.hide();
                        this.loader= false;
                        this.Products = this.Products.concat(this.ProductsList.hits.hits);
                        this.loadtrigger=true;
                    },
                    error => this.errorMessage = <any>error);
        }

    }

    public productInfo(productId) { 
        this.router.navigate(['/user/product', productId]);  
    }
    
    public initScrollSpy(){
        window.onscroll = () => {
             /*let status = "not reached";
             let body = document.getElementById("product-container");
             if(body){
                 let scrollHeight = jQuery(document).scrollTop();
                    //console.log(scrollHeight , (body.scrollHeight + jQuery("#product-container").offset().top))
                 if (scrollHeight > (body.scrollHeight + jQuery("#product-container").offset().top - 200)) {             
                    //console.log(this.from, this.total);
                    if((this.from + this.size) < this.total){
                        //console.log(this.from, this.size)
                        //this.from = this.from + this.size;
                        this.gerDataFromElasticOnScroll(this.from, this.size);  
                    }
                 }
             }*/
        };   
    }

    getDistinctCategoriesAndCount() {
        this.AttributeService.getcategoriesbasedbyGlobal().then(categories=>{
            console.log(categories)
            this.Categories = JSON.parse(categories);
            // this.Categories = this.CategoriesList.aggregations.langs.buckets;
        })
        // this.productshop.getDistinctCategoriesAndCount()
        //     .then(
        //     product => {
        //         this.CategoriesList = JSON.parse(product);
        //         this.Categories = this.CategoriesList.aggregations.langs.buckets;
        //         this.CategoryIds = this.CategoriesList.aggregations.categoryId.buckets;
        //     },
        //     error => this.errorMessage = <any>error);
    }

    getProductsByCategoryId(CategoryId,index) {
        this.SearchTerm = CategoryId;
        this.adminCriteria.firstLoad = false;
        this.Attributes = [];
        // var CatId = this.CategoryIds[index].key;
        var model={
            CategoryId:index,
        }
        this.AttributeService.fetchAttributesbyGlobal(model).then
        (result => {
            var results = JSON.parse(result);
            results.forEach(attribute => {
                attribute.selectedAttribute = [];
                var newArr =[];
                attribute.Values.forEach(element=>{
                    newArr.push(element.Value);
                })
                attribute.Values = newArr;
                this.Attributes.push(attribute);
            })
        });
        this.SelectedCategory = CategoryId;
        if (this.adminCriteria.Criteria.MandatoryFields.indexOf("Categories") == -1) {
            this.adminCriteria.Criteria.MandatoryFields.push("Categories");
        }
        this.adminCriteria.Criteria.Categories = this.SelectedCategory;
        this.updateFilterTags();
        this.gerDataFromElastic();
    }
    
    public getAttributesList(){
        this.AttributeService.getAll()
            .then(
            attributes => {
                this.AttributesList = JSON.parse(attributes);
                this.Attributes = this.AttributesList.Items;
                this.Attributes=[];
            },
            error => this.errorMessage = <any>error);
    }
    
    getProducts() {
        let page = 0;
        console.log('get product in search');
        this.productshop.getAll(page)
            .then(
            product => {
                this.ProductsList = JSON.parse(product);
                this.Products = this.ProductsList.Items;

                console.log("produvcts"+this.Products);
            },
            error => this.errorMessage = <any>error);
    }
}