import { Component} from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { ForumService } from '../../../services/forum.service';

@Component({
    
    selector: 'admin-status',
    templateUrl: './admin-status.component.html',
    providers: [UserService, ForumService]
})

export class AdminStatusComponent {
    errorMessage: any;
    selectedGroupId: any;
    getAllstatus=[];
    getStatusWiseView=[];
    getUserToken : any;
    setStatus : any;
    adminStatus : any;
    ShowUserProfile : any;
    ShowUserName : any;
    ShowUserId : any;
    ShowUserRole : any;
    totalPages: number;
    pageNumber: number;
    currentPage : number;
    constructor(private route: ActivatedRoute, private router: Router, private userService: UserService, private forumService: ForumService) {
        //this.getMyMarkedThreads = [];
        this.totalPages = 0;
        this.pageNumber = 0;
        this.Init();
    }

    public Init() {
         window.scrollTo(0,20); 
          this.forumService.viewAllStatus(this.pageNumber).then(
            response => {
                //console.log('getGroupNameList response',response);
                this.totalPages =JSON.parse(response).TotalPages;
                this.currentPage =JSON.parse(response).CurrentPage;
                this.getAllstatus=JSON.parse(response).Items;
                console.log('this.getStatusWiseView response',this.getAllstatus);
               // this.ShowStatus=true;
               
            },
            error => this.errorMessage = <any>error);  
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response);
                this.ShowUserName = this.ShowUserProfile.Items[0].UserName;
                this.ShowUserRole=this.ShowUserProfile.Items[0].Roles;
                //console.log("ShowUserRole",this.ShowUserRole);
              //  console.log("ShowUserId",this.ShowUserProfile.Items[0]._id);
                this.ShowUserId=this.ShowUserProfile.Items[0]._id;
             }, error => this.errorMessage = <any>error);  
    }
    
    ViewThread(GroupId,PostId){
     //   console.log("Id :",GroupId)
        let navimodel: NavigationExtras = {
            queryParams: { 'groupId': GroupId,'postId':PostId }
        }
       // console.log("navimodel 1:",navimodel)
        this.router.navigate(['/user/forum/list/thread-view'], navimodel);
    } 
    
    status(value){
       // console.log("Event",event);
        this.setStatus=value;
    }
    
    adminStatusfunc(value){
    this.adminStatus=value;
    }
    
    statusWise(){
        if(this.adminStatus!='All'){
        let model={
    "status" : this.adminStatus
                }
            this.pageNumber=0;
            this.totalPages=0;
       this.forumService.StateWiseView(this.pageNumber,model).then(
            response => {
                //console.log('getGroupNameList response',response);
                this.getAllstatus=JSON.parse(response).Items;
                this.totalPages=JSON.parse(response).TotalPages;
         //       console.log('this.getStatusWiseView response',this.getAllstatus);
               // this.ShowStatus=true;
               
            },
            error => this.errorMessage = <any>error);
        }
        else{
            this.Init();  
        }
    }
            
      
    pageSelectionNext() {

        this.totalPages = this.totalPages;
        this.pageNumber = this.pageNumber + 1;
       // console.log("Current Page Number :" + this.pageNumber);
        this.Init();

    }
    
    pageSelectionPrev() {


        this.pageNumber = this.pageNumber - 1;
       // console.log("Current Page Number :" + this.pageNumber);
        if(this.adminStatus!='All')
        this.statusWise();
        else
        this.Init();

    }
    
}