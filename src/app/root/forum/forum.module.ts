import { NgModule, Component } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { ForumComponent } from './forum.component';
import { ThreadViewComponent } from './_thread-view/thread-view.component';
import { PostThreadComponent } from './_post-thread/post-thread.component';
import { GroupViewComponent } from './_group-view/group-view.component';
import { ForumIndexComponent } from './_forum-index/forum-index.component';
import { AdminStatusComponent } from './_admin-status-view/admin-status.component';
import { Ng2CompleterModule } from "ng2-completer";
import {DropdownModule} from 'primeng/dropdown';
import {ButtonModule} from 'primeng/button';
import { RouterModule, Routes } from '@angular/router';
import { MatInputModule } from '@angular/material';


const shop:Routes = [
  { path: 'list', component: ForumComponent,children:[
    { path:'', redirectTo:'forum-index',pathMatch:'full'},
    { path:'forum-index', component: ForumIndexComponent},
    { path:'your-thread', loadChildren:'./_your-thread/your-thread.module#YourThreadModule'},
    { path:'thread-view', component: ThreadViewComponent},
    { path:'group-view', component: GroupViewComponent},
    { path:'admin-status', component: AdminStatusComponent},
    { path:'post-thread', component: PostThreadComponent}
  ]}
]

@NgModule({
    imports: [
        SharedModule,
        Ng2CompleterModule,
        DropdownModule,
        ButtonModule,
        MatInputModule,
        RouterModule.forChild(shop)
    ],
    declarations: [
        ForumComponent,
        ThreadViewComponent,
        PostThreadComponent,
        GroupViewComponent,
        ForumIndexComponent,
        AdminStatusComponent,
         ]
     
})
export class ForumModule { }