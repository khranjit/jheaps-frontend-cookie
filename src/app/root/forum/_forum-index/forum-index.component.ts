import { Component} from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { ForumService } from '../../../services/forum.service';

declare var jQuery: any;

@Component({
    
    selector: 'forum-index',
    templateUrl: './forum-index.component.html',
    providers: [UserService, ForumService]
})

export class ForumIndexComponent {
    errorMessage: any;
    getUserToken : any;
    ShowUserProfile : any;
    ShowUserName : any;
    ShowUserId : any;
    ShowUserRole : any;
    getGroupAllList =[];
    
    constructor(private route: ActivatedRoute, private router: Router, private userService: UserService, private forumService: ForumService) {
        this.Init();
    }

    public Init() {
          window.scrollTo(0,20); 
        this.getUserToken=localStorage.getItem('userToken');
        //console.log("this.getUserToken",this.getUserToken);
        if(this.getUserToken==null){
             this.forumService.viewAllForumGroupPost().then(
            response => {
                //console.log('getGroupAll response',response);
                this.getGroupAllList=JSON.parse(response).Items;
                
                
            },
            error => this.errorMessage = <any>error);
        }
        else{
           this.forumService.viewAllForumGroupPostUserBased().then(
            response => {
                // console.log('getGroupAll response',response);
                this.getGroupAllList=JSON.parse(response).Items;
                
            },
            error => this.errorMessage = <any>error);
        }
        
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response);
                this.ShowUserName = this.ShowUserProfile.Items[0].UserName;
                this.ShowUserRole=this.ShowUserProfile.Items[0].Roles;
      //          console.log("ShowUserRole",this.ShowUserRole);
        //        console.log("ShowUserId",this.ShowUserProfile.Items[0]._id);
                this.ShowUserId=this.ShowUserProfile.Items[0]._id;
             }, error => this.errorMessage = <any>error);
      //  console.log(this.timeSince('2017-09-16T09:07:33.215Z'));
       // console.log(this.timeSince(1505916214000))
       }
       DateChange(date)
       {
        var CreatedDate:any = new Date(date);
        var today:any = new Date();
        var seconds = Math.floor((today - CreatedDate) / 1000);

        var interval = Math.floor(seconds / 31536000);
      
        // if (interval > 1) {
        //   return interval + " years";
        // }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
        //   return interval + " months";
            return CreatedDate.toDateString("dd MMM yyyy");
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
          return interval + " days ago";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
          return interval + " hours ago";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
          return interval + " minutes ago";
        }
        return Math.floor(seconds) + " seconds ago";
       }
    //   timeSince(date) {
    //         var now = new Date().getTime();
    //       var senddate=new Date(date).getTime()
    //      var seconds = Math.floor((now - senddate) / 1000);
        
    //      var interval = Math.floor(seconds / 31536000);
        
    //      if (interval > 1) {
    //        return interval + " years";
    //      }
    //      interval = Math.floor(seconds / 2592000);
    //      if (interval > 1) {
    //        return interval + " months";
    //      }
    //      interval = Math.floor(seconds / 86400);
    //      if (interval > 1) {
    //        return interval + " days";
    //      }
    //      interval = Math.floor(seconds / 3600);
    //      if (interval > 1) {
    //        return interval + " hours";
    //      }
    //      interval = Math.floor(seconds / 60);
    //      if (interval > 1) {
    //        return interval + " minutes";
    //      }
    //      return Math.floor(seconds) + " seconds";
    //     }


    /*   checkCancelHours(CreatedDate) {
       var now = new Date().getTime();
        var ThirtyMinutes = new Date(CreatedDate).getTime();
        var minutes = Math.floor((now - ThirtyMinutes) / 60 * 60000);
        var hrs=minutes/60;
        var min=minutes%60;
        if(hrs<23){
            let updateTime=hrs+" hours "+min+" minutes ";
            return updateTime;
        }
        else{
             var Datefuns = new Date(CreatedDate);
            return Datefuns;       
        }
          
        var startTime = "09:00:00";
        var endTime = "10:00:00";
        
        var startDate = new Date().getTime();
        var endDate = new Date(CreatedDate).getTime();
        var timeDiff = Math.abs(startDate - endDate);
        
        var hh = Math.floor(timeDiff / 1000 / 60 / 60);
        if(hh < 10) {
            hh += 0 ;
        }
        timeDiff -= hh * 1000 * 60 * 60;
        var mm = Math.floor(timeDiff / 1000 / 60);
        if(mm < 10) {
            mm = 0 + mm;
        }
        timeDiff -= mm * 1000 * 60;
        var ss = Math.floor(timeDiff / 1000);
        if(ss < 10) {
            ss = 0 + ss;
        }
        
        return  hh + ":" + mm ;
     }*/
 
    postThread(GroupId){
        this.getUserToken=localStorage.getItem('userToken');
        if(this.getUserToken==null){
             jQuery("#loginmodal").modal('show');    
        }
        else{
        //    console.log("Id :",GroupId)
            let navimodel: NavigationExtras = {
                queryParams: { 'groupId': GroupId }
            }
          //  console.log("navimodel 1:",navimodel)
            this.router.navigate(['/user/forum/list/post-thread'], navimodel);
        }
    }   
    
     ViewGroup(Id){
       //console.log("Id :",Id)
        let navimodel: NavigationExtras = {
            queryParams: { 'groupId': Id }
        }
      //  console.log("navimodel 1:",navimodel)
        this.router.navigate(['/user/forum/list/group-view'], navimodel);
    }
        
    ViewThread(GroupId,PostId){
      //  console.log("Id :",GroupId)
        let navimodel: NavigationExtras = {
            queryParams: { 'groupId': GroupId,'postId':PostId }
        }
      //  console.log("navimodel 1:",navimodel)
        this.router.navigate(['/user/forum/list/thread-view'], navimodel);
    } 
}

