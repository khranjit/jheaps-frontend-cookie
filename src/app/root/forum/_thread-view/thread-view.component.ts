import { Component} from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { ForumService } from '../../../services/forum.service';
import { NotifyService } from '../../../services/notify.service';

declare var jQuery: any;

@Component({
    
    selector: 'thread-view',
    templateUrl: './thread-view.component.html',
providers: [UserService, ForumService, NotifyService]
})

 export class ThreadViewComponent {
    public forumGroupModel: ViewModels.IForumGroupTitleViewModel;
    public responseModel: ViewModels.IResponseViewModel;
    public replyModel: ViewModels.IReplyViewModel;
    errorMessage : any;
    selectedGroupId : any;
    selectedPostId : any;
    // selectedForum : any;
    getSeletedGroup =[];
    updateButton: boolean;
    addComment: boolean;
    addReply: boolean;
    ViewOwnThreadsAll: boolean;
    ShowStatus: boolean;
    View: boolean;
    yourThreadView: boolean;
    statusView: boolean;
    searchView: boolean;
    groupId: boolean;
    getGroupNameList = [];
    getGroupAllList = [];
    getSelectedGroup = [];
    getOwnThreads = [];
    getMyMarkedThreads = [];
    getStatusWiseView = [];
    getAllstatus = [];
    GroupforEdit = {};
    deleteGroup = {};
    selectedForum = {};
    selectedResponseId = "";
    selectedReplyId = "";
   // comment = ""
    getUserToken: string;
    setStatus: string;
    adminStatus: string;
    searchQuery: string;
    ShowUserProfile : any;
    ShowUserName : any;
    ShowUserId : any;
    ShowUserRole : any;
    totalPages: number;
    pageNumber: number;
    startOffset : number;
    endOffset : number;
    getTotalResponse=[];
    selectedResponseDisplay: any;
    token: any;
    
    constructor(private route: ActivatedRoute, private router: Router, private userService: UserService, private forumService: ForumService,
        private toaster: NotifyService) {
        this.forumGroupModel = <ViewModels.IForumGroupTitleViewModel>{};
        this.responseModel = <ViewModels.IResponseViewModel>{};
        this.replyModel = <ViewModels.IReplyViewModel>{};
        this.totalPages = 0;
        this.pageNumber = 0;
        this.startOffset=0;
        this.endOffset=0;
        this.selectedResponseDisplay = [];
        this.init();
    }
    
        public init() {
        window.scrollTo(0,20); 
        this.getSeletedGroup=[];
        this.getUserToken=localStorage.getItem('userToken');
        this.route.queryParams.subscribe(
            params => {
              //  console.log(params)
                this.selectedGroupId = params['groupId'];
                this.selectedPostId = params['postId']
                let model = { "groupId": this.selectedGroupId, "postId": this.selectedPostId };
                 if (this.getUserToken == null) {
                this.forumService.viewSelectedPostUnAuth(model).then(
                    response => {
                        this.selectedResponseDisplay=[];
                        this.getSeletedGroup.push(JSON.parse(response));
                        if(this.getSeletedGroup[0].Status==""){
                            this.setStatus="select";
                        }
                        else{
                            this.setStatus=this.getSeletedGroup[0].Status
                        }
                        this.getTotalResponse=JSON.parse(response).Responses;
                        this.totalPages=1 + Math.floor( this.getTotalResponse.length / 10);      
                        this.startOffset=this.pageNumber * 10;
                        this.endOffset= this.startOffset + 9 ;
                        if(this.getTotalResponse.length > this.endOffset)
                            this.endOffset= this.endOffset + 0 ;
                        else
                            this.endOffset=this.getTotalResponse.length - 1;
                        for(var i=this.startOffset; i<=this.endOffset;i++){
                             this.selectedResponseDisplay.push(this.getTotalResponse[i]);
                            }     
                    },
                    error => this.errorMessage = <any>error);
                }
                else{
                      this.forumService.viewSelectedPostAuth(model).then(
                    response => {
                        this.selectedResponseDisplay=[];
                        //console.log('getSeletedGroup response',response);
                        this.getSeletedGroup.push(JSON.parse(response));
                     //   console.log('getSeletedGroupAuth', this.getSeletedGroup);
                        if(this.getSeletedGroup[0].Status==""){
                      //      console.log("IF",this.getSeletedGroup[0].Status);
                            this.setStatus="select";
                        }
                        else{
                       //      console.log("ELSE",this.getSeletedGroup[0].Status);
                            this.setStatus=this.getSeletedGroup[0].Status
                        }
                        this.getTotalResponse=JSON.parse(response).Responses;
                     //   console.log("getTotalResponse",this.getTotalResponse.length);
                        this.totalPages=1 + Math.floor( this.getTotalResponse.length / 10);      
                       // console.log( " this.totalPagessssssssssss", this.totalPages);
                        this.startOffset=this.pageNumber * 10;
                        this.endOffset= this.startOffset + 9 ;
                        if(this.getTotalResponse.length > this.endOffset)
                            this.endOffset= this.endOffset + 0 ;
                        else
                            this.endOffset=this.getTotalResponse.length - 1;
                        for(var i=this.startOffset; i<=this.endOffset;i++){
                             this.selectedResponseDisplay.push(this.getTotalResponse[i]);
                            }     
                    },
                    error => this.errorMessage = <any>error);
                 }
            });
             this.getUserProfile();
           
    }
    
    getUserProfile(){
       this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response);
                this.ShowUserName = this.ShowUserProfile.Items[0].UserName;
                this.ShowUserRole=this.ShowUserProfile.Items[0].Roles;
             //   console.log("ShowUserRole",this.ShowUserRole);
               // console.log("ShowUserId",this.ShowUserProfile.Items[0]._id);
                this.ShowUserId=this.ShowUserProfile.Items[0]._id;
             }, error => this.errorMessage = <any>error); 
    }
    
    status(value){
  //      console.log("Event", event);
        this.setStatus = value;
    }
    
    setAdminStatus(){
        let model = {
            "groupId": this.selectedGroupId,
            "postId": this.selectedPostId,
            "Status": this.setStatus
        }
        this.forumService.setStatusAdmin(model).then(
            response => {
    //            console.log('status response', response);
                this.init();
    
            },
            error => this.errorMessage = <any>error);
    }
    
    sendResponse(){
        //console.log( this.responseModel);    
        let model = {
            "groupId": this.selectedGroupId,
            "postId": this.selectedPostId,
            "Response": this.responseModel
        };
      //  console.log(model);
        this.forumService.postForumResponse(model).then(
            response => {
                //console.log('response response',response);
                this.init();
                this.responseModel = <any>{};
                this.toaster.SuccessSnack( "You Posted Response Successfully");
                jQuery('#responseForm')[0].reset();
            },
            error => this.errorMessage = <any>error);
    }
    
    sendComment(id){
        let model = {
            "groupId": this.selectedGroupId,
            "postId": this.selectedPostId,
            "responseId": id,
            "Reply": {
                "Reply": this.replyModel.Reply
            }
        }
        //console.log(model);
        this.forumService.postForumResponseComment(model).then(
            response => {
                //console.log('response response',response);
                this.init();
                //this.comment = "";
                this.toaster.SuccessSnack( "You Posted Reply Successfully");
                jQuery('#CommentForm')[0].reset();
            },
            error => this.errorMessage = <any>error);
    }
    
    
    confirmDelete(group){
        this.deleteGroup = group;
        jQuery("#confirmDeleteModal").modal('show');
    }
    
   /*deletefunc(){
        //console.log('this.deleteGroup',this.deleteGroup);
        this.forumService.deleteForumGroup(this.deleteGroup).then(
            response => {
                //console.log('response',response);
                this.init();
                this.forumGroupModel = <any>{};
                jQuery("#confirmDeleteModal").modal('hide');
            },
            error => this.errorMessage = <any>error);
    }*/
    
    mark(){
        this.getUserToken=localStorage.getItem('userToken');
         if (this.getUserToken == null) {
            jQuery("#loginmodal").modal('show');    
        }
        else{
        let post = { "groupId": this.selectedGroupId, "postId": this.selectedPostId }
     //   console.log("Mark Post", post);
        this.forumService.markThreads(post).then(
            response => {
       //         console.log('markThreads response', response);
                this.init();
            },
            error => this.errorMessage = <any>error);
        }
    }
    
    pin(){
        let post = { "groupId": this.selectedGroupId, "postId": this.selectedPostId }
      //  console.log("Pin Post", post);
        this.forumService.pinThreads(post).then(
            response => {
        //        console.log('Pin response', response);
                this.init();
            },
            error => this.errorMessage = <any>error);
    }
    
    like(){
        this.getUserToken=localStorage.getItem('userToken');
         if (this.getUserToken == null) {
            jQuery("#loginmodal").modal('show');    
        }
        else{
        let post = { "groupId": this.selectedGroupId, "postId": this.selectedPostId }
       // console.log("Like Post", post);
        this.forumService.likeThread(post).then(
            response => {
         //       console.log('likeThreads response', response);
                this.init();
            },
            error => this.errorMessage = <any>error);
        }
    }
    
    openclose(){     
        let post = { "groupId": this.selectedGroupId, "postId": this.selectedPostId }
       // console.log("Open / close Post", post);
         this.forumService.setAdminThreadStatus(post).then(
            response => {
         //       console.log('Open / Close response', response);
                this.init();
    
            },
            error => this.errorMessage = <any>error);    
    }
    
    login(){
        jQuery("#loginmodal").modal('show');
        
       this.token = setInterval(() => {
        this.getTokenInit(); 
      });
     }
    
    getTokenInit(){
       this.getUserToken=localStorage.getItem('userToken');
       if(this.getUserToken==null){
            this.getUserToken=localStorage.getItem('userToken');
       }
       else{
          clearInterval(this.token);
          this.getUserProfile();
      }
         
    }
    
    ConfirmDeletePost(){
        jQuery("#deletePostModal").modal('show');
    }
    ConfirmDeleteResponse(responseId){
        this.selectedResponseId = responseId;
        jQuery("#deleteResponseModal").modal('show');
    }
    ConfirmDeleteReply(responseId, replyId){
        this.selectedResponseId = responseId;
        this.selectedReplyId = replyId;
        jQuery("#deleteReplyModal").modal('show');
    }
    DeletePost(){
        let model = {
            "groupId": this.selectedGroupId,
            "postId": this.selectedPostId
        }
       // console.log(model);
        /*this.forumService.DeleteThread(this.selectedGroupId,this.selectedPostId).then(
            response => {
                console.log('Delete Threads response',response);
                this.init();
               
            },
            error => this.errorMessage = <any>error); */
        this.forumService.DeleteThread(this.selectedGroupId, this.selectedPostId).then(
            response => {
         //       console.log('Delete Threads response', response);
                //this.init();
                let navimodel: NavigationExtras = {
                    queryParams: { 'groupId': this.selectedGroupId }
                }
           //     console.log("navimodel 1:", navimodel)
                this.router.navigate(['/user/forum/list/group-view'], navimodel);
                this.toaster.SuccessSnack( "Thread Successfully Deleted");
                jQuery("#deletePostModal").modal('hide');
            },
            error => this.errorMessage = <any>error);
    }
    DeleteResponse(){
        let model = {
            "groupId": this.selectedGroupId,
            "postId": this.selectedPostId,
    
        }
        this.forumService.DeleteResponse(this.selectedGroupId, this.selectedPostId, this.selectedResponseId).then(
            response => {
            //    console.log('Delete Response response', response);
                this.init();
                this.toaster.SuccessSnack( "Response Successfully Deleted");
                jQuery("#deleteResponseModal").modal('hide');
            },
            error => this.errorMessage = <any>error);
    }
    DeleteReply(){
        let model = {
            "groupId": this.selectedGroupId,
            "postId": this.selectedPostId
        }
        this.forumService.DeleteReply(this.selectedGroupId, this.selectedPostId, this.selectedResponseId, this.selectedReplyId).then(
            response => {
              //  console.log('Delete Reply response', response);
                this.toaster.SuccessSnack("Reply Successfully Deleted");
                this.init();
                jQuery("#deleteReplyModal").modal('hide');
            },
            error => this.errorMessage = <any>error);
    }
    
    windowTop(){
        window.scrollTo(0, 20);
    }
    
     pageSelectionNext() {
      //  console.log("Next Page");
        this. windowTop();
        this.selectedResponseDisplay = [];
        //this.totalPages = this.totalPages;
         this.pageNumber = this.pageNumber + 1;
       // console.log("Current Page Number :" + this.pageNumber);
        this.startOffset=this.pageNumber * 10;
        this.endOffset= this.startOffset + 9 ;
        if(this.getTotalResponse.length > this.endOffset)
            this.endOffset= this.endOffset + 0 ;
        else
            this.endOffset=this.getTotalResponse.length - 1;
        for(var i=this.startOffset; i<=this.endOffset;i++){
             this.selectedResponseDisplay.push(this.getTotalResponse[i]);
            }
       
    }
    
    pageSelectionPrev() {
        this. windowTop();
        this.selectedResponseDisplay = [];
        this.pageNumber = this.pageNumber - 1;
       // console.log("Current Page Number :" + this.pageNumber);
        this.startOffset=this.pageNumber * 10;
        this.endOffset= this.startOffset + 9 ;
         if(this.getTotalResponse.length>this.endOffset)
            this.endOffset= this.endOffset + 0 ;
        else
            this.endOffset=this.getTotalResponse.length - 1;
        
        for(var i=this.startOffset; i<=this.endOffset;i++){
          this.selectedResponseDisplay.push(this.getTotalResponse[i]);
        }
       
    }
    
    }
