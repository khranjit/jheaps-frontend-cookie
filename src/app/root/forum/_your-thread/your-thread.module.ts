import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { YourThreadComponent } from './your-thread.component';
import { MarkedThreadComponent } from './_marked-thread/marked-thread.component';
import { PostedThreadComponent } from './_posted-thread/posted-thread.component';
import { RouterModule, Routes } from '@angular/router';

const youthread:Routes = [
  { path: 'thread', component: YourThreadComponent, 
    children:[
      { path: '', redirectTo:'marked-thread', pathMatch:'full'},
      { path: 'marked-thread', component: MarkedThreadComponent},
      { path: 'posted-thread', component: PostedThreadComponent}
    ]
  }
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(youthread)
  ],
  declarations: [
      YourThreadComponent,
      MarkedThreadComponent,
      PostedThreadComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})

export class YourThreadModule { }