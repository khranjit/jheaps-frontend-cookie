import { Component} from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { UserService } from '../../../../services/user.service';
import { ForumService } from '../../../../services/forum.service';

@Component({
    
    selector: 'marked-thread',
    templateUrl: './marked-thread.component.html',
     providers: [UserService, ForumService]
})

export class MarkedThreadComponent {
    errorMessage: any;
    selectedGroupId: any;
    getMyMarkedThreads=[];
    getUserToken : any;
    searchQuery : any;
    ShowUserProfile : any;
    ShowUserName : any;
    ShowUserId : any;
    ShowUserRole : any;
    totalPages: number;
    pageNumber: number;
    currentPage : number;
    
    constructor(private route: ActivatedRoute, private router: Router, private userService: UserService, private forumService: ForumService) {
        //this.getMyMarkedThreads = [];
        this.totalPages = 0;
        this.pageNumber = 0;
        this.Init();
    }

    public Init() {
         this.forumService.MyMarkedThreads(this.pageNumber).then(
            response => {
                //console.log('getGroupNameList response',response);
                this.totalPages =JSON.parse(response).TotalPages;
                this.currentPage =JSON.parse(response).CurrentPage;
                this.getMyMarkedThreads=JSON.parse(response).Items;
               // console.log('this.getMyMarkedThreads response',this.getMyMarkedThreads);
            },
            error => this.errorMessage = <any>error);
        
      this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response);
                this.ShowUserName = this.ShowUserProfile.Items[0].UserName;
                this.ShowUserRole=this.ShowUserProfile.Items[0].Roles;
               // console.log("ShowUserRole",this.ShowUserRole);
               // console.log("ShowUserId",this.ShowUserProfile.Items[0]._id);
                this.ShowUserId=this.ShowUserProfile.Items[0]._id;
             }, error => this.errorMessage = <any>error);  
    }
    
    ViewThread(GroupId,PostId){
     //   console.log("Id :",GroupId)
        let navimodel: NavigationExtras = {
            queryParams: { 'groupId': GroupId,'postId':PostId }
        }
       // console.log("navimodel 1:",navimodel)
        this.router.navigate(['/user/forum/list/thread-view'], navimodel);
    } 
        
    pageSelectionNext() {
        this.totalPages = this.totalPages;
        this.pageNumber = this.pageNumber + 1;
        //console.log("Current Page Number :" + this.pageNumber);
        this.Init();

    }
    
    pageSelectionPrev() {
        this.pageNumber = this.pageNumber - 1;
       // console.log("Current Page Number :" + this.pageNumber);
        this.Init();

    }
}
