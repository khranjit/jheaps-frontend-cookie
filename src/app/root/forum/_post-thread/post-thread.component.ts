import { Component} from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { ForumService } from '../../../services/forum.service';
import { NotifyService } from '../../../services/notify.service';

declare var jQuery: any;

@Component({
    
    selector: 'post-thread',
    templateUrl: './post-thread.component.html',
    providers: [UserService, ForumService, NotifyService]
})

export class PostThreadComponent {
    public forumPostModel: ViewModels.IPostsViewModel;
    errorMessage: any;
    getUserToken : any;
    ShowUserProfile : any;
    ShowUserName : any;
    ShowUserId : any;
    ShowUserRole : any;
    selectedGroupId : any;
    selectedGroupDetails = <any>{};
    
    constructor(private route: ActivatedRoute, private router: Router, private userService: UserService, private forumService: ForumService,
     private toaster: NotifyService) {
         this.forumPostModel=<ViewModels.IPostsViewModel>{};
         this.Init();
    }
    
    Init(){
        window.scrollTo(0, 20);
        this.getUserToken=localStorage.getItem('userToken');
        this.route.queryParams.subscribe(
         params => {
            //console.log(params)
            this.selectedGroupId = params['groupId'];
             this.forumService.getGroupDetails(this.selectedGroupId ).then(
            response => {
              //     console.log("post response:",response)
                   this.selectedGroupDetails=JSON.parse(response);   
                },
                error => this.errorMessage = <any>error); 
         });
    }
    
    postForum(){
        //console.log('this.forumPostModel' ,this.forumPostModel); 
        let post={"groupId" : this.selectedGroupId, "Post" : this.forumPostModel  }
     //   console.log("Post",post);
        this.forumService.postForumContent(post).then(
            response => {
                this.forumPostModel=<any>{};
                this.toaster.SuccessSnack("Posted Successfully");
                 jQuery('#ForumForm')[0].reset();
                let navimodel: NavigationExtras = {
                queryParams: { 'groupId': this.selectedGroupId }
               
            }
       //     console.log("navimodel 1:",navimodel)
            this.router.navigate(['/user/forum/list/group-view'], navimodel);             
                },
                error => this.errorMessage = <any>error); 
    }
    
     ViewGroup(Id){
       //console.log("Id :",Id)
        let navimodel: NavigationExtras = {
            queryParams: { 'groupId': Id }
        }
       // console.log("navimodel 1:",navimodel)
        this.router.navigate(['/user/forum/list/group-view'], navimodel);
    }
   
    ViewThread(GroupId){
       // console.log("Id :",GroupId)
        let navimodel: NavigationExtras = {
            queryParams: { 'groupId': GroupId }
        }
       // console.log("navimodel 1:",navimodel)
        this.router.navigate(['/user/forum/list/post-thread'], navimodel);
    }   
}
