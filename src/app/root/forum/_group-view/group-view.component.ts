import { Component} from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { ForumService } from '../../../services/forum.service';

declare var jQuery: any;

@Component({
    
    selector: 'group-view',
    templateUrl: './group-view.component.html',
    providers: [UserService, ForumService]
})

export class GroupViewComponent {
    errorMessage: any;
    selectedGroupId: any;
    getSelectedGroup: any;
    getUserToken : any;
    searchQuery : any;
    totalPages: number;
    pageNumber: number;
    startOffset : number;
    endOffset : number;
    getTotalPageLength=[];
    selectedThreadDisplay: any;
    SearchThreadResult : boolean;
    
    constructor(private route: ActivatedRoute, private router: Router, private userService: UserService, private forumService: ForumService) {
        this.totalPages = 0;
        this.pageNumber = 0;
        this.startOffset=0;
        this.endOffset=0;
        this.getSelectedGroup = [];
        this.selectedThreadDisplay = [];
        this.SearchThreadResult=false;
        this.Init();
    }

    public Init() {
        this.getUserToken=localStorage.getItem('userToken');
        this.route.queryParams.subscribe(
            params => {
                //console.log(params)
                this.selectedGroupId = params['groupId'];
                if(params['search']){
                    this.SearchThreadResult=true;
                  //  console.log("Found");   
                    this.searchQuery=params['search']
                     let post={"SearchQuery" : this.searchQuery}
                  //  console.log("Search Post",post);
                    this.totalPages = 0;
                    this.pageNumber = 0;
                    this.startOffset=0;
                    this.endOffset=0;
                    this.forumService.forumSearch(post).then(
                        response => {
                            this.selectedThreadDisplay = [];
                    //        console.log('search response',response);
                            this.getSelectedGroup=[JSON.parse(response)];
                            /*this.getTotalPageLength=JSON.parse(response).Items[0].Posts;
                            this.totalPages=1 + Math.floor(JSON.parse(response).Items[0].Posts.length / 10);      
                            console.log( " this.totalPagessssssssssss", this.getTotalPageLength.length);
                            this.startOffset=this.pageNumber * 10;
                            this.endOffset= this.startOffset + 9 ;
                            if(this.getSelectedGroup[0].Posts.length > this.endOffset)
                                this.endOffset= this.endOffset + 0 ;
                            else
                                this.endOffset=this.getSelectedGroup[0].Posts.length - 1;
                            for(var i=this.startOffset; i<=this.endOffset;i++){
                                 this.selectedThreadDisplay.push(this.getTotalPageLength[i]);
                                }     */
                        },
                        error => this.errorMessage = <any>error); 
                }
                else{
                        this.SearchThreadResult=false;
                        this.selectedGroupId=params['groupId']
                        let model = { "groupId": this.selectedGroupId };
                        if (this.getUserToken == null) {
                            let model = { "groupId": this.selectedGroupId };
                            this.totalPages = 0;
                            this.pageNumber = 0;
                            this.startOffset=0;
                            this.endOffset=0;
                            this.forumService.viewSelectedGroupUnAuth(model).then(
                                response => {
                                    this.selectedThreadDisplay = [];
                                    //console.log('getSeletedGroup response',response);
                                    this.getSelectedGroup = [JSON.parse(response)];
                                    this.getTotalPageLength=JSON.parse(response).Posts;
                                    this.totalPages=1 + Math.floor(JSON.parse(response).Posts.length / 10);      
                      //              console.log( " this.totalPagessssssssssss", this.getTotalPageLength.length);
                                    this.startOffset=this.pageNumber * 10;
                                    this.endOffset= this.startOffset + 9 ;
                                    if(this.getSelectedGroup[0].Posts.length > this.endOffset)
                                        this.endOffset= this.endOffset + 0 ;
                                    else
                                        this.endOffset=this.getSelectedGroup[0].Posts.length - 1;
                                    for(var i=this.startOffset; i<=this.endOffset;i++){
                                         this.selectedThreadDisplay.push(this.getTotalPageLength[i]);
                                        }
                                    //this.pageNumber = this.pageNumber + 1;
                                },
                                error => this.errorMessage = <any>error);
                        }
                        else {
                            this.selectedGroupId=params['groupId']
                            let model = { "groupId": this.selectedGroupId };
                            this.totalPages = 0;
                            this.pageNumber = 0;
                            this.startOffset=0;
                            this.endOffset=0;
                            this.forumService.viewSelectedGroupAuth(model).then(
                                response => {
                                    this.selectedThreadDisplay = [];
                                    //console.log('getSeletedGroup response',response);
                                    this.getSelectedGroup = [JSON.parse(response)];
                                    this.getTotalPageLength=JSON.parse(response).Posts;
                                    this.totalPages=1 + Math.floor(this.getTotalPageLength.length / 10);  
                        //            console.log( " this.totalPagessssssssssss",this.getTotalPageLength.length);
                                    this.startOffset=this.pageNumber * 10;
                                    this.endOffset= this.startOffset + 9 ;
                                    if(this.getSelectedGroup[0].Posts.length > this.endOffset)
                                        this.endOffset= this.endOffset + 0 ;
                                    else
                                        this.endOffset=this.getSelectedGroup[0].Posts.length - 1;
                                    for(var i=this.startOffset; i<=this.endOffset;i++){
                                         this.selectedThreadDisplay.push(this.getTotalPageLength[i]);
                                        }
                                   // this.pageNumber = this.pageNumber + 1;
                                    },
                                    error => this.errorMessage = <any>error);
                            }
                        }
                        });
              }
              DateChange(date)
              {
               var CreatedDate:any = new Date(date);
               var today:any = new Date();
               var seconds = Math.floor((today - CreatedDate) / 1000);
       
               var interval = Math.floor(seconds / 31536000);
             
               // if (interval > 1) {
               //   return interval + " years";
               // }
               interval = Math.floor(seconds / 2592000);
               if (interval > 1) {
               //   return interval + " months";
                   return CreatedDate.toDateString("dd MMM yyyy");
               }
               interval = Math.floor(seconds / 86400);
               if (interval > 1) {
                 return interval + " days ago";
               }
               interval = Math.floor(seconds / 3600);
               if (interval > 1) {
                 return interval + " hours ago";
               }
               interval = Math.floor(seconds / 60);
               if (interval > 1) {
                 return interval + " minutes ago";
               }
               return Math.floor(seconds) + " seconds ago";
              }
    postThread(GroupId) {
        this.getUserToken=localStorage.getItem('userToken');
        if(this.getUserToken==null){
             jQuery("#loginmodal").modal('show');    
        }
        else{
      //  console.log("Id :", GroupId)
        let navimodel: NavigationExtras = {
            queryParams: { 'groupId': GroupId }
        }
        //console.log("navimodel 1:", navimodel)
        this.router.navigate(['/user/forum/list/post-thread'], navimodel);
        }
    }

    ViewThread(GroupId, PostId) {
       // console.log("Id :", GroupId)
        let navimodel: NavigationExtras = {
            queryParams: { 'groupId': GroupId, 'postId': PostId }
        }
       // console.log("navimodel 1:", navimodel)
        this.router.navigate(['/user/forum/list/thread-view'], navimodel);
    }
    
   pageSelectionNext() {
        window.scrollTo(0, 20);
       // console.log(this.getSelectedGroup[0].Posts);
        this.selectedThreadDisplay = [];
        //this.totalPages = this.totalPages;
         this.pageNumber = this.pageNumber + 1;
       // console.log("Current Page Number :" + this.pageNumber);
        this.startOffset=this.pageNumber * 10;
        this.endOffset= this.startOffset + 9 ;
        if(this.getSelectedGroup[0].Posts.length > this.endOffset)
            this.endOffset= this.endOffset + 0 ;
        else
            this.endOffset=this.getSelectedGroup[0].Posts.length - 1;
        for(var i = this.startOffset; i <= this.endOffset;i++){
             this.selectedThreadDisplay.push(this.getTotalPageLength[i]);
            }
       
    }
    
    pageSelectionPrev() {
         window.scrollTo(0, 20);
        this.selectedThreadDisplay = [];
        this.pageNumber = this.pageNumber - 1;
        //console.log("Current Page Number :" + this.pageNumber);
        this.startOffset=this.pageNumber * 10;
        this.endOffset= this.startOffset + 9 ;
         if(this.getSelectedGroup[0].Posts.length>this.endOffset)
            this.endOffset= this.endOffset + 0 ;
        else
            this.endOffset=this.getSelectedGroup[0].Posts.length - 1;
        
        for(var i=this.startOffset; i<=this.endOffset;i++){
          this.selectedThreadDisplay.push(this.getTotalPageLength[i]);
        }
       
    }
}
