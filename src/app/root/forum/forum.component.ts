import { Component} from '@angular/core';
import { UserService } from '../../services/user.service';
import { ForumService } from '../../services/forum.service';
import { ToasterPopService } from '../../services/toasterpop.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

declare var jQuery: any;


@Component({
    
    selector: 'forum',
    templateUrl: './forum.component.html',
    providers: [UserService, ForumService, ToasterPopService],
})

export class ForumComponent {

    forumgroups = [{"label":"Select a group"}];


    errorMessage: any;
    selectedGroupId: any;
    searchQuery: any;
    getGroupNameList = [];
    forumGroupName: any;
    ShowUserProfile: any;
    ShowUserName: any;
    ShowUserId: any;
    ShowUserRole: any;
    public ForumTitle:any;

    constructor(private route: ActivatedRoute, private router: Router, private userService: UserService, private forumService: ForumService,
        private toaster: ToasterPopService) {

        this.init();

    }

    init() {
        window.scrollTo(0, 20);
     //   console.log(window.location.pathname, "eewr");
        this.route.queryParams.subscribe(
            params => {
                if (params['groupId']) {
       //             console.log(params)
                    this.selectedGroupId = params['groupId'];
         //           console.log("Console is from Forum Component", this.selectedGroupId);
                }
                else {
                    this.selectedGroupId = "";
                }
            });
        this.forumService.viewForumGroup().then(
            response => {
                //console.log('getGroupNameList response',response);
                this.getGroupNameList = JSON.parse(response).Items;
                this.getGroupNameList.forEach(value=>{
                    let obj = {"label": value.GroupTitle,"value": value._id};
                    this.forumgroups.push(obj);        
                });
                if(this.selectedGroupId)
                {
                    var Group= this.getGroupNameList.find(x=> x._id==this.selectedGroupId);
                    this.ForumTitle = Group.GroupTitle;
                }
            },
            error => this.errorMessage = <any>error);
        this.userService.showProfile().then(
            response => {
                this.ShowUserProfile = JSON.parse(response);
                this.ShowUserName = this.ShowUserProfile.Items[0].UserName;
                this.ShowUserRole = this.ShowUserProfile.Items[0].Roles;
           //     console.log("ShowUserRole", this.ShowUserRole);
             //   console.log("ShowUserId", this.ShowUserProfile.Items[0]._id);
                this.ShowUserId = this.ShowUserProfile.Items[0]._id;
            }, error => this.errorMessage = <any>error);
            
    }

    youThread()
    {
        this.ForumTitle = "Your Threads";
        this.router.navigate(['/user/forum/list/your-thread/thread']);
    }
    onGroupSelect(id){
    let navimodel: NavigationExtras = {
        queryParams: { 'groupId': id }
    }
    this.router.navigate(['/user/forum/list/group-view'], navimodel);
    }

    ViewGroup(Id) {
        let navimodel: NavigationExtras = {
            queryParams: { 'groupId': Id }
        }
        var Group= this.getGroupNameList.find(x => x._id == Id);
        this.ForumTitle = Group.GroupTitle;
        this.router.navigate(['/user/forum/list/group-view'], navimodel);
    }
    checkId(id) {
        if (this.selectedGroupId == id)
            return true;
        else
            return false;
    }

    checkThreadURL() {
        if (window.location.pathname == '/user/forum/list/your-thread/thread/marked-thread' || window.location.pathname == '/user/forum/list/your-thread/thread/posted-thread')
        {
            this.ForumTitle = "Your Threads";
            return true;
        }
        else
        {
            return false;
        }
    }
    checkForumHeading()
    {
        if (window.location.pathname == '/user/forum/list/forum-index')
            return true;
        else
            return false;
    }
    checkGroupHeading()
    {
        if (window.location.pathname != '/user/forum/list/forum-index')
        {
            if(this.selectedGroupId)
            {
                var Group= this.getGroupNameList.find(x=> x._id==this.selectedGroupId);
                if(Group)
                {
                this.ForumTitle = Group.GroupTitle;                    
                }
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    checkAdminURL() {
        if (window.location.pathname == '/user/forum/list/admin-status')
            return true;
        else
            return false;

    }

    Search() {
        //console.log("Id :", this.searchQuery)
        let navimodel: NavigationExtras = {
            queryParams: { 'search': this.searchQuery }
        }
       // console.log("navimodel 1:", navimodel)
        this.router.navigate(['/user/forum/list/group-view'], navimodel);

    }

}