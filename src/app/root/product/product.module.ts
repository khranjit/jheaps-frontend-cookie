import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { ProductComponent } from './product.component';
import { NgxStarsModule } from 'ngx-stars';
import { RouterModule, Routes } from '@angular/router';
import {GalleriaModule} from 'primeng/galleria';
import { NgxGalleryModule } from 'ngx-gallery';
import { ProgressbarModule } from 'ngx-bootstrap';
import { MatInputModule } from '@angular/material';
import {MatTooltipModule} from '@angular/material/tooltip';

const product:Routes = [
  { path: 'item/:id', component: ProductComponent },
  { path: ':id', component: ProductComponent },
  { path: ':id/:name', component: ProductComponent }
]
@NgModule({
  imports: [
    SharedModule,
    NgxStarsModule,
    RouterModule.forChild(product),
    ProgressbarModule.forRoot(),
    GalleriaModule,
    NgxGalleryModule,
    MatInputModule,
    MatTooltipModule,
  ],
  declarations: [ 
    ProductComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ProductModule { } 