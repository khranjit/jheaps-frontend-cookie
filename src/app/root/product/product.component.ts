import { Directive, Component, Injector, ViewChild, OnInit } from '@angular/core';
import { Meta, Title } from "@angular/platform-browser";
import { ActivatedRoute, Router, NavigationExtras,NavigationEnd } from '@angular/router';
import { ProductDescriptionService } from '../../services/product-description.service';
import { ProductService } from '../../services/product.service';
import { ShopService } from '../../services/shop.service';
import { ProductReviewService } from '../../services/productreview.service';
import { AddToCartService, TempCartService, BuyNowService } from '../../services/cart.service';
import { AppComponent } from '../../app.component';
import { ToasterPopService } from '../../services/toasterpop.service';
import { HomeService } from '../../services/home.service';
import { LoginRedirectService } from '../../services/login.service';
import { NotifyService } from '../../services/notify.service';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation, NgxGalleryComponent, NgxGalleryImageSize } from 'ngx-gallery';
import { container } from '@angular/core/src/render3';
const config = require('../../../../config/app-config.json');


declare var jQuery: any;
declare var $: any;
declare var SimilarProduct: any;

@Component({
    selector: 'product',
    styleUrls: ['./product.component.css'],
    providers: [ProductDescriptionService, HomeService, ProductReviewService, AddToCartService, ProductService, ToasterPopService, ShopService],
    templateUrl: './product.component.html'
})

export class ProductComponent implements OnInit {
    galleryOptions: NgxGalleryOptions[];
    galleryImages: NgxGalleryImage[];
    @ViewChild(NgxGalleryComponent) gallery: NgxGalleryComponent;
    ngOnInit() {
        this.galleryOptions = [
            {
                width: '100%',
                height: '700px',
                thumbnailsColumns: 6,
                imagePercent: 85,
                thumbnailsPercent: 15,
                preview: false,
                imageAnimation: NgxGalleryAnimation.Slide,
                imageSize: NgxGalleryImageSize.Contain,
                arrowNextIcon: 'fa fa-angle-right ranjit',
                arrowPrevIcon: 'fa fa-angle-left ranjit'
            },
            // max-width 800
            {
                breakpoint: 800,
                width: '100%',
                height: '500px',
                thumbnailsColumns: 4
            },
            // max-width 767
            {
                breakpoint: 767,
                width: '100%',
                height: '400px',
                thumbnailsColumns: 4,
                thumbnailsPercent: 30,
                thumbnailsMargin: 20,
                thumbnailMargin: 5
            }
        ];
        this.galleryImages = this.imageUrls;
    }
    galleryOnScroll() {
        window.scroll(0, 0);
    }
    tableShow(i) {
        if (this.Attributes.length - 1 == i && this.tab) {
            this.tab = false;
            var trs = jQuery('#overview tr');
            trs.hide();
            trs.slice(0, 5).show();
        }
    }
    ngAfterViewInit() {
        // console.log('on after view init', this.gallery);
    }
    Product: any = {};
    favouriteShop: any;
    favouriteShopList: any = [];
    ReviewsList: any;
    toCart: any;
    Reviews: any;
    reviewFrom = 0;
    Id: any;
    reviewTo = 3;
    showReviewModel = false;
    ReviewForm = false;
    shipProfile: any = { "Detail": [{ "ShippingMethod": [{ ProcessingTime: '', "ShipsTo": [{ Cost: "" }] }] }] };
    addReviewButton: boolean = false;
    show = false;
    descriptionButtonText = 'show more';
    NewReviewModel = {
        Rating: 0,
        Review: '',
        Product: {},
        ShopId: ''
    };
    ProductInfo: any;
    ShopDetails: any;
    Overall: any = {};
    public errorMessage: any;
    public imageUrls: Array<any>;
    public app: any;
    RecommendedProductList: any;
    RecommendedProducts: any = [];
    public selectedVariation: any;
    public selectedVariation1: any;
    public VariationsNum: number;
    reviewChar: number;
    public productCurrency: any = { value: '₹' };
    Reviewslen: any;
    currentViewingImage = 0;
    getUserToken: any;
    test = false;
    public ProductVariationData = [];
    public OutOfStock = 0;
    public MainImage;
    public Price;
    loadLoginDirectives = true;
    public Qty;
    public cartProduct: any = {};
    productId: any;
    showReadButton = true;
    loadtrigger = true;
    hideRecSection: Boolean = false;
    leftProductSection: Boolean = true;
    productfound: Boolean = false;
    carousal = false;
    tab = false;
    Attributes = [];
    showbutton = false;
    productUrl = "/item";
    shippingCost: any;
    handCrafted: boolean;

    constructor(private meta: Meta, private title: Title, private route: ActivatedRoute, private buyNowService: BuyNowService, private loginRedirectService: LoginRedirectService, private homeService: HomeService, private tempCart: TempCartService, private router: Router, private productDescriptionService: ProductDescriptionService,
        private productReviewService: ProductReviewService, private addToCartService: AddToCartService, private Inj: Injector,
        private productService: ProductService, private toasterPopService: ToasterPopService, private shopService: ShopService, private snack: NotifyService) {
        this.reviewChar = 200;
        this.Init();
        window.scrollTo(0, 0);
        this.loginRedirectService.events$.forEach(event => {
            this.loadLoginDirectives = false;
            setTimeout(() => {
                this.loadLoginDirectives = true;
                this.checkFavouriteShopStatus(this.Product.ShopId._id);
            }, 2000)
        });
        jQuery('#productDescription').addClass('container_show_description');

    }
    onRatingSet(a) {
        a = Math.round(a);
        this.NewReviewModel.Rating = a;
    }
    checkFavouriteShopStatus(shopId) {
        var getUserToken = localStorage.getItem('userToken');
        if (getUserToken != null) {
            this.homeService.checkFavouriteShopStatus(shopId).then(response => {
                var res = JSON.parse(response);
                if (res.found) {
                    jQuery('#favShop').removeClass('setNormal');
                    jQuery('#favShop').addClass('setFavourite');
                }
                else {
                    jQuery('#favShop').removeClass('setFavourite');
                    jQuery('#favShop').addClass('setNormal');
                }
            }, error => {
                jQuery('#favShop').removeClass('setFavourite');
                jQuery('#favShop').addClass('setNormal');
            });
        }
        else {
            jQuery('#favShop').removeClass('setFavourite');
            jQuery('#favShop').addClass('setNormal');
        }
    }
    ReadMoreClick() {
        this.reviewFrom = 0;
        this.reviewTo += 3
    }
    showMore(event) {
        var target = event.currentTarget;
        if (this.show == true) {
            this.show = false;
            target.previousElementSibling.classList.add('container_show');
            target.previousElementSibling.classList.remove('show');
            target.innerHTML = "Show more";
        }
        else {
            this.show = true;
            target.previousElementSibling.classList.remove('container_show');
            target.previousElementSibling.classList.add('show');
            target.innerHTML = "Show less";
        }
    }

    showMore_descrption(event) {

        var target = event.currentTarget;

        if (this.show == true) {
            this.show = false;
            target.previousElementSibling.classList.add('container_show_description');
            target.previousElementSibling.classList.remove('show');
            target.innerHTML = "Show more";
        }
        else {
            this.show = true;
            target.previousElementSibling.classList.remove('container_show_description');
            target.previousElementSibling.classList.add('show');
            target.innerHTML = "Show less";
        }
    }
    CollapseClick() {
        this.reviewFrom = 0;
        this.reviewTo = 3
    }
    checkReview() {

        jQuery('#desId').removeClass('active');
        jQuery('#reviewId').addClass('active');
    }
    checkdes() {
        this.test = true;
    }
    addReview() {
        this.NewReviewModel.Product = this.Product._id;
        this.NewReviewModel.ShopId = this.Product.ShopId._id;
        this.productReviewService.saveReview(this.NewReviewModel).then(
            res => {
                var result = JSON.parse(res);
                if (result.Flag == 1) {
                    this.snack.ErrorSnack('You cannot review for this product');
                } else if (result.Flag == 2) {
                    this.snack.ErrorSnack('You are already reviewed this product');
                }
                else {
                    this.Review(this.Product._id);
                    this.OverallReview(this.Product._id);
                    this.addReviewButton = false;
                    this.showReviewModel = false;
                    this.ReviewForm = false;
                }
            },
            error => this.errorMessage = <any>error
        );
    }
    resetReviewModel() {
        this.getUserToken = localStorage.getItem('userToken');
        if (this.getUserToken != null) {
            this.productReviewService.checkReview(this.productId).then(
                res => {
                    var result = JSON.parse(res);
                    if (result.UserReview == "Already Reviewed") {
                        this.addReviewButton = false;
                        // this.snack.ErrorSnack('You are already reviewed this product');
                        this.showReviewModel = false;
                        jQuery('#tab-addReviews').removeClass('active in');
                    } else if (result.UserReview) {
                        this.addReviewButton = true;
                    }
                    else {
                        this.addReviewButton = false;
                        this.showReviewModel = false;
                        jQuery('#tab-addReviews').removeClass('active in');
                    }
                },
                error => this.errorMessage = <any>error
            );

        }
    }
    visibleReviewModel() {
        this.showReviewModel = true;
        this.NewReviewModel = {
            Rating: 0,
            Review: '',
            Product: {},
            ShopId: '',
        };
        this.ReviewForm = true;
        jQuery('#tab-addReviews').addClass('active in');
    }
    goBack() {
        this.showReviewModel = false;
        this.ReviewForm = false;
        jQuery('#tab-addReviews').removeClass('active in');
    }
    goToTop() {
        window.scrollTo(0, 0);
    }
    public Init() {
        this.tab = true;
        this.getUserToken = localStorage.getItem('userToken');
        this.ReviewsList = {};
        this.imageUrls = [];
        this.Product = <any>{ Categories: [{ Name: "" }], ShopId: { ShopAccountId: { approvedBusinessDetails: { Name: "" } } } };
        this.toCart = <any>{};
        this.route
            .params
            .subscribe(params => {
                if (params['id']) {
                    this.router.events.subscribe((event: any) => {
                        if (event instanceof NavigationEnd ) {
                            var url = event.url;
                            //get rid of the trailing / before doing a simple split on /
                            var url_parts = url.replace(/\/\s*$/,'').split('/'); 
                            //since we do not need example.com
                            url_parts.shift(); 
                            if(url_parts[2] == "item")
                            {
                                this.productUrl = "";
                            }
                            else
                            {
                                this.productUrl = "/item";
                            }
                        }
                      });
                    this.showbutton = false;
                    this.carousal = true;
                    this.ProductInfo = params['id'];
                    this.productId = params['id'];
                    this.Review(this.ProductInfo);
                    this.resetReviewModel();
                    this.productDescriptionService.getProductById(this.ProductInfo)
                        .then(
                            product => {
                                var result = product;
                                this.productfound = true;
                                this.OutOfStock = 0;
                                this.Product = JSON.parse(product);
                                this.title.setTitle(this.Product.Name);
                                var len = this.Product.Description.length;
                                var shippingProfile = this.Product.ShippingProfiles.Profiles;
                                this.shipProfile = shippingProfile.find(x => x._id == this.Product.ShippingProfile);
                                this.shippingCost=this.shipProfile.Detail[0].ShippingMethod[0].ShipsTo[0].Cost;
                                this.getShopDetailsFromShopId(this.Product.ShopId._id);
                                this.cartProduct.Product = this.Product._id;
                                this.cartProduct.Qty = 1;
                                if( this.Product.hasOwnProperty('handCrafted' )) {
                                    this.handCrafted=this.Product.handCrafted;
                                }
                                else {
                                    this.handCrafted=false;
                                }
                                this.imageUrls = [];
                                for (var i = 0; i < this.Product.Media.length; i++) {
                                    this.imageUrls.push({
                                        small: this.Product.Media[i].EditedImage,
                                        medium: this.Product.Media[i].EditedImage,
                                        big: this.Product.Media[i].EditedImage
                                    });
                                }
                                this.Product.Attributes.forEach(item => {
                                    if (item.ShowAttribute) {
                                        if(item.Value.length >0){
                                            this.Attributes.push({
                                                Name: item.Name,
                                                Values: item.Value
                                            })
                                        }
                                    }
                                })
                                if (this.Attributes.length < 5) {
                                    this.Product.Attributes.forEach(item => {
                                        if (!item.ShowAttribute) {
                                            if(item.Value.length >0){
                                                this.Attributes.push({
                                                    Name: item.Name,
                                                    Values: item.Value
                                                })
                                            }
                                        }
                                    })
                                }
                                if (this.Product.Description.length > 226) {
                                    this.showbutton = true;
                                }
                                this.imageCheck(this.Product);
                                this.checkFavouriteShopStatus(this.Product.ShopId._id);
                                this.getRecommendedProducts(this.Product);
                                this.OverallReview(this.productId);
                                var similarBody = {
                                    CategoryId: this.Product.Categories[0].Id,
                                    Price: this.Price
                                };
                                if (this.getUserToken != null) {
                                    this.productDescriptionService.saveTrending(this.ProductInfo);
                                    this.productDescriptionService.saveRecommended(similarBody);
                                }
                            },
                            error => {
                                this.errorMessage = <any>error;

                                this.router.navigate(['/user/notfoundpage']);

                            }
                        );
                }
            });
    }
    public OverallReview(productId) {
        this.productService.OverallReview(productId).then(result => {
            var res = JSON.parse(result);
            this.Overall.OneStar = res.OneStar;
            this.Overall.TwoStar = res.TwoStar;
            this.Overall.ThreeStar = res.ThreeStar;
            this.Overall.FourStar = res.FourStar;
            this.Overall.FiveStar = res.FiveStar;
            this.Overall.Rating = res.Rating;
            this.Overall.Total = this.Overall.OneStar + this.Overall.TwoStar + this.Overall.ThreeStar + this.Overall.FourStar + this.Overall.FiveStar;


        })
    }
    public imageCheck(ProductDetails) {
        if (ProductDetails.useVariation) {
            if (ProductDetails.useDefaultQuantity) {
                if (ProductDetails.QuantityAvailable <= 0) {
                    this.OutOfStock = 1;
                }
                this.VariationCheck();
            }
            else {
                this.VariationCheck();
            }
        }
        else {
            this.MediaImage(ProductDetails);
            this.Price = ProductDetails.Price;
            if (ProductDetails.QuantityAvailable > 0) {
                this.Qty = ProductDetails.QuantityAvailable;
                this.cartProduct.Qty = 1;
            }
            else {
                this.OutOfStock = 1;
            }
        }
    }
    public MediaImage(Product) {
        this.MainImage = this.Product.Media.find(x => x.IsPrimary).EditedImage;
        var arryindex = this.imageUrls.map(function (e) { return e.small; }).indexOf(this.MainImage);
        this.openPreview(arryindex);
        this.meta.addTags([
            { name: 'author', content: 'Coursetro.com' },
            { name: 'keywords', content: 'angular seo, angular 4 universal, etc' },
            { name: 'description', content: this.Product.Description },
            { property: 'og:image', content: this.MainImage }
        ]);

    }
    public getShopDetailsFromShopId(shopId) {
        this.shopService.getShopDetailsFromShopId(shopId)
            .then(
                shop => {
                    this.ShopDetails = JSON.parse(shop);
                },
                error => this.errorMessage = <any>error);
    }
    getRecommendedProducts(productObj) {
        this.productService.getRecommendedProducts(productObj)
            .then(
                recommended => {
                    this.RecommendedProductList = JSON.parse(recommended);
                    if (this.RecommendedProductList.hits.hits.length > 0) {
                        this.RecommendedProducts = this.RecommendedProductList.hits.hits;
                        this.hideRecSection = true;
                    }
                },
                error => this.errorMessage = <any>error);
    }
    public Review(productId) {
        this.productReviewService.getAll(0, 3, productId)
            .then(
                reviewProducts => {
                    this.ReviewsList = JSON.parse(reviewProducts);
                    this.Reviews = this.ReviewsList.Items;

                },
                error => this.errorMessage = <any>error);
    }
    public minusQuantity() {
        if (this.cartProduct.Qty > 1) {
            this.cartProduct.Qty = this.cartProduct.Qty - 1;
        }
    }
    public validQuantity() {
        if (this.cartProduct.Qty <= 0 && this.Qty >= 1) {
            this.cartProduct.Qty = 1;
            return true;
        }
        else if (this.cartProduct.Qty > 20) {
            this.cartProduct.Qty = 20;
            this.snack.ErrorSnack('We are sorry! Only 20 unit(s) for each customer.');
            return false;
        }
        else if (this.Qty < this.cartProduct.Qty) {
            this.cartProduct.Qty = this.Qty;
            this.snack.ErrorSnack('Only ' + this.Qty + ' unit(s) is available');
            return false;
        }
    }
    public plusQuantity() {
        if (this.cartProduct.Qty >= 20) {
            this.snack.ErrorSnack('We are sorry! Only 20 unit(s) for each customer.');
        }
        else if (this.cartProduct.Qty < this.Qty && this.cartProduct.Qty < 20) {
            this.cartProduct.Qty = this.cartProduct.Qty + 1;
        }
        else {
            this.snack.ErrorSnack('Only ' + this.Qty + ' unit(s) is available');
        }
    }
    public VariationCheck() {
        let j = 0;
        var variationdata = this.Product.Variations;
        var activeImage;
        for (let i = 0; i < variationdata.length; i++) {
            if (j == 0) {
                if (this.Product.Variations[i].Active && this.Product.Variations[i].Image) {
                    this.MainImage = this.Product.Variations[i].Image;
                }
                else {
                    this.MediaImage(this.Product);
                }
                if (this.Product.Variations[i].Active && this.Product.Variations[i].InStock && this.Product.Variations[i].Qty > 0) {
                    if (!this.Product.Variations[i].Image) {
                        this.MediaImage(this.Product);
                    }
                    else {
                        this.MainImage = this.Product.Variations[i].Image;
                    }
                    if (this.Product.useDefaultPrice) {
                        this.Price = this.Product.Price;
                    }
                    else {
                        this.Price = this.Product.Variations[i].Price;
                        this.Product.Price = this.Product.Variations[i].Price;
                    }
                    this.ProductVariationData[0] = this.Product.Variations[i].Value1;
                    if (this.Product.Variations[i].hasOwnProperty("Value2")) {
                        this.ProductVariationData[1] = this.Product.Variations[i].Value2;
                    }
                    this.cartProduct.Variation = this.Product.Variations[i]._id;
                    if (this.Product.useDefaultQuantity) {
                        this.Qty = this.Product.QuantityAvailable;
                    }
                    else {
                        this.Qty = this.Product.Variations[i].Qty;
                    }
                    this.cartProduct.Qty = 1;
                    j = 1;
                }
                else if (i == variationdata.length - 1 && j == 0) {
                    this.OutOfStock = 1;
                }
            }
        }
        var arryindex = this.imageUrls.map(function (e) { return e.small; }).indexOf(this.MainImage);
        this.openPreview(arryindex);
        this.meta.addTags([
            { name: 'author', content: 'Coursetro.com' },
            { name: 'keywords', content: 'angular seo, angular 4 universal, etc' },
            { name: 'description', content: this.Product.Description },
            { property: 'og:image', content: this.MainImage }
        ]);
    }
    openPreview(a): void {
        this.gallery.show(a);
    }
    //navigation functions
    onNavigateNewtab(link, event) {
        window.open(link+config.apiUrl+'/user/product/'+ event, "_blank");
    }
    public variationChange() {
        for (let i = 0; i < this.Product.Variations.length; i++) {
            if (this.Product.Variations[i].Value1 == this.ProductVariationData[0] && this.Product.Variations[i].Value2 == this.ProductVariationData[1]) {
                this.VariationsNum = i;
                if (this.Product.Variations[i].Active && this.Product.Variations[i].InStock && this.Product.Variations[i].Qty > 0) {
                    this.leftProductSection = true;
                    this.cartProduct.Variation = this.Product.Variations[i]._id;
                    if (this.Product.useDefaultPrice) {
                        this.Price = this.Product.Price;
                    }
                    else {
                        this.Price = this.Product.Variations[i].Price;
                    }
                    if (this.Product.useDefaultQuantity) {
                        this.Qty = this.Product.QuantityAvailable;
                    }
                    else {
                        this.Qty = this.Product.Variations[i].Qty;
                    }
                    if (this.Product.Variations[i].Image) {
                        this.MainImage = this.Product.Variations[i].Image;
                        var arrindex = this.imageUrls.map(function (e) { return e.small; }).indexOf(this.MainImage);
                        this.openPreview(arrindex)
                        console.log(this.imageUrls)
                    }
                    else {
                        this.MediaImage(this.Product);
                    }
                    this.cartProduct.Qty = 1;
                }
                else {
                    this.cartProduct.Variation = "";
                    this.leftProductSection = false;
                    this.snack.ErrorSnack('Sorry, Your Selected Product is not available So, Please select another one');
                }
            }
        }
    }
    buyNow() {
        this.cartProduct.BuyNow = true;
        var data = this.cartProduct;
        var Cart = JSON.stringify(data);
        this.getUserToken = localStorage.getItem('userToken');
        if (this.cartProduct.Variation != "") {
            if (this.getUserToken != null) {
                this.addToCartService.clearBuyNow().then(
                    res => {
                        this.addToCartService.setCartProduct(Cart)
                            .then(
                                cartItems => {
                                    var res = JSON.parse(cartItems);
                                    if (res.msg != true) {
                                        this.snack.ErrorSnack(res.msg);
                                    }
                                    else
                                    {
                                        this.router.navigate(['/user/cart/buy/buynow']);
                                    }
                                },
                                error => this.errorMessage = <any>error);

                    },
                    error => this.errorMessage = <any>error);
            }
            else {
                jQuery("#loginmodal").modal('show');
                var ramdom = Math.floor(Math.random() * 10000);
                this.loginRedirectService.SetRedirectURL(ramdom);
                this.tempCart.createBuyNow(ramdom);
                this.loginRedirectService.events$.forEach(event => {
                    var checkBuyNow = this.tempCart.getBuyNow();
                    if (event == ramdom && checkBuyNow) {
                        this.loginRedirectService.ClearRedirectURL();
                        this.buyNow();
                    }
                });
            }
        }
        else {
            this.snack.ErrorSnack('This Product is not available.Please select another');
        }
    }
    public addToCart() {
        this.getUserToken = localStorage.getItem('userToken');
        this.cartProduct.BuyNow = false;
        var data = this.cartProduct;
        data.addToCart = true;
        var Cart = JSON.stringify(data);
        if (this.cartProduct.Variation != "" && this.cartProduct.Qty != 0 && this.Qty >= this.cartProduct.Qty) {
            if (this.getUserToken != null) {
                this.addToCartService.setCartProduct(Cart)
                    .then(
                        cartItems => {
                            var res = JSON.parse(cartItems);
                            this.app = this.Inj.get(AppComponent);
                            this.app.cart();
                            if (res.msg != true) {
                                this.snack.ErrorSnack(res.msg);
                            }
                            else
                            {
                                this.snack.SuccessSnack("Item added to cart!");
                            }
                        },
                        error => this.errorMessage = <any>error);
            }
            else {
                var message = "";
                message = this.tempCart.PushTempCart(Cart);
                if (message === "Item already present") {
                    this.snack.ErrorSnack('Item already exists in your cart');
                    return;
                }
                else
                {
                    this.snack.SuccessSnack("Item added to cart!");
                }
                this.app = this.Inj.get(AppComponent);
                this.app.cart();
            }
        }
        else {
            this.snack.ErrorSnack('This Product is not available.Please select another');
        }
    }
    public productInfo(productId) {
        window.scrollTo(0, 0);
        this.router.navigate(['/user/product'+this.productUrl, productId]);
    }
    public productRedirect(id) {
        this.router.navigate(['/user/product', id]);
    }
    ShowProductsByShop(shopId) {
        this.router.navigate(['/user/shop', shopId]);
    }
    public loadScript(url) {
        let node = document.createElement('script');
        node.src = url;
        node.type = 'text/javascript';
        document.getElementsByTagName('head')[0].appendChild(node);
    }
    public initMagnificPopup() {
        var self = this;
        var arr = [];
        this.imageUrls.forEach(function (imageUrl) {
            var src = {
                src: imageUrl
            };
            arr.push(src);
        });
        setTimeout(function () {
            jQuery.magnificPopup.open({
                items: arr,
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
                },
                type: 'image'
            }, self.currentViewingImage);
        }, 0);
    }
    public changeGallerySource(event, imageSrc, i) {
        this.currentViewingImage = i;
        this.activeGalleryImage();
    }
    public previousGalleryImage() {
        if (this.currentViewingImage > 0) {
            this.currentViewingImage = this.currentViewingImage - 1;
        } else {
            this.currentViewingImage = this.imageUrls.length - 1;
        }
        this.activeGalleryImage();
    }
    public nextGalleryImage() {
        if (this.currentViewingImage < this.imageUrls.length) {
            this.currentViewingImage = this.currentViewingImage + 1;
        } else {
            this.currentViewingImage = 0;
        }
        this.activeGalleryImage();
    }
    public activeGalleryImage() {
        jQuery("#gallery-image").attr("src", this.imageUrls[this.currentViewingImage]);
        jQuery("#gallery-popup").attr("href", this.imageUrls[this.currentViewingImage]);
        jQuery(".gallery-cell").removeClass("is-selected is-nav-selected");
        jQuery("#gallery-thumb-" + this.currentViewingImage).addClass(" is-selected is-nav-selected");
    }
    showallReview() {
        this.router.navigate(['/user/review', this.productId]);
    }
    public initGallery() {
        var self = this;
        jQuery(function () {
            setTimeout(function () {
                jQuery(".flickity-prev-next-button").hide();
            }, 2000);
        });
    }
    slickActivate(i) {
        if (this.RecommendedProducts.length - 1 == i && this.carousal) {
            this.carousal = false;
            SimilarProduct.load();
        }
    }
    public callFlickity() {
        var self = this;
        jQuery(function () {
            setTimeout(function () {
                var $gallery = jQuery(".gallery-main").flickity({
                    cellAlign: 'center',
                    contain: true,
                    wrapAround: true,
                    autoPlay: false,
                    prevNextButtons: (self.imageUrls.length > 1),
                    percentPosition: true,
                    imagesLoaded: true,
                    draggable: false,
                    watchCSS: true,
                    lazyLoad: 1,
                    pageDots: false,
                    selectedAttraction: 0.1,
                    friction: 0.6,
                    rightToLeft: false,
                    arrowShape: 'M 10,50 L 60,100 L 65,95 L 20,50  L 65,5 L 60,0 Z'
                });

                // thumbs
                jQuery('.gallery-thumbs').flickity({
                    asNavFor: '.gallery-main',
                    contain: true,
                    cellAlign: 'left',
                    wrapAround: false,
                    autoPlay: false,
                    prevNextButtons: false,
                    percentPosition: true,
                    imagesLoaded: true,
                    pageDots: false,
                    selectedAttraction: 0.1,
                    friction: 0.6,
                    rightToLeft: false
                });
                setTimeout(function () {
                    $gallery.flickity('reloadCells');
                }, 5000);
                // magnific popup bug fix
                var flkty = $gallery.data('flickity');
                var arr = jQuery.map(jQuery(".gallery-main").find("img"), function (el) {
                    return { "src": el.src }
                });
                $gallery.on('staticClick', function (event, pointer, cellElement, cellIndex) {
                    if (!cellElement) {
                        return;
                    }
                    jQuery.magnificPopup.open({
                        items: arr,
                        gallery: {
                            enabled: true
                        },
                        type: 'image'
                    }, cellIndex);
                });
            }, 0);
        });
    }
}