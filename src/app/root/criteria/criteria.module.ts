import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { CriteriaComponent } from './criteria.component';
import {NgPipesModule} from 'ngx-pipes';
import { RouterModule, Routes } from '@angular/router';

const criteria:Routes = [
  { path: ':id', component: CriteriaComponent }
]
@NgModule({
  imports: [
      SharedModule,
      NgPipesModule,
      RouterModule.forChild(criteria)
  ],
  declarations: [
    CriteriaComponent
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class CriteriaModule { 
}
