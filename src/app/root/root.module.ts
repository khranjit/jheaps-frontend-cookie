import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const root:Routes = [
  { path: '', redirectTo:'home', pathMatch:'full' },
  // { path: 'home', loadChildren:'./home/home.module#HomeModule' },
  { path: 'product', loadChildren:'./product/product.module#ProductModule' },
  { path: 'shop', loadChildren:'./shop/shop.module#ShopModule' },
  { path: 'shopview', loadChildren:'./shopview/shopview.module#ShopViewModule' },
  { path: 'shopreview', loadChildren:'./shopreview/shopreview.module#ShopReviewModule' },
  { path: 'cart', loadChildren:'./cart/cart.module#CartModule' },
  { path: 'contact', loadChildren:'./contact/contact.module#ContactModule' },
  { path: 'search', loadChildren:'./search/search.module#SearchModule' },
  { path: 'criteria', loadChildren:'./criteria/criteria.module#CriteriaModule' },
  { path: 'privacy', loadChildren:'./privacy/privacy.module#PrivacyModule' },
  { path: 'feedback', loadChildren:'./feedback/feedback.module#FeedbackModule' },
  { path: 'faq', loadChildren:'./faq/faq.module#FaqModule' },
  { path: 'forum', loadChildren:'./forum/forum.module#ForumModule' },
  {path: 'success', loadChildren:'./_success/success.module#SuccessModule'},
  {path: 'failure', loadChildren:'./_failure/failure.module#FailureModule'},
  {path: 'review', loadChildren:'./review/review.module#ReviewModule' },
  {path: 'aboutus', loadChildren:'./aboutus/aboutus.module#AboutusModule'}
  // { path: "**", loadChildren:'./notfoundpage/notfoundpage.module#NotFoundPageModule' }
]
@NgModule({
  imports: [
    RouterModule.forChild(root)
  ],
  declarations: [],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class RootModule { } 