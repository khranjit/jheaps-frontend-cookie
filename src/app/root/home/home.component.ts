import { Component, ChangeDetectionStrategy, ViewEncapsulation, Injector, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { FeaturedProductService } from '../../services/featured.service';
import { TrendingProductService } from '../../services/trending.service';
import { FeaturedShopService } from '../../services/featured-shop.service';
import { HomeService } from '../../services/home.service';
import { ProductService } from '../../services/product.service';
import { ElasticService } from '../../services/elastic.service';
import { NotifyService } from '../../services/notify.service';
import { AddToCartService, TempCartService } from '../../services/cart.service';
import { AppComponent } from '../../app.component';
import { ProductDescriptionService } from '../../services/product-description.service';
import { LoginRedirectService, LogoutRedirectService } from '../../services/login.service';
declare var jQuery: any;
declare var FeaturedProduct: any;

import { CookieService } from 'ngx-cookie-service';

@Component({
    selector: 'Home',
    providers: [ProductDescriptionService,CookieService, TrendingProductService, AddToCartService, FeaturedProductService, FeaturedShopService, ProductService, HomeService, NotifyService, ElasticService],
    templateUrl: './home.component.html'
})
export class HomeComponent implements AfterViewInit {
    public offset1 = 0;
    app: any;
    toCart: any = {};
    public bannerModel: ViewModels.IBannerViewModel;
    public productModel: ViewModels.IFavoriteProductViewModel;
    public shopModel: ViewModels.IFavoriteShopViewModel;
    public Sections: any;
    public SectionsList: any;
    TrendingProducts: any;
    TrendingProductsList: any;
    FeaturedProducts: any;
    FeaturedProductsList: any;
    FeaturedShops: any = [];
    FeaturedShopList: any;
    ProductInfo: any;
    public errorMessage: any;
    public productCurrency: any = { value: "USD" };
    ProductList: any;
    Products: any;
    initialPage: number = 0;
    SpecialOfferTitles1: any;
    SpecialOfferTitles2: any;
    SpecialOfferTitles3: any;
    SpecialOfferTitles4: any;
    SpecialOfferTitlesList: any;
    ShowBannerList: any;
    ShowBanners: any;
    Banners: any;
    RandomBannerNumber: any;
    SearchTerm: string;
    ProductLimit: number;
    threeColumnResponse = [];
    hideTrendSection: boolean;
    getUserToken: any;
    loadLoginDirectives = true;
    getFavProductList: any;
    hideRecSection: Boolean = false;
    RecommendedProducts: any = [];
    RecommendedProductsList: any = {};
    offset = 0;
    limit = 12;
    randomProducts: any = [];
    totalRandomProducts: any = {};
    carousal = false;
    constructor(private route: ActivatedRoute, private logoutRedirectService: LogoutRedirectService, private productDescriptionService: ProductDescriptionService, private tempCart: TempCartService, private Inj: Injector, private addToCartService: AddToCartService, private router: Router, private featuredService: FeaturedProductService, private trendingService: TrendingProductService, private homeService: HomeService,
        private featuredShopService: FeaturedShopService,private _cookieService: CookieService, private productService: ProductService, private elasticService: ElasticService, private toasterPopService: NotifyService, private loginRedirectService: LoginRedirectService, private snack: NotifyService) {
        this.hideTrendSection = false;
        this.homeBanner();
        this.Init();
        // this.getSpecialBestAccessoriesTopProducts();
        // setTimeout(function () {
        //     jQuery('.ng2-auto-complete-wrapper').addClass('input-group-lg');
        // }, 200);
        window.scrollTo(0, 0);
        this.route.params.subscribe(params => {
            if (params.vvv) {
                jQuery("#loginmodal").modal('show');
            }
        });
    }
    moreRandomProdcuts() {
        if (this.totalRandomProducts.CurrentPage + 1 < this.totalRandomProducts.TotalPages) {
            this.offset = this.offset + 1;
            this.getRandomProducts();
        }
    }
    getRandomProducts() {
        this.productDescriptionService.getRandom(this.offset, this.limit)
            .then(
                random => {
                    var products = JSON.parse(random);
                    this.totalRandomProducts = products;
                    if (this.randomProducts.length > 0) {
                        this.randomProducts = this.randomProducts.concat(products.Items);
                    }
                    else {
                        this.randomProducts = products.Items;
                    }
                },
                error => this.errorMessage = <any>error);
    }
    ngOnInit() {

    }
    hideLoader() {
        jQuery('.loaderbanner').css('display', 'none')
    }
    slickActivate(i) {
        if (this.TrendingProducts.length - 1 == i && this.carousal) {
            this.carousal = false;
            FeaturedProduct.load();
        }
    }
    ngAfterViewInit() {
        this.loginRedirectService.events$.forEach(event => {
            this.loadLoginDirectives = false;
            setTimeout(() => {
                this.loadLoginDirectives = true;
            }, 1000)

        });
        this.logoutRedirectService.events$.forEach(event => {
            this.loadLoginDirectives = false;
            setTimeout(() => {
                this.loadLoginDirectives = true;
            }, 1000)
        })
    }
    public Init() {
        this.carousal = true;
        this.getUserToken = this._cookieService.get('userToken');
        this.getTrendingProduct();
        this.getRandomProducts();
    }
    getTrendingProduct() {
        this.productDescriptionService.getTrending()
            .then(
                trendingproducts => {
                    this.hideTrendSection = true;
                    this.TrendingProducts = [];
                    this.TrendingProductsList = JSON.parse(trendingproducts);
                    this.TrendingProducts = this.TrendingProductsList.Items;

                    this.TrendingProducts.forEach((TrendingPdt: any) => {
                        TrendingPdt["FavFlag"] = false;
                    });
                },
                error => this.errorMessage = <any>error);
    }
    getRecommendedProduct() {
        this.productDescriptionService.getRecommended()
            .then(
                products => {
                    this.RecommendedProducts = [];
                    this.RecommendedProductsList = JSON.parse(products);
                    if (this.RecommendedProductsList.length > 0) {
                        this.hideRecSection = true;
                        this.RecommendedProducts = this.RecommendedProductsList;
                        this.RecommendedProducts.forEach((RecommendedPdt: any) => {
                            RecommendedPdt["FavFlag"] = false;
                        });
                    }
                },
                error => this.errorMessage = <any>error);
    }
    getThreeColumnListInfo() {
        this.homeService.getThreeColumnsData().then(response => {
            if (JSON.parse(response).Items.length > 0) {
                this.threeColumnResponse = JSON.parse(response).Items;
            }
        },
            error => this.errorMessage = <any>error);
    }
    public addToCart(item: any) {
        this.productDescriptionService.getProductById(item._id)
            .then(
                product => {
                    var Product = JSON.parse(product);
                    this.getUserToken = this._cookieService.get('userToken');
                    if (Product.QuantityAvailable > 0) {
                        this.toCart = {};
                        this.toCart.Shop = {
                            Id: Product.Shop.Id,
                            Name: Product.Shop.Name
                        }
                        Product.Id = Product._id;
                        this.toCart.Product = Product;
                        this.toCart.Product.Variation = {};
                        this.toCart.Product.Media = Product.Media[0];
                        if (Product.Variations.length > 0) {
                            var activeVariation = Product.Variations.findIndex(ele => ele.Active == true && ele.Qty > 0);
                            if (activeVariation != -1) {
                                this.toCart.Product.Variation.Id = Product.Variations[activeVariation]._id;
                                this.toCart.Product.QuantityAvailable = Product.Variations[activeVariation].Qty;
                                this.toCart.Product.Price = Product.Variations[activeVariation].Price;
                                this.toCart.Product.Qty = 1;
                            }
                        }
                        if (this.getUserToken != null) {
                            this.addToCartService.clearBuyNow().then(
                                res => {
                                    this.addToCartService.setCartProduct(this.toCart).then(
                                        cartItems => {
                                            this.app = this.Inj.get(AppComponent);
                                            this.app.cart();
                                        },
                                        error => {
                                            this.errorMessage = <any>error;
                                            this.snack.ErrorSnack('Error while adding the product to cart');
                                        });
                                },
                                error => this.errorMessage = <any>error);
                        }
                        if (this.getUserToken == null) {

                            var message = "";
                            message = this.tempCart.PushTempCart(this.toCart);
                            if (message === "Item already present") {
                                this.snack.ErrorSnack('Already exist in your cart');
                            }
                            else {
                                this.app = this.Inj.get(AppComponent);
                                this.app.cart();
                            }
                        }
                    }
                    else {
                        this.snack.ErrorSnack('This product is Out of stock');
                    }

                },
                error => this.errorMessage = <any>error);
    }

    navigateToAdminCriteriaForBanner(criteria) {
        let link = ['/user/criteria', criteria.AdminCriteriaId._id];
        this.router.navigate(link);
    }
    navigateToAdminCriteria(criteria) {
        let link = ['/user/criteria', criteria.AdminCriteriaId];
        this.router.navigate(link);
    }
    public getSpecialBestAccessoriesTopProducts() {
        this.Products = [];
        this.Products.ColumnOne = [];
        this.Products.ColumnTwo = [];
        this.Products.ColumnThree = [];
        this.Products.ColumnFour = [];
        this.productService.getSpecialOfferTitles()
            .then(
                offers => {
                    this.productService.getSpecialBestAccessoriesTopProducts()
                        .then(products => {
                            this.ProductList = JSON.parse(products);
                            for (var j = this.ProductList.hits.hits.length - 1; j >= 0; j--) {
                                var randomIndex = Math.floor(Math.random() * (j + 1));
                                var itemAtIndex = this.ProductList.hits.hits[randomIndex];
                                this.ProductList.hits.hits[randomIndex] = this.ProductList.hits.hits[j];
                                this.ProductList.hits.hits[j] = itemAtIndex;
                            }
                            for (var i = 0; i < this.ProductList.hits.hits.length; i++) {
                                if (this.ProductList.hits.hits[i]._source.ColumnOne && this.Products.ColumnOne.length < this.ProductLimit) {
                                    this.Products.ColumnOne.push(this.ProductList.hits.hits[i]);
                                }

                                if (this.ProductList.hits.hits[i]._source.ColumnTwo && this.Products.ColumnTwo.length < this.ProductLimit) {
                                    this.Products.ColumnTwo.push(this.ProductList.hits.hits[i]);
                                }

                                if (this.ProductList.hits.hits[i]._source.ColumnThree && this.Products.ColumnThree.length < this.ProductLimit) {
                                    this.Products.ColumnThree.push(this.ProductList.hits.hits[i]);
                                }

                                if (this.ProductList.hits.hits[i]._source.ColumnFour && this.Products.ColumnFour.length < this.ProductLimit) {
                                    this.Products.ColumnFour.push(this.ProductList.hits.hits[i]);
                                }
                            }
                        },
                            error => this.errorMessage = <any>error);
                },
                error => this.errorMessage = <any>error);
    }

    public Product() {
        this.featuredService.getAll()
            .then(
                featuredproducts => {
                    this.FeaturedProductsList = JSON.parse(featuredproducts);
                    this.FeaturedProducts = this.FeaturedProductsList.Items;
                },
                error => this.errorMessage = <any>error);
    }
    public homeBanner() {
        this.bannerModel = <ViewModels.IBannerViewModel>{};
        this.ShowBannerList = [];
        this.ShowBanners = [];
        this.Banners = [];
        this.homeService.displayBanner().then(
            response => {
                this.ShowBannerList = JSON.parse(response);
                this.ShowBanners = this.ShowBannerList.Items;
                this.ShowBanners.forEach(element => {
                    var obj = {
                        Dimensions: {
                            editedImage: element.Dimensions.editedImage
                        },
                        BannerUrl: element.BannerUrl,
                        AdminCriteriaId: {
                            _id: element.AdminCriteriaId._id
                        }
                    }
                    if (element.BannerUrl) {
                        this.Banners.push(obj);
                    }
                });
                this.RandomBannerNumber = Math.floor((Math.random() * this.Banners.length));
            },
            error => this.errorMessage = <any>error);
    }

    getFavIcon(id) {
        this.getFavProductList.forEach((favPdt: any) => {
            if (favPdt.Product.Id == id) {
                return true;
            }
            else {
                return false;
            }
        });

    }
    public Shop() {
        this.featuredShopService.getAll()
            .then(
                featuredShop => {
                    this.FeaturedShopList = JSON.parse(featuredShop);
                    this.FeaturedShops = this.FeaturedShopList;
                },
                error => this.errorMessage = <any>error);
    }
    ShowProductsByShop(shopId) {
        this.router.navigate(['/user/shop', shopId]);
    }

    public productInfo(product) {
        this.router.navigate(['/user/product', product._id]);
        // this.router.navigate(['/user/product', product.product_id, product.Name.substring(0,35).split(' ').join('-')]);
    }
    public productInfofeatured(product) {
        let featuredProductInfo = product.Product;
        let navimodel: NavigationExtras = {
            queryParams: { 'ProductInfo': featuredProductInfo.Id }
        }
        this.router.navigate(['/user/product'], navimodel);
    }
}