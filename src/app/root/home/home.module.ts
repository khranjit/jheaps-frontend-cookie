import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { HomeComponent } from './home.component';
import { Ng2CompleterModule } from "ng2-completer";
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { LazyLoadImageModule,intersectionObserverPreset } from 'ng-lazyload-image';
const home:Routes = [
  { path: '', component: HomeComponent }
]
@NgModule({
  imports: [
    SharedModule,
    Ng2CompleterModule,
    NgbModule,
    RouterModule.forChild(home),
    CarouselModule.forRoot(),
    LazyLoadImageModule.forRoot({
      preset: intersectionObserverPreset
    })
  ],
  declarations:[HomeComponent],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class HomeModule { 
}
