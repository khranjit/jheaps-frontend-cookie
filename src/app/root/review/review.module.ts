import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { ReviewComponent } from './review.component';
import { RouterModule, Routes } from '@angular/router';
import { NgxStarsModule } from 'ngx-stars';
// import { RatingModule } from "ngx-rating";
// import { ShareButtonsModule } from 'ngx-sharebuttons';


const review:Routes = [
  { path: ':id', component: ReviewComponent }
]

@NgModule({
  imports: [
    SharedModule,
    NgxStarsModule,
    RouterModule.forChild(review),
  ],
  declarations: [ 
    ReviewComponent
  ]
})
export class ReviewModule { }