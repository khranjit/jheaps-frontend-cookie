import { Component, HostListener } from '@angular/core';
import { UserService } from '../../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyService } from '../../services/notify.service';
import { ProductReviewService } from '../../services/productreview.service';
import { ProductDescriptionService } from '../../services/product-description.service';


@Component({
  selector: 'review',
  styleUrls: ['review.component.css'],
  templateUrl: './review.component.html',
  providers: [UserService, NotifyService, ProductReviewService, ProductDescriptionService]
})

export class ReviewComponent {
  @HostListener("window:scroll", ["$event"])
  onWindowScroll() {
    let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    let max = document.documentElement.scrollHeight;
    var percentage = Math.round((pos / max) * 100) / 100;
    console.log("percentage " + percentage)
    if (percentage > 0.75) {
      this.callscroll();
    }
  }
  public imageUrls: Array<any>;
  ReviewsList: any;
  Product: any = [];
  Reviews: any = [];
  productId: any;
  public Price = 100;
  totalPages = 0;
  from = 0;
  currentPage = 0;
  pageNumber = 0;
  size = 4;
  total = 0;
  public sort_type: any;
  
  loader1 = false;
  public firstItem:boolean = false;
  public productCurrency: any = { value: '₹' };
  public errorMessage: any;
  
  constructor(private userService: UserService, private productDescriptionService: ProductDescriptionService, private route: ActivatedRoute, private router: Router, private toasterService: NotifyService, private productReviewService: ProductReviewService, ) {
    this.Init();
  }
  public Init() {
    this.route
      .params
      .subscribe(params => {
        if (params['id']) {
          this.productId = params['id'];
          this.Review(this.productId);
          this.productDescriptionService.getProductById(this.productId)
            .then(
              product => {
                this.Product = JSON.parse(product);

                this.imageUrls = this.Product.Media.find(x => x.IsPrimary).EditedImage;
              }
            ),
            (error => this.errorMessage = <any>error);

        }
      });
      this.sort_type ="";
  }
  public Review(productId) {
    this.productReviewService.viewReviewListSort(this.currentPage, this.size, productId,this.sort_type)
      .then(
        reviewProducts => {
          this.ReviewsList = JSON.parse(reviewProducts);
          this.Reviews = this.ReviewsList.Items;
          this.total = this.ReviewsList.TotalItems;
          this.firstItem = true;
        },
        error => this.errorMessage = <any>error);
  }
  async callscroll()
  {
    if(this.firstItem)
    {
      await this.getDataOnScroll();
    }
  }
  getDataOnScroll() {
    if (this.Reviews.length < this.total) {
      this.currentPage += 1;
      this.loader1= true;
         this.productReviewService.viewReviewListSort(this.currentPage, this.size, this.productId,this.sort_type).then(
          response => {
            var data = JSON.parse(response);
            console.log(data);
            this.Reviews = this.Reviews.concat(data.Items);
            this.loader1= false;
            return true;
          }, error => this.errorMessage = <any>error);
    }
    else
    {
      return false;
    }
  }
  public getDataFromQuery() {
    this.ReviewsList = [];
    this.Reviews = [];
    this.currentPage=0;
    this.productReviewService.viewReviewListSort(this.currentPage, this.size, this.productId,this.sort_type)
    .then(
      reviewProducts => {
        this.ReviewsList = JSON.parse(reviewProducts);
        this.Reviews = this.ReviewsList.Items;
        var list=this.ReviewsList;
     
      },
      error => this.errorMessage = <any>error);
  }
}


