import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { ShopReviewComponent } from './shopreview.component';
import { RouterModule, Routes } from '@angular/router';
import { NgxStarsModule } from 'ngx-stars';
// import { RatingModule } from "ngx-rating";

const shop:Routes = [
  { path: ':id', component: ShopReviewComponent }
]

@NgModule({
  imports: [
    SharedModule,
    NgxStarsModule,
    RouterModule.forChild(shop)
  ],
  declarations: [ 
    ShopReviewComponent  
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ShopReviewModule { }