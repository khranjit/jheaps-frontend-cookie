import { Component, HostListener, AfterViewInit, Injector } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { ShopService } from '../../services/shop.service';
import { AttributeService } from '../../services/attribute.service';
import { ElasticHelperService } from '../../services/elastic-helper.service';
import { ElasticService } from '../../services/elastic.service';
import { AddToCartService, TempCartService } from '../../services/cart.service';
import { HomeService } from '../../services/home.service';
import { LoginRedirectService } from '../../services/login.service';
import { ToasterPopService } from '../../services/toasterpop.service';
import { ProductDescriptionService } from '../../services/product-description.service';
import { NgxSpinnerService } from 'ngx-spinner';
declare var jQuery: any;

@Component({

    selector: 'shopreview',
    providers: [ProductService, ShopService, AttributeService, ElasticHelperService, ElasticService, AddToCartService, HomeService, ToasterPopService, ProductDescriptionService],
    templateUrl: './shopreview.component.html'
})

export class ShopReviewComponent implements AfterViewInit {
    @HostListener("window:scroll", ["$event"])
    onWindowScroll() {
      let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
      let max = document.documentElement.scrollHeight;
      var percentage = Math.round((pos / max) * 100) / 100;
      console.log("percentage " + percentage)
      if (percentage > 0.75) {
        this.callscroll();
      }
    }
    app: any;
    toCart: any = {}
    //defaultSearch = "Shop";
    public imageLoader =true;
    SelectedCategory = "";
    public Sections: any;
    public SectionsList: any;
    public Categories: any;
    public CategoriesList: any;
    public Products = [];
    public AllProducts=[];
    public ProductsList: any;
    public Attributes: any;
    public ImageURL= [];
    public AttributesList: any;
    public errorMessage: any;
    public SelectedSection: string;
    public ShopDetails: any;
    public sort_type: any;
    public SelectedAttributes: any;
    public productCurrency: any = { value: "USD" };
    public shopId: any;
    loadtrigger = true;
    ShopUserDetails = <any>{};
    ShopAddressDetails = <any>{};
    ShopBanner: any;
    adminCriteria = <any>{};
    ShopSectionsList: any;
    ShopSections: any;
    totalPages = 0;
    currentPage = 0;
    from = 0;
    size = 4;
    total = 0;
    reviewtotal:any;
    public pages: any[];
    totalShopSection: any;
    filterTags: any;
    shopReviews: any=[];
    reviewChar: number;
    getUserToken: any;
    loadLoginDirectives = true;
    productimage: any;
    loadimage: any;
    loader = false;
    pageLoader = false;
    public firstItem:boolean = false;
    shopOwner:any ={};
    showshoppage=true;
    scrollproduct=false;
    constructor(private route: ActivatedRoute, private loginRedirectService: LoginRedirectService, private loadingservice: NgxSpinnerService, private router: Router, private productshop: ProductService,
        private ShopService: ShopService, private AttributeService: AttributeService, private ESHelperService: ElasticHelperService,
        private ESService: ElasticService, private tempCart: TempCartService, private Inj: Injector, private addToCartService: AddToCartService, private homeService: HomeService, private toasterPopService: ToasterPopService, private productDescriptionService: ProductDescriptionService) {
        this.ShopDetails = <any>{};
        this.SelectedAttributes = [];
        this.getAttributesList();
        this.adminCriteria.Criteria = [];
        this.adminCriteria.Criteria.Price = [];
        this.adminCriteria.Criteria.Price.From = 0;
        this.adminCriteria.Criteria.Price.To = 10000;
        jQuery('#sidebar').removeClass('fix-position');

        this.reviewChar = 200;
        this.route
            .params
            .subscribe(params => {
                this.getShopDetailsFromShopId(params['id']);
                this.getDistinctCategoriesAndCount(params['id']);
                this.getDistinctSectionsAndCount(params['id']);
                this.adminCriteria.Criteria.ShopId = params['id'];
                this.adminCriteria.Criteria.MandatoryFields = [];
                this.adminCriteria.Criteria.MandatoryFields.push('ShopId');
                this.gerDataFromElastic(this.from, this.size);
                this.getShopOwnerDetailsFromShopId(params['id']);
            });
        // this.productimage = document.getElementById('prodimg');
        // this.productimage.hide();
        // this.loadimage = document.getElementById('loadimg');
        // this.loadimage.show();
        window.scrollTo(0, 20);
        this.sort_type = '';
        this.totalShopSection = 0;
        this.getReviews();
        this.loginRedirectService.events$.forEach(event => {
            this.loadLoginDirectives = false;
            setTimeout(() => {
                this.loadLoginDirectives = true;
            }, 1000)

        });
    }

    ngOnInit() {

        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        // this.favService.events$.forEach(event => 
        //     {
        //   //      console.log(event + "From App Component");
        //         this.GetfavouriteProductList();
        //         //this.GetfavouriteProductList();
        //     });

    }


    ngAfterViewInit() {

        this.loginRedirectService.events$.forEach(event => {

        })
        this.validateCriteria("Shop");

    }
    onLoad() {
        this.imageLoader= false;
    }

    public getShopDetailsFromShopId(shopId) {
        this.shopId = shopId;
        this.ShopBanner = [];
        this.ShopService.getShopDetailsFromShopId(shopId)
            .then(
                shop => {
                    this.ShopDetails = JSON.parse(shop).Items[0];
                    this.ShopUserDetails = JSON.parse(shop).Items[0].ShopAddress;
                    // this.ShopAddressDetails = JSON.parse(shop).Items[0].ShopAddress.Address;
                    this.ShopAddressDetails = JSON.parse(shop).Items[0].ShopAddress;

                    this.ShopBanner = this.ShopDetails.Banner;
                },
                error => this.errorMessage = <any>error);
    }
    public getShopOwnerDetailsFromShopId(shopId) {
        this.shopId = shopId;
        this.ShopService.getShopOwnerDetail(shopId)
            .then(
                shopowner => {
                    this.shopOwner = JSON.parse(shopowner);
                    
                },
                error => this.errorMessage = <any>error);
    }

    // public getReviews() {
    //     this.route.params.subscribe(params => {
    //         this.ShopService.getShopReviews(params['id']).then(
    //             reviews => {
    //                 this.shopReviews = JSON.parse(reviews);
                  

    //             },
    //             error => this.errorMessage = <any>error);
    //     },
    //         error => this.errorMessage = <any>error);
    // }
  
    public getReviews() {
        this.route.params.subscribe(params => {
            this.ShopService.getShopReviewPage(this.currentPage,this.size,this.shopId).then(
                reviews => {
                    var data= JSON.parse(reviews);
                    this.total=data.TotalItems;
                    this.shopReviews =data.Items;
                  

                },
                error => this.errorMessage = <any>error);
        },
            error => this.errorMessage = <any>error);
    }
   
    MediaImage(product){
        return product.Media.find(x => x.IsPrimary).EditedImage;

    }

    public getDistinctSectionsAndCount(shopId) {
        this.productshop.getDistinctSectionsAndCountByShopId(shopId)
            .then(
                sections => {
                    this.ShopSectionsList = JSON.parse(sections);
                    this.ShopSections = this.ShopSectionsList.aggregations.langs.buckets;
                    this.ShopService.getShopSectionsByShopId(shopId)
                        .then(
                            sectionList => {
                                let mongoShopSectionsList = JSON.parse(sectionList);
                                let mongoShopSections = mongoShopSectionsList.Items;
                                this.ShopSections.forEach(shopSection => {
                                    this.totalShopSection = this.totalShopSection + shopSection.doc_count;
                                    let filteredSection = mongoShopSections.filter(mongoShopSection => mongoShopSection._id == shopSection.key);
                                    if (filteredSection.length > 0) {
                                        shopSection.Name = filteredSection[0].Name;
                                    }
                                });

                            },
                            error => this.errorMessage = <any>error);
                },
                error => this.errorMessage = <any>error);
    }

    public getDistinctCategoriesAndCount(shopId) {
        this.productshop.getDistinctCategoriesAndCountByShopId(shopId)
            .then(
                product => {
                    this.CategoriesList = JSON.parse(product);
                    this.Categories = this.CategoriesList.aggregations.langs.buckets;
                },
                error => this.errorMessage = <any>error);
    }

    public getProductsBySection(section) {
        this.SelectedSection = section;
        this.adminCriteria = this.ESHelperService.formJsonLikeAdminCriteria(this.adminCriteria, 'ShopSection', section);
        this.getDataFromQuery("ShopSection");
    }

    // previousPage(){
    //     if(this.currentPage > 1){
    //         this.gerDataFromElastic( ((this.currentPage - 2)*this.size), this.size);   
    //     this.currentPage -= 1;
    // }

    // }

    // nextPage(){
    //     if(this.currentPage < this.totalPages){
    //         this.gerDataFromElastic( ((this.currentPage )*this.size), this.size);
    //     this.currentPage += 1;
    // }

    // }

    // selectPage(PageNumber) {
    //     this.currentPage = PageNumber;
    //     this.gerDataFromElastic( ((this.currentPage - 1)*this.size), this.size);
    // }

    public checkColorSelected(name, value) {
        if (!this.SelectedAttributes[name]) {
            return '';
        }
        if (this.SelectedAttributes[name].indexOf(value.toLowerCase()) > -1) {
            return 'selected';
        }
        return '';
    }

    public attributeFiltering(name, attributeValue) {
        let fieldName = "Attributes-" + name;
        attributeValue = attributeValue.toLowerCase()
        this.validateCriteria(name);
        if (!this.SelectedAttributes[name]) {
            this.SelectedAttributes[name] = [];
        }
        if (this.SelectedAttributes[name].indexOf(attributeValue) > -1) {
            this.SelectedAttributes[name].splice(this.SelectedAttributes[name].indexOf(attributeValue), 1);
        } else {
            this.SelectedAttributes[name].push(attributeValue);
        }
        if (this.adminCriteria.Criteria.MandatoryFields.indexOf(fieldName) == -1) {
            this.adminCriteria.Criteria.MandatoryFields.push(fieldName);
        }
        if (this.SelectedAttributes[name].length == 0) {
            this.adminCriteria.Criteria.MandatoryFields.splice(this.adminCriteria.Criteria.MandatoryFields.indexOf(fieldName), 1);
            delete this.adminCriteria.Criteria[name];
        }
        this.adminCriteria = this.ESHelperService.formJsonLikeAdminCriteria(this.adminCriteria, name, this.SelectedAttributes[name]);
        this.getDataFromQuery(fieldName);
    }

    public validateCriteria(name) {
        if (!this.adminCriteria.Criteria.MandatoryFields) {
            this.adminCriteria.Criteria.MandatoryFields = [];
        }
        this.adminCriteria.Criteria.Sort = this.sort_type;
    }

    public getDataFromQuery(name) {

        this.validateCriteria(name);
        if (this.adminCriteria.Criteria.MandatoryFields.indexOf(name) == -1) {
            this.adminCriteria.Criteria.MandatoryFields.push(name);
        }
        this.updateFilterTags();
        // this.ResetPagination();
        // this.gerDataFromElastic(this.from, this.size);
    }

    public updateFilterTags() {
        this.filterTags = [];
        this.adminCriteria.Criteria.MandatoryFields.forEach(field => {
            if (field != 'Shop' && field != 'ShopId' && field != 'Categories' && field != 'ShopSection' && field != 'Price' && field != 'Sort' && field != 'Search') {
                this.adminCriteria.Criteria[field.split("-")[1]].forEach(value => {
                    if (field.split("-")[1] == "IsNew") {
                        let isnew = value == "true" ? "New" : "Used";
                        let obj = { "filter": field, "value": isnew };
                        this.filterTags.push(obj);
                    } else {
                        let obj = { "filter": field, "value": value };
                        this.filterTags.push(obj);
                    }
                });
            } else {
                if (field == 'ShopSection') {
                    let obj = { "filter": field, "value": this.adminCriteria.Criteria[field].Name };
                    this.filterTags.push(obj);
                } else if (field == 'Sort') {
                } else if (field == 'Categories') {
                    let obj = { "filter": field, "value": this.adminCriteria.Criteria[field] };
                    this.filterTags.push(obj);
                }
                else if (field == 'ShopId') {
                }
                else if (field == 'Search') {
                }
                else if (field == 'Price') {
                    let obj = { "filter": field, "value": "Price " + this.adminCriteria.Criteria[field].From + ' - ' + this.adminCriteria.Criteria[field].To };
                    this.filterTags.push(obj);
                }
                else {
                    this.adminCriteria.Criteria[field].forEach(value => {
                        let obj = { "filter": field, "value": value };
                        this.filterTags.push(obj);
                    });
                }
            }
        });
    }

    public removeFilterTags(tag, index) {
        if (this.filterTags[index].filter != "Price" && this.filterTags[index].filter != "ShopSection" && this.filterTags[index].filter != "Categories") {
            let element = this.filterTags[index].filter.split("-")[1].split(' ').join('_') + "-" + this.filterTags[index].value;
            jQuery("#" + element.toLowerCase()).attr('checked', false);
            this.filterTags.splice(index, 1);
            this.attributeFiltering(tag.filter.split("-")[1], tag.value);
        }
        else {

            this.filterTags.splice(index, 1);
            var index = this.adminCriteria.Criteria.MandatoryFields.indexOf(tag.filter);
            this.adminCriteria.Criteria.MandatoryFields.splice(index, 1);
            // this.ResetPagination();
            // this.gerDataFromElastic(this.from, this.size);

        }
    }

    LoadOnScroll() {
        var scrollContainer = document.getElementById('scroll-container');
        var scrollBox = document.getElementById('scroll-box');
        var scrollContainerPos = scrollContainer.getBoundingClientRect();
        var scrollBoxPos = scrollBox.getBoundingClientRect();
        var scrollLength = (scrollBoxPos.height - scrollContainerPos.height);

        if (scrollLength - scrollContainer.scrollTop < 1) {
            setTimeout(() => {
                // this.gerDataFromElasticOnScroll();
            }, 500)
        }
    }

    // ResetPagination() {
    //     this.from = 0;
    //     this.total = 0;
    //     jQuery('#scroll-container').css("height", 500);
    // }

    ValidateScrollContainer() {
        setTimeout(() => {
            var scrollContainer = document.getElementById('scroll-container');
            var scrollBox = document.getElementById('scroll-box');
        if(scrollContainer!=null && scrollBox!=null){
            var scrollContainerPos = scrollContainer.getBoundingClientRect();
            var scrollBoxPos = scrollBox.getBoundingClientRect();
            if (scrollBoxPos.height < scrollContainerPos.height) {
                jQuery('#scroll-container').css("height", scrollBoxPos.height);
            }
        }
        }, 1000)
    }

    public gerDataFromElastic(from, size) {
        let query;
        query = this.ESHelperService.getQueryForShopCriteria(this.adminCriteria);
        // this.loadingservice.show();
        this.pageLoader = true;
        this.ESService.getRecordsByQuery(query, from, size)
            .then(
                product => {
                    this.ProductsList = JSON.parse(product);
                    this.Products=this.ProductsList.hits.hits;
                    // this.AllProducts = this.ProductsList.hits.hits;
                    // this.Products=this.AllProducts.slice(0,8);

                    // this.imageLoader=false;
                    // this.total = JSON.parse(product).hits.total;
                    this.firstItem=true;
                    this.pageLoader = false;
                    // this.loadingservice.hide();
                    this.ValidateScrollContainer();
                },
                error => this.errorMessage = <any>error);
    }
    async callscroll() {
        if (this.firstItem) {
            await  this.getDataOnScroll();
        }
    }
    getDataOnScroll() {
        if (this.shopReviews.length < this.total) {
          this.currentPage += 1;
          this.loader= true;
          this.ShopService.getShopReviewPage(this.currentPage, this.size,this.shopId).then(
            reviews => {
                var data=JSON.parse(reviews);
                
                this.shopReviews = this.shopReviews.concat(data.Items);
               
                this.loader= false;

            },
            error => this.errorMessage = <any>error);
            
        }
        else
        {
          return false;
        }
      }
     
    // public gerDataFromElasticOnScroll() {
        
        
    //     this.from += this.size;
    //     if (this.from < this.total && this.loadtrigger) {
    //         this.loadtrigger=false
    //         console.log("Loading");
    //         let query = this.ESHelperService.getQueryForShopCriteria(this.adminCriteria);
           
    //         this.loader = true;
    //         this.ESService.getRecordsByQuery(query, this.from, this.size)
    //             .then(
    //                 product => {
    //                     this.ProductsList = JSON.parse(product);
    //                     this.total = this.ProductsList.hits.total;
                      
    //                     this.loader = false;

    //                     this.Products = this.Products.concat(this.ProductsList.hits.hits);
    //                     this.loadtrigger=true;
                      
                        
    //                 },
    //                 error => this.errorMessage = <any>error);
    //     }
    

    // }


    public productInfo(productId) {
        this.router.navigate(['/user/product', productId]);
    }

 

    getProductsByCategoryId(CategoryId) {
        this.SelectedCategory = CategoryId;
        if (this.adminCriteria.Criteria.MandatoryFields.indexOf("Categories") == -1) {
            this.adminCriteria.Criteria.MandatoryFields.push("Categories");
        }
        this.adminCriteria.Criteria.Categories = this.SelectedCategory;
        this.updateFilterTags();
    //     this.ResetPagination();
    //     this.gerDataFromElastic(this.from, this.size);
    //
 }

    public getAttributesList() {
        this.AttributeService.getAll()
            .then(
                attributes => {
                    this.AttributesList = JSON.parse(attributes);
                    this.Attributes = this.AttributesList.Items;
                },
                error => this.errorMessage = <any>error);
    }

    getProducts() {
        let page = 0;
        this.productshop.getAll(page)
            .then(
                product => {
                    this.ProductsList = JSON.parse(product);
                    this.Products = this.ProductsList.Items;
                    // this.imageLoader=false;
                },
                error => this.errorMessage = <any>error);
    }
    viewAllItem()
    {
        this.router.navigate(['/user/home']);
    }

}
