import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { ShopComponent } from './shop.component';
import { RouterModule, Routes } from '@angular/router';
import { NgxStarsModule } from 'ngx-stars';
// import { RatingModule } from "ngx-rating";

const shop:Routes = [
  { path: ':id', component: ShopComponent }
]

@NgModule({
  imports: [
    SharedModule,
    NgxStarsModule,
    RouterModule.forChild(shop)
  ],
  declarations: [ 
    ShopComponent  
  ],
  schemas:[
    NO_ERRORS_SCHEMA
  ]
})
export class ShopModule { }