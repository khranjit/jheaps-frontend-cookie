import { Component, OnInit, Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CheckoutService } from '../../services/checkout.service';
// import { AppComponent } from '../../app.component';

@Component({
    
    selector: 'success',
    providers: [CheckoutService],
    templateUrl: './success.component.html'
})

export class SuccessComponent {

    errorMessage: any;
    paymentStatusInfoItems=[];
    public productCurrency: any = { value: "₹" };
    groupId:any;
    Total = <any>{
        TotalCost:0,
        ProductCost:0,
        ShippingCost:0,
        CouponDiscount:0
    };
    ShippingAddress:any ={};
    request: any;
    totalCost = 0;
    dataReady = false;
    public app: any;
    
    constructor(private route: ActivatedRoute, private router: Router, private checkoutService: CheckoutService,private Inj: Injector) {
        // this.orderStatus = {orderStatus:"success"};
        this.Init();
     }

    getShopkey(item) {
        //console.log(Object.keys(item))
        return Object.keys(item);
    }
    public MediaImage(Product)
    {
        if(Product.hasOwnProperty("Variation"))
        {
            if(Product.Variation.Image != "")
                return Product.Variation.Image;
            else
                return Product.Media.find(x=>x.IsPrimary).EditedImage;
            
        }
        else
        {
            return Product.Media.find(x=>x.IsPrimary).EditedImage;
        }
    }
    public Init() {
        this.route.params.subscribe(params => {
            // this.app = this.Inj.get(AppComponent);
            // this.app.cart();
            this.groupId = params['groupId']; 
            this.request = {
                groupId: this.groupId,
                orderStatus: "success"
            }

            this.checkoutService.CheckPaymentStatus(this.request).then(
                response => {
                    this.paymentStatusInfoItems = JSON.parse(response);
                    if(this.paymentStatusInfoItems.length>0)
                    {
                        this.ShippingAddress = this.paymentStatusInfoItems[0].ShippingAddress;
                        this.paymentStatusInfoItems.forEach(ele =>{
                            this.Total.TotalCost += ele.Totals.TotalCost;
                            this.Total.ProductCost += ele.Totals.ProductCost;
                            this.Total.ShippingCost += ele.Totals.ShippingCost;
                            this.Total.CouponDiscount += ele.Totals.CouponDiscount;
                        });
                    }
                    this.dataReady = true;
                    //console.log("Payment Status  : ", this.paymentStatusInfo)
                   
                }, error => this.errorMessage = <any>error);
           
          }
        );
        
    }

    public productInfo(product) {
        this.router.navigate(['/user/product', product]);  
    }

    redirectToHome()
    {
       this.router.navigate(['']); 
        }

}