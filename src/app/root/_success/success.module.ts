import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SuccessComponent } from './success.component';
import { SharedModule } from '../../shared/shared.module';

  
const success: Routes = [
    {path: ':groupId',component:SuccessComponent}
]
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(success)
  ],
  declarations: [
    SuccessComponent
  ],
})
export class SuccessModule { }