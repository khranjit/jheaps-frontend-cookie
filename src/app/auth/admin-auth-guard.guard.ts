import { Injectable } from '@angular/core';
import { Router, CanActivate , ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
declare var jQuery: any;

@Injectable()

export class AuthGuardAdmin implements CanActivate {
  constructor(private router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean 
    {
        if (localStorage.getItem('userToken') || localStorage.getItem('role') == 'masteradmin' || localStorage.getItem('role') == 'admin') 
        {
          return true;
        }
      this.router.navigate(['']);
      return false;
    }
}


