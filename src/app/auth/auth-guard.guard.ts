import { Injectable } from '@angular/core';
import { Router, CanActivate , ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
declare var jQuery: any;

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean 
    {
      if (localStorage.getItem('userToken')) 
      {
        return true;
      }
      this.router.navigate(['']);
      jQuery("#loginmodal").modal("show");
      return false;
    }

  canLoad()
  {
    if (localStorage.getItem('role') == 'seller') 
    {
      return true;
    }
    this.router.navigate(['']);
    return false;
  }
}


