import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import { Router } from '@angular/router';
import * as express from 'express';
import * as q from 'q';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class ShippingProfileService {
    private shippingprofiles: any;

    constructor(private http: Http) { }

    addShippingProfile(shippingProfileModel): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = shippingProfileModel;
        let response;
//        console.log(body);
        return this.http.post(config.apiUrl+'/api/shipping-profile', body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    viewShippingProfile() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/shipping-profile', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    checkShippingProfile(item){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = {"shippingProfileId":item};
      //  console.log('working', body);
        let response;
        return this.http.post(config.apiUrl+'/api/shipping-profile/checkshipping', body,{ headers: headers })
         .toPromise()
          .then(this.extractData)
          .catch(this.handleError);
    }

    removeShippingProfile(id): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        return this.http.delete(config.apiUrl+'/api/shipping-profile/' + id, { headers: headers })
            .toPromise()
            .then(this.extractDataStatus)
            .catch(this.handleError);
    }

    putShippingProfile(shippingProfileModel): any {
//         let headers = new Headers();
//         headers.append('Content-Type', 'application/json');
//         headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
//         let body = shippingProfileModel;
//   //      console.log('working', body);
//         let response;
//         this.http.post(config.apiUrl+'/api/shipping-profile/', body, { headers: headers })
//             .subscribe(
//             data => response = data, //For Success Response
//             err => { 
//             //    console.error(err)
//              }, //For Error Response
//             () => { 
//             //    console.log("Updated"); 
//             }
//             );

            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
            let body = shippingProfileModel;
            let response;
    //        console.log(body);
            return this.http.post(config.apiUrl+'/api/shipping-profile', body, { headers: headers })
                .toPromise()
                .then(this.extractData)
                .catch(this.handleError);
    }

    deleteShippingProfile(ShippingProfile): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        this.http.delete(config.apiUrl+'/api/shipping-profile/' + ShippingProfile._id, { headers: headers })
            .subscribe(
            data => response = data, //For Success Response
            err => {
                // console.error(err) 
            }, //For Error Response
            () => { 
                //console.log("Deleted");
             }
            );
    }
    addShippingAddress(ShippingAddress): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        //console.log('working', ShippingAddress);
        let response;
        return this.http.post(config.apiUrl+'/api/shipping-address', ShippingAddress, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    putShippingAddress(shippingAddressModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = shippingAddressModel;
       // console.log('working', body);
        let response;
        return this.http.put(config.apiUrl+'/api/shipping-address/' + shippingAddressModel._id, body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
 
    }
    showShippingAddress(): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        return this.http.get(config.apiUrl+'/api/shipping-address',{ headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    removeShippingAddress(deleteAddress) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.delete(config.apiUrl+'/api/shipping-address/'+deleteAddress, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    changeProfileAsPrimary(model){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        //console.log('working', model);
        let response;
        return this.http.post(config.apiUrl+'/api/shipping-address/profile', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);        
    }
    
    changePrimaryAddress(id){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        return this.http.get('api/shipping-address/primary/'+id,{ headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    getAll(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));

        return this.http.get(config.apiUrl+'/api/shipping-profile', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let shippingprofilesList = res.json();
        return JSON.stringify(shippingprofilesList);
    }

    private extractDataStatus(res: Response) {
        return res.status;
    }

    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        //console.error(errMsg);
        return Promise.reject(errMsg);
    }

}