import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class ElasticService {
    
    constructor(private http: Http) { }

    getSearchSuggestions(searchTerm): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        // let query = {
        //               "query": {
        //                     "bool": {
        //                       "must": [{
        //                             "wildcard": {
        //                              "_all" : {
        //                                     "value" : "*"+searchTerm.toLowerCase()+"*",
        //                                     "boost" : 2.0
        //                              }
        //                           }
        //                        }]
        //                   }
        //                 }
        //             };
        var query = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "query_string": {
                                "default_field":"Name",
                                "query": "*"+searchTerm.toLowerCase()+"*"

                            }
                        }
                    ]
                }
            }

        }
        //  console.log(" getSearchSuggestions " + JSON.stringify(query));           
        return this.http.post('/elastic/'+config.elasticUrl+'/_search', query, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    getShopSearchSuggestions(searchTerm): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        var query = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "query_string": {
                                "default_field":"displayName",
                                "query": "*"+searchTerm.toLowerCase()+"*"

                            }
                        }
                    ]
                }
            }

        }
        return this.http.post('/elastic/jheaps.shopsearches/_search', query, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    

    getSearchSuggestionsWithPaging(searchTerm, from, size): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let query = {
                      "query": {
                            "bool": {
                              "must": [{
                                "query_string": {
                                    "query": "*"+searchTerm.toLowerCase()+"*"
        
                                }
                               }]
                          }
                        }
                    };
         console.log(" getSearchSuggestions " + JSON.stringify(query));           
        return this.http.post('/elastic/'+config.elasticUrl+'/_search?from='+from+'&size='+size, query, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }



    getProductByShopId(searchTerm, shopId, from?, size?,isTopProduct?): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

            let query;
                    if(isTopProduct != null)
                    {
                        if(isTopProduct == true)
                        {
                            query = {
                                "query": {
                                      "bool": {
                                        "must": [{
                                            "query_string": {
                                                "query": "*"+searchTerm.toLowerCase()+"*"
                    
                                            }
                                         },
                                         {
                                           "match_phrase": {
                                               "ShopId" :  shopId
                                            }
                                         },
                                         {
                                          "term": {
                                              "isTopProduct" :  isTopProduct
                                           }
                                        }]
                                    }
                                  }
                              };
                        }

                        else
                        {
                            query= {
                                "query": {
                                    "bool": {
                                        "must": [
                                            {
                                                "query_string": {
                                                    "query": "*"+searchTerm.toLowerCase()+"*"
                        
                                                }
                                           },
                                            {
                                                "match_phrase": {
                                                    "ShopId": shopId
                                                }
                                            },
                                            {
                                                "bool": {
                                                    "should": [
                                                        {
                                                            "bool": {
                                                                "must_not": {
                                                                    "exists": {
                                                                        "field": "isTopProduct"
                                                                    }
                                                                }
                                                            }
                                                        },
                                                        {
                                                            "term": {
                                                                "isTopProduct": false
                                                            }
                                                        }
                                                    ]
                                                }
                                            }
                                        ]
                                    }
                                }
                            }
                        }
                        
                    }
                    else{
                         query = {
                            "query": {
                                  "bool": {
                                    "must": [{
                                        "query_string": {
                                            "query": "*"+searchTerm.toLowerCase()+"*"
                
                                        }
                                     },
                                     {
                                       "match_phrase": {
                                           "ShopId" :  shopId
                                        }
                                     }]
                                }
                              }
                          };
                    }
                    console.log(" getProductByShopId " + JSON.stringify(query));
                    if(size)
                    {
                        return this.http.post('/elastic/'+config.elasticUrl+'/_search?from='+from+'&size='+size, query, { headers: headers })
                        .toPromise()
                        .then(this.extractDataGet)
                        .catch(this.handleError);
                    }
        return this.http.post('/elastic/'+config.elasticUrl+'?size=10000', query, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    updateProductById(productId,value): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

            let query = {
                
                    "doc" : {
                        "isTopProduct" : value
                    }
                
            };
                               
        return this.http.post('/elastic/'+config.elasticUrl+'/_doc/'+productId+'/_update', query, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    getProductByShopIdWithSorting(searchTerm, shopId, from, size, sort, isAsc, data): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let sortQuery;
        if(sort == "product"){
            if(isAsc['product']){
                sortQuery = [{ "Name" : {"order" : "asc"}}];    
            }else{ 
                sortQuery = [{ "Name" : {"order" : "desc"}}]; 
            }               
        }else if(sort == "qty"){
            if(isAsc['qty']){
                sortQuery = [{ "QuantityAvailable" : {"order" : "asc"}}];    
            }else{ 
                sortQuery = [{ "QuantityAvailable" : {"order" : "desc"}}]; 
            }    
        }else if(sort == "date"){
            if(isAsc['date']){
                sortQuery = [{ "CreatedDate" : {"order" : "asc"}}];    
            }else{ 
                sortQuery = [{ "CreatedDate" : {"order" : "desc"}}]; 
            }      
        }
        let query = <any>{
                      "query": {
                            "bool": {
                              "must": [{
                                "query_string": {
                                    "query": "*"+searchTerm.toLowerCase()+"*"
        
                                }
                               },
                               {
                                 "match_phrase": {
                                     "ShopId" :  shopId
                          }
                        }
                            ]
                          }
                        }
                    };

                    var queryData=query.query.bool.must;
                    if(data!="Active")
                    {
                        if(data=="Qty")
                        {
                            var pushData = 
                            {
                                "bool": {
                                        "should": [
                                                {"bool": {"must":[{"term":{"useDefaultQuantity":false}},{"range":{"VariationQuantityAvailable":{"lte": 0 }}}]}},
                                                {"bool": {"must":[{"term":{"useDefaultQuantity":true}},{"range":{"QuantityAvailable":{"lte": 0 }}}]}}
                                                ]
                                        }
                            };
                            queryData.push(pushData);
                        }
                        else
                        {
                            var pushData1 = 
                            {
                                "match_phrase": 
                                    {
                                        "Show" :  data
                                    }
                            };
                          if(searchTerm == "")
                          queryData.splice(0,1);
                          queryData.push(pushData1);
                        }
                        
                    }
        query.sort = sortQuery;
        console.log(" getProductByShopIdWithSorting " + JSON.stringify(query));
        return this.http.post('/elastic/'+config.elasticUrl+'/_search?from='+from+'&size='+size, query, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    getRecordsByQuery(query, from, size): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        console.log(" getRecordsByQuery " + JSON.stringify(query));
        return this.http.post('/elastic/'+config.elasticUrl+'/_search?from='+from+'&size='+size, query, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
     getOtherProductByShopIdWithSorting(searchTerm, shopId, from, size, sort, isAsc, Attr): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let sortQuery;
        if(sort == "product"){
            if(isAsc['product']){
                sortQuery = [{ "Name" : {"order" : "asc"}}];    
            }else{ 
                sortQuery = [{ "Name" : {"order" : "desc"}}]; 
            }               
        }else if(sort == "qty"){
            if(isAsc['qty']){
                sortQuery = [{ "QuantityAvailable" : {"order" : "asc"}}];    
            }else{ 
                sortQuery = [{ "QuantityAvailable" : {"order" : "desc"}}]; 
            }    
        }else if(sort == "date"){
            if(isAsc['date']){
                sortQuery = [{ "CreatedDate" : {"order" : "asc"}}];    
            }else{ 
                sortQuery = [{ "CreatedDate" : {"order" : "desc"}}]; 
            }      
        }
        let query = <any>{
                      "query": {
                            "bool": {
                              "must": [{
                                "query_string": {
                                    "query": "*"+searchTerm.toLowerCase()+"*"
        
                                }
                               },
                               {
                                 "match_phrase": {
                                     "Shop.id" :  shopId
                                  }
                              }]
                          }
                        }
                    };
        query.sort = sortQuery;
        console.log(" getOtherProductByShopIdWithSorting " + JSON.stringify(query));
        return this.http.post('/elastic/'+config.elasticUrl+'/_search?from='+from+'&size='+size+Attr, query, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    private extractDataGet(res: Response) {
        let responseData = res.json();
        return JSON.stringify(responseData);
    }
    private extractData(res: Response) {
        return res.status;
    }
    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
       // console.error(errMsg);
        return Promise.reject(errMsg);
    }

}


