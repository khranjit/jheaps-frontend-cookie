import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
const config = require('../../../config/app-config.json');
@Injectable()
export class MessageService {

    constructor(private http: Http) { }

    /*getInboxMessages(pagenumber): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/message/inbox?offset='+pagenumber, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }*/

    getInboxMessages(from, size): Promise<any> {
        let headers = new Headers();
        let body = { query: "inbox" };
        //  console.log("BODY", body);
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/message?offset=' + from + '&limit=' + size, body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    /*getOutboxMessages(pagenumber): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/message/sentbox?offset='+pagenumber,{ headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }*/

    getOutboxMessages(from, size): Promise<any> {
        console.log('Service +' + from);
        let headers = new Headers();
        let body = { query: "sentbox" }
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/message?offset=' + from + '&limit=' + size, body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getTrashMessages(from, size): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/message/trash?offset=' + from + '&limit=' + size, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    moveToTrashMessages(res): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/message/move/trash/' + res, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    sendMail(Mail): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = Mail;
        let response;
        //console.log(body);
        return this.http.post('api/message/send', body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    replyMail(Mail): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = Mail;
        let response;
        //console.log(body);
        return this.http.post('api/message/reply', body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    adminResponse(message): Promise<any>{
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = message;
        let response;
        //console.log(body);
        return this.http.post('api/ordertickets/adminresponse', body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    deleteMessages(deleteItems): Promise<any> {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = deleteItems;
        let response;
        //console.log(body);
        return this.http.post('api/message/multi-delete', body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getSellerTickets(TicketId): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        var model ={ 
            TicketId: TicketId
        };
        //console.log(body);
        return this.http.post('api/ordertickets/get',model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    replyMessage(replyModel){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/ordertickets/post', replyModel, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    replyForAdmin(replyModel){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/admintickets/post', replyModel, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getBuyerTickets(searchTerm, offset, limit): any{
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        //console.log(body);
        return this.http.get('api/buyertickets/get', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);

    }
    viewBuyerTickets(TicketId): any{
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        var model ={ 
            TicketId: TicketId
        };
        //console.log(body);
        return this.http.post(config.apiUrl+'/api/ordertickets/buyerticket',model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);

    }

    getAdminTickets(searchTerm, offset, limit): any{
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        //console.log(body);
        return this.http.get('api/admintickets/get', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);

    }

    addTicket(Mail): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = { "Tickets": Mail };
        let response;
        //console.log(body);
        return this.http.post('api/seller-care', body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    closeTicket(ticket): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        //console.log(body);
        return this.http.put('api/seller-care/close', ticket, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    reopenTicket(ticket): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put('api/seller-care/reopen', ticket, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    getUserTickets(searchTerm, offset, limit): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/seller-care?offset=' + offset + '&limit=' + limit + '&searchterm=' + searchTerm + '&sort=-1', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    getUserReplies(message): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/seller-care/userreply?Ticketid=' + message._id, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    replyTicket(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/seller-care/reply', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    public getmessageList(from?, size?) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        if (size) {
            return this.http.get(config.apiUrl+'/api/messagelist/?from=' + from + '&limit=' + size, { headers: headers })
                .toPromise()
                .then(this.extractData)
                .catch(this.handleError);
        }
        return this.http.get(config.apiUrl+'/api/messagelist', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let responseData = res.json();
        return JSON.stringify(responseData);
    }

    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        //  console.error(errMsg);
        return Promise.reject(errMsg);
    }

}

