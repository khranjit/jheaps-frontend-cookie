import { Injectable } from '@angular/core';
import { SuccessSnackComponent,ErrorSnackComponent } from '../snack/snack.component';
import {MatSnackBar} from '@angular/material';
const config = require('../../../config/app-config.json');
@Injectable()
export class NotifyService {

  constructor(public snackBar: MatSnackBar) {}

  SuccessSnack(message) {
    this.snackBar.openFromComponent(SuccessSnackComponent, {
      duration: 5000,
      data: message
    });
  }

  ErrorSnack(message) {
    this.snackBar.openFromComponent(ErrorSnackComponent, {
      duration: 5000,
      data: message
    });
  }
}
