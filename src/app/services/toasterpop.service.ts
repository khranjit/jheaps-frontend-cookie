import { Injectable } from '@angular/core';
import * as express from 'express';
import { ToasterService } from 'angular2-toaster';

import * as q from 'q';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()                                                                                                                                                                                                   
export class ToasterPopService{
	constructor(private toasterService:ToasterService){  }
    
    popSuccess(title:string, content:string){
         this.toasterService.pop('success', title, content);
    }
    
    popAlert(title:string, content:string){
         this.toasterService.pop('error', title, content);
    }
}
