import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import { Router } from '@angular/router';
import * as express from 'express';
import * as q from 'q';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class SellOnZgrooService {
    constructor(private http: Http) { }
    
    // IFSCcodeToBankDetails(IFSC_code): any {
    //     return this.http.get('https://api.techm.co.in/api/v1/ifsc/' + IFSC_code).toPromise()
    //         .then(this.extractDataGet)
    // }
    IFSCcodeToBankDetails(IFSC_code): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let options = new RequestOptions({ headers: headers });
        let body = IFSC_code;
        return this.http.post(config.apiUrl+'/api/auth/ifsccode', {IFSC:IFSC_code})
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
       
    }
    BankSearch(): any {
        return this.http.get('https://api.techm.co.in/api/listbanks').toPromise()
            .then(this.extractDataGet)
    }
    StateSearch(value): any {
        return this.http.get('https://api.techm.co.in/api/listbranches/'+value).toPromise()
            .then(this.extractDataGet)
    }
    DistrictList(state): any {
       // console.log(state);
        return this.http.get('https://api.techm.co.in/api/state/'+state).toPromise()
            .then(this.extractDataGet)
    }
    private extractDataGet(res: Response) {
        return res.json();
    }
    private extractData(res: Response) {
        let loginInfo = res.json();
        if (res.status == 200) {
            return JSON.stringify(loginInfo);
        }
        else {
            let err = {
                "Token": "NULL",
                "TimeToExpireInDays": 300,
                "Activated": false
            }
            return JSON.stringify(err);
        }

    }

    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        //console.error(errMsg);
        return Promise.reject(errMsg);
    }

}
