import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import {Router} from '@angular/router';
import * as express from 'express';
import * as q from 'q';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()                                                                                                                                                                                                   
export class TaxpayerService{
   private products : any;

   constructor(private http: Http){ }
getAll() : Promise<any> {
     
     
       let taxpayersList;
         return this.http.get(config.apiUrl+'/api/tax-rate')
                .toPromise()
                .then(this.extractData)
                .catch(this.handleError);
     }

  private extractData(res: Response) {
       let taxpayersList= res.json();
        return JSON.stringify(taxpayersList);
     }

  private handleError (error: Response | any) {
  
       let errMsg: string;
          if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
           } 
          else {
            errMsg = error.message ? error.message : error.toString();
           }
     //     console.error(errMsg);
          return Promise.reject(errMsg);
          }
   
}