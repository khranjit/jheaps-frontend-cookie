import { Injectable } from '@angular/core'
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import { Router } from '@angular/router';
import * as express from 'express';
import * as q from 'q';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class PhoneService {

  constructor(private http: Http) { }

  getData(): Promise<any> {
    return this.http.get(config.apiUrl+'/api/verify')
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  sendVerificationCode(profileViewModel: ViewModels.IProfileViewModel): Promise<any> {
   // console.log("Profile View Model ", profileViewModel);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    return this.http.post(config.apiUrl+'/api/authy/user/register', profileViewModel, { headers: headers })
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  verifyToken(): Promise<any> {
    return this.http.get(config.apiUrl+'/api/verify')
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let accountKitData = res.json();
    return JSON.stringify(accountKitData);
  }

  private handleError(error: Response | any) {

    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    }
    else {
      errMsg = error.message ? error.message : error.toString();
    }
    //console.error(errMsg);
    return Promise.reject(errMsg);
  }

}