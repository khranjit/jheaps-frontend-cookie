import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import {Router} from '@angular/router';
import * as express from 'express';
import * as q from 'q';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()                                                                                                                                                                                                   
export class PreferenceService{
	constructor(private http: Http){ }
   	
   savePreference(preferenceModel): Promise<any> {
       let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let body = preferenceModel;
    let response;
//    console.log(body);
    return this.http.post('api/preferences', body,{ headers: headers })
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  showPreference(): Promise<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    return this.http.get(config.apiUrl+'/api/preferences',{ headers: headers })
      .toPromise()
      .then(this.extractDataGet)
      .catch(this.handleError);
  }
  editPreference(preferenceModel): Promise<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let body = preferenceModel;
  //  console.log('working', preferenceModel);
    let response;
    return this.http.put(config.apiUrl+'/api/preferences/'+preferenceModel._id, body,{ headers: headers })
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
        return res.status;
  }
  private extractDataGet(res: Response) {
    let preference = res.json();
    return JSON.stringify(preference);
  }
  private handleError(error: Response | any) {

    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    }
    else {
      errMsg = error.message ? error.message : error.toString();
    }
    //console.error(errMsg);
    return Promise.reject(errMsg);
  }
  }
