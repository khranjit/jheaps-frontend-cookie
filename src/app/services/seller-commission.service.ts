import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import {Router} from '@angular/router';
import * as express from 'express';
import * as q from 'q';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class SellerCommissionService {
    private products: any;

    constructor(private http: Http) { }
    setAll(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/defaultcommission', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getDefaultCommission(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/defaultcommission', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    viewAll(from,size,userName): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = { "userName": userName }
  //      console.log(body);
        return this.http.post(config.apiUrl+'/api/commission/view/?offset='+from+'&limit='+size, body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
//     viewShopbyModel(id): Promise<any> {
//         let headers = new Headers();
//         headers.append('Content-Type', 'application/json');
//         headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
//         let body = { "Value": id }
//   //      console.log(body);
//         return this.http.post(config.apiUrl+'/api/commission/view', body, { headers: headers })
//             .toPromise()
//             .then(this.extractData)
//             .catch(this.handleError);
//     }
    updateCommissionbyShop(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/commission', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    updatePlan(objid, planid, model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = { "CommissionId": objid, "PlanId": planid, "Plan": model }

        return this.http.put(config.apiUrl+'/api/commission/', body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    deleteCommission(objid, planid): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.delete(config.apiUrl+'/api/commission/' + objid + '/' + planid, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    private extractData(res: Response) {
        let taxpayersList = res.json();
        return JSON.stringify(taxpayersList);
    }

    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
    //    console.error(errMsg);
        return Promise.reject(errMsg);
    }

}