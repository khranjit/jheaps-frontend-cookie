import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
const config = require('../../../config/app-config.json');

@Injectable()
export class ForumService {

    constructor(private http: Http) { }

    createForumGroup(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/forumgrouptitle', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

   viewForumGroup(from?,size?): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        if(size)
        {
            return this.http.get(config.apiUrl+'/api/forumgrouptitle/?limit='+from+'&limit='+size, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
        }
        return this.http.get(config.apiUrl+'/api/forumgrouptitle', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    getGroupDetails(id): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/forumgrouptitle/'+id, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    ownThreads(pagenumber): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/forumcontent/own-threads?offset='+pagenumber,  { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    viewAllStatus(pagenumber): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/forumcontent/status?offset='+pagenumber, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
     MyMarkedThreads(pagenumber): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/forumcontent/mark?offset='+pagenumber, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    viewAllForumGroupPost(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/forumcontent', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    viewAllForumGroupPostUserBased(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/forumcontent/user-based', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    viewSelectedGroupAuth(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/forumcontent/group', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
     viewSelectedGroupUnAuth(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        //headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/forumcontent/group/unauth', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    StateWiseView(pageNumber,model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        //headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/forumcontent/status?offset='+pageNumber, model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    viewSelectedPostAuth(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/forumcontent/post', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    viewSelectedPostUnAuth(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/forumcontent/post/unauth', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    updateForumGroup(id, model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/forumgrouptitle/'+id, model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    deleteForumGroup(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.delete(config.apiUrl+'/api/forumgrouptitle/'+model._id, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
     forum(): Promise<any> {
        // console.log(user.message);
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = "";
        let response;
        /*return this.http.post(config.apiUrl+'/api/forum/post/login', body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);*/
        return this.http.post(config.apiUrl+'/api/forum/login', body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

   postForumContent(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/forumcontent', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    postForumResponse(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/forumcontent/response', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    postForumResponseComment(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/forumcontent/reply', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    setStatusAdmin(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/forumcontent/status', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
     setAdminThreadStatus(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/forumcontent/open-close', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
     DeleteThread(groupId,postId): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        //console.log("delete content" , model);
        return this.http.delete(config.apiUrl+'/api/forumcontent/'+groupId+'/'+postId, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    DeleteResponse(groupId,postId,responseId): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        //console.log("delete content" , model);
        return this.http.delete(config.apiUrl+'/api/forumcontent/'+groupId+'/'+postId+'/'+responseId, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    DeleteReply(groupId,postId,responseId,replyId): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        //console.log("delete content" , model);
        return this.http.delete(config.apiUrl+'/api/forumcontent/'+groupId+'/'+postId+'/'+responseId+'/'+replyId, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
     /*DeleteThread(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        console.log("delete content" , model);
        return this.http.delete(config.apiUrl+'/api/forumcontent/post',model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }*/
    
    markThreads(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/forumcontent/mark', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    pinThreads(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/forumcontent/pin', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    likeThread(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/forumcontent/like', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
      
   forumSearch(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        //headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/forumcontent/search', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    private extractData(res: Response) {
        let forumInfo = res.json();
        return JSON.stringify(forumInfo);
    }

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
       // console.error(errMsg);
        return Promise.reject(errMsg);
    }

}