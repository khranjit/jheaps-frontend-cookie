import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import { Router } from '@angular/router';
import * as express from 'express';
import * as q from 'q';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { any } from 'nconf';
const config = require('../../../config/app-config.json');
@Injectable()
export class HomeService {

    constructor(private http: Http) { }

    displayBanner(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/banner', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    viewFavouriteProduct(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/favorite-product', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getThreeColumnsData(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/threecolumn', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    updateColumnData(columnInfo): any {
     //   console.log(columnInfo);
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/threecolumn/' + columnInfo._id, columnInfo, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    saveColumnData(columnInfo): any {
       // console.log(columnInfo);
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/threecolumn', columnInfo, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    addFavouriteProduct(productModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/favorite-product/' + productModel, {}, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    checkFavouriteProduct(productModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/favorite-product/check/' + productModel, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    viewFavouriteShop(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/shop-favorite', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    addFavouriteShop(shopModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/shop-favorite/add/'+ shopModel, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    checkFavouriteShopStatus(shopModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/shop-favorite/check/'+ shopModel, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let home = res.json();
       // console.log("from home service",JSON.stringify(home) )
        return JSON.stringify(home);
    }

    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        //console.error(errMsg);
        return Promise.reject(errMsg);
    }
}

// export class FavService{
//     private _subject = new Subject<any>();
    
//       newEvent(event) {
//         this._subject.next(event);
//       }
    
//       get events$ () {
//         return this._subject.asObservable();
//       }
// }