import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');

@Injectable()
export class ApproveSellerService {

    invokeEvent: Subject<any> = new Subject();
    private awsImageModel: ViewModels.IAwsImageModel;

    constructor(private http: Http) { }

    public getSellerWaitingList(pageNumber,SearchTerm): any  {
        let headers = new Headers();
        var model={SearchTerm:SearchTerm};
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/auth/un-approve-seller?offset='+pageNumber+'&limit=10',model, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    public getincompleteList(offset,limit,search){
        let headers = new Headers();
        // var model={SearchTerm:SearchTerm};
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/auth/getincompletelist?offset='+offset+'&limit='+limit+'&search='+search, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    public getEditBusinessList(offset,limit,search){
        let headers = new Headers();
        // var model={SearchTerm:SearchTerm};
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/auth/geteditbusinesslist?offset='+offset+'&limit='+limit+'&search='+search, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    getSellerDetails(id) : any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body={"ShopId" :id }
        return this.http.post(config.apiUrl+'/api/shop-account',body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    approveSeller(model): any  {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/auth/approve-seller',model, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    approveUpdatedSeller(model): any  {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/shop-account/approve-details',model, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    getDocumentUpdatedSellerList(pageNumber,SearchTerm): any  {
        let headers = new Headers();
        var model={SearchTerm:SearchTerm};
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/shop-account/updated-list?offset='+pageNumber+'&limit=10',model, { headers: headers })
        .toPromise()
        .then(this.extractDataGet)
        .catch(this.handleError);
    }

    approveUpdatedPhone(model): any  {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/shop-account/approve-phone',model, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    getDocumentUpdatedSellerDetails(id): any  {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body={"ShopId" :id }
        return this.http.get(config.apiUrl+'/api/shop-account/view/'+id, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    searchShopUnApprove(model): any  {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/-seller/search',model, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    searchShopApprove(model): any  {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/shop-account/search',model, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        return res.status;

    }
    private extractDataGet(res: Response) {
        let responseData = res.json();
        return JSON.stringify(responseData);

    }

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Promise.reject(errMsg);
    }

}