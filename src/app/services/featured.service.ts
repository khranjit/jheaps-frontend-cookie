import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import { Router } from '@angular/router';
import * as express from 'express';
import * as q from 'q';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class FeaturedProductService {
    private featuredproduct: any;

    constructor(private http: Http) { }
    getProducts(productModel): any {

        let response;
        this.http.get(config.apiUrl+'/api/featured-product/' + productModel._id)
            .subscribe(
            data => response = data, //For Success Response
            err => { 
           //     console.error(err) 
            }, //For Error Response
            () => { 
                // console.log(JSON.parse(response)); 
            }
            );
    }
    
    setFeaturedProduct(featuredproduct): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = featuredproduct;
        return this.http.post(config.apiUrl+'/api/featured-product/', body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    deleteFeaturedProduct(featuredShop): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        return this.http.delete(config.apiUrl+'/api/featured-shop/' + featuredShop._id, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    getAll(): Promise<any> {
        let featuredproductsList;
        return this.http.get(config.apiUrl+'/api/featured-product')
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }


    private extractDataGet(res: Response) {
        let featuredproductsList = res.json();
        return JSON.stringify(featuredproductsList);

    }
    private extractData(res: Response) {
        return res.status;
    }

    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
//        console.error(errMsg);
        return Promise.reject(errMsg);
    }





}