import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
const config = require('../../../config/app-config.json');

@Injectable()
export class FeedbackService {

    constructor(private http: Http) { }

    sendFeedback(idea,feed) : Promise<any> {
        
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = {"Title":idea,"Feedback": feed};
        let response;
        return this.http.post(config.apiUrl+'/api/feedback', body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

     getFeedback(pageNumber,sort) : Promise<any> {
        
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        return this.http.get(config.apiUrl+'/api/feedback?offset='+pageNumber+'&&sort='+sort,  { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
      getOwnFeedback(pageNumber,sort) : Promise<any> {
        
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        return this.http.get(config.apiUrl+'/api/feedback/own?offset='+pageNumber+'&&sort='+sort,  { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
      getStatusFeedback(pageNumber,sort,status) : Promise<any> {
        
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
       // headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        let body={"Status": status};
//        console.log(body);
        return this.http.post(config.apiUrl+'/api/feedback/status?offset='+pageNumber+'&&sort='+sort,body,  { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    putComment(model) : Promise<any> {
        
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        return this.http.put(config.apiUrl+'/api/feedback/comment',model,  { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
     countVote(model) : Promise<any> {
        
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        return this.http.put(config.apiUrl+'/api/feedback/vote',model,  { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    updateStatus(model) : Promise<any> {
        
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        return this.http.put(config.apiUrl+'/api/feedback/status',model,  { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
     deleteFeedback(id) : Promise<any> {
        
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        return this.http.delete(config.apiUrl+'/api/feedback/'+id, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    deleteComment(feedbackId, commentId) : Promise<any> {
        
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        return this.http.delete(config.apiUrl+'/api/feedback/comment/'+feedbackId+'/'+commentId, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
        
    private extractData(res: Response) {
        let forumInfo = res.json();
        return JSON.stringify(forumInfo);
    }

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
  //      console.error(errMsg);
        return Promise.reject(errMsg);
    }

}