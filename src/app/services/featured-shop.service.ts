import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import { Router } from '@angular/router';
import * as express from 'express';
import * as q from 'q';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class FeaturedShopService {
    private featuredShop: any;

    constructor(private http: Http) { }
    getSingle(productModel): any {
        let body = productModel;
      //  console.log('working', body);
        let response;
        this.http.get(config.apiUrl+'/api/featured-shop/' + productModel._id, body)
            .subscribe(
            data => response = data, //For Success Response
            err => { console.error(err) }, //For Error Response
            () => { console.log(JSON.parse(response)); }
            );
    }
    
    deleteFeaturedShop(featuredShop): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        return this.http.delete(config.apiUrl+'/api/featuredshop/' + featuredShop, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
  addFeaturedShop(model): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        return this.http.post(config.apiUrl+'/api/featuredshop' ,model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    searchShop(model, from?, size?): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        if(size)
        {
            return this.http.post(config.apiUrl+'/api/shop/search/?from='+from+'&size='+size ,model, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
        }
        return this.http.post(config.apiUrl+'/api/shop/search' ,model, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    getAll(): Promise<any> {
        let featuredshopList;
        return this.http.get(config.apiUrl+'/api/featuredshop')
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    private extractDataGet(res: Response) {
        let featuredshopList = res.json();
        return JSON.stringify(featuredshopList);
    }
    
    private extractData(res: Response) {
        return res.status;
    }

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        //console.error(errMsg);
        return Promise.reject(errMsg);
    }

}

