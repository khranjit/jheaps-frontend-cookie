import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import {Router} from '@angular/router';
import * as express from 'express';
import * as q from 'q';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class CreditDebitNoteService {
    private products: any;

    constructor(private http: Http) { }
    setAll(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/CreditDebitNote', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    update(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = model	
        return this.http.put(config.apiUrl+'/api/EditCreditDebitNote',model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
   
    delete(deleteModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        return this.http.delete(config.apiUrl+'/api/DeleteCreditDebitNote/'+deleteModel._id,{ headers: headers })
          .toPromise()
          .then(this.extractData)
          .catch(this.handleError);
     }
     searchTerm(searchTerm): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/searchTermForshop?searchterm=' + searchTerm, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getAll(search,from?, size?): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/ViewNote?offset=' + from + '&limit=' + size + '&searchterm=' + search, { headers: headers })
          .toPromise()
          .then(this.extractDataGet)
          .catch(this.handleError);
      }
      

      private extractDataGet(res: Response) {
        let CreditList = res.json();
       return JSON.stringify(CreditList);
         
     }

   
    private extractData(res: Response) {
        let taxpayersList = res.json();
        return JSON.stringify(taxpayersList);
    }

    private handleError(error: Response | any) {

        // let errMsg: string;
        // if (error instanceof Response) {
        //     const body = error.json() || '';
        //     const err = body.error || JSON.stringify(body);
        //     errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        // }

        // else {
        //     errMsg = error.message ? error.message : error.toString();
        // }
        return Promise.reject(error);
    }

}
