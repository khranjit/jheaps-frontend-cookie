import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
const config = require('../../../config/app-config.json');

@Injectable()
export class ActivateService {

    constructor(private http: Http) { }

    activateToken(token: String): Promise<any> {
        console.log("token "+token)
        return this.http.post(config.apiUrl+'/api/auth/activate?token=' + token, '')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let isActivated = res.json();
        return isActivated;
    }

    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
//        console.error(errMsg);
        return Promise.reject(errMsg);
    }

}

