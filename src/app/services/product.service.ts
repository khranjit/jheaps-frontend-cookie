import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class ProductService {
    private products: any;
    invokeEvent: Subject<any> = new Subject();
    private awsImageModel: ViewModels.IAwsImageModel;

    constructor(private http: Http, private location: Location) { }
    
    setProduct(productModel): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = productModel;
       // console.log('working', productModel);
        let response;
        return this.http.post(config.apiUrl+'/api/product', body, { headers: headers })
              .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleErrorforproduct);
    }

    putProduct(productModel):  Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/product/' + productModel._id, productModel, { headers: headers })
              .toPromise()
            .then(this.extractData)
            .catch(this.handleErrorforproduct);
    }

    deleteProduct(ProductID): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        return this.http.delete(config.apiUrl+'/api/product/' + ProductID, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getProduct(productid): Promise<any> {
        let productsList;
        return this.http.get(config.apiUrl+'/api/product/' + productid)
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    getProductById(id): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/product/'+id, { headers: headers })
           .toPromise()
           .then(this.extractDataGet)
           .catch(this.handleError);
       
    }
    
    getProductsByShop(shopId, sort_type): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body;
        if(sort_type == "default-sorting"){
            body = {
                "from" : 0, "size" : 12,
                "query": {
                    "bool": {
                      "must": [
                        {
                              "match_phrase": { "ShopId": shopId }
                        }
                      ]
                    }
                 }
            };
        }else if(sort_type == "price-low-to-high"){
            body = {
                "from" : 0, "size" : 12,
                "query": {
                    "match_phrase": { "ShopId": shopId }
                },
                "sort" : [
                    { "Price" : {"order" : "asc"}}
                ]
            };
        }else if(sort_type == "price-high-to-low"){
            body = {
                "from" : 0, "size" : 12,
                "query": {
                    "match_phrase": { "ShopId": shopId }
                },
                "sort" : [
                    { "Price" : {"order" : "desc"}} 
                ]
            };
        }else if(sort_type == "rating"){
            body = {
                "from" : 0, "size" : 12,
                "query": {
                    "match_phrase": { "ShopId": shopId }
                },
                "sort" : [
                    { "Rating" : {"order" : "asc"}} 
                ]
            };
        }

        let response;
        return this.http.post('/elastic/'+config.elasticUrl+'/_search', body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    getAll(page): Promise<any> {
        let productsList;
        return this.http.get(config.apiUrl+'/api/product?offset=' + page)
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    getSpecialBestAccessoriesTopProducts() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = {
            "query": {
                "bool": {
                    "should": [
                        { "match_phrase": { "ColumnOne": "true" } },
                        { "match_phrase": { "ColumnTwo": "true" } },
                        { "match_phrase": { "ColumnThree": "true" } },
                        { "match_phrase": { "ColumnFour": "true" } }
                    ]
                }
            }
        };

        let response;
        return this.http.post('/elastic/'+config.elasticUrl+'/_search?size=200', body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    getProductsByCategoryId(CategoriesId) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = {
            "query": {
                "bool": {
                  "must": [
                    {
                          "match_phrase": { "Categories.Name": CategoriesId }
                    }
                  ]
                }
             }
        };

        let response;
        return this.http.post('/elastic/'+config.elasticUrl+'/_search', body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    searchProduct(searchTerm) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = {
            "query": {
                "bool": {
                  "must": [
                    {
                          "match": {
                                "_all": searchTerm
                          }
                    }
                  ]
                }
             }
        };

        let response;
        return this.http.post('/elastic/'+config.elasticUrl+'/_search', body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    getDistinctCategoriesAndCount() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = {
            "size": 0,
            "aggs": {
                "langs": {
                    "terms": { "field": "Categories.Name"}
                },
                "categoryId":{

                    "terms":{"field": "Categories.Id"}
                }
            }
        }

        let response;
        return this.http.post('/elastic/'+config.elasticUrl+'/_search', body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

   
    
    getDistinctCategoriesAndCountByShopId(shopId) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = {
            "size": 0,
            "query": {
                "bool": {
                  "must": [
                    {
                        "match_phrase": {
                            "ShopId": shopId
                        }
                    }
                  ]
                }
            },
            "aggs": {
                "langs": {
                    "terms": { "field": "Categories.Name"}
                },
                "categoryId":{

                    "terms":{"field": "Categories.Id"}
                }
            }
        }

        let response;
        return this.http.post('/elastic/'+config.elasticUrl+'/_search', body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    getDistinctSectionsAndCountByShopId(shopId) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = {
            "size": 0,
            "query": {
                "bool": {
                  "must": [
                    {
                        "match_phrase": {
                            "ShopId": shopId
                        }
                    }
                  ]
               }
            },
            "aggs": {
                "langs": {
                    "terms": { "field": "ShopSection.Id"}
                }
            }
        }

        let response;
        return this.http.post('/elastic/'+config.elasticUrl+'/_search', body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    OverallReview(productid){
        // let productsList;
        return this.http.get(config.apiUrl+'/api/product/overall/' + productid)
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);    
    }

    getRecommendedProducts(productObj) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let LowerPrice = productObj.Price - (productObj.Price * 0.2); 
        let HigherPrice = productObj.Price + (productObj.Price * 0.5);
        let body = {
            "query": {
                "bool": {
                    "must": [
                        {
                           "match": { "Categories.Name": productObj.Categories[0].Name }
                        },
                        {
                            "bool":

                        {  "should": [
                                        {
                                         "range":
                                         {
                                         "Price": { "gte": LowerPrice, "lte": HigherPrice }
                                         }
                                        },
                                        {  
                                         "range": 
                                         {
                                         "Variations.Price": { "gte": LowerPrice, "lte": HigherPrice }
                                         }
                                        }
                                    ]
                         }
                        }  
                    ],
                
                    "must_not": [
                        {
                           "match": { "_id": productObj._id }
                        }
                    ],
                   
                }
            }
            // "sort": [
            //     { "Sales.LifeToTime": { "order": "desc" } }
            // ]
        }
 
        let response;
        return this.http.post('/elastic/'+config.elasticUrl+'/_search?size=6', body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    updateColumnMapping(ColumnMappingModel) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = ColumnMappingModel;
        let response;
        return this.http.post(config.apiUrl+'/api/admincriteria/update', body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    getSpecialOfferTitles(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));

        return this.http.get(config.apiUrl+'/api/admincriteria', { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    getDraftProducts(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));

        return this.http.get(config.apiUrl+'/api/products/draft', { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    getInactiveProducts(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));

        return this.http.get(config.apiUrl+'/api/products/inactive', { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    getSoldOutProducts(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));

        return this.http.get(config.apiUrl+'/api/products/soldout', { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    addSpecialOfferTitles(body): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));

        return this.http.post(config.apiUrl+'/api/admincriteria', body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    updateSpecialOfferTitles(id, body): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));

        return this.http.post(config.apiUrl+'/api/admincriteria/', body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    updateproduct(id, body): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));

        return this.http.post(config.apiUrl+'/api/product/update/'+id, body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    resetAdminCriteriaByColumnName(body): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));

        return this.http.put(config.apiUrl+'/api/admincriteria/reset', body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    convertToBase64AndUpload(file): any {
        //console.log("file", file);
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.awsImageModel = <ViewModels.IAwsImageModel>{};
        var myReader: FileReader = new FileReader();
        myReader.readAsDataURL(file);
        let url;
        let response;
        myReader.onloadend = (e) => {
            this.awsImageModel.Name = file.name.split(".")[0];
          //  console.log(this.awsImageModel.Name);
            this.awsImageModel.ImageData = myReader.result.toString();
            this.http.post(config.apiUrl+'/api/aws/image/upload', this.awsImageModel, { headers: headers })
                .subscribe(
                data => response = data.json(), //For Success Response
                err => { 
                //    console.error(err)
                 }, //For Error Response
                () => {
                    url = JSON.stringify(response);
                  //  console.log("URL "+url);
                    this.invokeEvent.next(url);
                }
                );
        }
    }

    private extractDataGet(res: Response) {
        let productsList = res.json();
        return JSON.stringify(productsList);
    }
    private extractData(res: Response) {
        return res.status;
    }
    private handleErrorforproduct(error: Response | any) {

        let errMsg: string;
        if(error.status == 500){
           errMsg = error._body.toString() 
        }
        else {
            if (error instanceof Response) {
                const body = error.json() || '';
                const err = body.error || JSON.stringify(body);
                errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
            }
            else {
                errMsg = error.message ? error.message : error.toString();
            }
        }
       
    //    console.error(errMsg);
        return Promise.reject(errMsg);
    }
    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
    //    console.error(errMsg);
        return Promise.reject(errMsg);
    }

}

