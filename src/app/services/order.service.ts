import { Injectable } from '@angular/core';

import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import {Router} from '@angular/router';
import * as express from 'express';
import * as q from 'q';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class OrderService {
    private orders: any;

    constructor(private http: Http) { }

    getAll(): Promise<any> {
          let ordersList;
        return this.http.get(config.apiUrl+'/api/order')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
  
    viewOrder(pageNumber,size,sort, model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/orderdetails' + "?offset=" + pageNumber + "&sort="+sort+"&limit="+size, model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
  // 
    cancelOrderBuyer(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/order/cancel/buyer', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    cancelOrderSeller(id, model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/order/cancel/seller/' + id, model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    returnOrder(model): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/order/product-return', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
  
    setOrderStatus(model) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/order/deliver', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
   
    getDeliveredOrder(offset,limit) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/order/deliver?offset='+offset+'&sort=1&limit='+limit, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    Buyerpuchase(offset,limit) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/order/purchase?offset='+offset+'&limit='+limit, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    getReadyShipOrder(offset,limit) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/order/pickup-scheduled?offset='+offset+'&sort=1&limit='+limit, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    getReturnOrderListforSeller(offset,limit,sort,search) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/order/product-return?offset='+offset+'&sort='+sort+'&limit='+limit+'&Search='+search, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
   
    ReturnOrderStatus(model) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
 //       console.log(model);
        return this.http.post(config.apiUrl+'/api/order/return-response', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    mailToAdmin(model){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
   //     console.log(model);
        return this.http.post(config.apiUrl+'/api/order/return-response', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    ReturnOrderReplace(model){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
     //   console.log(model);
        return this.http.post(config.apiUrl+'/api/order/return-response', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    productReview(model) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/product/review', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    addPackageDetails(model) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
       // console.log(model);
        return this.http.post(config.apiUrl+'/api/order-detail', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    updatePackageDetails(model) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/updateorder-detail', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    updateBankDetails(model) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/order/updateBankDetails', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getPackageDetails(id) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/order-detail/'+id, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    showReturnedOrder(model)
    {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/show-returnorder',model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
   
     shippingLabel(model) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/zepo/shipping-lable', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    PrintInvoice(model) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/zepo/printinvoice', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    PrintReturnInvoice(model) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/zepo/printreturninvoice', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    donloadShippingLabel(model) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/zepo/downloadlable', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }  
    printOrders(model) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/order/print', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
      
    replaceToSeller(model) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/order/replacetoseller', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
   
    private extractData(res: Response) {
        let ordersList = res.json();
        return JSON.stringify(ordersList);
    }

    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        //console.error(errMsg);
        return Promise.reject(errMsg);
    }

}
