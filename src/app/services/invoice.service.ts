import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
const config = require('../../../config/app-config.json');

@Injectable()
export class InvoiceService {
   constructor(private http: Http) { }

   getInvoice(): Promise<any> {
       let headers = new Headers();
       headers.append('Content-Type', 'application/json');
       headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
       return this.http.get(config.apiUrl+'/api/invoice', { headers: headers })
           .toPromise()
           .then(this.extractData)
           .catch(this.handleError);
   }
   GetInvoiceDownload(): Promise<any> {
       let headers = new Headers();
       headers.append('Content-Type', 'application/json');
       headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
       return this.http.get(config.apiUrl+'/api/invoice/excel-download', { headers: headers })
           .toPromise()
           .then(this.extractData)
           .catch(this.handleError);
   }

   getInvoiceAdmin(): Promise<any> {
       let headers = new Headers();
       headers.append('Content-Type', 'application/json');
       headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
       return this.http.get(config.apiUrl+'/api/invoice/admin', { headers: headers })
           .toPromise()
           .then(this.extractData)
           .catch(this.handleError);
   }
 
   private extractData(res: Response) {
       let home = res.json();
       // console.log("from home service",JSON.stringify(home) )
       return JSON.stringify(home);
   }

   private handleError(error: Response | any) {

       let errMsg: string;
       if (error instanceof Response) {
           const body = error.json() || '';
           const err = body.error || JSON.stringify(body);
           errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
       }
       else {
           errMsg = error.message ? error.message : error.toString();
       }
       //console.error(errMsg);
       return Promise.reject(errMsg);
   }
}
