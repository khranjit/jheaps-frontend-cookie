import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class ElasticHelperService {

    constructor(private http: Http) { }

    public formJsonLikeAdminCriteria(adminCriteria, fieldName, values) {
        if (values) {
            adminCriteria.Criteria[fieldName] = values;
        }
        return adminCriteria;
    }

    getQueryForActualAdminCriteria(criteriaModel) {
        var filterMust = [];
        if (criteriaModel.Criteria.Condition == "New") {
            var termQuery = {
                "term": {
                    "IsNew": true
                }

            };
            filterMust.push(termQuery);
        }
        else {
            var termQuery = {
                "term": {
                    "IsNew": false
                }

            };
            filterMust.push(termQuery);
        }

        if (criteriaModel.Criteria.hasOwnProperty('SubCategories')) {
            if (criteriaModel.Criteria.SubCategories.length > 0) {
                var SubCategoriesQuery = [];
                criteriaModel.Criteria.SubCategories.forEach(ele => {
                    var termQuery1 = {
                        "match_phrase": {
                            "Categories.SubCategories._id": ele
                        }
                    };
                    SubCategoriesQuery.push(termQuery1);
                }

                );
                var mustSubQuery = {
                    "bool": {
                        "should": SubCategoriesQuery
                    }

                };

                filterMust.push(mustSubQuery);
            }
        }
        //}

        if (criteriaModel.Criteria.Categories && criteriaModel.Criteria.Categories.length > 0) {
            var CategoriesQuery = [];
            criteriaModel.Criteria.Categories.forEach(ele => {
                var termQuery2 = {
                    "match_phrase": {
                        "Categories.Id": ele
                    }
                };
                CategoriesQuery.push(termQuery2);
            }

            );
            var mustSubQuery1 = {
                "bool": {
                    "should": CategoriesQuery
                }

            };

            filterMust.push(mustSubQuery1);

        }

        if (criteriaModel.Criteria.SellerRating) {
            var termQuery3 = {
                "range":
                {
                    "Rating": {
                        "gte": criteriaModel.Criteria.SellerRating,
                        "lte": 5
                    }
                }
            };

            filterMust.push(termQuery3);

        }

        // if(criteriaModel.Criteria.ShipsFrom && criteriaModel.Criteria.ShipsFrom.length > 0)
        // {
        //     var termQuery4 = {
        //         "match_phrase" : {
        //             "ShipsFrom" :  criteriaModel.Criteria.ShipsFrom
        //         }

        //     };
        //     filterMust.push(termQuery4);
        // }

        // if(criteriaModel.Criteria.Price )
        // {
        //     let from = criteriaModel.Criteria.Price.From;
        //     let to = criteriaModel.Criteria.Price.To;
        //     if(from > to){
        //         from = criteriaModel.Criteria.Price.To;
        //         to = criteriaModel.Criteria.Price.From;      
        //     }
        //     var termQuery5 = { "range": 
        //                         { "FirstVariationPrice":{ 
        //                             "gte":  from,
        //                             "lte":  to
        //                             }
        //                     }
        //                 };

        //     filterMust.push(termQuery5)          
        // }

        var filterShould = [];
        if (criteriaModel.Criteria.Shop && criteriaModel.Criteria.Shop.length > 0) {

            criteriaModel.Criteria.Shop.forEach(ele => {
                var termQuery6 = {
                    "match_phrase": {
                        "ShopId": ele
                    }
                };
                filterShould.push(termQuery6);
            }

            );
        }


        let criteriaMandatoryFields = criteriaModel.Criteria.MandatoryFields;

        if (criteriaMandatoryFields.length > 0) {
            //var MandatoryFieldsQuery = [];
            criteriaMandatoryFields.forEach(mandatoryField => {
                let mustMatch = this.getTermQueryByFieldName(criteriaModel, mandatoryField);
                if (mustMatch) {
                    filterMust.push(mustMatch);
                }
            });


        }
        let query = <any>{
            "query": {
                "constant_score": {
                    "filter": {
                        "bool": {
                            "should": filterShould,
                            "must": filterMust
                        }
                    }
                }
            }
        }

        if (criteriaModel.Criteria.Sort) {
            query.sort = this.addSortToQuery(criteriaModel, 'Sort');
        }
        // console.log(JSON.stringify(query))


        return query;

        // else{

        // }
        //  console.log(criteriaModel.Criteria.MandatoryFields);
        //   let mustMatchQueries = [];
        //   let shouldMatchQueries = [];
        //   let sortQuery;
        // console.log(Object.keys(criteriaModel.Criteria));

        //   criteriaMandatoryFields.forEach(mandatoryField => {
        //       let mustMatch = this.getTermQueryByFieldName(criteriaModel, mandatoryField);
        //       if(mustMatch){
        //          mustMatchQueries.push(mustMatch); 
        //       }            
        //   });

        //   Object.keys(criteriaModel.Criteria).forEach(field => {
        //       if(criteriaMandatoryFields.indexOf("Attributes-"+field) >= 0
        //           || criteriaMandatoryFields.indexOf(field) >= 0){
        //           return false;        
        //       }  
        //       let shouldMatch = this.getTermQueryByFieldName(criteriaModel, field);
        //       if(shouldMatch){ 
        //           shouldMatchQueries.push(shouldMatch);
        //       }
        //   });


        //
        //console.log(mustMatchQueries);
        //console.log(shouldMatchQueries);
        //   let query = <any>{
        //                 "query": {
        //                   "bool": {
        //                     "must": mustMatchQueries,
        //                     "should": shouldMatchQueries
        //                   }
        //                 }
        //               };

    }

    public getQueryForAdminCriteriaCategory(criteriaModel) {
        //    console.log(criteriaModel.Criteria);
        let criteriaMandatoryFields = criteriaModel.Criteria.MandatoryFields;
        //  console.log(criteriaModel.Criteria.MandatoryFields);
        let mustMatchQueries = [];
        let shouldMatchQueries = [];
        let sortQuery;
        // console.log(Object.keys(criteriaModel.Criteria));

        criteriaMandatoryFields.forEach(mandatoryField => {
            let mustMatch = this.getTermQueryByFieldName(criteriaModel, mandatoryField);
            if (mustMatch) {
                mustMatchQueries.push(mustMatch);
            }
        });

        // Object.keys(criteriaModel.Criteria).forEach(field => {
        //     if(criteriaMandatoryFields.indexOf("Attributes-"+field) >= 0
        //         || criteriaMandatoryFields.indexOf(field) >= 0){
        //         return false;        
        //     }  
        //     let shouldMatch = this.getTermQueryByFieldName(criteriaModel, field);
        //     if(shouldMatch){ 
        //         shouldMatchQueries.push(shouldMatch);
        //     }
        // });


        //
        //console.log(mustMatchQueries);
        //console.log(shouldMatchQueries);
        let query = <any>{
            "query": {
                "bool": {
                    "must": mustMatchQueries,
                    "should": shouldMatchQueries
                }
            }
        };
        if (criteriaModel.Criteria.Sort) {
            query.sort = this.addSortToQuery(criteriaModel, 'Sort');
        }
        // console.log(JSON.stringify(query))
        return query;
    }

    public getQueryForShopCriteria(criteriaModel) {
        //    console.log(criteriaModel.Criteria);
        let criteriaMandatoryFields = criteriaModel.Criteria.MandatoryFields;
        //  console.log(criteriaModel.Criteria.MandatoryFields);
        let mustMatchQueries = [];
        let shouldMatchQueries = [];
        let sortQuery;
        // console.log(Object.keys(criteriaModel.Criteria));

        if (criteriaModel.Criteria.hasOwnProperty("category") && criteriaModel.firstLoad) {
            if (criteriaModel.Criteria.category.Id) {
                var queryForCategory = {
                    "match_phrase": {
                        "Categories.Id": criteriaModel.Criteria.category.Id
                    }
                };
                mustMatchQueries.push(queryForCategory);
            }
            if (criteriaModel.Criteria.category.subCategory) {
                var queryForSubcategory = {
                    "match_phrase": {
                        "Categories.SubCategories._id": criteriaModel.Criteria.category.subCategory
                    }
                };
                mustMatchQueries.push(queryForSubcategory);
            }
        }
        criteriaMandatoryFields.forEach(mandatoryField => {
            let mustMatch = this.getTermQueryByFieldName(criteriaModel, mandatoryField);
            if (mustMatch) {
                mustMatchQueries.push(mustMatch);
            }
        });

        // Object.keys(criteriaModel.Criteria).forEach(field => {
        //     if(criteriaMandatoryFields.indexOf("Attributes-"+field) >= 0
        //         || criteriaMandatoryFields.indexOf(field) >= 0){
        //         return false;        
        //     }  
        //     let shouldMatch = this.getTermQueryByFieldName(criteriaModel, field);
        //     if(shouldMatch){ 
        //         shouldMatchQueries.push(shouldMatch);
        //     }
        // });


        //
        //console.log(mustMatchQueries);
        //console.log(shouldMatchQueries);
        let query = <any>{
            "query": {
                "bool": {
                    "must": mustMatchQueries,
                    "should": shouldMatchQueries
                }
            }
        };
        if (criteriaModel.Criteria.Sort) {
            query.sort = this.addSortToQuery(criteriaModel, 'Sort');
        }
        // console.log(JSON.stringify(query))
        return query;
    }



    public getQueryForAdminCriteria(criteriaModel) {
        //    console.log(criteriaModel.Criteria);
        let criteriaMandatoryFields = criteriaModel.Criteria.MandatoryFields;
        //  console.log(criteriaModel.Criteria.MandatoryFields);
        let mustMatchQueries = [];
        let shouldMatchQueries = [];
        let sortQuery;
        // console.log(Object.keys(criteriaModel.Criteria));
        if (criteriaModel.Criteria.hasOwnProperty('SubCategories')) {
            if (criteriaModel.Criteria.SubCategories.length > 0) {
                var SubCategoriesQuery = [];
                criteriaModel.Criteria.SubCategories.forEach(ele => {
                    var termQuery1 = {
                        "match_phrase": {
                            "Categories.SubCategories._id": ele
                        }
                    };
                    SubCategoriesQuery.push(termQuery1);
                }
                );
                var mustSubQuery = {
                    "bool": {
                        "should": SubCategoriesQuery
                    }

                };

                mustMatchQueries.push(mustSubQuery);
            }
        }
        //}

        if (criteriaModel.Criteria.Categories && criteriaModel.Criteria.Categories.length > 0) {
            var CategoriesQuery = [];
            criteriaModel.Criteria.Categories.forEach(ele => {
                var termQuery2 = {
                    "match_phrase": {
                        "Categories.Id": ele
                    }
                };
                CategoriesQuery.push(termQuery2);
            }

            );
            var mustSubQuery1 = {
                "bool": {
                    "should": CategoriesQuery
                }

            };

            mustMatchQueries.push(mustSubQuery1);

        }

        criteriaMandatoryFields.forEach(mandatoryField => {
            let mustMatch = this.getTermQueryByFieldName(criteriaModel, mandatoryField);
            if (mustMatch) {
                mustMatchQueries.push(mustMatch);
            }
        });

        // Object.keys(criteriaModel.Criteria).forEach(field => {
        //     if(criteriaMandatoryFields.indexOf("Attributes-"+field) >= 0
        //         || criteriaMandatoryFields.indexOf(field) >= 0){
        //         return false;        
        //     }  
        //     let shouldMatch = this.getTermQueryByFieldName(criteriaModel, field);
        //     if(shouldMatch){ 
        //         shouldMatchQueries.push(shouldMatch);
        //     }
        // });


        //
        //console.log(mustMatchQueries);
        //console.log(shouldMatchQueries);
        let query = <any>{
            "query": {
                "bool": {
                    "must": mustMatchQueries,
                    "should": shouldMatchQueries
                }
            }
        };
        if (criteriaModel.Criteria.Sort) {
            query.sort = this.addSortToQuery(criteriaModel, 'Sort');
        }
        // console.log(JSON.stringify(query))
        return query;
    }

    private addSortToQuery(criteriaModel, fieldName) {
        let sortQuery = <any>{};
        if (criteriaModel.Criteria[fieldName] == 'price-low-to-high') {
            sortQuery = {
                "sort": [
                    { "Price": { "order": "asc" } }
                ]
            };
        } else if (criteriaModel.Criteria[fieldName] == 'price-high-to-low') {
            sortQuery = {
                "sort": [
                    { "Price": { "order": "desc" } }
                ]
            };
        } else if (criteriaModel.Criteria[fieldName] == 'rating') {
            sortQuery = {
                "sort": [
                    { "Rating": { "order": "desc" } }
                ]
            };
        } else if (criteriaModel.Criteria[fieldName] == 'latest') {
            sortQuery = {
                "sort": [
                    { "CreatedDate": { "order": "desc" } }
                ]
            };
        }
        return sortQuery.sort;
    }

    private getTermQueryByFieldName(criteriaModel, fieldName) {
        let termQuery;
        if (criteriaModel.Criteria[fieldName] || fieldName.split("-")[0] == "Attributes") {
            if (fieldName == "Discount" || fieldName == "Query" || fieldName == "MandatoryFields" || fieldName == "Sort") {
                //Neglecting these
            }
            else if (fieldName.split("-")[0] == "Attributes") {
                if (criteriaModel.Criteria[fieldName.split("-")[1]].length > 0) {
                    if (fieldName.split("-")[1] == "IsNew") {
                        termQuery = {
                            "terms": {
                                "IsNew": criteriaModel.Criteria[fieldName.split("-")[1]]
                            }
                        };
                    } else {
                        termQuery = {
                            "terms": {
                                "Attributes.Value": criteriaModel.Criteria[fieldName.split("-")[1]]
                            }
                        };
                    }
                }
            }
            else if (fieldName == "Categories") {
                termQuery = {
                    "match_phrase": {
                        "Categories.Name": criteriaModel.Criteria[fieldName]
                    }
                };
            }
            else if (fieldName == "ShopId") {
                termQuery = {
                    "match_phrase": {
                        "ShopId": criteriaModel.Criteria[fieldName]
                    }
                };
            }
            else if (fieldName == "AdminCategory") {
                termQuery = {
                    "match_phrase": {
                        "Categories.Name": criteriaModel.Criteria[fieldName]
                    }
                };
            } else if (fieldName == "SubCategories") {
                termQuery = {
                    "terms": {
                        "Categories.SubCategories._id": criteriaModel.Criteria[fieldName]
                    }
                };
            } else if (fieldName == "Shop") {
                termQuery = {
                    "terms": {
                        "ShopId": criteriaModel.Criteria[fieldName]
                    }
                };
            } else if (fieldName == "ShopSection") {
                if (criteriaModel.Criteria[fieldName] != 'all') {
                    termQuery = {
                        "match_phrase": {
                            "ShopSection.Id": criteriaModel.Criteria[fieldName].key
                        }
                    };
                }
            } else if (fieldName == "Search") {
                if (criteriaModel.Criteria[fieldName]) {
                    termQuery = {
                        "query_string": {
                            "default_field":"Name",
                            "query": "*" + criteriaModel.Criteria[fieldName] + "*"

                        }
                    }
                };
            } else if (fieldName == "Price") {
                let from = criteriaModel.Criteria.Price.From;
                let to = criteriaModel.Criteria.Price.To;
                if (from > to) {
                    from = criteriaModel.Criteria.Price.To;
                    to = criteriaModel.Criteria.Price.From;
                }
                termQuery = {
                    "range":
                    {
                        "Price": {
                            "gte": from,
                            "lte": to
                        }
                    }
                };
            } else if (fieldName) {
                //   console.log("FieldNAme",fieldName); 
                if (criteriaModel.Criteria[fieldName].length > 0) {
                    //     console.log("FieldNAme Entered If",fieldName, criteriaModel.Criteria[fieldName]); 
                    termQuery = {
                        [Array.isArray(criteriaModel.Criteria[fieldName]) ? "terms" : "term"]: {
                            [fieldName]: criteriaModel.Criteria[fieldName]
                        }
                    };
                }
            }
        }

        return termQuery;
    }

    private extractDataGet(res: Response) {
        let productsList = res.json();
        return JSON.stringify(productsList);
    }
    private extractData(res: Response) {
        return res.status;
    }
    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        // console.error(errMsg);
        return Promise.reject(errMsg);
    }

}

