import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import { Router } from '@angular/router';
import * as express from 'express';
import * as q from 'q';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class SectionService {
  private sections: any;

  constructor(private http: Http) { }

  setSection(sectionModel): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let body = sectionModel;
   // console.log('working', sectionModel);
    let response;
    return this.http.post(config.apiUrl+'/api/shop-section', body, { headers: headers })
     .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  putSection(sectionModel): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let body = sectionModel;
    //console.log('working', body);
    let response;
    return this.http.put(config.apiUrl+'/api/shop-section/update/' + sectionModel._id, body, { headers: headers })
    .toPromise()
    .then(this.extractData)
    .catch(this.handleError); 
  }
    
  displayProductLinkToSection(id): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let response;
    return this.http.get(config.apiUrl+'/api/product/section/' +id,{ headers: headers })
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError); 
  }
    
 getSectionById(id): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let response;
    return this.http.get(config.apiUrl+'/api/shop-section/sectionview/'+ id,{ headers: headers })
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError); 
  }   
    
  deleteSection(Section): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let response;
    return this.http.delete(config.apiUrl+'/api/shop-section/' + Section, { headers: headers })
    .toPromise()
    .then(this.extractData)
    .catch(this.handleError); 
  }

  getAll(page): Promise<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    return this.http.get(config.apiUrl+'/api/shop-section?offset=' + page, { headers: headers })
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError); 
  }

  private extractData(res: Response) {
    let SectionsList = res.json();
    if(res.status == 409){
         return JSON.stringify(SectionsList);
       }else{ 
      //   console.log("success");
       return JSON.stringify(SectionsList);
  }
}

  private handleError(error: Response | any) {

    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    }
    else {
      errMsg = error.message ? error.message : error.toString();
    }
   // console.error(errMsg);
    return Promise.reject(errMsg);
  }

}