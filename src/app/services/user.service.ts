import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import { Router } from '@angular/router';
import * as express from 'express';
import * as q from 'q';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');

import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class UserService {

    constructor(private http: Http,private cookie:CookieService) { }

    GetAllUsers(model, from?, size?,sortType?):Promise<any>
    {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        
        let response;

        if(size)
        {
            return this.http.post(config.apiUrl+'/api/user/view-users/?offset='+from+'&limit='+size+'&sort='+sortType, {UserNameOrEmail:model}, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError)
        }

        return this.http.post(config.apiUrl+'/api/user/view-users', {UserNameOrEmail:model}, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

 
    verifyMobEmail(verifyUser): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        //headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        let body = {
            username:verifyUser
        };
        let response;
       // console.log(body);
        return this.http.post(config.apiUrl+'/api/user/verifyMobEmail',body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    getUserBlock(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        return this.http.get(config.apiUrl+'/api/findUserBlock', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    unBlockeduser(userModel): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        let body = userModel;
        let response;
        return this.http.post(config.apiUrl+'/api/user/unblockuser', body, { headers: headers })
            .toPromise()
            .then(this.extractDataStatus)
            .catch(this.handleError);
    }
    viewBlockedusers(userModel, pageNumber, size): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        return this.http.get(config.apiUrl+'/api/user/viewblockeduser/?offset=' + pageNumber+'&limit='+size, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    putBlockuser(userModel): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        let body = userModel;
        let response;
        return this.http.post(config.apiUrl+'/api/user/blockuser', body, { headers: headers })
            .toPromise()
            .then(this.extractDataStatus)
            .catch(this.handleError);
    }

    putProfile(userModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        let body = userModel;
     //   console.log('working', body);
        let response;
        this.http.put(config.apiUrl+'/api/user/profile', body, { headers: headers })
            .subscribe(
            data => response = data, //For Success Response
            err => { 
                //console.error(err)
             }, //For Error Response
            () => { 
                //console.log("Updated"); 
            }
            );
    }

    resetPasswordLink(link): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        //headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        let body = link;
        let response;
       // console.log(body);
        return this.http.post(config.apiUrl+'/api/auth/resetpasswordactivate?token=' + link, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    resetPassword(body): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        // headers.append('Authorization', 'Bearer ' + token);
        // let body = {userModel,token};
    //    console.log('working', body);
        let response;
        return this.http.put(config.apiUrl+'/api/user/reset-password', body, { headers: headers })
            .toPromise()
            .then(this.extractDataStatus)
            .catch(this.handleError);
    }
    changePassword(userModel): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        let body = userModel;
       // console.log('working', body);
        let response;
        return this.http.put(config.apiUrl+'/api/user/change-password', body, { headers: headers })
            .toPromise()
            .then(this.extractDataStatus)
            .catch(this.handleError);
    }
    // generateCaptcha(): Promise<any> {
    //     let headers = new Headers();
    //     headers.append('Content-Type', 'application/json');
    //     headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
     
    
       
    //     return this.http.get(config.apiUrl+'/api/user/generateCaptcha', { headers: headers })
    //         .toPromise()
    //         .then(this.extractData)
    //         .catch(this.handleError);
    // }
    generateCaptcha(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        return this.http.get(config.apiUrl+'/api/user/generateCaptcha', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    getCaptcha(captchatext): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
    
        return this.http.get(config.apiUrl+'/api/user/getCaptcha?text='+captchatext,{ headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    getUserDetailsById(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
    
        return this.http.get(config.apiUrl+'/api/user/getUserDetail',{ headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getUserDetailsByNameOrEmail(userName): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        let body = {
            "Value": userName
        };
        let response;
        //console.log(body);
        return this.http.post(config.apiUrl+'/api/user/viewprofile', body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    changeSellerTier(tierValue, userId): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        let body = {
            "TierValue": tierValue,
            "UserId": userId
        };
    //    console.log('working', body);
        let response;
        return this.http.put(config.apiUrl+'/api/user/change-tier', body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    getAllSellers(userName, pageNumber) : Promise<any> {
        
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        let response;
        return this.http.get(config.apiUrl+'/api/user/all-sellers?offset='+pageNumber+ '&userName='+userName,  { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    showProfile(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        return this.http.get(config.apiUrl+'/api/user', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    updateProfile(profileModel): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        let body = profileModel;
      //  console.log('working', body);
        let response;
        return this.http.put(config.apiUrl+'/api/user/profile', body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    savePreference(preferenceModel): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        let body = preferenceModel;
        let response;
        //console.log(body);
        return this.http.post('api/preferences', body, { headers: headers })
            .toPromise()
            .then(this.extractDataStatus)
            .catch(this.handleError);
    }

    viewFavouriteShop(from,to): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        return this.http.get(config.apiUrl+'/api/shop-favorite?from='+from+'&size='+to, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    viewFavouriteProduct(from,to): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        return this.http.get(config.apiUrl+'/api/favorite-product?from='+from+'&size='+to, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    viewFavProduct(from,to): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        return this.http.get(config.apiUrl+'/api/favorite-product/user?from='+from+'&size='+to, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    viewReview(pageNumber,limit): Promise<any> {

        let headers = new Headers();
        let currentPage = pageNumber;

        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.cookie.get('userToken'));
        return this.http.get(config.apiUrl+'/api/product/user/review?offset='+currentPage+'&limit='+limit+'&sort=3', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    getAll(): Promise<any> {

        let profile;
        return this.http.get(config.apiUrl+'/api/user/profile')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    getAllShop(): Promise<any> {


        return this.http.get(config.apiUrl+'/api/user/shop')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    private extractDataStatus(res: Response) {
        return res.status;
    }

    private extractData(res: Response) {
        let profile = res.json();
        return JSON.stringify(profile);
    }

    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        //console.error(errMsg);
        return Promise.reject(errMsg);
    }

}