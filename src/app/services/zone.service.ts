import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
const config = require('../../../config/app-config.json');

@Injectable()
export class ZoneService {

    constructor(private http:Http){}

    GetCountries(): Promise<any> {
        // let headers = new Headers();
        // headers.append('Content-Type', 'application/json');
        // headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        console.log("Inside service");
        return this.http.get('http://services.groupkt.com/state/get/IND/all')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    CheckDelete(country):Promise<any>
    {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/check-delete-zone', {Country:country}, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    CheckDeleteType(type, country):Promise<any>
    {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/check-delete-zone-type', {Type:type, Country:country}, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }


    SaveZone(model): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/zone', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    GetAllZone(pagenumber,limit): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/zone/getall?limit='+limit+'&offset='+pagenumber, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    GetZones(): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/zone/getall', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    GetZoneById(model): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/zone/'+model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
     
    UpdateZone(model):any {
         
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/zone/' +model._id, model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    DeleteZone(model):any {
         
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.delete(config.apiUrl+'/api/zone/' +model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    CheckCountry(model): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/zone/country', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    CheckMultiCountry(model): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/zone/multicountry', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    private extractData(res: Response) {
        let data = res.json();
        return JSON.stringify(data);
    }

    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Promise.reject(errMsg);
    }
}