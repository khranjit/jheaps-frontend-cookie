import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class AddToCartService {
    private cartProducts: any;

    constructor(private http: Http) { }

    setCartProduct(cartModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = cartModel;
      //  console.log('working', body);
        let response;
        return this.http.post(config.apiUrl+'/api/cart/product', body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    clearBuyNow(): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/cart/clear-buynow', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    saveForLaterCartProduct(cartModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/saveforlater', cartModel, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    getSavedForLater(): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/saveforlater', { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    deleteSavedForLater(product): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/saveforlater/move/',product, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
     ChangeProductSubProfile(productId, shippingMethodObject): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/cart/update-shipping-cost/'+productId, shippingMethodObject, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    getProductById(productId){
      //  console.log('product id:'+productId);
        return this.http.get('api/cart/get-product-detail/'+productId)
        .toPromise()
        .then(this.extractDataGet)
        .catch(this.handleError);
    }
    getSingleProductById(productId){
        //  console.log('product id:'+productId);
          return this.http.get('api/cart/single/product/'+productId)
          .toPromise()
          .then(this.extractDataGet)
          .catch(this.handleError);
      }
      
    
    putCartProduct(cartModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = cartModel;
        //console.log('working', body);
        let response;
        return this.http.put('api/cart/product' + cartModel._id, body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    deleteCartProduct(cart): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        var query: any = {};
        query= {ProductId: cart.Product._id};
        if(cart.hasOwnProperty("Variation"))
        {
            query.VariationId = cart.Variation._id;
        }
        return this.http.post('api/cart/removecartproduct',query,{ headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    getAll(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get('api/cart',{ headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }


    getBuyNow(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get('api/cart/buynow',{ headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    getCartProductsCount(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get('api/cart/count',{ headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    showTempCart(products)
    {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post('api/tempcart',products,{ headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    cartChange(model){

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/cart/edit',model,{ headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);

    }
    proceedToCheckout(ShippingAddressModel){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = ShippingAddressModel;
        return this.http.get('api/pay-pal/adaptive' ,{ headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    getPaymentApprovalUrl(model,cartId): any{  
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken')); 
        let body={"ShippingAddress" :model};
     //   console.log(body);
        return this.http.post(config.apiUrl+'/api/payu/update-cart/' + cartId,body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    getPaymentApprovalUrlPaybiz(model,cartId): any{  
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        
        return this.http.post('api/payu/update-cart/' + cartId,model, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
          
    }
    getPaymentApprovalUrlOneShopIN(model,cartId): any{  
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken')); 
       // let body={"ShopId" : ShopId,"ShippingAddress" :model};
        return this.http.post(config.apiUrl+'/api/payu/update-cart/' + cartId,model, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
     getPaymentApprovalUrlOneShopCOM(model,cartId,ShopId): any{  
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken')); 
        let body={"ShopId" : ShopId,"ShippingAddress" :model};
        return this.http.post('api/payu/update-cart/' + cartId,body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    contactShop(MessageViewModel){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post('api/message/send' , MessageViewModel,{ headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    applyCoupon(model){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/cart/products-after-discount',model,{ headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    cancelCoupon(model){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/cart/products-after-cancel-coupon',model ,{ headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    private extractData(res: Response) {
        return res.status;

    }
    private extractDataGet(res: Response) {
        let cartProducts = res.json();
        return JSON.stringify(cartProducts);

    }

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Promise.reject(errMsg);
    }

}
var reportData = {};
export class tempservice {
    // reportData = [];
    setData(item)
    {  
        reportData=item;
    }

    getData()
    {
        return (reportData);
       // return (reportData.length > 0)? reportData : null;
    }

    // clearData()
    // {
    //     this.reportData = [] ;
    // }
}
export class TempCartService {
    tempCart:any = [];
    cartValue:any = {};
    favoriteshop:any=null;
    favoriteproduct:any=null;

    createBuyNow(value)
    {
        this.cartValue = {
            number: value,
            valid: false
        }
        return true;
    }
    updateBuyNow()
    {
        this.cartValue.valid = true;
        return true;
    }
    getBuyNow()
    {
        return this.cartValue.valid;
    }

    PushTempCart(item)
    {  
        item = JSON.parse(item);
        console.log(this.tempCart);
         var message = "";
        if(this.tempCart.length>0)
        {
            this.tempCart.forEach(element => {
                if(element.Product === item.Product)
                {
                    if(element.hasOwnProperty("Variation") && item.hasOwnProperty("Variation"))
                    {
                        if(element.Variation == item.Variation)
                        {
                            message = "Item already present";
                        }
                    }
                    else
                    {
                        message = "Item already present";                        
                    }
                }
            });
        }
        if(message === "")
        this.tempCart.push(item);
       // console.log(this.tempCart);
       // console.log(this.tempCart.count);
        return message;
    }

    GetTempCart()
    {
        return (this.tempCart.length > 0)? this.tempCart : null;
    }
    updateQtyAndVariation(product)
    {
        this.tempCart.find(x=>{
            if(x.Product == product.Product)
            {
                if(x.hasOwnProperty('Variation'))
                {
                    if(x.Variation == product.Variation)
                    {
                        x.Qty = product.Qty;
                        if(product.hasOwnProperty("NewVariation"))
                        {
                            x.Variation = product.NewVariation;
                        }
                    }
                }
                else
                {
                    x.Qty = product.Qty; 
                }
            }
        })
    }
    removeProduct(product)
    {
        var indexVal = 0;
        this.tempCart.find((x,index)=>{
            if(x.Product == product.Product._id)
            {
                if(x.hasOwnProperty('Variation'))
                {
                    if(x.Variation == product.Variation._id)
                    {
                        indexVal = index;
                    }
                }
                else
                {
                    indexVal = index;
                }
            }
        })  
        this.tempCart.splice(indexVal,1);
    }

    ClearTempCart()
    {
        this.tempCart = [] ;
    }

    setShopID(shopID)
    {
        this.favoriteshop=shopID;
    }

    getShopID()
    {
        return this.favoriteshop;
    }

    clearShopID()
    {
        this.favoriteshop=null;
    }

    setProductID(productID)
    {
        this.favoriteproduct=productID;
    }

    getProductID()
    {
        return this.favoriteproduct;
    }

    clearProductID()
    {
        this.favoriteproduct=null;
    }
}

export class BuyNowService {
     isBuyNow:boolean = false;

    ActivateBuyNow(){
        this.isBuyNow = true;
    }

    CheckBuyNow()
    {
        return this.isBuyNow
    }

    ClearBuyNow()
    {
        this.isBuyNow = false;
    }
}

// export class CartReloadService{
//     private _subject = new Subject<any>();
    
//       newEvent(event) {
//         this._subject.next(event);
//       }
    
//       get events$ () {
//         return this._subject.asObservable();
//       }
// }