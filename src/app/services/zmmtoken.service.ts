import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Router} from '@angular/router';
import 'rxjs/add/operator/map';
import * as q from 'q';
//import apiProxy = require('../../proxy/proxy');
import * as express from 'express';
const config = require('../../../config/app-config.json');
@Injectable()
export class ZmmtokenServices {


    constructor(private http: Http) {
        //   http.get('user.json').subscribe((res:Response) => this.http=res.json());
    }

    getZmmtoken(): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/zmm/get-token',  { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let zmmtoken = res.json();
     //   console.log("from home service",JSON.stringify(zmmtoken) )
        return JSON.stringify(zmmtoken);
    }

    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
       // console.error(errMsg);
        return Promise.reject(errMsg);
    }
  
}