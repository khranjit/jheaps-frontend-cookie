import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class CheckoutService {
         private paymentstatus: any;
        
        constructor(private http: Http) { }
         
        
        CheckPaymentStatus(status){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        //console.log(status)
        
        return this.http.post(config.apiUrl+'/api/payu/make-order/',status,{ headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
        }
    
        private extractData(res: Response) {
            return res.status;
    
        }
        private extractDataGet(res: Response) {
            let paymentstatus = res.json();
            return JSON.stringify(paymentstatus);
    
        }
    
        private handleError(error: Response | any) {
            let errMsg: string;
            if (error instanceof Response) {
                const body = error.json() || '';
                const err = body.error || JSON.stringify(body);
                errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
            }
            else {
                errMsg = error.message ? error.message : error.toString();
            }
            return Promise.reject(errMsg);
        }

}