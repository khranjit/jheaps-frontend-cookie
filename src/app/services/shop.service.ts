import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import { Router } from '@angular/router';
import * as express from 'express';
import * as q from 'q';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class ShopService {


    constructor(private http: Http) { }
    
    getShops(): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/shop', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    finalshop(shopModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/shop', shopModel, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
   

    vacationdetails(vacationModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/shop/set-vacation', vacationModel, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    updateVacationdetails(vacationModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/shop/update-vacation', vacationModel, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    updateShippingdetails(shippingModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/shop/update-shipping', shippingModel, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    resetVacationdetails(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/shop/reset-vacation', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
 

    deletevacation(vacationId): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.delete(config.apiUrl+'/api/shop/delete-vacation/'+vacationId, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    AddShippingDetails(vacationModel): Promise<any>{
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/shop/add-shipping-amount', vacationModel, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    getShippingDetails():Promise<any>{
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/shop/get-shipping', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getvacationdetails(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/shop/get-vacation', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    getShopDetailsFromShopId(shopId): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/shop/view/'+ shopId, { headers: headers })
            .toPromise()
            .then(this.extractData) 
            .catch(this.handleError);
    }
    getShopOwnerDetail(shopId): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/shop/owner/'+ shopId, { headers: headers })
            .toPromise()
            .then(this.extractData) 
            .catch(this.handleError);
    }

    getShopSectionsByShopId(shopId): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/shop-section/viewbyshop/'+ shopId, { headers: headers })
            .toPromise()
            .then(this.extractData) 
            .catch(this.handleError);  
    }
    
    getOwnShopDetails(): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/shop/ownshop', { headers: headers })
            .toPromise()
            .then(this.extractData) 
            .catch(this.handleError);
    }
    
    updateShop(shopModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/shop/updateshop', shopModel, { headers: headers })
            .toPromise()
            .then(this.extractData) 
            .catch(this.handleError);
    }
    
    checkCODAvailable(): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/shop/checkcod',  { headers: headers })
            .toPromise()
            .then(this.extractData) 
            .catch(this.handleError);
    }

    getShopReviews(shopId)
    {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/product/shop/review/'+shopId, { headers: headers })
            .toPromise()
            .then(this.extractData) 
            .catch(this.handleError);
        }
        getOverallRating(shopId)
        {
            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
            return this.http.get(config.apiUrl+'/api/product/shop/overllRating/'+shopId, { headers: headers })
                .toPromise()
                .then(this.extractData) 
                .catch(this.handleError);
            }
        // getShopReviewPage(shopId)
        // {
        //     let headers = new Headers();
        //     headers.append('Content-Type', 'application/json');
        //     headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        //     return this.http.get(config.apiUrl+'/api/product/shop/review/'+shopId, { headers: headers })
        //         .toPromise()
        //         .then(this.extractData) 
        //         .catch(this.handleError);
        //     }
            getShopReviewPage(offset,limit,shopId) {
                var modal = {
                  shopid:shopId
                }
                let headers = new Headers();
                headers.append('Content-Type', 'application/json');
                headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
                return this.http.get(config.apiUrl+'/api/product/shopAll/review/'+shopId+'?offset='+offset+'&limit='+limit, { headers: headers })
            
                    .toPromise()
                    .then(this.extractData)
                    .catch(this.handleError);
            }
       
    checkCourierAvailability(pincode){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'SHIPIT 06bd027a79eeeff5b66cbf6961f84e2:NTQ3ODA1ZWVhNGY1OTRhMjJlY2RiOWMzMDU2ZjMzYzU1NzRjZGM4Yw==');
       let model={"pickup_pincode":pincode,
                "delivery_pincode":pincode,
                "invoice_value":1,
                "payment_mode":"online",
                "insurance":"false","number_of_package":1,
                "package_details":
                    [{
                    "package_content":"Description",
                    "no_of_items":1,
                    "invoice_value":1,
                    "package_dimension":
                        {
                        "weight":1,
                        "height":11,
                        "length":11,
                        "width":11
                        }
                    }],
                "product_type":"Parcel"
                }

        
        
        let body={"pickup_pincode":pincode, "delivery_pincode":pincode,
         "payment_mode":"online","insurance":"false","number_of_package":1,
         "package_details":[{"package_content":"Description","no_of_items":1,"invoice_value":1,"package_dimension":{"weight":1.0,"height":11.0,"length":11.0,"width":11.0}}],
         "product_type":"Parcel"};
        return this.http.post('http://api.couriers.vello.in/users/1585/pincodeservice/rate', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
        }
    
    shopDetails(shopModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/shop', shopModel, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    getShopID()
    {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/shop-account', { headers: headers })
            .toPromise()
            .then(this.extractData) 
            .catch(this.handleError);
        }

        CheckBusinessName(item)
     {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/shop-account/checkbusinessname/?businessName='+item, { headers: headers })
            .toPromise()
            .then(this.extractData) 
            .catch(this.handleError);
        }
        CheckBusinessDisplayName(item)
        {
           let headers = new Headers();
           headers.append('Content-Type', 'application/json');
           headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
           return this.http.get(config.apiUrl+'/api/shop-account/checkbusinessdisplayname/?businessDisplayName='+item, { headers: headers })
               .toPromise()
               .then(this.extractData) 
               .catch(this.handleError);
           }

        CheckGSTIN(item)
        {
           let headers = new Headers();
           headers.append('Content-Type', 'application/json');
           headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
           return this.http.get(config.apiUrl+'/api/shop-account/checkgstin/?GSTIN='+item, { headers: headers })
               .toPromise()
               .then(this.extractData) 
               .catch(this.handleError);
           }

           CheckTAN(item)
           {
              let headers = new Headers();
              headers.append('Content-Type', 'application/json');
              headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
              return this.http.get(config.apiUrl+'/api/shop-account/checktan/?TAN='+item, { headers: headers })
                  .toPromise()
                  .then(this.extractData) 
                  .catch(this.handleError);
              }

              CheckPAN(item)
              {
                 let headers = new Headers();
                 headers.append('Content-Type', 'application/json');
                 headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
                 return this.http.get(config.apiUrl+'/api/shop-account/checkpan/?PAN='+item, { headers: headers })
                     .toPromise()
                     .then(this.extractData) 
                     .catch(this.handleError);
                 }

                 CheckPersonalPAN(item)
              {
                 let headers = new Headers();
                 headers.append('Content-Type', 'application/json');
                 headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
                 return this.http.get(config.apiUrl+'/api/shop-account/checkppan/?PPAN='+item, { headers: headers })
                     .toPromise()
                     .then(this.extractData) 
                     .catch(this.handleError);
                 }

                 CheckPhone(item)
                 {
                    let headers = new Headers();
                    headers.append('Content-Type', 'application/json');
                    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
                    return this.http.get(config.apiUrl+'/api/shop-account/checkphone/?Phone='+item, { headers: headers })
                        .toPromise()
                        .then(this.extractData) 
                        .catch(this.handleError);
                    }

                    CheckAccount(number, ifsc)
                    {
                       let headers = new Headers();
                       headers.append('Content-Type', 'application/json');
                       headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
                       return this.http.get(config.apiUrl+'/api/shop-account/checkaccount/?Account='+number+'&ifsc='+ifsc ,{ headers: headers })
                           .toPromise()
                           .then(this.extractData) 
                           .catch(this.handleError);
                       }


    
    putShopDetailBusiness(id, model)
    {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/shop-account/business/'+id, model,{ headers: headers })
            .toPromise()
            .then(this.extractData) 
            .catch(this.handleError);
    }
    putShopDetailBusinessByAdmin(id, model)
    {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/shop-account/businessbyadmin/'+id, model,{ headers: headers })
            .toPromise()
            .then(this.extractData) 
            .catch(this.handleError);
    }
    updateBusinessPhoneNumber(id, model)
    {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/shop-account/updatebusinessPhone/'+id, model,{ headers: headers })
            .toPromise()
            .then(this.extractData) 
            .catch(this.handleError);
    }
    
    putShopDetailBank(id, model)
    {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/shop-account/bank/'+id, model,{ headers: headers })
            .toPromise()
            .then(this.extractData) 
            .catch(this.handleError);
        }

    private extractData(res: Response) {
        let createdShop = res.json();
        return JSON.stringify(createdShop);
    }
    
    private extractDataStatus(res: Response) {
        return res.status;

    }

    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
   //     console.error(errMsg);
        return Promise.reject(errMsg);
    }
}