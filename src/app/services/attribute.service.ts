import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import * as express from 'express';
import * as q from 'q';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');

@Injectable()
export class AttributeService {
  private attributes: any;

  constructor(private http: Http) { }

  setAttribute(attributeModel): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let body = attributeModel;
  //  console.log('working', body);
    let response;
    return this.http.post(config.apiUrl+'/api/attribute', body,{ headers: headers })
     .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  setAttribute1(attributeModel): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let body = attributeModel;
  //  console.log('working', body);
    let response;
    return this.http.post(config.apiUrl+'/api/attribute1', body,{ headers: headers })
     .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  getcategoriesbasedbyProducts(shopId){
    return this.http.get(config.apiUrl+'/api/fetchcategorybyshop/' + shopId)
    .toPromise()
    .then(this.extractDataGet)
    .catch(this.handleError); 
}

  fetchAttributesbyShop(attributeModel): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let body = attributeModel;
  //  console.log('working', body);
    let response;
    return this.http.post(config.apiUrl+'/api/fetchattributebyshop',body,{ headers: headers })
     .toPromise()
      .then(this.extractDataGet)
      .catch(this.handleError);
  }
  getcategoriesbasedbyGlobal(){
    return this.http.get(config.apiUrl+'/api/fetchcategorybyglobal')
    .toPromise()
    .then(this.extractDataGet)
    .catch(this.handleError); 
  }
  fetchAttributesbyGlobal(attributeModel): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let body = attributeModel;
  //  console.log('working', body);
    let response;
    return this.http.post(config.apiUrl+'/api/fetchattributebyglobal',body,{ headers: headers })
     .toPromise()
      .then(this.extractDataGet)
      .catch(this.handleError);
  }

  fetchAttributes(attributeModel): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let body = attributeModel;
  //  console.log('working', body);
    let response;
    return this.http.post(config.apiUrl+'/api/fetchattribute',body,{ headers: headers })
     .toPromise()
      .then(this.extractDataGet)
      .catch(this.handleError);
  }

  checkAttribute(model): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let body = {"attributeName":model.Name, "attributeId": model._id};
  //  console.log('working', body);
    let response;
    return this.http.post(config.apiUrl+'/api/attribute/check-attribute', body,{ headers: headers })
     .toPromise()
      .then(this.extractDataGet)
      .catch(this.handleError);
  }

  checkAttributeName(model): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let body = {"attributeName":model.Name};
  //  console.log('working', body);
    let response;
    return this.http.post(config.apiUrl+'/api/attribute/check-attribute-name', body,{ headers: headers })
     .toPromise()
      .then(this.extractDataGet)
      .catch(this.handleError);
  }

  checkAttributeValue(model, value): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let body = {"attributeName":model.Name, "attributeValue":value};
  //  console.log('working', body);
    let response;
    return this.http.post(config.apiUrl+'/api/attribute/check-attribute-value', body,{ headers: headers })
     .toPromise()
      .then(this.extractDataGet)
      .catch(this.handleError);
  }



  putAttribute(attributeModel): any {
    let headers = new Headers();
    //  console.log("ID---", attributeModel._id);
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let body = attributeModel;
   // console.log('working', body);
    let response;
    return this.http.put(config.apiUrl+'/api/attribute/'+attributeModel._id, body,{ headers: headers })
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  putAttributeValues(attributeModel): any {
    let headers = new Headers();
    //  console.log("ID---", attributeModel._id);
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let body = attributeModel;
   // console.log('working', body);
    let response;
    return this.http.put(config.apiUrl+'/api/attributeValues/'+attributeModel._id, body,{ headers: headers })
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  deleteAttribute(Attribute): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let response;
    return this.http.delete(config.apiUrl+'/api/attribute/'+Attribute._id,{ headers: headers })
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  getAll(from?, size?): Promise<any> {

    if(size)
    {
      return this.http.get(config.apiUrl+'/api/attribute/all/?from='+from+'&size='+size)
      .toPromise()
      .then(this.extractDataGet)
      .catch(this.handleError);
    }
    //let attributesList;
    return this.http.get(config.apiUrl+'/api/attribute/all')
      .toPromise()
      .then(this.extractDataGet)
      .catch(this.handleError);
  }
  getUserDefined(from?, size?): Promise<any> {

    if(size)
    {
      return this.http.get(config.apiUrl+'/api/userattribute/all/?from='+from+'&size='+size)
      .toPromise()
      .then(this.extractDataGet)
      .catch(this.handleError);
    }
    //let attributesList;
    return this.http.get(config.apiUrl+'/api/userattribute/all')
      .toPromise()
      .then(this.extractDataGet)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
        return res.status;
    
  }
  private extractDataGet(res: Response) {
     let attributesList = res.json();
    return JSON.stringify(attributesList);
      
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    }
    else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Promise.reject(errMsg);
  }

}