import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Router} from '@angular/router';
import 'rxjs/add/operator/map';
import * as q from 'q';
//import apiProxy = require('../../proxy/proxy');
import * as express from 'express';
const config = require('../../../config/app-config.json');
@Injectable()
export class ReportsService {


    constructor(private http: Http) {
        //   http.get('user.json').subscribe((res:Response) => this.http=res.json());
    }

    getReports(reportsModel): any {
        let headers = new Headers();
        reportsModel.download = false;
        headers.append('Content-Type', 'application/json');
        headers.append('Data-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/seller-report/Reports', reportsModel, { headers: headers })
            .toPromise()
            .then(this.extractData)


            .catch(this.handleError);
    }
    downloadReport(reportsModel): any {
        let headers = new Headers();
        reportsModel.download = true;
        headers.append('Content-Type', 'application/json');
        headers.append('Data-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/seller-report/excel-download', reportsModel, { headers: headers })
            .toPromise()
            .then(this.extractData)


            .catch(this.handleError);
    }
    getReport1(reportsModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Data-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/seller-report/report1', reportsModel, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    ViewSettlementReport(model){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Data-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/seller-report/viewSettlementReport',model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    PaymentComplete(model){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Data-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/seller-report/paymentcomplete',model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    PaymentCompleteUpdate(model){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Data-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/seller-report/paymentcompleteupdate',model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    Report33(model){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Data-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/seller-report/report33',model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    downloadSettlementReport(BatchId,testrun){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Data-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        var model = {
            Batch:BatchId,
            Test:testrun
        }
        return this.http.post(config.apiUrl+'/api/seller-report/downloadSettlementReport',model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    downloadCODSettlementReport(BatchId,testrun){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Data-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        var model = {
            Batch:BatchId,
            Test:testrun
        }
        return this.http.post(config.apiUrl+'/api/seller-report/downloadCODSettlementReport',model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    fetchSettlementReports(){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Data-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/seller-report/fetchsettlementReports', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    fetcholdsettlementReports(model){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Data-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/seller-report/fetcholdsettlementReports',model ,{ headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    private extractData(res: Response) {
        let reports = res.json();
     //   console.log("from home service",JSON.stringify(reports) )
        return JSON.stringify(reports);
    }
    deleteReport(deModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
      //  headers.append('Data-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/seller-report/excel/remove-file', deModel, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    } 

    private handleError(error: Response | any) {

        // let errMsg: string;
        // if (error instanceof Response) {
        //     const body = error.json() || '';
        //     const err = body.error || JSON.stringify(body);
        //     errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        // }
        // else {
        //     errMsg = error.message ? error.message : error.toString();
        // }
       // console.error(error);
        return Promise.reject(error);
    }
  
}