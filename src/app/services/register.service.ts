import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Router} from '@angular/router';
import 'rxjs/add/operator/map';
import * as q from 'q';
//import apiProxy = require('../../proxy/proxy');
import * as express from 'express';
const config = require('../../../config/app-config.json');
@Injectable()
export class RegisterService {


    constructor(private http: Http) {
        //   http.get('user.json').subscribe((res:Response) => this.http=res.json());
    }

    //this UserNameTest() method is used to check the existing user name in database
    UserNametest(userName){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
     //   console.log("Service works");
     //   console.log("service passing parameter:"+userName);
        return this.http.post(config.apiUrl+'/api/auth/username', {UserName:userName})
        .toPromise()
        .then(this.extractData)
        .catch(this.handleError);
    }

    UserMailtest(email){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
     //   console.log("Service works");
     //   console.log("service passing parameter:"+userName);
        return this.http.post(config.apiUrl+'/api/auth/email', {Email:email})
        .toPromise()
        .then(this.extractData)
        .catch(this.handleError);
    }

    register(registermodel) {
        //   let response = apiProxy.CallApi(config.apiUrl+'/api/auth/signup', 'POST', req)
        //   console.log(response);
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        let body = registermodel;
         console.log('working', registermodel);
        let response;
        return this.http.post(config.apiUrl+'/api/auth/signup', body)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    getCountries(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/auth/countries', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    checkPincode(model)
    {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/auth/pincode',model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    getStates(countryId) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let options = new RequestOptions({ headers: headers });
        let body = countryId;
        return this.http.post(config.apiUrl+'/api/auth/states', body)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    getCities(stateId) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let options = new RequestOptions({ headers: headers });
        let body = stateId;
        return this.http.post(config.apiUrl+'/api/auth/cities', body)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    forgotPassword(fPasswordmodel) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let options = new RequestOptions({ headers: headers });
        let body = fPasswordmodel;
       // console.log('working', fPasswordmodel);
        let response;
        return this.http.post(config.apiUrl+'/api/auth/forgot-password', body)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    SendOTP(number){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        var body = {
            numb : number
        }
        let options = new RequestOptions({ headers: headers });

        return this.http.post(config.apiUrl+'/api/auth/sendotp', body)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    resetPassword(model){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.post(config.apiUrl+'/api/auth/resetpassword', model)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    ForgotSendOTP(number){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        var body = {
            numb : number
        }
        let options = new RequestOptions({ headers: headers });

        return this.http.post(config.apiUrl+'/api/auth/forgotsendotp', body)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    SendEmailOTP(email){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        var body = {
            email : email
        }
        let options = new RequestOptions({ headers: headers });
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/auth/sendemailotp', body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    OTPCheck(model){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });

        return this.http.post(config.apiUrl+'/api/auth/checkotp', model)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    EmailOTPCheck(model){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/auth/emailcheckotp', model,{ headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    SaveMobile(mobile){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let options = new RequestOptions({ headers: headers });
        return this.http.put(config.apiUrl+'/api/auth/savemobile', mobile, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    SaveEmail(mobile){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let options = new RequestOptions({ headers: headers });
        return this.http.put(config.apiUrl+'/api/auth/saveemail', mobile, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    UpdateProfileImage(model){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let options = new RequestOptions({ headers: headers });
        return this.http.put(config.apiUrl+'/api/auth/saveimage', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    private extractData(res: Response) {
        let loginInfo = res.json();
        if (res.status == 200) {
            return JSON.stringify(loginInfo);
        }
        else {
            let err = {
                "Token": "NULL",
                "TimeToExpireInDays": 300,
                "Activated": false
            }
            return JSON.stringify(err);
        }

    }

    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        //console.error(errMsg);
        return Promise.reject(errMsg);
    }





}