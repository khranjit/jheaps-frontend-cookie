import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
const config = require('../../../config/app-config.json');
@Injectable()
export class CODLimitService {
    
    
    
    constructor(private http: Http) { }
    
    
   
    
    getCODLimit():Promise<any>{
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/codlimit', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError); 
    }


     saveCODLimit(model): Promise<any>{
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/codlimit/save',model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);   
    }

    updateCODLimit(model): Promise<any>{
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/codlimit/update',model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);   
    }
    
   

    private extractData(res: Response) {
        let responseData = JSON.stringify(res.json());
        return responseData;
    }

    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Promise.reject(errMsg);
    }

}

