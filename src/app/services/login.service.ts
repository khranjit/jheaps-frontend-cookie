import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Subject } from 'rxjs/Subject';
const config = require('../../../config/app-config.json');
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class LoginService {

  constructor(private http: Http,private cookie: CookieService) { }

  login(loginmodel): Promise<any> {
    // console.log(user.message);
    let body = loginmodel;
    let response;
   // console.log(body);
    return this.http.post(config.apiUrl+'/api/auth/login', body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  
  isLoggedIn(){

      let userToken = this.cookie.get('userToken');
      if(userToken){
          return true;
      }
      return false;
  }
  
  glogin(gloginmodel): Promise<any> {
    // console.log(user.message);
    let body = gloginmodel;
    let response;
   // console.log(body);
    return this.http.post(config.apiUrl+'/api/auth/google', body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  fblogin(fbloginmodel): Promise<any> {
    // console.log(user.message);
    let body = fbloginmodel;
    let response;
   // console.log(body);
    return this.http.post(config.apiUrl+'/api/auth/facebook', body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let loginInfo = res.json();
    if (res.status == 200) {
      return JSON.stringify(loginInfo);
    }
    else {
      let err = {
        "Token": "NULL",
        "TimeToExpireInDays": 300,
        "Activated": false
      }
      return JSON.stringify(err);
    }

  }

  private handleError(error: Response | any) {

    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    }
    else {
      errMsg = error.message ? error.message : error.toString();
    }
   // console.error(errMsg);
    return Promise.reject(errMsg);
  }


  logout(): Promise<any> {
    let productsList;
    return this.http.get(config.apiUrl+'/api/auth/log-out')
      .toPromise()
      .then(this.Data)
      .catch(this.handleError);
  }

  private Data(res: Response) {
    let status = res.status;
    return status;
  }
}

export class LoginRedirectService {

  private _subject = new Subject<any>();
  private _loginRedirectURL = '';

  SetRedirectURL(url)
  {   
    this._loginRedirectURL = url;
  }

  GetRedirectURL()
  {
      return (this._loginRedirectURL)? this._loginRedirectURL : '';
  }

  ClearRedirectURL()
  {
      this._loginRedirectURL = '';
  }

  newEvent() {
    this._subject.next(this.GetRedirectURL());
  }

  get events$ () {
    var subject = this._subject.asObservable();
    return subject;
  }

}


export class LogoutRedirectService{

  private _subject = new Subject<any>();
  private _logoutRedirectURL = '';

  SetRedirectURL(url)
  {   
    this._logoutRedirectURL = url;
  }

  GetRedirectURL()
  {
      return (this._logoutRedirectURL)? this._logoutRedirectURL : '';
  }

  ClearRedirectURL()
  {
      this._logoutRedirectURL = '';
  }

  newEvent() {
    this._subject.next(this.GetRedirectURL());
  }

  get events$ () {
    return this._subject.asObservable();
  }

}