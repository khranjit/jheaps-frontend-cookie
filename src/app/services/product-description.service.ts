import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import { Router } from '@angular/router';
import * as express from 'express';
import * as q from 'q';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class ProductDescriptionService {
  private productdescription: any;

  constructor(private http: Http) { }

  getProductById(productModel): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(config.apiUrl+'/api/product/'+productModel)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  getProductByProductId(productModel): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(config.apiUrl+'/api/product/open/'+productModel)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  saveTrending(productModel): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(config.apiUrl+'/api/trending-product/add/'+productModel)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  saveRecommended(productModel): any {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
      return this.http.post(config.apiUrl+'/api/recommended-product', productModel, { headers: headers })
        .toPromise()
        .then(this.extractData)
        .catch(this.handleError);
  }
  getRecommended(): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    return this.http.get(config.apiUrl+'/api/recommended-product', { headers: headers })
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
}
  getTrending(): Promise<any> {
    return this.http.get(config.apiUrl+'/api/trending-product/show')
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  getRandom(offset?,limit?): Promise<any> {
    return this.http.get(config.apiUrl+'/api/randomproducts?offset='+offset+'&limit='+limit)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  getAll(): Promise<any> {
    let featuredshopList;
    return this.http.get(config.apiUrl+'/api/product/58ef6e2afb4ec424135a3d39')
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
  private extractData(res: Response) {
    let responseData = res.json();
    return JSON.stringify(responseData);
  }

  
  private handleError(error: Response | any) {

    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    }
    else {
      errMsg = error.message ? error.message : error.toString();
    }
 //   console.error(errMsg);
    return Promise.reject(errMsg);
  }
  



  
}