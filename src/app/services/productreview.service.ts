import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import { Router } from '@angular/router';
import * as express from 'express';
import * as q from 'q';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class ProductReviewService {
    private productreview: any;

    constructor(private http: Http) { }

    saveReview(model)
    {
        let headers = new Headers();

        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/product/review/',model,{ headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getAll(offset,limit,productId): Promise<any> {

        let productreviewList;
        let headers = new Headers();

        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/product/review/' + productId+'?offset='+offset+'&limit='+limit, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    viewReviewList(offset,limit,productId) {
        var modal = {
          prodid:productId
        }
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/product/review/'+productId+'?offset='+offset+'&limit='+limit,{ headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    viewReviewListSort(offset,limit,productId,sort) {
        var modal = {
          prodid:productId
        }
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/product/review/'+productId+'?offset='+offset+'&limit='+limit+'&sort='+sort,{ headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    checkReview(productId): Promise<any> {

        let productreviewList;
        let headers = new Headers();

        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/product/checkreview/' + productId, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let productreviewList = res.json();
        return JSON.stringify(productreviewList);
    }

    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
//        console.error(errMsg);
        return Promise.reject(errMsg);
    }





}