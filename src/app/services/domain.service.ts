import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
//import apiProxy = require('../../proxy/proxy');
import { Router } from '@angular/router';
import * as express from 'express';
import * as q from 'q';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class DomainService {

    constructor(private http: Http, private location: Location) { }

    getDomain() {
        let domain = location.origin
        if (domain.match(".in"))
            return "in";
        else if (domain.match(".com"))
            return "com";
    }
}