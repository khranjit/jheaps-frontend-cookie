import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import { Router } from '@angular/router';
import * as express from 'express';
import * as q from 'q';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
const config = require('../../../config/app-config.json');
@Injectable()
export class CategoryService {
    private categories: any;

    constructor(private http: Http,  private location: Location) { }

    setCategory(categoryModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = categoryModel;
      //  console.log('working', categoryModel);
        let response;
        return this.http.post(config.apiUrl+'/api/category', body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
  updateCategory(categoryModel,id): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = categoryModel;
        //console.log('working', categoryModel);
        let response;
        return this.http.put(config.apiUrl+'/api/category/'+id, body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    selectCategory(categoryModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = categoryModel;
       // console.log('working', categoryModel);
        let response;
        return this.http.post(config.apiUrl+'/api/category/homepage/mega-menu', body, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    showCategories(): Promise<any> {
        let categoriesList;
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        var body = {
            "DomainName": "in"
        };
        return this.http.post(config.apiUrl+'/api/category/treeview', body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }


    deleteCategory(Category): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        return this.http.delete(config.apiUrl+'/api/category/' + Category._id, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getAll(offset): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        var body = {
            "DomainName": "in"
        };
        /*return this.http.get(config.apiUrl+'/api/category?offset='+offset, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);*/
        return this.http.post(config.apiUrl+'/api/category/find-all', body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    getCategoryInTreeView(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        var body = {
            "DomainName": "in"
        };
        /*return this.http.get(config.apiUrl+'/api/category/treeview')
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);*/
        return this.http.post(config.apiUrl+'/api/category/treeview', body, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError)
    }

    getCategoryList(model): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/category/treeview', model, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    getAllListWithoutPagination(model,from?,size?): any {
        
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        if(size)
        {
            return this.http.post(config.apiUrl+'/api/category/find-all/?from=' + from + '&size=' + size, model, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
        }
        return this.http.post(config.apiUrl+'/api/category/find-all', model, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    getAllCategories(model):any{
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
       
        return this.http.post(config.apiUrl+'/api/category/find-categories', model, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    getDomain() {
        let domain = location.origin
        if (domain.match(".in"))
            return "in";
        else if (domain.match(".com"))
            return "com";
    }

    checkCategory(model): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = {"categoryModel":model};
      //  console.log('working', body);
        let response;
        return this.http.post(config.apiUrl+'/api/attribute/checkcategory', body,{ headers: headers })
         .toPromise()
          .then(this.extractDataGet)
          .catch(this.handleError);
      }

      checkCategoryAttribute(model, name): any {
        let headers = new Headers();
        model.AttrName = name;
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = {"categoryModel":model};
      //  console.log('working', body);
        let response;
        return this.http.post(config.apiUrl+'/api/attribute/checkcategoryattribute', body,{ headers: headers })
         .toPromise()
          .then(this.extractDataGet)
          .catch(this.handleError);
      }
    
      checkCategoryName(model): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let body = {"categoryName":model.Name};
      //  console.log('working', body);
        let response;
        return this.http.post(config.apiUrl+'/api/attribute/checkcategoryname', body,{ headers: headers })
         .toPromise()
          .then(this.extractDataGet)
          .catch(this.handleError);
      }
    
    private extractData(res: Response) {
        return res.status;

    }
    private extractDataGet(res: Response) {
        let categoriesList = res.json();
        return JSON.stringify(categoriesList);

    }

    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        //console.error(errMsg);
        return Promise.reject(errMsg);
    }

}