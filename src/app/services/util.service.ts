import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
const config = require('../../../config/app-config.json');
@Injectable()
export class UtilService {
    
    private awsImageModel: ViewModels.IAwsImageModel;
    invokeEvent: Subject<any> = new Subject();
    
    constructor(private http: Http) { }
    
    
    convertToBase64AndUpload(file){
        let response, url;
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        this.awsImageModel = <ViewModels.IAwsImageModel>{};
        var myReader: FileReader = new FileReader();
        myReader.readAsDataURL(file);
        myReader.onloadend = (e) => {
            this.awsImageModel.Name = file.name.split(".")[0];
            this.awsImageModel.ImageData = myReader.result.toString();
            var i = new Image();
            var self = this;
            i.onload= function(){
                if(i.width > 750 && i.height > 500){
                    self.http.post(config.apiUrl+'/api/aws/image/upload', {
                        "Name" :  file.name.split(".")[0],
                        "ImageData" : myReader.result
                    }, { headers: headers })
                        .subscribe(
                            data => response = data.json(), //For Success Response
                            err => { console.error(err) }, //For Error Response
                            () => {
                                url = JSON.stringify(response);
                           //     console.log("URL "+url);
                                self.invokeEvent.next(url);
                            }
                        );
                }else{
                    self.invokeEvent.next("error");
                }
            }
            i.src = myReader.result.toString();
        }
    }
    convertToBase64AndUploadImage(file,width,height){
        let response, url;
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        this.awsImageModel = <ViewModels.IAwsImageModel>{};
        var myReader: FileReader = new FileReader();
        myReader.readAsDataURL(file);
        myReader.onloadend = (e) => {
            this.awsImageModel.Name = file.name.split(".")[0];
            this.awsImageModel.ImageData = myReader.result.toString();
            var i = new Image();
            var self = this;
            i.onload= function(){
                if(i.width > width && i.height > height){
            //         self.http.post(config.apiUrl+'/api/aws/file-img/upload', {
            //             "Name" :  file.name.split(".")[0],
            //             "ImageData" : myReader.result
            //         }, { headers: headers })
            //             .subscribe(
            //                 data => response = data.json(), //For Success Response
            //                 err => { console.error(err) }, //For Error Response
            //                 () => {
            //                     url = JSON.stringify(response);
            //  //                   console.log("URL "+url);
            //                     self.invokeEvent.next(url);
            //                 }
            //             );

                  self.uploadBase64AndGetUrl(file.name.split(".")[0], myReader.result).then(
                      data =>{
                        url = JSON.parse(data).ImageData;
                        self.invokeEvent.next(url);
                      }, err=> {console.log(err);}
                  )
                }else{
                    self.invokeEvent.next("error");
                }
            }
            i.src = myReader.result.toString();
        }
    }
    
    convertToBase64Change(file, width, height, size){
        let response, base64;
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        var myReader: FileReader = new FileReader();
        myReader.readAsDataURL(file);
        myReader.onloadend = (e) => {
            var i = new Image();
           
            var self = this;
            i.onload= function(){
              //  console.log("From Service ,",i.width,i.height)
                if(i.width >= width && i.height >= height && size <= 5e+7 ){
                    base64 = myReader.result;
                    self.invokeEvent.next(base64);
                 }else{
                    self.invokeEvent.next("error");
                }
            }
            i.src = myReader.result.toString();
        }
    }
    convertToBase64ChangeBanner(file){
        let response, base64;
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        var myReader: FileReader = new FileReader();
        myReader.readAsDataURL(file);
        myReader.onloadend = (e) => {
            var i = new Image();
           
            var self = this;
            i.onload= function(){
                    base64 = myReader.result;
                    self.invokeEvent.next(base64);
   
            }
            i.src = myReader.result.toString();
        }
    }
    
    // convertToBase64(file){
    //     let response, base64;
    //     let headers = new Headers();
    //     headers.append('Content-Type', 'application/json');
    //     headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    //     var myReader: FileReader = new FileReader();
    //     myReader.readAsDataURL(file);
    //     myReader.onloadend = (e) => {
    //         var i = new Image();
    //         var self = this;
    //         i.onload= function(){
    //             if(i.width > 750 && i.height > 500){
    //                 base64 = myReader.result;
    //                 self.invokeEvent.next(base64);
    //                 console.log("entered");
    //             }else{
    //                 self.invokeEvent.next("error");
    //             }
    //         }
    //         i.src = myReader.result;
    //     }
    // }

    convertToBase64(file){
        let response, base64;
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        var myReader: FileReader = new FileReader();
        myReader.readAsDataURL(file);
        myReader.onloadend = (e) => {
                //    console.log(myReader.result);
                    var i = new Image();
                    //i.invokeEvent.next(base64);
                    var self = this;
                    i.onload= function(){
                        
                            base64 = myReader.result;
                            self.invokeEvent.next(base64);
                  //          console.log("entered");
                       
                    }
                    i.src = myReader.result.toString();
                }
            }
    
    
      convertToBase64_All(file){
        let response, base64;
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        var myReader: FileReader = new FileReader();
        myReader.readAsDataURL(file);
        myReader.onloadend = (e) => {
            var i = new Image();
            var self = this;
            i.onload= function(){
                base64 = myReader.result;
               self.invokeEvent.next(base64);
                
            }
            i.src = myReader.result.toString();
        }
    }
    
    
    uploadBase64AndGetUrl(fileName, base64): Promise<any>{
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/aws/file-img/upload', {
                        "Name" :  fileName,
                        "Base64Data" : base64
                    }, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);   
    }
    uploadBase64AndGetUrlproduct(fileName, base64,folder): Promise<any>{
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/aws/file-img/upload', {
                        "Name" :  fileName,
                        "Base64Data" : base64,
                        "folder":folder
                    }, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);   
    }
    deleteAWSFile(deModel): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
      //  headers.append('Data-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/aws/remove-file', deModel, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    updatebannerImage(model): Promise<any>{
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/banner/updatebanner', model, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);   
    }
    
    ValidateCouriers(zip, paymentMode):Promise<any>{
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/zepo/validatecouriers/?zip='+zip+'&paymentMode='+paymentMode, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError); 
    }

    ValidateCouriersCODSeller():Promise<any>{
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/zepo/validate-seller-cod/', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError); 
    }

    public ValidateZepo(shopId?):Promise<any>{
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        if(shopId)
        {
            return this.http.get(config.apiUrl+'/api/zepo/validate-cart/?shopId='+shopId, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
        }
        return this.http.get(config.apiUrl+'/api/zepo/validate-cart/', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError); 
    }

     uploadBase64AndGetUrlFile(fileName, ext, base64,file): Promise<any>{
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/aws/fileupload', {
                        "Name" :  fileName,
                        "Extension" : "."+ext,
                        "Base64Data" : base64,
                        "File" : file
                    }, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);   
    }
    
    getBase64FromUrl(url): Promise<any>{
        var last = url.substring(url.lastIndexOf("/") + 1, url.length);
        let fileName = {
            "filename" : last
        };
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/aws/image/getdata', fileName, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);   
    }
    getBase64FromUrlforShopInfo(fileName): Promise<any>{
       console.log("getBase64FromUrlforShopInfo is called")
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/aws/image/getdata', fileName, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);   
    }
    
    geoLocation(){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get('//ipinfo.io', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);   
    }
  
    uplaodImageAndGetUrl(awsImageModel): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/aws/image/upload', awsImageModel, { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    
    getCountries(): any {
        let countryList = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua &amp; Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Cape Verde","Cayman Islands","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cruise Ship","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kuwait","Kyrgyz Republic","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Mauritania","Mauritius","Mexico","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Namibia","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Pakistan","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre &amp; Miquelon","Samoa","San Marino","Satellite","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","South Africa","South Korea","Spain","Sri Lanka","St Kitts &amp; Nevis","St Lucia","St Vincent","St. Lucia","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks &amp; Caicos","Uganda","Ukraine","United Arab Emirates","United Kingdom","Uruguay","Uzbekistan","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];
        return countryList;
    }
    
    getCountriesList(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/auth/countries', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }
    getAllCitiesList(): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/auth/allcities', { headers: headers })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let responseData = JSON.stringify(res.json());
        return responseData;
    }

    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Promise.reject(errMsg);
    }

}

