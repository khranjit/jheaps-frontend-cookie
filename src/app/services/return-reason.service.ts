import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import { Router } from '@angular/router';
import * as express from 'express';
import * as q from 'q';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class ReturnReasonService {
  private attributes: any;

  constructor(private http: Http) { }

  createReason(reason): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let body = reason;
  //  console.log('working', body);
    let response;
    return this.http.post(config.apiUrl+'/api/returnreason/create', body,{ headers: headers })
     .toPromise()
      .then(this.extractDataGet)
      .catch(this.handleError);
  }

  checkReason(id): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
  //  console.log('working', body);
    let response;
    return this.http.get(config.apiUrl+'/api/returnreason/checkreason/'+id,{ headers: headers })
     .toPromise()
      .then(this.extractDataGet)
      .catch(this.handleError);
  }

  editReason(reason): any {
    let headers = new Headers();
    //  console.log("ID---", attributeModel._id);
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let body = reason;
   // console.log('working', body);
    let response;
    return this.http.put(config.apiUrl+'/api/returnreason/edit', body,{ headers: headers })
      .toPromise()
      .then(this.extractDataGet)
      .catch(this.handleError);
  }
  deleteReason(id): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let response;
    return this.http.delete(config.apiUrl+'/api/returnreason/delete/'+id,{ headers: headers })
      .toPromise()
      .then(this.extractDataGet)
      .catch(this.handleError);
  }
  getAllDropDownReason(): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
    let response;
    return this.http.get(config.apiUrl+'/api/returnreason/getdropdownreasons',{ headers: headers })
      .toPromise()
      .then(this.extractDataGet)
      .catch(this.handleError);
  }
  getAll(from?, size?): Promise<any> {

    if(size)
    {
      return this.http.get(config.apiUrl+'/api/returnreason/viewreasons/?offset='+from+'&limit='+size)
      .toPromise()
      .then(this.extractDataGet)
      .catch(this.handleError);
    }
    //let attributesList;
    return this.http.get(config.apiUrl+'/api/returnreasons')
      .toPromise()
      .then(this.extractDataGet)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
        return res.status;
    
  }
  private extractDataGet(res: Response) {
     let reason = res.json();
    return JSON.stringify(reason);
      
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    }
    else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Promise.reject(errMsg);
  }

}