import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { ObjectUnsubscribedError } from 'rxjs';
const config = require('../../../config/app-config.json');
@Injectable()
export class AdminCriteriaService {

    invokeEvent: Subject<any> = new Subject();
    private awsImageModel: ViewModels.IAwsImageModel;

    constructor(private http: Http) { }

    public getAdminCriteriaList(from?,size?) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        if(size)
        {
            return this.http.get(config.apiUrl+'/api/adminshopby/?offset='+from+'&limit='+size, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
        }
        return this.http.get(config.apiUrl+'/api/adminshopby', { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    public getAdminCriteriaById(id) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/adminshopby/shop/' + id, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    public listSubAdminProfile(from?, size?) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        if(size)
        {
            return this.http.get(config.apiUrl+'/api/auth/subadmin/?offset='+from+'&limit='+size, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
        }
        return this.http.get(config.apiUrl+'/api/auth/subadmin', { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    public deleteAdminCriteria(id): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.delete(config.apiUrl+'/api/adminshopby/'+id, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    displayBannerLinkToCriteria(id): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        let response;
        return this.http.get(config.apiUrl+'/api/adminshopby/' +id,{ headers: headers })
          .toPromise()
          .then(this.extractDataGet)
          .catch(this.handleError); 
      }
    
    
    public updateAdminCriteria(adminCriteriaModal): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/adminshopby/'+adminCriteriaModal._id, adminCriteriaModal, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    public addAdminCriteria(adminCriteriaModal): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/adminshopby', adminCriteriaModal, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    viewSubAdminProfile(profileModel, from?, size?): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/auth/finduser/?offset='+from+'&limit='+size, profileModel, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    makeSubAdmin(profileModel): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/auth/subadmin/add ', profileModel, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    deleteSubAdmin(profileModel): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
//        console.log(profileModel);
        return this.http.post(config.apiUrl+'/api/auth/subadmin/remove ', profileModel, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    /*
    * Start Banner Services
    */
    
    public getBannerList(pageNumber, size) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.get(config.apiUrl+'/api/banner/?offset='+pageNumber+'&limit='+size, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    updateBanner(bannerModel, id): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.put(config.apiUrl+'/api/banner/'+id, bannerModel, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    addBanner(bannerModel): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.post(config.apiUrl+'/api/banner', bannerModel, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }
    
    deleteBanner(id): Promise<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
        return this.http.delete(config.apiUrl+'/api/banner/'+id, { headers: headers })
            .toPromise()
            .then(this.extractDataGet)
            .catch(this.handleError);
    }

    convertToBase64AndUpload(file): any {
  //      console.log("file", file)
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.awsImageModel = <ViewModels.IAwsImageModel>{};
        var myReader: FileReader = new FileReader();
        myReader.readAsDataURL(file);
        let url;
        let response;
        myReader.onloadend = (e) => {
            this.awsImageModel.Name = file.name.split(".")[0];
            //this.awsImageModel.Description = "Zgroo Image Upload";
            this.awsImageModel.ImageData = myReader.result.toString();
            this.http.post(config.apiUrl+'/api/aws/image/upload', this.awsImageModel, { headers: headers })
                .subscribe(
                data => response = data.json(), //For Success Response
                err => { console.error(err) }, //For Error Response
                () => {
                    url = JSON.stringify(response);
                    this.invokeEvent.next(url);
                }
                );
        }
    }
    
    /*
    * End Banner Services
    */
    
   /* private extractData(res: Response) {
        return res.status;

    }*/
    private extractDataGet(res: Response) {
        let responseData = res.json();
        return JSON.stringify(responseData);

    }

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Promise.reject(errMsg);
    }

}