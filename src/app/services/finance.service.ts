import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import apiProxy = require('../../proxy/proxy');
import { Router } from '@angular/router';
import * as express from 'express';
import * as q from 'q';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
const config = require('../../../config/app-config.json');
@Injectable()
export class FinanceService {
  	constructor(private http: Http) { }
		showCoupon(offset,limit): Promise<any> {
		    let headers = new Headers();
		    headers.append('Content-Type', 'application/json');
		    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
		    return this.http.get(config.apiUrl+'/api/coupon?limit='+limit+'&offset='+offset,{ headers: headers })
		      .toPromise()
		      .then(this.extractDataGet)
		      .catch(this.handleError);
	  	}
	  	deleteCoupon(CouponModel): any {
			let headers = new Headers();
		    headers.append('Content-Type', 'application/json');
		    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
		    let response;
		    return this.http.delete(config.apiUrl+'/api/coupon/'+CouponModel._id,{ headers: headers })
		      .toPromise()
		      .then(this.extractData)
		      .catch(this.handleError);
		 }
		 addCoupon(couponModel): Promise<any> {
		 	let headers = new Headers();
			headers.append('Content-Type', 'application/json');
			headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
			let body = couponModel ;
			let response;
			//console.log(body);
			return this.http.post(config.apiUrl+'/api/coupon', body,{ headers: headers })
				.toPromise()
			    .then(this.extractDataGet)
			 	.catch(this.handleError);  
		}
		 updateCoupon(CouponModel): any {
		    let headers = new Headers();
		    headers.append('Content-Type', 'application/json');
		    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));
		    let body = CouponModel																																																																																																																																																																																																																																																																																				;
		    //console.log('working', body);
		    let response;
		    return this.http.put(config.apiUrl+'/api/coupon/'+CouponModel._id, body,{ headers: headers })
		      .toPromise()
		      .then(this.extractData)
		      .catch(this.handleError);
		  }

		  getCouponProducts(body):any{
			let headers = new Headers();
		    headers.append('Content-Type', 'application/json');
		    headers.append('Authorization', 'Bearer ' + localStorage.getItem('userToken'));																																																																																																																																																																																																																																																																																				;
//		    console.log('working', body);
		    let response;
		    return this.http.post(config.apiUrl+'/api/coupon/products', body,{ headers: headers })
		      .toPromise()
		      .then(this.extractDataGet)
		      .catch(this.handleError);
		  }
		
		  private extractDataGet(res: Response) {
	      		let data = res.json();
			    return JSON.stringify(data);
			      
			  }
		  private extractData(res: Response) {
		    return res.status;
	  		}
		  private handleError(error: Response | any) {
	      	let errMsg: string;
			if (error instanceof Response) {
				const body = error.json() || '';
			    const err = body.error || JSON.stringify(body);
			    errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
			 }
			 else {
			 	errMsg = error.message ? error.message : error.toString();
			 }
			  return Promise.reject(errMsg);
		}
	}