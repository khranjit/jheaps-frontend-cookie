import { NgtUniversalModule } from '@ng-toolkit/universal';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, ApplicationRef, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule }   from './shared/shared.module';
import { HttpModule } from '@angular/http';
import { ToasterModule } from 'angular2-toaster'; 
import { CookieModule } from 'ngx-cookie';
import { NgxSpinnerService } from 'ngx-spinner';
import { Ng2CompleterModule } from "ng2-completer";
// import {ShareButtonsModule} from 'ngx-sharebuttons';
import { FormsModule } from '@angular/forms';
import { removeNgStyles, createNewHosts, createInputTransfer } from '@angularclass/hmr';
import {  TempCartService, BuyNowService, tempservice} from './services/cart.service';
import { LoginRedirectService, LogoutRedirectService } from './services/login.service';
import {NotifyService} from './services//notify.service';
import { SuccessSnackComponent,ErrorSnackComponent } from './snack/snack.component';
import {MatSnackBarModule, MatFormFieldModule, MatSelectModule, MatCheckboxModule, MatInputModule, } from '@angular/material';
import { HomeModule } from './root/home/home.module';
import { AuthGuard } from './auth/auth-guard.guard';
import { AuthGuardAdmin } from './auth/admin-auth-guard.guard';
import { HomeService } from './services/home.service';
import { NgxSpinnerModule } from 'ngx-spinner';
@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    SuccessSnackComponent,
    ErrorSnackComponent
  ],
  imports: [
    CommonModule,
    NgtUniversalModule,
    TransferHttpCacheModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserModule,
    NgxSpinnerModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    AppRoutingModule,
    SharedModule,
    HomeModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    CookieModule.forRoot(),
    Ng2CompleterModule,
    ToasterModule,
    NgxSpinnerModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [
    TempCartService,
    tempservice,
    LoginRedirectService,
    LogoutRedirectService,
    BuyNowService,
    HomeService,
    NotifyService,
    AuthGuard,
    AuthGuardAdmin
  ],
  entryComponents: [SuccessSnackComponent,ErrorSnackComponent]
})
export class AppModule { }
