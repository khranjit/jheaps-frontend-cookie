import { NgModule, NO_ERRORS_SCHEMA, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import {NgPipesModule} from 'ngx-pipes';
import { AddFavouriteProductDirective } from '../directives/add-favourite-product.directive';
import { AddFavouriteShopDirective } from '../directives/add-favourite-shop.directive';
import { ImageBinding } from '../directives/image-binding.directive';
import { ProductImageDirective } from '../directives/product-image.directive';
import { ProductPriceDirective } from '../directives/product-price.directive';
import { ProductOutOfStockDirective } from '../directives/product-out-of-stock.directive';
import { AddToCartDirective } from "../directives/add-to-cart.directive";
import { XLargeDirective } from '../app.component';



const MODULES = [
    // Do NOT include UniversalModule, HttpModule, or JsonpModule here
    CommonModule,
    FormsModule,
    ReactiveFormsModule
];

const PIPES = [
    // put pipes here
];

const COMPONENTS = [
    // put shared components here
    
];

const PROVIDERS = [

]


@NgModule({
    imports: [
    ],
    declarations: [
        ...PIPES,
        ...COMPONENTS,
        AddFavouriteProductDirective,
        ProductImageDirective,
        ProductPriceDirective,
        ProductOutOfStockDirective,
        AddFavouriteShopDirective,
        ImageBinding,
        AddToCartDirective,
        XLargeDirective
    ],
    exports: [
        ...MODULES,
        ...PIPES,
        ...COMPONENTS,
        AddFavouriteProductDirective,
        ProductImageDirective,
        ProductPriceDirective,
        ProductOutOfStockDirective,
        AddFavouriteShopDirective,
        ImageBinding,
        AddToCartDirective,
        XLargeDirective,
        NgPipesModule
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
        };
    }
}
