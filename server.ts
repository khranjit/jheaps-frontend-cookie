import 'zone.js/dist/zone-node';
    import 'reflect-metadata';
    import {enableProdMode} from '@angular/core';
    import {ngExpressEngine} from '@nguniversal/express-engine';
    import {provideModuleMap} from '@nguniversal/module-map-ngfactory-loader';
    
    import * as express from 'express';
    import * as bodyParser from 'body-parser';
    import * as cors from 'cors';
    import * as compression from 'compression';
    import {join} from 'path';
    const session  = require('express-session');
    const config = require('./config/app-config.json');
    var RedisStore = require('connect-redis')(session);
    import Proxy from './src/proxy/proxy';
    const domino = require('domino');
    const fs = require('fs');
    var path    = require("path");
    const DIST_FOLDER = path.join(process.cwd(), 'dist/browser');
    const template = fs.readFileSync(path.join(DIST_FOLDER,'/index.html')).toString();
    const win = domino.createWindow(template);
    import 'localstorage-polyfill';
    const MockBrowser = require('mock-browser').mocks.MockBrowser;
    const mock = new MockBrowser();
    global['navigator'] = mock.getNavigator();
    global['localStorage'] = localStorage;
    global['window'] = win;
    global['document'] = win.document;
    
    enableProdMode();
    
    export const app = express();
    
    app.use(compression());
    app.use(cors());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.urlencoded({ extended: true, limit: config.frontend.maxFileSize }));
    app.use(bodyParser.json({limit: config.frontend.maxFileSize}));
    app.use(session({
            secret: config.redis.secret,
            store: new RedisStore({ 
            host:config.redis.host, 
            port: config.redis.port, 
            ttl: config.redis.ttl, 
            prefix: 'pp.sess' ,
            pass : config.redis.password
            }),
            resave: true,
            saveUninitialized: true
        }));
    app.use('/api', Proxy.ApiProxy(config.webServer.host,config.webServer.port));
    app.use('/elastic', Proxy.ApiProxy(config.elastic.host,config.elastic.port));
    
    // const DIST_FOLDER = join(process.cwd(), 'dist');
    
    const {AppServerModuleNgFactory, LAZY_MODULE_MAP} = require('./dist/server/main');
    
    app.engine('html', ngExpressEngine({
      bootstrap: AppServerModuleNgFactory,
      providers: [
        provideModuleMap(LAZY_MODULE_MAP)
      ]
    }));
    
    app.set('view engine', 'html');
    app.set('views', './dist/browser');
    
    app.get('/redirect/**', (req, res) => {
      const location = req.url.substring(10);
      res.redirect(301, location);
    });
    
    app.get('*.*', express.static('./dist/browser', {
      maxAge: '1y'
    }));
    
    app.get('/*', (req, res) => {
      res.render('index', {req, res}, (err, html) => {
        if (html) {
          res.send(html);
        } else {
          console.error(err);
          res.send(err);
        }
      });
    });
    