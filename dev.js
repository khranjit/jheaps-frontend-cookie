const express = require('express'),
bodyParser  = require('body-parser'),
session     = require('express-session');
const app = express();
const config = require('./config/app-config.json');
var RedisStore = require('connect-redis')(session);
var path    = require("path");
 var proxy = require('./src/proxy/proxy');
app.use(bodyParser.urlencoded({ extended: true, limit: config.frontend.maxFileSize }));
app.use(bodyParser.json({limit: config.frontend.maxFileSize}));
app.use(session({
        secret: config.redis.secret,
        store: new RedisStore({ 
          host:config.redis.host, 
          port: config.redis.port, 
          ttl: config.redis.ttl, 
          prefix: 'pp.sess' ,
          pass : config.redis.password
        }),
        resave: true,
        saveUninitialized: true
    }));
  
app.use('/node_modules', express.static(__dirname + '/node_modules'));     
app.use( express.static( __dirname + '/dist/browser' ));
app.use('/api', proxy.ApiProxy(config.webServer.host,config.webServer.port));
app.use('/elastic', proxy.ApiProxy(config.elastic.host,config.elastic.port));
app.get('/*', function (req, res) {
  res.sendFile(path.join( __dirname +'/dist','/browser/index.html'));
})
app.listen(5200,'0.0.0.0', function () {
  console.log('Server started Successfully at port 5200!')
})
